# Imli-Backend (All the Backend code of Imli)

This repository goes by following details:

## Ruby version
`ruby-2.2.1`

## System dependencies
* rvm (ruby version manager)
* PostgreSQL 9.4
* Redis
* MongoDB

## Configuration

## Database creation

## Database initialization

## How to run the test suite

## Services (job queues, cache servers, search engines, etc.)

## Deployment instructions
