# This migration comes from supply_chain_engine_engine (originally 20151007135411)
#
# create table warehouse
#
class CreateCashAndCarry < ActiveRecord::Migration
  #
  # create table cash_and_carry
  #
  # name: name of c&c
  #
  def up
    create_table :cash_and_carries do |t|
      t.string :name
      t.integer :status

      t.timestamps null: false
    end
  end

  # 
  # delete cash_and_carry table
  # 
  def down
    drop_table :cash_and_carries
  end
end
