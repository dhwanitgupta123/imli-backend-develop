class SequentialBrandPackCode < ActiveRecord::Migration
  #
  # Migration to create sequence for the Brand pack code
  #
  def up
    max_bp_code = MasterProductModule::V1::BrandPack.maximum('brand_pack_code')
    if max_bp_code.blank?
      max_bp_code = '1'
    else
      max_bp_code = (max_bp_code.to_i + 1).to_s
    end
    say "Creating sequence for  brand pack code starting at max_bp_code"
    execute 'CREATE SEQUENCE brand_pack_code_seq START ' + max_bp_code + ';'
    say "Adding NEXTVAL('brand_pack_code_seq') to column brand_pack_code"
    execute "ALTER TABLE brand_packs ALTER COLUMN brand_pack_code SET DEFAULT NEXTVAL('brand_pack_code_seq');"
  end

  #
  # Function to rollback the migration
  #
  def down
    execute 'DROP SEQUENCE IF EXISTS brand_pack_code_seq CASCADE;'
  end
end
