class RenamePdfLinkToImageInInventoryOrder < ActiveRecord::Migration

  #
  # Migration to add column for images & remove column for pdf_link
  #
  def up
    remove_column :inventory_orders, :pdf_link
    add_column :inventory_orders, :images, :integer, array: true, default: []
  end

  #
  # function to rollback the migration
  #
  def down
    remove_column :inventory_orders, :images
    add_column :inventory_orders, :pdf_link, :string, default: ''
  end
end
