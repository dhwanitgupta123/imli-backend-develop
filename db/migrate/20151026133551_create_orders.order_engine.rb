# This migration comes from order_engine
#
# create table orders
#
class CreateOrders < ActiveRecord::Migration
  #
  # create table orders
  #
  # ship_by: ship_by date
  # status: status of order
  #
  def up
    create_table :orders do |t|
      t.string :order_id
      t.string :ship_by
      t.string :status

      t.references :user, index: true, foreign_key: true
      t.references :address, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # delete orders table
  # 
  def down
    drop_table :orders
  end
end