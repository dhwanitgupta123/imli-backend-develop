#
# Migration to add service tax column in Plan table
#
class AddServiceTaxToPlan < ActiveRecord::Migration
  #
  # Function to add service tax column to Plan table
  #
  def up
    add_column :plans, :service_tax, :decimal, default: 0.0
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :plans, :service_tax
  end
end
