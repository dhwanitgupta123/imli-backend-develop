#
# This migration add initial role to the Role Table
#
class AddInitialRole < ActiveRecord::Migration
  def up
    # populate Roles table
    UsersModule::V1::Role.create({:role_name => 'IMLI_API_ROLE', :permissions => '{1}'})
  end

  def down
    initial_role = UsersModule::V1::Role.find_by(role_name: 'IMLI_API_ROLE')
    initial_role.destroy
  end
end
