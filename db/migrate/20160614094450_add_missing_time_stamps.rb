class AddMissingTimeStamps < ActiveRecord::Migration
  def up
    add_timestamps :areas
    add_timestamps :geographical_clusters
    add_timestamps :third_party_orders
  end
end
