class AddCancelledInwardStateToStock < ActiveRecord::Migration
  #
  # Migration to crrect the data for inward of cancelled orders
  #
  def up
    order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
    order_stock_helper = MarketplaceOrdersModule::V1::OrderStockingHelper
    order_mapper = MarketplaceOrdersModule::V1::OrderMapper
    warehouse_stock = WarehouseProductModule::V1::WarehouseStock
    wbp_service = WarehouseProductModule::V1::WarehouseBrandPackService
    cart_service = MarketplaceOrdersModule::V1::CartService
    warehouse_stock_service = WarehouseProductModule::V1::WarehouseStockService
    stock_states = WarehouseProductModule::V1::ModelStates::V1::WarehouseStockStates
    order_helper = MarketplaceOrdersModule::V1::OrderHelper
    cancelled_order_states = [order_state::ORDER_CANCELLED[:value], order_state::CANCELLED_BY_ADMIN[:value]]

    cancelled_orders = MarketplaceOrdersModule::V1::Order.where(status: cancelled_order_states)

    warehouse_brand_pack_service = wbp_service.new
    cart_service = cart_service.new
    warehouse_stock_service = warehouse_stock_service.new
    warehouse_inward_stocks = warehouse_stock.where(status: [stock_states::INWARD, stock_states::CANCELLED_INWARD])
    if warehouse_inward_stocks.present?
      warehouse_inward_stocks.each do |inward_stock|
        inward_stock.delete
      end
    end
    if cancelled_orders.present?
      cancelled_orders.each do |order|
        order_stock_helper.inward_warehouse_brand_packs(order.cart)
      end
    end
  end
end
