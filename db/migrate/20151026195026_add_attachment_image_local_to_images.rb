class AddAttachmentImageLocalToImages < ActiveRecord::Migration
  #
  # creates images table
  # 
  # image_local: locally save image
  # image_s3: image save in s3 
  #
  def up
    create_table :images do |t|
      t.attachment :image_local
      t.attachment :image_s3
    end
  end

  #
  # delete images table 
  #
  def down
    drop_table :images
  end
end
