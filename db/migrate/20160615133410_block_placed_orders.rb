class BlockPlacedOrders < ActiveRecord::Migration
  #
  # Migration to block the Orders which are in placed state so that availablity of item can be known
  #
  def up
    order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates::PLACED[:value]
    placed_orders = MarketplaceOrdersModule::V1::Order.where({ status: order_state})
    order_panel_service = MarketplaceOrdersModule::V1::OrderPanelService.new
    placed_orders.each do |order|
      order_panel_service.block_warehouse_brand_packs(order)
    end
  end

  def down

  end
end
