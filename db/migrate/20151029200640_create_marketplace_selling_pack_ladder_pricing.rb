# This migration comes from pricing engine
#
# creating table marketplace_selling_pack_ladder_pricings
#
class CreateMarketplaceSellingPackLadderPricing < ActiveRecord::Migration
	#
	# create table marketplace_selling_pack_ladder_pricings
	# 	* quantity: Quantity of MPSP
	# 	* selling_price: selling price of MPSP
	# 	* margin: Margin on MRP
	# 	* savings: saving on MPSP on that quantity
	#
  def up
    create_table :marketplace_selling_pack_ladder_pricings do |t|
    	t.integer :quantity, default: 0
    	t.decimal :selling_price, default: 0
    	t.decimal :additional_savings, default: 0

    	t.belongs_to :marketplace_selling_pack_pricing

    	t.timestamps null: false
    end

    add_index :marketplace_selling_pack_ladder_pricings, [:marketplace_selling_pack_pricing_id], name: :idx_mpsp_ladder_pricing

  end

  #
  # drop table marketplace_selling_pack_ladder_pricings
  #
  def down
    drop_table :marketplace_selling_pack_ladder_pricings
  end
end
