#
# Migration to add a coloumn to areas table 
#
class AddInitialsToAreas < ActiveRecord::Migration
  #
  # Function to add initials column in areas Table
  #
  def up
    add_column :areas, :initials, :string
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_column :areas, :initials
  end
end
