class CreateUsers < ActiveRecord::Migration
  # 
  # [up description]
  # 
  # create table users
  # 
  # phone_number (string) to store the phone number of user which is unique
  # first_name (string) to store the first name of user
  # last_name (string) to store the last name of user
  # referral_code (string) to store the referral code of user for referring new user
  # referred_by (string) to store the user_id of that user by whom this new user was invited
  # referral_limit (integer) to give a limited amount of invites to a user
  # workflow_state (integer) to store the current state of user
  # 
  def up
    create_table :users do |t|
      t.string :phone_number, unique: true
      t.string :first_name
      t.string :last_name
      t.string :referral_code, index: true, unique: true
      t.string :referred_by
      t.integer :referral_limit, default: 0
      t.integer :workflow_state
      t.timestamps null: false
    end
  end

  # 
  # [down description]
  # 
  # drop table user for rollback
  # 
  def down
    drop_table :users
  end
end
