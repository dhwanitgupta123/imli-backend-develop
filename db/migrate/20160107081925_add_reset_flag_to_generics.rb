#
# Migration to add reset_flag column to Generic Provider table
#
class AddResetFlagToGenerics < ActiveRecord::Migration
  #
  # Function to add reset_flag column to Generic provider
  #
  def up
    add_column :generics, :reset_flag, :boolean, default: false
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_column :generics, :reset_flag
  end

end
