class CreateInventoryAddresses < ActiveRecord::Migration
  #
  # [up description]
  #
  # create table addresses to store the delivery address of the inventory
  #
  # inventory_name (string) to give a name to the address
  # address_line1 (string) store the line1 of address which is compuslory
  # address_line2 (string) stores the line 2 of address
  # landmark (string) to store the nearest landmark of the address
  #
  def change
    create_table :inventory_addresses do |t|
      t.string :inventory_name
      t.string :address_line1
      t.string :address_line2
      t.string :landmark
      
      t.references :inventory, index: true, foreign_key: true
      t.references :area, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # [down description]
  # 
  # drop table address for the rollback
  # 
  def down
      drop_table :inventory_addresses
  end
end
