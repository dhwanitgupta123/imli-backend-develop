#
# Migration to copy ProductSuggesterModel V1 to ProductSuggesterModel V2 table
#
class CopyProductSuggesterV1ModeltoProductSuggesterV2Model < ActiveRecord::Migration

  #
  # Function to copy ProductSuggesterModel V1 to ProductSuggesterModel V2 table
  #
  def up
    product_suggestions_v1 = CommonModule::V1::ProductSuggester.all

    if product_suggestions_v1.count > 0
      product_suggestions_v1.each do |user_wise_product_suggestions|
        user_id = user_wise_product_suggestions.user_id
        product_suggestions_by_user = user_wise_product_suggestions.product_suggestions
        created_at = user_wise_product_suggestions.created_at

        product_suggestions_by_user.each do |product_suggestion|
          create_new_product_suggestion_v2(user_id, product_suggestion, created_at)
        end
      end
    end

  end

  #
  # creates new ProductSuggester V2 object in Mongoid document
  #
  # @param
  # * user_id [Integer] user_id of the user who suggested
  # * product [String] product suggestion by user
  # * created_at [ActiveSupport::TimeWithZone Object]
  #
  # @return [ProductSuggester Model] new ProductSuggester Model
  def create_new_product_suggestion_v2(user_id, product_suggestion, created_at)
    product_suggester_model_v2 = CommonModule::V2::ProductSuggester

    new_suggestion = product_suggester_model_v2.new
    new_suggestion.product_suggestion = product_suggestion
    new_suggestion.user_id = user_id

    # Update the creation time to the user's first product suggestion created in V1
    new_suggestion.created_at = created_at

    new_suggestion.save!
  end

  #
  # Function to roll back the migration
  #
  def down
    CommonModule::V2::ProductSuggester.delete_all
  end
end
