#
# Migration to add a quantity column to memberships table
#
class AddQuantityToMemberships < ActiveRecord::Migration
  #
  # Function to add quantity column in membership Table
  def up
    add_column :memberships, :quantity, :integer
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_column :memberships, :quantity
  end
end
