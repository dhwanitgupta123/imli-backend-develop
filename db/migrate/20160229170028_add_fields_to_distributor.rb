class AddFieldsToDistributor < ActiveRecord::Migration
  #
  # Function to add VRF fields in distributor table
  #
  def up
    add_column :distributors, :bank_account_name, :string
    add_column :distributors, :bank_name, :string
    add_column :distributors, :ifsc_code, :string
    add_column :distributors, :branch_name, :string
    add_column :distributors, :bank_account_number, :string
    add_column :distributors, :contact_person, :string
    add_column :distributors, :contact_number, :string
    add_column :distributors, :email_ids, :string, array: true, default: []
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :distributors, :bank_account_name
    remove_column :distributors, :bank_name
    remove_column :distributors, :ifsc_code
    remove_column :distributors, :branch_name
    remove_column :distributors, :bank_account_number
    remove_column :distributors, :contact_person
    remove_column :distributors, :contact_number
    remove_column :distributors, :email_ids
  end
end
