#
# Migration to add plan_id column to plans table
#
class AddPlanKeyToPlans < ActiveRecord::Migration
  #
  # Function to add pland_id column in plans Table
  #
  def up
    add_column :plans, :plan_key, :integer
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_column :plans, :plan_key
  end
end
