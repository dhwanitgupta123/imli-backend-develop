class AddPdfLinkToInventoryOrder < ActiveRecord::Migration
  #
  # Function to add Pdf Link column to inventory order
  #
  def up
    add_column :inventory_orders, :pdf_link, :string
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :inventory_orders, :pdf_link
  end
end
