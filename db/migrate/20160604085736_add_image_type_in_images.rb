class AddImageTypeInImages < ActiveRecord::Migration
  def up
    add_column :images, :image_type, :integer, default: 0
  end

  def down
    remove_column :images, :image_type
  end
end
