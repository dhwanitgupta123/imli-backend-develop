#
# Migration to change brand pack sku from temp to proper sku
#
class ChangeSkuNameOfBrandPack < ActiveRecord::Migration
  #
  # Function to change brand pack sku from temp to proper sku
  #
  def up
    brand_packs = MasterProductModule::V1::BrandPack.where(sku: 'temp')
    brand_packs.each do |brand_pack|
      brand_pack.sku = brand_pack.product.name + ' ' + brand_pack.sku_size.to_i.to_s + ' ' + brand_pack.unit
      brand_pack.save!
    end
  end
  #
  # Function to roll back the migration
  #
  def down
    # do nothing
  end
end
