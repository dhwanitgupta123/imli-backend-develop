#
# Migration to add checkpoint column to mpsp pricing ladder table
#
class AddCheckpointToLadders < ActiveRecord::Migration
  #
  # Function to add checkpoint column in mpsp pricing ladder Table
  #
  def up
    add_column :marketplace_selling_pack_ladder_pricings, :checkpoint, :boolean, default: false
    mpsp_ladder_pricings = MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricing.all
    if mpsp_ladder_pricings.present?
      mpsp_ladder_pricings.each do |mpsp_ladder_pricing|
        mpsp_ladder_pricing.checkpoint = true
        mpsp_ladder_pricing.save!
      end
    end
  end

  #
  # Function to remove checkpoint column from mpsp ladder pricing Table
  #
  def down
    remove_column :marketplace_selling_pack_ladder_pricings, :checkpoint
  end
end
