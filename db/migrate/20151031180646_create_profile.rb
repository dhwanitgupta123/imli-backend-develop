class CreateProfile < ActiveRecord::Migration
  #
  # create profile table
  # savings: Total savings of a user
  #
  def up
    create_table :profiles do |t|
      t.references :user, index: true, foreign_key: true
      t.decimal :savings, default: 0
      t.string :pincode
      t.timestamps null: false
    end
  end

  #
  # drop table profiles
  #
  def down
    drop_table :profiles
  end
end
