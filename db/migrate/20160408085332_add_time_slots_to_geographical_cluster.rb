class AddTimeSlotsToGeographicalCluster < ActiveRecord::Migration
  def up
    add_reference :time_slots, :geographical_cluster, index: true, foreign_key: true
    add_column :time_slots, :order_limit, :integer, default: 0
    add_column :time_slots, :buffer_limit, :integer, default: 0
    add_column :time_slots, :unavailable_dates, :date, array: true, default: []
    add_column :time_slots, :unavailable_days, :integer, array: true, default: []
  end

  def down
    remove_column :time_slots, :order_limit
    remove_column :time_slots, :buffer_limit
    remove_column :time_slots, :unavailable_dates
    remove_column :time_slots, :unavailable_days
    remove_reference :time_slots, :geographical_cluster
  end
end
