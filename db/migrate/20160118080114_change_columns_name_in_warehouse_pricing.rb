class ChangeColumnsNameInWarehousePricing < ActiveRecord::Migration
  #
  # Migration to change the name of columns
  #
  def up
    rename_column :warehouse_pricings, :spat, :mrp
    rename_column :warehouse_pricings, :margin, :selling_price
    rename_column :warehouse_pricings, :spat_per_unit, :cst
  end

  #
  # Function to rollback the migration
  #
  def down
    rename_column :warehouse_pricings, :mrp, :spat
    rename_column :warehouse_pricings, :selling_price, :margin
    rename_column :warehouse_pricings, :cst, :spat_per_unit
  end
end
