# This migration comes from pricing engine
#
# creating table marketplace_brand_pack_pricings
#
class CreateMarketplaceBrandPackPricing < ActiveRecord::Migration
	#
	# create table marketplace_brand_pack_pricings
	# 	* spat: decimal: Selling price and tax
	# 	* margin: margin on selling price
	# 	* service tax: service tax to be applied
	# 	* vat: vat tax to be applied
	# 	* discount: discount on Marketplace Brand Pack
	#
  def up
    create_table :marketplace_brand_pack_pricings do |t|
    	t.decimal :spat, default: 0
    	t.decimal :margin, default: 0
    	t.decimal :service_tax, default: 0
    	t.decimal :vat, default: 0
    	t.decimal :discount, default: 0
      t.decimal :buying_price, default: 0
      t.decimal :selling_price, default: 0
      t.decimal :mrp, default: 0
      t.decimal :savings, default: 0

    	t.belongs_to :marketplace_brand_pack

    	t.timestamps null: false
    end

    add_index :marketplace_brand_pack_pricings, [:marketplace_brand_pack_id], name: :idx_mpbp_pricing

  end

  #
  # drop table marketplace_brand_pack_pricings
  #
  def down
  	drop_table :marketplace_brand_pack_pricings
  end
end
