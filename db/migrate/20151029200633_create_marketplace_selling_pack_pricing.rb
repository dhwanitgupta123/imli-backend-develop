# This migration comes from pricing engine
#
# creating table marketplace_selling_pack_pricings
#
class CreateMarketplaceSellingPackPricing < ActiveRecord::Migration
	#
	# create table marketplace_selling_pack_pricings
	# 	* service tax: service tax to be applied
	# 	* vat: vat tax to be applied
	# 	* discount: discount on Marketplace Selling Pack
	#
  def up
    create_table :marketplace_selling_pack_pricings do |t|
      t.decimal :mrp, default: 0
      t.decimal :base_selling_price, default: 0
      t.decimal :savings, default: 0
    	t.decimal :vat, default: 0
      t.decimal :service_tax, default: 0
      t.decimal :taxes, default: 0
    	t.decimal :discount, default: 0

    	t.belongs_to :marketplace_selling_pack

    	t.timestamps null: false
    end

    add_index :marketplace_selling_pack_pricings, [:marketplace_selling_pack_id], name: :idx_mpsp_pricing

  end

  #
  # drop table marketplace_selling_pack_pricings
  #
  def down
  	drop_table :marketplace_selling_pack_pricings
  end
end
