#
# Migration to add images column to plans table
#
class AddImagesToPlans < ActiveRecord::Migration
  #
  # Function to add images column in plans Table
  #
  def up
    add_column :plans, :images, :integer, array: true, default: []
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :plans, :images
  end
end
