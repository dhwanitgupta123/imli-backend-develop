class CreateBenefitsPlans < ActiveRecord::Migration
  #
  # up: Create table BenefitsPlans (many-to-many relationship)
  #
  # plan_id [Integer] reference to Plan table
  # benefit_id [Integer] reference to Benefit table
  #
  def up
    create_table :benefits_plans do |t|
      t.belongs_to :plan, index: true
      t.belongs_to :benefit, index: true

      t.timestamps null: false
    end
  end

  #
  # down: Drop table BenefitsPlans
  # Used for db rollback
  #
  def down
    drop_table :benefits_plans
  end
end