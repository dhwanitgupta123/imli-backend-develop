class AddAdditionalDiscountToOrderProduct < ActiveRecord::Migration
  #
  # Function to add additional_discount to OrderProduct
  #
  def up
    add_column :order_products, :additional_discount, :decimal, default: BigDecimal.new('0')
    add_column :order_products, :final_price, :decimal, default: BigDecimal.new('0')

    all_order_products = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProduct.all
    all_order_products.each do |order_product|
      order_product.final_price = order_product.quantity * order_product.selling_price
      order_product.save!
    end
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :order_products, :additional_discount
    remove_column :order_products, :final_price
  end
end
