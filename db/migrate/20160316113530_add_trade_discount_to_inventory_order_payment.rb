class AddTradeDiscountToInventoryOrderPayment < ActiveRecord::Migration
  #
  # Migration to add trade discount to inventory order payment table
  #
  def up
    add_column :inventory_order_payments, :trade_discount, :decimal, default: 0.0
    add_column :inventory_order_payments, :trade_discount_value, :decimal, default: 0.0
  end

  #
  # Migration to rollback the migration
  #
  def down
    remove_column :inventory_order_payments, :trade_discount
    remove_column :inventory_order_payments, :trade_discount_value
  end
end
