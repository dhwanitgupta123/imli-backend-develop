class CreateGenerics < ActiveRecord::Migration
  # 
  # [up description]
  # 
  # password (string) to store secure password for the generic login
  # 
  # 
  def up
    create_table :generics do |t|
      t.string :password
      t.references :provider, index: true, foreign_key: true
      t.timestamps null: false
    end
  end

  def down
    drop_table :generics
  end
end

