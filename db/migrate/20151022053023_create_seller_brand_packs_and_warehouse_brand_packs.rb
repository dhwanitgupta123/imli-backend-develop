
# This migration comes from supply_chain_engine_engine (originally 20151007135411)
#
# create table through table for SBP & WBP
#
class CreateSellerBrandPacksAndWarehouseBrandPacks < ActiveRecord::Migration
  #
  # create through table for WBP & SBP
  #
  def up
    create_table :seller_brand_packs_warehouse_brand_packs, id: false do |t|
      t.belongs_to :warehouse_brand_pack
      t.belongs_to :seller_brand_pack
    end
    add_index :seller_brand_packs_warehouse_brand_packs, [:warehouse_brand_pack_id], name: :idx_through_on_wbp_sbp
    add_index :seller_brand_packs_warehouse_brand_packs, [:seller_brand_pack_id], name: :idx_through_on_sbp_wbp
  end

  #
  # delete through table
  #
  def down
    drop_table :seller_brand_packs_warehouse_brand_packs
  end
end
