#
# This migration add billing amount column in Payments table
#
class AddDeliveryChargesToPayments < ActiveRecord::Migration
  def up
    add_column :payments, :delivery_charges, :decimal
  end

  def down
    remove_column :payments, :delivery_charges
  end
end
