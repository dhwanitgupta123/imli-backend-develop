class CreateCity < ActiveRecord::Migration
  # 
  # create table cities
  # 
  # name: city name
  # state_id: reference to the state table
  # 
  def up
    create_table :cities do |t|

      t.string :name
      
      t.references :state, index: true, foreign_key: true
      
      t.timestamps null: false
    end
  end

  # 
  # deletes the table cities
  # 
  def down
    drop_table :cities
  end
end
