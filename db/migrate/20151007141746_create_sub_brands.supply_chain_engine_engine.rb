# This migration comes from supply_chain_engine_engine (originally 20151007135010)
#
# create table sub_brands
#
class CreateSubBrands < ActiveRecord::Migration
  #
  # sub_brands table
  #
  # name: sub_brand name
  # logo_url: logo_url string
  # brand_id: brand id
  #
  def up
    create_table :sub_brands do |t|
      t.string :name
      t.string :logo_url
      t.string :initials
      t.integer :status

      t.timestamps null: false

      t.references :brand, index: true, foreign_key: true
    end
  end

  #
  # delete sub_brands
  #
  def down
    drop_table :sub_brands
  end
end
