#
# Migration to add status column to addresses table
#
class AddStatusToAddress < ActiveRecord::Migration
  #
  # Function to add status column to addresses table
  #
  def up
    add_column :addresses, :status, :integer
    addresses = AddressModule::V1::Address.all

    # Move all existing addresses to ACTIVE state
    addresses.each do |address|
      address.status = AddressModule::V1::ModelStates::V1::AddressStates::ACTIVE
      address.save!
    end
  end

  #
  # Function to remove status column from addresses Table
  #
  def down
    remove_column :addresses, :status
  end
end
