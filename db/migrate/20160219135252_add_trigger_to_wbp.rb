class AddTriggerToWbp < ActiveRecord::Migration
  #
  # Function to add threshold_stock_value, outbound_frequency_period_in_days in wbps table
  #
  def up
    add_column :warehouse_brand_packs, :threshold_stock_value, :integer, default: 0
    add_column :warehouse_brand_packs, :outbound_frequency_period_in_days, :integer, default: 0
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :warehouse_brand_packs, :threshold_stock_value
    remove_column :warehouse_brand_packs, :outbound_frequency_period_in_days
  end
end
