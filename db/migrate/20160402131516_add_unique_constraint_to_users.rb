class AddUniqueConstraintToUsers < ActiveRecord::Migration
  def up
    add_index :users, :phone_number, unique: true
    remove_index :users, :referral_code
    add_index :users, :referral_code, unique: true
  end

  def down
    remove_index :users, :phone_number
  end
end
