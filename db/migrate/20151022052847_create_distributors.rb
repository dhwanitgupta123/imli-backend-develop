class CreateDistributors < ActiveRecord::Migration

  #
  # create distributors table
  #
  # name: name of distributor
  #
  def up
    create_table :distributors do |t|
      t.string :name
      t.integer :status

      t.timestamps null: false
    end
  end

  #
  # delete distributors table
  #
  def down
    drop_table :distributors
  end
end
