class AddMinAndMaxOffsetInTimeSlots < ActiveRecord::Migration
  def up
    add_column :time_slots, :min_slot_offset, :integer
    add_column :time_slots, :max_slot_offset, :integer
  end

  def down
    remove_column :time_slots, :min_slot_offset
    remove_column :time_slots, :max_slot_offset
  end
end
