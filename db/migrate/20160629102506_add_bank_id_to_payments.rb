class AddBankIdToPayments < ActiveRecord::Migration
  def up
    add_column :payments, :bank_id, :integer, default: nil
  end

  def down
    remove_column :payments, :bank_id
  end
end
