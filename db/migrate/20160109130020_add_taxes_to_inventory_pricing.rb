class AddTaxesToInventoryPricing < ActiveRecord::Migration
  #
  # Function to add CST, Octrai tax column to IBP
  #
  def up
    add_column :inventory_pricings, :cst, :decimal, default: 0.0
    add_column :inventory_pricings, :octrai, :decimal, default: 0.0
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :inventory_pricings, :cst
    remove_column :inventory_pricings, :octrai
  end
end
