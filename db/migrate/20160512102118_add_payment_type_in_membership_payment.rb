#
# Migration to add payment_type column in Membership Payment table
#
class AddPaymentTypeInMembershipPayment < ActiveRecord::Migration
  #
  # Function to add payment_type column to Membership Payment table
  #
  def up
    add_column :membership_payments, :payment_type, :integer, default: 0
    set_payment_type_to_pay
  end

  def set_payment_type_to_pay
    payments = UsersModule::V1::MembershipModule::V1::MembershipPayment.all
    payments.each do |payment|
      payment.payment_type = PaymentModule::V1::PaymentType.get_id_by_key('PAY')
      payment.status = UsersModule::V1::ModelStates::MembershipPaymentEvents::PAYMENT_SUCCESS
      payment.save!
    end
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :membership_payments, :payment_type
  end
end
