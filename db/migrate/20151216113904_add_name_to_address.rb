#
# Migration to add name column to addresses table
#
class AddNameToAddress < ActiveRecord::Migration
  #
  # Function to add name column to addresses table
  #
  def up
    add_column :addresses, :recipient_name, :string
    addresses = AddressModule::V1::Address.all
    if addresses.present?
      addresses.each do |address|
        address.recipient_name = address.user.first_name + ' ' + (address.user.last_name || '')
        address.save!
      end
    end
  end

  #
  # Function to remove name column from addresses Table
  #
  def down
    remove_column :addresses, :recipient_name
  end
end
