class CreateClustersMarketplaceSellingPacks < ActiveRecord::Migration
  # 
  # up: create table clusters_marketplace_selling_packs
  #
  # marketplace_selling_pack: [reference] reference to marketplace_selling_packs table
  # cluster: [reference] reference to clusters table
  #
  def up
    create_table :clusters_marketplace_selling_packs do |t|
      t.belongs_to :cluster, index: true
      t.belongs_to :marketplace_selling_pack, index: {:name => 'mpsp'}
    end
  end

  #
  # drop clusters_marketplace_selling_packs table
  #
  def down
    drop_table :clusters_marketplace_selling_packs
  end
end
