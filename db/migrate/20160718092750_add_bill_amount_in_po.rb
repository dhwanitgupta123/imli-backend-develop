class AddBillAmountInPo < ActiveRecord::Migration
   def up
    add_column :inventory_order_payments, :bill_amount, :decimal
  end

  def down
    remove_column :inventory_order_payments, :bill_amount
  end
end
