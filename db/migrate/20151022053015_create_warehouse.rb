# This migration comes from supply_chain_engine_engine (originally 20151007135411)
#
# create table warehouse
#
class CreateWarehouse < ActiveRecord::Migration
  #
  # create table warehouses
  #
  # name: name of warehouse
  # status: status of warehouse
  # cash_and_carry: cash and carry has many warehouses
  #
  def up
    create_table :warehouses do |t|
      t.string :name
      t.integer :status
      t.references :cash_and_carry, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # delete warehouses table
  # 
  def down
    drop_table :warehouses
  end
end


