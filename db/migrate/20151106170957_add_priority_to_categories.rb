#
# Class to add priority column to categories table
#
class AddPriorityToCategories < ActiveRecord::Migration
  # 
  # Function to add priority column to categories
  #
  def up
    add_column :categories, :priority, :integer
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :categories, :priority
  end
end
