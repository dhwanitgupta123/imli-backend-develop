class AddAmpleCreditAmountToPayments < ActiveRecord::Migration
  #
  # Adding apply_ample_credit field in payments
  # 
  def up
    add_column :payments, :ample_credit_amount, :decimal, default: 0
  end

  #
  # removing field apply_ample_credit
  # 
  def down
    remove_column :payments, :ample_credit_amount
  end
end
