class AddCstTinToDistributor < ActiveRecord::Migration
  #
  # Function to add CST, Tin No column to Distributor
  #
  def up
    add_column :distributors, :cst_no, :string
    add_column :distributors, :tin_no, :string
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :distributors, :cst_no
    remove_column :distributors, :tin_no
  end
end
