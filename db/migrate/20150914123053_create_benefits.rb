class CreateBenefits < ActiveRecord::Migration
  #
  # up: Create table benefits
  #
  # name [String] to store the benefit name/identifier name (ex: EXTRA_DISCOUNT )
  # details [String] to store other details/description
  # condition [String] to store conditions for which reward is to be given
  # reward_value [String] to store reward amount (ex: 25, 1000)
  # reward_type [String] to store reward type (ex: INR, PERCENTAGE)
  # status [Integer] to store status of the benefit (ex: Active, Inactive, etc.)
  #
  def up
    create_table :benefits do |t|
      t.string :name
      t.text :details
      t.string :condition
      t.string :reward_value
      t.string :reward_type
      t.integer :status

      t.timestamps null: false
    end
  end

  #
  # down: Drop table Benefits
  # Used for db rollback
  #
  def down
    drop_table :benefits
  end
end
