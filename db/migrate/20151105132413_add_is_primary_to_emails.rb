#
# Migration to add a column to emails table
#
class AddIsPrimaryToEmails < ActiveRecord::Migration
  #
  # Function to add is primary column in emails Table
  def up
    add_column :emails, :is_primary, :boolean, default: false
  end

  #
  # Function to roll back the migration
  #
  def down
    remove_column :emails, :is_primary
  end
end
