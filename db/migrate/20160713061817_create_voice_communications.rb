class CreateVoiceCommunications < ActiveRecord::Migration

  #
  # create table voice_communications to store ample voice communication details
  #
  # sid (string) unique call sid
  # from_phone_number (string) phone number from where the communication took place
  # to_phone_number (string) phone number to which the communication took place
  # status (integer) stores the enum value of status of call
  # recording_url (string) url containing the media file of the recorded conversation
  # call_direction (integer) stores the enum value of call type(inbound, outbound, etc)
  # start_time (string) start time of call
  # end_time (string) end time of call
  # duration (string) duration of call
  #
  #
  def up
    create_table :voice_communications do |t|
      t.string  :sid
      t.string  :from_phone_number
      t.string  :to_phone_number
      t.integer :status
      t.string  :recording_url
      t.integer :call_direction
      t.string  :start_time
      t.string  :end_time
      t.string  :duration

      t.timestamps null: false
    end
  end

  #
  # drop table voice_communications for the rollback
  #
  def down
      drop_table :voice_communications
  end
end
