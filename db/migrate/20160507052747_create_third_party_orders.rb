class CreateThirdPartyOrders < ActiveRecord::Migration

  #
  # up: creating third party orders
  #
  # label: String title of third_party_orders
  # reference_id: reference order id of the third party service object
  # reference_type: type of the third party service
  # amount: persisted amount of the third_party_order
  # status: workflow state of the order
  # ample_order_id: linking of third_party_order with the Ample marketplace order
  #
  def up
    create_table :third_party_orders do |t|
      t.string :label
      t.integer :reference_id
      t.integer :reference_type
      t.decimal :amount, default: BigDecimal.new('0')
      t.integer :status
      t.references :order, index: true

      # To-Do : change reference name to ample_order_id
      #t.references :ample_order_id, references: :orders, index: true
    end
  end


  #
  # down: Drop third_party_orders table
  #
  def down
    drop_table :third_party_orders
  end
end
