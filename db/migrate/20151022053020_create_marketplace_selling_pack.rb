# This migration comes from supply_chain_engine_engine
#
# creating table marketplace_selling_packs
#
class CreateMarketplaceSellingPack < ActiveRecord::Migration
	#
  # create table marketplace_selling_packs
  #
  # status: status of marketplace_selling_packs
  # selling price: selling price of product
  # mrp: mrp of product
  # savings: savings on product
  # discount: discount amount
  # is_on_sale: if product is on sale
  # is_ladded_pricing_active: if ladder pricing is active on the product
  #
  def up
    create_table :marketplace_selling_packs do |t|

      t.integer :max_quantity, default: 0
    	t.boolean :is_on_sale, :default  => false
    	t.boolean :is_ladder_pricing_active, :default  => false
    	t.integer :status
      t.string :display_name
      t.string :display_pack_size
      t.string :primary_tags
      t.string :secondary_tags
      t.text :description

    	t.timestamps null: false

    end
  end

  #
  # delete marketplace_selling_packs table
  #
  def down
    drop_table :marketplace_selling_packs
  end
end

