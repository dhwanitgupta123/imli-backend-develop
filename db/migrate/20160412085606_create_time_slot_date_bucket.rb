class CreateTimeSlotDateBucket < ActiveRecord::Migration
  def up
    create_table :time_slot_date_buckets do |t|
      t.date :slot_date
      t.references :time_slot
      t.integer :order_count, default: 0
      t.integer :buffer_count, default: 0

      t.timestamps null: false
    end

    add_index :time_slot_date_buckets, [:slot_date, :time_slot_id], unique: true
  end

  def down
    drop_table :time_slot_date_buckets
  end
end
