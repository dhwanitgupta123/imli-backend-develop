# This migration comes from order_engine
#
# create table carts
#
class CreateCarts < ActiveRecord::Migration
  #
  # create carts
  #
  # cart_total: total value of the cart
  # cart_savings: total savings on the cart
  # user_cart_status: status if it's an active/inactive cart.
  #                   User can have only ONE active cart
  # 
  def up
    create_table :carts do |t|
      t.integer :user_cart_status
      t.decimal :total_mrp, default: 0 
      t.decimal :cart_savings, default: 0
      t.decimal :cart_total, default: 0

      t.references :user, index: true, foreign_key: true
      t.references :order, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # delete carts table
  # 
  def down
    drop_table :carts
  end
end