class CreateCategories < ActiveRecord::Migration
  # 
  # up: create tables categories
  # 
  # label: [string] used to store name of the category
  # image_url: [string] contains S3 url of the image for the category
  # department: [references] category belongs to the department
  # parent: [reference] category has sub category, denoted by children
  # status: [integer] to store the current state of category
  # 
  def up
    create_table :categories do |t|
      t.string :label
      t.string :image_url
      t.references :department, index: true, foreign_key: true
      t.references :parent_category, index: true
      t.integer :status
      t.string :description

      t.timestamps null: false
    end
  end

  #
  # down: Drop table Category
  # Used for db rollback
  #
  def down
    drop_table :categories
  end
end
