# This migration comes from pricing engine
#
# creating table inventory_pricing
#
class CreateInventoryPricing < ActiveRecord::Migration
	#
	# create table inventory_pricings
	# 	* spat: decimal: Selling price and tax
	# 	* margin: margin on selling price
	# 	* service tax: service tax to be applied
	# 	* vat: vat tax to be applied
	# 	* spat_per_unit: SPAT per unit pack size
	#
  def up
    create_table :inventory_pricings do |t|
    	t.decimal :spat, default: 0
    	t.decimal :margin, default: 0
    	t.decimal :service_tax, default: 0
    	t.decimal :vat, default: 0
    	t.decimal :spat_per_unit, default: 0

    	t.belongs_to :inventory_brand_pack, index: true, foreign_key: true

    	t.timestamps null: false
    end
  end

  #
  # drop table inventory_pricings
  #
  def down
  	drop_table :inventory_pricings
  end
end
