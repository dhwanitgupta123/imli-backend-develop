#
# Class to create a DB table of warehouse stocks
#
class CreateWarehouseStock < ActiveRecord::Migration
  #
  # Migration function to create warehouse stocks table
  # * quantity: [integer]
  # * date_of_purchase: [integer] epoch time of purchase
  # * status [:integer] status of that much quantity of stock
  #
  def up
    create_table :warehouse_stocks do |t|
      t.integer :quantity
      t.integer :date_of_purchase
      t.integer :status

      t.references :warehouse_pricing, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  #
  # Function to rollback the migration
  #
  def down
    drop_table :warehouse_stocks
  end
end
