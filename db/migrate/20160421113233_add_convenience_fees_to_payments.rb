class AddConvenienceFeesToPayments < ActiveRecord::Migration
  #
  # Migration to add convenience charges to order payment table
  #
  def up
    add_column :payments, :convenience_charges, :decimal, default: BigDecimal.new('0')
  end

  #
  # Migration to rollback the changes above
  #
  def down
    remove_column :payments, :convenience_charges
  end
end
