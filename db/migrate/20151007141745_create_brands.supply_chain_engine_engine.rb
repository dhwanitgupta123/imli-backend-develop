# This migration comes from supply_chain_engine_engine (originally 20151007134944)
#
# create table brands
#
class CreateBrands < ActiveRecord::Migration
  #
  # brands table
  #
  # name: brand name
  # code: brand code
  # logo_url: brand logo url
  #
  def up
    create_table :brands do |t|
      t.string :name
      t.string :code
      t.string :logo_url
      t.string :initials
      t.integer :status
      t.timestamps null: false

      t.references :company, index: true, foreign_key: true
    end
  end

  #
  # delete brands table
  #
  def down
    drop_table :brands
  end
end
