class CreateTimeSlots < ActiveRecord::Migration
  #
  # up: create tables time slots
  #
  # from_time: [string] slot start time
  # to_time: [time] slot end time
  # label: [string] display text of that slot
  # status: [integer] active/inactive status of slot
  # 
  def up
    create_table :time_slots do |t|
      t.time :from_time
      t.time :to_time
      t.string :label
      t.integer :status

      t.timestamps null: false
    end
  end

  #
  # down: Drop table Time slots
  # Used for db rollback
  #
  def down
    drop_table :time_slots
  end
end

