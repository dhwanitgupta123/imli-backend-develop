
# This migration comes from supply_chain_engine_engine (originally 20151007135411)
#
# create table through table for IBP & WBP
#
class CreateWarehouseBrandPacksAndInventoryBrandPacks < ActiveRecord::Migration
  #
  # create through table for IBP & WBP
  #
  def up
    create_table :inventory_brand_packs_warehouse_brand_packs, id: false do |t|
      t.belongs_to :warehouse_brand_pack
      t.belongs_to :inventory_brand_pack
    end
    add_index :inventory_brand_packs_warehouse_brand_packs, [:warehouse_brand_pack_id], name: :idx_through_on_wbp
    add_index :inventory_brand_packs_warehouse_brand_packs, [:inventory_brand_pack_id], name: :idx_through_on_ibp
  end

  # 
  # delete through table
  # 
  def down
    drop_table :inventory_brand_packs_warehouse_brand_packs
  end
end
