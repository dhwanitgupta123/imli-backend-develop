class CreateArea < ActiveRecord::Migration

  # 
  # create table areas 
  # 
  # name: area name
  # pincode: 6 digit code
  # is_active: boolean that signifies if we are active or not in the given pincode
  # 
  def up
    create_table :areas do |t|
      
      t.string :name
      t.string :pincode,  unique: true

      t.integer :status

      t.references :city, index: true, foreign_key: true
    end
  end

  # 
  # [down description]
  # 
  # drop table areas for the rollback
  # 
  def down
      drop_table :areas
  end
end
