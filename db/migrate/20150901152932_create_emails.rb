class CreateEmails < ActiveRecord::Migration
  # 
  # [up description]
  # 
  # Create table emails
  # 
  # email_id (string) to store the email id of the user
  # account_type (integer) to store the type of user is viz: employee, customer, vendor
  # authentication_token (string) to generate authentication url
  # verified (boolean) to ensure if email is verified
  # 
  def up
    create_table :emails do |t|
      t.string :email_id
      t.integer :account_type
      t.string :authentication_token
      t.boolean :verified
      t.timestamp :expires_at
      t.timestamps null: false
      t.references :user, index: true, foreign_key: true
    end
  end

  # 
  # [down description]
  # 
  # drop table emails for the rollback
  # 
  def down 
    drop_table :emails
  end
end
