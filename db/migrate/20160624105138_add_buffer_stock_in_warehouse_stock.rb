class AddBufferStockInWarehouseStock < ActiveRecord::Migration
  def up
    add_column :warehouse_brand_packs, :buffer_stock, :integer, default: 0
  end

  def down
    remove_column :warehouse_brand_packs, :buffer_stock
  end
end
