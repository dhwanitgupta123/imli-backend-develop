# This migration comes from order_engine
#
# create table warehouse_orders
#
class CreateWarehouseOrders < ActiveRecord::Migration
  #
  # create table warehouse_orders
  #
  # ship_by: ship_by date
  # status: status of order
  #
  def up
    create_table :warehouse_orders do |t|
      t.string :warhouse_order_id
      t.string :ship_by
      t.string :status

      t.references :order, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # delete warehouse_orders table
  # 
  def down
    drop_table :warehouse_orders
  end
end