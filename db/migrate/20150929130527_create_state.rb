class CreateState < ActiveRecord::Migration
  # 
  # create table states
  # 
  # name: state name
  # country_id: reference to the country
  # 
  def up
    create_table :states do |t|
      t.string :name

      t.references :country, index: true, foreign_key: true
      
      t.timestamps null: false
    end
  end

  # 
  # deletes states table
  # 
  def down
    drop_table :states
  end
end
