class AddFieldsToBenefits < ActiveRecord::Migration
  def up
    add_column :benefits, :refer_limit, :integer
    add_column :benefits, :benefit_limit, :integer
    add_column :benefits, :benefit_to_user, :integer
    add_column :benefits, :benefit_to_referrer, :integer
    add_column :benefits, :trigger_point_of_user, :integer
    add_column :benefits, :trigger_point_of_referrer, :integer
    add_column :benefits, :is_global, :boolean
    add_column :benefits, :refer_count, :integer, default: 0
    add_column :benefits, :benefit_count, :integer, default: 0
    add_column :benefits, :benefit_type, :integer, default: 0

    add_reference :benefits, :benefit_of, polymorphic: true, index: true

    add_initial_benefit_to_users
  end

  def add_initial_benefit_to_users
    benefit_service = CommonModule::V1::BenefitService.new({})
    users = UsersModule::V1::User.all
    users.each do |user|
      benefit_service.add_initial_benefits_to_user(user)
    end
  end

  def down
    remove_column :benefits, :refer_limit
    remove_column :benefits, :benefit_limit
    remove_column :benefits, :benefit_to_referrer
    remove_column :benefits, :benefit_to_user
    remove_column :benefits, :trigger_point_of_referrer
    remove_column :benefits, :trigger_point_of_user
    remove_column :benefits, :is_global
    remove_column :benefits, :benefit_count
    remove_column :benefits, :benefit_type
    remove_column :benefits, :refer_count
    remove_column :benefits, :benefit_of_type
    
    remove_reference :benefits, :benefit_of

  end
end
