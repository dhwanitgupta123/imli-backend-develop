#
# Migration to add ample_order_payment_id column in membership payments table
#
class AddOrderPaymentInMembershipPayment < ActiveRecord::Migration
  #
  # Function to add ample order payment id in membership payment for ref.
  #
  def up
    add_column :membership_payments, :ample_order_payment_id, :integer
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :membership_payments, :ample_order_payment_id
  end
end
