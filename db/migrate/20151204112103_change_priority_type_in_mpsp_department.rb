class ChangePriorityTypeInMpspDepartment < ActiveRecord::Migration
  def up
    change_column :mpsp_departments, :priority, 'integer USING CAST(priority AS integer)'
  end

  def down
    change_column :mpsp_departments, :priority, :string
  end
end
