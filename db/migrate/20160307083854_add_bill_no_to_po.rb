class AddBillNoToPo < ActiveRecord::Migration
  #
  # Function to add Bill_no fields in inventory_order table
  #
  def up
    add_column :inventory_orders, :bill_no, :string
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :inventory_orders, :bill_no
  end
end
