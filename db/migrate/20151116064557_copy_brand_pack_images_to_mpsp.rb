#
# Migration to add images column to marketplace selling pack table
#
class CopyBrandPackImagesToMpsp < ActiveRecord::Migration
  #
  # Function to add images column in marketplace selling pack Table
  #
  def up
    mpsps = MarketplaceProductModule::V1::MarketplaceSellingPack.all
    if mpsps.present?
      mpsps.each do |mpsp|
        mpsp.images = mpsp.marketplace_brand_packs[0].brand_pack.images
        mpsp.save
      end
    end
  end

  def down
    mpsps = MarketplaceProductModule::V1::MarketplaceSellingPack.all
    if mpsps.present?
      mpsps.each do |mpsp|
        mpsp.images = nil
        mpsp.save
      end
    end
  end
end