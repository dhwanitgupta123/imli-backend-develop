class ChangePriorityTypeInMpspCategory < ActiveRecord::Migration
  def up
    change_column :mpsp_categories, :priority, 'integer USING CAST(priority AS integer)'
  end

  def down
    change_column :mpsp_categories, :priority, :string
  end
end
