# This migration comes from supply_chain_engine_engine (originally 20151007135411)
#
# create table warehouse_brand_packs
#
class CreateWarehouseBrandPack < ActiveRecord::Migration
  #
  # create table warehouses
  #
  # status: status of WBP
  # warehouse: warehouse has many WBP
  # Inventory brand pack: WBP has many IBP
  #
  def up
    create_table :warehouse_brand_packs do |t|
      t.integer :status
      t.references :warehouse, index: true, foreign_key: true
      t.references :brand_pack, index: true, foreign_key: true
      t.timestamps null: false
    end
  end

  # 
  # delete warehouse_brand_packs table
  # 
  def down
    drop_table :warehouse_brand_packs
  end
end


