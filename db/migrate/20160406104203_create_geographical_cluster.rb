class CreateGeographicalCluster < ActiveRecord::Migration
  #
  # up: creating geographical_clusters table
  #
  # label: String title of geographical_clusters
  # parent_cluster: parent geographical_cluster of the current cluster
  # level: level of geographical_cluster
  #
  def up
    create_table :geographical_clusters do |t|
      t.string :label
      t.integer :level

      t.references :parent_cluster, index: true
    end

    add_reference :areas, :geographical_cluster

    initialize_leaf_clusters
  end

  def down
    drop_table :geographical_clusters
    remove_reference :areas, :geographical_cluster
  end

  def initialize_leaf_clusters
    areas = AddressModule::V1::Area.all
    return if areas.blank?

    areas.each do |area|
      geographical_cluster = GeographicalClustersModule::V1::GeographicalCluster.new
      geographical_cluster.area = area
      geographical_cluster.level = 0
      geographical_cluster.label = area.pincode.to_s
      geographical_cluster.save!
    end
  end
end
