# This migration comes from pricing engine
#
# creating table warehouse_pricings
#
class CreateWarehousePricing < ActiveRecord::Migration
	#
	# create table warehouse_pricings
	# 	* spat: decimal: Selling price and tax
	# 	* margin: margin on selling price
	# 	* service tax: service tax to be applied
	# 	* vat: vat tax to be 
	# 	* spat_per_unit: SPAT per unit pack size
	#
  def up
    create_table :warehouse_pricings do |t|
    	t.decimal :spat, default: 0
    	t.decimal :margin, default: 0
    	t.decimal :service_tax, default: 0
    	t.decimal :vat, default: 0
    	t.decimal :spat_per_unit, default: 0

    	t.belongs_to :warehouse_brand_pack, index: true, foreign_key: true

    	t.timestamps null: false
    end
  end

  #
  # drop table warehouse_pricings
  #
  def down
  	drop_table :warehouse_pricings
  end
end
