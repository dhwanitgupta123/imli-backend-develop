#
# Migration to add benefit_key column to benefits table
#
class AddBenefitKeyToBenefits < ActiveRecord::Migration
  #
  # Function to add benefit_key column in benefits Table
  #
  def up
    add_column :benefits, :benefit_key, :integer
  end

  #
  # Function to remove benefit_key column from benefits Table
  #
  def down
    remove_column :benefits, :benefit_key
  end
end
