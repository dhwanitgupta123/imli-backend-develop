class AddFromTimeSecondsAndToTimeSecondsInTimeSlots < ActiveRecord::Migration
  def up
    add_column :time_slots, :from_time_in_seconds, :integer, default: 0
    add_column :time_slots, :to_time_in_seconds, :integer, default: 0

    time_slots = MarketplaceOrdersModule::V1::TimeSlot.all

    if time_slots.present?
      time_slots.each do |time_slot|
        time_slot.from_time_in_seconds = CommonModule::V1::DateTimeUtil.
                                            convert_time_to_seconds(time_slot.from_time.in_time_zone) if time_slot.from_time.present?
        time_slot.to_time_in_seconds = CommonModule::V1::DateTimeUtil.
                                            convert_time_to_seconds(time_slot.to_time.in_time_zone) if time_slot.to_time.present?
        time_slot.save
      end
    end
  end

  def down
    remove_column :time_slots, :from_time_in_seconds, :integer
    remove_column :time_slots, :to_time_in_seconds, :integer
  end
end
