# This migration comes from supply_chain_engine_engine (originally 20151007134524)
#
# create table companies
#
class CreateCompanies < ActiveRecord::Migration
  #
  # table companies
  #  
  # name: company name
  # logo_url: company logo url
  # vat_number: vat number
  # tin_number: tin number
  # cst: cst
  # account_details: account details
  # trade_promotion_margin: promotion margin
  # data_sharing_margin: sharing margin
  # incentives: incentives
  # reconciliation_period: time period
  # legal_document_url: legal document link
  # 
  def up
    create_table :companies do |t|
      t.string :name
      t.string :logo_url
      t.string :vat_number
      t.string :tin_number
      t.string :cst
      t.string :account_details
      t.string :trade_promotion_margin
      t.string :data_sharing_margin
      t.string :incentives
      t.string :legal_document_url
      t.string :initials
      t.integer :status

      t.timestamp :reconciliation_period
      t.timestamps null: false
    end
  end

  # 
  # delete table companies
  # 
  def down
    drop_table :companies
  end
end