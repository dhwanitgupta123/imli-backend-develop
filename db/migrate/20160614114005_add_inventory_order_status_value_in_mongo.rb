class AddInventoryOrderStatusValueInMongo < ActiveRecord::Migration
  #
  # Migration to add order status value along with the label
  #
  def up
    inventory_order_state_attributes = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderStates.all
    return if inventory_order_state_attributes.blank?
    inventory_order_state_attributes.each do |inventory_order_state_attribute|
      order_status = inventory_order_state_attribute.order_status
      state = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderStates.get_order_state_by_label(order_status)
      next if state.blank?
      inventory_order_state_attribute.order_status_value = state[:value]
      inventory_order_state_attribute.save!
    end
  end

  def down
    
  end
end
