class AddDpCodeToIbp < ActiveRecord::Migration
  #
  # Function to add distributor_pack_code in inventory_brand_packs table
  #
  def up
    add_column :inventory_brand_packs, :distributor_pack_code, :string
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :inventory_brand_packs, :distributor_pack_code
  end
end
