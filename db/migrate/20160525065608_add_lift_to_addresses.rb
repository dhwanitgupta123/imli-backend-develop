class AddLiftToAddresses < ActiveRecord::Migration
  def up
    add_column :addresses, :has_lift, :boolean
  end

  def down
    remove_column :addresses, :has_lift
  end
end
