# This migration comes from supply_chain_engine_engine (originally 20151007135411)
#
# create table brand_packs
#
class CreateBrandPacks < ActiveRecord::Migration
  #
  # create table brand_packs
  #
  # photo: brand_packs photo url
  # sku: sku
  # mrp: maximum retail prize
  # sku_size: quantity
  # unit: sku units
  # description: sku discription
  # shelf_life: shelf life
  #
  def up
    create_table :brand_packs do |t|
      t.integer :images, array: true, default: []
      t.string :sku
      t.float :mrp, default: 0
      t.string :sku_size
      t.string :unit
      t.string :description
      t.string :shelf_life
      t.string :brand_pack_code
      t.integer :status
      t.string :article_code
      t.string :retailer_pack

      t.references :product, index: true, foreign_key: true
      t.references :sub_category, index: true

      t.timestamps null: false
    end
  end

  #
  # delete brand_packs table
  #
  def down
    drop_table :brand_packs
  end
end
