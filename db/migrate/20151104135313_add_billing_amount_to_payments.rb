#
# This migration add billing amount column in Payments table
#
class AddBillingAmountToPayments < ActiveRecord::Migration
  def up
    add_column :payments, :billing_amount, :decimal
  end

  def down
    remove_column :payments, :billing_amount
  end
end
