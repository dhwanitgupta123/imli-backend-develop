class AddSchemeDiscountToInventoryOrderProduct < ActiveRecord::Migration
  #
  # Migration to add scheme discount to inventory order product table
  #
  def up
    add_column :inventory_order_products, :scheme_discount, :decimal, default: 0.0
    add_column :inventory_order_products, :scheme_discount_value, :decimal, default: 0.0
  end

  #
  # Migration to rollback the migration
  #
  def down
    remove_column :inventory_order_products, :scheme_discount
    remove_column :inventory_order_products, :scheme_discount_value
    remove_column :inventory_order_products, :net_total
  end
end
