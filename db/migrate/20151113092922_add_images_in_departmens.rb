#
# Migration to add images column to departments table
#
class AddImagesInDepartmens < ActiveRecord::Migration
  #
  # Function to add images column in departments Table
  #
  def up
    add_column :departments, :images, :integer, array: true, default: []
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :departments, :images
  end
end
