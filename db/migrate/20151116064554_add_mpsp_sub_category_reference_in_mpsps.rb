class AddMpspSubCategoryReferenceInMpsps < ActiveRecord::Migration
  def up
    add_reference :marketplace_selling_packs, :mpsp_sub_category, index: true
  end

  def down
    remove_reference :marketplace_selling_packs, :mpsp_sub_category
  end
end