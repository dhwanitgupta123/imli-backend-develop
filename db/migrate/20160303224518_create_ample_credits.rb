class CreateAmpleCredits < ActiveRecord::Migration
  def up
    create_table :ample_credits do |t|

      t.decimal :balance, default: 0
      t.decimal :max_limit, default: 0
      t.decimal :min_limit, default: 0
      t.decimal :conversion, default: 1
      t.string :currency, default: 'INR'

      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end

    users = UsersModule::V1::User.all
    ample_credit = CreditsModule::V1::AmpleCredit
    users.each do |user|
      new_ample_credit = ample_credit.new(user_id: user.id)
      new_ample_credit.save!
    end
  end

  def down
    drop_table :ample_credits
  end
end
