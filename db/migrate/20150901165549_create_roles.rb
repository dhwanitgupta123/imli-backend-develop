class CreateRoles < ActiveRecord::Migration
  #
  # up: Create table Roles
  #
  # role_name [String] to store the role label
  # permissions [Array(Integer)] to store the permissions accosiated with that role
  # parent_id [Integer] to store its parent role id (tree structure), Nil in case of root
  #
  def up
    create_table :roles do |t|
      t.string :role_name
      t.integer :permissions, array: true, default: []
      t.references :parent, index: true

      t.timestamps null: false
    end
  end

  #
  # down: Drop table Roles
  # Used for db rollback
  #
  def down
    drop_table :roles
  end
end