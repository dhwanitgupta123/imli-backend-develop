#
# Migration to rename spat column to net_landing_price in inventory_pricings table
#
class ChangeSpatToNetLandingPriceInInventoryPricings < ActiveRecord::Migration

	#
  # Function to rename spat column to net_landing_price in inventory_pricings table
  #
  def change
  	rename_column :inventory_pricings, :spat, :net_landing_price
  end

  #
  # Function to roll back the migration
  #
  def down
    rename_column :inventory_pricings, :net_landing_price, :spat
  end
end
