class AddOutOfStockFlagInBrandPack < ActiveRecord::Migration
  def up

    add_column :brand_packs, :is_out_of_stock, :boolean, default: false

    brand_packs  = MasterProductModule::V1::BrandPack.all
    if brand_packs.present?
      brand_packs.each do |brand_pack|
        brand_pack.is_out_of_stock = false
        brand_pack.save
      end
    end

  end

  def down
    remove_column :brand_packs, :is_out_of_stock
  end
end

