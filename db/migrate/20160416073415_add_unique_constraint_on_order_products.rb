class AddUniqueConstraintOnOrderProducts < ActiveRecord::Migration
  def up
    add_index :order_products, [:cart_id, :marketplace_selling_pack_id], unique: true
  end

  def down
    remove_index :order_products, [:cart_id, :marketplace_selling_pack_id]
  end
end