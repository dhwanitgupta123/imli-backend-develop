#
# Class to add status column to roles table
#
class AddStatusToRoles < ActiveRecord::Migration

  #
  # Function to add status column to roles table
  # Function to reset Role Model to utilize in this migration
  # Initialize all existing roles to Active State
  #
  def up
    add_column :roles, :status, :integer
    UsersModule::V1::Role.reset_column_information
    UsersModule::V1::Role.all.each do |role|
      role.status = UsersModule::V1::ModelStates::RoleStates::ACTIVE
      role.save!
    end
  end

  #
  # Function to roll back migration
  #
  def down
    remove_column :roles, :status
  end
end
