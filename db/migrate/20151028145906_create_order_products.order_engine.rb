# This migration comes from order_engine
#
# create table order_products
#
class CreateOrderProducts < ActiveRecord::Migration
  #
  # create order_products
  #
  # quantity: quantity of a particular selling pack in the order
  # buying_status: buying status of a particular selling pack in the order
  # 
  def up
    create_table :order_products do |t|
      t.integer :quantity, default: 0
      t.integer :buying_status

      t.references :cart, index: true, foreign_key: true
      t.references :marketplace_selling_pack, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  # 
  # delete order_products table
  # 
  def down
    drop_table :order_products
  end
end