#
# Migration to add dirtybit column to MPSP
#
class AddDirtyBitToMpsps < ActiveRecord::Migration
  #
  # Function to add dirtybit column to MPSP
  #
  def up
    add_column :marketplace_selling_packs, :dirty_bit, :boolean, default: false
  end
  
  #
  # Function to roll back the migration
  #
  def down
    remove_column :marketplace_selling_pack, :dirty_bit
  end
end
