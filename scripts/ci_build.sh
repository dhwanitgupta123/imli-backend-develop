#!/bin/bash -xe

echo_rspec_block_name()
{
  echo "
  ##########################################################################################
  ## Running Bundle Install and Rspecs for $1
  ##########################################################################################
  "
}

script_directory="$( cd "$( dirname "$0" )" && pwd )"
# rails application root directory
application_root_dir=`dirname $script_directory`
# engines directory of rails application
echo $application_root_dir
engines_dir="$application_root_dir/engines"
# array of full path of engines
engines=`ls -d $engines_dir/*`

echo_rspec_block_name $application_root_dir
# bundle install for application
cd $application_root_dir; bundle install; rake db:create; rake db:schema:load; rake db:migrate ; rake db:seed ; rake db:test:prepare; bundle exec rspec

for engine in $engines; do
  echo_rspec_block_name $engine
  # chaning directory to target engine and running bundle install
  cd $engine ; bundle install ; bundle exec rspec
done
