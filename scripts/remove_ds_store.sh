#!/bin/bash

files=`find . -name .DS_Store`

for file in $files; do
  git rm -f $file
done
