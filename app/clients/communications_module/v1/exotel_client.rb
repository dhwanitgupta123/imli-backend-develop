require 'imli_singleton'
#
# Module to handle all the functionalities related to Communications
#
module CommunicationsModule
  #
  # Version1 for communications module
  #
  module V1
    #
    # Client Layer which is responsible to interact with Exotel Gem
    # and post/retrieve information
    #
    module ExotelClient
      extend self

      EXOTEL_CALLER_ID = APP_CONFIG['config']['EXOTEL_CALLER_ID'] || nil

      def connect_to_agent(params={})
        # takes default callerId to place call if not specified
        params.merge!({
          caller_id: EXOTEL_CALLER_ID
          })
        Exotel::Call.connect_to_agent(params)
      end

      def connect_to_flow
        Exotel::Call.connect_to_flow(params)
      end

      def details(sid)
        Exotel::Call.details(sid)
      end

    end
  end
end
