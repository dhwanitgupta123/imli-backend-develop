require 'imli_singleton'
#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Logic layer which is responsible to create user_details search index
    # and retrieve results
    #
    class UserDetailsSearchClient < SearchModule::V1::BaseSearchClient
    include ImliSingleton

      def initialize(params = '')
        @params = params
        # Creates only single instance of Repository
        @repository = USER_DETAILS_REPOSITORY
        @model = SearchModule::V1::UserDetailsSearchModel
        super
      end

    end
  end
end
