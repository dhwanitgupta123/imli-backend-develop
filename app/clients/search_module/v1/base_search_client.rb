#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Logic layer which is responsible to create user_details search index
    # and retrieve results
    #
    class BaseSearchClient
    require 'imli/elastic_search_client'

      def initialize(params = {})
        @params = params
        @custom_error_util = CommonModule::V1::CustomErrors
      end

      def save(user_details)
        begin
          Imli::ElasticSearchClient.save(@repository, user_details)
        rescue => e
          raise e
        end
      end

      def update(user_details)
        begin
          Imli::ElasticSearchClient.update(@repository, user_details)
        rescue => e
          raise e
        end
      end

      def index_data(user_details)
        begin
          Imli::ElasticSearchClient.index_data(@repository, user_details)
        rescue => e
          raise e
        end
      end

      def get_document_by_id(user_details)
        begin
          Imli::ElasticSearchClient.get_document_by_id(@repository, user_details)
        rescue => e
          raise e
        end
      end

      def search(options)
        begin
          Imli::ElasticSearchClient.search(@repository,
            {query: options[:query], max_results: options[:max_results], min_score: options[:min_score]} )
        rescue => e
          raise e
        end
      end

      def delete_previous_index
        begin
          Imli::ElasticSearchClient.delete_previous_index(@repository)
        rescue => e
          raise e
        end
      end

      def repository_exists?
        begin
          Imli::ElasticSearchClient.repository_exists?(@repository)
        rescue => e
          raise e
        end
      end

      def index_exists?
        begin
          Imli::ElasticSearchClient.index_exists?(@repository)
        rescue => e
          raise e
        end
      end

    end
  end
end
