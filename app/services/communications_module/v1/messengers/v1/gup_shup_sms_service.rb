module CommunicationsModule
  module V1
    module Messengers
      module V1
        class GupShupSmsService < BaseModule::V1::BaseService
          require 'gupshup'

          #
          # function to send SMS to user
          #
          # Parameters::
          #   * sms_attributes [hash] it contains
          #       * message: text to be sent to the user
          #       * phone_number: phone number to which sms has to be sent
          #
          def self.send_message(sms_attributes)
            gup_shup = Gupshup::Enterprise.new(
              APP_CONFIG['config']['SMS_SERVICE_USERNAME'], APP_CONFIG['config']['SMS_SERVICE_PASSWORD'])
            gup_shup.send_text_message(
              sms_attributes[:message],
              sms_attributes[:phone_number]
            )
          end
        end
      end
    end
  end
end
