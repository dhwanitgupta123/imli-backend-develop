module CommunicationsModule
  module V1
    module Messengers
      module V1
        class RouteSmsService < BaseModule::V1::BaseService

          USERNAME = APP_CONFIG['config']['ROUTE_SMS_USERNAME']
          PASSWORD = APP_CONFIG['config']['ROUTE_SMS_PASSWORD']
          SERVER = APP_CONFIG['config']['ROUTE_SMS_SMPP_SERVER']
          PORT = APP_CONFIG['config']['ROUTE_SMS_PORT']
          SOURCE = APP_CONFIG['config']['ROUTE_SMS_SOURCE']
          REQUEST = 'http://' + SERVER + ':' + PORT + '/bulksms/bulksms?username=' + USERNAME + '&password=' + PASSWORD + '&type=0&dlr=1&destination=%{phone_number}&source=' + SOURCE + '&message='

          #
          # function to send SMS to user
          #
          # Parameters::
          #   * sms_attributes [hash] it contains
          #       * message: text to be sent to the user
          #       * phone_number: phone number to which sms has to be sent
          #
          def self.send_message(sms_attributes)
            phone_number = sms_attributes[:phone_number]
            message = sms_attributes[:message]
            final_request = REQUEST%{phone_number: phone_number} + message
            HTTParty.get(final_request)
          end
        end
      end
    end
  end
end
