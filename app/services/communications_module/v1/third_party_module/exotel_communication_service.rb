require 'imli_singleton'
#
# Module to handle all the functionalities related to Communications
#
module CommunicationsModule
  #
  # Version1 for communications module
  #
  module V1
    #
    # Module to handle all the functionalities from Thrid Party Integrations
    #
    module ThirdPartyModule
      # Service layer to handle logic related to Exotel Communication
      class ExotelCommunicationService
        include ImliSingleton

        #
        # Initializing exotel communication service
        # @param params [JSON] [parameters required for versioning at various levels]
        #
        def initialize(params={})
          @params = params
        end

        def place_call(exotel_voice_attributes)
          exotel_client = CommunicationsModule::V1::ExotelClient
          begin
            exotel_call = exotel_client.connect_to_agent(
            :from => exotel_voice_attributes[:from],
            :to => exotel_voice_attributes[:to],
            :call_type => 'trans',
            :status_callback => exotel_voice_attributes[:status_callback]
            )
            if exotel_call.status.upcase == 'DND'
              voice_communication_service_instance = CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationService.instance(@params)
              voice_communication = voice_communication_service_instance.get_voice_communication_by_field('id', exotel_voice_attributes[:id])
              voice_communication_attributes = {
                status: CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationStates.get_value_by_key('DND')
              }
              voice_communication_service_instance.update_voice_communication(voice_communication_attributes, voice_communication)
              raise CommonModule::V1::CustomErrors::RunTimeError.new(exotel_call.message)
            else
              exotel_response = {
                start_time: exotel_call.start_time,
                sid: exotel_call.sid
              }
            end
          rescue Exotel::UnexpectedError, Exotel::AuthenticationError => e
            exotel_response = (Hash.from_xml(e.to_s)).with_indifferent_access
            raise CommonModule::V1::CustomErrors::InvalidDataError.new(exotel_response[:TwilioResponse][:RestException][:Message])
          end
        end

        def register_exotel_callback(request)
          voice_communication_service_instance = CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationService.instance(@params)

          # fetch corresponding voice communication status map value corresponding to exotel communication state
          voice_communication_status_map_value = CommunicationsModule::V1::ThirdPartyModule::ExotelVoiceStatusCodes.get_voice_communication_status_map_value_by_key(request[:Status])
          voice_communication_service_instance.register_callback({
            sid: request[:CallSid],
            status: voice_communication_status_map_value
            })
        end

        def get_exotel_communication_details(communication_id)
          exotel_client = CommunicationsModule::V1::ExotelClient

          begin
            exotel_call_details = exotel_client.details(communication_id)

            status = exotel_call_details.status
            # fetch corresponding voice communication status map value corresponding to exotel communication state
            voice_communication_status_map_value = CommunicationsModule::V1::ThirdPartyModule::ExotelVoiceStatusCodes.get_voice_communication_status_map_value_by_key(status)

            exotel_response = {
              recording_url: exotel_call_details.recording_url,
              status: voice_communication_status_map_value,
              end_time: exotel_call_details.end_time,
              duration: exotel_call_details.duration
            }
          rescue Exotel::UnexpectedError, Exotel::AuthenticationError => e
            exotel_response = (Hash.from_xml(e.to_s)).with_indifferent_access
            raise CommonModule::V1::CustomErrors::InvalidDataError.new(exotel_response[:TwilioResponse][:RestException][:Message])
          end

        end

      end
    end
  end
end
