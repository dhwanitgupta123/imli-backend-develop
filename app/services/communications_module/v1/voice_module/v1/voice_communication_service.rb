require 'imli_singleton'
module CommunicationsModule
  module V1
    module VoiceModule
      module V1
        class VoiceCommunicationService < CommunicationsModule::V1::CommunicationService
          include ImliSingleton
          #
          # Initializing voice communication service
          # @param params [JSON] [parameters required for versioning at various levels]
          #
          def initialize(params={})
            @params = params
            # Informing Communication Service that Voice Communication is on
            @params.merge!({communication_type: CommunicationsModule::V1::ModelStates::V1::CommunicationModelTypes.get_value_by_key('VOICE')})
            super(@params)
          end

          def place_call(request)
            callback_url = get_voice_callback_url
            voice_communication_dao_instance = CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationDao.instance(@params)
            # VALIDATION: validate if requested from_phone_number doesn't have an active call present
            active_calls_present_flag = voice_communication_dao_instance.active_calls_present_for_initiator?(request[:from])
            if active_calls_present_flag
              raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new('User is already on an ongoing call!')
            else
              # create an entry in Voice Communication Model
              voice_communication = voice_communication_dao_instance.create_voice_communication({
                from_phone_number: request[:from],
                to_phone_number: request[:to],
                call_direction: CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationDirectionTypes.get_value_by_key('OUTBOUND'),
                status: CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationStates.get_value_by_key('INITIATED')
                })
              # Place call via Thirdparty Communications Service ~[Exotel service]
              placed_voice_communication_details = place_call_using_exotel_communication_service({
                id: voice_communication[:id],
                from: voice_communication[:from_phone_number],
                to: voice_communication[:to_phone_number],
                status_callback: callback_url
                })
              # Update Values after placing call for SID, start_time
              update_voice_communication(placed_voice_communication_details, voice_communication)
            end
          end

          def register_callback(callback_parameters)
            return if callback_parameters[:sid].blank?
            voice_communication = get_voice_communication_by_field('sid', callback_parameters[:sid])
            update_voice_communication(callback_parameters, voice_communication)
          end

          def get_communication_details(communication_sid)
            return if communication_sid.blank?
            voice_communication = get_voice_communication_by_field('sid', communication_sid)
            voice_communication_dao_instance = CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationDao.instance(@params)

            # fetch status from exotel iff all details of call are not present already in Voice Communication Model
            if voice_communication_dao_instance.missing_information?(voice_communication)
              exotel_communication_service_instance = CommunicationsModule::V1::ThirdPartyModule::ExotelCommunicationService.instance(@params)
              exotel_communication_details = exotel_communication_service_instance.get_exotel_communication_details(communication_sid)
              voice_communication = update_voice_communication(exotel_communication_details, voice_communication)
            end

            return voice_communication
          end

          def get_voice_communication_by_field(field, field_value)
            voice_communication_dao_instance = CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationDao.instance(@params)
            voice_communication = voice_communication_dao_instance.get_by_field(field, field_value)
          end

          def update_voice_communication(voice_communication_details, voice_communication)
            voice_communication_dao_instance = CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationDao.instance(@params)
            voice_communication = voice_communication_dao_instance.update(voice_communication_details, voice_communication)
          end

          ###############################
          #       Private Functions     #
          ###############################

          private

          def place_call_using_exotel_communication_service(voice_communication_attributes)
            exotel_communication_service_instance = CommunicationsModule::V1::ThirdPartyModule::ExotelCommunicationService.instance(@params)
            voice_communication = exotel_communication_service_instance.place_call(voice_communication_attributes)
          end

          def get_voice_callback_url
            current_host = CommonModule::V1::Cache.read('HOST')
            callback_api_url = current_host.to_s + '/voice/callback'
          end
        end
      end
    end
  end
end
