require 'imli_singleton'
module CommunicationsModule
  module V1
    class CommunicationService < BaseModule::V1::BaseService
      include ImliSingleton

      MODEL_STATES_ENUM = CommunicationsModule::V1::ModelStates::V1::CommunicationModelTypes

      #
      # Initializing communication service
      # @param params [JSON] [parameters required for versioning at various levels]
      #
      # @return [type] [description]
      def initialize(params={})
        @params = params
        initialize_communication_type(params[:communication_type])
      end

      def get_communication_details(communication_attributes)
        case @communication_type.to_i
        when MODEL_STATES_ENUM.get_value_by_key('SMS')
          # SMS detail request
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new("SMS isn't a logged Communication Type")
        when MODEL_STATES_ENUM.get_value_by_key('NOTIFICATION')
          # NOTIFICATION detail request
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new("Notification isn't logged Communication Type")
        when MODEL_STATES_ENUM.get_value_by_key('EMAIL')
          # EMAIL detail request
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new("Email isn't logged Communication Type")
        when MODEL_STATES_ENUM.get_value_by_key('VOICE')
          # VOICE detail request
          communication_sid = communication_attributes[:sid]
          voice_communication_service_instance = CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationService.instance(@params)
          voice_communication_service_instance.get_communication_details(communication_sid)
        end
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

        def initialize_communication_type(communication_type)
          states_array = MODEL_STATES_ENUM.get_all_states_array
          if states_array.include? communication_type.to_i
            # Valid Communication Types
            @communication_type = communication_type.to_i
          else
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new("Unsupported Communication Type")
          end
        end
    end
  end
end
