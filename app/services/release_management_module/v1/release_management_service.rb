module ReleaseManagementModule
  module V1
    class ReleaseManagementService < BaseModule::V1::BaseService
      # Add versioned classed in global constants
      CACHE_UTIL = CommonModule::V1::Cache

      #
      # Initialize Releasement Management service with required params
      #
      # @param params [Hash] Hash object of parameters
      #
      def initialize(params)
        @params = params
        super
      end

      #
      # Get app update popup content
      # Fetch the content from Redis if version is outdated
      #
      # @param params [JSON] [Parameters containing platform and app version]
      #
      # @return [JSON] [Hash containing content OR empty hash]
      #
      def get_app_update_popup_content(params)
        if is_version_outdated?(params)
          content = {
            target_version: CACHE_UTIL.read('ANDROID_LATEST_VERSION'),
            type: CACHE_UTIL.read('APP_UPDATE_POPUP_TYPE'),
            optional: CACHE_UTIL.read('APP_UPDATE_OPTIONAL'),
            title: CACHE_UTIL.read('APP_UPDATE_POPUP_TITLE'),
            description: CACHE_UTIL.read('APP_UPDATE_POPUP_DESCRIPTION'),
            change_log: CACHE_UTIL.read_array('APP_UPDATE_POPUP_CHANGE_LOG') || [],
            images: get_app_update_images_array
          }
          return content
        end
        return {}
      end

      #
      # check whether version of app which sent the request is outdated or not
      #
      # @param params [JSON] [Parameters containing platform and app version]
      #
      # @return [Boolean] [true of version is outdated else false]
      #
      def is_version_outdated?(params)
        platform = USER_SESSION[:platform_type] || params[:platform]
        app_version = params[:version_code]

        if platform.present? && platform.to_s != 'ANDROID'
          return false
        end

        latest_version = CACHE_UTIL.read('ANDROID_LATEST_VERSION')

        return true if version_lower_than_latest_version(app_version, latest_version)
        return false
      end

      #
      # Check whether the current version is lower than the latest version
      #
      # @param current_version [String] [Current version of the app]
      # @param latest_version [String] [Latest supported version]
      #
      # @return [Boolean] [true if current version is lower than latest version]
      #
      def version_lower_than_latest_version(current_version, latest_version)
        return false if current_version.blank? || latest_version.blank?
        return true if current_version.to_i < latest_version.to_i
        return false
      end

      #
      # Fetch all size image urls based on the IDs stored in our cache
      #
      # @return [Array] [Array of image hashes with priority and urls]
      #
      def get_app_update_images_array
        image_ids = CACHE_UTIL.read_array('APP_UPDATE_POPUP_IMAGE_IDS') || []
        return ImageServiceModule::V1::ImageServiceHelper.get_images_hash_by_ids(image_ids) if image_ids.present?
        return []
      end

    end
  end
end
