module SettingsModule
  module V1
    module FeatureSettingsService
      class << self

        FALSE_VALUE_FOR_A_FEATURE = '0'

        #
        # Fetch user specific features
        #
        # @param filtered_map [JSON] [Hash of keys whose features are to be fetched]
        #
        # @return [JSON] [Hash containing feature names and its values]
        #
        def fetch_user_specific_features(filtered_map)
          features = load_features(filtered_map)
          user_specific_settings(features)
        end

        #
        # Get dynamic experiment value based on the configuration
        # TO-DO: This is not a GOOD way. Fetching an experiment value should
        # be independent of dynamic settings map.
        # Also, filters should be separate from ImliWeblab.
        # Weblab role is just to give value based on percentage mentioned
        #
        # @param key [String] [Key for which value is to be fetched]
        #
        # @return [String] [Value corresponds to the given key]
        #
        def get_dynamic_feature_value(key)
          feature_class = SettingsModule::V1::Feature
          feature_config = dynamic_settings_map[key.to_s]
          return FALSE_VALUE_FOR_A_FEATURE if feature_config.blank?
          feature = feature_class.new(key, feature_config)
          fetch_feature_value(feature)
        end

        #
        # Load Features as per the filtered map of keys passed
        #
        # @param filtered_map [JSON] [Hash of keys whose features are to be fetched]
        #
        # @return [JSON] [Hash of Feature objects]
        #
        def load_features(filtered_map)
          feature_class = SettingsModule::V1::Feature
          features = []
          filtered_map.each do |feature_name, configurations|
            feature = feature_class.new(feature_name, configurations)
            features.push(feature)
          end
          features
        end

        #
        # Fetch user specific settings
        #
        # @param features [Array] [Array of Feature objects]
        #
        # @return [Array] [Array containing feature keys and its values]
        #
        def user_specific_settings(features)
          return {} if features.blank?
          user_specific_settings = {}
          features.each do |feature|
            user_specific_settings[feature.name] = fetch_feature_value(feature)
          end
          user_specific_settings
        end

        #
        # Fetch feature value as per its experiment
        # after applying corresponding filters
        #
        def fetch_feature_value(feature)
          if feature.filters.present?
            return FALSE_VALUE_FOR_A_FEATURE unless apply_filters(feature.filters)
          end
          get_treatment_value(feature.experiment)
        end

        #
        # Apply filters as per the given Array of Filter classes
        #
        def apply_filters(filters)
          return true if filters.blank?
          filters.each do |filter|
            return false unless filter.apply({user_id: user_id})
          end
          return true
        end

        #
        # Get experiment/treatment value via SHIELD service
        #
        def get_treatment_value(experiment)
          SettingsModule::V1::ShieldService.get_treatment_value(experiment.to_s) || FALSE_VALUE_FOR_A_FEATURE
        end

        #
        # Fetch user_id of the requester coming in API request (fetched in ACL using session_token)
        #
        def user_id
          USER_SESSION[:user_id]
        end

        def dynamic_settings_map
          dynamic_settings_instance = SettingsModule::V1::DynamicSettings::DynamicSettings.new
          dynamic_settings = dynamic_settings_instance.fetch
        end

      end # End of class
    end
  end
end