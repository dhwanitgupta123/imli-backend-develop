module SettingsModule
  module V1
    module ConstantSettingsService
      class << self

        #
        # TO-DO
        # For now, I am just hard coding the settings keys here
        # They need to move out to some yml with proper heirarchy structure
        # when a new use case comes for user_specific_constant
        #
        def fetch_user_specific_constants
          convenience_fees = get_convenience_fees_hash
          return {
            'CONVENIENCE_COST': JSON.generate(convenience_fees)
          }
        end

        #
        # Get convenience fees hash according to the requester's user_id
        #
        # @return [Hash] [Hash containing convenience fees ladder, fees label, and restricted amount]
        #
        def get_convenience_fees_hash
          cart_computation_service = MarketplaceOrdersModule::V1::CartComputationService.instance
          cache_util = CommonModule::V1::Cache

          convenience_fees_label = cache_util.read('CONVENIENCE_FEES_LABEL')
          convenience_fees_short_description = cache_util.read('CONVENIENCE_FEES_SHORT_DESCRIPTION')
          convenience_cost = cart_computation_service.get_convenience_cost_for_user(user)
          return {} if convenience_cost.blank?
          # Remove order_number key and include fees_display_label
          convenience_cost.delete(:ORDER_NUMBER)
          convenience_cost = convenience_cost.merge({
            'CONVENIENCE_FEES_LABEL': convenience_fees_label,
            'CONVENIENCE_FEES_SHORT_DESCRIPTION': convenience_fees_short_description
            })
          return convenience_cost
        end

        #
        # Fetch user_id of the requester coming in API request (fetched in ACL using session_token)
        #
        def user
          user_id = USER_SESSION[:user_id]
          user_service = UsersModule::V1::UserService.new
          user_service.get_user_from_user_id(user_id)
        end


      end
    end
  end
end
