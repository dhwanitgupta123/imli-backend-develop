module SettingsModule
  module V1
    module Filters
      module UserOrderCountFilter
        class << self

          MIN_ORDER_COUNT = CommonModule::V1::Cache.read_int('MIN_ORDER_COUNT_FOR_CONVENIENCE_FEES') || 3

          #
          # Apply filter by counting user's Orders and compare it with 
          # minimum order count
          #
          # If current_order_number is less than MIN_ORDER_COUNT, then return false
          # ElsIf current_order_number is more than MIN_ORDER_COUNT, then return true
          #
          # @param options [JSON] [Hash containing user_id]
          #
          # @return [Boolean] [true if conditions apply else false]
          #
          def apply(options)
            user_id = options[:user_id]
            is_eligible?(current_order_count(user_id))
          end

          #
          # Check for order number eligibility as per the MIN_ORDER_COUNT key value
          #
          def is_eligible?(order_count)
            return false if order_count.blank?
            # Compare it with min order count range
            case order_count.to_i
            when 0..MIN_ORDER_COUNT
              return false
            when proc { |n| n > MIN_ORDER_COUNT }
              return true
            else # default
              return false
            end
          end

          #
          # Fetch current order count for the passed user_id
          #
          def current_order_count(user_id)
            user_service = UsersModule::V1::UserService.new
            user = user_service.get_user_from_user_id(user_id)

            order_dao = MarketplaceOrdersModule::V1::OrderDao.new
            order_count = order_dao.get_current_number_of_order_of_user(user)
            return order_count
          end

        end
      end
    end
  end
end
