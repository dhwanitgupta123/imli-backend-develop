module SettingsModule
  module V1
    module ImliStaticSettingService
      class << self

        #
        # Get static feature value for a particular key
        #
        # @param key [String] [Key for which value is to be fetched]
        #
        # @return [String] [Value corresponds to the given key]
        #
        def get_feature_value(key)
          return '0' if key.blank?
          SettingsModule::V1::Shield.get_static_feature_value(key)
        end

      end
    end
  end
end