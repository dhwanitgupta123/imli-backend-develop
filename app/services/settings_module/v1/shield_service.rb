module SettingsModule
  module V1
    class ShieldService < BaseModule::V1::BaseService

      SHIELD = SettingsModule::V1::Shield

      # 
      # Initialize Shield service with required params
      #
      def initialize(params)
        @params = params
      end

      #
      # Get Treatment Value corresponding to the requester
      # Currently the deciding factor is only 'user_id'. Later on, it can
      # be extended to Marketplace, Location, Platform, etc.
      #
      # @param key [String] [Key for which SHIELD treatment value is to be fetched]
      #
      # @return [String] [Value of the treatment]
      #
      def self.get_treatment_value(key)
        SHIELD.get_treatment_value(key)
      end

      #
      # Start SHIELD experiment for a particular user
      # Currently the deciding factor is only 'user_id'. Later on, it can
      # be extended to Marketplace, Location, Platform, etc.
      #
      # @param key [String] [Name of the experiment for which SHIELD experiment is to be started]
      #
      # @return [String] [Value of the experiment]
      #
      def self.start_experiment(experiment_name)
        SHIELD.start_experiment(experiment_name)
      end

      #
      # Finish SHIELD experiment for a particular user
      # Currently the deciding factor is only 'user_id'. Later on, it can
      # be extended to Marketplace, Location, Platform, etc.
      #
      # @param key [String] [Name of the experiment for which SHIELD experiment is to be finished]
      #
      # @return [String] [Value of the experiment]
      #
      def self.finish_experiment(experiment_name)
        SHIELD.finish_experiment(experiment_name)
      end


      def self.get_experiment_value(key)
        return '0' if key.blank?
        setting_service = SettingsModule::V1::SettingService
        setting_service.get_dynamic_setting_with_key(key.to_s)
      end

    end
  end
end