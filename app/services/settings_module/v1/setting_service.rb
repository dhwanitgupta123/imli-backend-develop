require 'json'
module SettingsModule
  module V1
    class SettingService < BaseModule::V1::BaseService

      # 
      # Initialize Employee service with required params
      #
      # @param params [Hash] Hash object of parameters
      #
      def initialize(params)
        @params = params
        super
      end

      #
      # Get all the settings required on App side
      # Keys:
      #   - MAP --> contains all maps of states, events, etc.
      #   - FEATURES --> contains feature enable/disable keys
      #   - CONSTANTS --> contains all relevant constants needed on App side
      #
      # @return [type] [description]
      #
      def get_all_static_settings
        begin
          # Fetch settings from Map files
          map_settings_instance = SettingsModule::V1::StaticSettings::MapSettings.new(@params)
          feature_settings_instance = SettingsModule::V1::StaticSettings::FeatureSettings.new(@params)
          constant_settings_instance = SettingsModule::V1::StaticSettings::ConstantSettings.new(@params)
          map_settings  = map_settings_instance.fetch
          feature_settings = feature_settings_instance.fetch
          required_constants = constant_settings_instance.fetch

          settings = {}
          settings['MAP'] = map_settings
          settings['FEATURES'] = feature_settings
          settings['CONSTANTS'] = required_constants
          return settings
        rescue => e
          return {}
        end
      end

      #
      # Fetch dynamic settings map, and fetch user specific values for the map
      #
      # @return [JSON] [Hash of user specific dynamic settings]
      #
      def get_all_dynamic_settings(params)
        dynamic_settings_instance = SettingsModule::V1::DynamicSettings::DynamicSettings.new(@params)
        feature_setting_service = SettingsModule::V1::FeatureSettingsService
        constant_setting_service = SettingsModule::V1::ConstantSettingsService

        # Call Shield Service to fetch user specific settings
        dynamic_settings = dynamic_settings_instance.fetch

        return {} if dynamic_settings.blank?
        # As per the dynamic settings map (used for filtering/whitelisting)
        # fetch corresponding user specific values
        user_specific_features = feature_setting_service.fetch_user_specific_features(dynamic_settings)
        user_specific_constants = constant_setting_service.fetch_user_specific_constants
        dynamic_components = fetch_dynamic_components(params)
        settings = {}
        settings['MAP'] = {}
        settings['FEATURES'] = user_specific_features
        settings['CONSTANTS'] = user_specific_constants
        settings['COMPONENTS'] = dynamic_components
        return settings
      end

      #
      # Get dynamic experiment value based on the configuration
      # 
      # TO-DO: This is not a GOOD way. Fetching an experiment value should
      # be independent of dynamic settings map.
      # Also, filters should be separate from ImliWeblab.
      # Weblab role is just to give value based on percentage mentioned
      #
      # @param key [String] [Key for which value is to be fetched]
      #
      # @return [String] [Value corresponds to the given key]
      #
      def self.get_dynamic_setting_with_key(key)
        feature_setting_service = SettingsModule::V1::FeatureSettingsService
        feature_setting_service.get_dynamic_feature_value(key)
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # Fetch user specific values for given filtered map
      # Filtered Map: Hash containing
      #   - key: Name of the feature
      #   - value: Name of the experiment
      #
      # @param filtered_map [JSON] [Hash containing whitelisted values whose values are to be fetched]
      #
      # @return [JSON] [Hash of user specific settings with key as setting_name and value as feature value]
      #
      def fetch_user_specific_features(filtered_map)
        return {} if filtered_map.blank?
        shield_service = SettingsModule::V1::ShieldService
        user_specific_settings = {}
        filtered_map.each do |key, value|
          treatment_value = shield_service.get_treatment_value(value.to_s) || '0'
          user_specific_settings[key.to_s] = treatment_value
        end
        return user_specific_settings
      end

      #
      # Fetch Dynamic components for the user
      #
      # @param params [JSON] [Parameters containing context from the user: platform and app version]
      #
      # @return [JSON] [Components hash containing POPUP array ]
      #
      def fetch_dynamic_components(params)
        release_management_service = ReleaseManagementModule::V1::ReleaseManagementService.new(@params)

        popups = []
        app_update_popup = release_management_service.get_app_update_popup_content({platform: params[:platform], version_code: params[:version_code]})
        
        if app_update_popup.present?
          popups.push(app_update_popup)
        end

        components = {
          POPUPS: popups
        }
        return components
      end

    end
  end
end