module SettingsModule
  module V1
    module ImliWeblabService
      class << self

        #
        # Get dynamic experiment value for a particular key
        # Experiment will corresponds to the requester calling this function
        # So, it needs USER_SESSION[:user_id] to be set
        #
        # @param key [String] [Key for which value is to be fetched]
        #
        # @return [String] [Value corresponds to the given key]
        #
        def get_feature_value(key)
          return '0' if key.blank?
          SettingsModule::V1::SettingService.get_dynamic_setting_with_key(key.to_s)
        end

      end
    end
  end
end