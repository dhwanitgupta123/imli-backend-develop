module CategorizationModule
  module V1
    #
    # Categorization Api Service class to implement CRUD for categorization APIs
    #
    class CategorizationApiService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      DEPARTMENT_SERVICE = CategorizationModule::V1::DepartmentService
      CATEGORY_SERVICE = CategorizationModule::V1::CategoryService
      CATEGORY_SERVICE_HELPER = CategorizationModule::V1::CategoryServiceHelper
      CATEGORIZATION_API_HELPER = CategorizationModule::V1::CategorizationApiHelper
      CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      CATEGORY_API_RESPONSE_DECORATOR = CategorizationModule::V1::CategorizationApiResponseDecorator
      CATEGORY_DAO = CategorizationModule::V1::CategoryDao
      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao
      GENERAL_HELPER = CommonModule::V1::GeneralHelper
      
      #
      # initialize CategorizationApiService Class
      #
      def initialize(params)
        @params = params
      end

      #
      # function to get all active department details
      #
      # Response::
      #   * sends response with all the active department details
      #
      # @return [response]
      #
      def get_all_departments_details(paginate_params)
        begin
          department_details = get_department_details(paginate_params)
          CATEGORY_API_RESPONSE_DECORATOR.create_department_details_response(department_details)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          CATEGORY_API_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # Get all active products corresponding to given category
      #
      # @param params [JSON] [Hash containing category id and paginate parameters]
      # Hash:
      #   - id : category id
      #   - paginate_params: parameters for pagination
      #
      # @return [Response] [response to be sent to the user]
      def get_products_by_category(params)
        begin

          response = get_products_for_category(params)
          return response
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          CATEGORY_API_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          CATEGORY_API_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError, CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          CATEGORY_API_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end
      end

      #
      # Get all active products corresponding to given category but with FILTERS
      # applied over them.
      #
      # @param params [JSON] [Hash containing category id and paginate parameters with FILTERS]
      # Hash:
      #   - id : category id
      #   - product_params: containing {:filters, :page_no, :per_page, :order...}
      #
      # @return [Response] [response to be sent to the user]
      def get_products_by_filter(params)
        begin
          response = get_products_after_applying_filters(params)
          return response
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          CATEGORY_API_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          CATEGORY_API_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError, CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
          CATEGORY_API_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end
      end
      #
      # Function to get all active products corresponding to given category
      #
      # @param params [JSON] [Hash containing category id and paginate parameters]
      # Hash:
      #   - id : category id
      #   - paginate_params: parameters for pagination
      #
      # @return [Response] [response to be sent to the user]
      # 
      # @raise [InsufficientDataError] [if passed parameters were insufficient enough to process]
      # @raise [PreConditionRequiredError] [requested category is not active anymore]
      # @raise [ResourceNotFoundError] [if no subcategories exist for requested category]
      # @raise [CategoryNotFoundError] [if no category exist for passed category]
      #
      def get_products_for_category(params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Category ID'}) if params[:id].blank?
        category = fetch_and_validate_category(params[:id])
        # Fetch all subcategories
        sub_categories = fetch_sub_categories_for_category(category)

        # Fetch all ACTIVE products for sub-categories
        products_array = get_products_for_sub_categories(sub_categories, params[:testing])

        return CATEGORY_API_RESPONSE_DECORATOR.create_products_and_filters_response(sub_categories, products_array)
      end

      #
      # Get all active products corresponding to given category but with FILTERS
      # applied over them.
      #
      # @param params [JSON] [Hash containing category id and paginate parameters with FILTERS]
      # Hash:
      #   - id : category id
      #   - product_params: containing {:filters, :page_no, :per_page, :order...}
      #
      # @return [Response] [response to be sent to the user]
      #
      # @raise [InsufficientDataError] [if passed parameters were insufficient enough to process]
      # @raise [PreConditionRequiredError] [requested category is not active anymore]
      # @raise [ResourceNotFoundError] [if no subcategories exist for requested category]
      # @raise [CategoryNotFoundError] [if no category exist for passed category]
      #
      def get_products_after_applying_filters(params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Category ID'}) if params[:id].blank?
        category = fetch_and_validate_category(params[:id])
        # Fetch all subcategories
        sub_categories = fetch_sub_categories_for_category(category)

        # Check for filters passed in request
        # Need to modify the logic to optimize DB query
        # Also, Add pagination to fetch MPSPs using where query
        # Open Issue: https://github.com/imlitech/imli-backend/issues/183
        if (params[:product_params].present? && params[:product_params][:filters].present?)
          filters = params[:product_params][:filters]
          filtered_sub_categories = fetch_sub_categories_from_filters(filters)
          # Override sub categories only if filtered sub categories are SUBSET of overall sub categories
          if filtered_sub_categories.present? && (filtered_sub_categories - sub_categories).empty?
            sub_categories = filtered_sub_categories
          end
        end
        # Fetch all ACTIVE products for sub-categories
        products_array = get_products_for_sub_categories(sub_categories)
        return CATEGORY_API_RESPONSE_DECORATOR.create_products_and_filters_response(sub_categories, products_array)
      end

      #
      # Fetch all active MPSPs for given sub categories
      #
      # @param sub_categories [Array] [Array of Sub categories Object]
      #
      # @return [Array] [Array of all active MPSP/display products]
      #
      # @raise [ResourceNotFoundError] [if no products found after search]
      #
      def get_products_for_sub_categories(sub_categories, testing)
        category_service = CATEGORY_SERVICE.new(@params)
        products_array = []

        sub_categories = CATEGORY_DAO.get_aggregated_sub_categories(sub_categories)

        # Fetch products for each active sub_categories
        sub_categories.each do |sub_category|
          display_products = category_service.get_display_products_for_subcategory(sub_category, testing)
          # Removing dupliates by taking union of previous array and result array
          # Array Union Property: http://ruby-doc.org/core-2.2.3/Array.html#7C-method
          products_array = products_array | display_products
        end

        if products_array.blank?
          raise CUSTOM_ERRORS_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::NO_PRODUCT_EXISTS)
        end
        
        products_array = MARKETPLACE_SELLING_PACK_DAO.get_aggregated_products(products_array)
        products_array = products_array.sort{|a,b| [a.marketplace_brand_packs[0].brand_pack.sub_category.priority, b.marketplace_selling_pack_pricing.savings] <=> [b.marketplace_brand_packs[0].brand_pack.sub_category.priority,a.marketplace_selling_pack_pricing.savings] }
        return products_array
      end

      #
      # Fetch and validate category
      #
      # @param category_id [Integer] [id for which category is to be fetched]
      #
      # @return [Object] [Category Object w.r.t. passed id]
      #
      # @raise [CategoryNotFoundError] [if no corresponding category exists]
      # @raise [PreConditionRequiredError] [if found category is not Active]
      def fetch_and_validate_category(category_id)
        category_service = CATEGORY_SERVICE.new(@params)
        category = category_service.get_category_by_id(category_id)
        if category.status !=  CATEGORY_MODEL_STATES::ACTIVE
          raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(CONTENT_UTIL::NON_ACTIVE_CATEGORY)
        end
        return category
      end

      #
      # Fetch sub categories for a given category
      #
      # @param category [Object] [category for which sub-categories is to be fetched]
      #
      # @return [Array] [Array of Sub-Category Object w.r.t. passed category]
      #
      # @raise [PreConditionRequiredError] [if no corresponding sub-category exists]
      #
      def fetch_sub_categories_for_category(category)
        sub_categories = category.sub_categories.order('priority ASC')
 
        if sub_categories.blank?
          raise CUSTOM_ERRORS_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::NO_SUB_CATEGORIES + ' : ' + category.id.to_s)
        end
        return sub_categories
      end

      #
      # Fetch sub categories for a given FILTER
      #
      # @param filter [JSON] [JSON Hash containg array of sub_categories IDs]
      #
      # @return [Array] [Array of Sub-Category Object w.r.t. passed filter hash]
      #
      def fetch_sub_categories_from_filters(filters)
        sub_categories_ids = filters[:sub_categories]
        return [] unless sub_categories_ids.present?
        # Fetch sub category corresponding to passed IDs
        category_service = CATEGORY_SERVICE.new(@params)
        sub_categories_array = []
        sub_categories_ids.each do |id|
          begin
            sub_category = category_service.get_category_by_id(id)
            sub_categories_array.push(sub_category)
          rescue CUSTOM_ERRORS_UTIL::CategoryNotFoundError => e
            # Do nothing. Just ignore that filter
          end
        end
        return sub_categories_array
      end

      #
      # function to get department details
      #
      # Response::
      #   * sends response with all the departments details and page count
      #
      # @return [response]
      #
      def get_department_details(paginate_params)
        department_service = DEPARTMENT_SERVICE.new(@params)
        category_service = CATEGORY_SERVICE.new(@params)

        active_departments_array = []
        departments_response = department_service.get_active_departments(paginate_params)
        page_count = departments_response[:page_count]
        active_departments = departments_response[:departments]
        active_departments.each do |department|
          active_categories = category_service.get_active_categories_by_department(department)
          unless active_categories.blank?
            active_categories_hash = CATEGORY_SERVICE_HELPER.get_categories_hash(active_categories)
            department_details = CATEGORIZATION_API_HELPER.map_department_to_hash(department, active_categories_hash)
            active_departments_array.push(department_details)
          end
        end
        return { departments: active_departments_array, page_count: page_count }
      end
      
    end
  end
end