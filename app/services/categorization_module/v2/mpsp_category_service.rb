module CategorizationModule
  module V2
    #
    # MpspCategory Service class to implement CRUD for mpsp_category model
    #
    class MpspCategoryService < BaseModule::V1::BaseService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      MPSP_CATEGORIZATION_API_SERVICE = CategorizationModule::V2::MpspCategorizationApiService
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      GENERAL_HELPER = CommonModule::V1::GeneralHelper

      #
      # initialize MpspCategoryService Class
      #
      def initialize(params)
        @params = params
        @mpsp_category_model = CategorizationModule::V1::MpspCategory
      end

      #
      # function to check the presence of a mpsp_category based on it's id
      #
      # Parameters::
      #   *mpsp_category_id [ineteger] id of mpsp_category to be searched
      #
      # @return [mpsp_category object]
      def get_mpsp_category_by_id(mpsp_category_id)
        mpsp_category = @mpsp_category_model.find_by_mpsp_category_id(mpsp_category_id)
        if mpsp_category.present?
          return mpsp_category
        else
          raise CUSTOM_ERRORS_UTIL::CategoryNotFoundError.new(CONTENT_UTIL::CATEGORY_NOT_PRESENT)
        end
      end

      #
      # function to get list of all the  active categories under a mpsp_department
      #
      # @return [array of categories object]
      def get_active_mpsp_categories_by_mpsp_department(mpsp_department, testing=false)
        mpsp_categorization_api_service = MPSP_CATEGORIZATION_API_SERVICE.new(@params)
        all_mpsp_categories = @mpsp_category_model.get_active_mpsp_categories_by_mpsp_department(mpsp_department)
        active_mpsp_categories = all_mpsp_categories.dup
        active_mpsp_categories.each do |mpsp_category|
          mpsp_sub_category_and_products = mpsp_categorization_api_service.get_products_for_mpsp_sub_categories(mpsp_category.mpsp_sub_categories, {pagination_params: {per_page: -1}, testing: testing})
          if mpsp_sub_category_and_products[:mpsp_sub_categories].blank?
            active_mpsp_categories = active_mpsp_categories - [mpsp_category]
          end
        end
        return active_mpsp_categories
      end

      #
      # Get display products/MPSPs for a given sub category
      # Return empty array if it is not active
      #
      # @param mpsp_sub_category [Object] it is output of join on tables Active SubMpspCategory, Active BrandPacks,
      #                               Active MPBP and Active MPSP
      #
      # @return [Array] [Array of display products/MPSPs]
      def get_display_products_for_mpsp_sub_category(mpsp_sub_category, testing=false)
        return [] if mpsp_sub_category.blank?

        if !GENERAL_HELPER.string_to_boolean(testing)
          active_products =  mpsp_sub_category.marketplace_selling_packs
        else
          active_products = mpsp_sub_category.marketplace_selling_packs.
            where("status = ? OR dirty_bit = ?", COMMON_MODEL_STATES::ACTIVE, true)
        end
        return active_products
      end
    end
  end
end
