module CategorizationModule
  module V2
    #
    # MpspCategorizationApiService handles mpsp categorization related logic
    #
    class MpspCategorizationApiService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      MPSP_DEPARTMENT_SERVICE = CategorizationModule::V1::MpspDepartmentService
      MPSP_CATEGORY_SERVICE = CategorizationModule::V2::MpspCategoryService
      MPSP_CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      MPSP_CATEGORY_API_RESPONSE_DECORATOR = CategorizationModule::V2::MpspCategorizationApiResponseDecorator
      MPSP_CATEGORY_DAO = CategorizationModule::V1::MpspCategoryDao
      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao
      MARKETPLACE_SELLING_PACK = MarketplaceProductModule::V1::MarketplaceSellingPack
      PAGINATION_UTIL = CommonModule::V1::Pagination
      MPSP_CATEGORY_SERVICE_HELPER = CategorizationModule::V1::MpspCategoryServiceHelper
      MPSP_CATEGORIZATION_API_HELPER = CategorizationModule::V1::MpspCategorizationApiHelper

      #
      # initialize MpspCategorizationApiService Class
      #
      def initialize(params={})
        @params = params
      end
      #
      # Function to get all active products corresponding to given mpsp_category
      #
      # @param params [JSON] [Hash containing mpsp_category id and paginate parameters]
      # Hash:
      #   - id : mpsp_category id
      #   - paginate_params: parameters for pagination
      #
      # @return [Response] [response to be sent to the user]
      #
      # @raise [InsufficientDataError] [if passed parameters were insufficient enough to process]
      # @raise [PreConditionRequiredError] [requested mpsp_category is not active anymore]
      # @raise [ResourceNotFoundError] [if no submpsp_categories exist for requested mpsp_category]
      # @raise [CategoryNotFoundError] [if no mpsp_category exist for passed mpsp_category]
      #
      def get_products_for_mpsp_category(params)
        raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'MpspCategory ID'}) if params[:id].blank?
        mpsp_category = fetch_and_validate_mpsp_category(params[:id])
        # Fetch all submpsp_categories
        mpsp_sub_categories = fetch_mpsp_sub_categories_for_mpsp_category(mpsp_category)

        # Fetch all ACTIVE products for sub-mpsp_categories
        products_array_and_update_mpsp_sub_categories = get_products_for_mpsp_sub_categories(mpsp_sub_categories, params)
        return MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_products_and_filters_response(products_array_and_update_mpsp_sub_categories)
      end

      #
      # function to get all active mpsp_department details
      #
      # Response::
      #   * sends response with all the active mpsp_department details
      #
      # @return [response]
      #
      def get_all_mpsp_departments_details(paginate_params)
        mpsp_department_details = get_mpsp_department_details(paginate_params)
        MPSP_CATEGORY_API_RESPONSE_DECORATOR.create_mpsp_department_details_response(mpsp_department_details)
      end

      #
      # Fetch and validate mpsp_category
      #
      # @param mpsp_category_id [Integer] [id for which mpsp_category is to be fetched]
      #
      # @return [Object] [MpspCategory Object w.r.t. passed id]
      #
      # @raise [CategoryNotFoundError] [if no corresponding mpsp_category exists]
      # @raise [PreConditionRequiredError] [if found mpsp_category is not Active]
      def fetch_and_validate_mpsp_category(mpsp_category_id)
        mpsp_category_service = MPSP_CATEGORY_SERVICE.new(@params)
        mpsp_category = mpsp_category_service.get_mpsp_category_by_id(mpsp_category_id)
        if mpsp_category.status !=  MPSP_CATEGORY_MODEL_STATES::ACTIVE
          raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(CONTENT_UTIL::NON_ACTIVE_CATEGORY)
        end
        return mpsp_category
      end


      #
      # Fetch sub mpsp_categories for a given mpsp_category
      #
      # @param mpsp_category [Object] [mpsp_category for which sub-mpsp_categories is to be fetched]
      #
      # @return [Array] [Array of Sub-MpspCategory Object w.r.t. passed mpsp_category]
      #
      # @raise [PreConditionRequiredError] [if no corresponding sub-mpsp_category exists]
      #
      def fetch_mpsp_sub_categories_for_mpsp_category(mpsp_category)
        mpsp_sub_categories = mpsp_category.mpsp_sub_categories.order('priority ASC')

        if mpsp_sub_categories.blank?
          raise CUSTOM_ERRORS_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::NO_SUB_CATEGORIES + ' : ' + mpsp_category.id.to_s)
        end
        return mpsp_sub_categories
      end

      #
      # Fetch all active MPSPs for given sub mpsp_categories
      #
      # @param mpsp_sub_categories [Array] [Array of Sub mpsp_categories Object]
      #
      # @return [Array] [Array of all active MPSP/display products]
      #
      # @raise [ResourceNotFoundError] [if no products found after search]
      #
      def get_products_for_mpsp_sub_categories(mpsp_sub_categories, params={})
        pagination = PAGINATION_UTIL.new
        mpsp_category_service = MPSP_CATEGORY_SERVICE.new(@params)
        testing = params[:testing] || false
        products_array = []
        marketplace_selling_pack_dao = MARKETPLACE_SELLING_PACK_DAO.new
        mpsp_sub_categories = MPSP_CATEGORY_DAO.get_aggregated_mpsp_sub_categories(mpsp_sub_categories, testing)
        # Fetch products for each active mpsp_sub_categories
        if mpsp_sub_categories.present?
          mpsp_sub_categories.each do |mpsp_sub_category|
            display_products = mpsp_category_service.get_display_products_for_mpsp_sub_category(mpsp_sub_category, testing)
            display_products = remove_out_of_stock_mpsp(display_products)
            # Deleting sub_category if active display product is 0 in that mpsp_sub_category
            if display_products.blank?
              mpsp_sub_categories = mpsp_sub_categories - [mpsp_sub_category]
            end
            # Removing dupliates by taking union of previous array and result array
            # Array Union Property: http://ruby-doc.org/core-2.2.3/Array.html#7C-method
            products_array = products_array | display_products
          end
        end

        return {products_array: products_array, mpsp_sub_categories: mpsp_sub_categories} if products_array.blank?
        products_hash = pagination.get_paginated_array_elements(products_array, params[:pagination_params])
        products_array = MARKETPLACE_SELLING_PACK_DAO.get_aggregated_products(products_hash[:elements])
        products_array = products_array.sort{|a,b|
          [a.mpsp_sub_category.priority, b.marketplace_selling_pack_pricing.savings] <=>
          [b.mpsp_sub_category.priority, a.marketplace_selling_pack_pricing.savings] }

        return {products_array: products_array, mpsp_sub_categories: mpsp_sub_categories, page_count: products_hash[:page_count]}
      end

      #
      # function to get mpsp_department details
      #
      # Response::
      #   * sends response with all the mpsp_departments details and page count
      #
      # @return [response]
      #
      def get_mpsp_department_details(paginate_params)
        mpsp_department_service = MPSP_DEPARTMENT_SERVICE.new(@params)
        mpsp_category_service = MPSP_CATEGORY_SERVICE.new(@params)

        active_mpsp_departments_array = []
        mpsp_departments_response = mpsp_department_service.get_active_mpsp_departments(paginate_params)
        page_count = mpsp_departments_response[:page_count]
        active_mpsp_departments = mpsp_departments_response[:mpsp_departments]

        active_mpsp_departments.each do |mpsp_department|
          active_mpsp_categories = mpsp_category_service.get_active_mpsp_categories_by_mpsp_department(mpsp_department, paginate_params[:testing])
          unless active_mpsp_categories.blank?
            active_mpsp_categories_hash = MPSP_CATEGORY_SERVICE_HELPER.get_mpsp_categories_hash(active_mpsp_categories)
            mpsp_department_details = MPSP_CATEGORIZATION_API_HELPER.map_mpsp_department_to_hash(mpsp_department, active_mpsp_categories_hash)
            active_mpsp_departments_array.push(mpsp_department_details)
          end
        end
        return { mpsp_departments: active_mpsp_departments_array, page_count: page_count }
      end

      def remove_out_of_stock_mpsp(mpsps)
        # OUT_OF_STOCK_FLAG to turn on & off out of stock feature
        # SHOW_OUT_OF_STOCK_PRODUCT_FLAG to show and not to show out of stock products
        out_of_stock_flag = ImliStaticSettings.get_feature_value('OUT_OF_STOCK_FLAG')
        show_out_of_stock_product_flag = ImliStaticSettings.get_feature_value('SHOW_OUT_OF_STOCK_PRODUCT_FLAG')
        mpsp_array = []
        if out_of_stock_flag == '1'
          mpsps.each do |mpsp|
            is_out_of_stock = MarketplaceProductModule::V1::MarketplaceSellingPackDao.is_out_of_stock?(mpsp)
            if (USER_SESSION[:platform_type] == 'ANDROID' && USER_SESSION[:version_number] < 22)
              mpsp_array.push(mpsp) unless is_out_of_stock
            else
              if (USER_SESSION[:platform_type] == 'WEB') || (USER_SESSION[:platform_type] == 'ANDROID' && USER_SESSION[:version_number] >= 22)
                if show_out_of_stock_product_flag == '0'
                  mpsp_array.push(mpsp) unless is_out_of_stock
                else
                  mpsp_array.push(mpsp)
                end
              else
                mpsp_array.push(mpsp) unless is_out_of_stock
              end
            end
          end
        else
          return mpsps
        end
        return mpsp_array
      end
    end
  end
end
