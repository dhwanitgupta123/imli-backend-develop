module PaymentModule
  module V1
    class PaytmPaymentService < BaseModule::V1::BaseService

      PAYTM_PAYMENT_RESPONSE_DECORATOR = PaymentModule::V1::PaytmPaymentResponseDecorator
      PAYTM_PAYMENT_UTILS = PaymentModule::V1::PaytmPaymentUtils
      TRANSACTION_MODEL = TransactionsModule::V1::Transaction
      TRANSACTIONS_DAO = TransactionsModule::V1::TransactionsDao
      USER_SERVICE = UsersModule::V1::UserService
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      TRANSACTION_STATUS = TransactionsModule::V1::TransactionStatus
      PAYMENT_TYPES = TransactionsModule::V1::PaymentTypes
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      SENTRY_LOGGER = CommonModule::V1::Logger
      TRANSACTIONS_RESPONSE_DECORATOR = TransactionsModule::V1::TransactionsResponseDecorator
      CONTENT_UTIL = CommonModule::V1::Content


      def initialize(params)
        @params = params
        @user_service = USER_SERVICE.new(@params)
        @transactions_dao = TRANSACTIONS_DAO.new
        # @paytm_payment_utils_class = PAYTM_PAYMENT_UTILS.new
        @request_type = APP_CONFIG['config']['PAYTM_REQUEST_TYPE']
        @mid = APP_CONFIG['config']['PAYTM_MID']
        @merchant_key = APP_CONFIG['config']['PAYTM_MERCHANT_KEY']
        @industry_type_id = APP_CONFIG['config']['PAYTM_INDUSTRY_TYPE_ID']
        @website = APP_CONFIG['config']['PAYTM_WEBSITE']
        @app = APP_CONFIG['config']['PAYTM_APP']
        @login_theme = APP_CONFIG['config']['PAYTM_LOGIN_THEME']
        @callback_url = APP_CONFIG['config']['PAYTM_CALLBACK_URL']
        @checksum_url = APP_CONFIG['config']['PAYTM_CHECKSUM_URL']
        @request_url = APP_CONFIG['config']['PAYTM_REQUEST_URL']
        @theme = APP_CONFIG['config']['PAYTM_THEME']
        @promo_camp_id = APP_CONFIG['config']['PROMO_CAMP_ID']
        # double check to make sure in production orignal amount should be taken
        @max_transaction_amount = APP_CONFIG['config']['MAX_TRANSACTION_AMOUNT'] if Rails.env != 'production'
        @handler_internal_url = APP_CONFIG['config']['PAYTM_HANDLER_INTERNAL_URL']
        @currency = "INR";
      end

      #
      # Function to initiate the transaction with PayTm
      #
      def initiate_transaction(request)
        begin
          user = @user_service.get_user_from_user_id(USER_SESSION[:user_id])
          payment = get_payment(request[:payment_id], request[:payment_type])
          transaction_args = create_transaction_args(payment, request[:payment_type], user)
          transaction = @transactions_dao.create_transaction(transaction_args)
          # Log Transaction initiation just before returning final response
          ApplicationLogger.debug('Transaction initiated, id: ' + transaction.id.to_s + ' for payment id: ' + payment.id.to_s)
          return PAYTM_PAYMENT_RESPONSE_DECORATOR.create_ok_response({
              request_type: @request_type,
              mid: @mid,
              transaction_id: transaction.id.to_s,
              cust_id: transaction_args[:user_id],
              txn_amount: transaction_args[:amount],
              channel_id: USER_SESSION[:platform_type] == 'ANDROID' ? 'WAP' : 'WEB',
              industry_type_id: @industry_type_id,
              website:  USER_SESSION[:platform_type] == 'ANDROID' ? @app : @website,
              mobile_no: user.phone_number,
              email_id: transaction_args[:user_email_id],
              verified_by: 'MOBILE_NO',
              is_user_verified: @user_service.user_is_verified?(user) ? 'YES' : 'NO',
              login_theme: @login_theme,
              theme: @theme,
              checksum_url: @checksum_url,
              callback_url: @callback_url,
              request_url: @request_url,
              payment_status: payment.status
            })
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return CITRUS_PAYMENT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          return CITRUS_PAYMENT_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      #
      # Service function to return the Checksum generated for the transaction
      #
      def get_checksum(params)
        return { payt_STATUS: -1 } if params.blank? && params[:ORDER_ID].blank?
        keys = params.keys
        paytmHASH = Hash.new
        keys.each do |k|
          if ! params[k].empty?
            paytmHASH[k] = params[k]
          end
        end
        order_id = paytmHASH["ORDER_ID"]
        checksum = PaymentModule::V1::PaytmChecksumTool.new.get_checksum_hash(paytmHASH).gsub("\n",'')
        begin
          transaction = @transactions_dao.get_transaction_by_id(order_id)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return { payt_STATUS: -1 }
        end
        transaction = @transactions_dao.update_transaction({ signature: checksum }, transaction)
        return {
          CHECKSUMHASH: checksum,
          ORDER_ID: order_id,
          payt_STATUS: 1
        }
      end

      #
      # Function to validate the checksum after the transaction has passed through the payment gateway
      #
      _ExceptionHandler_
      def validate_checksum(params)
        platform = params["platform"].present? ? params["platform"].upcase : ''
        params.delete("platform")
        paytmHASH = Hash.new
        keys = params.keys
        keys.each do |k|
          paytmHASH[k] = params[k]
        end
        paytmHASH = PaymentModule::V1::PaytmChecksumTool.new.get_checksum_verified_array(paytmHASH)
        if platform == "WEB"
          if paytmHASH["IS_CHECKSUM_VALID"] == "Y"
            return {
              platform: platform,
              order_id: params["ORDERID"],
              status: TransactionsModule::V1::TransactionStatus::SUCCESS
            }
          else
            return {
              platform: platform,
              order_id: params["ORDERID"],
              status: TransactionsModule::V1::TransactionStatus::FAILED
            }
          end
        end
        return {
          platform: 'ANDROID',
          paytm_response: paytmHASH.to_s
        }
      end

      #
      # Function to update the transaction with the final status and place or fail the order accordingly
      #
      def update_transaction(request)
        ApplicationLogger.info('UpdateTransaction request: ' + request.to_s)
        paytm_response = JSON.parse(get_paytm_transaction_status(request[:id]))
        paytm_response_code = paytm_response['RESPCODE']
        transaction = @transactions_dao.get_transaction_by_id(request[:id])
        payment_handler = PAYMENT_TYPES.get_payment_handler_from_payment_type(transaction.payment_type)
        payment_handler_class = payment_handler.new(@params)
        request = request.merge(paytm: paytm_response.to_json)
        transaction_status = request[:status].to_i
        transaction_status = get_final_transaction_status(transaction_status, paytm_response_code)
        begin
          ApplicationLogger.debug('Transaction completed: ' + transaction.id.to_s + ' for type: ' + transaction.payment_type.to_s)
          payment_handler_class.handle_payment(transaction_status, { payment_id: transaction.payment_id, user_id: transaction.user_id })
          @transactions_dao.update_transaction({status: transaction_status, transaction_data: request}, transaction)
        rescue => e
          SENTRY_LOGGER.log_exception(e)
          ApplicationLogger.debug('Unable to update the payment for  transaction id ' + transaction.id.to_s + ' , ' + e.message)
        end
        return TRANSACTIONS_RESPONSE_DECORATOR.create_transaction_status_response(transaction_status)
      end

      def get_payment(payment_id, payment_type)
        payment_dao = PAYMENT_TYPES.get_payment_dao_from_payment_type(payment_type)
        payment_dao_class = payment_dao.new
        return payment_dao_class.get_payment_from_id(payment_id)
      end

      #
      # Function to fetch the latest TXN status from Paytm for a payment
      #
      def get_paytm_transaction_status(order_id)
        status_request = @handler_internal_url + "TXNSTATUS?JsonData={'MID':'#{@mid}','ORDERID':'#{order_id}'}"
        paytm_response = HTTParty.get(status_request)
        return paytm_response
      end

      def create_transaction_args(payment, payment_type, user)
        user_emails = USER_SERVICE_HELPER.get_primary_mail(user)
        primary_email = user_emails.email_id
        
        # TODO: We should call handler.get_total_payable_amount(payment_id) 
        amount = payment.net_total.to_f

        if @max_transaction_amount.present?
          amount = @max_transaction_amount.to_f
        end
        
        return {
          payment_id: payment.id,
          payment_type: payment_type,
          amount: amount,
          user_email_id: primary_email,
          user_id: user.id,
          currency: @currency,
          gateway: TransactionsModule::V1::GatewayType::PAYTM
        }
      end

      #
      # Function to double check the txn status with the latest txn status from the Paytm
      #
      def get_final_transaction_status(transaction_status, paytm_response_code)
        if transaction_status != TransactionsModule::V1::TransactionStatus::SUCCESS
          if paytm_response_code == PaymentModule::V1::PaytmResponseCodesUtils::SUCCESS
            transaction_status = TransactionsModule::V1::TransactionStatus::SUCCESS
            ApplicationLogger.warn('Transaction status ' + transaction_status.to_s + ' failed/cancelled, while paytm response code ' + paytm_response_code.to_s + ' is success')
          end
        else
          if paytm_response_code != PaymentModule::V1::PaytmResponseCodesUtils::SUCCESS
            transaction_status = TransactionsModule::V1::TransactionStatus::FAILED
            ApplicationLogger.warn('Transaction status ' + transaction_status.to_s + ' success, while paytm response code ' + paytm_response_code.to_s + ' is not success')
          end
        end
        return transaction_status
      end
    end
  end
end
