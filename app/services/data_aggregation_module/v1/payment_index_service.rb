#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This class is responsible to create and maniooulate payment-index
    #
    class PaymentIndexService < DataAggregationModule::V1::IndexInterface

      PAYMENT_DATA_LOADER = DataAggregationModule::V1::DataLoaders::V1::PaymentDataLoader
      NUMBER_OF_DAYS_DATA = 1

      def initialize(params = '')
        @params = params
        @repository = PAYMENT_REPOSITORY
        @payment_data_loader = PAYMENT_DATA_LOADER.new
      end

      # 
      # This function update payment-index with loaded data, it index
      # all data if payment-index has 0 document else update the
      # payment-repositroy
      #
      def update_index
        if index_exists?
          payment_search_models = @payment_data_loader.load_data(NUMBER_OF_DAYS_DATA)
          index_data(payment_search_models)
          ApplicationLogger.debug('Successfully updated payment-index with ' + payment_search_models.length.to_s + ' models')
        else
          re_initialize_index
        end
      end

      # 
      # This function delete payment-index and re-create
      # with all the payment data
      #
      def re_initialize_index
        delete_previous_index
        payment_search_models = @payment_data_loader.load_data(0, true)
        index_data(payment_search_models)
        ApplicationLogger.debug('Successfully re_initialize payment-index with ' + payment_search_models.length.to_s + ' models')
      end

    end
  end
end
