#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # Module responsible for loading data
    #
    module DataLoaders
      #
      # Version1 for data loaders 
      #
      module V1
        #
        # This class loads all payment data and return in api_log_search_model 
        #
        class ApiLogDataLoader


          def initialize
            @api_log_search_model = DataAggregationModule::V1::ApiLogSearchModel
            @activity_log_model = LoggingModule::V1::ActivityLog
          end

          # 
          # This will load payment data, if all = true then load all the data
          # else load data which has updated_at > x_days
          #
          # @param x_days [Integer] this specify we need data after (today - x_days) days
          # @param all = false [Boolean] if all is true then return all the data
          # 
          # @return [Array] Array of api_log_search_module
          #
          def load_data(x_days, all = false)

            api_log_search_models = []

            if all == false
              today_date = DateTime.now
              start_of_day_time = today_date.beginning_of_day
              from_time = start_of_day_time - x_days.day

              api_logs = get_updated_api_logs(from_time)
            else
              api_logs = @activity_log_model.all
            end
            
            return [] if api_logs.blank?

            api_logs.each do |api_log|
              attributes = JSON.parse(api_log.to_json)
              attributes["id"] = api_log.id.to_s
              attributes.delete("_id")
              api_log_search_models.push(get_api_log_search_model(attributes))
            end

            return api_log_search_models
          end

          ##################################
          ############Private###############
          ##################################

          # 
          # This function returns all the orders which are updated or
          # there carts are updated
          #
          # @param from_time [Time] from time
          # 
          # @return orders [Array] array of order
          #
          def get_updated_api_logs(from_time)
            @activity_log_model.where(:updated_at.gte => from_time)
          end

          #
          # map order to order model object
          # 
          # @param row [Hash] containing order related information
          # 
          # @return [Object] api_log_search_model object
          # 
          def get_api_log_search_model(attributes)
            attributes = attributes.symbolize_keys
            if attributes[:api_spawn_time].blank?
              attributes[:api_spawn_time] = attributes[:created_at]
            end
            attributes[:api_spawn_time] =  DateTime.parse(attributes[:api_spawn_time].to_s)
            @api_log_search_model.new(attributes) 
          end
        end
      end
    end
  end
end
