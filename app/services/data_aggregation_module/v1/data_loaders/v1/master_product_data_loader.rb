#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
     #
    # Module responsible for loading data
    #
    module DataLoaders
      #
      # Version1 for data loaders 
      #
      module V1
        #
        # This class loads all master_product data and return in master_product_search_model 
        #
        class MasterProductDataLoader

          def initialize
            @brand_pack_model = MasterProductModule::V1::BrandPack
            @master_product_search_model = DataAggregationModule::V1::MasterProductSearchModel
            @brand_pack_dao = MasterProductModule::V1::BrandPackDao
          end

          # 
          # This will load master_product data, if all = true then load all the data
          # else load data which has updated_at > x_days
          #
          # @param x_days [Integer] this specify we need data after (today - x_days) days
          # @param all = false [Boolean] if all is true then return all the data
          # 
          # @return [Array] Array of master_product_search_module
          #
          def load_data(x_days, all = false)

            if all == false
              today_date = DateTime.now
              start_of_day_time = today_date.beginning_of_day
              from_time = start_of_day_time - x_days.day

              brand_packs = @brand_pack_model.where('updated_at > ?', from_time)
            else
              brand_packs = @brand_pack_model.all
            end

            brand_packs = @brand_pack_dao.get_master_products_data(brand_packs)

            master_product_search_models = []

            brand_packs.each do |brand_pack|

              product = brand_pack.product
              sub_brand = product.sub_brand
              brand = sub_brand.brand
              company = brand.company
              sub_category = brand_pack.sub_category
              category = sub_category.parent_category
              department = category.department

              row = [ brand_pack.id, brand_pack.sku, brand_pack.sku_size, brand_pack.unit, brand_pack.brand_pack_code, brand_pack.article_code,
                      brand_pack.retailer_pack, product.name, sub_brand.name, brand.name, company.name, sub_category.label, category.label,
                      department.label, brand_pack.mrp, brand_pack.updated_at ]

              master_product_search_models.push(get_master_product_search_model(row))
            end

            return master_product_search_models
          end

          ##################################
          ############Private###############
          ##################################

          #
          # map row to master_product model object
          # 
          # @param row [Hash] containing master_product related information
          # 
          # @return [MasterProductModel] master_product_search_model object
          # 
          def get_master_product_search_model(row)
            @master_product_search_model.new({
               id: row[0].to_i,
               sku: row[1].to_s,
               sku_size: row[2].to_s,
               unit: row[3].to_s,
               brand_pack_code: row[4].to_s,
               article_code: row[5].to_s,
               retailer_pack: row[6].to_s,
               product: row[7].to_s,
               sub_brand: row[8].to_s,
               brand: row[9].to_s,
               company: row[10].to_s,
               sub_category: row[11].to_s,
               category: row[12].to_s,
               department: row[13].to_s,
               mrp: row[14].to_i,
               updated_at: row[15]
              }) 
          end
        end
      end
    end
  end
end
