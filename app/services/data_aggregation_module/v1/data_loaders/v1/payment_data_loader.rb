#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # Module responsible for loading data
    #
    module DataLoaders
      #
      # Version1 for data loaders
      #
      module V1
        #
        # This class loads all payment data and return in payment_search_model
        #
        class PaymentDataLoader

          GATEWAY_TYPE = TransactionsModule::V1::GatewayType
          PAYMENT_TYPE = MarketplaceOrdersModule::V1::PaymentType

          def initialize
            @order_model = MarketplaceOrdersModule::V1::Order
            @order_product_service_instance = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService.new
            @order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
            @cart_model = MarketplaceOrdersModule::V1::Cart
            @order_helper = MarketplaceOrdersModule::V1::OrderHelper
            @payment_search_model = DataAggregationModule::V1::PaymentSearchModel
            @payment_dao = MarketplaceOrdersModule::V1::PaymentDao.new
            @payment_helper = MarketplaceOrdersModule::V1::PaymentHelper
            @order_dao = MarketplaceOrdersModule::V1::OrderDao
            @transaction_dao = TransactionsModule::V1::TransactionsDao.new
          end

          #
          # This will load payment data, if all = true then load all the data
          # else load data which has updated_at > x_days
          #
          # @param x_days [Integer] this specify we need data after (today - x_days) days
          # @param all = false [Boolean] if all is true then return all the data
          #
          # @return [Array] Array of payment_search_module
          #
          def load_data(x_days, all = false)

            payment_search_models = []

            if all == false
              today_date = DateTime.now
              start_of_day_time = today_date.beginning_of_day
              from_time = start_of_day_time - x_days.day

              orders = get_updated_orders(from_time)
            else
              orders = @order_dao.get_completed_orders
            end

            orders = @order_dao.get_order_and_user_data(orders)

            orders.each do |order|

              cart = order.cart
              user = order.user
              order_date = @order_helper.get_order_date(order)
              order_date = Time.at(order_date.to_i)

              membership_details = get_membership_order_details(order)

              payments = @payment_dao.get_active_payments_for_order(order)

              last_successful_payment = true

              payments.each do |payment|

                payment_display_labels = @payment_helper.get_payment_display_labels(payment)
                transaction_params = get_transaction_params(payment)

                order_area = order.address.area

                row = [ payment.id, order.order_id, order_date, user.id, user.phone_number, user.first_name,
                        order_area.pincode, cart.total_mrp, cart.cart_savings, cart.cart_total,
                        payment.delivery_charges, payment.billing_amount, payment.grand_total, payment.discount,
                        payment.net_total, payment.total_savings, payment_display_labels[:mode_display_name],
                        payment_display_labels[:status_display_name], last_successful_payment,
                        transaction_params[:gateway], payment.ample_credit_amount, transaction_params[:amount],
                        transaction_params[:payment_type],
                        membership_details[:membership_label], membership_details[:membership_amount],
                        membership_details[:membership_status] ]

                payment_search_models.push(get_payment_search_model(row))

                last_successful_payment = false
              end
            end

            return payment_search_models
          end

          ##################################
          ############Private###############
          ##################################

          #
          # This function returns all the orders which are updated or
          # there carts are updated
          #
          # @param from_time [Time] from time
          #
          # @return orders [Array] array of order
          #
          def get_updated_orders(from_time)
            orders =  @order_model.changed_after_time(from_time)
            return @order_dao.get_completed_orders(orders)
          end

          #
          # This function returns transaction params for the payment
          #
          # @param payment [Model] payment model
          #
          # @return [Hash] transaction hash
          #
          def get_transaction_params(payment)

            transaction = @transaction_dao.get_successfull_transaction_for_payment_id(payment.id)

            return { gateway: '' } if transaction.blank?

            return {
              gateway: GATEWAY_TYPE.get_display_gateway_by_type(transaction.gateway),
              amount: transaction.amount,
              payment_type: PAYMENT_TYPE.get_key_by_id(transaction.payment_type)
            }
          end

          def get_membership_order_details(order)
            membership_label = 'NO MEMBERSHIP'
            membership_amount = 0
            membership_status = 'NA'
            return {} if order.blank?
            if order.third_party_orders.present?
              third_party_order_dao = MarketplaceOrdersModule::V1::ThirdPartyOrderDao.instance
              membership_orders = third_party_order_dao.get_membership_orders_associated_with_ample_order(order.id)
              membership_order = membership_orders.first if membership_orders.present?
              if membership_order.present?
                membership_label = membership_order.label
                membership_amount = membership_order.amount
                membership_status = MarketplaceOrdersModule::V1::ModelStates::V1::ThirdPartyOrderStates.get_third_party_order_state_display_name(membership_order.status)
              end
            else
              final_payment = order.payment # Will make a query to DB
              convenience_charges = final_payment.convenience_charges
              if convenience_charges > 0
                membership_label = 'PAY_PER_ORDER'
                membership_amount = convenience_charges
                membership_status = 'PLACED'
              end
            end

            # Return hash of all membership details
            return {
              membership_label: membership_label,
              membership_amount: membership_amount,
              membership_status: membership_status
            }

          end
          #
          # map order to order model object
          #
          # @param row [Hash] containing order related information
          #
          # @return [Object] payment_search_model object
          #
          def get_payment_search_model(row)
            @payment_search_model.new({
               id: row[0].to_i,
               order_id: row[1].to_s,
               order_time: DateTime.parse(row[2].to_s),
               user_id: row[3].to_i,
               phone_number: row[4].to_i,
               name: row[5].to_s,
               pincode: row[6].to_i,
               cart_mrp: row[7].to_f,
               cart_savings: row[8].to_f,
               cart_total: row[9].to_f,
               payment_delivery_charge: row[10].to_f,
               payment_billing_amount: row[11].to_f,
               payment_grand_total: row[12].to_f,
               payment_discount: row[13].to_f,
               payment_net_total: row[14].to_f,
               payment_total_savings: row[15].to_f,
               payment_mode: row[16].to_s,
               payment_status: row[17].to_s,
               last_successful_payment: row[18],
               gateway: row[19].to_s,
               ample_credit_amount: row[20].to_f,
               transaction_amount: row[21].to_f,
               payment_type: row[22].to_s,
               membership_label: row[23].to_s,
               membership_amount: row[24].to_f,
               membership_status: row[25].to_s
              })
          end
        end
      end
    end
  end
end
