#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # Module responsible for loading data
    #
    module DataLoaders
      #
      # Version1 for data loaders
      #
      module V1
        #
        # This class loads all payment data and return in user_search_model
        #
        class UserDataLoader


          def initialize
            @order_model = MarketplaceOrdersModule::V1::Order
            @order_product_service_instance = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService.new
            @order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
            @user_search_model = DataAggregationModule::V1::UserSearchModel
            @user_model = UsersModule::V1::User
            @membership_dao = UsersModule::V1::MembershipDao.new({})
            @membership_model = UsersModule::V1::Membership
            @user_service_helper = UsersModule::V1::UserServiceHelper
            @payment_dao = MarketplaceOrdersModule::V1::PaymentDao.new
            @order_dao = MarketplaceOrdersModule::V1::OrderDao
          end

          #
          # This will load payment data, if all = true then load all the data
          # else load data which has updated_at > x_days
          #
          # @param x_days [Integer] this specify we need data after (today - x_days) days
          # @param all = false [Boolean] if all is true then return all the data
          #
          # @return [Array] Array of user_search_module
          #
          def load_data(x_days, all = false)

            user_search_models = []

            if all == false
              today_date = DateTime.now
              start_of_day_time = today_date.beginning_of_day
              from_time = start_of_day_time - x_days.day

              users = get_updated_users(from_time)
            else
              users = @user_model.all
            end

            users.each do |user|
              orders =  user.orders
              completed_orders = []
              completed_orders = @order_dao.get_completed_orders(orders) if orders.present?
              used_ample_credits = get_total_user_ample_credits_of_user(completed_orders)
              number_of_complete_orders = completed_orders.count
              membership = @membership_dao.get_only_active_membership(user)
              active_plan = ''
              membership_expire = Time.zone.now - 100.year
              active_plan = membership.plan.name if membership.present?
              membership_expire = membership.expires_at if membership.present?
              used_referral = user.referred_by.present? ? true:false
              primary_email = @user_service_helper.get_primary_mail(user)

              if primary_email.blank?
                primary_email_id = ''
              else
                primary_email_id = primary_email.email_id
              end
              row = [ user.id, user.first_name, user.phone_number, number_of_complete_orders, active_plan, membership_expire,
                      user.created_at, primary_email_id, used_referral, used_ample_credits, user.referred_by ]

              user_search_models.push(get_user_search_model(row))
            end

            return user_search_models
          end

          ##################################
          ############Private###############
          ##################################

          #
          # This function returns all the orders which are updated or
          # there carts are updated
          #
          # @param from_time [Time] from time
          #
          # @return orders [Array] array of order
          #
          def get_updated_users(from_time)

            return @user_model.changed_after_time(from_time)
          end

          def get_total_user_ample_credits_of_user(orders)

            return 0.0 if orders.blank?

            total_ample_credits = 0.0

            orders.each do |order|
              payment = @payment_dao.get_orders_payment_as_per_its_mode(order)
              total_ample_credits += payment.ample_credit_amount
            end

            return total_ample_credits
          end
          #
          # map order to order model object
          #
          # @param row [Hash] containing order related information
          #
          # @return [Object] user_search_model object
          #
          def get_user_search_model(row)
            @user_search_model.new({
               id: row[0].to_i,
               name: row[1].to_s,
               phone_number: row[2].to_i,
               completed_orders: row[3].to_i,
               active_plan: row[4].to_s,
               membership_expire_date: row[5],
               registration_date: DateTime.parse(row[6].to_s),
               primary_email: row[7].to_s,
               used_referral: row[8],
               used_ample_credits: row[9].to_f,
               referred_by: row[10]
              })
          end
        end
      end
    end
  end
end
