#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    #
    # This class is responsible to create and maniooulate master_product-index
    #
    class MasterProductIndexService < DataAggregationModule::V1::IndexInterface

      MASTER_PRODUCT_DATA_LOADER = DataAggregationModule::V1::DataLoaders::V1::MasterProductDataLoader
      NUMBER_OF_DAYS_DATA = 1

      def initialize(params = '')
        @params = params
        @repository = MASTER_PRODUCT_REPOSITORY
        @master_product_data_loader = MASTER_PRODUCT_DATA_LOADER.new
      end

      # 
      # This function update master_product-index with loaded data, it index
      # all data if master_product-index has 0 document else update the
      # master_product-repositroy
      #
      def update_index
        if index_exists?
          master_product_search_models = @master_product_data_loader.load_data(NUMBER_OF_DAYS_DATA)
          index_data(master_product_search_models)
          ApplicationLogger.debug('Successfully updated master_product-index with ' + master_product_search_models.length.to_s + ' models')
        else
          re_initialize_index
        end
      end

      # 
      # This function delete master_product-index and re-create
      # with all the master_product data
      #
      def re_initialize_index
        delete_previous_index
        master_product_search_models = @master_product_data_loader.load_data(0, true)
        index_data(master_product_search_models)
        ApplicationLogger.debug('Successfully re_initialize master_product-index with ' + master_product_search_models.length.to_s + ' models')
      end

    end
  end
end
