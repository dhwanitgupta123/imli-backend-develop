#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Logic layer which is responsible to create marketplace_selling_pack index
    # and retrieve results
    #
    module UserDetailsIndexService
      class << self

        # This is maximum n for search api of elasticsearch
        MAX_RESULTS = 10
        CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
        PAGINATION_UTIL = CommonModule::V1::Pagination

        #
        # This function checks if marketplace-selling-pack-index already present or not
        # if not then retrieve data from MarketplaceSellingPack table and index the data
        #
        # but if force == 'true' then it will always re-index the data
        #
        # @param force [String] to specify force re-indexing
        #
        def initialize_index(force = false)
          # Fetch all DB models
          # Create corresponding search models
          # Repository.create_index
          user_details_search_client = SearchModule::V1::UserDetailsSearchClient.instance
          return false if !user_details_search_client.repository_exists?
          return false if (force != true) && user_details_search_client.index_exists?

          user_details_search_client.delete_previous_index

          pagination_params = {
            per_page: PAGINATION_UTIL::INFINITE_PER_PAGE
          }

          user_dao = UsersModule::V1::UserDao.instance
          all_users = user_dao.get_non_deleted_users(pagination_params)[:elements]

          all_users.each do |user|
            user_details = get_user_search_model(user)
            save(user_details)
          end
        end

        #
        # Get user details search hash
        #
        # @param user [Object] [User model]
        #
        # @return [Object] [UserDetailSearchModel object containing user details as attributes]
        #
        def get_user_search_model(user)
          # Create search model from db model
          user_dao = UsersModule::V1::UserDao.instance
          orders = modify_orders_to_string(user_dao.get_user_orders(user))
          name = user_dao.get_full_name(user)
          emails_hash = append_emails_to_string(user_dao.get_all_email_ids(user))

          user_details = SearchModule::V1::UserDetailsSearchModel.new({
            id: user.id,
            name: name,
            phone_number: user.phone_number,
            emails: emails_hash[:modified],
            display_emails: emails_hash[:original],
            orders: orders
          })
        end

        #
        # save the document in repository
        #
        def save(user_details)
          user_details_search_client = SearchModule::V1::UserDetailsSearchClient.instance
          user_details_search_client.save(user_details)
        end

        #
        # Get Results by processing the query, building it and then make a search call over ElasticSearchRepo
        #   * PreProcessors.apply
        #   * QueryBuilder.build
        #   * PostProcessors.apply
        #   * return results
        #
        # @param query [String] query string
        # @param n = 10 [Integer] top n results
        #
        # @return [Array] Array of marketplace_selling_pack object
        #
        def get_results(query, n = MAX_RESULTS)
          # PreProcessors.apply
          # QueryBuilder.build
          # PostProcessors.apply
          # return results
          pre_processor = SearchModule::V1::UserDetailsPreProcessor
          post_processor = SearchModule::V1::UserDetailsPostProcessor
          query_builder = SearchModule::V1::UserDetailsQueryHelper

          query = pre_processor.process(query)
          query = query_builder.build(query)
          results = search(query, n)
          results = post_processor.process(results)
          return results
        end

        #
        # This function add the mpsp in mpsp_index
        #
        # @param marketplace_selling_pack [Model] model object of mpsp
        #
        def add_document(user)
          # Fetch document by model.id
          # Create SearchModel
          # Repository.save
          user_details_search_client = SearchModule::V1::UserDetailsSearchClient.instance
          return unless user_details_search_client.get_document_by_id(user.id) == false

          user_details = get_user_search_model(user)
          begin
            save(user_details)
          rescue => e
            raise CUSTOM_ERRORS_UTIL::RunTimeError.new(CONTENT_UTIL::UNABLE_TO_ADD_DOCUMENT)
          end
        end

        #
        # This function update the index with given mpsp
        #
        # @param user [MOdel] updated model of mpsp
        #
        # @error [RunTimeError] if document with given not found
        #
        def update_document(user)
          # Create SearchModel
          # Repository.update
          user_details_search_client = SearchModule::V1::UserDetailsSearchClient.instance
          user_details = get_user_search_model(user)
          begin
            user_details_search_client.update(user_details)
          rescue => e
            raise CUSTOM_ERRORS_UTIL::RunTimeError.new(CONTENT_UTIL::MISSING_DOCUMENT)
          end
        end

        #
        # This will delete the index if it exists
        #
        def delete_index
          # Delete index
        end

        def add_index(user_id)
          user = find_user(user_id)
          add_document(user) if user.present?
        end

        def update_index(user_id)
          user = find_user(user_id)
          update_document(user) if user.present?
        end

        def find_user(user_id)
          return nil if user_id.blank?
          user_data_service = UsersModule::V1::UserDataService.instance
          begin
            user_data_service.get_user_from_user_id(user_id)
          rescue => e
            return nil
          end
        end

        #
        # Return document by given id
        #
        # @param id [Integer] id of mpsp
        #
        # @return [Document] this return document if it succeed to find
        # @return [False] if document not found in the repository
        #
        def get_document_by_id(id)
          # Repository.find
          user_details_search_client = SearchModule::V1::UserDetailsSearchClient.instance
          user_details_search_client.get_document_by_id(id)
        end

        #
        # Search query in repository
        #
        def search(query, max_results)
          # Repository.search
          user_details_search_client = SearchModule::V1::UserDetailsSearchClient.instance
          user_details_search_client.search(query: query, max_results: max_results).to_a
        end

        ###############################
        #       Private Functions     #
        ###############################

        private

        #
        # Modify orders array to string separated by spaces
        # Also, modify the order ids by removing their hiphens and converting it to downcase
        #
        # @param orders_array [Array] [Array of orders]
        #
        # @return [String] [Modified string]
        #
        def modify_orders_to_string(orders_array)
          str = ''
          orders_array.each do |order|
            str += downcase(remove_hiphens(order.order_id)) + ' '
          end
          str
        end

        #
        # Append Emails array to a string separated by space
        # Also, create a modified version of this string after removing dots and convert it
        # to downcase
        #
        # @param emails_array [Array] [Array of email ids]
        #
        # @return [Hash] [Hash of original and modified strings]
        #
        def append_emails_to_string(emails_array)
          original_string = ''
          modified_string = ''
          emails_array.each do |email|
            original_string += email + ' '
            modified_string += downcase(remove_dots(email)) + ' '
          end
          return {original: original_string, modified: modified_string}
        end

        def remove_hiphens(str)
          CommonModule::V1::StringUtil.remove_hiphens(str)
        end

        def remove_dots(str)
          CommonModule::V1::StringUtil.remove_dots(str)
        end

        def downcase(str)
          CommonModule::V1::StringUtil.downcase(str)
        end

      end # End of class
    end
  end
end
