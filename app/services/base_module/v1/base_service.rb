module BaseModule
  module V1
    class BaseService
      extend AnnotationsModule::V1::TransactionalBlockAnnotations
      extend AnnotationsModule::V1::ExceptionHandlerAnnotations

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params = '')
        self.extend AnnotationsModule::V1::ExceptionHandlerAnnotations
        self.extend AnnotationsModule::V1::TransactionalBlockAnnotations
        @custom_error_util = CommonModule::V1::CustomErrors
      end
    end
  end
end
