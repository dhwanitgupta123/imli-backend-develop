#
# Module to handle functionalities related to file upload to S3
#
module FileServiceModule
  #
  # Version1 for file service module
  #
  module V1
    #
    # This class is responsible to upload data to S3
    #
    class FileService < BaseModule::V1::BaseService
      UPLOAD_DATA_WORKER = FileServiceModule::V1::UploadDataWorker
      UPLOAD_FILE_WORKER = FileServiceModule::V1::UploadFileWorker
      FILE_SERVICE_UTIL = FileServiceModule::V1::FileServiceUtil
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CACHE_UTIL = CommonModule::V1::Cache

      def initialize(params)
        @params = params
        @default_options = {
          acl: CACHE_UTIL.read('ACCESS_CONTROL_LISTS')
        }
      end

      #
      # Upload the data to S3 asynchronously, this function gets the object with given key
      # from the S3_BUCKET and create the unique url
      #
      # @param data [String] String to write
      # @param key [key] key of the object
      # @param options [Hash] hash contains other options like
      #         :content_length - detemine size of data
      #         :single_request - this method will always upload the data in a single request
      #         for other options visit this url
      #         http://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#write-instance_method
      #
      # @return [String] public url of the file uploaded
      #
      def upload_data_to_s3_async(data, key, options = {})
        options = @default_options if options.blank?
        file_service_util = FILE_SERVICE_UTIL.new
        s3_key = file_service_util.get_s3_key(key)
        s3_key = S3_PRIVATE_FOLDER + '/' + s3_key if options[:acl] == 'private'
        s3_object = S3_BUCKET.objects[s3_key]
        UPLOAD_DATA_WORKER.perform_async({ data: data, key: s3_key, options: options })
        return {url: s3_object.public_url.to_s, path: s3_key}
      end

      #
      # Upload the data to S3 synchronously, this function gets the object with given key
      # from the S3_BUCKET and create or override the content
      #
      # @param data [String] String to write
      # @param key [key] key of the object
      # @param options [Hash] hash contains other options  like
      #         :content_length - detemine size of data
      #         :single_request - this method will always upload the data in a single request
      #         for other options visit this url
      #         http://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#write-instance_method
      #
      # @return [String] public url of the file uploaded
      #
      def upload_data_to_s3(data, key, options = {})
        file_service_util = FILE_SERVICE_UTIL.new
        s3_key = file_service_util.get_s3_key(key)
        s3_key = S3_PRIVATE_FOLDER + '/' + s3_key if options[:acl] == 'private'
        options = @default_options if options.blank?
        s3_url = file_service_util.upload_data({ data: data, key: s3_key, options: options })
        return {url: s3_url, path: s3_key}
      end

      #
      # Upload the file to S3 asynchronously, this function gets the object with given key
      # from the S3_BUCKET and create or override the content
      #
      # @param data [String] file_path
      # @param key [key] key of the object
      # @param options [Hash] hash contains other options  like
      #         :content_length - detemine size of data
      #         :single_request - this method will always upload the data in a single request
      #         for other options visit this url
      #         http://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#write-instance_method
      #
      # @return [String] public url of the file uploaded
      #
      # @error [FileNotExists] if given file_path not exists
      #
      def upload_file_to_s3_async(file_path, key, options = {})
        raise CUSTOM_ERROR_UTIL::FileNotFound.new('file does not exists, file_path: ' + file_path) unless File.file?(file_path)
        options = @default_options if options.blank?
        file_service_util = FILE_SERVICE_UTIL.new
        s3_key = file_service_util.get_s3_key(key)
        s3_key = S3_PRIVATE_FOLDER + '/' + s3_key if options[:acl] == 'private'
        s3_object = S3_BUCKET.objects[s3_key]
        UPLOAD_FILE_WORKER.perform_async({ key: s3_key, file_path: file_path, options: options })
        return {url: s3_object.public_url.to_s, path: s3_key}
      end

      #
      # Upload the file to S3 synchronously, this function gets the object with given key
      # from the S3_BUCKET and create or override the content
      #
      # @param data [String] file_path
      # @param key [key] key of the object
      # @param options [Hash] hash contains other options like
      #         :content_length - detemine size of data
      #         :single_request - this method will always upload the data in a single request
      #         for other options visit this url
      #         http://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#write-instance_method
      #
      # @return [String] public url of the file uploaded
      #
      # @error [FileNotExists] if given file_path not exists
      # @error [S3FailedToUpload] if it fails to upload file
      #
      def upload_file_to_s3(file_path, key, options = {})
        raise CUSTOM_ERROR_UTIL::FileNotFound.new('file does not exist, file_path: ' + file_path) unless File.file?(file_path)
        options = @default_options if options.blank?
        file_service_util = FILE_SERVICE_UTIL.new
        s3_key = file_service_util.get_s3_key(key)
        s3_key = S3_PRIVATE_FOLDER + '/' + s3_key if options[:acl] == 'private'
        s3_url = file_service_util.upload_file({ key: s3_key, file_path: file_path, options: options })
        return {url: s3_url, path: s3_key}
      end

      #
      # Update the Uploaded the file to S3 synchronously, this function gets the object with given key
      # from the S3_BUCKET and override the content
      #
      # @param data [String] input_file_path, absolte path of the file
      # @param key [key] output_file_s3_key, S3 key of the object
      # @param options [Hash] hash contains other options like
      #         :content_length - detemine size of data
      #         :single_request - this method will always upload the data in a single request
      #         for other options visit this url
      #         http://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#write-instance_method
      #
      # @return [String] public url of the file uploaded
      #
      # @error [FileNotExists] if given file_path not exists
      # @error [S3FailedToUpload] if it fails to upload file
      #
      def update_file_to_s3(output_file_s3_key, input_file_path, options = {})
        options = @default_options if options.blank?

        file_service_util = FILE_SERVICE_UTIL.new
        file_service_util.upload_file( {
            key: output_file_s3_key,
            file_path: input_file_path,
            options: options
          })
      end

      #
      # This function generate the signed url of a private s3 object and assign
      # expire time
      #
      # @param key [String] s3 path for a object
      # @param expires [ActiveSupport::Duration] duration for which link should be active, example (5.minutes, 10.hours)
      #
      # @return [String] signed url
      #
      # @error  InsufficientDataError if expire time is not present it is must
      # @error  S3ObjectNotFoundError if s3 object not present corresponding to the key
      #
      def get_signed_s3_url(key, expires)

        if expires.blank? || expires.class != ActiveSupport::Duration
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::INVALID_DURATION)
        end

        s3_object = S3_BUCKET.objects[key]
        raise CUSTOM_ERROR_UTIL::S3ObjectNotFoundError.new(CONTENT_UTIL::INVALID_S3_KEY) if !s3_object.exists?

        return s3_object.url_for(:get, { :expires => expires.from_now, :secure => true }).to_s
      end
    end
  end
end
