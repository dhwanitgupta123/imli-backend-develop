#!/usr/bin/python
import csv
import re
import unicodedata
import urllib2
import sys
from bs4 import BeautifulSoup

#
# This function take input as amazon product page url,
# get html response and parse html to get selling price
# of product
#
def get_amazon_price(amazon_url):
  
  # Fetching html content of url
  try:
    html_doc = urllib2.urlopen(amazon_url.strip(' ')).read()
  except:
    return 'Not a valid url'
  
  # parsing html response
  soup = BeautifulSoup(html_doc, 'html.parser')
 
  prices = soup.find('div', id='price_feature_div')
  if prices is None:
    return 'N/A'

  table = prices.find('table', 'a-lineitem')
  if table is None:
    return 'N/A'
 
  trs = table.find_all('tr')
  price_array = ['NA', 'NA', 'NA']
  count = 0 
  for tr in trs:
    tds = tr.find_all('td')
    if tds is None or len(tds) < 2:
      continue
    tag = tds[0].get_text()
    tag = unicodedata.normalize('NFKD', tag).encode('ascii','ignore')
    price = tds[1].get_text()
    price = unicodedata.normalize('NFKD', price).encode('ascii','ignore')
    # Regex for all the integers or integers seperated by '.'
    price = re.findall(r"\d+.\d+|\d+", price)
    if len(price) > 0:
      price = price[0]
      price_array[count] = price
      count = count + 1

  if count > 2:
    return price_array[1]
  else:
    return price_array[0]

#
# This function take input as big basket product page url,
# get html response and parse html to get selling price
# of product
#
def get_big_basket_price(big_basket_url):

  # Fetching html content of url
  try:
    html_doc = urllib2.urlopen(big_basket_url.strip(' ')).read()
  except:
    return 'Not a valid url'

  # parsing html response
  soup = BeautifulSoup(html_doc, 'html.parser')

  priceDiv = soup.find('div', class_='uiv2-price')

  if priceDiv is None:
    return 'N/A'

  price = priceDiv.get_text()
  price = unicodedata.normalize('NFKD', price).encode('ascii','ignore')
  # Regex for all the integers or integers seperated by '.'
  price = re.findall(r"\d+.\d+|\d+", price)
  
  return price[0]

#
# This function take input as purplle product page url,
# get html response and parse html to get selling price
# of product
#
def get_purplle_price(purplle_url):

  # Fetching html content of url
  try:
    html_doc = urllib2.urlopen(purplle_url.strip(' ')).read()
  except:
    return 'Not a valid url'

  # parsing html response
  soup = BeautifulSoup(html_doc, 'html.parser')

  priceDiv = soup.find('div', class_='price')

  if priceDiv is None:
    return 'N/A'

  price = priceDiv.get_text()
  price = unicodedata.normalize('NFKD', price).encode('ascii','ignore')
  # Regex for all the integers or integers seperated by '.'
  price = re.findall(r"\d+.\d+|\d+", price)
  
  return price[0]

##############################

amazon_price = get_amazon_price(sys.argv[1].strip())
big_basket_price = get_big_basket_price(sys.argv[2].strip())
purplle_price = get_purplle_price(sys.argv[3].strip())

print big_basket_price + '#' + amazon_price + '#' + purplle_price
