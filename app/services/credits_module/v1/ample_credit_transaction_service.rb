#
# Module to handle functionalities related to Ample Credits
#
require 'imli_singleton'
module CreditsModule
  module V1
    class AmpleCreditTransactionService < BaseModule::V1::BaseService
      include ImliSingleton

      def initialize(params = '')
        @params = params
      end

      #
      # This function credit the amount to the user's ample_credit
      #
      # @param args [Hash] consist of user_id and amount to add
      #
      _RunInTransactionBlock_
      def credit_into_ample_credits(args)
        user_id = args[:id]
        amount = args[:amount]

        user = get_user_from_id(user_id)

        ApplicationLogger.info('Request to CREDIT amount ' + amount.to_s + ' for user_id ' + user.id.to_s +
                                 ' by requester_id ' + (USER_SESSION[:user_id] || '').to_s)

        ample_credit_service = CreditsModule::V1::AmpleCreditService.new(@params)
        ample_credit = ample_credit_service.credit({
                                      ample_credit: user.ample_credit,
                                      amount: amount.to_f
                                    })
        return user
      end

      #
      # This function debit the amount from user's ample_credit
      #
      # @param args [Hash] consist of user_id and amount to substract
      #
      _RunInTransactionBlock_
      def debit_from_ample_credits(args)
        user_id = args[:id]
        amount = args[:amount]

        user = get_user_from_id(user_id)

        ApplicationLogger.info('Request to DEBIT amount ' + amount.to_s + ' for user_id ' + user.id.to_s +
                                 ' by requester_id ' + (USER_SESSION[:user_id] || '').to_s)

        ample_credit_service = CreditsModule::V1::AmpleCreditService.new(@params)
        ample_credit = ample_credit_service.debit({
                                      ample_credit: user.ample_credit,
                                      amount: amount.to_f
                                    })
        return user
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # This function fetch user from given id
      #
      # @param user_id [Integer] id of the user
      # @raise InvalidArgumentsError if no user found for the given id
      #
      def get_user_from_id(user_id)
        user_data_service = UsersModule::V1::UserDataService.instance(@params)
        begin
          user = user_data_service.get_user_from_user_id(user_id)
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError => e
          # Rescue UnAuthenticatedError and raise InvalidArgumentsError, which
          # will then be captured by general ExceptionHandler
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
        end
        return user
      end

    end
  end
end

