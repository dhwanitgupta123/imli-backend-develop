module CommonModule
  module V1
    #
    # Carousel Service class to implement CRUD for Caraousel model
    #
    class CarouselService < BaseModule::V1::BaseService
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      CAROUSEL_MODEL = CommonModule::V1::Carousel
      CAROUSEL_TYPES_CLASS = CommonModule::V1::ModelStates::V1::CarouselTypes
      CAROUSEL_TYPES_CONSTANTS = CommonModule::V1::ModelStates::V1::CarouselTypes.constants
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      CAROUSEL_RESPONSE_DECORATOR = CommonModule::V1::CarouselResponseDecorator
      CAROUSEL_HELPER = CommonModule::V1::CarouselHelper

      #
      # initialize CarouselService Class
      #
      def initialize(params)
        @params = params
      end

      # 
      # update_carousel API service function
      # @param update_carousel_params [Hash] contains params type [String], label [String], images[Array]
      # 
      # @return [response]
      # 
      def update_carousel(update_carousel_params)
        begin
          carousel = update_the_carousel(update_carousel_params)
          return CAROUSEL_RESPONSE_DECORATOR.create_carousel_ok_response(carousel)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return CAROUSEL_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return CAROUSEL_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return CAROUSEL_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end     
      end

      # 
      # update_the_carousel logic function for update_carousel service function
      # @param update_carousel_params[Hash] contains params type [String], images[Array]
      # 
      # @return [Carousel Model]
      # 
      def update_the_carousel(update_carousel_params)
        validate_update_carousel_params(update_carousel_params)

        type = update_carousel_params[:type]
        images_array = update_carousel_params[:images]

        caraousel = CAROUSEL_MODEL.update_the_carousel({
          type: type,
          images: images_array
        })
        return caraousel
      end

      # 
      # get_carousel API service function
      # @param get_carousel_params [Hash] contains params type [String]
      # 
      # @return [response]
      # 
      def get_carousel(get_carousel_params)
        begin
          carousel = get_the_carousel(get_carousel_params)
          return CAROUSEL_RESPONSE_DECORATOR.create_carousel_ok_response(carousel) if carousel.present?
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return CAROUSEL_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return CAROUSEL_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return CAROUSEL_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end 
      end

      # 
      # get_the_carousel logic function for get_carousel service function
      # @param get_carousel_params [Hash] contains params type [String]
      # 
      # @return [response]
      # 
      def get_the_carousel(get_carousel_params)
        validate_get_carousel_params(get_carousel_params)

        type = get_carousel_params[:type]

        carousel = CAROUSEL_MODEL.get_the_carousel({
          type: type
        })
        
        return carousel
      end

      # 
      # get_orphan_carousels_types_with_details API service function
      # 
      # @return [Array] carousels_array having details of all carousels defined in CarouselTypes class
      # 
      def get_orphan_carousels_types_with_details
        carousels = get_all_orphan_carousel_types_with_details
        return CAROUSEL_RESPONSE_DECORATOR.create_all_carousels_details_ok_response(carousels)
      end

      # 
      # get_all_orphan_carousel_types_with_details function to fetch all the orphan carousel types with details
      # 
      # - Fecthes the Carousel Types from CarouselTypes class and finds the difference with the existing carousel
      #   types in the MongoDB. Hence, it gives the Carousel types available to be created.
      # 
      # @return [Array] orphan_carousels_array
      # 
      def get_all_orphan_carousel_types_with_details
        carousels_types_array = []
        return [] if CAROUSEL_TYPES_CONSTANTS.blank?
        CAROUSEL_TYPES_CONSTANTS.each do |carousel_type|
          carousel_hash = CAROUSEL_TYPES_CLASS.const_get(carousel_type)
          carousels_types_array << carousel_hash[:type]
        end
        
        acquired_carousels_types_array = []
        acquired_carousels = get_all_the_carousels
        unless acquired_carousels.blank?
          acquired_carousels.each do |acquired_carousel|
            acquired_carousels_types_array << acquired_carousel[:type]
          end
        end
        
        orphan_carousels_type_array = carousels_types_array - acquired_carousels_types_array

        orphan_carousels_array = []
        unless orphan_carousels_type_array.blank?
          orphan_carousels_type_array.each do |orphan_carousel_type|
            orphan_carousel_hash = CAROUSEL_HELPER.get_carousel_details_from_carousel_type(orphan_carousel_type)
            orphan_carousels_array << orphan_carousel_hash
          end
        end

        return orphan_carousels_array
      end

      # 
      # get_valid_carousel_types function to get all the valid carousel types
      # 
      # @return [Array] [carousels_types_array]
      # 
      def get_valid_carousel_types
        carousels_types_array = []
        return carousels_types_array if CAROUSEL_TYPES_CONSTANTS.blank?
        CAROUSEL_TYPES_CONSTANTS.each do |carousel_type|
          carousel = CAROUSEL_TYPES_CLASS.const_get(carousel_type) 
          carousels_types_array << carousel[:type]
        end
        return carousels_types_array
      end


      # 
      # get_all_carousels API service function
      # 
      # @return [Array] carousels_array having all the carousels present
      # 
      def get_all_carousels
        carousels = get_all_the_carousels
        return CAROUSEL_RESPONSE_DECORATOR.create_get_all_carousels_ok_response(carousels)
      end

      def get_all_the_carousels
        carousels_array = CAROUSEL_MODEL.get_all_carousels
        carousels_mapped_array = CAROUSEL_HELPER.map_carousels_to_array(carousels_array)
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      # 
      # validate_update_carousel_params function
      # 
      # * Validates Carousel type [String], images [Array] are correct and exist
      # 
      # @param update_carousel_params [Hash] contains params type [String], description [String], images[Array]
      # 
      # Raises:
      # * InsufficientDataError : if type or images array is blank
      # * ResourceNotFoundError : if images provided do not exist
      # 
      def validate_update_carousel_params(update_carousel_params)    
        if update_carousel_params[:type].blank? or
              update_carousel_params[:images].blank? or
                 update_carousel_params.blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT%{ field: 'Type and Images' })      
        end
        images_array = update_carousel_params[:images]

        images_array.each do |image|
          image_status = IMAGE_SERVICE_HELPER.get_image_by_id(image[:image_id])
          if image_status.blank?
            raise CUSTOM_ERRORS_UTIL::ResourceNotFoundError.new(CONTENT::REQUESTED_FIELD_NOT_FOUND%{ field: 'IMAGE' })
          end
        end

        carousel_type_exists?(update_carousel_params[:type])
      end

      # 
      # validate_get_carousel_params function
      # 
      # * Validates Carousel type [String] is correct and exists
      # 
      # @param get_carousel_params [Hash] contains params type [String]
      # 
      # Raises:
      # * InsufficientDataError : if type is blank
      # 
      def validate_get_carousel_params(get_carousel_params)
        if get_carousel_params[:type].blank? or get_carousel_params.blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT::INSUFFICIENT_DATA)      
        end
      end

      # 
      # carousel_type_exists? function
      # 
      # * Checks if the request carousel type exists i.e. is defined in the CarouselTypes class
      # 
      # @param carousel_type [String] type of Carousel
      # 
      # Raises:
      # * InvalidDataError : if carousel type is not valid in CarouselTypes class
      # 
      def carousel_type_exists?(carousel_type)
        unless get_valid_carousel_types.include?(carousel_type.to_i)
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::REQUESTED_FIELD_NOT_FOUND%{ field: 'Carousel Type' })
        end
      end
    end
  end
end