module CommonModule
  module V1
    #
    # Product Suggester Service class to implement CRUD for PRODUCT SUGGESTER model
    #
    class ProductSuggesterService < BaseModule::V1::BaseService
      #
      # Defining global variables with versioning
      #
      USER_SERVICE = UsersModule::V1::UserService
      PRODUCT_SUGGESTER_RESPONSE_DECORATOR = CommonModule::V1::ProductSuggesterResponseDecorator
      USER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::UserEmailWorker
      EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType
      CACHE_UTIL = CommonModule::V1::Cache
      PRODUCT_SUGGESTER_HELPER = CommonModule::V1::ProductSuggesterHelper

      #
      # initialize ProductSuggesterService Class
      #
      def initialize(params)
        @params = params
        @product_suggester_dao = CommonModule::V1::ProductSuggesterDao.new(params)
      end

      #
      # update_product_suggestions function is API function
      # @param product_suggestion [Hash]
      # - contains [String] product_name
      #
      # @return [response]
      #
      def update_product_suggestions(product_suggestion)
        begin
          response = update_suggestions_by_user(product_suggestion)
          return PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_product_suggestion_ok_response if response == true
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERROR_UTIL::InvalidDataError => e
          PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError => e
          PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      #
      # [update_suggestions_by_user is logic function for update_product_suggestions]
      # @param product_suggestion [Hash]
      # - contains [String] product_name
      #
      # @return [Boolean] true if updated
      #
      def update_suggestions_by_user(product_suggestion)
        validate_product_suggestion_params(product_suggestion)
        user = validate_user

        @product_suggester_dao.add_product_suggestion({
          user_id: user.id,
          product_name: product_suggestion
          })
        return true
      end

      #
      # function to get all product suggestion details
      #
      # Response::
      #   * sends response with all the product suggestion details
      #
      # @return [response]
      #
      def get_all_product_suggestions(paginate_params)
        begin
          product_suggestions = get_all_suggestions(paginate_params)
          return PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_all_suggestions_response(product_suggestions)
        rescue CUSTOM_ERROR_UTIL::InvalidDataError => e
          PRODUCT_SUGGESTER_RESPONSE_DECORATOR.create_response_invalid_data_passed(e.message)
        end
      end

      #
      # function to get all suggestions details
      #
      # Response::
      #   * sends response with all the suggestions details
      #
      # @return [response]
      #
      def get_all_suggestions(paginate_params)
        validate_user
        all_suggestions = @product_suggester_dao.paginated_product_suggestions(paginate_params)
        return all_suggestions
      end

      #
      # Function to send email of all the product suggested by user in last 24 hrs
      #
      def send_last_x_hours_product_suggestion_mail(x = '')
        product_suggester_dao = CommonModule::V1::ProductSuggesterDao
        product_suggestions_in_interval = product_suggester_dao.get_all_last_x_hours_suggestions(x)
        if product_suggestions_in_interval.present?
          product_suggestions_array = []

          product_suggestions_in_interval.each do |user_suggestion|
            user_details = PRODUCT_SUGGESTER_HELPER.get_user_details_for_product_suggestion(user_suggestion)
            user_emails = user_details[:emails]
            user_emails_string = ""

            if user_emails.present?
              user_emails.each_with_index {|user_email, index|

                 next_user_email = user_emails[index+1]
                 user_emails_string = user_emails_string.to_s + user_email[:email_id].to_s

                 if next_user_email.present?
                   user_emails_string = user_emails_string + ","
                 end
              }
            end

            if user_emails_string.blank?
              user_emails_string = "-"
            end
            product_suggestions_array.push({
              user_details: {
                user_id: user_details[:id],
                phone_number: user_details[:phone_number],
                first_name: user_details[:first_name],
                last_name: user_details[:last_name],
                emails: user_emails_string
              },
              product_name: user_suggestion[:product_suggestion][:product_name]
            })
          end

          mail_attributes = {
              email_type: EMAIL_TYPE::PRODUCT_SUGGESTION[:type],
              to_name: CACHE_UTIL.read('IMLI_TEAM_NAME'),
              to_email_id: CACHE_UTIL.read('IMLI_TEAM_MAIL_IDS').to_s,
              subject: CACHE_UTIL.read('PRODUCT_SUGGESTION'),
              multiple: true,
              pdf_needed: true,
              product_suggestions_array: product_suggestions_array,
              aggregation_timeline_hours: x
            }
            USER_EMAIL_WORKER.perform_async(mail_attributes)
        end

        return product_suggestions_array
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      def validate_user
        user_service = USER_SERVICE.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        return user
      end

      #
      # Validate product suggestion parameters.
      #
      # @raise [InsufficientDataError] [if any of the hash fields are blank]
      def validate_product_suggestion_params(product_suggestion)
        if product_suggestion.blank? or product_suggestion[:product_name].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::INSUFFICIENT_DATA)
        end
      end
    end
  end
end
