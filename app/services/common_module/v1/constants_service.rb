module CommonModule
  module V1
    #
    # Constants Service class to implement CRUD for Caraousel model
    #
    class ConstantsService < BaseModule::V1::BaseService

      CLUSTER_TYPES_UTIL = ClusterModule::V1::ClusterTypes
      CLUSTER_LEVELS_UTIL = ClusterModule::V1::ClusterLevels
      CONSTANTS_RESPONSE_DECORATOR = CommonModule::V1::ConstantsResponseDecorator

      #
      # initialize CarouselService Class
      #
      def initialize(params)
        @params = params
      end

      #
      # Return all constants related to cluster module
      #  
      # @return [JsonResponse] Constants cluster response 
      #
      def get_cluster_constants
        cluster_constants = {}
        cluster_constants[:level_constants] = CLUSTER_LEVELS_UTIL::CLUSTER_LEVELS
        cluster_constants[:type_constants] = CLUSTER_TYPES_UTIL::CLUSTER_TYPES
        return CONSTANTS_RESPONSE_DECORATOR.create_cluster_constant_response(cluster_constants)
      end
    end
  end
end
