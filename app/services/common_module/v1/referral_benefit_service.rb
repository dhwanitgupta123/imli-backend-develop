require 'imli_singleton'

module CommonModule
  module V1
    class ReferralBenefitService < BaseModule::V1::BaseService
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @user_service = UsersModule::V1::UserService.new(@params)
      end

      def block_referral_benefits(args)

        user_id = args[:user_id]
        block_referral = args[:block_referral] || 0
        block_benefit = args[:block_benefit] || 0

        user = @user_service.get_user_from_user_id(user_id) # will raise exception if user not found

        referral_benefit = get_referral_benefit(user)

        if block_referral.to_i == 1 && block_benefit.to_i == 1
          benefit = CommonModule::V1::BenefitDao.update_benefit(referral_benefit, {
                                                                  refer_limit: referral_benefit.refer_count,
                                                                  benefit_limit: referral_benefit.benefit_count
                                                                })
        elsif block_referral.to_i == 1
          benefit = CommonModule::V1::BenefitDao.update_benefit(referral_benefit, {
                                                                  refer_limit: referral_benefit.refer_count
                                                                })
        elsif block_benefit.to_i == 1
          benefit = CommonModule::V1::BenefitDao.update_benefit(referral_benefit, {
                                                                  benefit_limit: referral_benefit.benefit_count
                                                                })
        end

        return {user: user, benefit: benefit}
      end

      private

      def get_referral_benefit(user)
        referral_benefit = CommonModule::V1::BenefitService.get_ample_credit_benefit_of_user(user)

        return referral_benefit if referral_benefit.present?

        raise CommonModule::V1::CustomErrors::ResourceNotFoundError.new(CommonModule::V1::Content::BENEFIT_NOT_FOUND)
      end
    end
  end
end
