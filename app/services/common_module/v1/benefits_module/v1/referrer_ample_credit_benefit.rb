module CommonModule
  module V1
    module BenefitsModule
      module V1
        class ReferrerAmpleCreditBenefit < CommonModule::V1::BenefitsModule::V1::BenefitsInterface

          CACHE = CommonModule::V1::Cache

          def initialize(params = '')
            @params = params
            @ample_credit_service = CreditsModule::V1::AmpleCreditService
            @user_notification_service = UsersModule::V1::UserNotificationService
          end

          # 
          # This function applies AmpleCreditBenefit the benefit to referrer
          #
          # @param params [Hash] containing user and benefit
          #
          def apply(params)
            user = params[:user]
            benefit = params[:benefit]

            return false unless is_valid?(user, benefit)

            ample_credit_service = @ample_credit_service.new(@params)

            referrer = user.referrer

            amount = CACHE.read_int('AMPLE_CREDIT_BENEFIT_AMOUNT') || 200

            ample_credit_service.credit({
                ample_credit: referrer.ample_credit,
                amount: amount
              })

            notify_referrer(referrer, amount)
            
            return update_referrer_benefit
          end

          #
          # Notifying referrer about his/her updated ample credits
          #
          # @param referrer [Model] user model
          # @param amount [Decimal] amount in decimal
          # 
          def notify_referrer(referrer, amount)
            user_notification_service = @user_notification_service.new(@params)
            user_notification_service.send_successful_referral_notification({user: referrer, amount: amount})

            user_notification_service.send_successful_referral_sms({user: referrer, amount: amount})
          end

          # 
          # This function updates the benefit_count of referrer
          #
          def update_referrer_benefit
            @referrer_benefit.benefit_count += 1
            @referrer_benefit.save
          end

          # 
          # checks user is valid or not
          #
          # @param user [Model] user
          # 
          # @return [Boolean] true if benefit limit of user is less than benefit count
          #
          def is_valid?(user, benefit)

            return false  if user.referred_by.blank?

            @referrer_benefit = get_referrer_benefit(user, benefit)

            return false if @referrer_benefit.benefit_count >= @referrer_benefit.benefit_limit

            return true
          end
        end
      end
    end
  end
end
