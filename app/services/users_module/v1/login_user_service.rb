module UsersModule
  module V1
    class LoginUserService < BaseModule::V1::BaseService

      def initialize(params)
        @params = params
        @provider_service = UsersModule::V1::ProviderService
        @custom_error_util = CommonModule::V1::CustomErrors
      end
      # 
      # It initiates the processing the updating login type request of user
      # 
      # Paramaters::
      #     * user_params [Hash] Stores the strong parameter of User
      #     * request_type [enum] Stores the provider request type
      #     * user [object] Stores the user which is trying to update the provider type in the system
      # 
      # @return [object] Returns provider object if new provider is created
      def update_login_type(args)
        user = args[:user]
        if user.provider.present? 
          provider_args = {user_params: args[:user_params], request_type: args[:request_type], provider: user.provider}
          update_provider_type(provider_args)
          return user.provider
        else
          raise @custom_error_util::RunTimeError.new('Provider not found')
        end
      end  

      # 
      # It initiates the processing the login request of user
      # 
      # Paramaters::
      #     * user_params [hash] of phone_number & password or otp
      #     * user [object] has user who wants to update provider type
      #     * request_type [enum] to know if provider is Generic or messag
      # 
      # @return [object] Returns provider object if new provider is created
      def login_user(args)
        user = args[:user]
        if user.provider.present? 
          provider_args = {user_params: args[:user_params], request_type: args[:request_type], provider: user.provider}
          check_provider(provider_args)
        else
          raise @custom_error_util::RunTimeError.new('Provider not found')
        end
      end

      # 
      # Function to call update_provider_type of ProviderService
      # 
      # Paramaters::
      #     * user_params [Hash] Stores the strong parameter of User
      #     * provider [object] Stores the provider of user which is trying to update provider type in the system
      #     * request_type [enum] to know which provider to update
      # 
      # @return [object] provider model object
      def update_provider_type(args)
        provider_service = @provider_service.new(@params)
        provider_service.update_provider_type(args)
      end 

      # 
      # Fucntion to call check_provider fuunction of ProviderService
      # 
      # Paramaters::
      #     * user_params [hash] of phone_number & password or otp
      #     * user [object] has user who wants to update provider type
      #     * request_type [enum] to know if provider is Generic or messag
      # 
      # @return [boolean] if User credentials matches
      def check_provider(args)
        provider_service = @provider_service.new(@params)
        provider_service.check_provider(args)
      end 
    end
  end
end