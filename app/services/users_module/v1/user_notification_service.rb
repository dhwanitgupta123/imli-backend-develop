require 'imli_static_settings'
module UsersModule
  module V1
    class UserNotificationService < BaseModule::V1::BaseService

      NOTIFICATION_TYPE = CommunicationsModule::V1::Notifications::V1::NotificationType
      USER_NOTIFICATION_WORKER = CommunicationsModule::V1::Notifications::V1::UserNotificationWorker
    	USER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::UserEmailWorker
      NOTIFICATION_WORKER = CommunicationsModule::V1::Notifications::V1::UserNotificationWorker
      SMS_WORKER = CommunicationsModule::V1::Messengers::V1::SmsWorker
      PROMOTION_WORKER = CommunicationsModule::V1::Mailers::V1::PromotionEmailWorker
    	EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType
      EMAIL_SERVICE = UsersModule::V1::EmailService
      ORDER_DATA_SERVICE = MarketplaceOrdersModule::V1::OrderDataService
      CACHE_UTIL = CommonModule::V1::Cache
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      SMS_TYPE = CommunicationsModule::V1::Messengers::V1::SmsType

    	def initialize(params)
        @params = params
      end

      #
      # Function to send welcome to Ample club email
      #
      def send_welcome_email(email_id)
        return if email_id.blank?
        email_service = EMAIL_SERVICE.new
        begin
          email = email_service.get_email_from_email_id(email_id)
          authentication_token = email.authentication_token
          current_host = CACHE_UTIL.read('HOST')
          api_url = current_host.to_s + '/api/users/verify_email?token=' + authentication_token.to_s

          email_subject = CACHE_UTIL.read('WELCOME_EMAIL_SUBJECT')
          mail_attributes = {
            email_type: EMAIL_TYPE::WELCOME[:type],
            api_url: api_url,
            subject: email_subject.to_s,
            to_first_name: email.user.first_name.to_s,
            to_last_name: email.user.last_name.to_s,
            to_email_id: email.email_id.to_s,

          }
          USER_EMAIL_WORKER.perform_async(mail_attributes)
        rescue => e
          ApplicationLogger.debug('Failed to send Welcome Email to user id:' + email.user.id.to_s + 'for email id:' + email.id.to_s + ' , ' + e.message)
        end
      end

      #
      # Function to send email of list of Pending Users
      #
      def send_all_pending_user_mail(users_hash)
        if users_hash[:users].present?
          users = USER_SERVICE_HELPER.create_users_json(users_hash[:users])
          mail_attributes = {
            email_type: EMAIL_TYPE::PENDING_USERS[:type],
            to_name: CACHE_UTIL.read('IMLI_TEAM_NAME'),
            to_email_id: CACHE_UTIL.read('IMLI_TEAM_MAIL_IDS').to_s,
            subject: CACHE_UTIL.read('PENDING_USERS'),
            multiple: true,
            pdf_needed: true,
            users: users
          }
          USER_EMAIL_WORKER.perform_async(mail_attributes)
          return users
        end
      end

      #
      # Function to send invite email
      #
      def send_invite_mail(emails_array)
        unless emails_array.blank?
          emails_string = ""
          emails_array.each do |email|
            emails_string = emails_string + ',' + email[:email_id]
          end
          mail_attributes = {
            email_type: EMAIL_TYPE::INVITE[:type],
            from: CACHE_UTIL.read('INVITE_EMAIL_FROM'),
            to_email_id: emails_string.to_s,
            subject: CACHE_UTIL.read('INVITE_MAIL_SUBJECT'),
            multiple: true
          }
          PROMOTION_WORKER.perform_async(mail_attributes)
        end
      end

      #
      # Function to send successful referral notification to user
      #
      def send_successful_referral_notification(args)

        user = args[:user]
        amount = args[:amount]

        begin
          params = {
            username: 'parse_user_' + user.id.to_s,
            notification_type: NOTIFICATION_TYPE::SUCCESSFUL_REFERRAL[:type],
            amount_credited: amount
          }
          USER_NOTIFICATION_WORKER.perform_async(params)
        rescue => e
          ApplicationLogger.debug('Failed to push notification for ample credit referral benefit to user: ' + user.id.to_s + ' , ' + e.message)
        end
      end

      #
      # Function to send successful referral sms to user
      #
      def send_successful_referral_sms(args)

        user = args[:user]
        amount = args[:amount]

        begin
          message_attributes = {
            phone_number: user.phone_number,
            amount_credited: amount
          }
          SMS_WORKER.perform_async(message_attributes, SMS_TYPE::SUCCESSFUL_REFERRAL)
        rescue => e
          ApplicationLogger.debug('Failed to send SMS for placed order to user : ' + user.id.to_s + ' , ' + e.message)
        end
      end

      #
      # Function to send membership expiring sms
      #
      def membership_expiry_sms(membership)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_EXPIRING_SMS') == '0'
        timestamp = Time.zone.now.change(hour: 11).in_time_zone('UTC')
        user = membership.users.first
        savings = user.profile.savings
        args = {
          phone_number: membership.users.first.phone_number,
          savings: savings,
          membership_name: membership.plan.name,
          expires_at: membership.expires_at.in_time_zone.strftime("%d %b, %Y")
        }
        begin
          SMS_WORKER.perform_at(timestamp, args, SMS_TYPE::MEMBERSHIP_EXPIRING)
        rescue => e
          ApplicationLogger.debug('Failed to send membership_expiry_sms')
        end
      end

      #
      # Function to send membership expiring notification
      #
      def membership_expiry_notification(membership)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_EXPIRING_NOTIFICATION') == '0'
        timestamp = Time.zone.now.change(hour: 11).in_time_zone('UTC')
        args = {
          username: 'parse_user_' + membership.users.first.id.to_s,
          notification_type: NOTIFICATION_TYPE::MEMBERSHIP_EXPIRING[:type]
        }
        begin
          NOTIFICATION_WORKER.perform_at(timestamp, args)
        rescue => e
          ApplicationLogger.debug('Failed to push membership_expiry_notification')
        end
      end

      #
      # Function to send membership expiring email
      #
      def membership_expiry_email(membership)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_EXPIRING_EMAIL') == '0'
        plan_image_link = CommonModule::V1::PlanHelper.get_plan_image_link_for_communication(membership.plan)
        timestamp = Time.zone.now.change(hour: 11).in_time_zone('UTC')
        user = membership.users.first
        email = UsersModule::V1::UserServiceHelper.get_primary_mail(user)
        email_subject = 'Your ' + membership.plan.name + ' membership is expiring soon.'
        mail_attributes = {
            email_type: EMAIL_TYPE::MEMBERSHIP_EXPIRING[:type],
            to_first_name: user.first_name,
            last_name: user.last_name,
            plan_name: membership.plan.name,
            plan_image: plan_image_link,
            subject: email_subject,
            to_email_id: email.email_id.to_s,
            expires_at: membership.expires_at.in_time_zone.strftime("%d %b, %Y")
        }
        begin
          USER_EMAIL_WORKER.perform_at(timestamp, mail_attributes)
        rescue => e
          ApplicationLogger.debug('Failed to send membership_expiry_email')
        end
      end

      #
      # Function to notify user that membership is expired
      #
      def notify_membership_expired(membership)
        membership_expired_sms(membership)
        membership_expired_notification(membership)
        membership_expired_email(membership)
      end

      #
      # Function to send membership expired sms
      #
      def membership_expired_sms(membership)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_EXPIRED_SMS') == '0'
        timestamp = Time.zone.now.change(hour: 11).in_time_zone('UTC')
        args = {
          phone_number: membership.users.first.phone_number,
          membership_name: membership.plan.name,
          expires_at: membership.expires_at.in_time_zone.strftime("%d %b, %Y")
        }
        begin
          SMS_WORKER.perform_at(timestamp, args, SMS_TYPE::MEMBERSHIP_EXPIRED)
        rescue => e
          ApplicationLogger.debug('Failed to send membership_expired_sms')
        end
      end

      #
      # Function to send membership expired notification
      #
      def membership_expired_notification(membership)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_EXPIRED_NOTIFICATION') == '0'
        timestamp = Time.zone.now.change(hour: 11).in_time_zone('UTC')
        args = {
          username: 'parse_user_' + membership.users.first.id.to_s,
          notification_type: NOTIFICATION_TYPE::MEMBERSHIP_EXPIRED[:type]
        }
        begin
          NOTIFICATION_WORKER.perform_at(timestamp, args)
        rescue => e
          ApplicationLogger.debug('Failed to push membership_expired_notification')
        end
      end

      #
      # Function to send membership expired email
      #
      def membership_expired_email(membership)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_EXPIRED_EMAIL') == '0'
        plan_image_link = CommonModule::V1::PlanHelper.get_plan_image_link_for_communication(membership.plan)
        timestamp = Time.zone.now.change(hour: 11).in_time_zone('UTC')
        user = membership.users.first
        email = UsersModule::V1::UserServiceHelper.get_primary_mail(user)
        email_subject = 'Your ' + membership.plan.name + ' membership has expired'
        mail_attributes = {
            email_type: EMAIL_TYPE::MEMBERSHIP_EXPIRED[:type],
            to_first_name: user.first_name,
            last_name: user.last_name,
            plan_name: membership.plan.name,
            plan_image: plan_image_link,
            subject: email_subject,
            to_email_id: email.email_id.to_s,
            expires_at: membership.expires_at.in_time_zone.strftime("%d %b, %Y")
        }
        begin
          USER_EMAIL_WORKER.perform_at(timestamp, mail_attributes)
        rescue => e
          ApplicationLogger.debug('Failed to send membership_expired_email')
        end
      end

      #
      # Function to notify user that membership is subscribed and activated
      #
      def notify_membership_subscribed(membership)
        membership_subscribed_sms(membership)
        membership_subscribed_notification(membership)
        membership_subscribed_email(membership)
      end

      #
      # Function to send membership subscribed sms
      #
      def membership_subscribed_sms(membership)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_SUBSCRIBED_SMS') == '0'
        args = {
          phone_number: membership.users.first.phone_number,
          first_name: membership.users.first.first_name,
        }
        begin
          SMS_WORKER.perform_async(args, SMS_TYPE::MEMBERSHIP_UPGRADE)
        rescue => e
          ApplicationLogger.debug('Failed to send membership_subscribed_sms')
        end
      end

      #
      # Function to send membership subscribed notification
      #
      def membership_subscribed_notification(membership)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_SUBSCRIBED_NOTIFICATION') == '0'
        args = {
          username: 'parse_user_' + membership.users.first.id.to_s,
          notification_type: NOTIFICATION_TYPE::MEMBERSHIP_UPGRADE[:type]
        }
        begin
          NOTIFICATION_WORKER.perform_async(args)
        rescue => e
          ApplicationLogger.debug('Failed to push membership_subscribed_notification')
        end
      end

      #
      # Function to send membership subscribed notification
      #
      def membership_subscribed_email(membership)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_SUBSCRIBED_EMAIL') == '0'
        plan_image_link = CommonModule::V1::PlanHelper.get_plan_image_link_for_communication(membership.plan)
        plan_details = membership.plan.attributes
        user = membership.users.first
        email = UsersModule::V1::UserServiceHelper.get_primary_mail(user)
        email_subject = 'Your ' + membership.plan.name + ' membership has been subscribed'
        mail_attributes = {
            email_type: EMAIL_TYPE::MEMBERSHIP_UPGRADE[:type],
            to_first_name: user.first_name,
            last_name: user.last_name,
            plan_details: plan_details,
            plan_image: plan_image_link,
            subject: email_subject,
            to_email_id: email.email_id.to_s,
            expires_at: membership.expires_at.in_time_zone.strftime("%d %b, %Y")
        }
        begin
          USER_EMAIL_WORKER.perform_async(mail_attributes)
        rescue => e
          ApplicationLogger.debug('Failed to send membership_subscribed_email')
        end
      end

      #
      # Function to notify user that membership is cancelled and de-activated
      #
      def notify_membership_cancelled(membership, refund_amount)
        membership_cancelled_sms(membership, refund_amount)
        membership_cancelled_notification(membership)
        membership_cancelled_email(membership, refund_amount)
      end

      #
      # Function to send membership cancellation sms
      #
      def membership_cancelled_sms(membership, refund_amount)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_CANCELLED_SMS') == '0'
        args = {
          phone_number: membership.users.first.phone_number,
          first_name: membership.users.first.first_name,
          refund_amount: refund_amount
        }
        begin
          SMS_WORKER.perform_async(args, SMS_TYPE::MEMBERSHIP_CANCELLED)
        rescue => e
          ApplicationLogger.debug('Failed to send membership_cancelled_sms')
        end
      end

      #
      # Function to send membership cancellation notification
      #
      def membership_cancelled_notification(membership)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_CANCELLED_NOTIFICATION') == '0'
        args = {
          username: 'parse_user_' + membership.users.first.id.to_s,
          notification_type: NOTIFICATION_TYPE::MEMBERSHIP_CANCELLED[:type]
        }
        begin
          NOTIFICATION_WORKER.perform_async(args)
        rescue => e
          ApplicationLogger.debug('Failed to push membership_cancelled_notification')
        end
      end

      #
      # Function to send membership cancellation notification
      #
      def membership_cancelled_email(membership, refund_amount)
        return if ImliStaticSettings.get_feature_value('MEMBERSHIP_CANCELLED_EMAIL') == '0'
        plan_image_link = CommonModule::V1::PlanHelper.get_plan_image_link_for_communication(membership.plan)
        user = membership.users.first
        email = UsersModule::V1::UserServiceHelper.get_primary_mail(user)
        email_subject = 'Your ' + membership.plan.name + ' membership has been cancelled'
        mail_attributes = {
            email_type: EMAIL_TYPE::MEMBERSHIP_CANCELLED[:type],
            to_first_name: user.first_name,
            last_name: user.last_name,
            plan_name: membership.plan.name,
            plan_image: plan_image_link,
            refund_amount: refund_amount,
            subject: email_subject,
            to_email_id: email.email_id.to_s
        }
        begin
          USER_EMAIL_WORKER.perform_async(mail_attributes)
        rescue => e
          ApplicationLogger.debug('Failed to send membership_cancelled_email')
        end
      end

      #
      # Send email to the user's primary mail id
      # with last month's account summary
      #
      # @param  {Model} user
      # @param  {DateTime} previous_month_datetime
      # @param  {Hash} mail_attributes
      #         - {String} monthly_orders
      #         - {String} monthly_savings
      #         - {String} monthly_orders_count
      #
      def self.send_monthly_account_summary_email(user, previous_month_datetime, mail_attributes)
        begin
          timestamp = Time.zone.now.change(hour: 10).in_time_zone('UTC')
          first_name = user.first_name if user.present?
          email = USER_SERVICE_HELPER.get_primary_mail(user)
          email_subject = 'ACCOUNT MONTHLY SUMMARY [' + previous_month_datetime.strftime("%b'%y") + '] ' + ' -Ample Wholesale'
          if email.present?
            mail_attributes = mail_attributes.merge({
              email_type: EMAIL_TYPE::MONTHLY_ACCOUNT_SUMMARY[:type],
              subject: email_subject.to_s,
              to_first_name: first_name,
              to_email_id: email.email_id,
              statement_of_month: previous_month_datetime.strftime("%B").upcase,
              pdf_needed: false
              })
            USER_EMAIL_WORKER.perform_at(timestamp, mail_attributes)
          end
        rescue => e
          ApplicationLogger.debug('Failed to send user account monthly summary : ' + user.id.to_s + ' , ' + e.message)
        end
      end

    end
  end
end
