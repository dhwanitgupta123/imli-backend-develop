require 'imli_singleton'
module UsersModule
  module V1
    class UserDataService < BaseModule::V1::BaseService
      include ImliSingleton

      #
      # Initialize UserDataService with passed params
      #
      def initialize(params = {})
        @params = params
        super
      end

      #
      # Fetch user corresponding to passed user id
      #
      # @param user_id [Integer] unique id of an user
      #
      # @return [Object] User object
      # @raise [@custom_error_util::UnAuthenticatedUserError] if invalid user id passed
      #
      def get_user_from_user_id(user_id)
        begin
          user_dao = UsersModule::V1::UserDao.instance(@params)
          user = user_dao.get_by_id(user_id)
        rescue ActiveRecord::RecordNotFound
          raise @custom_error_util::UnAuthenticatedUserError.new(@content_util::NO_USER)
        end
      end

    end #End of class
  end
end
