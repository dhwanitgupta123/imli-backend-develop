module UsersModule
  module V1
    module ProvidersModule
      module V1
        class MessageProviderService < BaseModule::V1::BaseService

          def initialize
            @message_model = UsersModule::V1::ProvidersModule::V1::Message
            @cache_util = CommonModule::V1::Cache
            @custom_error_util = CommonModule::V1::CustomErrors
            @otp_password_helper = UsersModule::V1::OneTimePasswordHelper
            @content_util = CommonModule::V1::Content
          end
          # 
          # to create new object of message model
          # 
          # Description::
          #   * Calls generate_otp of OneTimePasswordHelper module 
          # 
          # @return [object] Message model object
          def new_message_provider
            otp = @otp_password_helper.generate_otp
            new_message(otp)
          end

          # 
          # function to decide if message provider needed to be updated or newly created
          # 
          # Parameters::
          #   * message [object] which needs to be updated/created
          # 
          # @return [object] Message model object
          def create_update_message_provider(message)
            @message = message
            @message = message.present? ? update_message_provider : new_message_provider
          end

          # 
          # Function checks if OTP sent by user matches or not
          # 
          # Parameters::
          #   * message [object] which has the OTP from system
          #   * otp [integer] otp sent by user
          # 
          # @return [boolean] if OTP matches then true
          def verify_otp(otp, message)
            @otp_password_helper.verify_otp(otp,message)
          end

          # 
          # function to update the message provider
          # 
          # Description::
          #   * if otp has expired the generate new otp and update the message provider with new expiry
          #   * otherwise just extend the expiry and let the otp remain the same
          # 
          # @return [object] message object 
          def update_message_provider
            if @otp_password_helper.otp_expired(@message)
              return @message if update_sms_provider
            else
              if @message.resend_count < @cache_util.read("RESEND_COUNT").to_i
                return @message if update_expiry
              else
                raise @custom_error_util::ReachedMaximumLimitError.new(@content_util::TOO_MANY_REQUESTS)
              end
            end
          end

          # 
          # function to resend otp
          # 
          # Description::
          #   * If otp in DB expired? generate new & send
          #   * If Not expired increase expiry, resend count by one and send
          # 
          # Parameters::
          #   * message [object]
          #  
          # @return [integer] new /updated OTP
          def resend_otp(message)
            @message = message
            @message = update_message_provider
            return @message.otp
          end

          private
            # 
            # function to generate new otp & update otp & expiry time if otp has expired for existing user
            # 
            # Description::
            #   * Generates new OTP 
            #   * Resets resend_count = 0 to keep track of spamming
            # 
            # @return [boolean] if update was successfull
            def update_sms_provider
              @message.otp = @otp_password_helper.generate_otp
              @message.resend_count = 0
              @message.save_message
            end

            # 
            # function to update expiry time for otp if for existing user otp is not expired
            # 
            # @return [boolean] if update was successfull
            def update_expiry
              @message.save_message
            end

            # 
            # Function to create message model
            # 
            # Parameters::
            #   * OTP [Integer] contains OTP
            # 
            # @return [object] message object
            def new_message(otp)
              @message_model.new(otp: otp)
            end
        end
      end
    end
  end
end