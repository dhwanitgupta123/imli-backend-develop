#
# Module to handle user related functionality
#
module UsersModule
  module V1
    #
    # Module to handle membership related functionality
    #
    module MembershipModule
      module V1
        #
        # Membership Payment service class handles the membership payment related logic functionality
        #
        class MembershipPaymentService < BaseModule::V1::BaseService

          CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
          CONTENT_UTIL = CommonModule::V1::Content
          PAYMENT_MODEL = UsersModule::V1::MembershipModule::V1::MembershipPayment
          PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
          MEMBERSHIP_PAYMENT_HELPER = UsersModule::V1::MembershipModule::V1::MembershipPaymentHelper
          MEMBERSHIP_PAYMENT_MODES = UsersModule::V1::MembershipModule::V1::MembershipPaymentModes
          TRANSACTION_STATUS = TransactionsModule::V1::TransactionStatus
          MEMBERSHIP_SERVICE = UsersModule::V1::MembershipService
          USER_SERVICE = UsersModule::V1::UserService
          PAYMENT_DAO = UsersModule::V1::MembershipModule::V1::MembershipPaymentDao
          PAYMENT_TRANSACTION_SERVICE =TransactionsModule::V1::PaymentTransactionsService
          PAYMENT_TYPES = TransactionsModule::V1::PaymentTypes

          #
          # initialize required classes
          #
          def initialize(params = {})
            @params = params
          end

          #
          # Function returns new Payment object of membership payment
          #
          def new_payment(payment_args)
            payment = PAYMENT_MODEL.new
            return payment.new_payment(payment_args)
          end

          #
          # Handle payment response.
          # Depending on the transaction status, it should call internal
          # payment_success or payment_failed methods to perform certain specific operations
          #
          # @param transaction_status [Integer] [status of the payment transaction]
          # @param args = '' [JSON] [optional Hash which contains extra parameters like payment_id, user_id]
          #
          # @return [Object] [Payment object with respect to passed id]
          #
          def handle_payment(transaction_status, args = '')
            if args.present? && args[:payment_id].present?
              ApplicationLogger.debug('Handle online Membership payment with status: ' + transaction_status.to_s + ' , for payment id: ' + args[:payment_id].to_s)
            else
              ApplicationLogger.debug('Transaction callback received with NIL payment id for transaction: ' + transaction_status.to_s)
            end
            # Route to proper handler based upon transaction status
            case transaction_status.to_i
            when TRANSACTION_STATUS::SUCCESS
              return payment_successful(args)
            when TRANSACTION_STATUS::FAILED
              return payment_failed(args)
            when TRANSACTION_STATUS::CANCELLED
              # For now, call payment failed itself
              return payment_failed(args)
            end
          end

          def handle_refund(transaction_status, args = '')
            # TO-DO: Handle Order Refunds
          end


          #
          # Payment Failed. Called when payment of that order failed
          # It should change payment state to FAILED
          #
          # Parameters hash:
          #   - payment_id: payment id
          #   - user_id: id of user for which payment is to be made
          #
          # @param payment_params [JSON] [Hash of payment parameters containing payment_id]
          #
          # @return [Object] [Payment object which is succeeded]
          #
          # @raise [InvalidArgumentsError] [if no payment exists with respect to passed id OR payment is not in pending state]
          #
          def payment_failed(payment_params)
            return nil if payment_params.blank?
            # Might need this part in future
            # user_service = USER_SERVICE.new
            # membership_service = MEMBERSHIP_SERVICE.new
            # user_id = payment_params[:user_id]
            # user = user_service.get_user_from_user_id(user_id)
            payment = get_payment_by_id({ id: payment_params[:payment_id] })
            ApplicationLogger.debug('Membership Payment failed for payment id: ' + payment.id.to_s)
            # membership_service.transactional_payment_failed(payment, user)
            payment.payment_fail!
            return { payment: payment }
          end

          #
          # Payment Successfull
          # Parameters:
          #   - payment_id: payment id
          #   - user_id: id of user for which payment is to be made
          #
          # @param payment_params [JSON] [Hash of payment parameters containing payment_id]
          #
          # @return [Object] [Payment object which is succeeded]
          #
          # @raise [InvalidArgumentsError] [if payment is not in pending state or No active order associated with that payment]
          #
          def payment_successful(payment_params)
            return nil if payment_params.blank?
            user_service = USER_SERVICE.new(@params)
            membership_service = MEMBERSHIP_SERVICE.new
            payment_id = payment_params[:payment_id]
            user_id = payment_params[:user_id]
            user = user_service.get_user_from_user_id(user_id)
            payment = get_payment_by_id({ id: payment_params[:payment_id] })
            ApplicationLogger.debug('Membership Payment successful for payment id: ' + payment.id.to_s)
            membership_service.transactional_payment_success(payment, user)
            payment.payment_success!
            return { payment: payment }
          end

          #
          # Function calculates the amount to be paid to for the membership subscrition
          #   * It handles if the user has one already active membership
          #
          def calculate_membership_payment(membership, plan)
            refund = 0.0
            if membership.present?
              if membership.plan != plan
                refund = get_membership_remaining_cost(membership)
              end
            end
            grand_total = get_new_membership_cost(plan)
            billing_amount = grand_total - refund
            if billing_amount < 0
              payment_type = PaymentModule::V1::PaymentType.get_id_by_key('REFUND')
              billing_amount = billing_amount * (-1)
            else
              payment_type = PaymentModule::V1::PaymentType.get_id_by_key('PAY')
            end
            service_tax_collected = get_service_tax_amount(billing_amount, plan)
            net_total = billing_amount
            payment_args = {
              grand_total: grand_total,
              billing_amount: billing_amount,
              net_total: net_total,
              refund: refund,
              total_service_tax: service_tax_collected,
              total_savings: BigDecimal.new('0.0'),
              payment_type: payment_type
            }
            details_array = MEMBERSHIP_PAYMENT_HELPER.calculate_payment_mode_details(payment_args)
            return { payment_args:  payment_args, payment_mode_details: details_array }
          end

          #
          # Function to calculate the amount of money to be refunded
          #
          def refund_membership_payment(membership)
            refund = get_membership_remaining_cost(membership)
            service_tax = get_service_tax_amount(refund, membership.plan)
            billing_amount = grand_total = net_total = refund
            payment_args = {
              grand_total: grand_total,
              billing_amount: billing_amount,
              net_total: net_total,
              refund: refund,
              total_service_tax: service_tax,
              total_savings: BigDecimal.new('0.0'),
              payment_type: PaymentModule::V1::PaymentType.get_id_by_key('REFUND')
            }
            return payment_args
          end

          #
          # Function calculates the final amount to be paid by the user
          # when payment is initiated.
          # It calcluates the discount based on Payment mode
          #
          def final_membership_payment(membership, payment_args)
            payment_mode_savings = get_payment_mode_savings(payment_args[:mode].to_s)
            payment = get_pending_payment(membership.membership_payments)
            payment.mode = payment_args[:mode]
            payment.payment_mode_savings = payment_mode_savings * payment.billing_amount / 100
            payment.total_savings = payment_mode_savings * payment.billing_amount / 100
            payment.net_total = payment.billing_amount - payment.payment_mode_savings
            payment.save!
          end

          #
          # Function fetches the payment mode savings
          #
          def get_payment_mode_savings(mode)
            # TO-DO : Payment mode savings for membership will come into picture
            # for standalone membership payment
            # For, now keeping it default to 0
            return BigDecimal.new('0.0')

            # payment_details = PAYMENT_HELPER.fetch_payment_details_hash
            # mode_details = payment_details[mode]
            # if mode_details.blank? || !MEMBERSHIP_PAYMENT_MODES::ALLOWED_MODES.include?(mode.to_i)
            #   raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_PAYMENT_MODE)
            # end
            # payment_mode_savings = mode_details['payment_mode_savings'].to_d
          end

          #
          # Function calculates cost of remaining user's active membership
          #   * This amount gets adjusted in the cost of new of membership subscribed
          #   * Amount is calculated on prorated basis, i.e. the number of months consumed
          #
          def get_membership_remaining_cost(membership)
            membership_used_in_months = months_of_membership_consumed(membership)
            membership_cost_paid = membership.plan.fee * membership.quantity
            plan_duration = membership.plan.duration * membership.quantity
            cost_of_membership_used = membership_used_in_months * membership_cost_paid / plan_duration.to_f
            refund  = membership_cost_paid  - cost_of_membership_used
            return refund
          end

          #
          # Function returns the fee of new plan
          #
          def get_new_membership_cost(plan)
            return plan.fee
          end

          #
          # Function fetches payment based on it's ID
          #
          def get_payment_by_id(payment_args)
            payment_dao = PAYMENT_DAO.new
            return payment_dao.get_payment_from_id(payment_args[:id])
          end

          def get_service_tax_amount(billing_amount, plan)
            return billing_amount * (plan.service_tax * 0.01) / (1 + (plan.service_tax * 0.01))
          end

          def get_membership_payment_transaction(membership_payments)
            membership_payment = get_active_payable_membership_payment(membership_payments)
            return nil if membership_payment.blank?
            transaction_service = PAYMENT_TRANSACTION_SERVICE.new(@params)
            if membership_payment.ample_order_payment_id.blank?
              return transaction_service.get_transaction_by_payment_type_and_id(membership_payment.id, PAYMENT_TYPES::MEMBERSHIP_PAYMENT)
            else
              return transaction_service.get_transaction_by_payment_type_and_id(membership_payment.ample_order_payment_id, PAYMENT_TYPES::ORDER_PAYMENT)
            end
          end

          def partial_or_full_refund_transaction(membership, refund_amount)
            membership_cost_paid = membership.plan.fee * membership.quantity
            if refund_amount == membership_cost_paid
              return TRANSACTION_STATUS::FULLY_REFUNDED
            else
              return TRANSACTION_STATUS::PARTIALLY_REFUNDED
            end
          end

          def refund_membership_amount(args)
            transaction_service = PAYMENT_TRANSACTION_SERVICE.new
            return transaction_service.refund_amount(args)
          end

          def months_of_membership_consumed(membership)
            time_helper = CommonModule::V1::TimeHelper
            month_diff = time_helper.get_difference_in_months(Time.zone.now, membership.starts_at.in_time_zone)
            return month_diff
          end

          def membership_payment_success(membership)
            payment_event = UsersModule::V1::ModelStates::MembershipPaymentEvents
            pending_payment = get_pending_payment(membership.membership_payments)
            return if pending_payment.blank?
            change_state(pending_payment, payment_event::PAYMENT_SUCCESS)
          end

          def get_pending_payment(payments)
            return nil if payments.blank?
            payment_dao = PAYMENT_DAO.new
            return payment_dao.get_pending_payment(payments)
          end

          #
          # change state function for membership payment
          # @param payment [MembershipPayment Model]
          # @param event [Integer] MembershipPaymentEvent
          #
          # @return [MembershipPaymentModel] updated membershipPayment object
          #
          def change_state(payment, event)
            payment_dao = PAYMENT_DAO.new
            payment_dao.change_state(payment, event)
          end

          def get_active_payable_membership_payment(membership_payments)
            payment_dao = PAYMENT_DAO.new
            payment_dao.get_active_payable_membership_payment(membership_payments)
          end

          def discard_refund_payment(payment)
            payment_dao = PAYMENT_DAO.new
            payment_dao.change_state(payment, UsersModule::V1::ModelStates::MembershipPaymentEvents::PAYMENT_FAIL)
          end
        end
      end
    end
  end
end
