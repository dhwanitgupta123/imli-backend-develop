#
# Module to handle user related functionality
#
module UsersModule
  module V1
    #
    # Membership Payment service class handles the membership payment related logic functionality
    #
    class MembershipOrderService < BaseModule::V1::BaseService

      def initialize(params = {})
        @params = params
        super
      end

      #
      # Function to fetch the membership properties for the client
      #
      def get_membership_properties(membership_id)
        membership_service = UsersModule::V1::MembershipService.new
        membership = membership_service.get_membership_by_id(membership_id)
        return UsersModule::V1::MembershipResponseDecorator.get_membership_order_hash(membership)
      end

      #
      # Function to activate the membership & success the payment object
      #
      def place_order(membership_id, options)
        membership_service = UsersModule::V1::MembershipService.new
        membership = membership_service.get_membership_by_id(membership_id)
        return if membership.workflow_state != UsersModule::V1::ModelStates::MembershipStates::PAYMENT_PENDING
        membership_service.change_state(membership, UsersModule::V1::ModelStates::MembershipEvents::PUT_ON_HOLD)
      end

      def capture_payment(membership_id, options)
        membership_service = UsersModule::V1::MembershipService.new
        user_service = UsersModule::V1::UserService.new
        membership_payment_dao = UsersModule::V1::MembershipModule::V1::MembershipPaymentDao.new
        user = user_service.get_user_from_user_id(options[:user][:id])
        membership = membership_service.get_membership_by_id(membership_id)
        return if membership.workflow_state != UsersModule::V1::ModelStates::MembershipStates::ON_HOLD
        pending_payment = membership_payment_dao.get_pending_payment(membership.membership_payments)
        membership_service.success_membership_payment(pending_payment, user)
        payment_args = {
          ample_order_payment_id: options[:payment][:id],
          status: UsersModule::V1::ModelStates::MembershipPaymentEvents::PAYMENT_SUCCESS
        }
        membership_payment_dao.update_model(payment_args, pending_payment, 'Membership Payment')
      end

      def cancel_order(membership_id, options)
        membership_service = UsersModule::V1::MembershipService.new
        if options[:payment_done]
          membership_service.cancel_membership({id: membership_id})
        else
          user_notification_service = UsersModule::V1::UserNotificationService.new({})
          membership = membership_service.get_membership_by_id(membership_id)
          membership_service.change_state(membership, UsersModule::V1::ModelStates::MembershipEvents::CANCEL)
          user_notification_service.notify_membership_cancelled(membership, 0)
        end
      end

      def is_valid_membership_request?(args)
        user = args[:user]
        id = args[:id]
        return false if user.blank? || id.blank?
        membership_dao = UsersModule::V1::MembershipDao.new(@params)
        begin
          pending_membership = membership_dao.get_payment_pending_membership(user)
        rescue => e
          return false
        end
        return id == pending_membership.id
      end

    end
  end
end
