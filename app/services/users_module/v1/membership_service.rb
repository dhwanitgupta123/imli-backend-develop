module UsersModule
  module V1
    #
    # Memberhsip service to handle the membership related logic 
    #
    class MembershipService < BaseModule::V1::BaseService

      PLAN_KEYS= CommonModule::V1::PlanKeys
      PLAN_STATES = CommonModule::V1::ModelStates::V1::PlanStates
      MEMBERSHIP_STATES = UsersModule::V1::ModelStates::MembershipStates
      MEMBERSHIP_EVENTS = UsersModule::V1::ModelStates::MembershipEvents
      MEMBERSHIP = UsersModule::V1::Membership
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      PAYMENT_SERVICE = UsersModule::V1::MembershipModule::V1::MembershipPaymentService
      PROFILE_SERVICE = UsersModule::V1::ProfileService
      MEMBERSHIP_HELPER = UsersModule::V1::MembershipHelper
      MEMBERSHIP_DAO = UsersModule::V1::MembershipDao
      GATEWAY = TransactionsModule::V1::GatewayType
      CACHE_UTIL = CommonModule::V1::Cache
      USER_NOTIFICATION_SERVICE = UsersModule::V1::UserNotificationService
      MEMBERSHIP_REWARD = 1

      # 
      # initialize required classes
      # 
      def initialize(params={})
        @params = params
        @plan_service = CommonModule::V1::PlanService
        @user_service = UsersModule::V1::UserService
        @membership_response_decorator = UsersModule::V1::MembershipResponseDecorator
        @user_response_decorator = UsersModule::V1::UserResponseDecorator
      end

      # 
      # [subscribe_to_membership API function]
      # @param membership_params [hash] has plan_id, required
      # 
      # @return [MembershipModel] membership object
      # 
      def subscribe_to_membership(membership_params)
        begin
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Plan' }) if membership_params.first[:plan].blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Plan ID' }) if membership_params.first[:plan][:id].blank?
          validate_subscribe_plan(membership_params.first[:plan][:id])
          user_service = @user_service.new(@params)
          user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
          membership_details = transactional_subscribe_user_to_membership(membership_params.first, user)
          membership = membership_details[:membership]
        return @membership_response_decorator.create_user_membership_payment_details_response(membership, membership_details[:payment_details])
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError, CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return @membership_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return @membership_response_decorator::create_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        end
      end

      # 
      # [payment_initiate API function]
      # @param membership_params [hash] has payment mode, required
      # 
      # @return [MembershipModel] membership object
      # 
      def payment_initiate(membership_params)
        begin
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Payment' }) if membership_params.first[:payment].blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Payment mode' }) if membership_params.first[:payment][:mode].blank?
          user_service = @user_service.new(@params)
          user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
          response = transactional_payment_initiate(membership_params.first, user)
        return response
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError, CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return @membership_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return @membership_response_decorator::create_not_found_error(e.message)
        end
      end

      # 
      # [payment_success API function]
      # @param membership_params [hash] has payment ID, required
      # 
      # @return [MembershipModel] membership object
      # 
      def payment_success(membership_params)
        begin
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Payment' }) if membership_params.first[:payment].blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Payment ID' }) if membership_params.first[:payment][:id].blank?
          user_service = @user_service.new(@params)
          payment_service = PAYMENT_SERVICE.new
          user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
          payment = payment_service.get_payment_by_id(membership_params.first[:payment])
          membership = transactional_payment_success(payment, user)
        return @membership_response_decorator.create_user_membership_response(membership) if membership.present?
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError, CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return @membership_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return @membership_response_decorator::create_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        end
      end

      # 
      # [payment_failure API function]
      # @param membership_params [hash] has payment ID, required
      # 
      # @return [MembershipModel] membership object
      # 
      def payment_failed(membership_params)
        begin
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Payment' }) if membership_params.first[:payment].blank?
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(
            CONTENT::FIELD_MUST_PRESENT % { field: 'Payment ID' }) if membership_params.first[:payment][:id].blank?
          user_service = @user_service.new(@params)
          payment_service = PAYMENT_SERVICE.new
          user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
          payment = payment_service.get_payment_by_id(membership_params.first[:payment])
          membership = transactional_payment_failed(payment, user)
        return @membership_response_decorator.create_user_membership_response(membership) if membership.present?
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError, CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return @membership_response_decorator.create_response_invalid_data_passed(e.message)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError => e
          return @membership_response_decorator::create_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::PreConditionRequiredError => e
          return @membership_response_decorator.create_response_bad_request(e.message)
        end
      end

      # 
      # Transactional function to subscribe user to function
      #
      def transactional_subscribe_user_to_membership(membership_params, user)
        transactional_function = Proc.new do |args|
          return subscribe_user_to_membership(args[:membership_params], args[:user])
        end
        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => { membership_params: membership_params, user: user }
          })
        transaction_status.run();
      end

      #
      # Function to validate if user is allowed to subscribe to the selected Plan
      #
      def validate_subscribe_plan(plan_id)
        plan_service = @plan_service.new(@params)
        @requested_plan = plan_service.get_plan_by_id(plan_id)
        if @requested_plan.plan_key == PLAN_KEYS::TRIAL
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::PLAN_NOT_AVAILABLE_FOR_SUBSCRIPTION)
        end
      end

      # 
      # logic function for subscribe_to_membership API function
      #  
      # @param membership_params [hash] has plan_id, required
      # 
      # @return [MembershipModel] membership object
      # 
      def subscribe_user_to_membership(membership_params, user)
        plan_service = @plan_service.new(@params)
        plan_id = membership_params[:plan][:id]
        requested_plan = @requested_plan.blank? ? plan_service.get_plan_by_id(plan_id) : @requested_plan
        if requested_plan.status != PLAN_STATES::ACTIVE
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::PLAN_NOT_AVAILABLE_FOR_SUBSCRIPTION)
        end
        membership_dao = MEMBERSHIP_DAO.new
        payment_pending_membership = get_payment_pending_membership(user) 
        active_user_membership = membership_dao.get_only_active_membership(user)
        membership_args = {
          plan: requested_plan,
          starts_at: Time.zone.now,
          expires_at: Time.zone.now + requested_plan.duration.months,
          quantity:  1
        }
        if payment_pending_membership.present? && payment_pending_membership.plan == requested_plan
          payment_details = calculate_membership_payment(active_user_membership, membership_args)
          return { membership: payment_pending_membership, payment_details: payment_details[:payment_mode_details] }
        end
        expire_payment_pending_memberships_of_user(user)
        membership_details = validate_existing_membership(active_user_membership, membership_args)
        user.memberships << membership_details[:membership]
        user.save_user
        return membership_details
      end

      # 
      # Transactional function to rollback DB instances in case of some error
      # during payment initiation of membership subscription
      #
      def transactional_payment_initiate(membership_params, user)
        transactional_function = Proc.new do |args|
          membership = initiate_membership_payment(args[:membership_params], args[:user])
          return @membership_response_decorator.create_user_membership_response(membership)
        end
        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => { membership_params: membership_params, user: user }
          })
        transaction_status.run();
      end

      # 
      # Function to initiate payment for a membership subscription
      #   * It fetches the payment pending membership of user
      #   * Calculates the final payment amount based on the payment mode
      #   * returns the updated membership
      #
      def initiate_membership_payment(membership_params, user)
        payment_service = PAYMENT_SERVICE.new
        membership_model = MEMBERSHIP.new
        membership_dao = MEMBERSHIP_DAO.new(@params)

        user_membership = membership_dao.get_payment_pending_membership(user)
        if payment_service.get_pending_payment(user_membership.membership_payments).blank?
          active_user_membership = membership_dao.get_only_active_membership(user)
          payment_details = calculate_membership_payment(active_user_membership, {plan: user_membership.plan})
          user_membership.membership_payments << payment_service.new_payment(payment_details[:payment_args])
        end
        payment_service.final_membership_payment(user_membership, membership_params[:payment])
        change_state(user_membership, MEMBERSHIP_EVENTS::PUT_ON_HOLD)
        return user_membership
      end

      # 
      # Transactional function when payment of membership subscription is successful
      #
      def transactional_payment_success(payment, user)
        transactional_function = Proc.new do |args|
          return success_membership_payment(args[:payment], args[:user])
        end
        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => { payment: payment, user: user }
          })
        transaction_status.run();
      end

      # 
      # Transactional function when payment of membership subscription is successful
      #
      def transactional_payment_failed(payment, user)
        transactional_function = Proc.new do |args|
          return failed_membership_payment(args[:payment], args[:user])
        end
        transaction_status = TransactionHelper.new({
          :function => transactional_function,
          :args => { payment: payment, user: user }
          })
        transaction_status.run();
      end

      # 
      # Function when payment was successful
      #  * It validates if the Payment id is of correct membership
      #  * Sets Starts at & expires at attribute of membership and updates it
      #  * If users active membership has a same plan as the new one, it updates membership args accordingly
      #  * Activates the membership
      #
      def success_membership_payment(payment, user)
        membership_model = MEMBERSHIP.new
        profile_service = PROFILE_SERVICE.new(@params)
        membership_dao = MEMBERSHIP_DAO.new(@params)
        payment_service = PAYMENT_SERVICE.new
        user_membership = membership_dao.get_on_hold_membership(user)
        pending_payment = payment_service.get_pending_payment(user_membership.membership_payments)
        starts_at = Time.zone.now
        expires_at = starts_at + user_membership.plan.duration.months
        if pending_payment == payment
          active_membership = membership_dao.get_only_active_membership(user)
          if active_membership.present?
            if(active_membership.plan == user_membership.plan)
              starts_at = active_membership.starts_at
              expires_at = active_membership.expires_at + user_membership.plan.duration.months 
            end
          end
          expire_active_and_pending_membership(user)
          membership_args = {
            starts_at: starts_at,
            expires_at: expires_at
          }
          user_membership = membership_model.update_membership(user_membership, membership_args)
          user_membership = change_state(user_membership, MEMBERSHIP_EVENTS::PAYMENT_SUCCESSFUL)
          user_notification_service = USER_NOTIFICATION_SERVICE.new(@params)
          user_notification_service.notify_membership_subscribed(user_membership)
          ApplicationLogger.debug(
            'User: ' + user.id.to_s + ' Membership: ' + user_membership.id.to_s + ' activated successfully, with plan: ' + user_membership.plan.name.to_s)
          profile_service.update_user_savings(user.profile, pending_payment.total_savings)
        else
          ApplicationLogger.debug(
            'Membership Payment id: ' + payment.id.to_s + ', is not valid payment id')
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_PAYMENT_ID)
        end
        return user_membership
      end

      # 
      # Deprecated Functioanlity
      # 
      # Function to perform following steps when payment fails
      #  * It validates if the Payment id is of correct membership
      #  * It sets membership status to deleted
      #
      # def failed_membership_payment(payment, user)
      #   membership_dao = MEMBERSHIP_DAO.new(@params)
      #   payment_service = PAYMENT_SERVICE.new
      #   user_membership = membership_dao.get_payment_pending_membership(user)
      #   payment_pending = payment_service.get_pending_payment(user_membership.membership_payments)
      #   if payment_pending == payment
      #     user_membership = payment_service.change_state(payment_pending, MEMBERSHIP_EVENTS::PAYMENT_FAILED)
      #     ApplicationLogger.debug('User id: ' + user.id.to_s + ' membership id: ' + user_membership.id.to_s + ' moved to deleted state')
      #   else
      #     ApplicationLogger.debug('Membership Payment id: ' + payment.id.to_s + ', is not valid payment id')
      #     raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_PAYMENT_ID)
      #   end
      #   return user_membership
      # end


      # 
      # Function to validate exisiting membership of User
      #  * if existing_user_membership is blank then we create a new membership
      #  * if existing_user_membership has a plan which is a part of request, we extend the
      #    membership expiry date by that duration days
      #  * if existing_user_membership has a plan apart from plan in request, we force_expire that
      #    plan & associate requested plan in new membership
      # 
      #  TO-DO: Money refund for the exisiting plan, which is not utilized
      #
      def validate_existing_membership(existing_membership, membership_args)
        payment_service = PAYMENT_SERVICE.new
        membership_model = MEMBERSHIP.new
        payment_details = calculate_membership_payment(existing_membership, membership_args)
        if existing_membership.present?
          if existing_membership.plan == membership_args[:plan]
            membership_args[:starts_at] = existing_membership.starts_at
            membership_args[:expires_at] = existing_membership.expires_at + membership_args[:plan].duration.months
            membership_args[:quantity] = membership_args[:quantity] + existing_membership.quantity
          end
        end
        new_membership = membership_model.new_membership(membership_args)
        new_membership.membership_payments << payment_service.new_payment(payment_details[:payment_args])
        new_membership.save_membership
        new_membership = move_user_membership_to_pending_state(membership_args[:plan], new_membership)
        return { membership: new_membership, payment_details: payment_details[:payment_mode_details] }
      end

      # 
      # Function to calculate cost of membership
      #
      def calculate_membership_payment(membership, membership_args)
        payment_service = PAYMENT_SERVICE.new
        membership_payment = payment_service.calculate_membership_payment(membership, membership_args[:plan])
        return membership_payment
      end

      # 
      # Function move membership to 
      #  * Pending if it is a 'trial' membership plan
      #  * payment_pending if it is not a 'trial' membership plan
      #
      def move_user_membership_to_pending_state(plan, membership)
        if plan.plan_key == PLAN_KEYS::TRIAL
          event = MEMBERSHIP_EVENTS::INITIATE_TRIAL  
        else
          event = MEMBERSHIP_EVENTS::INITIATE_PAYMENT
        end
        membership = change_state(membership, event)
        return membership
      end

      #
      # Function to activate Ample-Sample membership of User if it exists
      #
      def activate_ample_sample_membership(user)
        membership_dao = MEMBERSHIP_DAO.new(@params)
        membership_model = MEMBERSHIP.new
        payment_service = PAYMENT_SERVICE.new
        pending_membership = membership_dao.get_only_pending_membership(user)
        if pending_membership.present?
          duration = MEMBERSHIP_HELPER.get_membership_duration(pending_membership)
          membership_model = MEMBERSHIP.new
          membership_args = {
            starts_at: Time.zone.now,
            expires_at: Time.zone.now + duration.months
          }
          pending_membership = membership_model.update_membership(pending_membership, membership_args)
          activated_membership = change_state(pending_membership, MEMBERSHIP_EVENTS::ACTIVATE)
          payment_service.membership_payment_success(activated_membership)
          ApplicationLogger.debug(
            'User: ' + user.id.to_s + ', on first Successful order place, Ample Sample Membership: ' + activated_membership.id.to_s + ' activated successfully')
        end
      end

      # 
      # Function to fetch the active membership of the User & expire payment pending membership
      #
      def expire_payment_pending_memberships_of_user(user)
        user_memberships = user.memberships.where(workflow_state: [MEMBERSHIP_STATES::PAYMENT_PENDING, MEMBERSHIP_STATES::ON_HOLD])
        user_memberships.each do |membership|
          change_state(membership, MEMBERSHIP_EVENTS::FORCE_EXPIRE)
        end
      end

      # 
      # Function to add one month extra free trial membership to the referred by user
      #
      def reward_referral_one_month_membership(referred_by)
        membership_dao = MEMBERSHIP_DAO.new(@params)
        membership_model = MEMBERSHIP.new
        user_service = @user_service.new(@params)
        referred_by = user_service.get_user_from_user_id(referred_by.to_i)
        if validate_membership_limit(referred_by)
          membership = membership_dao.get_ample_sample_membership(referred_by.memberships)
          if membership.present?
            membership_args = {
              expires_at: membership.expires_at + MEMBERSHIP_REWARD.months
            }
            membership = membership_model.update_membership(membership, membership_args)
            referred_by.benefit_limit -= 1
            referred_by.save_user
            ApplicationLogger.debug('One  month free membership Awarded to user:' + referred_by.id.to_s)
          end
        end
      end

      #
      # Function to get active or pending membership of User
      #
      def get_pending_or_active_membership(user)
        membership_dao = MEMBERSHIP_DAO.new(@params)
        return membership_dao.get_pending_or_active_membership(user)
      end


      # 
      # Function to expire the PENDING & ACTIVE membership of user
      # When New membership payment has happened successfully
      #
      def expire_active_and_pending_membership(user)
        user_memberships = user.memberships.where(workflow_state: [MEMBERSHIP_STATES::ACTIVE, MEMBERSHIP_STATES::PENDING, MEMBERSHIP_STATES::PENDING_EXPIRY])
        user_memberships.each do |membership|
          if membership.active? || membership.pending? ||membership.pending_expiry?
            change_state(membership, MEMBERSHIP_EVENTS::FORCE_EXPIRE)
          end
        end
      end

      #
      # Function to check if user's benefit limit is not zero
      #
      def validate_membership_limit(user)
        if user.benefit_limit > 0
          return true
        else
          ApplicationLogger.debug('Benefit Limit of user: ' + user.id.to_s + ' expired')
          return false
        end
      end

      # 
      # change state function for membership
      # @param membership [Membership Model] 
      # @param event [Integer] MembershipEvent
      # 
      # @return [MembershipModel] updated membership object
      # 
      def change_state(membership, event)
        begin
          case event.to_i
            when MEMBERSHIP_EVENTS::INITIATE_TRIAL
              membership.trigger_event('initiate_trial')
            when MEMBERSHIP_EVENTS::INITIATE_PAYMENT
              membership.trigger_event('initiate_payment')
            when MEMBERSHIP_EVENTS::ACTIVATE
              membership.trigger_event('activate')
            when MEMBERSHIP_EVENTS::PAYMENT_SUCCESSFUL
              membership.trigger_event('payment_successful')
            when MEMBERSHIP_EVENTS::PAYMENT_FAILED
              membership.trigger_event('payment_failed')
            when MEMBERSHIP_EVENTS::EXPIRE
              membership.trigger_event('expire')
            when MEMBERSHIP_EVENTS::FORCE_EXPIRE
              membership.trigger_event('force_expire')
            when MEMBERSHIP_EVENTS::CANCEL
              membership.trigger_event('cancel')
            when MEMBERSHIP_EVENTS::PENDING_EXPIRE
              membership.trigger_event('pending_expire')
            when MEMBERSHIP_EVENTS::REACTIVATE
              membership.trigger_event('reactivate')
            when MEMBERSHIP_EVENTS::PUT_ON_HOLD
              membership.trigger_event('put_on_hold')
            else
              ApplicationLogger.info('Not a valid Event: ' + event.to_s + ' for Membership transition')
              raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
          end
         end
         ApplicationLogger.debug(
          'Membership: ' + membership.id.to_s + ' moved to state: ' + membership.workflow_state.to_s)
        return membership
      end

      #
      # Function to validate User Membership and return membership if active else all plans
      #
      def validate_membership
        user_service = @user_service.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        membership = get_pending_or_active_membership(user)
        if membership.present?
          membership_hash = @membership_response_decorator.get_membership_hash(membership)
          return @membership_response_decorator.create_valid_membership_response(membership_hash)
        else
          on_hold_membership = get_on_hold_membership(user)
          payment_pending_membership = get_payment_pending_membership(user)
          if on_hold_membership.present?
            return @membership_response_decorator.create_pre_condition_failed_response(CONTENT::MEMBERSHIP_IN_HOLD_STATE)
          end
          if payment_pending_membership.present?
            selected_plan = payment_pending_membership.plan
          end
          plan_dao= CommonModule::V1::PlanDao.new
          plans = plan_dao.get_all_active_plans
          return CommonModule::V1::PlanResponseDecorator.create_available_plans_response(plans, selected_plan)
        end
      end

      #
      # API Function to cancel the User membership
      #
      _RunInTransactionBlock_
      def cancel_membership(params)
        return @membership_response_decorator.create_response_bad_request(CONTENT::FIELD_MUST_PRESENT%{ field: 'Membership ID' }) if params[:id].blank?
        membership = get_membership_by_id(params[:id])
        payment_service = PAYMENT_SERVICE.new
        refund_args = payment_service.refund_membership_payment(membership)
        refund_payment = payment_service.new_payment(refund_args)
        membership.membership_payments << refund_payment
        transaction_status = partial_or_full_refund_transaction(membership, refund_args[:net_total])
        transaction = get_membership_payment_transaction(membership)
        if transaction.blank?
          payment_service.discard_refund_payment(refund_payment)
          return @membership_response_decorator.create_response_bad_request(CONTENT::RESOURCE_NOT_FOUND%{resource: 'Membership Payment Transaction'})
        end
        refund_status = refund_membership_amount({
          transaction: transaction,
          refund_amount: refund_args[:net_total],
          transaction_status: transaction_status
        })
        if refund_status == transaction_status
          user_notification_service = USER_NOTIFICATION_SERVICE.new(@params)
          change_state(membership, MEMBERSHIP_EVENTS::CANCEL)
          user_notification_service.notify_membership_cancelled(membership, refund_args[:net_total])
          @membership_response_decorator.create_user_membership_response(membership)
        else
          payment_service.discard_refund_payment(refund_payment)
          return @membership_response_decorator.create_response_bad_request(CONTENT::REFUND_FAILED)
        end
      end

      def get_membership_by_id(id)
        membership_dao = MEMBERSHIP_DAO.new(@params)
        return membership_dao.get_membership_by_id(id)

      end

      def calculate_membership_refund_amount(membership)
        payment_service = PAYMENT_SERVICE.new(@params)
        return payment_service.get_membership_remaining_cost(membership)
      end

      def get_membership_payment_transaction(membership)
        payment_service = PAYMENT_SERVICE.new(@params)
        return payment_service.get_membership_payment_transaction(membership.membership_payments)
      end

      def partial_or_full_refund_transaction(membership, refund_amount)
        payment_service = PAYMENT_SERVICE.new(@params)
        return payment_service.partial_or_full_refund_transaction(membership, refund_amount)
      end

      def refund_membership_amount(args)
        payment_service = PAYMENT_SERVICE.new(@params)
        return payment_service.refund_membership_amount(args)
      end

      #
      # Function to expire the membership of the user
      # If days to expire is <=  SMS/NOTIF/EMAIL start day & >= SMS/Notif/Email end day we send sms, notif & email
      # 
      def self.expire_membership
        sms_start_day = CACHE_UTIL.read_int('SMS_START_DAY') || 3
        sms_end_day = CACHE_UTIL.read_int('SMS_END_DAY') || 1
        notification_start_day = CACHE_UTIL.read_int('NOTIFICATION_START_DAY') || 6
        notification_end_day = CACHE_UTIL.read_int('NOTIFICATION_END_DAY') || 4
        email_start_day = CACHE_UTIL.read_int('EMAIL_START_DAY') || 7
        email_end_day = CACHE_UTIL.read_int('EMAIL_END_DAY') || 6
        user_notification_service = USER_NOTIFICATION_SERVICE.new(@params)
        membership_dao = MEMBERSHIP_DAO.new(@params)
        active_memberships = membership_dao.get_all_active_and_pending_expiry_memberships
        return if active_memberships.blank?
        active_memberships.each do |membership|
          days_for_expiry = ((membership.expires_at - Time.zone.now)/1.day).round
          if days_for_expiry <= 0
            membership_service = UsersModule::V1::MembershipService.new
            membership_service.expire_membership_and_notify_user(membership)
          else
            if days_for_expiry <= sms_start_day && days_for_expiry >= sms_end_day
              user_notification_service.membership_expiry_sms(membership)
            end
            if days_for_expiry <= notification_start_day && days_for_expiry >= notification_end_day
              user_notification_service.membership_expiry_notification(membership)
            end
            if days_for_expiry <= email_start_day && days_for_expiry >= email_end_day
              user_notification_service.membership_expiry_email(membership)
            end
          end
        end 
      end

      #
      # Function to expire User's Membership & notify the User
      #
      def expire_membership_and_notify_user(membership)
        user_notification_service = USER_NOTIFICATION_SERVICE.new(@params)
        change_state(membership, MEMBERSHIP_EVENTS::EXPIRE)
        user_notification_service.notify_membership_expired(membership)
      end

      #
      # Function to expire the ample sample membership of the user
      #
      def pending_expire_user_ample_sample_membership(user)
        membership_dao = MEMBERSHIP_DAO.new
        membership = membership_dao.get_ample_sample_membership(user.memberships)
        return if membership.blank? || membership.expired?
        change_state(membership, MEMBERSHIP_EVENTS::PENDING_EXPIRE)
      end

      #
      # Function to reactivate the membership from pending expiry state if no other active membership presentg
      #
      def reactivate_pending_expiry_ample_sample_membership(user)
        membership_dao = MEMBERSHIP_DAO.new
        membership = membership_dao.get_only_active_membership(user)
        return if membership.present?
        membership = get_pending_expiry_ample_sample_membership(user)
        return if membership.blank?
        change_state(membership, MEMBERSHIP_EVENTS::REACTIVATE)
      end

      #
      # Function to fetch pending expiry ample sample membership
      #
      def get_pending_expiry_ample_sample_membership(user)
        membership_dao = MEMBERSHIP_DAO.new
        membership = membership_dao.get_ample_sample_membership(user.memberships)
        return membership.present? && membership.pending_expiry? ? membership : nil
      end

      #
      # Function to expire pending expiry ample sample membership
      #
      def expire_pending_expiry_ample_sample_membership(user)
        membership = get_pending_expiry_ample_sample_membership(user)
        return if membership.blank?
        membership_model = MEMBERSHIP.new
        membership_model.update_membership(membership, { expires_at: Time.zone.now })
        expire_membership_and_notify_user(membership)
      end

      #
      # Function to fetch Payment pending membership of User
      # If not present return true
      #
      def get_payment_pending_membership(user)
        membership_dao = MEMBERSHIP_DAO.new
        begin
          membership = membership_dao.get_payment_pending_membership(user)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError
          return nil
        end
        return membership
      end

      #
      # Function to fetch on hold membership of User
      # If not present return true
      #
      def get_on_hold_membership(user)
        membership_dao = MEMBERSHIP_DAO.new
        begin
          membership = membership_dao.get_on_hold_membership(user)
        rescue CUSTOM_ERRORS_UTIL::ResourceNotFoundError
          return nil
        end
        return membership
      end

      #
      # Function to fetch last expired membership of the user
      #
      def get_last_expired_membership(memberships)
        return nil if memberships.blank?
        membership_dao = MEMBERSHIP_DAO.new
        last_expired_membership = membership_dao.get_last_expired_membership(memberships)
        return last_expired_membership
      end
    end
  end
end  
