module UsersModule
  module V1
    #
    # Service class to handle all the profile related logics
    #
    class ProfileService < BaseModule::V1::BaseService

      PROFILE_MODEL = UsersModule::V1::Profile
      USER_SERVICE = UsersModule::V1::UserService
      PROFILE_DAO = UsersModule::V1::ProfileDao
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      PROFILE_RESPONSE_DECORATOR = UsersModule::V1::ProfileResponseDecorator
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator
      GENERAL_HELPER = CommonModule::V1::GeneralHelper
      ADDRESS_HELPER = AddressModule::V1::AddressHelper
      def initialize(params='')
        @params = params
      end
      
      #
      # Function to create new profile of a new user
      #
      def new_profile
        return PROFILE_MODEL.new
      end

      # 
      # Function to update the savings of User
      #
      def update_user_savings(profile, savings)
        if profile.present?
          profile.savings += savings
          profile.save_profile
        end
      end

      #
      # API function to add the pincode of the User's area
      #
      def add_pincode(params)
        begin
          validate_add_pincode_request(params)
          add_pincode_to_profile(params)
          return PROFILE_RESPONSE_DECORATOR.create_success_response
        rescue CUSTOM_ERRORS_UTIL::RecordNotFoundError => e
          return PROFILE_RESPONSE_DECORATOR.create_not_found_error(e.message)
        rescue CUSTOM_ERRORS_UTIL::InsufficientDataError => e
          return PROFILE_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidDataError => e
          return PROFILE_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::InvalidArgumentsError => e
          return PROFILE_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        rescue CUSTOM_ERRORS_UTIL::UnAuthenticatedUserError => e
          return USER_RESPONSE_DECORATOR.create_response_unauthenticated_user(e.message)
        end
      end

      #
      # Service function to add the pincode to the User's profile
      #
      def add_pincode_to_profile(params)
        user_service = USER_SERVICE.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        profile = user.profile
        profile.pincode = params[:profile][:pincode]
        profile.save_profile
      end

      #
      # Function to validate if the pincode is correct & valid
      #
      def validate_add_pincode_request(params)
        error_array = []
        error_array.push('Request Parameter') if params.blank?
        error_array.push('Profile') if params[:profile].blank?
        error_array.push('Pincode') if params[:profile][:pincode].blank?
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERRORS_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT%{ field: error_string })
        end
        raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::INVALID_PINCODE) unless ADDRESS_HELPER.valid_pincode?(params[:profile][:pincode])
        profile_dao = PROFILE_DAO.new
        area = profile_dao.get_area_from_pincode(params[:profile][:pincode])
        if area.present?
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(CONTENT::MUST_BE_ACTIVE%{ name: 'Area' }) unless area.active?
        end
      end
    end
  end
end
