module TransactionsModule
  module V1
    class PaymentTransactionsService < BaseModule::V1::BaseService

      CONTENT = CommonModule::V1::Content
      PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR = TransactionsModule::V1::PaymentTransactionsResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      GATEWAY_TYPE = TransactionsModule::V1::GatewayType
      POST_ORDER_PAYMENT_SERVICE = PaymentModule::V1::PostOrderPaymentService
      TRANSACTION_STATUS = TransactionsModule::V1::TransactionStatus
      TRANSACTION_SERVICE = TransactionsModule::V1::TransactionsService
      TRANSACTIONS_DAO = TransactionsModule::V1::TransactionsDao
      PAYMENT_TYPES = TransactionsModule::V1::PaymentTypes

      def initialize(params = '')
        @params = params
      end

      #
      # API function to inititiate trasaction
      #
      _ExceptionHandler_
      def initiate_transaction(initiate_transaction_params)
        validate_initiate_request(initiate_transaction_params)
        return enact_initiation(initiate_transaction_params)
      end

      #
      # Service function to initiate transaction based on the gate way
      #
      def enact_initiation(initiate_transaction_params)
        if initiate_transaction_params[:gateway].to_i == GATEWAY_TYPE::CITRUS_WEB
          return initiate_transaction_via_citrus(initiate_transaction_params)
        elsif initiate_transaction_params[:gateway].to_i == GATEWAY_TYPE::RAZORPAY
          return initiate_transaction_via_razorpay(initiate_transaction_params)
        elsif initiate_transaction_params[:gateway].to_i == GATEWAY_TYPE::PAYTM
          return initiate_transaction_via_paytm(initiate_transaction_params)
        else
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(CONTENT::WRONG_GATEWAY)
        end
      end

      #
      # API function to update trasaction
      #
      _ExceptionHandler_
      def update_transaction(update_transaction_params)
        validate_update_request(update_transaction_params)
        return enact_update(update_transaction_params)
      end

      #
      # Service function to update transaction based on the gate way
      #
      def enact_update(update_transaction_params)
        if update_transaction_params[:gateway].to_i == GATEWAY_TYPE::CITRUS_WEB
          return update_citrus_transaction(update_transaction_params)
        elsif update_transaction_params[:gateway].to_i == GATEWAY_TYPE::RAZORPAY
          return update_razorpay_transaction(update_transaction_params)
        elsif update_transaction_params[:gateway].to_i == GATEWAY_TYPE::PAYTM
          return update_paytm_transaction(update_transaction_params)
        else
          return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(CONTENT::WRONG_GATEWAY)
        end
      end

      #
      # Service function to record transaction
      # Currently, supporting only POST_ORDER
      #
      # @param record_transaction_params [JSON] [Transaction hash specific to payment mode]
      #
      # @return [JSON] [Response to be sent to the user]
      #
      def record_transaction(record_transaction_params)
        ApplicationLogger.info('Recording transaction with POST_ORDER gateway')
        post_order_payment_service = POST_ORDER_PAYMENT_SERVICE.new(@params)
        return post_order_payment_service.record_post_order_transaction(record_transaction_params)
      end

      def get_transaction_by_payment_type_and_id(payment_id, payment_type)
        return nil if payment_id.blank?
        return nil if payment_type.blank?
        transaction_dao = TRANSACTIONS_DAO.new
        return transaction_dao.get_transaction_by_payment_type_and_id(payment_id, payment_type)
      end

      def refund_amount(args)
        refund_amount_via_gateway(args)
      end

      def refund_amount_via_gateway(args)
        args[:transaction] = args[:transaction].to_a.first
        transaction_args = create_refund_transaction_args(args)
        transactions_dao = TRANSACTIONS_DAO.new
        refund_transaction = transactions_dao.create_transaction(transaction_args)
        if GATEWAY_TYPE::RAZORPAY == args[:transaction].gateway
          ApplicationLogger.info('Refund initiated with Razorpay gateway')
          razorpay_payment_service = RAZORPAY_PAYMENT_SERVICE.new(@params)
          return razorpay_payment_service.refund_amount(args, refund_transaction)
        else
          return args[:transaction_status]
        end
      end

      private

      #
      # Function to validate transation initiation request
      #
      def validate_initiate_request(initiate_transaction_params)
        error_array = []
        error_array.push('Payment ID') if initiate_transaction_params.blank? || initiate_transaction_params[:payment_id].blank?
        error_array.push('Payment type') if initiate_transaction_params[:payment_type].blank?
        error_array.push('Gateway') if initiate_transaction_params[:gateway].blank?
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT%{ field: error_string })
        end
      end

      #
      # Function to validate update transaction request
      #
      def validate_update_request(update_transaction_params)
        error_array = []
        error_array.push('Transaction ID') if update_transaction_params.blank? || update_transaction_params[:id].blank?
        error_array.push('Transaction status') if update_transaction_params[:status].blank?
        error_array.push('Gateway') if update_transaction_params[:gateway].blank?
        error_array.push('Razorpay details') if update_transaction_params[:gateway].to_i == GATEWAY_TYPE::RAZORPAY && update_transaction_params[:status].to_i == TRANSACTION_STATUS::SUCCESS && update_transaction_params[:razorpay].blank?
        error_array.push('Razorpay payment ID') if update_transaction_params[:gateway].to_i == GATEWAY_TYPE::RAZORPAY && update_transaction_params[:razorpay].present? && update_transaction_params[:status].to_i == TRANSACTION_STATUS::SUCCESS && update_transaction_params[:razorpay][:payment_id].blank?
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::FIELD_MUST_PRESENT%{ field: error_string })
        end
      end

      #
      # Function to initiate TXN via citrus
      #
      def initiate_transaction_via_citrus(initiate_transaction_params)
        ApplicationLogger.info('Transaction initiated with Citrus web gateway')
        citrus_payment_service = PaymentModule::V1::CitrusPaymentService.new(@params)
        response = citrus_payment_service.generate_url(initiate_transaction_params)
        return PAYMENT_TRANSACTIONS_RESPONSE_DECORATOR.create_citrus_response(response)
      end

      #
      # Function to initiate TXN via razorpay
      #
      def initiate_transaction_via_razorpay(initiate_transaction_params)
        ApplicationLogger.info('Transaction initiated with Razorpay gateway')
        razorpay_payment_service = PaymentModule::V1::RazorpayPaymentService.new(@params)
        return razorpay_payment_service.initiate_transaction(initiate_transaction_params)
      end

      #
      # Function to initiate TXN via paytm
      #
      def initiate_transaction_via_paytm(initiate_transaction_params)
        ApplicationLogger.info('Transaction initiated with PayTm web gateway')
        paytm_payment_service_class = PaymentModule::V1::PaytmPaymentService.new(@params)
        return paytm_payment_service_class.initiate_transaction(initiate_transaction_params)
      end

      #
      # Function to update TXN via Citrus
      #
      def update_citrus_transaction(update_transaction_params)
        ApplicationLogger.info('Transaction updated with Citrus web gateway')
        citrus_payment_service = PaymentModule::V1::CitrusPaymentService.new(@params)
        return citrus_payment_service.update_transaction(update_transaction_params)
      end

      #
      # Function to update TXN via Razorpay
      #
      def update_razorpay_transaction(update_transaction_params)
        ApplicationLogger.info('Transaction updated with Razorpay gateway')
        razorpay_payment_service = PaymentModule::V1::RazorpayPaymentService.new(@params)
        return razorpay_payment_service.update_transaction(update_transaction_params)
      end 

      #
      # Function to update TXN via Paytm
      #
      def update_paytm_transaction(update_transaction_params)
        ApplicationLogger.info('Transaction updated with Razorpay gateway')
        razorpay_payment_service = PaymentModule::V1::PaytmPaymentService.new(@params)
        return razorpay_payment_service.update_transaction(update_transaction_params)
      end 
      
      #
      # Function to create args for refund transaction
      #
      def create_refund_transaction_args(args)
        transaction = args[:transaction]
        amount = args[:refund_amount]
        return {
          payment_id: transaction.payment_id,
          payment_type: transaction.payment_type,
          amount: amount,
          user_email_id: transaction.user_email_id,
          user_id: transaction.user_id,
          currency: transaction.currency,
          gateway: transaction.gateway
        }
      end
    end
  end
end
