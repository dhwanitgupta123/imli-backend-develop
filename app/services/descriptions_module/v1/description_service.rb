module DescriptionsModule
  module V1
    class DescriptionService < BaseModule::V1::BaseService

      DESCRIPTION_HELPER = DescriptionsModule::V1::DescriptionHelper
      DESCRIPTION_MODEL = DescriptionsModule::V1::Description
      CACHE_CLIENT = CacheModule::V1::CacheClient

      def initialize(params = '')
        @params = params
        @description_dao = DescriptionsModule::V1::DescriptionDao.new(params)
      end

      def transactional_create_descriptions(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return create_descriptions(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      # 
      # It takes the description tree as input and accordingly creates all the required
      # nodes
      #
      # @param description_tree [Tree] tree where each node contain childs, and other data
      # 
      # @return [Model] root node of description tree 
      #
      def create_descriptions(description_tree)

        object = DESCRIPTION_HELPER.validate_resource(description_tree[:resource], description_tree[:resource_id])
        
        if object.description.present?
          raise CUSTOM_ERROR_UTIL::RecordAlreadyExistsError.new(object.description[:id].to_s)
        end

        parent_description = @description_dao.create(description_hash(description_tree).
                                merge(description_of_id: description_tree[:resource_id], 
                                  description_of_type: description_tree[:resource]))

        childs = description_tree[:childs]

        create_all_childs(parent_description, childs)

        return parent_description.childs(true)
      end

      # 
      # This function recursively creates all the child nodes
      #
      # @param parent_description [Model] parent node of childs
      # @param childs [Array] array of hash  
      #
      def create_all_childs(parent_description, childs)
        
        return if childs.blank?

        childs.each do |child|
          description = @description_dao.create(description_hash(child).merge(parent_id: parent_description.id))
          parent_description.childs.push(description)
          create_all_childs(description, child[:childs]) if child[:childs].present?
        end
      end

      def transactional_update_descriptions(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return update_descriptions(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      # 
      # This function update the description
      #
      # @param args [Hash] containing description to update and from which to update
      # 
      # @return [Model] root node
      #
      def update_descriptions(args)

        root_description = @description_dao.get_by_id(args[:id])

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_ROOT_DESCRIPTION) unless root_description.root? 

        description_tree = args

        compare_and_update(root_description, description_tree)

        return root_description
      end

      # 
      # This function compare the two trees (current tree and tree to update)
      # and accordingly create, update or delete the node
      #
      # @param description [Model] description node
      #
      # @param updated_description [Hash] new node values
      #
      def compare_and_update(description, updated_description)

        # updating the basic information example heading, data of node 
        @description_dao.update(description_hash(updated_description), description)
        
        childs = description.child_ids 
        updated_childs = []
        #
        # getting childs which need to be updated assuming child containing id when there is update
        #
        updated_childs = updated_description[:childs].map {|x| x[:id]} if updated_description[:childs].present?

        #
        # currently present child - child in request are nodes which need to be deleted
        deleted_childs = childs - updated_childs

        #
        # getting nodes that need to be added and that need to be updated
        segregated_childs = DESCRIPTION_HELPER.segregate_new_childs(updated_description[:childs])
        new_childs = segregated_childs[:new_childs]

        if new_childs.present?
          create_all_childs(description, new_childs)
        end

        @description_dao.delete_childs(deleted_childs) if deleted_childs.present?

        childs_to_update = segregated_childs[:to_update_childs]

        return if childs_to_update.blank?

        childs_to_update.each do |child|
          compare_and_update(@description_dao.get_by_id(child[:id]), child)
        end
      end

      # 
      # This function returns description tree by id
      #
      # @param id [Integer] root description id
      # 
      # @return [Hash] description tree hash
      #
      def get_description_tree(id)

        description_tree = CACHE_CLIENT.get({key: id}, DESCRIPTION_MODEL.name)

        return description_tree if description_tree.present?

        root_description = @description_dao.get_by_id(id)

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_ROOT_DESCRIPTION) unless root_description.root? 

        return root_description.childs(true)

      end

      # 
      # creating hash from description
      #
      # @param description [model] description
      # 
      # @return [Hash] description hash
      #
      def description_hash(description)
        {
          heading: description[:heading],
          data: description[:data]
        }
      end
    end
  end
end
