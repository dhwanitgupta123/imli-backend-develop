#
#This is super class for each type of Activity Analyzer
# 
module AnalyzerModule
  module V1
    class ActivityAnalyzer
      def initialize
        @activity_log = LoggingModule::V1::ActivityLog
      end
    end
  end
end