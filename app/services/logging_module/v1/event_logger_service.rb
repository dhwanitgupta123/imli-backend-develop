module LoggingModule
  module V1
    module EventLoggerService
      extend self

      #
      # This function create record and push the dump to kafka
      #
      # @params args [Hash] hash containing fields to log in kafka
      #
      def log_event(args)

        record = LoggingModule::V1::EventLoggerHelper.create_record(args)

        Imli::Logger::LoggerClient.add_record(record)
      end

      def extract_events_by_filters(filters)
        query = LoggingModule::V1::EventQueryHelper.build_select_query(filters)
        Imli::Druid::SelectQueryResponseReader.new(query.send)
      end
    end
  end
end
