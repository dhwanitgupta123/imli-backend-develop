module LoggingModule
  module V1
    class ActivityLogger

      def initialize
        @user_activity = LoggingModule::V1::ActivityLog
        @custom_error_util = CommonModule::V1::CustomErrors
      end

      # 
      # save the record in MongoDB  
      # 
      # @param log_params [hash] Consist of information need to be logged
      # 
      def log_activity(log_params)

        raise @custom_error_util::InsufficientDataError.new if log_params.nil?

        activity_log = @user_activity.new
        activity_log.api_name = log_params['api_name']
        activity_log.phone_number = log_params['phone_number']
        activity_log.request_ip = log_params['request_ip']
        activity_log.request_url = log_params['request_url']
        activity_log.request_query_string = log_params['request_query_string']
        activity_log.response_error = log_params['response_error']
        activity_log.response_status = log_params['response_status']
        activity_log.device_id = log_params['device_id']
        activity_log.app_version = log_params['app_version']
        activity_log.platform_type = log_params['platform_type']
        activity_log.time = log_params['time']
        api_spawn_time_string = log_params['api_spawn_time']
        api_spawn_time = Time.parse(api_spawn_time_string) if api_spawn_time_string.present?
        activity_log.api_spawn_time = api_spawn_time if api_spawn_time.present?
        activity_log.save

      end
    end
  end
end
