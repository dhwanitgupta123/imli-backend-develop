module LoggingModule
  module V1
    module ErrorLoggerService
      extend self

      #
      # This function create record and push the dump to kafka
      #
      # @params args [Hash] hash containing fields to log in kafka
      #
      def log_error(args)

        record = LoggingModule::V1::ErrorLoggerHelper.create_record(args)

        Imli::Logger::LoggerClient.add_record(record)
      end

      #
      # This function create record and push the dump to kafka async
      #
      # @params args [Hash] hash containing fields to log in kafka
      #
      def log_error_async(e)

        return if Rails.env != 'production'

        LoggingModule::V1::ErrorLoggerWorker.perform_async({
                                                             error: create_error_hash(e),
                                                             request: create_request_hash(USER_SESSION[:request])
                                                           })
      end

      private

      def create_request_hash(request)

        return {} if request.blank?

        return {
          url: request.url,
          path: request.path,
          params: request.params
        }

      end

      def create_error_hash(e)
        return {} if e.blank?

        error_hash = {
          message: e.message,
          backtrace: e.backtrace,
          name: ''
        }

        error_hash[:name] = e.class.name if e.class.present?

        return error_hash
      end
    end
  end
end
