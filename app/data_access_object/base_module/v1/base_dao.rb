module BaseModule
  module V1
    class BaseDao

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      PAGINATION_UTIL = CommonModule::V1::Pagination

      def initialize(params = '')
        @params = params
        @custom_error_util = CommonModule::V1::CustomErrors
      end

      #
      # this function only permit attributes required for the table
      #
      # @return [Hash] filterd params
      #
      def model_params(args, model)
        args = ActionController::Parameters.new(args)
        args.permit(model.attribute_names)
      end

      #
      # Create the record
      #
      # @param args [hash] model arguments
      #
      # @return [Model]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create_model(args, model, message='')
        message = 'Model ' if message.blank?
        new_model = model.new(model_params(args, model))

        begin
          new_model.save!
          return new_model
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create ' + message + ', ' + e.message)
        end
      end

      #
      # Update the given model
      #
      # @param args [hash] arguments to update
      # @param model [Model] model
      #
      # @return [Model] updated model
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update_model(args, record, message = '')
        message = 'Model' if message.blank?
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(message +  ' not found') if record.nil? || args.nil?
        begin
          record.update_attributes!(model_params(args, record))
          return record
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update ' + message + ', '+ e.message)
        end
      end

      #
      # get the mode by id
      #
      # @param id [Integer] model id
      # @param model [Model] model
      #
      # @return [Model]  model if model exists else return nil
      #
      def get_model_by_id(model, id)
        model.find_by(id: id)
      end

      #
      # get the model by field
      #
      # @param field [String] field value
      # @param model [Model] model
      # @param field_name String] field name
      #
      # @return [Model]  model if model exists else return nil
      #
      def get_model_by_field(model, field_name, field_value)
        model_instance = model.find_by(field_name => field_value)
        if model_instance.blank?
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT::RESOURCE_NOT_FOUND % {resource: model})
        end
        return model_instance
      end

      #
      # get all the records in the table
      #
      # @param model [Model] model
      #
      # @return [Array] array of model object
      #
      def get_all_element(model, pagination_params = {})
        set_pagination_properties(pagination_params, model)

        if @state.blank?
          elements = model.order(@sort_order)
        else
          elements = model.where({ status: @state }).order(@sort_order)
        end
        page_count = (elements.count / @per_page).ceil
        elements = elements.limit(@per_page).offset((@page_no - 1) * @per_page)
        return {elements: elements, page_count: page_count}
      end

      #
      # Apply pagination over some given set of elements
      #
      # @param model [Class] [Model Class]
      # @param filtered_elements [Array] [ActiveRecord Array of model object elements]
      # @param pagination_params = {} [JSON] [Hash of paginated properties]
      #
      # @return [JSON] [Hash of elements and its page count]
      #
      def get_paginated_element(model, filtered_elements, pagination_params = {})
        set_pagination_properties(pagination_params, model)
        # Apply state filtering over elements
        if @state.blank?
          elements = filtered_elements.order(@sort_order)
        else
          elements = filtered_elements.where({ status: @state }).order(@sort_order)
        end
        # Now compute total page count based on final filtered elements
        page_count = (elements.count / @per_page).ceil
        # apply per page limit on filterd elements
        elements = elements.limit(@per_page).offset((@page_no - 1) * @per_page)

        return {elements: elements, page_count: page_count}
      end

      #
      # setting pagination parameters for the model instance
      #
      def set_pagination_properties(pagination_params, model)
        @per_page = (pagination_params[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (pagination_params[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = pagination_params[:sort_by] || PAGINATION_UTIL::SORT_BY
        order = pagination_params[:order] || PAGINATION_UTIL::ORDER
        @state = pagination_params[:state]
        @state = @state.to_i if @state.present?

        # if @state.present? && !COMMON_MODEL_STATES::ALLOWED_STATES.include?(@state)
        #   raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_STATE)
        # end

        @per_page = PAGINATION_UTIL::PER_PAGE.to_f if @per_page <= 0
        @page_no = PAGINATION_UTIL::PAGE_NO.to_f if @page_no <= 0

        sort_by = PAGINATION_UTIL::SORT_BY unless model.attribute_names.include?(sort_by)
        order = PAGINATION_UTIL::ORDER unless PAGINATION_UTIL::ALLOWED_ORDERS.include?(order)

        @sort_order = model.table_name + '.' + sort_by + ' ' + order
      end

      # class function of above
      #
      # @return [Hash] filterd params
      #
      def self.model_params(args, model)
        args = ActionController::Parameters.new(args)
        args.permit(model.attribute_names)
      end

    end
  end
end
