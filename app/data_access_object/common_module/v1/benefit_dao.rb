require 'imli_singleton'
module CommonModule
  module V1
    #
    # Dao class to handle all the profile related logics
    #
    class BenefitDao < BaseModule::V1::BaseDao
      include ImliSingleton

      def initialize(params='')
        @params = params
      end

      #
      # Function to get area corresponding to the pincode
      #
      def self.get_benefits_by_type(benefits, type)

        return [] if benefits.blank?

        benefits.where(benefit_type: type)
      end

      def self.update_benefit(benefit, args)
        args = model_params(args, CommonModule::V1::Benefit)

        begin
          benefit.update_attributes!(args)
          return benefit
        rescue ActiveRecord::RecordInvalid => e
          raise CommonModule::V1::CustomErrors::InvalidArgumentsError.new('Not able to update benefit')
        end
      end
    end
  end
end
