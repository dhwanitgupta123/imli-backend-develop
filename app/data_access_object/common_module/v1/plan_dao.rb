module CommonModule
  module V1
    #
    # Plan Dao class to implement CRUD for PLAN model
    #
    class PlanDao < BaseModule::V1::BaseDao

      #
      # initialize PlanService Class
      #
      def initialize(params = {})
        @params = params
      end

      def get_all_active_plans
        plan_model = CommonModule::V1::Plan
        return plan_model.get_all_active_plans
      end

      #
      # Function to check if atleast one plan is active apart from Ample Sample
      #
      def atleast_one_active_plan_exclude_ample_sample?
        plan_model = CommonModule::V1::Plan
        active_status = CommonModule::V1::ModelStates::V1::PlanStates::ACTIVE
        plans = plan_model.where.not(plan_key: CommonModule::V1::PlanKeys::TRIAL).where(status: active_status)
        if plans.count <= 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::ATLEAST_ONE_PLAN_ACTIVE)
        else
          true
        end
      end
    end
  end
end