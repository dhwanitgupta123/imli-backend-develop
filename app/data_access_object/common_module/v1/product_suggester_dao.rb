module CommonModule
  module V1
    class ProductSuggesterDao < BaseModule::V1::BaseDao

      #
      # initialize required classes
      #
      def initialize(params = {})
        @params = params
      end

      #
      # add_product_suggestion function
      # @param args [Hash]
      # contains-
      # 			* user_id [String]: user id of the user suggesting the product
      # 			* product [String]: product name of the suggested product
      #
      # Summary-
      # * Ccreates a new record in ProductSuggester Model
      #
		  def add_product_suggestion(args)

        new_suggestion = create_new_suggestion(args)
        begin
          new_suggestion.save!
          rescue => e
          	raise CUSTOM_ERROR_UTIL::InvalidDataError.new(CONTENT::PROBLEM_SAVING_SUGGESTION)
        end
		  end

		  #
		  # user_suggestion_exists? function
		  #
		  # @return [Boolean] If suggestions exist for user or not
		  def self.user_suggestion_exists?(user_id)
        product_suggester_model = CommonModule::V2::ProductSuggester
        return false if user_id.blank?
        user_suggestions_status = product_suggester_model.where(user_id: user_id).entries.blank? ? true : false

        return user_suggestions_status
		  end

	 	  #
      # function to return array of all the suggestions paginated
      #
      # @return [array of product suggester object]
      def paginated_product_suggestions(paginate_param)
        product_suggester_model = CommonModule::V2::ProductSuggester

        if paginate_param[:order].present? && paginate_param[:order] != 'ASC' && paginate_param[:order] != 'DESC'
          raise CUSTOM_ERROR_UTIL::InvalidDataError.new(CONTENT::WRONG_ORDER)
        end
        if paginate_param[:sort_by].present? && !product_suggester_model.attribute_names.include?(paginate_param[:sort_by])
          raise CUSTOM_ERROR_UTIL::InvalidDataError.new(CONTENT::INVALID_PARAMETER%{ field: paginate_params[:sort_by] })
        end

        if paginate_param[:user_id].present?
          return get_suggestions_by_user_id(paginate_param)
        else
          return get_all_suggestions(paginate_param)
        end
		  end

		  #
      # function to return paginated suggestions by user_id
      #
      def get_suggestions_by_user_id(paginate_param)
        product_suggester_model = CommonModule::V2::ProductSuggester
        set_pagination_properties(paginate_param)
        suggestions = product_suggester_model.where({ user_id: paginate_param[:user_id] }).order_by(@sort_order)
        return get_paginated_suggestions(suggestions)
      end

      #
      # function to return paginated all suggestions
      #
      def get_all_suggestions(paginate_param)
        product_suggester_model = CommonModule::V2::ProductSuggester
        set_pagination_properties(paginate_param)
        suggestions = product_suggester_model.all.order_by(@sort_order)
        return get_paginated_suggestions(suggestions)
      end

      #
		  # creates new ProductSuggester object in Mongoid document
		  # @param args [Hash]
		  # * contains user_id [Integer] user_id of the user who suggested
		  # * contains product [String] product suggestion by user
		  #
		  # @return [ProductSuggester Model] new ProductSuggester Model
		  def create_new_suggestion(args)
        product_suggester_model = CommonModule::V2::ProductSuggester
        new_suggestion = product_suggester_model.new
        new_suggestion.product_suggestion = args[:product_name]
        new_suggestion.user_id = args[:user_id]

        return new_suggestion
		  end

      #
      # Funciton to return list of products suggested by User in last 24 hrs
      #
      def self.get_all_last_x_hours_suggestions(x = '')
        product_suggester_model = CommonModule::V2::ProductSuggester
        x =  CommonModule::V1::Cache.read('DEFAULT_TIME_RANGE').to_i if x.blank?
        product_suggestions_in_interval = product_suggester_model.where(created_at: (x.hours.ago..Time.zone.now))
        return product_suggestions_in_interval
      end

		  ###############################
      #       Private Functions     #
      ###############################

		  private

		  #
      # function to set pagination properties
      #
      def set_pagination_properties(paginate_param)
        pagination_util = CommonModule::V1::Pagination
        @per_page = (paginate_param[:per_page] || pagination_util::PER_PAGE).to_f
        @page_no = (paginate_param[:page_no] || pagination_util::PAGE_NO).to_f
        sort_by = paginate_param[:sort_by] || pagination_util::SORT_BY
        order = paginate_param[:order] || pagination_util::ORDER
        @sort_order = sort_by + ' :' + order
      end

      #
      # function to paginate the suggestions
      #
      def get_paginated_suggestions(suggestions)
        page_count = (suggestions.count / @per_page).ceil
        paginated_suggestion = suggestions.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { suggestions: paginated_suggestion.entries, page_count: page_count }
      end

    end
  end
end
