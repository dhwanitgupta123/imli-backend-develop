module DescriptionsModule
  module V1
    class DescriptionDao < BaseModule::V1::BaseDao

      DESCRIPTION_MODEL = DescriptionsModule::V1::Description

      # 
      # initialize required classes
      # 
      def initialize(params = {})
        @params = params
      end

      # 
      # This function creates description
      #
      # @param args [Hash] containing model arguments
      # 
      # @return [Model] Description
      #
      def create(args)

        description = DESCRIPTION_MODEL.new(model_params(args, DESCRIPTION_MODEL))
        description.data = args[:data] if args[:data].present?

        begin
          description.save!
          return description
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
        end
      end


      # 
      # This function creates description
      #
      # @param args [Hash] containing model arguments
      # @param description [Model] model to update
      # 
      # @return [Model] Description
      #
      def update(args, description)

        description.data = args[:data] if args[:data].present?

        begin
          description.update_attributes!(model_params(args, DESCRIPTION_MODEL))
          return description
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
        end
      end

      # 
      # This function return model if exists else nil
      #
      # @param id [Integer] id
      # 
      # @return [Model] description
      #
      def get_by_id(id)
        begin
          DESCRIPTION_MODEL.find(id)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT::DESCRIPTION_NOT_FOUND)
        end
      end

      # 
      # Delete the description
      #
      def delete(description)
        description.destroy
      end

      # 
      # Delete all the childs
      #
      # @param descriptions [Array] childs to delete
      # 
      def delete_childs(descriptions)
        descriptions = DESCRIPTION_MODEL.where(id: descriptions)
        descriptions.each do |description|
          description.destroy
        end
      end
    end
  end
end
