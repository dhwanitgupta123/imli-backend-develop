module CreditsModule
  module V1
    class AmpleCreditDao < BaseModule::V1::BaseDao

      AMPLE_CREDIT_MODEL = CreditsModule::V1::AmpleCredit

      # 
      # initialize required classes
      # 
      def initialize(params = {})
        @params = params
      end

      # 
      # This function creates ample_credit
      #
      # @param args [Hash] containing model arguments
      # 
      # @return [Model] AmpleCredit
      #
      def create(args)

        ample_credit = AMPLE_CREDIT_MODEL.new(model_params(args, AMPLE_CREDIT_MODEL))

        begin
          ample_credit.save!
          return ample_credit
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
        end
      end


      # 
      # This function creates ample_credit
      #
      # @param args [Hash] containing model arguments
      # @param ample_credit [Model] model to update
      # 
      # @return [Model] AmpleCredit
      #
      def update(args, ample_credit)

        begin
          ample_credit.update_attributes!(model_params(args, AMPLE_CREDIT_MODEL))
          return ample_credit
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
        end
      end
    end
  end
end
