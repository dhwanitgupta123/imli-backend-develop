require "imli_singleton"
module CommunicationsModule
  module V1
    module VoiceModule
      module V1
        class VoiceCommunicationDao < CommunicationsModule::V1::CommunicationDao
          include ImliSingleton

          #
          # Initializing voice communication dao
          # @param params [JSON] [parameters required for versioning at various levels]
          #
          def initialize(params={})
            @params = params
            super
          end

          def active_calls_present_for_initiator?(initiator_phone_number)
            return if initiator_phone_number.blank?
            disconnected_states = CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationStates.get_disconnected_states_values
            active_calls_entries = CommunicationsModule::V1::VoiceModule::V1::VoiceCommunication.where(from_phone_number: initiator_phone_number).where.not(status: disconnected_states)
            if active_calls_entries.present?
              return true
            else
              return false
            end
          end

          def create_voice_communication(atrributes_hash)
            create_model(atrributes_hash, CommunicationsModule::V1::VoiceModule::V1::VoiceCommunication, 'Voice Communication')
          end

          def get_by_id(id)
            get_model_by_id(CommunicationsModule::V1::VoiceModule::V1::VoiceCommunication, id)
          end

          def get_by_field(field_name, field_value)
            get_model_by_field(CommunicationsModule::V1::VoiceModule::V1::VoiceCommunication, field_name, field_value)
          end

          def update(args, voice_communication)
            update_model(args, voice_communication, 'Voice Communication')
          end

          def missing_information?(voice_communication)
            voice_communication.missing_information?
          end

        end
      end
    end
  end
end
