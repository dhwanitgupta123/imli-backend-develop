require "imli_singleton"
module CommunicationsModule
  module V1
    class CommunicationDao < BaseModule::V1::BaseDao
      include ImliSingleton

      #
      # Initializing communication dao
      # @param params [JSON] [parameters required for versioning at various levels]
      #
      def initialize(params={})
        @params = params
        super
      end

    end
  end
end
