#
# Module to handle user related functionality
#
module UsersModule
  module V1
    #
    # Module to handle membership related functionality
    #
    module MembershipModule
      module V1
        #
        # Membership Payment Dao class handles the membership payment model functionality
        #
        class MembershipPaymentDao < BaseModule::V1::BaseDao

          CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
          CONTENT_UTIL = CommonModule::V1::Content
          PAYMENT_MODEL = UsersModule::V1::MembershipModule::V1::MembershipPayment

          # 
          # Function fetches payment based on it's ID
          #
          def get_payment_from_id(payment_id)
            begin
              payment_model = PAYMENT_MODEL.new
              payment = payment_model.get_payment_by_id(payment_id)
            rescue => e
              raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::RESOURCE_NOT_FOUND%{resource: 'Payment'})
            end
          end

          def get_pending_payment(payments)
            return nil if payments.blank?
            return payments.where(status: UsersModule::V1::ModelStates::MembershipPaymentStates::PENDING).first
          end

          # 
          # change state function for membership payment
          # @param payment [MembershipPayment Model] 
          # @param event [Integer] MembershipPaymentEvent
          # 
          # @return [MembershipPaymentModel] updated membershipPayment object
          # 
          def change_state(payment, event)
            payment_event = UsersModule::V1::ModelStates::MembershipPaymentEvents
            begin
              case event.to_i
              when payment_event::INITIATE
                payment.initiate!
              when payment_event::PAYMENT_FAIL
                payment.payment_fail!
              when payment_event::PAYMENT_SUCCESS
                payment.payment_success!
              else
                ApplicationLogger.info('Not a valid Event: ' + event.to_s + ' for Membership transition')
              end
            end
            return payment
          end

          def get_active_payable_membership_payment(membership_payments)
            payment_states = UsersModule::V1::ModelStates::MembershipPaymentStates
            payment_type =  PaymentModule::V1::PaymentType.get_id_by_key('PAY')
            return membership_payments.where(status: payment_states::SUCCESS, payment_type: payment_type).first
          end
        end
      end
    end
  end
end
