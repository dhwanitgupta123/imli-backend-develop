require 'imli_singleton'
module UsersModule
  module V1
    #
    # Memberhsip service to handle the membership related logic
    #
    class UserDao < BaseModule::V1::BaseDao
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @user_model = UsersModule::V1::User
        @user_model_states = UsersModule::V1::ModelStates::UserStates
        super
      end

      def create(args)
        create_model(args, @user_model, 'User')
      end

      def update(args, user)
        update_model(args, user, 'User')
      end

      def get_by_id(id)
        get_model_by_id(@user_model, id)
      end

      def get_all(pagination_params = {})
        get_all_element(@user_model, pagination_params)
      end

      def get_non_deleted_users(pagination_params = {})
        users = @user_model.where.not(workflow_state: @user_model_states::DELETED)
        get_paginated_element(@user_model, users, pagination_params)
      end

      def get_user_orders(user)
        user.orders
      end

      def get_full_name(user)
        (user.first_name || '') + ' ' + (user.last_name || '')
      end

      def get_all_email_ids(user)
        emails = []
        user.emails.each do |email|
          emails.push(email.email_id)
        end
        emails
      end

      def get_aggregated_users_and_orders_for_user_search_model(user_search_models)
        ids = []
        user_search_models.each do |u|
          ids.push(u.attributes["id"])
        end
        # Join orders table with users having given IDs
        UsersModule::V1::User.where(id: ids).includes(:orders)
      end

      def self.get_referrees_of_user(user)
        return [] if user.blank?
        UsersModule::V1::User.where(referred_by: user.id)
      end

    end
  end
end
