require 'imli_singleton'
module UsersModule
  module V1
    #
    # Role Data Access Object class to handle the Role related logic
    #
    class RoleDao < BaseModule::V1::BaseDao
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @role_model = UsersModule::V1::Role
        @role_model_states = UsersModule::V1::ModelStates::RoleStates
        super
      end

      def create(args)
        create_model(args, @role_model, 'Role')
      end

      def update(args, role)
        update_model(args, role, 'Role')
      end

      def get_by_id(id)
        get_model_by_id(@role_model, id)
      end

      def get_all(pagination_params = {})
        get_all_element(@role_model, pagination_params)
      end

      def get_active_roles_of_user(user)
        roles = user.roles.where(status: @role_model_states::ACTIVE)
      end

    end
  end
end
