module CommunicationsModule
  module V1
    # Controller to handle all the API calls related to Communications
    class CommunicationsApiController < BaseModule::V1::ApplicationController

      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper

      ##################################################################################
      #                     voice_communication_callback API                           #
      ##################################################################################
      #
      # POST API for callback hit after termination of voice communication
      #
      # Response::
      #    * 200 ok: if call is successfully placed
      #
      def voice_communication_callback
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        exotel_callback_api = CommunicationsModule::V1::ThirdPartyModule::ExotelCallbackApi.instance(deciding_params)
        response = exotel_callback_api.enact(voice_communication_callback_params)
        send_response(response)
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def voice_communication_callback_params
        params.permit(:CallSid, :Status)
      end
    end
  end
end
