module CommunicationsModule
  module V1
    # Controller to handle all the API calls related to Communications
    class CommunicationsController < BaseModule::V1::ApplicationController

      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper

      ##################################################################################
      #                             place_voice_call API                               #
      ##################################################################################
      #
      # POST API to place a voice call
      #
      # Request::
      #    * from: from phone number to place call from
      #    * to: to phone number to place call to
      #
      # Response::
      #    * 200 ok: if call is successfully placed
      #    * 400 : if invalid or insufficient data
      #
      def place_voice_call
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        place_call_api = CommunicationsModule::V1::VoiceModule::V1::PlaceCallApi.instance(deciding_params)
        response = place_call_api.enact(place_voice_call_params)
        send_response(response)
      end

      swagger_controller :communications, 'Communications APIs'
      swagger_api :place_voice_call do
        summary 'It places a new voice call'
        notes 'It takes from and to number of 10-digit and calls the number'
        param :body, :place_call_request, :place_call_request,
              :required, 'place call request'
        response :bad_request, 'Parameters missing/wrong parameters'
      end

      swagger_model :place_call_request do
        description 'Request for placing a call'
        property :communication, :communication_hash_for_new_voice_role, :required, 'communication hash with new call request details'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :communication_hash_for_new_voice_role do
        description 'Communication hash with new call request details'
        property :from, :string, :required, 'from number to be called from'
        property :to, :string, :required, 'to number to be called'
      end

      ##################################################################################
      #                         get_communication_details API                          #
      ##################################################################################
      #
      # GET API to fetch details of communication
      #
      # Request::
      #    * id: id of communication
      #
      # Response::
      #    * 200 ok: if call is successfully placed
      #    * 400 : if invalid or insufficient data
      #
      def get_communication_details
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_communication_details_api = CommunicationsModule::V1::GetCommunicationDetailsApi.instance(deciding_params)
        response = get_communication_details_api.enact(get_communication_details_request_params)
        send_response(response)
      end

      swagger_controller :communications_api, 'Communications APIs'
      swagger_api :get_communication_details do
        summary 'It fetches communication details'
        notes 'It takes communication sid and communication type'
        param :path, :sid, :integer, :required, 'communication id for which details are to be fetched'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :communication_type, :integer, :required, 'communication type for which communication id needs to fetched'
        response :bad_request, 'Parameters missing/wrong parameters'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      def get_communication_details_request_params
        params.permit(:sid, :communication_type)
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def place_voice_call_params
       params.require(:communication).permit(:from, :to)
      end
    end
  end
end
