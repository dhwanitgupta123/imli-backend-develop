module PaymentModule
  module V1
    class PaytmPaymentsController < BaseModule::V1::ApplicationController 

      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      #
      # API to return the checksum for the transaction through the Paytm
      #
      _LogActivity_
      def get_checksum
        deciding_params = @application_helper.get_deciding_params(params)
        paytm_payment_service_class = PaymentModule::V1::PaytmPaymentService.new(deciding_params)
        response = paytm_payment_service_class.get_checksum(get_checksum_params)
        render json: response
      end

      #
      # API to validate the checksum if it is same or not
      #
      _LogActivity_
      def transaction_callback
        deciding_params = @application_helper.get_deciding_params(params)
        paytm_payment_service_class = PaymentModule::V1::PaytmPaymentService.new(deciding_params)
        response = paytm_payment_service_class.validate_checksum(validate_checksum_params)
        platform = response[:platform].present? ? response[:platform].upcase : ''
        if platform == "WEB"
          order_id = response[:order_id]
          status = response[:status]
          url = APP_CONFIG['config']['PAYTM_WEB_CALLBACK_URL'] + "?tid=#{order_id}&st=#{status}"
          redirect_to url
        else
          @encoded_json = response[:paytm_response]
          render 'payment_module/v1/paytm_payments/web_view'
        end
      end

      private

      def get_checksum_params
        params.permit(:CHANNEL_ID, :INDUSTRY_TYPE_ID, :MID, :ORDER_ID, :TXN_AMOUNT, :WEBSITE, :CUST_ID, :EMAIL, :MOBILE_NO, :THEME, :CALLBACK_URL, :REQUEST_TYPE)
      end

      def validate_checksum_params
        params.permit(:SUBS_ID, :MID, :TXNID, :ORDERID, :BANKTXNID, :TXNAMOUNT, :CURRENCY, :STATUS, :RESPCODE, :RESPMSG,
          :TXNDATE, :GATEWAYNAME, :BANKNAME, :PAYMENTMODE, :CHECKSUMHASH, :platform)
      end
    end
  end
end
