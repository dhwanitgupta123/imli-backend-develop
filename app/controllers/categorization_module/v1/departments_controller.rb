module CategorizationModule
  module V1
    #
    # Departments controller service class to route panel API calls to the Department service
    #
    class DepartmentsController < BaseModule::V1::ApplicationController
      #
      # initialize DepartmentService Class
      #
      def initialize
        @department_service = CategorizationModule::V1::DepartmentService
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      ##################################################################################
      #                             create_department API                              #
      ##################################################################################

      #
      # function to create new department
      #
      # POST /departments/new
      #
      # Request::
      #   * department_params [hash] it contains label & image url of the department
      #
      # Response::
      #   * sends ok response to the panel
      #
      def create_department
        deciding_params = @application_helper.get_deciding_params(params)
        department_service = @department_service.new(deciding_params)
        response = department_service.create_department(create_department_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :departments, 'CategorizationModule APIs'
      swagger_api :create_department do
        summary 'It creates a new department'
        notes 'create_department API creates a new department'
        param :body, :department_request, :department_request, :required, 'create_department request'
        response :bad_request, 'if label of department is missing'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :department_request do
        description 'request schema for /create_department API'
        property :department, :department_hash, :required, 'department model hash',
                 { 'items' => { '$ref' => 'department_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :department_hash do
        description 'details of department to be created'
        property :label, :string, :required, 'name of department'
        property :image_url, :string, :optional, 'image url for the department image'
        property :description, :string, :optional, 'description of department'
        property :priority, :integer, :required, 'priority of department'
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }
      end

      ##################################################################################
      #                             update_department API                              #
      ##################################################################################

      #
      # function to update a department
      #
      # PUT /departments/update/:id
      #
      # Request::
      #   * department_params [hash] it contains updated label & image url of the department
      #
      # Response::
      #   * sends ok response to the panel
      #
      def update_department
        deciding_params = @application_helper.get_deciding_params(params)
        department_service = @department_service.new(deciding_params)
        response = department_service.update_department(update_department_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :departments, 'CategorizationModule APIs'
      swagger_api :update_department do
        summary 'It updates a department'
        notes 'update_department API updates a department'
        param :path, :id, :integer, :optional, 'department_id of which details are requested'
        param :body, :update_department_request, :update_department_request, :required, 'update_department request'
        response :bad_request, 'if id of department is missing'
        response :internal_server_error, 'run time error happened'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :update_department_request do
        description 'request schema for /update_department API'
        property :department, :update_department_hash, :required, 'department model hash',
                 { 'items' => { '$ref' => 'update_department_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :update_department_hash do
        description 'details of department to be updated'
        property :label, :string, :required, 'name of department'
        property :image_url, :string, :optional, 'image url for the department image'
        property :description, :string, :optional, 'description of department'
        property :priority, :integer, :required, 'priority of department'
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }
      end

      swagger_model :image_hash_array do
        description 'details of image id and corresponding priority'
        property :image_id, :integer, :required, 'reference to image'
        property :priority, :integer, :required, 'priority of image'
      end

      ##################################################################################
      #                                    change_state API                            #
      ##################################################################################

      #
      # function to change the status of department
      #
      # PUT /departments/state/:id
      #
      # Request::
      #   * department_id [integer] id of department of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = @application_helper.get_deciding_params(params)
        department_service = @department_service.new(deciding_params)
        response = department_service.change_state(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :departments, 'CategorizationModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of department'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the department & chnges the status if everyhting is correct'
        param :path, :id, :integer, :required, 'department_id to be deactivated'
        param :body, :change_state_request, :change_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or department not found'
        response :internal_server_error, 'run time error happened'
        response :precondition_required, 'event cant be triggered based on the current status of department'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_state_request do
        description 'request schema for /change_state API'
        property :department, :change_state_hash, :required, 'hash of event to trigger state change',
                 { 'items' => { '$ref' => 'change_state_hash' } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      ##################################################################################
      #                                get_department API                              #
      ##################################################################################

      #
      # function to get data of a department
      #
      # GET /departments/:id
      #
      # Request::
      #   * department_id [integer] id of the department whose detail is requested
      #
      # Response::
      #   * sends response with all the categories which are the part of requested department
      #
      def get_department
        deciding_params = @application_helper.get_deciding_params(params)
        department_service = @department_service.new(deciding_params)
        response = department_service.get_department(params[:id])
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :departments, 'CategorizationModule APIs'
      swagger_api :get_department do
        summary 'It returns details of a department'
        notes 'get_department API returns the details of a department which includes its categories'
        param :path, :id, :integer, :required, 'department_id of which details are requested'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        response :bad_request, 'wrong parameters or department not found'
        response :internal_server_error, 'run time error happened'
      end

      ##################################################################################
      #                            get_all_departments API                             #
      ##################################################################################

      #
      # function to get label of all the departments
      #
      # GET /departments
      #
      # Response::
      #   * sends response with all the department labels
      #
      def get_all_departments
        deciding_params = @application_helper.get_deciding_params(params)
        department_service = @department_service.new(deciding_params)
        response = department_service.get_all_departments(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :departments, 'UsersModule APIs'
      swagger_api :get_all_departments do
        summary 'It returns details of all the departments'
        notes 'get_all_departments API returns the details of all the departments'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all departments'
        param :query, :per_page, :integer, :optional, 'how many departments to display in a page'
        param :query, :state, :integer, :optional, 'to fetch departments based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
      end

      #
      # function to get label of all the departments, categories and sub_categories
      #
      # GET /departments
      #
      # Response::
      #   * sends response with all the department labels
      #
      def get_all_departments_categories_and_sub_categories
        deciding_params = @application_helper.get_deciding_params(params)
        department_service = @department_service.new(deciding_params)
        response = department_service.get_all_departments_categories_and_sub_categories
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :departments, 'UsersModule APIs'
      swagger_api :get_all_departments_categories_and_sub_categories do
        summary 'It returns details of all the departments'
        notes 'get_all_departments API returns the details of all the departments'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        response :bad_request, 'wrong pagination parameters'
      end

      private

      #
      # White list params for Create dept API
      #
      def create_department_params
        params.require(:department).permit(:label, :image_url, :description, :priority, images: [:image_id, :priority])
      end

      #
      # White list params for update dept API
      #
      def update_department_params
        params.require(:department).permit(:id, :label, :image_url, :description, :priority, images: [:image_id, :priority]).merge({ id: params[:id] })
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:department).permit(:event).merge({ id: params[:id] })
      end

      #
      # White list params for get_all_departments API
      #
      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end
