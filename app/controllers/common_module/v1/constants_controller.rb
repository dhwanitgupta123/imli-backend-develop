module CommonModule
  module V1
    # Controller to handle all the API calls related to contant maps
    class ConstantsController < BaseModule::V1::ApplicationController
      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      CONSTANTS_SERVICE = CommonModule::V1::ConstantsService

      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      #
      # API to get all the constants for cluster
      #
      # Response::
      #    * 200 ok: return all constants
      #
      def get_cluster_constants
        constants_service = CONSTANTS_SERVICE.new(@deciding_params)
        response = constants_service.get_cluster_constants
        send_response(response)
      end

      swagger_controller :constants, 'CommonModule APIs'
      swagger_api :get_cluster_constants do
        summary 'It return a cluster constants'
      end
    end
  end
end
