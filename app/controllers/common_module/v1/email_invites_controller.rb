module CommonModule
  module V1
    #
    # Controller to handle all the API email invites
    #
    class EmailInvitesController < BaseModule::V1::ApplicationController
      # Storing versioned classes and helpers in class level constants
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      EMAIL_INVITES_SERVICE = CommonModule::V1::EmailInvitesService

      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end

      ##################################################################################
      #                                email_invites API                               #
      ##################################################################################
      #
      # API to take email for invites
      # POST email/invites
      #
      # Request::
      #    * email: hash containing email_id(String)
      #
      # Response::
      #    * 200 : if successfully created
      #    * 400 : if invalid or insufficient
      #
      def email_invites
        email_invites_service = EMAIL_INVITES_SERVICE.new(@deciding_params)
        response = email_invites_service.email_invites(email_invites_param)
        send_response(response)
      end

      swagger_controller :email_invites, 'CommonModule APIs'
      swagger_api :email_invites do
        summary 'It stores email ids for invites'
        notes 'It takes email id for invites & saves them in Mongodb'
        param :body, :email_invites_request, :email_invites_request,
              :required, 'email_invites_request'
        response :bad_request, 'Parameters missing/wrong parameters'
      end

      swagger_model :email_invites_request do
        description 'Request for email_invites_request'
        property :email_id, :string, :required, 'email_id'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # White list params for email_invites params
      #
      def email_invites_param
        params.permit(:email_id)
      end
    end
  end
end
