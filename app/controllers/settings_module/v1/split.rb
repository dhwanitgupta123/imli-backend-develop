module SettingsModule
  module V1

    #
    # SPLIT uses 'split gem' which require many parameters which are available only on controller level
    # like params, payload, cookies, sessions, etc. etc.
    # 
    # SPLIT is a wrapper over split gem and has certain functions
    # to start and finish experiments
    # Wiki for split : https://github.com/splitrb/split
    #
    class Split < ActionController::Base

      #
      # A custom method to pass controller's request to the 'split gem helper files'
      # Request object is needed to fetch certain relevant entries
      # Ex: request.parameters, request.user_agent, request.ip, etc.
      # 
      # Note: ActionDispatch::Request object must be set to the SHIELD_PARAMS (global
      # variable) in the request controller.
      # Ex: SettingsModule::V1::SettingsController:get_dynamic_settings
      #
      def self.shield_custom_request
        return SHIELD_PARAMS[:request]
      end

      # 
      # A custom method to override shield experiment value. Currently returning
      # nil, which means nothing is overrided.
      # TO-DO: 
      # Access cookies from the ActionDispatch::Request , stored in SHIELD_PARAMS
      # and check for the value for corresponding treatment name.
      # Ex: request.cookies['experiment_name'] --> '1' or '0'
      #
      # @param experiment_name [String] [Experiment name for which override value is to be checked]
      #
      def self.shield_experiment_override(experiment_name)
        # Need to fetch override value from cookie/session
        # and return it.
        return nil
      end

      #
      # A custom method to return unique identifier required for persisting
      # experiment result of corresponding requester in REDIS.
      # Now, the logic is to return the 'user_id' of the requester (ACL controller sets this ID)
      # If user_id not present, then return a default value to the user
      #
      # @return [String] [user id of requester OR 'default' string]
      #
      def shield_unique_identifier
        uid = USER_SESSION[:user_id]
        return uid if uid.present?
        return 'default'
      end

      #
      # Start Split experiment by calling 'split gem' ab_test function
      #
      # @param experiment_name [String] [Name of the experiment]
      #
      # @return [String] [Value corresponding to the experiment if valid experiment name else nil]
      #
      def start_split_experiment(experiment_name)
        begin
          ab_test(experiment_name)
        rescue ::Split::ExperimentNotFound => e
          return nil
        end
      end

      #
      # Finish Split experiment by calling 'split gem' finished function
      #
      # @param experiment_name [String] [Name of the experiment]
      # 
      # @return [Object] [Split Experiment Object in case of valid experiment name else nil]
      #
      def finish_split_experiment(experiment_name)
        finished(experiment_name)
      end


    end
  end
end