#
# Module to handle all the functionalities related to LoggingModule
#
module LoggingModule
  #
  # Version1 for LoggingModule module
  #
  module V1
    #
    # EventLoggerController controller service class
    #
    class EventLoggerController < BaseModule::V1::ApplicationController

      #
      # before actions to be executed
      #
      before_action do
        @deciding_params = CommonModule::V1::ApplicationHelper.get_deciding_params(params)
      end

      def get_events_by_filters
        get_events_by_filters_api = LoggingModule::V1::GetEventsByFiltersApi.instance(@deciding_params)
        response = get_events_by_filters_api.enact(event_filter_params)
        send_response(response)
      end


      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :event_logger, 'LoggingModule APIs'
      swagger_api :get_events_by_filters do
        summary 'It returns details of all the events corresponding to the model'
        notes 'get_events_by_filters API returns the details of all the events'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :id, :integer, :required, 'id of the model'
        param :query, :table, :string, :required, 'table name'
        param :query, :last_n, :integer, :optional, 'number of events'
        param :query, :user, :integer, :optional, 'user id'
        param :query, :database, :string, :optional, 'database name'
        param :query, :from_time, :integer, :optional, 'from time'
        param :query, :to_time, :integer, :optional, 'to time'
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private

      def event_filter_params
        params.permit(:id, :table, :last_n, :user, :database, :from_time, :to_time)
      end
    end
  end
end
