module UsersModule
  module V1
    class EmailsApiController < BaseModule::V1::ApplicationController

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content

      # 
      # initialize UserService Class User Model Class
      #
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
        @user_service = UsersModule::V1::UserService
        @cache_util = CommonModule::V1::Cache
        @status_codes_util = CommonModule::V1::StatusCodes
      end

      ##################################################################################
      #                                 verify_email API                               #
      ##################################################################################
      #
      # API to verify email id of the user
      #
      # Request::
      #    * token: authentication token associated with email id
      # Response::
      #    * 200 ok: if token verified successfully
      #    * 419 : if authentication token expired
      #    * 200  : if email already verified
      #
      _LogActivity_
      def verify_email
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.verify_email({
          token: params[:token]
          })
        if response.present?
          if response[:response] == RESPONSE_CODES_UTIL::SUCCESS
            @is_success = true
            @message = CONTENT_UTIL::EMAIL_VERIFIED_SUCCESS
          else
            @is_success = false
            @message = response[:error][:message] if response[:error].present?
          end
        end

        # Redirection page for BuyAmple Homepage
        @redirection_page = @cache_util.read('BUYAMPLE_HOMEPAGE')

        render template: 'users_module/v1/emails_api/verify_email'

      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :verify_email do
        summary "It verifies the new Email added by the user by verifying the token"
        notes "verify_email API searches the Authentication token, if found and is not expired,
        it sets email verified otherwise raises error as response"
        param :query, :token, :string, :required, "verify_email request"
        response :bad_request, "wrong authentication token"
        response :authentication_timeout, "your authentication token has timed out"
      end

      ##################################################################################
      #                           resend_email_token API                               #
      ##################################################################################
      # POST /resend_email_token
      #
      # API to resend email token
      #
      # Request::
      #    * session_token: required for authentication
      #    * email id to verify
      # Response::
      #    * 200 ok: if email token generated and sent successfully
      #
      _LogActivity_
      def resend_email_token
        deciding_params = @application_helper.get_deciding_params(params)
        user_service = @user_service.new(deciding_params)
        response = user_service.resend_email_token(user_email_params)
        send_response(response)
      end

      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :users, "UsersModule APIs"
      swagger_api :resend_email_token do
        summary "It resends the email authentication token"
        notes "resend_email_token API generates the new Authentication token if expired"
        param :body, :resend_email_token_request, :resend_email_token_request, :required, "resend_email_token request"
        response :unauthorized, "user not found"
        response :bad_request, "wrong authentication token"
        response :internal_server_error, "run time error happened"
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model 
      #       to create complex swagger_model
      swagger_model :resend_email_token_request do
        description "User required params for the resend_email_token request"
        property :user, :user_resend_email_token_hash, :required, "user hash with email_id", { "items" => { "$ref" => "user_resend_email_token_hash" } }
        property :session_token, :string, :required, "session_token to authorize user access"
      end
      swagger_model :user_resend_email_token_hash do
        description "User hash for the resend_email_token request"
        property :email_id, :string, :required, "User Email ID"
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      # filter email_id from user hash
      def user_email_params
        params.require(:user).permit(:email_id)
      end
    end
  end
end
