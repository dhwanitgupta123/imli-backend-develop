module UsersModule
  module V1
    class UsersDataController < BaseModule::V1::ApplicationController

      #
      # initialize UserService Class User Model Class
      #
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
        @user_service = UsersModule::V1::UserService
      end


      ##################################################################################
      #                      get_user_basic_details API                                #
      ##################################################################################
      #
      # API to get user details based on passed id
      #
      # Request::
      #    * query: search query for which we need to find users data
      #
      # Response::
      #    * 200 ok: with users array. Empty if no users
      #
      def get_user_basic_details
        deciding_params = @application_helper.get_deciding_params(params)
        api = UsersModule::V1::GetUserBasicDetailsApi.instance(deciding_params)
        response = api.enact(get_user_basic_details_request_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      #
      swagger_controller :users_data, 'UsersModule APIs'
      swagger_api :get_user_basic_details do
        summary 'It fetches user details'
        notes 'It takes user session token and id for which user is to be fetched. It validates and fetches user based
        for given id and return user details.
        User should have specific permissions to access this API'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :path, :id, :integer, :optional, 'id of the user whose details is to be fetched'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                      get_user_personal_info API                                #
      ##################################################################################
      #
      # API to get user personal info based on passed id
      #
      # Request::
      #    * query: search query for which we need to find users data
      #
      # Response::
      #    * 200 ok: with users array. Empty if no users
      #
      def get_user_personal_info
        deciding_params = @application_helper.get_deciding_params(params)
        api = UsersModule::V1::GetUserPersonalInfoApi.instance(deciding_params)
        response = api.enact(get_user_personal_info_request_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      #
      swagger_controller :users_data, 'UsersModule APIs'
      swagger_api :get_user_personal_info do
        summary 'It fetches user details'
        notes 'It takes user session token and id for which user is to be fetched. It validates and fetches user based
        for given id and return user details.
        User should have specific permissions to access this API'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :path, :id, :integer, :optional, 'id of the user whose details is to be fetched'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                      get_user_address_details API                              #
      ##################################################################################
      #
      # API to get user personal info based on passed id
      #
      # Request::
      #    * query: search query for which we need to find users data
      #
      # Response::
      #    * 200 ok: with users array. Empty if no users
      #
      def get_user_orders
        deciding_params = @application_helper.get_deciding_params(params)
        api = UsersModule::V1::GetUserOrdersApi.instance(deciding_params)
        response = api.enact(get_user_orders_request_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      #
      swagger_controller :users_data, 'UsersModule APIs'
      swagger_api :get_user_orders do
        summary 'It fetches user details'
        notes 'It takes user session token and id for which user is to be fetched. It validates and fetches user based
        for given id and return user details.
        User should have specific permissions to access this API'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :path, :id, :integer, :optional, 'id of the user whose details is to be fetched'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      ##################################################################################
      #                      get_user_referral_details API                             #
      ##################################################################################
      #
      # API to get user personal info based on passed id
      #
      # Request::
      #    * query: search query for which we need to find users data
      #
      # Response::
      #    * 200 ok: with users array. Empty if no users
      #
      def get_user_referral_details
        deciding_params = @application_helper.get_deciding_params(params)
        api = UsersModule::V1::GetUserReferralDetailsApi.instance(deciding_params)
        response = api.enact(get_user_referral_details_request_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      #
      swagger_controller :users_data, 'UsersModule APIs'
      swagger_api :get_user_referral_details do
        summary 'It fetches user details'
        notes 'It takes user session token and id for which user is to be fetched. It validates and fetches user based
        for given id and return user details.
        User should have specific permissions to access this API'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :path, :id, :integer, :optional, 'id of the user whose details is to be fetched'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                      get_user_memberships_details API                          #
      ##################################################################################
      #
      # API to get user personal info based on passed id
      #
      # Request::
      #    * query: search query for which we need to find users data
      #
      # Response::
      #    * 200 ok: with users array. Empty if no users
      #
      def get_user_memberships_details
        deciding_params = @application_helper.get_deciding_params(params)
        api = UsersModule::V1::GetUserMembershipsDetailsApi.instance(deciding_params)
        response = api.enact(get_user_memberships_details_request_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      #
      swagger_controller :users_data, 'UsersModule APIs'
      swagger_api :get_user_memberships_details do
        summary 'It fetches user details'
        notes 'It takes user session token and id for which user is to be fetched. It validates and fetches user based
        for given id and return user details.
        User should have specific permissions to access this API'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :path, :id, :integer, :optional, 'id of the user whose details is to be fetched'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                      get_user_address_details API                              #
      ##################################################################################
      #
      # API to get user personal info based on passed id
      #
      # Request::
      #    * query: search query for which we need to find users data
      #
      # Response::
      #    * 200 ok: with users array. Empty if no users
      #
      def get_user_address_details
        deciding_params = @application_helper.get_deciding_params(params)
        api = UsersModule::V1::GetUserAddressDetailsApi.instance(deciding_params)
        response = api.enact(get_user_address_details_request_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      #
      swagger_controller :users_data, 'UsersModule APIs'
      swagger_api :get_user_address_details do
        summary 'It fetches user details'
        notes 'It takes user session token and id for which user is to be fetched. It validates and fetches user based
        for given id and return user details.
        User should have specific permissions to access this API'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :path, :id, :integer, :optional, 'id of the user whose details is to be fetched'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                      get_user_wallet_details API                               #
      ##################################################################################
      #
      # API to get user personal info based on passed id
      #
      # Request::
      #    * query: search query for which we need to find users data
      #
      # Response::
      #    * 200 ok: with users array. Empty if no users
      #
      def get_user_wallet_details
        deciding_params = @application_helper.get_deciding_params(params)
        api = UsersModule::V1::GetUserWalletDetailsApi.instance(deciding_params)
        response = api.enact(get_user_wallet_details_request_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      #
      swagger_controller :users_data, 'UsersModule APIs'
      swagger_api :get_user_wallet_details do
        summary 'It fetches user details'
        notes 'It takes user session token and id for which user is to be fetched. It validates and fetches user based
        for given id and return user details.
        User should have specific permissions to access this API'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :path, :id, :integer, :optional, 'id of the user whose details is to be fetched'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_brand_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      ###############################
      #       Private Functions     #
      ###############################

      private

      def get_user_basic_details_request_params
        params.permit(:id)
      end

      def get_user_personal_info_request_params
        params.permit(:id)
      end

      def get_user_orders_request_params
        params.permit(:id)
      end

      def get_user_referral_details_request_params
        params.permit(:id)
      end

      def get_user_memberships_details_request_params
        params.permit(:id)
      end

      def get_user_address_details_request_params
        params.permit(:id)
      end

      def get_user_wallet_details_request_params
        params.permit(:id)
      end

    end
  end
end