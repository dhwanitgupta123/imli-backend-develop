module CreditsModule
  module V1
    class AmpleCreditsController < BaseModule::V1::ApplicationController

      #
      # initialize UserService Class User Model Class
      #
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
      end


      ##################################################################################
      #                   credit_into_ample_credits API                                #
      ##################################################################################
      #
      # API to credit passed amount into user's ample credits based on passed id
      #
      # Request::
      #    * id: user id of the user
      #    * amount: amount to be credited
      #
      # Response::
      #    * 200 ok: with user ample credits details.
      #    * 400 bad_request: if incorrect user-id passed or must parameters missing
      #
      def credit_into_ample_credits
        deciding_params = @application_helper.get_deciding_params(params)
        api = CreditsModule::V1::CreditIntoAmpleCreditsApi.instance(deciding_params)
        response = api.enact(credit_into_ample_credits_request_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      #
      swagger_controller :ample_credits, 'CreditsModule APIs'
      swagger_api :credit_into_ample_credits do
        summary 'It credit amount into user ample credits'
        notes 'It takes user session token and id of user in whose account transaction is to be done.
        Also the amount which is to be credited.
        User should have specific permissions to access this API'
        param :body, :credit_request, :credit_request, :required, "credit request"
        response :bad_request, 'if request params are incorrect or incomplete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :credit_request do
        description "User required params for the modifying ample credits"
        property :ample_credits, :ample_credits_hash_for_credit, :required, "ample credits hash with amount"
        property :user, :user_hash_for_credit, :required, "user hash containing id"
        property :session_token, :string, :required, "session_token to authorize user access"
      end
      swagger_model :ample_credits_hash_for_credit do
        description "Ample credits hash for credit amount"
        property :amount, :string, :required, "Amount in string"
      end
      swagger_model :user_hash_for_credit do
        description "User hash for credit amount"
        property :id, :integer, :required, "id of the user whose ample credits is to be modified"
      end


      ##################################################################################
      #                   debit_from_ample_credits API                                #
      ##################################################################################
      #
      # API to debit passed amount from user's ample credits based on passed id
      #
      # Request::
      #    * id: user id of the user
      #    * amount: amount to be credited
      #
      # Response::
      #    * 200 ok: with user ample credits details.
      #    * 400 bad_request: if incorrect user-id passed or must parameters missing
      #
      def debit_from_ample_credits
        deciding_params = @application_helper.get_deciding_params(params)
        api = CreditsModule::V1::DebitFromAmpleCreditsApi.instance(deciding_params)
        response = api.enact(debit_from_ample_credits_request_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      #
      swagger_controller :ample_credits, 'CreditsModule APIs'
      swagger_api :debit_from_ample_credits do
        summary 'It debit amount from user ample credits'
        notes 'It takes user session token and id of user in whose account transaction is to be done.
        Also the amount which is to be credited.
        User should have specific permissions to access this API'
        param :body, :debit_request, :credit_request, :required, "credit request"
        response :bad_request, 'if request params are incorrect or incomplete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :debit_request do
        description "User and Credits required params for the modifying ample credits"
        property :ample_credits, :ample_credits_hash_for_debit, :required, "ample credits hash with amount"
        property :user, :user_hash_for_debit, :required, "user hash containing id"
        property :session_token, :string, :required, "session_token to authorize user access"
      end
      swagger_model :ample_credits_hash_for_debit do
        description "Ample credits hash for credit amount"
        property :amount, :string, :required, "Amount in string"
      end
      swagger_model :user_hash_for_debit do
        description "User hash for credit amount"
        property :id, :integer, :required, "id of the user whose ample credits is to be modified"
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      def credit_into_ample_credits_request_params
        user = params.require(:user).permit(:id)
        ample_credits = params.require(:ample_credits).permit(:amount)
        return {id: user[:id], amount: ample_credits[:amount]}
      end

      def debit_from_ample_credits_request_params
        user = params.require(:user).permit(:id)
        ample_credits = params.require(:ample_credits).permit(:amount)
        return {id: user[:id], amount: ample_credits[:amount]}
      end

    end
  end
end

