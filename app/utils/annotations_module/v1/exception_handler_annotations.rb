module AnnotationsModule
  module V1
    module ExceptionHandlerAnnotations
      include Annotations

      #
      # Annotation, this will add annotation corresponding to the function
      #
      def _ExceptionHandler_
        annotate_method(:exception, true, AnnotationsModule::V1::ExceptionHandlerAnnotations.method("execute"))
      end

      #
      # This funntion check if this is a leave jump_point or not
      # and correspondingly call other jp or call final call
      #
      # @param jp_context [Hash] it contain context of jump point and proc
      #
      #
      def self.execute(jp_context)
        custom_error_util = CommonModule::V1::CustomErrors
        base_response_decorator = BaseModule::V1::BaseResponseDecorator
        begin
          Annotations.proceed(jp_context)
        rescue custom_error_util::InvalidArgumentsError => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue custom_error_util::InsufficientDataError => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator.create_response_bad_request(e.message)
        rescue custom_error_util::ResourceNotFoundError => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator.create_response_bad_request(e.message)
        rescue custom_error_util::RecordAlreadyExistsError => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator.create_response_pre_condition_required(e.message)
        rescue custom_error_util::RunTimeError => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator.create_response_runtime_error(e.message)
        rescue custom_error_util::PreConditionRequiredError => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator.create_response_pre_condition_required(e.message)
        rescue custom_error_util::InvalidDataError => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator.create_response_invalid_data_passed(e.message)
        rescue custom_error_util::UnAuthorizedUserError => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator::create_response_unauthorized_access(e.message)
        rescue custom_error_util::UnAuthenticatedUserError => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator.create_not_found_error(e.message)
        rescue custom_error_util::FeatureDisabled => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue => e
          LoggingModule::V1::ErrorLoggerService.log_error_async(e)
          return base_response_decorator.create_response_runtime_error(e.message)
        end
      end

    end
  end
end
