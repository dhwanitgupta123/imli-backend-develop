module AnnotationsModule
  module V1
    module LatencyAnnotations
      include Annotations

      #
      # Annotation, this will add annotation corresponding to the function
      #
      def _Latency_
        annotate_method(:latency, true, AnnotationsModule::V1::LatencyAnnotations.method("execute"))
      end

      def self.execute(jp_context)
        t1 = Time.zone.now.to_f
        response = Annotations.proceed(jp_context)
        t2 = Time.zone.now.to_f

        latency = t2 - t1
        puts latency
        ##
        # LOG the metric
        # #
        return response
      end
    end
  end
end
