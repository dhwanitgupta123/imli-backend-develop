module AnnotationsModule
  module V1
    # This Annotation will run the corresponding functional block in ActiveRecord::Base::Transaction
    # For usage, just mention it above the required function and include this class while initializing
    # Example usage:
    # 
    # class SomeClass
    #   extend AnnotationsModule::V1::TransactionalBlockAnnotations
    # 
    #   _RunInTransactionBlock_
    #   def my_critical_function
    #     # Some code to execute in transaction
    #   end
    # end
    #
    module TransactionalBlockAnnotations
      include Annotations

      #
      # Annotation, this will add annotation corresponding to the function
      #
      def _RunInTransactionBlock_
        annotate_method(:exception, true, AnnotationsModule::V1::TransactionalBlockAnnotations.method("execute"))
      end

      #
      # This funntion check if this is a leave jump_point or not 
      # and correspondingly call other jp or call final call
      #
      # @param jp_context [Hash] it contain context of jump point and proc
      #
      def self.execute(jp_context)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        ActiveRecord::Base.transaction do
          return Annotations.proceed(jp_context)
        end
      end

    end #End of module
  end
end
