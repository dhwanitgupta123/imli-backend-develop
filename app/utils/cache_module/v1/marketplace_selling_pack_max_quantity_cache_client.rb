#
# Module to handle all the functionalities related to supply chain
#
module CacheModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Store MPSP new Max quantity in redis
    #
    module MarketplaceSellingPackMaxQuantityCacheClient

      NAMESPACE = 'MarketplaceSellingPack::MaxQuantity::'

      #
      # Write the key, value pair in storage
      #
      # @param key [String] key for mpsp
      # @param value [Integer] max quantity for mpsp
      #
      def self.write_key(key, value)
        CommonModule::V1::Cache.write key: key, value: value
      end

      #
      # Check if mpsp max quantity exists in storage
      #
      # @param mpsp max quantity key with id appended [String] key for mpsp max quantity
      #
      # @return [Boolean] true if key is in storage else false
      #
      def self.exists?(key)
        CommonModule::V1::Cache.exists?(key)
      end

      #
      # Create key to save in cache
      #
      # @param mpsp_id [integer]
      #
      # @return [String] appending with prefix
      #
      def self.create_key(mpsp_id)
        prefix = NAMESPACE + mpsp_id.to_s
        return prefix
      end

      #
      # Get Value of a key
      # 
      # @param mpsp_id [integer] MPSP id of which max quantity to be fetched
      # 
      # @return [Integer] max quantity
      #
      def self.get_max_quantity(mpsp_id)
        key = create_key(mpsp_id)
        max_quantity = CommonModule::V1::Cache.read(key)
        return max_quantity
      end
    end
  end
end
