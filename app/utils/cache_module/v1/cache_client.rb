#
# This module is responsible to handle all model caches
#
module CacheModule
  #
  # Version1 for validation module
  #
  module V1
    #
    # cache client
    #
    class CacheClient

      CACHE_UTILS = CommonModule::V1::Cache

      #
      # This function will be overrided by implementations of this interface
      #
      def self.get(params, namespace)
        key = params[:key].to_s
        value = CACHE_UTILS.read(key, namespace)

        return nil if  value.nil?

        return Marshal.load(value)
      end

      #
      # This function will be overrided by implementations of this interface
      #
      def self.set(params, namespace, ttl = nil)
        key = params[:key].to_s
        value = params[:value]

        if ttl.present?
          CACHE_UTILS.write({ namespace: namespace, key: key, value: Marshal.dump(value), expire: ttl})
        else
          CACHE_UTILS.write({ namespace: namespace, key: key, value: Marshal.dump(value)})
        end
      end

      #
      # Remove key from cache
      #
      def self.remove(params, namespace)
        key = params[:key].to_s
        CACHE_UTILS.delete(key, namespace)
      end

      #
      # Update version of cached data
      #
      def self.update_version(current_version_key, namespace)

        current_version = get_version(current_version_key, namespace)

        current_version = 0 if current_version.blank?

        current_version += 1

        current_version = current_version % 1000003

        CACHE_UTILS.write({namespace: namespace, key: current_version_key, value: current_version})

        return current_version
      end

      #
      # Get version of data
      #
      def self.get_version(key, namespace)
        CACHE_UTILS.read_int(key, namespace)
      end

    end
  end
end
