#
# This module is responsible to handle all model caches
#
module CacheModule
  #
  # Version1 for validation module
  #
  module V1
    #
    # categorization cache client
    #
    class CategorizationCacheClient < CacheModule::V1::CacheClient

      CACHE_UTILS = CommonModule::V1::Cache
      NAMESPACE = 'CATEGORIZATION_TREE::BRANDPACK::'
      CURRENT_VERSION_KEY = 'CURRENT_VERSION'
      CACHED_VERSION_KEY = 'CACHED_VERSION'
      DATA_KEY = 'DATA'

      #
      # update version of categorization data
      #
      def self.update_current_version
        return update_version(CURRENT_VERSION_KEY, NAMESPACE)
      end

      #
      # This function get the current and cached version of categorization tree and check if both are same or not
      # and if they are same then returns the cached categorization tree else return nil
      #
      def self.get_categorization_tree

        current_version = get_version(CURRENT_VERSION_KEY, NAMESPACE)

        return nil if current_version.blank?

        cached_version = get_version(CACHED_VERSION_KEY, NAMESPACE)

        return nil if current_version != cached_version

        return get({key: DATA_KEY}, NAMESPACE)

      end

      #
      # This function sets the categorization tree in cache with its current version
      #
      def self.set_categorization_tree(data)

        current_version = get_version(CURRENT_VERSION_KEY, NAMESPACE)

        if current_version.blank?
          update_current_version
        end

        set({key: DATA_KEY, value: data}, NAMESPACE)

        CACHE_UTILS.write({namespace: NAMESPACE, key: CACHED_VERSION_KEY, value: current_version})
      end

    end
  end
end
