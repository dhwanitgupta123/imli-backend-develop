module CategorizationModule
  module V1
    #
    # Categorization Api Service class to implement CRUD for categorization APIs
    #
    class CategoryUtil

      CATEGORY_DAO = CategorizationModule::V1::CategoryDao

      # 
      # This function determines the sub_categories
      #
      # @param args [Hash] it may contain category_id, sub_category_id and department_id
      # 
      # @return [Model] sub categories
      #
      def self.get_sub_categories(args)
        category_dao = CATEGORY_DAO.new
        return args[:sub_category_id] if args[:sub_category_id].present?

        return category_dao.get_sub_categories_by_categories(args[:category_id]) if args[:category_id].present?

        if args[:department_id].present?
          categories = category_dao.get_categories_by_department(args[:department_id])
          return category_dao.get_sub_categories_by_categories(categories)
        end

        return nil
      end

    end
  end
end
