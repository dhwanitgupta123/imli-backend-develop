module SettingsModule
  #
  # Version1 for settings module
  #
  module V1
    #
    # This is an interface which enforce all the validators to extend it
    # and implement validate function
    #
    module StaticSettings

      class ConstantSettings < SettingsModule::V1::StaticSettings::BaseStaticSettings

        def initialize(params = '')
          @settings_name = 'CONSTANT_SETTINGS'
          super
        end

        def fetch
          settings = super
          settings = settings.merge(create_map_of_all_computed_constants)
          return settings
        end

        def load_and_store
          constant_settings = load
          constants_map = create_map_of_all_required_constants(constant_settings)
          # Store above fetched settings in Redis
          store(constants_map)
          return constants_map
        end

        ###############################
        #       Private Functions     #
        ###############################

        private

        #
        # Function to get map of all relevant constants which are required
        # on mobile App.
        # Fetch values of constants from CACHE, because there source might change
        #
        def create_map_of_all_required_constants(constant_settings)
          cache_util = CommonModule::V1::Cache
          if constant_settings.blank?
            return {}
          end
          map = {}
          constant_settings.each do |key, value|
            cached_value = cache_util.read(value.to_s)
            map[key.to_s] = cached_value
          end
          return map
        end

        #
        # Create Map of extra constants, which are to be computed realtime
        #
        # @return [JSON] [Hash containing computed constants]
        #
        def create_map_of_all_computed_constants
          date_time_util = CommonModule::V1::DateTimeUtil

          current_server_time = date_time_util.get_current_time(false)
          current_server_time_epoch = date_time_util.convert_time_to_epoch(current_server_time)

          return {
            SERVER_TIME: current_server_time_epoch
          }
        end

      end # End of class
    end
  end
end
