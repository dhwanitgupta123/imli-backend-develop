module CommunicationsModule
  module V1
    module Messengers
      module V1
        #
        # Class to store the enums of SMS templates name
        #
        class SmsType
          COULD_NOT_PICK_UP_CALL = 'could_not_pick_up_call'
          CUSTOMER_DENIED = 'customer_denied'
          CUSTOMER_NO_SHOW = 'customer_no_show'
          DELIVERY_CANCELLED = 'delivery_cancelled'
          DELIVERY_SLOT_CHANGED = 'delivery_slot_changed'
          MEMBERSHIP_EXPIRED = 'membership_expired'
          MEMBERSHIP_EXPIRING = 'membership_expiring'
          MEMBERSHIP_UPGRADE = 'membership_upgrade'
          MEMBERSHIP_CANCELLED = 'membership_cancelled'
          NORMAL_CUSTOMER_RETURN = 'normal_customer_return'
          ORDER_PLACED = 'order_placed'
          ORDER_PLACED_WITH_TIME_SLOT = 'order_placed_with_time_slot'
          OTP = 'otp'
          OUT_FOR_DELIVERY = 'out_for_delivery'
          PAYMENT_FAILURE = 'payment_failure'
          POST_CUSTOMER_CALL = 'post_customer_call'
          PRODUCT_DELIVERED = 'product_delivered'
          RETURN_PACKAGE_OPEN = 'return_package_open'
          RETURN_PICK_UP = 'return_pick_up'
          RETURN_PICKED_UP = 'return_picked_up'
          RETURN_RECEIVED = 'return_received'
          RUNNING_LATE = 'running_late'
          WELCOME = 'welcome'
          SUCCESSFUL_REFERRAL = 'successful_referral'
          MEMBERSHIP_ORDER_EXPIRING = 'membership_order_expiring'
          RESCHEDULE_ORDER = 'delivery_slot_changed'
        end
      end
    end
  end
end
