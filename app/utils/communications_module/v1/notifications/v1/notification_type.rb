module CommunicationsModule
  module V1
    module Notifications
      module V1
        class NotificationType

          USER_STATES = UsersModule::V1::ModelStates::UserStates
          ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates

          SIGN_UP = {
            type: 'SIGN_UP',
            template: 'sign_up',
            title: 'Sign up',
            action: '',
            data: {}
          }
          WELCOME = {
            type: 'WELCOME',
            template: 'welcome',
            title: 'Welcome',
            action: 'ACTIVATE_USER',
            data: { user_state: USER_STATES::ACTIVE }
          }
          PLACED_ORDER = {
            type: 'PLACED_ORDER',
            template: 'placed_order',
            title: 'Order placed',
            action: 'OPEN_ORDERS',
            data: { order_state: ORDER_STATES::PLACED[:value] }
          }
          PLACED_ORDER_WITH_TIME_SLOT = {
            type: 'PLACED_ORDER_WITH_TIME_SLOT',
            template: 'placed_order_with_time_slot',
            title: 'Order placed',
            action: 'OPEN_ORDERS',
            data: { order_state: ORDER_STATES::PLACED[:value] }
          }
          PAYMENT_FAILURE = {
            type: 'PAYMENT_FAILURE',
            template: 'payment_failure',
            title: 'Payment failed',
            action: '',
            data: {}
          }
          OUT_FOR_DELIVERY = {
            type: 'OUT_FOR_DELIVERY',
            template: 'out_for_delivery',
            title: 'Out for delivery',
            action: 'OPEN_ORDERS',
            data: { order_state: ORDER_STATES::DISPATCHED[:value] }
          }
          DELIVERY_CANCELLED = {
            type: 'DELIVERY_CANCELLED',
            template: 'delivery_cancelled',
            title: 'Order Cancelled',
            action: 'OPEN_ORDERS',
            data: { order_state: ORDER_STATES::ORDER_CANCELLED[:value] }
          }
          SUCCESSFUL_REFERRAL = {
            type: 'SUCCESSFUL_REFERRAL',
            template: 'successful_referral',
            title: "You've received Rs. %{amount}",
            action: '',
            data: {}
          }
          MEMBERSHIP_EXPIRING = {
            type: 'MEMBERSHIP_EXPIRING',
            template: 'membership_expiring',
            title: 'Membership Expiring',
            action: 'OPEN_SHOP',
            data: {}
          }
          MEMBERSHIP_EXPIRED = {
            type: 'MEMBERSHIP_EXPIRED',
            template: 'membership_expired',
            title: 'Membership Expired',
            action: 'OPEN_SHOP',
            data: {}
          }
          MEMBERSHIP_UPGRADE = {
            type: 'MEMBERSHIP_UPGRADE',
            template: 'membership_upgrade',
            title: 'Membership Upgraded',
            action: 'OPEN_SHOP',
            data: {}
          }
          MEMBERSHIP_CANCELLED = {
            type: 'MEMBERSHIP_CANCELLED',
            template: 'membership_cancelled',
            title: 'Membership Cancelled',
            action: 'OPEN_SHOP',
            data: {}

          }
          RESCHEDULE_ORDER = {
            type: 'RESCHEDULE_ORDER',
            template: 'delivery_slot_changed',
            title: "Delivery slot changed",
            action: '',
            data: {}
          }
        end
      end
    end
  end
end
