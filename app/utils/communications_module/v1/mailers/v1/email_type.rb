module CommunicationsModule
  module V1
    module Mailers
      module V1
        class EmailType
          WELCOME = {
            type: 'WELCOME',
            template: 'welcome'
          }
          PLACED_ORDER = {
            type: 'PLACED_ORDER',
            template: 'placed_order'
          }
          AGGREGATED_PRODUCTS = {
            type: 'AGGREGATED_PRODUCTS',
            template: 'aggregated_orders'
          }
          SINGLE_ORDER_PACKING_DETAILS = {
            type: 'SINGLE_ORDER_PACKING_DETAILS',
            template: 'single_order_packing_details'
          }
          PENDING_USERS = {
            type: 'ALL_PENDING_USERS',
            template: 'all_pending_users'
          }
          EMAIL_INVITES = {
            type: 'EMAIL_INVITES',
            template: 'email_invites'
          }
          PRODUCT_SUGGESTION = {
            type: 'PRODUCT_SUGGESTION',
            template: 'product_suggestion'
          }
          INVITE = {
            type: 'INVITE',
            template: 'invite_mail'
          }
          PLACED_PURCHASE_ORDER = {
            type: 'PLACED_PURCHASE_ORDER',
            template: 'purchase_order_placed'
          }
          VERIFIED_PURCHASE_ORDER = {
            type: 'VERIFIED_PURCHASE_ORDER',
            template: 'purchase_order_verified'
          }
          PURCHASE_ORDER_FILL_RATE = {
            type: 'PURCHASE_ORDER_FILL_RATE',
            template: 'purchase_order_fill_rate'
          }
          PURCHASE_ORDER_DUMP = {
            type: 'PURCHASE_ORDER_DUMP',
            template: 'purchase_order_dump'
          }
          OUT_OF_STOCK_PRODUCTS_REPORT = {
            type: 'OUT_OF_STOCK_PRODUCTS_REPORT',
            template: 'out_of_stock_products_report'
          }
          MEMBERSHIP_EXPIRING = {
            type: 'MEMBERSHIP_EXPIRING',
            template: 'membership_expiring'
          }
          MEMBERSHIP_EXPIRED = {
            type: 'MEMBERSHIP_EXPIRED',
            template: 'membership_expired'
          }
          MEMBERSHIP_UPGRADE = {
            type: 'MEMBERSHIP_UPGRADE',
            template: 'membership_upgrade'
          }
          MEMBERSHIP_CANCELLED = {
            type: 'MEMBERSHIP_CANCELLED',
            template: 'membership_cancelled'
          }
          MONTHLY_ACCOUNT_SUMMARY = {
            type: 'MONTHLY_ACCOUNT_SUMMARY',
            template: 'monthly_account_summary'
          }
        end
      end
    end
  end
end
