module CommunicationsModule
  module V1
    module ThirdPartyModule
      module ExotelVoiceStatusCodes
        extend self

        MAP = [
          {
            id: 1,
            key: 'queued',
            voice_communication_state_reference: CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationStates.get_detail_hash_by_key('QUEUED')
          },
          {
            id: 2,
            key: 'in-progress',
            voice_communication_state_reference: CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationStates.get_detail_hash_by_key('IN_PROGRESS')
          },
          {
            id: 3,
            key: 'completed',
            voice_communication_state_reference: CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationStates.get_detail_hash_by_key('COMPLETED')
          },
          {
            id: 4,
            key: 'failed',
            voice_communication_state_reference: CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationStates.get_detail_hash_by_key('FAILED')
          },
          {
            id: 5,
            key: 'busy',
            voice_communication_state_reference: CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationStates.get_detail_hash_by_key('BUSY')
          },
          {
            id: 6,
            key: 'no-answer',
            voice_communication_state_reference: CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationStates.get_detail_hash_by_key('NO_ANSWER')
          }
        ]

        def get_id_by_key(key)
          references = MAP.select {|hash| hash[:key] == key.to_s}
          return references[0][:id] if references.present?
          return nil
        end

        def get_key_by_id(id)
          references = MAP.select {|hash| hash[:id] == id.to_i}
          return references[0][:key] if references.present?
          return nil
        end

        def get_label_by_id(id)
          references = MAP.select {|hash| hash[:id] == id.to_i}
          hash_entry = references[0] if references.present?
          return {} if hash_entry.blank?
          label = hash_entry[:label] || ''
          return {label: label}
        end

        def get_detail_hash_by_key(key)
          references = MAP.select {|hash| hash[:key] == key.to_s}
          return references[0] if references.present?
          return nil
        end

        def get_voice_communication_status_map_value_by_key(key)
          reference = get_detail_hash_by_key(key)
          return reference[:voice_communication_state_reference][:value] if reference.present?
        end

      end
    end
  end
end
