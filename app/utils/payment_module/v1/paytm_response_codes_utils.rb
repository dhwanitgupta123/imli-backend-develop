module PaymentModule
  module V1
    #
    # Module to store the response codes of Paytm
    #
    module PaytmResponseCodesUtils
      SUCCESS = "01"
    end
  end
end