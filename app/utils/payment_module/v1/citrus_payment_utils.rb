module PaymentModule
  module V1
    class CitrusPaymentUtils
      require 'base64'
      require 'cgi'
      require 'openssl'
      def hmac_sha1(data, secret)
        hmac = OpenSSL::HMAC.hexdigest(OpenSSL::Digest::Digest.new('sha1'), secret.encode("ASCII"), data.encode("ASCII"))
        return hmac
      end
    end
  end
end
