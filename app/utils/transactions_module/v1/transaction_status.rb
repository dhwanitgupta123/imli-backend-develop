module TransactionsModule
  module V1
    class TransactionStatus
      SUCCESS = 1
      FAILED = 2
      CANCELLED = 3
      FULLY_REFUNDED = 4
      PARTIALLY_REFUNDED = 5
    end
  end
end
