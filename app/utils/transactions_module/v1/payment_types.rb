module TransactionsModule
  module V1
    class PaymentTypes
      ORDER_PAYMENT = 1
      ORDER_REFUND = 2
      MEMBERSHIP_PAYMENT = 3
      MEMBERSHIP_REFUND = 4 

      def self.get_payment_handler_from_payment_type(payment_type)
        case payment_type.to_i
        when ORDER_PAYMENT
          return MarketplaceOrdersModule::V1::PaymentService
        when MEMBERSHIP_PAYMENT
          return UsersModule::V1::MembershipModule::V1::MembershipPaymentService
        end
      end

      def self.get_payment_dao_from_payment_type(payment_type)
        case payment_type.to_i
        when 1..2
          return MarketplaceOrdersModule::V1::PaymentDao
        when 3..4
          return UsersModule::V1::MembershipModule::V1::MembershipPaymentDao
        end
      end
    end
  end
end
