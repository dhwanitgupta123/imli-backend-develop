#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    #
    # Validate if user membership is not in expired state 
    #
    class AppVersionValidator < ValidationsModule::V1::ValidationInterface      

      CONTENT_UTIL = CommonModule::V1::Content
      VALIDATION_RESPONSE_DECORATOR = ValidationsModule::V1::ValidationResponseDecorator
      # 
      # Validate if given user is not in expired state
      #
      # @param user [ModelObject] User to check status for
      # 
      # @return [Hash] containg result if result is false then it also contain corresponding message 
      #
      def validate(user)
        version_number = CommonModule::V1::Cache.read_int('VERSION_NUMBER_FOR_MEMBERSHIP_FEATURE') || 18
        if USER_SESSION[:platform_type] == 'ANDROID' && USER_SESSION[:version_number] < version_number
          return { result: false, response: VALIDATION_RESPONSE_DECORATOR.create_response_bad_request(CONTENT_UTIL::UPDATE_YOUR_APP) }
        end
        return {result: true}
      end

    end #End of class
  end
end