#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    #
    # Validate the User based on it's state
    #
    class UserAccessValidator < ValidationsModule::V1::ValidationInterface      

      CONTENT_UTIL = CommonModule::V1::Content
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator
      # 
      # Validate if given user is in active state or not
      #
      # @param user [ModelObject] User to check status for
      # 
      # @return [Hash] containg result if result is false then it also contain corresponding message 
      #
      def validate(user)
        return { result: true } if user.active? || user.pending?
        return { result: false, response: USER_RESPONSE_DECORATOR.create_response_bad_request(CONTENT_UTIL::INACTIVE_STATE) } if user.inactive?
        return { result: false, response: USER_RESPONSE_DECORATOR.create_response_unauthorized_access(CONTENT_UTIL::TEMP_BLOCKED_USER) } if user.deleted? || user.blacklisted?
        return { result: false, response: USER_RESPONSE_DECORATOR.create_response_bad_request }
      end
    end
  end
end