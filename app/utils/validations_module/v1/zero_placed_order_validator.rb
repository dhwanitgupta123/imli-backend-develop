#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    #
    # Validate if user has zero order
    #
    class ZeroPlacedOrderValidator < ValidationsModule::V1::ValidationInterface      

      CONTENT_UTIL = CommonModule::V1::Content
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator
      # 
      # Validate if given user has order which is placed, completed or any of the final state
      # if there is then return false else if user 
      #
      # @param user [ModelObject] User to check order for
      # 
      # @return [Hash] containg result if result is false then it also contain corresponding message 
      #
      def validate(user)
        order_dao = MarketplaceOrdersModule::V1::OrderDao.new

        is_zero_order = order_dao.is_first_order_of_user?(user)

        return { result: true } if is_zero_order == true
        return { result: false, response: USER_RESPONSE_DECORATOR.create_response_bad_request(CONTENT_UTIL::ALREADY_ORDERED) }
      end
    end
  end
end