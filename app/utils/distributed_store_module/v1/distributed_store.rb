module DistributedStoreModule
  module V1
    #
    # Distributed Store Management Util
    # Currently, used to interact with CONSUL (distributed storage system)
    #
    class DistributedStore < BaseModule::V1::BaseUtil

      #
      # Get key from distributed store
      #
      # @param key [String] [key for which value is to be fetched]
      #
      # @return [String] [Stored value corresponding to given key]
      #
      def self.get(key)
        Diplomat::Kv.get(key)
      end

      # 
      # Store/Set value for key in distributed store (CONSUL)
      #
      # @param key [String] [key for which value is to be stored]
      # @param value [String] [value to store]
      # 
      # @return [Boolean] [true if successfully stored]
      #
      def self.put(key, value)
        Diplomat::Kv.put(key, value)
      end

    end
  end
end