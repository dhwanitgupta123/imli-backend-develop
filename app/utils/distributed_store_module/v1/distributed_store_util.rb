module DistributedStoreModule
  module V1
    #
    # Distributed Store Management Util
    # Currently, used to interact with CONSUL (distributed storage system)
    #
    class DistributedStoreUtil < BaseModule::V1::BaseUtil

      DISTRIBUTED_STORE = DistributedStoreModule::V1::DistributedStore
      SENTRY_LOGGER = CommonModule::V1::Logger

      #
      # Fetch default and environment specific config from CONSUL and merge them both
      # If not forund, return nil
      #
      # @param configuration_path [String] [Relative path from which configuration need to be fetched]
      #
      # @return [JSON] [Hash fetched from consul]
      #
      def self.fetch_environment_specific_config_from_distributed_store(configuration_path)
        begin
          default_config = JSON.parse(DISTRIBUTED_STORE.get('default' + configuration_path))
        rescue => e
          puts '[FAILED]: CONSUL - Failed to fetch Default value for '+ configuration_path.to_s + ' , error= ' + e.message
          # Do not raise anything
        end
        begin
          environment_config = JSON.parse(DISTRIBUTED_STORE.get(Rails.env + configuration_path))
        rescue => e
          puts '[FAILED]: CONSUL - Failed to fetch ' + Rails.env.to_s + ' specific value for '+ configuration_path.to_s + ' , error= ' + e.message
          # Do not raise anything
        end
        if default_config.present? || environment_config.present?
          puts '[SUCCESS]: CONSUL - Config fetched from consul successfully'
        end
        if default_config.present? && environment_config.present?
          # Merge default configuration with Environment configurations
          return default_config.merge(environment_config)
        else
          return default_config || environment_config
        end
        return nil
      end

      #
      # Get Config Value from FEATURES settings
      #
      # @param key [String] [key to be fetched]
      #
      # @return [String] [If present, return value associated with the key else return nil]
      #
      def self.get_config_value(key)
        begin
          return DISTRIBUTED_STORE.get(key)
        rescue => e
          SENTRY_LOGGER.log_exception(e)
          return nil
        end
      end

      #
      # Update Config Value from FEATURES settings
      #
      # @param key [String] [Key to be updated]
      # @param value [String] [Value to insert]
      #
      # @return [Boolean] [true if successfull else false]
      #
      def self.update_config_value(key, value)
        begin
          DISTRIBUTED_STORE.put(key, value)
          # TO-DO
          # RELOAD value from Consul and store in REDIS
          # SETTINGS_UTIL.get_and_locally_store_feature_settings
          return true
        rescue => e
          SENTRY_LOGGER.log_exception(e)
          return false
        end
      end

    end
  end
end