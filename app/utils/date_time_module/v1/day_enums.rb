module DateTimeModule
  module V1
    module DayEnums

      DAYS_MAP = {
        0 => 'SUNDAY',
        1 => 'MONDAY',
        2 => 'TUESDAY',
        3 => 'WEDNESDAY',
        4 => 'THURSDAY',
        5 => 'FRIDAY',
        6 => 'SATURDAY'
      }

      DAYS = [0 , 1, 2, 3, 4, 5, 6]

      def self.get_day_by_key(int_day)
        DAYS_MAP[int_day]
      end

      def self.get_day_by_value(day)
        DAYS_MAP.key(day.upcase)
      end
    end
  end
end
