module CommonModule
  module V1
    # 
    # PlanNames has enums corresponding to the possible types of Plan model
    #
    class PlanKeys
      TRIAL      = 1
      AMPLE_GO   = 2
      AMPLE_MORE = 3
      AMPLE_PRO  = 4

      #
      # Get plan short description
      # :: TO-DO ::
      # This needs to be move to DB column in plans table, and will be entered
      # by business side from Panel
      #
      # @param plan_key [Integer] [Plan key]
      #
      # @return [String] [short description for that plan]
      #
      def self.get_plan_short_description(plan_key)
        return '' if plan_key.blank?
        case plan_key.to_i
        when TRIAL
          return 'Trial plan'
        when AMPLE_GO
          return 'Monthly plan'
        when AMPLE_MORE
          return 'Half yearly Plan'
        when AMPLE_PRO
          return 'Annual Plan'
        else
          return ''
        end
      end

    end #End of class
  end
end
