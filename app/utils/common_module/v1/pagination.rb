#
# Common Module which caters common functionality
#
module CommonModule
  #
  # Version 1 for Pagination
  #
  module V1
    #
    # Class pagination to apply filter based on page, per_page & other filters
    #
    class Pagination
      # TO-DO:
      # Remove these normal variables and use only prefixed
      # variables in future

      # if no page_no is specified
      PAGE_NO = 1

      # if length of page is not specified
      PER_PAGE = 20

      # sort object based on which attribute
      SORT_BY = 'id'

      # default sort order Ascending/Descending
      ORDER = 'ASC'

      # Default values
      DEFAULT_PAGE_NO = 1
      DEFAULT_PER_PAGE = 20
      DEFAULT_SORT_BY = 'id'
      DEFAULT_ORDER = 'ASC'
      # Infinited number of per page results (Value of 2**32)
      INFINITE_PER_PAGE = 2**32
      INFINITE_PAGE_NO = 1
      # allowed orders
      ALLOWED_ORDERS = %w(ASC, DESC)
      CATEGORIZATION_SORT_BY = 'priority'

      #
      # Apply pagination over some given set of elements
      #
      # @param model [Class] [Model Class]
      # @param filtered_elements [Array] [ActiveRecord Array of model object elements]
      # @param pagination_params = {} [JSON] [Hash of paginated properties]
      #
      # @return [JSON] [Hash of elements and its page count]
      #
      def get_paginated_array_elements(array, pagination_params = {})
        set_pagination_properties(pagination_params)
        start_index = (@page_no - 1) * @per_page
        # Now compute total page count based on final filtered elements
        page_count = (array.count / @per_page).ceil
        # apply per page limit on filterd elements
        elements = array.slice(start_index, @per_page)

        return {elements: elements, page_count: page_count}
      end

      #
      # setting pagination parameters for the model instance
      #
      def set_pagination_properties(pagination_params)
        pagination_params = pagination_params || {}
        @per_page = (pagination_params[:per_page] || PER_PAGE).to_f
        @per_page = PER_PAGE.to_f if @per_page == 0
        @per_page = INFINITE_PER_PAGE.to_f if @per_page < 0

        @page_no = (pagination_params[:page_no] || PAGE_NO).to_f
        @page_no = PAGE_NO.to_f if @page_no <= 0
      end
    end
  end
end
