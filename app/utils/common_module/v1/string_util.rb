module CommonModule
  module V1

    class StringUtil < BaseModule::V1::BaseUtil

      def self.remove_hiphens(str)
        return '' if str.blank?
        str.gsub('-', '')
      end

      def self.remove_dots(str)
        return '' if str.blank?
        str.gsub('.', '')
      end

      def self.downcase(str)
        return '' if str.blank?
        str.downcase
      end

      def self.remove_characters(str, regex = '')
        return '' if str.blank?
        return str if regex.blank?

        str.gsub(regex, '')
      end

      def self.strip(str)
        return '' if str.blank?
        str.strip
      end

    end # End of class
  end
end


