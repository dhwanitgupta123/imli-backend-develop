# Predicates to decide routing for both routes.rb and module level routing
# @author Sumit <sumitchhuttani007@gmail.com>
module CommonModule
  module V1
    class RoutingPredicates < BaseModule::V1::BaseUtil
      
      #
      # Matches passed version with corresponding version defined in
      # app routing configuration file
      # @param version [String] [version to match]
      # @param args [Hash] [containing decision factors]
      # 
      # @return [Boolean] [true if matches else false]
      def self.match_from_yml(version, args)
        return false if args.nil?

        decider_version = $APP_ROUTING_CONFIG[args[:version]]["providers_module"]
        return false if decider_version.blank?
        # Any case comparision of strings
        (version.to_s).casecmp(decider_version.to_s) == 0
      end

      # 
      # Matches passed version with app_version passed as
      # request params
      # @param version [String] [version to match]
      # @param request [Request] [Request object required for verification]
      # 
      # @return [Boolean] [true if matches else false]
      def self.match_app_version(version, request)
        # any case comparision of strings
        version.casecmp(request.params[:app_version]) == 0
      end

    end
  end
end