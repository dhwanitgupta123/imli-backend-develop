module CommonModule
  module V1
    module AmountType
      extend self

      MAP = [
        {
          id: 1,
          key: 'RUPEES',
          label: 'Rupees'
        },
        {
          id: 2,
          key: 'PERCENTAGE',
          label: 'Percentage'
        }
      ]

      def get_id_by_key(key)
        references = MAP.select {|hash| hash[:key] == key.to_s}
        return references[0][:id] if references.present?
        return nil
      end

      def get_label_by_id(id)
        references = MAP.select {|hash| hash[:id] == id.to_i}
        return eval(references[0][:label]) if references.present?
        return ''
      end

      def get_key_by_id(id)
        references = MAP.select {|hash| hash[:id] == id.to_i}
        return references[0][:key] if references.present?
        return ''
      end

    end
  end
end
