module TriggersModule
  module V1
    # 
    # Enums corresponding to the possible trigger
    # 
    class TriggerEnum

      TRIGGER_ENUM = [
        {
          id: 1,
          trigger: 'ON_REFERRAL_ACTIVATION'
        },
        {
          id: 2,
          trigger: 'AFTER_FIRST_ORDER'
        }
      ]

      # 
      # Return trigger_id hash by given trigger as input
      #
      # @param trigger [Integer] trigger name
      # 
      # @return [Integer] id of trigger
      #
      def self.get_trigger_id_by_trigger(trigger)
        trigger_enum = TRIGGER_ENUM.select {|trigger_row| trigger_row[:trigger] == trigger.to_s}
        return trigger_enum[0][:id] if trigger_enum.present?
        return nil
      end
    end
  end
end
