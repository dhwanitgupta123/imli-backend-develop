module UsersModule
  module V1
    class ProviderType < BaseModule::V1::BaseUtil
      SMS = 1
      GENERIC = 2
      FORGOT_PASSWORD = 3
      FB = 4
      GOOGLE = 5
    end
  end
end