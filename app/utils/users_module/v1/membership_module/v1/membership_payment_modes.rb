module UsersModule
  module V1
    module MembershipModule
      module V1
        # Payment Modes are linked with payment details.
        # If you are adding any key here, Don't forget to modify its corresponding
        # yml file (config/payment_details.yml), and vice versa
        #
        class MembershipPaymentModes
          DEBIT_CARD = 1
          CREDIT_CARD = 2
          NET_BANKING = 3
          ALLOWED_MODES = [1,2,3]
        end
      end
    end
  end
end
