module UsersModule
  module V1
  	class UserRegistrationUtil

      CACHE_UTIL = CommonModule::V1::Cache
      REFERRAL_POPUP_NAMESPACE = 'NEW_USER::REFERRAL_POPUP'

      def self.init_user_constants(user)
        set_referral_popup_constant(user)
      end

      def self.set_referral_popup_constant(user)
        CACHE_UTIL.write({key: user.phone_number.to_s, value: 0, namespace: REFERRAL_POPUP_NAMESPACE})
      end

      def self.get_referral_popup_constant(user)
        return CACHE_UTIL.read_int(user.phone_number.to_s, REFERRAL_POPUP_NAMESPACE)
      end

      def self.delete_referral_popup_constant(user)
        CACHE_UTIL.delete(user.phone_number.to_s, REFERRAL_POPUP_NAMESPACE)
      end
  	end
  end
end
