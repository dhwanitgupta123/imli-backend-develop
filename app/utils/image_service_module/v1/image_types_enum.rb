module ImageServiceModule
  module V1
    module ImageTypesEnum
      extend self

      IMAGE_TYPES = [
        {
          id: 1,
          label: 'BANNER'
        }
      ]

      def get_image_type_by_label(label)
        type_enum = IMAGE_TYPES.select {|type| type[:label] == label.to_s}
        return type_enum[0][:id] if type_enum.present?
        return nil
      end
    end
  end
end
