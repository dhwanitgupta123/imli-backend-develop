module ImageServiceModule
  module V1
    module ImageStyles
      PLAN_IMAGE_STYLE_FILTERS = [
        'xxxhdpi_small',
        'xhdpi_medium',
        'xhdpi_small',
        'mdpi_large',
        'mdpi_medium'
      ]

      def self.get_image_styles_by_image_type(type)
        if type.present? && type.to_i == ImageServiceModule::V1::ImageTypesEnum.get_image_type_by_label('BANNER')
          return S3_IMAGE_STYLES[:carousel].merge(S3_IMAGE_STYLES[:default])
        else
          return S3_IMAGE_STYLES[:default]
        end
      end
    end
  end
end
