#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    class ApiLogAggregatorWorker < DataAggregatorWorker

      def perform
        api_log_index_service = DataAggregationModule::V1::ApiLogIndexService.new
        api_log_index_service.update_index
      end
    end
  end
end
