#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    class UserAggregatorWorker < DataAggregatorWorker

      def perform
        user_index_service = DataAggregationModule::V1::UserIndexService.new
        user_index_service.update_index
      end
    end
  end
end
