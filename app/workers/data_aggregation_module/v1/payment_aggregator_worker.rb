#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    class PaymentAggregatorWorker < DataAggregatorWorker

      def perform
        payment_index_service = DataAggregationModule::V1::PaymentIndexService.new
        payment_index_service.re_initialize_index
      end
    end
  end
end
