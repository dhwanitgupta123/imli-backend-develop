#
# This module handle all functionalities of scripts 
#
module ScriptsModule
  #
  # Version1 fror scripts module 
  #
  module V1
    class PriceComparatorWorker
      include Sidekiq::Worker
      sidekiq_options :queue => :job_queue, :retry => false

      def perform(file_path)
        scripts_service = ScriptsModule::V1::ScriptsService.new
        scripts_service.initiate_crawler(file_path)
      end
    end
  end
end
