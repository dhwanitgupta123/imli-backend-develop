#
# Module to handle functionalities related to file upload to S3
#
module FileServiceModule
  #
  # Version1 for file service module
  #
  module V1
    #
    # This class is super class for each file service worker
    #
    class FileServiceWorker
      include Sidekiq::Worker
      sidekiq_options queue: :file_queue
      FILE_SERVICE_UTIL = FileServiceModule::V1::FileServiceUtil
    end
  end
end