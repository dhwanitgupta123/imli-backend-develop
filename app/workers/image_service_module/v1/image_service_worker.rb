#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    #
    # This class is super class for each image service worker
    #
    class ImageServiceWorker
      include Sidekiq::Worker
      sidekiq_options queue: :image_queue
      IMAGE_SERVICE = ImageServiceModule::V1::ImageService
    end
  end
end