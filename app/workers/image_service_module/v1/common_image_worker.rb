#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    #
    # This class is responsible to upload image to S3
    #
    class CommonImageWorker < ImageServiceWorker

      #
      # Call image service to move images to s3
      #
      # @param args [Array] array of image_id's
      #
      def perform(args)
        image_service = IMAGE_SERVICE.new
        image_service.sync_image_with_remote_storage(args)
      end
    end
  end
end
