module LoggingModule
  module V1
    class LogWorker
      include Sidekiq::Worker
      sidekiq_options :queue => :default, :retry => false
    end
  end
end