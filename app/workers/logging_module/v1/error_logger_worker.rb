module LoggingModule
  module V1
    class ErrorLoggerWorker < LogWorker

      #
      # function to save the log in ActivityLog table
      #
      # @param logs [flat_hash] containing information to log
      #
      def perform(args)
        LoggingModule::V1::ErrorLoggerService.log_error(args)
      end

    end
  end
end
