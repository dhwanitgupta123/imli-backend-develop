module LoggingModule
  module V1
    class EventLoggerWorker < LogWorker

      #
      # function to save the log in ActivityLog table
      #
      # @param logs [flat_hash] containing information to log
      #
      def perform(args)
        LoggingModule::V1::EventLoggerService.log_event(args)
      end

    end
  end
end
