module AnalyzerModule
  module V1
    class ActivityAnalyzerWorker
      include Sidekiq::Worker
      sidekiq_options :queue => :default, :retry => false
    end
  end
end
