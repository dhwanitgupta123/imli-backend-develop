module AnalyzerModule
  module V1
    module UserAnalyzer
      module V1
        class UserActivityAnalyzerWorker < AnalyzerModule::V1::ActivityAnalyzerWorker
          USER_ACTIVITY_ANALYZER_MODEL = AnalyzerModule::V1::UserAnalyzer::V1::UserActivityAnalyzer
          # 
          # function to perform analyzing user activity
          # 
          # @param phone_number [String] unique id of user
          # 
          def perform(phone_number)
            user_activity_analyzer = USER_ACTIVITY_ANALYZER_MODEL.new
            user_activity_analyzer.analyze(phone_number)
          end
        end
      end
    end
  end
end
