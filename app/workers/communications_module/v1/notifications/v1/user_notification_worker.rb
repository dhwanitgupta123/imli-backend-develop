module CommunicationsModule
  module V1
    module Notifications
      module V1
        class UserNotificationWorker < NotificationWorker

          NOTIFICATION_TYPE = CommunicationsModule::V1::Notifications::V1::NotificationType
          PARSE_SERVICE = CommunicationsModule::V1::Notifications::V1::ParseNotificationService

          # 
          # function to retrieve perform job for the worker and 
          # delegate it to corresponding instance function
          # 
          # Paramaters::
          #   * notification_attributes [flat hash] contains data points for the
          #     notification templates
          # 
          def perform(notification_attributes)
            case notification_attributes['notification_type']
            when NOTIFICATION_TYPE::SIGN_UP[:type]
              template = NOTIFICATION_TYPE::SIGN_UP[:template]
              notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::SIGN_UP[:title],
                action: NOTIFICATION_TYPE::SIGN_UP[:action],
                data: NOTIFICATION_TYPE::SIGN_UP[:data]
              })
            when NOTIFICATION_TYPE::WELCOME[:type]
              template = NOTIFICATION_TYPE::WELCOME[:template]
               notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::WELCOME[:title],
                action: NOTIFICATION_TYPE::WELCOME[:action],
                data: NOTIFICATION_TYPE::WELCOME[:data]
              })
             when NOTIFICATION_TYPE::SUCCESSFUL_REFERRAL[:type]
              amount = notification_attributes['amount_credited']
              title = NOTIFICATION_TYPE::SUCCESSFUL_REFERRAL[:title] % {amount: amount.to_s}
              template = NOTIFICATION_TYPE::SUCCESSFUL_REFERRAL[:template]
              notification_attributes = notification_attributes.merge({
                title: title,
                action: NOTIFICATION_TYPE::SUCCESSFUL_REFERRAL[:action],
                data: NOTIFICATION_TYPE::SUCCESSFUL_REFERRAL[:data]
              })
            when NOTIFICATION_TYPE::MEMBERSHIP_EXPIRING[:type]
              template = NOTIFICATION_TYPE::MEMBERSHIP_EXPIRING[:template]
               notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::MEMBERSHIP_EXPIRING[:title],
                action: NOTIFICATION_TYPE::MEMBERSHIP_EXPIRING[:action],
                data: NOTIFICATION_TYPE::MEMBERSHIP_EXPIRING[:data]
              })
            when NOTIFICATION_TYPE::MEMBERSHIP_EXPIRED[:type]
              template = NOTIFICATION_TYPE::MEMBERSHIP_EXPIRED[:template]
               notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::MEMBERSHIP_EXPIRED[:title],
                action: NOTIFICATION_TYPE::MEMBERSHIP_EXPIRED[:action],
                data: NOTIFICATION_TYPE::MEMBERSHIP_EXPIRED[:data]
              })
            when NOTIFICATION_TYPE::MEMBERSHIP_UPGRADE[:type]
              template = NOTIFICATION_TYPE::MEMBERSHIP_UPGRADE[:template]
               notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::MEMBERSHIP_UPGRADE[:title],
                action: NOTIFICATION_TYPE::MEMBERSHIP_UPGRADE[:action],
                data: NOTIFICATION_TYPE::MEMBERSHIP_UPGRADE[:data]
              })
            when NOTIFICATION_TYPE::MEMBERSHIP_CANCELLED[:type]
              template = NOTIFICATION_TYPE::MEMBERSHIP_CANCELLED[:template]
               notification_attributes = notification_attributes.merge({
                title: NOTIFICATION_TYPE::MEMBERSHIP_CANCELLED[:title],
                action: NOTIFICATION_TYPE::MEMBERSHIP_CANCELLED[:action],
                data: NOTIFICATION_TYPE::MEMBERSHIP_CANCELLED[:data]
              })
            end
            deliver_push_notification(notification_attributes, template)
          end

          # 
          # function to push notification to the user asynchronously
          # 
          # Paramaters::
          #   * notification_attributes [flat hash] contains data points for the
          #     notification templates
          # 
          def deliver_push_notification(notification_attributes, template)
            PARSE_SERVICE.send(notification_attributes, template)
          end
        end
      end
    end
  end
end
