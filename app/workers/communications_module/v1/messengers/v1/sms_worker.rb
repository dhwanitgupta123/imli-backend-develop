module CommunicationsModule
  module V1
    module Messengers
      module V1
        class SmsWorker
          include Sidekiq::Worker
          SMS_PROVIDER = CommunicationsModule::V1::Messengers::V1::SmsProviderHelper
          SMS_ATTRIBUTES_HELPER = CommunicationsModule::V1::Messengers::V1::SmsAttributesHelper
          RETRY_COUNT = 5
          sidekiq_options :queue => :sms_queue, :backtrace => true, :retry => RETRY_COUNT
          TOGGLE = APP_CONFIG['config']['TOGGLE_SMS_PLATFORM'].to_i || SMS_PROVIDER::GUPSHUP

          # 
          # function to retrieve perform job for the worker and 
          # call sms service send message 
          # 
          # Paramaters::
          #   * sms_attributes [flat hash] contains data points for the sms to be send
          #
          def perform(sms_attributes, message_type)
            sms_attributes = SMS_ATTRIBUTES_HELPER.get_sms_attributes(sms_attributes, message_type)
            if TOGGLE == SMS_PROVIDER::ROUTESMS
              sms_service = CommunicationsModule::V1::Messengers::V1::RouteSmsService
            elsif TOGGLE == SMS_PROVIDER::GUPSHUP
              sms_service = CommunicationsModule::V1::Messengers::V1::GupShupSmsService
            end  
            if Rails.env == 'production' || Rails.env == 'feature' || Rails.env == 'pre_prod'
              sms_service.send_message(sms_attributes)
            end
            return true
          end
        end
      end
    end
  end
end
