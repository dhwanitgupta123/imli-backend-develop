module CommunicationsModule
  module V1
    module Mailers
      module V1
        class EmailWorker
          include Sidekiq::Worker
          sidekiq_options :queue => :email_queue, :backtrace => true, :retry => 5
        end
      end
    end
  end
end