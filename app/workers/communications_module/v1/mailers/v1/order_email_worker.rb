module CommunicationsModule
  module V1
    module Mailers
      module V1
        class OrderEmailWorker < EmailWorker
          CACHE_UTIL = CommonModule::V1::Cache
          MAILER_SERVICE = CommunicationsModule::V1::Mailers::V1::MailerService
          EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType

          # 
          # function to retrieve perform job for the worker and 
          # delegate it to corresponding instance function
          # 
          # Paramaters::
          #   * mail_attributes [flat hash] contains data points for the email templates
          #
          def perform(mail_attributes)
            case mail_attributes['email_type']
            when EMAIL_TYPE::PLACED_ORDER[:type]
              template_name = EMAIL_TYPE::PLACED_ORDER[:template]
            when EMAIL_TYPE::AGGREGATED_PRODUCTS[:type]
              template_name = EMAIL_TYPE::AGGREGATED_PRODUCTS[:template]
            when EMAIL_TYPE::SINGLE_ORDER_PACKING_DETAILS[:type]
              template_name = EMAIL_TYPE::SINGLE_ORDER_PACKING_DETAILS[:template]
            else
              ApplicationLogger.debug('Failed to send mail')
            end
            deliver_order_mail(mail_attributes.merge(template_name: template_name))
          end

          # 
          # function to send placed order email asynchronously
          # 
          # Paramaters::
          #   * mail_attributes [flat hash] contains data points for the email templates
          # 
          def deliver_order_mail(mail_attributes)
            MAILER_SERVICE.send_customize_email(mail_attributes).deliver
          end
        end
      end
    end
  end
end
