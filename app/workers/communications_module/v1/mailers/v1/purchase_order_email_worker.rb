module CommunicationsModule
  module V1
    module Mailers
      module V1
        class PurchaseOrderEmailWorker < EmailWorker

          # 
          # function to retrieve perform job for the worker and 
          # delegate it to corresponding instance function
          # 
          # Paramaters::
          #   * mail_attributes [flat hash] contains data points for the email templates
          #
          def perform(mail_attributes)
            email_type = CommunicationsModule::V1::Mailers::V1::EmailType
            case mail_attributes['email_type']
            when email_type::PLACED_PURCHASE_ORDER[:type]
              template_name = email_type::PLACED_PURCHASE_ORDER[:template]
            when email_type::VERIFIED_PURCHASE_ORDER[:type]
              template_name = email_type::VERIFIED_PURCHASE_ORDER[:template]
            when email_type::PURCHASE_ORDER_FILL_RATE[:type]
              template_name = email_type::PURCHASE_ORDER_FILL_RATE[:template]
            when email_type::PURCHASE_ORDER_DUMP[:type]
              template_name = email_type::PURCHASE_ORDER_DUMP[:template]
            when email_type::OUT_OF_STOCK_PRODUCTS_REPORT[:type]
              template_name = email_type::OUT_OF_STOCK_PRODUCTS_REPORT[:template]
            else
              ApplicationLogger.debug('Failed to send mail')
            end
            deliver_order_mail(mail_attributes.merge(template_name: template_name))
          end

          # 
          # function to send placed order email asynchronously
          # 
          # Paramaters::
          #   * mail_attributes [flat hash] contains data points for the email templates
          # 
          def deliver_order_mail(mail_attributes)
            mailer_service = CommunicationsModule::V1::Mailers::V1::MailerService
            mailer_service.send_customize_email(mail_attributes).deliver
          end
        end
      end
    end
  end
end
