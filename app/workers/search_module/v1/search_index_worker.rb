module SearchModule
  module V1
    class SearchIndexWorker
      include Sidekiq::Worker
      sidekiq_options :queue => :default, :retry => false
    end
  end
end
