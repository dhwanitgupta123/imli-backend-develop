#
# Module to handle functionalities related to user
#
module SearchModule
  #
  # Version1 for users module
  #
  module V1
    class UserDetailsIndexWorker < SearchIndexWorker

      def perform(user_id, options)
        return false if user_id.blank?
        user_id = user_id.to_i
        user_index_service = SearchModule::V1::UserDetailsIndexService
        if options['create'].present?
          user_index_service.add_index(user_id)
        end
        if options['update'].present?
          user_index_service.update_index(user_id)
        end
      end

    end
  end
end
