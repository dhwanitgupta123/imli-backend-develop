require 'imli_singleton'
#
# Module to handle functionalities related to User
#
module CommonModule
  module V1

    #
    # API to apply referral code benefits to user
    #
    class BlockReferralBenefitsApi < BaseModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @referral_benefit_service = CommonModule::V1::ReferralBenefitService.instance(@params)
      end

      #
      # Block user referral benefits
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        user_hash = @referral_benefit_service.block_referral_benefits(request)
        return UsersModule::V1::UserResponseDecorator.create_block_referral_benefit_response(user_hash)
      end
    end
  end
end
