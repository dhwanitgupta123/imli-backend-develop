#
# Module to handle functionalities related to Ample Credits
#
require 'imli_singleton'
module CreditsModule
  module V1

    class DebitFromAmpleCreditsApi < BaseModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @response_builder = CreditsModule::V1::DebitFromAmpleCreditsResponse
        super
      end

      #
      # Function
      #
      # @param request [Hash] Request Hash
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)
        validate(request)

        ample_credit_transaction_service = CreditsModule::V1::AmpleCreditTransactionService.instance(@params)
        result = ample_credit_transaction_service.debit_from_ample_credits(request)
        @response_builder.build(result)
      end

      #
      # Function to validate request
      #
      def validate(request)
        user_id = request[:id]
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'User id'}) unless user_id.present?
        amount = request[:amount]
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Amount'}) if amount.blank?
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::FIELD_ONLY_POSITIVE % {field: 'Amount'}) if amount.to_f <= 0
      end

    end
  end
end
