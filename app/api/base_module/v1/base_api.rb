#
# Module to handle all the BASIC functionalities related to APIs
#
module BaseModule
  module V1
    #
    # BaseApi to inject basic functionalities
    #
    class BaseApi
      extend AnnotationsModule::V1::ExceptionHandlerAnnotations

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      SENTRY_LOGGER = CommonModule::V1::Logger

      def initialize(params='')
        self.extend AnnotationsModule::V1::ExceptionHandlerAnnotations
      end
    end
  end
end
