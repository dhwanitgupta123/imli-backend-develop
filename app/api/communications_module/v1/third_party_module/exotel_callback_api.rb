#
# Module to handle functionalities related to Communications
#
require 'imli_singleton'
module CommunicationsModule
  module V1
    module ThirdPartyModule
      #
      # API for exotel callback
      #
      class ExotelCallbackApi < BaseModule::V1::BaseApi
        include ImliSingleton

        def initialize(params = '')
          @params = params
          @response_builder = CommunicationsModule::V1::ThirdPartyModule::ExotelCallbackResponse
          super
        end

        #
        # Places request to register the Exotel callback
        #
        # @return [Response] [response to be sent to the user]
        #
        _ExceptionHandler_
        def enact(request)
          exotel_communication_service_instance = CommunicationsModule::V1::ThirdPartyModule::ExotelCommunicationService.instance(@params)
          voice_communication = exotel_communication_service_instance.register_exotel_callback(request)
          @response_builder.build(voice_communication)
        end
      end
    end
  end
end
