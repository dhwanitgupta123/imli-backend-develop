#
# Module to handle functionalities related to Communications
#
require 'imli_singleton'
module CommunicationsModule
  module V1
    #
    # API to place a get communication details
    #
    class GetCommunicationDetailsApi < BaseModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = {})
        @params = params
        @response_builder = CommunicationsModule::V1::GetCommunicationDetailsResponse
        super
      end

      #
      # Validate request and requests communication details
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        validate_communication_details_request(request)
        communication_type = request[:communication_type]
        @params.merge!({communication_type: communication_type})
        communication_service_instance = CommunicationsModule::V1::CommunicationService.instance(@params)
        communication_details = communication_service_instance.get_communication_details(request)
        options_hash = {
          communication_type: communication_type
        }
        response = @response_builder.build(communication_details, options_hash)
        return response
      end

      #
      # Function to validate get communication details call request
      #
      # Raise Error::
      #   * InsufficientDataError if required parameters are missing
      def validate_communication_details_request(request)
        missing_fields = []
        if request[:sid].blank?
          missing_fields << 'SId'
        end
        if request[:communication_type].blank?
          missing_fields << 'Communication Type'
        end
        if missing_fields.present?
          missing_fields = missing_fields.join(',') + ' fields'
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: missing_fields})
        end
      end

    end
  end
end
