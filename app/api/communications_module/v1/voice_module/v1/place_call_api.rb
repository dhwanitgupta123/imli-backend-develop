#
# Module to handle functionalities related to Communications
#
require 'imli_singleton'
module CommunicationsModule
  module V1
    module VoiceModule
      module V1
        #
        # API to place a voice call
        #
        class PlaceCallApi < BaseModule::V1::BaseApi
          include ImliSingleton

          def initialize(params = '')
            @params = params
            @response_builder = CommunicationsModule::V1::VoiceModule::V1::PlaceCallResponse
            super
          end

          #
          # Validate request hash and request a new voice call
          #
          # @return [Response] [response to be sent to the user]
          #
          _ExceptionHandler_
          def enact(request)
            validate_place_call_request(request)
            voice_communication_service_instance = CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationService.instance(@params)
            voice_communication = voice_communication_service_instance.place_call(request)
            @response_builder.build(voice_communication)
          end

          #
          # Function to validate place call request
          #
          # Raise Error::
          #   * InsufficientDataError if required parameters are missing
          def validate_place_call_request(request)
            missing_fields = []
            if request[:from].blank?
              missing_fields << 'From'
            end
            if request[:to].blank?
              missing_fields << 'To'
            end
            if missing_fields.present?
              missing_fields = missing_fields.join(',') + ' fields'
              raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: missing_fields})
            else
              validate_phone_number(request[:to])
              validate_phone_number(request[:from])
            end
          end

          #
          # Validates phone_number
          #
          # Parameters::
          #   * phone_number [string]
          #
          # Raise Error::
          #   * InvalidDataError if given phone number is not valid
          def validate_phone_number(phone_number)
            unless CommonModule::V1::GeneralHelper.phone_number_valid?(phone_number)
              raise CUSTOM_ERROR_UTIL::InvalidDataError.new(CONTENT_UTIL::WRONG_PHONE_NUMBER)
            end
          end

        end
      end
    end
  end
end
