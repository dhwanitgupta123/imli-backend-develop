module ScriptsModule
  module V1
    class ComparePriceApi < BaseModule::V1::BaseApi

      SCRIPTS_SERVICE = ScriptsModule::V1::ScriptsService
      RESPONSES_DECORATOR = BaseModule::V1::BaseResponseDecorator

      def initialize(params)
        @params = params
        super
      end

      #
      # This function initiate the crawler function, which generate
      # the prices of amazon, big basket and ample
      #
      # @param request [Hash] Request contain file_path of input csv
      #
      # @return [JSONResponse] Return success response
      #
      def enact(request)
        scripts_service = SCRIPTS_SERVICE.new(@params)
        scripts_service.initiate_crawler(request[:file_path])
        return RESPONSES_DECORATOR.create_success_response
      end
    end
  end
end
