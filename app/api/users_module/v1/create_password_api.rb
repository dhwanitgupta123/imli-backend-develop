#
# Module to handle functionalities related to User
#
module UsersModule
  module V1

    #
    # API to create password for given user
    #
    class CreatePasswordApi < BaseModule::V1::BaseApi

      USER_SERVICE = UsersModule::V1::UserService
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator

      def initialize(params = '')
        @params = params
      end

      #
      # Create password for the user (passed session token), only
      # if either it is first time or reset request is raised
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_create_password_request(request)
          user_service = USER_SERVICE.new(@params)
          response = user_service.create_password(request[:user])
          return USER_RESPONSE_DECORATOR.create_password_response(response)
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError => e
          return USER_RESPONSE_DECORATOR.create_response_unauthenticated_user(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return USER_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError, CUSTOM_ERROR_UTIL::InvalidDataError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return USER_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      #
      # Function to validate create password request
      #
      def validate_create_password_request(request)
        if request[:user].blank? || request[:user][:password].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Password'})
        end
      end

    end
  end
end