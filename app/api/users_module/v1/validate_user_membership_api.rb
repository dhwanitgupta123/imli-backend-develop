#
# Module to handle functionalities related to User
#
module UsersModule
  module V1
    
    #
    # To validate the User Membership
    #
    class ValidateUserMembershipApi < BaseModule::V1::BaseApi

      def initialize(params = '')
        @params = params
      end

      #
      # Return Membership/Available plans as per the status of the Membership
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact
        membership_service = UsersModule::V1::MembershipService.new(@params)
        return membership_service.validate_membership
      end
    end
  end
end
