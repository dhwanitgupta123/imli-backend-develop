#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    class GetUserReferralDetailsApi < BaseModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @response_builder = UsersModule::V1::GetUserReferralDetailsResponse
        super
      end

      #
      # Function
      #
      # @param request [Hash] Request Hash
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)
        validate(request)
        user_data_service = UsersModule::V1::UserDataService.instance(@params)
        begin
          user = user_data_service.get_user_from_user_id(request[:id])
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError => e
          # Rescue UnAuthenticatedError and raise InvalidArgumentsError, which
          # will then be captured by general ExceptionHandler
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
        end
        @response_builder.build(user)
      end

      #
      # Function to validate search query is string or not
      #
      def validate(request)
        user_id = request[:id]
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('User id must be present') unless user_id.present?
      end

    end
  end
end
