#
# Module to handle functionalities related to User
#
module UsersModule
  module V1
    
    #
    # Update referral popup constant for the user
    #
    class UpdateReferralPopupAPI < BaseModule::V1::BaseApi

      USER_SERVICE = UsersModule::V1::UserService
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator

      def initialize(params = '')
        @params = params
      end

      #
      # Validate request hash and apply benefits
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact
        user_service = USER_SERVICE.new(@params)
        user = user_service.update_referral_popup
        return USER_RESPONSE_DECORATOR.create_active_user_status_response(user)
      end
    end
  end
end
