#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # API to delete a role
    #
    class DeleteRoleApi < BaseModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @response_builder = UsersModule::V1::DeleteRoleResponse
        super
      end

      #
      # Validate request hash and request delete role
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        validate_delete_role_request(request)
        role_service_instance = UsersModule::V1::RoleService.new(@params)
        role = role_service_instance.delete_role(request)
        @response_builder.build(role)
      end

      #
      # Function to validate delete role request
      #
      def validate_delete_role_request(request)
        if request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT%{ field: 'Role Id'})
        end
      end

    end
  end
end
