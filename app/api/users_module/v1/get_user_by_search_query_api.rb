#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    class GetUserBySearchQueryApi < BaseModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @response_builder = UsersModule::V1::GetUserBySearchQueryResponse
        super
      end

      #
      # Function takes input corresponding to create geographical_cluster and returns the
      # success or error response
      #
      # @param request [Hash] Request object, geographical_cluster params
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)
        validate(request)
        user_service = UsersModule::V1::UserService.new(@params)
        result = user_service.get_users_by_search_query(request[:query])
        @response_builder.build(result)
      end

      #
      # Function to validate search query is string or not
      #
      def validate(request)
        query = request[:query]
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Query should be a string') unless query.is_a?(String)
      end

    end
  end
end
