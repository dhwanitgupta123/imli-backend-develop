#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    class GetUserBasicDetailsApi < BaseModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @response_builder = UsersModule::V1::GetUserBasicDetailsResponse
        super
      end

      #
      # Function takes input corresponding to create geographical_cluster and returns the
      # success or error response
      #
      # @param request [Hash] Request object, geographical_cluster params
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)
        validate(request)
        user_service = UsersModule::V1::UserService.new(@params)
        begin
          user = user_service.get_user_from_user_id(request[:id])
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError => e
          # Rescue UnAuthenticatedError and raise InvalidArgumentsError, which
          # will then be captured by general ExceptionHandler
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
        end
        @response_builder.build(user)
      end

      #
      # Function to validate search query is string or not
      #
      def validate(request)
        user_id = request[:id]
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('User id must be present') unless user_id.present?
      end

    end
  end
end
