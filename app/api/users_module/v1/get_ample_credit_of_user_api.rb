#
# Module to handle functionalities related to User
#
module UsersModule
  module V1
    
    #
    # Get current balance in ample credit of user
    #
    class GetAmpleCreditOfUserApi < BaseModule::V1::BaseApi

      USER_SERVICE = UsersModule::V1::UserService
      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator
      AMPLE_CREDIT_MAPPER = CreditsModule::V1::AmpleCreditMapper

      def initialize(params = '')
        @params = params
      end

      #
      # Return current ample credits of user
      #
      # @return [JsonResponse] ample credit response of user
      #
      _ExceptionHandler_
      def enact
        user_service = USER_SERVICE.new(@params)
        ample_credit_and_benefit = user_service.get_current_ample_credit_and_benefit
        ample_credit_and_benefit = AMPLE_CREDIT_MAPPER.map_ample_credit_and_benefit_to_hash(ample_credit_and_benefit)
        return USER_RESPONSE_DECORATOR.create_ample_credit_and_benefit_response(ample_credit_and_benefit)
      end
    end
  end
end