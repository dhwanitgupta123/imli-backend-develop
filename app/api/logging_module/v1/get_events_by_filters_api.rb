require 'imli_singleton'
module LoggingModule
  module V1
    class GetEventsByFiltersApi < BaseModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
      end

      _ExceptionHandler_
      def enact(args)
        validate_request(args)
        events_response_reader = LoggingModule::V1::EventLoggerService.extract_events_by_filters(args)
        updated_events = LoggingModule::V1::EventLoggerMapper.update_events_response(events_response_reader.get_events)
        return LoggingModule::V1::EventLoggerResponseDecorator.create_events_response(updated_events)
      end

      def validate_request(args)
        if args[:id].blank? || args[:table].blank?
          CommonModule::V1::CustomErrors::InvalidArgumentsError.new('id or table name cannot be blank')
        end
      end
    end
  end
end
