module CategorizationModule
  module V1
    #
    # GetProductsByMpspCategoryApi retrieves products that belongs to sub_categories which has category as input
    # category id
    #
    class GetProductsByMpspCategoryApi < BaseModule::V1::BaseApi

      #
      # Get all active products corresponding to given mpsp_category
      #
      # @param params [JSON] [Hash containing mpsp_category id and paginate parameters]
      # Hash:
      #   - id : mpsp_category id
      #   - paginate_params: parameters for pagination
      #
      # @return [Response] [response to be sent to the user]
      _ExceptionHandler_
      def enact(params)
        if bump_version?
          mpsp_categorization_api_service = CategorizationModule::V2::MpspCategorizationApiService.new
        else
          mpsp_categorization_api_service = CategorizationModule::V1::MpspCategorizationApiService.new
        end
        return mpsp_categorization_api_service.get_products_for_mpsp_category(params)
      end

      private

      def bump_version?
        version_number = CommonModule::V1::Cache.read_int('VERSION_NUMBER_FOR_V2_PRODUCTS')
        if (USER_SESSION[:platform_type] == 'ANDROID' &&
                USER_SESSION[:version_number] >= (version_number || 18)) ||
                USER_SESSION[:platform_type].blank? ||
                USER_SESSION[:platform_type] == 'WEB'
          return true
        end
        return false
      end
    end
  end
end
