module CategorizationModule
  module V1
    #
    # GetAllMpspDepartmentsDetailsApi returns all the active departments with it's categories
    #
    class GetAllMpspDepartmentsDetailsApi < BaseModule::V1::BaseApi

      #
      # Get all active departments & category
      #
      # @param params [JSON] [paginate parameters]
      # Hash:
      #   - paginate_params: parameters for pagination
      #
      # @return [Response] [response to be sent to the user]
      _ExceptionHandler_
      def enact(params)
        if bump_version?
          mpsp_categorization_api_service = CategorizationModule::V2::MpspCategorizationApiService.new
        else
          mpsp_categorization_api_service = CategorizationModule::V1::MpspCategorizationApiService.new
        end
        return mpsp_categorization_api_service.get_all_mpsp_departments_details(params)
      end

      private

      def bump_version?
        version_number = CommonModule::V1::Cache.read_int('VERSION_NUMBER_FOR_V2_PRODUCTS')
        if (USER_SESSION[:platform_type] == 'ANDROID' &&
                USER_SESSION[:version_number] >= (version_number || 18)) ||
                USER_SESSION[:platform_type].blank? ||
                USER_SESSION[:platform_type] == 'WEB'
          return true
        end
        return false
      end
    end
  end
end
