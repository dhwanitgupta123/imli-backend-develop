module SettingsModule
  module V1
    class Feature
      attr_accessor :name
      attr_accessor :experiment
      attr_accessor :filters

      # 
      # Constructor
      # It initializes Feature instance name, experiment and filters classes
      #
      # @param name [String] [Name of the feature]
      # @param options = {} [JSON] [String in case of  old yml and Hash in case of new yml]
      #
      def initialize(name, options = {})
        @name = name.to_s
        if options.is_a?(String)
          # For backward compatability, since old yml is in format:
          # 'feature_name': 'Experiment_Name'
          self.experiment = options.to_s
        elsif options.present?
          set_parameters({experiment: options['experiment'], filters: options['filters']})
        end 
      end

      #
      # Set parameters for Feature object
      #
      # @param options [JSON] [Hash containing experiment name and filters array]
      #
      def set_parameters(options)
        self.experiment = options[:experiment]
        self.filters = load_filters(options[:filters])
      end

      #
      # Load filters as per given in the configuration
      #
      # @param filters [Array] [Array of Strings]
      #
      # @return [Array] [Array of classes]
      #
      def load_filters(filters)
        return [] if filters.blank?
        filter_classes = []
        if filters.is_a?(Array)
          filters.each do |filter|
            filter = filter.is_a?(String)? filter.constantize : filter
            filter_classes.push(filter)
          end
        end
        filter_classes
      end

      #
      # EqualTo Comparator for comparing two features
      #
      # @param obj [Object] [Feature object which is to be compared]
      #
      # @return [Boolean] [true if equal else false]
      #
      def ==(obj)
        self.name == obj.name
      end

    end # End of class
  end
end