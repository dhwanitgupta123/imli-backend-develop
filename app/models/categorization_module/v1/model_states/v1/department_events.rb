module CategorizationModule
  module V1
    # 
    # Module to keep information of model state
    # 
    module ModelStates
      module V1
        # 
        # DepartmentEvents has enums corresponding to the possible events of department model
        # 
        class DepartmentEvents
          ACTIVATE    = 1
          DEACTIVATE  = 2
          SOFT_DELETE = 3
        end
      end
    end
  end
end