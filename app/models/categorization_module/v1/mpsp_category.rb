module CategorizationModule
  module V1
    #
    # Category model class to interact with mpsp_category db table
    #
    class MpspCategory < BaseModule::BaseModel
      #
      # Defining global variables with versioning
      #
      CATEGORY_MODEL_STATES = CategorizationModule::V1::ModelStates::V1::CategoryStates
      MPSP_CATEGORY_MODEL = CategorizationModule::V1::MpspCategory
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      PAGINATION_UTIL = CommonModule::V1::Pagination
      MPSP_CATEGORIZATION_CACHE_CLIENT = CacheModule::V1::MpspCategorizationCacheClient

      #
      # Category belongs to mpsp_department
      #
      belongs_to :mpsp_department, dependent: :destroy

      #
      # Category has a self join to it's sub mpsp_category
      #
      belongs_to :mpsp_parent_category, class_name: 'MpspCategory', dependent: :destroy
      has_many :mpsp_sub_categories, class_name: 'MpspCategory', foreign_key: 'mpsp_parent_category_id', dependent: :destroy

      #
      # Sub Category has many brand packs
      #
      has_many :marketplace_selling_packs, class_name: 'MarketplaceProductModule::V1::MarketplaceSellingPack', foreign_key: 'mpsp_sub_category_id', dependent: :destroy
      #
      # Category object can't exists without it's label
      #
      validates :label, presence: true

      after_save :after_save_actions
      #
      # Workflow to define states of the Category
      #
      # Initial State => Inactive
      #
      # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, CATEGORY_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, CATEGORY_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, CATEGORY_MODEL_STATES::DELETED
      end

      #
      # Create mpsp_category with passed arguments
      #
      # Parameters::
      #   * args [Hash] Hash of all required details
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError: if unable to save to DB
      #     (mostly occurs when validation checks failed)
      #
      # @return [Category Object] if created successfully
      #
      def self.create_mpsp_category(mpsp_category_params)
        mpsp_category = MPSP_CATEGORY_MODEL.new(
          label: mpsp_category_params[:label],
          description: mpsp_category_params[:description],
          priority: mpsp_category_params[:priority]
        )
        begin
          mpsp_category.save_mpsp_category
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(e.message)
        end
        return mpsp_category
      end

      #
      # Update mpsp_department with passed arguments
      #
      # Parameters::
      #   * args [Hash] Hash of all required details
      #
      # Raise Errors::
      #   * CustomErrors::RunTimeError: if unable to save to DB
      #     (mostly occurs when validation checks failed)
      #
      # @return [Department Object] if created successfully
      #
      def update_mpsp_category(mpsp_category_params)
        self.label = mpsp_category_params[:label] if mpsp_category_params[:label].present?
        self.mpsp_department_id = mpsp_category_params[:mpsp_department_id] if mpsp_category_params[:mpsp_department_id].present?
        self.mpsp_parent_category_id = mpsp_category_params[:mpsp_category_id] if mpsp_category_params[:mpsp_category_id].present?
        self.description = mpsp_category_params[:description] if mpsp_category_params[:description].present?
        self.priority = mpsp_category_params[:priority] if mpsp_category_params[:priority].present?
        begin
          self.save_mpsp_category
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidDataError.new(e.message)
        end
      end

      #
      # to save the Category in DB
      #
      def save_mpsp_category
        self.save!
      end

      #
      # function to return the mpsp_category object if it exists
      #
      # Parameters::
      #   * mpsp_category_id [integer] id of mpsp_category to be searched
      #
      # @return [ Category object]
      #
      def self.find_by_mpsp_category_id(mpsp_category_id)
        begin
          return MPSP_CATEGORY_MODEL.find(mpsp_category_id)
        rescue ActiveRecord::RecordNotFound => e
          return nil
        end
      end

      #
      # function to return array of all the mpsp_categories paginted
      #
      # @return [array of mpsp_categories object]
      def self.paginated_mpsp_categories(mpsp_paginate_param)
        if mpsp_paginate_param[:state].present?
          return get_mpsp_categories_by_status(mpsp_paginate_param)
        else
          return get_all_mpsp_categories(mpsp_paginate_param)
        end
      end

      #
      # function to return paginated mpsp_categories based on the status
      #
      def self.get_mpsp_categories_by_status(mpsp_paginate_param)
        set_pagination_properties(mpsp_paginate_param)
        mpsp_categories = MPSP_CATEGORY_MODEL.where({ status: @state, mpsp_parent_category: nil}).order(@sort_order)
        return get_paginated_mpsp_categories(mpsp_categories, mpsp_paginate_param)
      end

      #
      # function to return paginated all mpsp_categories
      #
      def self.get_all_mpsp_categories(mpsp_paginate_param)
        set_pagination_properties(mpsp_paginate_param)
        mpsp_categories = MPSP_CATEGORY_MODEL.where.not({ status: CATEGORY_MODEL_STATES::DELETED, mpsp_department: nil }).order(@sort_order)
        return get_paginated_mpsp_categories(mpsp_categories, mpsp_paginate_param)
      end

      #
      # function to return array of all the sub mpsp_categories paginted
      #
      # @return [array of sub mpsp_categories object]
      def self.paginated_mpsp_sub_categories(mpsp_paginate_param)
        if mpsp_paginate_param[:state].present?
          return get_mpsp_sub_categories_by_status(mpsp_paginate_param)
        else
          return get_all_mpsp_sub_categories(mpsp_paginate_param)
        end
      end

      #
      # function to return paginated mpsp_categories based on the status
      #
      def self.get_mpsp_sub_categories_by_status(mpsp_paginate_param)
        set_pagination_properties(mpsp_paginate_param)
        mpsp_sub_categories = MPSP_CATEGORY_MODEL.where({ status: @state, mpsp_department: nil}).order(@sort_order)
        return get_paginated_mpsp_sub_categories(mpsp_sub_categories, mpsp_paginate_param)
      end

      #
      # function to return paginated all mpsp_categories
      #
      def self.get_all_mpsp_sub_categories(mpsp_paginate_param)
        set_pagination_properties(mpsp_paginate_param)
        mpsp_sub_categories = MPSP_CATEGORY_MODEL.where.not({ status: CATEGORY_MODEL_STATES::DELETED, mpsp_parent_category: nil }).order(@sort_order)
        return get_paginated_mpsp_sub_categories(mpsp_sub_categories, mpsp_paginate_param)
      end

      #
      # function to set pagination properties
      #
      def self.set_pagination_properties(mpsp_paginate_param)
        @per_page = (mpsp_paginate_param[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (mpsp_paginate_param[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = mpsp_paginate_param[:sort_by] || PAGINATION_UTIL::CATEGORIZATION_SORT_BY
        order = mpsp_paginate_param[:order] || PAGINATION_UTIL::ORDER
        @sort_order = sort_by + ' ' + order
        @state = mpsp_paginate_param[:state]
      end

      #
      # function to paginate the mpsp_categories
      #
      def self.get_paginated_mpsp_categories(mpsp_categories, filter_param)
        if filter_param[:mpsp_department_id].present?
          mpsp_categories = mpsp_categories.where(mpsp_department_id: filter_param[:mpsp_department_id])
        end
        page_count = (mpsp_categories.count / @per_page).ceil
        paginated_mpsp_categories = mpsp_categories.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { mpsp_categories: paginated_mpsp_categories, page_count: page_count }
      end

      #
      # function to paginate the mpsp_categories
      #
      def self.get_paginated_mpsp_sub_categories(mpsp_sub_categories, filter_param)
        if filter_param[:mpsp_category_id].present?
          mpsp_sub_categories = mpsp_sub_categories.where(mpsp_parent_category_id: filter_param[:mpsp_category_id])
        elsif filter_param[:mpsp_department_id].present?
          mpsp_categories = get_mpsp_category_by_mpsp_department_id(filter_param[:mpsp_department_id])
          mpsp_sub_categories = mpsp_sub_categories.where(mpsp_parent_category: mpsp_categories)
        end
        page_count = (mpsp_sub_categories.count / @per_page).ceil
        paginated_mpsp_sub_categories = mpsp_sub_categories.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { mpsp_sub_categories: paginated_mpsp_sub_categories, page_count: page_count }
      end

      #
      # function to return array of all the active mpsp_categories
      #
      # @return [array of mpsp_categories object]
      def self.get_active_mpsp_categories_by_mpsp_department(mpsp_department)
        set_pagination_properties({})
        mpsp_categories = mpsp_department.mpsp_categories.
            where({ status: CATEGORY_MODEL_STATES::ACTIVE }).order(@sort_order)
        return mpsp_categories
      end

      #
      # Function to change state of mpsp_category based on the event
      #
      # Raise Error::
      #   * CustomError::PreConditionRequiredError if trsnsition is not defined for the current status
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
          update_mpsp_categorization_version
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(e.message)
        end
      end

      #
      # Get current state of Category
      #
      # @return [String] Current state
      def get_current_state
        self.current_state.name
      end

      #
      # Return mpsp_category with label = label and mpsp_department_id = mpsp_department_id
      #
      def self.get_mpsp_category_by_label_and_mpsp_department(label, mpsp_department_id)
        MPSP_CATEGORY_MODEL.find_by(label: label, mpsp_department_id: mpsp_department_id)
      end

      #
      # Return sub mpsp_category by label and mpsp_category id
      #
      def self.get_sub_mpsp_category_by_label_and_mpsp_category_id(label, mpsp_category_id)
        MPSP_CATEGORY_MODEL.find_by(label: label, mpsp_parent_category_id: mpsp_category_id)
      end

      #
      # This function called after each update
      # and perform actions
      #
      def after_save_actions
        update_mpsp_categorization_version
        update_mpsp_index
      end

      #
      # This function called after each update
      # and update the current version of mpsp categorization tree
      #
      def update_mpsp_categorization_version
        MPSP_CATEGORIZATION_CACHE_CLIENT.update_current_version
      end

      #
      # This function checks if category of sub_category changes or department
      # of category changes and accordingly update all the mpsps within the node
      #
      def update_mpsp_index
        return unless self.changed?

        mpsp_index_service = SearchModule::V1::MarketplaceSellingPackIndexService.new

        if self.changes[:mpsp_parent_category_id].present?
          marketplace_selling_packs = get_mpsps_within_sub_category
        elsif self.changes[:mpsp_department_id].present?
          marketplace_selling_packs = get_mpsps_within_category
        end

        mpsp_index_service.update_nodes(marketplace_selling_packs)
      end

      #
      # This function returns mpsps within the sub_category
      #
      def get_mpsps_within_sub_category
        return self.marketplace_selling_packs
      end

      #
      # This function returns mpsps within the category
      #
      def get_mpsps_within_category

        marketplace_selling_pack_dao = MarketplaceProductModule::V1::MarketplaceSellingPackDao.new

        return marketplace_selling_pack_dao.get_mpsps_by_mpsp_sub_categories(self.mpsp_sub_category_ids)
      end

      #
      # Return msps_category with mpsp_department_id = mpsp_department_id
      #
      def self.get_mpsp_category_by_mpsp_department_id(mpsp_department_id)
        MPSP_CATEGORY_MODEL.where(mpsp_department_id: mpsp_department_id)
      end
    end
  end
end
