module SearchModule
  #
  # Version1 for Search module
  #
  module V1
    #
    # Custom repository class for user-details-index
    # will be used for searching a User based on his name/phone_number/email
    #
    class UserDetailsRepository
      include Elasticsearch::Persistence::Repository

      def initialize(options = {})
        # throws Faraday::ConnectionFailed on timeout
        transport_options = options[:transport_options] || { request: { timeout: 5 } }
        index 'user-details-index' # index name, used by elasticsearch to hash index uniquely
        client Elasticsearch::Client.new url: options[:url], log: options[:log], transport_options: transport_options
      end

      klass SearchModule::V1::UserDetailsSearchModel # Json result transform to klass

      settings number_of_shards: 1 do
        mapping do
          indexes :emails, analyzer: 'whitespace'
        end
      end

      #
      # serializing document klass(user) to hash
      #
      # @param document [klass] klass object
      #
      # @return [hash] transformed hash
      #
      def serialize(document)
        hash = document.to_hash.clone
        hash.to_hash
      end

      #
      # deserialize hash to klass type
      #
      # @param document [hash]
      #
      # @return [klass] Object of type klass with attributes = hash
      #
      def deserialize(document)
        hash = document['_source']
        klass.new hash
      end
    end
  end
end
