module UsersModule::V1
  #
  # Membership module to handle membership related functionalities
  #
  module MembershipModule::V1
    #
    # Class to interact with membership_payments db table
    #
    class MembershipPayment < BaseModule::BaseModel
      CONTENT_UTIL = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      PAYMENT_STATES = UsersModule::V1::ModelStates::MembershipPaymentStates
      belongs_to :membership, class_name: 'UsersModule::V1::Membership'

      #
      # Workflow to define states of the Payment
      #
      # Initial State => PENDING
      #
      # State Diagram::
      #   PENDING --payment_fail--> FAILED
      #   PENDING --payment_success--> SUCCESS
      #
      # * payment_fail, payment_success are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :pending, PAYMENT_STATES::PENDING do
          event :payment_fail, transitions_to: :failed
          event :payment_success, transitions_to: :success
        end
        state :failed, PAYMENT_STATES::FAILED
        state :success, PAYMENT_STATES::SUCCESS
      end

      #
      # Function returns the new object of membership_payment
      #
      def new_payment(payment_args)
        return MembershipPayment.new(payment_args)
      end

      #
      # Function returns payment based on it's ID
      #
      def get_payment_by_id(payment_id)
        begin
          return MembershipPayment.find(payment_id)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_PAYMENT_ID)
        end
      end
    end
  end
end
