#
# Module to handle all the functionalities related to user module
#
module UsersModule
  #
  # Version1 for users module
  #
  module V1
    module ModelStates
      #
      # Class for Payment states
      # 
      class MembershipPaymentStates

        PENDING     = 1
        FAILED      = 2
        SUCCESS     = 3

      end
    end
  end
end
