module UsersModule
  module V1
    #
    # Module to keep information of model state
    #
    module ModelStates
      #
      # RoleEvents has enums corresponding to the possible events of Role model
      #
      class RoleEvents
        ACTIVATE     = 1
        SOFT_DELETE  = 2
      end
    end
  end
end
