module UsersModule
  module V1
    module ModelStates
      #
      # RoleStates has enums corresponding to the possible
      # states of Role model
      #
      class RoleStates
        ACTIVE      = 1
        DELETED     = 2
      end
    end
  end
end
