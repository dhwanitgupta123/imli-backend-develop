module UsersModule
  module V1
    module ModelStates
      #
      # Membership model can have following states.
      # The corresponding integer value is being stored in DB
      # column 'workflow_state'
      #
      class MembershipStates
        ACTIVE          = 1
        PENDING         = 2
        PAYMENT_PENDING = 3
        DELETED         = 4
        EXPIRED         = 5
        INACTIVE        = 6
        CANCELLED       = 7
        PENDING_EXPIRY  = 8
        ON_HOLD         = 9


        ALLOWED_STATES = [ACTIVE, PAYMENT_PENDING, PENDING, PENDING_EXPIRY, ON_HOLD]


        def self.get_membership_status_hash_by_id(state)
          return {} if state.blank?
          label = ''
          case state.to_i
          when ACTIVE
            label = 'Active'
          when PENDING
            label = 'Pending'
          when PAYMENT_PENDING
            label = 'Payment Pending'
          when DELETED
            label = 'Deleted'
          when EXPIRED
            label = 'Expired'
          when INACTIVE
            label = 'Inactive'
          when CANCELLED
            label = 'Cancelled'
          when ON_HOLD
            label = 'On Hold'
          when PENDING_EXPIRY
            label = 'Pending Expiry'
          end
          return {id: state, label: label}
        end

      end
    end
  end
end
