module UsersModule
  module V1
    module ModelStates
      #
      # UserStates has enums corresponding to the possible
      # states of User model
      #
      class UserStates
        ACTIVE      = 1
        PENDING     = 2
        INACTIVE    = 3
        DELETED     = 4
        BLACKLISTED = 5



        def self.get_status_hash_by_id(id)
          return {} if id.blank?

          case id.to_i
          when ACTIVE
            status_hash = {value: ACTIVE, label: 'Active'}
          when PENDING
            status_hash = {value: PENDING, label: 'Pending'}
          when INACTIVE
            status_hash = {value: INACTIVE, label: 'Inactive'}
          when DELETED
            status_hash = {value: DELETED, label: 'Deleted'}
          when BLACKLISTED
            status_hash = {value: BLACKLISTED, label: 'Blacklisted'} 
          else
            status_hash = {}
          end
          status_hash
        end

      end

    end
  end
end