#
# Module to handle all the functionalities related to user module
#
module UsersModule
  #
  # Version1 for users module
  #
  module V1
    module ModelStates
      #
      # Class for Payment Events
      #
      class MembershipPaymentEvents

        INITIATE           = 1
        PAYMENT_FAIL       = 2
        PAYMENT_SUCCESS    = 3

      end
    end
  end
end
