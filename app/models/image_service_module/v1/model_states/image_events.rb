#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    module ModelStates
     # 
    # Module to keep information of model state
    # 
      class ImageEvents
        ACTIVATE    = 1
        DEACTIVATE  = 2
        SOFT_DELETE = 3
      end
    end
  end
end
