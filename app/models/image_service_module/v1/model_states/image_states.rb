#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    module ModelStates
      #
      # ImageStates has enums corresponding to the possible
      # states of Image model
      #
      class ImageStates
        ACTIVE      = 1
        INACTIVE    = 2
        DELETED     = 3
      end

    end
  end
end
