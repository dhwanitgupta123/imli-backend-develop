module BaseModule
  class BaseModel < ActiveRecord::Base

    self.abstract_class = true

    after_commit :log_create_event, on: [:create]
    after_commit :log_update_event, on: [:update]
    after_commit :log_destroy_event, on: [:destroy]

    after_save :accumulate_changes

    def accumulate_changes
      @changes_in_model = [] if @changes_in_model.blank?
      @changes_in_model.push(self.changes)
    end

    def final_accumulated_changes
      @changes_in_model || []
    end

    def is_key_changed?(key)
      return false if key.blank?
      final_accumulated_changes.each do |change_hash|
          return true if change_hash[key.to_sym].present?
        end
      return false
    end

    def accumulate_changes
      @changes_in_model = [] if @changes_in_model.blank?
      @changes_in_model.push(self.changes)
    end

    def final_accumulated_changes
      @changes_in_model || []
    end

    def is_key_changed?(key)
      return false if key.blank?
      final_accumulated_changes.each do |change_hash|
        return true if change_hash[key.to_sym].present?
      end
      return false
    end

    def log_create_event
      log_event('Create')
    end

    def log_update_event
      log_event('Update')
    end

    def log_destroy_event
      log_event('Destroy')
    end

    def log_event(action)
      begin
        return if Rails.env != 'production' || @changes_in_model.blank?

        args = {}
        args[:time] = CommonModule::V1::DateTimeUtil.get_current_time('utc').strftime("%Y-%m-%dT%H:%M:%SZ")
        args[:user] = USER_SESSION[:user_id] if USER_SESSION.present?
        args[:record] = self.attributes
        args[:table] = self.class.table_name
        args[:action] = action
        args[:changes] = @changes_in_model
        args[:database] = Rails.env + '-' + Rails.configuration.database_configuration[Rails.env]['database']
        LoggingModule::V1::EventLoggerWorker.perform_async(args)
      rescue => e
        ApplicationLogger.error('BASEMODEL : Error while logging after commit event ' + e.message)
      end
    end
  end
end
