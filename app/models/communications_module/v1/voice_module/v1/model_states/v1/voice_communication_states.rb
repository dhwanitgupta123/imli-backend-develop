module CommunicationsModule
  module V1
    module VoiceModule
      module V1
        #
        # Module to keep information of model state
        #
        module ModelStates
          module V1
            #
            # VoiceCommunicationStates has enums corresponding to the possible states of Voice Communication model
            #
            module VoiceCommunicationStates
              extend self

              MAP = [
                {
                  value: 1,
                  key: 'INITIATED',
                  label: 'INITIATED'
                },
                {
                  value: 2,
                  key: 'QUEUED',
                  label: 'QUEUED'
                },
                {
                  value: 3,
                  key: 'IN_PROGRESS',
                  label: 'IN PROGRESS'
                },
                {
                  value: 4,
                  key: 'COMPLETED',
                  label: 'COMPLETED'
                },
                {
                  value: 5,
                  key: 'FAILED',
                  label: 'FAILED'
                },
                {
                  value: 6,
                  key: 'BUSY',
                  label: 'BUSY'
                },
                {
                  value: 7,
                  key: 'NO_ANSWER',
                  label: 'NO ANSWER'
                },
                {
                  value: 8,
                  key: 'DND',
                  label: 'DND'
                }
              ]

              # Disconnected States of Voice Communication
              DISCONNECTED_STATES_VALUES = [4, 5, 6, 7, 8]


              def get_value_by_key(key)
                references = MAP.select {|hash| hash[:key] == key.to_s}
                return references[0][:value] if references.present?
                return nil
              end

              def get_key_by_value(value)
                references = MAP.select {|hash| hash[:value] == value.to_i}
                return references[0][:key] if references.present?
                return nil
              end

              #
              # Get voice communication state hash corresponding to passed value
              #
              # @param state_value [Integer] [Value of the state]
              #
              # @return [JSON] [Hash of voice communication state]
              #
              def get_detail_hash_by_value(value)
                references = MAP.select {|hash| hash[:value] == value.to_i}
                return references[0] if references.present?
                # Return empty hash if no match in is MAP encountered
                return {}
              end

              def get_label_by_value(value)
                references = MAP.select {|hash| hash[:value] == value.to_i}
                hash_entry = references[0] if references.present?
                return {} if hash_entry.blank?
                label = hash_entry[:label] || ''
                return {label: label}
              end

              def get_detail_hash_by_key(key)
                references = MAP.select {|hash| hash[:key] == key.to_s}
                return references[0] if references.present?
                return nil
              end

              #
              # Returns array of disconnected state values
              #
              def get_disconnected_states_values
                state_values_array = []
                DISCONNECTED_STATES_VALUES.each do |disconnected_state|
                  state_values_array.push(value = disconnected_state)
                end

                return state_values_array
              end

              def get_all_states_array
                state_values_array = []
                MAP.each do |state|
                  state_values_array.push(value = state[:value])
                end

                return state_values_array
              end
            end
          end
        end
      end
    end
  end
end
