module CommunicationsModule
  module V1
    module VoiceModule
      module V1
        #
        # Module to keep information of model state
        #
        module ModelStates
          module V1
            #
            # VoiceCommunicationDirectionTypes has enums corresponding to the possible call directions of Voice Communication model
            #
            module VoiceCommunicationDirectionTypes
              extend self

              MAP = [
                {
                  value: 1,
                  key: 'OUTBOUND',
                  label: 'OUTBOUND'
                },
                {
                  value: 2,
                  key: 'INBOUND',
                  label: 'INBOUND'
                }
              ]

              #
              # Get voice communication direction type corresponding to passed value
              #
              # @param state_value [Integer] [Value of the direction type]
              #
              # @return [JSON] [Hash of voice communication direction type]
              #
              def get_voice_communication_direction_type_by_value(state_value)
                references = MAP.select {|hash| hash[:value] == state_value.to_i}
                return references[0] if references.present?
                # Return empty hash if end of the MAP encountered
                return {}
              end

              def get_value_by_key(key)
                references = MAP.select {|hash| hash[:key] == key.to_s}
                return references[0][:value] if references.present?
                return nil
              end
            end
          end
        end
      end
    end
  end
end
