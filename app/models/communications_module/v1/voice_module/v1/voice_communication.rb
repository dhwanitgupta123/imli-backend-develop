module CommunicationsModule
  module V1
    module VoiceModule
      module V1
        class VoiceCommunication < BaseModule::BaseModel
          validates :from_phone_number, :to_phone_number, presence: true
          validates :sid, uniqueness: true

          def missing_information?
            self.attributes.values.include? nil
          end
        end
      end
    end
  end
end
