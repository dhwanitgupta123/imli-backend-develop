module CommunicationsModule
  module V1
    #
    # Module to keep information of model state
    #
    module ModelStates
      module V1
        #
        # CommunicationModelTypes has enums corresponding to the possible Communication Types
        #
        module CommunicationModelTypes
          extend self

          MAP = [
            {
              value: 1,
              key: 'SMS',
              label: 'SMS',
              model: ''
            },
            {
              value: 2,
              key: 'NOTIFICATION',
              label: 'NOTIFICATION',
              model: ''
            },
            {
              value: 3,
              key: 'EMAIL',
              label: 'EMAIL',
              model: ''
            },
            {
              value: 4,
              key: 'VOICE',
              label: 'VOICE',
              model: 'CommunicationsModule::V1::VoiceModule::V1::VoiceCommunication'
            }
          ]

          #
          # Get communication type corresponding to passed value
          #
          # @param state_value [Integer] [Value of the communication type]
          #
          # @return [JSON] [Hash of communication type]
          #
          def get_communication_type_by_value(state_value)
            references = MAP.select {|hash| hash[:value] == state_value.to_s}
            return references[0] if references.present?
            # Return empty hash if no match in is MAP encountered
            return {}
          end

          def get_all_states_array
            state_values_array = []
            MAP.each do |state|
              state_values_array.push(value = state[:value])
            end

            return state_values_array
          end

          def get_value_by_key(key)
            references = MAP.select {|hash| hash[:key] == key.to_s}
            return references[0][:value] if references.present?
            return nil
          end

        end
      end
    end
  end
end
