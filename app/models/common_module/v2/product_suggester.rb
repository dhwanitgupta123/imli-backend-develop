module CommonModule
  #
  # Module Version V2
  #
  module V2
  	#
    # Mongoid document to save ProductSuggestions by User
    #
		class ProductSuggester

			#Making it mongoid document
		  include Mongoid::Document
		  #Saving timeStamp with each log
      include Mongoid::Timestamps

      field :user_id, type: Integer
      field :product_suggestion, type: Hash

		  validates_presence_of :user_id, :product_suggestion

		end
	end
end
