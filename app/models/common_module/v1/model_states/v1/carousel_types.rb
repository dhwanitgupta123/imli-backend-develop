module CommonModule
  module V1
    # 
    # Module to keep information of model state
    # 
    module ModelStates
      module V1
        # 
        # CarouselTypes has enums corresponding to the possible types of Carousel model
        # 
        class CarouselTypes

          # Hard coded Carousel types. This enum needs to be updated
          # if more Carousel types need to be added

          DESKTOP_HOME = {
            type: 1,
            label: "DESKTOP_HOME",
            description: "This carousel will be displayed on Desktop Home"
          }

          MOBILE_HOME  = {
            type: 2,
            label: "MOBILE_HOME",
            description: "This carousel will be displayed on Mobile Home"
          }

        end
      end
    end
  end
end