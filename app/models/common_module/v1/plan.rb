module CommonModule
  module V1
    class Plan < BaseModule::BaseModel
      #
      # Defining global variables with versioning
      #
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      PLAN_MODEL_STATES = CommonModule::V1::ModelStates::V1::PlanStates
      PLAN_MODEL_EVENTS = CommonModule::V1::ModelStates::V1::PlanEvents
      PLAN_MODEL = CommonModule::V1::Plan
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper

      # A plan can have many benefits associated with it
      has_and_belongs_to_many :benefits

      # A plan can have many memberships associated with it
      has_many :memberships, class_name: "UsersModule::V1::Membership"

      # onCreate/onUpdate hooks
      validates :plan_key, presence: true, on: :create
      validates :plan_key, presence: true, on: :update, if: :validate_plan_key_param
      validates :images, presence: true, on: :create, if: :validate_images_param
      validates :images, presence: true, on: :update, if: :validate_images_param

      #
      # Workflow to define states of the PLAN
      #
      # Initial State => Inactive
      #
      # # State Diagram::
      #   Inactive --activate--> Active
      #   Active  --deactivate--> Inactive
      #   Deleted
      #
      # * activate, deactivate are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :inactive, PLAN_MODEL_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end
        state :active, PLAN_MODEL_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, PLAN_MODEL_STATES::DELETED

        before_transition do |from, to, triggering_event, *event_args|
          if to == :active && no_active_plan_with_requested_plan_key? == false
            raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(@message)
          end
        end
      end

      #
      # function to create new plan object
      # This is just to create, object.save! needs to be called explicitly.
      #
      # @param args [Hash] [Hash containing plan attributes]
      #
      # @return [Plan] [Plan object created]
      def self.new_plan(args)
        if args[:images].present?
          args[:images] = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(args[:images])
        end
        plan = PLAN_MODEL.new(args)
        return plan
      end

      #
      # Update the plan in DB
      #
      # @param args [JSON] [Hash containing plan attributes]
      #
      # @return [type] [description]
      # @raise [InvalidArgumentsError] [if validation fails while updating]
      def update_plan(args)
        begin
          if args[:images].present?
            args[:images] = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(args[:images])
          end
          self.update_attributes!(args)
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # to save the plan in DB
      #
      def save_plan
        begin
          self.save!
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # Find Plan by plan id
      #
      # @param plan_id [Integer] [Plan id for which plan to find]
      #
      # @return [Plan] [Plan object associated with that id]
      #
      # @raise [ResourceNotFoundError] [if no plan found]
      def self.find_by_plan_id(plan_id)
        begin
          PLAN_MODEL.find(plan_id.to_i)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERRORS_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::PLAN_NOT_FOUND)
        end
      end

      #
      # Find Plan by plan uniqued id
      #
      # @param plan_name [string] [Plan name for which plan to find]
      #
      # @return [Plan] [Plan object associated with that name]
      #
      # @raise [ResourceNotFoundError] [if no plan found]
      def self.find_by_plan_key(plan_key)
        plan = PLAN_MODEL.where(plan_key: plan_key)
        if plan.blank?
          raise CUSTOM_ERRORS_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::PLAN_NOT_FOUND)
        end
        return plan.first
      end

      #
      # Returns all the plans
      #
      # @return [Array] array of plan model
      #
      def self.get_all_plans
        PLAN_MODEL.all
      end

      #
      # Returns all the active plans
      #
      # @return [Array] array of plan model
      #
      def self.get_all_active_plans
        PLAN_MODEL.where({ status: PLAN_MODEL_STATES::ACTIVE })
      end

      #
      # Get all plans which are not in DELETED state
      #
      def self.get_all_non_deleted_plans
        PLAN_MODEL.where.not({ status: PLAN_MODEL_STATES::DELETED })
      end

      #
      # Get all Plans by status
      # @param status [Integer] [Status of the plan]
      #
      # @return [Array] [Array of Plan model]
      #
      def self.get_plans_by_status(status)
        PLAN_MODEL.where({status: status})
      end

      #
      # Function to change state of plan based on the event
      #
      # Raise Error::
      #   * CustomError::PreConditionRequiredError if transition is not defined for the current status
      #
      def trigger_event(event)
        begin
          case event.to_i
            when PLAN_MODEL_EVENTS::ACTIVATE
              self.activate!
            when PLAN_MODEL_EVENTS::DEACTIVATE
              self.deactivate!
            when PLAN_MODEL_EVENTS::SOFT_DELETE
              self.soft_delete!
            else
              raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::EVENT_NOT_DEFINED % {event: "#{event.to_s}"})
          end
        rescue Workflow::NoTransitionAllowed => e
          raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(e.message)
        end
      end

      #
      # Get current state of Plan
      #
      # @return [String] Current state
      def get_current_state
        self.current_state.name
      end

      #
      # Checks if no active plan is present with requested plan_key or with current plan's plan_key
      #
      # @error [PreConditionRequiredError] if requested plan update is active and active plan already exists with the current plan's requested plan type
      #
      def no_active_plan_with_requested_plan_key?(plan_key = self.plan_key)
        active_plans_with_plan_key = PLAN_MODEL.where({ status: PLAN_MODEL_STATES::ACTIVE, plan_key: plan_key })
        if active_plans_with_plan_key.blank?
          return true;
        else
          @message = 'An active Plan with this Plan Type already exists'
          return false;
        end
      end

      def is_plan_active?
        is_plan_active = (get_current_status == PLAN_MODEL_STATES::ACTIVE) ? true : false
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # Validates Image params and executes on iff diff is detected
      #
      def validate_images_param
        changes_in_images_param = get_changes_in_field('images')
        if changes_in_images_param.present?
          existing_value = changes_in_images_param[0]
          change_requested = changes_in_images_param[1]
          self.class.validate_images(change_requested)
        end
      end

      #
      # Validates plan_key params and executes on iff diff is detected
      #
      # @error [PreConditionRequiredError] if requested plan update is active and active plan already exists with the current plan's requested plan type
      #
      def validate_plan_key_param
        changes_in_plan_key = get_changes_in_field('plan_key')
        if changes_in_plan_key.present?
          existing_value = changes_in_plan_key[0]
          change_requested = changes_in_plan_key[1]
          if is_plan_active?
            unless no_active_plan_with_requested_plan_key?(change_requested)
              raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(@message)
            end
          end
          self.plan_key = change_requested
        end
      end

      def get_changes_in_field(field='')
        changes_in_field = ''
        unless field.blank?
          changed_fields = get_changed_fields_in_model
          unless changed_fields.blank?
            if changed_fields.include?(field.to_s)
              changes_in_field = self.changes[field.to_s]
            end
          end
        end
        return changes_in_field
      end

      def get_changed_fields_in_model
        self.changed
      end

      #
      # validate if all the image_id present in image_id_array is present or not
      #
      # @param image_id_array [Array] array containing all the image_ids
      #
      # @error [InvalidArgumentsError] if image_id not present in image table
      #
      def self.validate_images(image_id_array)
        if IMAGE_SERVICE_HELPER.validate_images(image_id_array) == false
          raise CUSTOM_ERRORS_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::IMAGE_LINKING_FAILED)
        end
      end

      #
      # Get current status of Plan
      #
      # @return [Integer] Current status
      def get_current_status
        self.status
      end

    end
  end
end
