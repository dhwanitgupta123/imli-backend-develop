module CommonModule
  module V1
  	#
    # Mongoid document to save Carousel Model
    #
		class Carousel
       
      #Making it mongoid document
      include Mongoid::Document
      #Saving timeStamp with each log
      include Mongoid::Timestamps

			CAROUSEL_MODEL = CommonModule::V1::Carousel
      CUSTOM_ERRORS = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper

      field :type, type: Integer
		  field :images, type: Array

      # 
      # update_the_carousel function
      # 
      # * checks it the carousel exists for the type, if not, creates it
      # * inserts images as per priority in the Carousel Model object
      # 
      # @param args [Hash] contains: valid type [String], valid images[Array]
      # 
      # @return [Carousel Model]
      # 
		  def self.update_the_carousel(args)
		  	initialize_carousel_params(args)
        
		  	existing_carousel = carousel_exists?(args[:type])
		  	
		  	if existing_carousel.blank?
		  		existing_carousel = create_new_carousel
		  	end
		  	
        existing_carousel.images.clear
        existing_carousel.images = @images
        existing_carousel.save!

        # binding images with the Model name
        image_ids_array = IMAGE_SERVICE_HELPER.get_sorted_image_id_array_by_priority(existing_carousel.images)
        bind_images_with_resource(image_ids_array, self.name)

        return existing_carousel
		  end

      # 
      # get_the_carousel function
      # 
      # * checks it the carousel exists for the type, if yes, returns it
      # 
      # @param args [Hash] contains: valid type [String]
      # 
      # @return [Carousel Model]
      # 
      def self.get_the_carousel(args)
        initialize_carousel_params(args)

        existing_carousel = carousel_exists?(args[:type])
        if existing_carousel.blank?
          raise CUSTOM_ERRORS::ResourceNotFoundError.new(CONTENT::REQUESTED_FIELD_NOT_FOUND%{ field: 'Carousel' })
        end

        return existing_carousel
      end

      def self.get_all_carousels
        CAROUSEL_MODEL.all.entries
      end

      # 
      # carousel_exists? function to check if Carousel exists
      # 
      # @return [CarouselModel]
      # 
      def self.carousel_exists?(carousel_type)
        CAROUSEL_MODEL.where(type: carousel_type).entries.first
      end
		  
		  ###############################
      #       Private Functions     #
      ###############################
      
		  private

		  # 
		  # creates new CarouselModel object in Mongoid document
		  # 
		  # @return [Carousel Model] new Carousel Model
		  def self.create_new_carousel
		  	new_carousel = CAROUSEL_MODEL.new
		  	new_carousel.images = []
        new_carousel.type = @type
		  	new_carousel.save!

		  	return new_carousel
		  end

		  # 
		  # initialize params for Carousel
		  # @param args [Hash] 
		  # * contains type [String] type of Carousel
		  # * contains images [Array] images
		  # 
		  def self.initialize_carousel_params(args)
		  	@type = args[:type] if args[:type].present?
        if args[:images].present?
          @images = []
          sorted_images_array = args[:images].sort_by {|image| image[:priority].to_i}
          sorted_images_array.each do |image_hash|
            refined_image_hash = {
              image_id: image_hash[:image_id],
              action: image_hash[:action]
            }
            @images << refined_image_hash
          end
        end
		  end

      # 
      # Bind images with resource
      #
      # @param image_ids [Array] Array of image_ids
      # @param resource_name [String] Mode name
      #
      def self.bind_images_with_resource(image_ids, resource_name)
        IMAGE_SERVICE_HELPER.bind_image_with_resource(image_ids, resource_name)
      end
      
		end
	end
end