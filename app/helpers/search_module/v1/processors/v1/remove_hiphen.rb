#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Processors which need to apply prior fetching for UserDetails
    #
    module Processors
      module V1
        module RemoveHiphen
          extend self

          #
          # Remove hiphen from query string
          #
          def apply(query)
            CommonModule::V1::StringUtil.remove_hiphens(query)
          end

        end
      end
    end
  end
end