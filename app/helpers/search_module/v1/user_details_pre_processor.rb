#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Processors which need to apply prior fetching for UserDetails
    #
    module UserDetailsPreProcessor
      extend self

      def fetch_processors
        [
          SearchModule::V1::Processors::V1::RemoveHiphen,
          SearchModule::V1::Processors::V1::RemoveDots,
          SearchModule::V1::Processors::V1::Downcase
        ]
      end

      #
      # Process the query before calling Elastic search
      #
      # @param query [String] [Incoming query]
      #
      # @return [String] [Processed query]
      #
      def process(query)
        fetch_processors.each do |processor|
          query = processor.apply(query)
        end
        query
      end

    end #End of module
  end
end
