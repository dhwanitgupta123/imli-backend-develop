#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Processors which need to apply prior fetching for UserDetails
    #
    module UserDetailsPostProcessor
      extend self

      def fetch_processors
        [ ]
      end

      #
      # Process the Results after calling Elastic search
      #
      # @param results [Array] [Results coming from elastic search]
      #
      # @return [Array] [processed array of results]
      #
      def process(results)
        fetch_processors.each do |processor|
          results = processor.apply(results)
        end
        results
      end

    end #End of module
  end
end
