#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Logic layer which is responsible to create marketplace_selling_pack index
    # and retrieve results
    #
    module UserDetailsQueryHelper
      extend self
      require 'imli/elastic_query_builder'

      # This function breaks the query by white space convert it into tokens
      # and corresponding to each token it creates
      # 'fuzzy query'
      # 'regexp query'
      # 'term query'
      # and concat them in boolean query through OR ('should') operator
      #
      # Example : query = 'tata tea'
      # 1. tokens = ['tata', 'tea']
      # 2. let regexp_query for token1 in text field  = regexp1 and for token2 = regexp2
      # 3. let fuzzy_query for token1 in text field = fuzzy1 and for token2 = fuzzy2
      # 4. let fuzzy_query for token1 in context field = c_fuzzy1 and for token2 = c_fuzzy2
      # 5. let match_query = match_query
      # 6. then boolean_query = {"should": [regexp1, regexp2, fuzzy1, fuzzy2, c_fuzzy1, c_fuzzy2], "must" :[match_query]}
      # where
      #   => regexp1 = { regexp => { 'tata.*', 2 } }, here 2 is to boost query which means
      #                                               we are giving 2 times weight to this field
      #   => regexp2 = { regexp => {'tea.*', 2 } } same as above
      #   => fuzzy1 = {fuzzy => { 'tata', 1 } } here first param is value and second is fuzzyness
      #                                        fuzzyness specify edit distance parameter, which means
      #                                        if fuzzyness = 1 then query which can be converted to
      #                                        indexed string using one operation then it will return true
      #   => fuzzy2 = { fuzzy => {'tea', 1 } } same as above
      #   ABOVE QUERIES ARE CORRESPONDING TO "TEXT" field
      #   => c_fuzzy1 = { fuzzy => {'tea', 1 } } but this is for field "CONTEXT"
      #   => match_query = { match => { 'status', 1} } this will return document with status 1
      #   => boolean_query => {boolean => { 'should': [regexp1, regexp2, fuzzy1, fuzzy2, c_fuzzy1, c_fuzzy2] , 'must': [match_query]}}
      #   above query is same as (regexp1 OR regexp2 OR fuzzy1 OR fuzzy2 OR c_fuzzy1 OR c_fuzzy2) AND (match_query)
      #
      def build(query)
        boolean_query = Imli::ElasticQueryBuilder.get_boolean_query

        # Tokenate the query
        tokens = query.split(' ')

        tokens.each do |token|
          phone_regexp_query = Imli::ElasticQueryBuilder.get_regexp_query('phone_number', token + '.*', 5)
          emails_regexp_query = Imli::ElasticQueryBuilder.get_regexp_query('emails', token + '.*', 4)
          name_regexp_query = Imli::ElasticQueryBuilder.get_regexp_query('name', token + '.*', 2)
          orders_regexp_query = Imli::ElasticQueryBuilder.get_regexp_query('orders', token + '.*', 1)

          name_fuzzy_query = Imli::ElasticQueryBuilder.get_fuzzy_query('name', token, 1, 0.2)
          emails_fuzzy_query = Imli::ElasticQueryBuilder.get_fuzzy_query('emails', token, 1, 0.01)

          queries_to_add_with_or_query = [phone_regexp_query, emails_regexp_query, name_regexp_query, orders_regexp_query, name_fuzzy_query, emails_fuzzy_query]

          boolean_query = Imli::ElasticQueryBuilder.add_array_of_queries_in_boolean_query('should', queries_to_add_with_or_query, boolean_query)
        end
        boolean_query
      end

    end
  end
end
