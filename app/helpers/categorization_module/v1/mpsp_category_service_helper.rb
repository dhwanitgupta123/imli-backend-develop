#
# Module handles all the functionality related to Categorization of products
# 
module CategorizationModule
  #
  # Categorization module Version 1
  # 
  module V1 
    #
    # Module to generate CategoryService responses & other functionalities
    # 
    # @author [anuj]
    #
    module MpspCategoryServiceHelper
      # 
      # Create response of a department with all the mpsp_categories
      # 
      def self.get_mpsp_categories_hash(mpsp_categories)
        if mpsp_categories.blank?
          return []
        end
        mpsp_categories_hash = []
        mpsp_categories.each do |mpsp_category|
          mpsp_category = create_mpsp_category_hash(mpsp_category)
          mpsp_categories_hash.push(mpsp_category)
        end
        return mpsp_categories_hash
      end

      # 
      # Create response of a mpsp_category with all the mpsp sub categories
      # 
      def self.get_mpsp_sub_categories_hash(mpsp_sub_categories)
        if mpsp_sub_categories.blank?
          return [];
        end
        mpsp_sub_categories_hash = []
        mpsp_sub_categories.each do |mpsp_sub_category|
          mpsp_sub_category = create_mpsp_sub_category_hash(mpsp_sub_category)
          mpsp_sub_categories_hash.push(mpsp_sub_category)
        end
        return mpsp_sub_categories_hash
      end
      
      # 
      # create hash of mpsp_category object attributes
      # 
      def self.create_mpsp_category_hash(mpsp_category )
        response = {
          id: mpsp_category.id,
          department_id: mpsp_category.mpsp_department_id,
          label: mpsp_category.label,
          description: mpsp_category.description,
          priority: mpsp_category.priority,
          status: mpsp_category.status
        }
        return response
      end

      #
      # create hash of sub mpsp_category object attributes
      # 
      def self.create_mpsp_sub_category_hash(mpsp_sub_category)
        response = {
          id: mpsp_sub_category.id,
          category_id: mpsp_sub_category.mpsp_parent_category_id,
          label: mpsp_sub_category.label,
          description: mpsp_sub_category.description,
          priority: mpsp_sub_category.priority,
          status: mpsp_sub_category.status
        }
        return response
      end
    end
  end
end