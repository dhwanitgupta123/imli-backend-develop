module CommonModule
  module V1
    module ApplicationHelper


      # 
      # Logic for getting deciding parameters for versioning
      # @param params [Hash] params of http request
      # 
      # @return [Hash] Hash of parameters used for deciding versioning
      def self.get_deciding_params(params)
        deciding_params = params[:app_version].present? ? params[:app_version] : nil
        return deciding_params
      end
    end
  end
end