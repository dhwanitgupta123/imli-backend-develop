module CommonModule
  module V1
    #
    # Helper for serializing ProductSuggester Object model
    #
    module ProductSuggesterHelper

      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper

      #
      # create hash of product suggestion object attributes
      #
      def self.get_user_details_for_product_suggestion(product_suggestion)
        user_id = product_suggestion[:user_id]
        return {} if user_id.blank?

        user = USER_SERVICE_HELPER.get_user_from_user_id(user_id)
        if user.blank?
          user_details = {}
        else
          user_details = USER_SERVICE_HELPER.get_user_minimal_details(user)
        end

        return user_details
      end

      #
      # Create response with a list of all the suggestions
      #
      def self.get_product_suggestions_hash(suggestions)
        suggestions_hash = []
        return suggestions_hash if suggestions.blank?
        suggestions.each do |suggestion|
          suggestion = create_suggestion_hash(suggestion)
          suggestions_hash.push(suggestion)
        end
        return suggestions_hash
      end

      #
      # create hash of product suggestion object attributes
      #
      def self.create_suggestion_hash(suggestion)
        response = {
          user_id: suggestion[:user_id],
          product_suggestion: suggestion[:product_suggestion]
        }
        return response
      end
   	end
  end
end
