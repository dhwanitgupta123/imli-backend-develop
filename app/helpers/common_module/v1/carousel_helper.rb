module CommonModule 
  module V1
    # 
    # Helper for serializing Carousel Object model
    #
    module CarouselHelper

      CAROUSEL_TYPES_CLASS = CommonModule::V1::ModelStates::V1::CarouselTypes
      CAROUSEL_TYPES_CONSTANTS = CommonModule::V1::ModelStates::V1::CarouselTypes.constants
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      
      # 
      # map_carousel_images_to_array returns array of images
      # 
      def self.map_carousel_images_to_array(images)
        return [] if images.blank?
      	images_array = []
        priority = 1
      	images.each do |image|
      		image_hash = get_carousel_image_hash(image)
          image_hash[:priority] = priority.to_s
      		images_array.push(image_hash)
          priority += 1
      	end
      	return images_array
      end

      # 
      # get_carousel_image_hash returns hash of image for Carousel Model object
      # @param image [Hash]
      # - contains:
      #   * image_id [String] image id of image in the carousel
      #   * priority [String] priority of image
      #   * action   [String] action to be performed onclick() event on image
      #   * urls     [Array]  array of image in different resolutions
      # 
      # @return [Hash] carousel_image_hash
      # 
      def self.get_carousel_image_hash(image)
        carousel_image_hash = {}
        return carousel_image_hash if image.blank?
        image_array = IMAGE_SERVICE_HELPER.get_images_hash_by_ids([image[:image_id]])
        unless image_array.blank?
          carousel_image_hash = (image_array.first).merge({action: image[:action]})
        end
      	return carousel_image_hash
      end

      def self.get_carousel_details_from_carousel_type(type)
        carousels_array = []
        CAROUSEL_TYPES_CONSTANTS.each do |carousel_type|
          carousel = CAROUSEL_TYPES_CLASS.const_get(carousel_type)
          carousels_array << carousel
        end
        
        matched_carousel_type = (carousels_array.select {|carousel| carousel[:type] == type}).first
        matched_carousel_hash = get_carousel_type_hash(matched_carousel_type)

        return matched_carousel_hash
      end

      def self.get_carousel_type_hash(carousel_type)
        return {} if carousel_type.blank?
        carousel_type_hash = {
          type: carousel_type[:type],
          label: carousel_type[:label],
          description: carousel_type[:description]
        }
        return carousel_type_hash
      end

      def self.get_carousel_hash(carousel)
        images_array = map_carousel_images_to_array(carousel[:images])
        carousel_type_details = get_carousel_details_from_carousel_type(carousel[:type])
        carousel_hash = {
              type: carousel[:type],
              label: carousel_type_details[:label],
              description: carousel_type_details[:description],
              images: images_array
            }
        return carousel_hash
      end

      def self.map_carousels_to_array(carousels)
        carousels_array = []
        return carousels_array if carousels.blank?
        carousels.each do |carousel|
          carousel_hash = get_carousel_hash(carousel)
          carousels_array << carousel_hash
        end

        return carousels_array
      end

   	end
  end
end