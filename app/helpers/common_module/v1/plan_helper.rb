module CommonModule
  module V1
    #
    # Helper for serializing PLAN Object model
    #
    module PlanHelper
      BENEFIT_HELPER = CommonModule::V1::BenefitHelper
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      IMAGE_STYLES_UTIL = ImageServiceModule::V1::ImageStyles

      #
      # Get PLAN details
      # @param plan [PLAN Object] [Model Plan object whose details are needed]
      #
      # @return [JSON] [Hash of relevant Plan fields]
      def self.get_plan_details(plan, selected = false)
        return {} if plan.blank?
        return {
          id: plan.id,
          name: plan.name,
          details: plan.details,
          duration: plan.duration,
          validity_label: plan.validity_label,
          fee: plan.fee,
          service_tax: plan.service_tax,
          plan_key: plan.plan_key,
          status: plan.status,
          selected: selected,
          monthly_cost: get_monthly_cost(plan.fee, plan.duration),
          billed_duration_label: get_billed_duration_label(plan.duration) + ' 100% satisfaction guarantee. Cancel anytime.',
          short_description: get_short_description(plan),
          benefits: BENEFIT_HELPER.get_active_benefits_array(plan.benefits),
          images: IMAGE_SERVICE_HELPER.get_images_hash_by_ids(plan.images, IMAGE_STYLES_UTIL::PLAN_IMAGE_STYLE_FILTERS)
        }
      end

      #
      # Get Array of PLANs
      # @param plans [Array] [Array of PLAN Object]
      #
      # @return [Array] [Array containing hashes of relevant attributes of PLAN model]
      def self.get_plans_array(plans)
        plans_array = []
        return plans_array if plans.blank?
        plans.each do |plan|
          plans_array.push(get_plan_details(plan))
        end
        plans_array = sort_plans_array(plans_array)
        return plans_array
      end

      #
      # Get Array of PLANs which user can subscribe to
      # @param plans [Array] [Array of PLAN Object]
      #
      # @return [Array] [Array containing hashes of relevant attributes of PLAN model]
      def self.get_plans_array_for_user(plans, selected_plan = nil)
        plan_key = CommonModule::V1::PlanKeys
        plans_array = []
        return plans_array if plans.blank?
        plans.each do |plan|
          selected = selected_plan == plan ? true : false
          plans_array.push(get_plan_details(plan, selected)) unless plan.plan_key == plan_key::TRIAL
        end
        plans_array = sort_plans_array(plans_array)
        return plans_array
      end

      #
      # Function to get the monthly cost of the plan
      #
      def self.get_monthly_cost(fee, duration)
        return fee if duration < 1
        return (fee/duration.to_f).round(2)
      end

      #
      # Function to return billed label duration string
      #
      def self.get_billed_duration_label(duration)
        general_helper = CommonModule::V1::GeneralHelper
        case duration
        when 12
          return 'billed annually.'
        when 6
          return 'billed half yearly.'
        when 3
          return 'billed quarterly.'
        when 1
          return 'billed over ' + general_helper.number_to_words(duration) + ' month.'
        else
          return 'billed over ' + general_helper.number_to_words(duration) + ' months.'
        end
      end

      #
      # Function to return short description for the plan
      #
      def self.get_short_description(plan)
        return '' if plan.blank?
        CommonModule::V1::PlanKeys.get_plan_short_description(plan.plan_key)
      end

      #
      # Function to sort array on monthly cost
      #
      def self.sort_plans_array(plans_array)
        return plans_array.sort{ |a,b| b[:monthly_cost] <=> a[:monthly_cost] }
      end

      #
      # Function to fetch the link of plan image for the communication purpose
      #
      def self.get_plan_image_link_for_communication(plan)
        images = IMAGE_SERVICE_HELPER.get_images_hash_by_ids(plan.images, IMAGE_STYLES_UTIL::PLAN_IMAGE_STYLE_FILTERS)
        images.each do |image|
          if image[:priority] == "1"
            # Using original style type image
            return image[:urls][:original]
          end
        end
        return ''
      end
    end
  end
end
