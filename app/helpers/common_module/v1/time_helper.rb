# General Helper containing common functions which are used
# by different service objects
# @author Sumit <sumitchhuttani007@gmail.com>
module CommonModule
  module V1
    module TimeHelper

      #
      # Function to return hash of day, month & year of a time stamp
      #
      def self.get_details_of_time(timestamp)
        return {} if timestamp.blank?
        return {
          day: timestamp.day,
          month: timestamp.month,
          year: timestamp.year
        }
      end

      #
      # Function to return diff between two time stamps in months
      #   * time1 & time2 are time stamps
      #   * time1 should be greater than time 2
      #   * if day of time1 is greater then day of time2 1 month is added
      # 
      #   Example1: time1 = 2/10/16, time2 = 29/1/17, total_months = 4
      #   Example2: time1 = 4/5/16, time2 = 2/10/16, total_months = 5
      # 
      def self.get_difference_in_months(time1, time2)
        return 0 if time1 < time2
        time1 = get_details_of_time(time1)
        time2 = get_details_of_time(time2)
        return 0 if time1.blank? || time2.blank?

        year_diff = time1[:year] - time2[:year]
        month_diff = time1[:month] - time2[:month]
        day_diff = time1[:day] - time2[:day]
        total_months = month_diff
        total_months += year_diff * 12
        if day_diff > 0
          total_months += 1
        end
        return total_months
      end
    end
  end
end
