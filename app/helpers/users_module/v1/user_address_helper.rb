#
# Module handles all the functionality related to User
# 
module UsersModule
  #
  # UsersModule module Version 1
  # 
  module V1 
    #
    # Module to facilitate functionalities for various user's address
    # related queries
    #
    module UserAddressHelper
      extend self

      def get_address_limit_of_user(user)
        default_limit = 0
        address_benefit = nil
        membership_service = UsersModule::V1::MembershipService.new
        membership = membership_service.get_pending_or_active_membership(user)
        return default_limit if membership.blank?
        benefits = membership.plan.benefits
        if benefits.present?
          benefit_service = CommonModule::V1::BenefitService.new
          address_benefit = benefit_service.get_address_benefit(benefits)
        end
        return address_benefit.reward_value if address_benefit.present?
        return default_limit
      end

    end
  end
end