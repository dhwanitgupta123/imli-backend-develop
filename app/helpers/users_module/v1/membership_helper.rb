# 
# Helper to facilitate membership related functionalities
#
module UsersModule
  module V1
    module MembershipHelper

      #
      # Function to get exact duration of Ample Sample membership 
      # due to additional referral bonus membership days
      #
      def self.get_membership_duration(membership)
        time_helper = CommonModule::V1::TimeHelper
        duration = time_helper.get_difference_in_months(membership.expires_at, membership.starts_at)
        return duration
      end

      def self.is_membership_customer?(user)
        membership_service = UsersModule::V1::MembershipService.new
        memberships = membership_service.get_pending_or_active_membership(user)
        if memberships.present?
          return true
        else
          return false
        end
      end

      def self.get_last_expired_membership(memberships)
        return nil if memberships.blank?
        membership_service = UsersModule::V1::MembershipService.new
        last_expired_membership = membership_service.get_last_expired_membership(memberships)
        return last_expired_membership
      end

    end
  end
end
