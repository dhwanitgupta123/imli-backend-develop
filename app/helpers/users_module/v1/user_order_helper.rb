#
# Module handles all the functionality related to User
#
module UsersModule
  #
  # UsersModule module Version 1
  #
  module V1
    #
    # Module to facilitate functionalities for various user's order
    # related queries
    #
    module UserOrderHelper
      extend self

      def is_referral_used_in_first_order(user)
        order_helper = MarketplaceOrdersModule::V1::OrderHelper
        first_order = order_helper.get_first_order_of_user(user)
        return false if first_order.blank?
        ample_credits_used = order_helper.get_ample_credits_used_in_order(first_order)
        if ample_credits_used > 0
          return true
        else
          return false
        end
      end

      def get_user_orders_stats(user)
        order_dao = MarketplaceOrdersModule::V1::OrderDao
        status_count_map = order_dao.get_order_status_count(user)
        return [] if status_count_map.blank?
        status_array = []
        status_count_map.each do |key, value|
          status_hash = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates.get_order_state_by_value(key)
          status_hash = status_hash.merge({count: value.to_i})
          status_array.push(status_hash)
        end
        return status_array
      end


    end
  end
end
