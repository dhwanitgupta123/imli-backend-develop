#
# Module handles all the functionality related to User
# 
module UsersModule
  #
  # UsersModule module Version 1
  # 
  module V1
    module EmailMapper
      extend self

      # 
      # function to create hash of email object
      #
      def map_email_to_hash(email)
        return {} if email.blank?
        response = {
          id: email.id,
          email_id: email.email_id || '',
          is_primary: email.is_primary || false,
          account_type: email.account_type || '',
          verified: email.verified || false,
          expires_at: email.expires_at.to_i || ''
        }
        return response
      end

      def map_emails_to_array(emails)
        return [] if emails.blank?
        emails_array = []
        emails.each do |email|
          emails_array.push(map_email_to_hash(email))
        end
        emails_array
      end

    end
  end
end