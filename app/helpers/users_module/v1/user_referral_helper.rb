#
# Module handles all the functionality related to User
# 
module UsersModule
  #
  # UsersModule module Version 1
  # 
  module V1 
    #
    # Module to facilitate functionalities for various user's referral
    # related queries
    #
    module UserReferralHelper
      extend self

      def get_referrees_of_user(user)
        UsersModule::V1::UserDao.get_referrees_of_user(user)
      end

    end
  end
end