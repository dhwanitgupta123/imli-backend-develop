#
# Helper to facilitate membership related functionalities
#
module UsersModule
  module V1
    module MembershipMapper
      extend self

      def map_membership_to_hash(membership, options={})
        return {} if membership.blank?
        membership_hash = get_membership_basic_hash(membership)
        membership_hash[:plan] = membership_plan_details(membership.plan) if options[:plan_details].present?
        membership_hash[:membership_payment] = membership_payment_details(membership.membership_payment) if options[:payment_details].present?

        return membership_hash
      end

      def get_membership_basic_hash(membership)
        status_hash = UsersModule::V1::ModelStates::MembershipStates.get_membership_status_hash_by_id(membership.workflow_state)
        response = {
          id: membership.id,
          starts_at: membership.starts_at.to_i || '',
          expires_at: membership.expires_at.to_i || '',
          status: status_hash || {},
          quantity: membership.quantity || 0,
        }
      end

      def membership_plan_details(plan)
        return {} if plan.blank?
        CommonModule::V1::PlanHelper.get_plan_details(plan)
      end

      def membership_payment_details(membership_payment)
        return {} if membership_payment.blank?
        UsersModule::V1::MembershipModule::V1::MembershipPaymentHelper.get_payment_hash(membership_payment)
      end

      def map_memberships_to_array(memberships, options={})
        return [] if memberships.blank?
        memberships_array = []
        memberships.each do |membership|
          memberships_array.push(map_membership_to_hash(membership, options))
        end
        return memberships_array
      end

    end
  end
end
