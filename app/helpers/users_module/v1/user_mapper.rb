#
# Module handles all the functionality related to User
#
module UsersModule
  #
  # UsersModule module Version 1
  #
  module V1
    #
    # Module to generate responses for various UserService APIs
    #
    module UserMapper
      extend self

      MAX_ORDERS_IN_USER_SEARCH_QUERY = CommonModule::V1::Cache.read_int('MAX_ORDERS_IN_USER_SEARCH_QUERY')
      #
      # Map get users by search query response
      #
      # @param users [Array] [Array of UserDetailSearchModel]
      #
      # @return [Hash] [Hash containing users key]
      #
      def map_get_users_by_search_query_response(user_search_models)
        user_details_array = []
        user_dao = UsersModule::V1::UserDao.instance
        aggregated_users = user_dao.get_aggregated_users_and_orders_for_user_search_model(user_search_models)
        user_search_models.each do |user_search_model|
          user_details_array.push(user_search_query_hash(user_search_model, aggregated_users))
        end
        {
          users: user_details_array
        }
      end

      #
      # User Search query hash
      #
      # @param user_detail [Object] [UserDetailSearch model]
      #
      # @return [Hash] [Hash containing phone_number, name, and emails]
      #
      def user_search_query_hash(user_detail, aggregated_users)
        attributes = user_detail.attributes
        user_id = attributes["id"]
        last_n_orders = MAX_ORDERS_IN_USER_SEARCH_QUERY
        orders = user_orders(get_user(aggregated_users, user_id), last_n_orders)
        display_emails = attributes["display_emails"]
        display_emails = display_emails.split(" ")

        {
          id: user_id,
          phone_number: attributes["phone_number"],
          name: attributes["name"],
          emails: attributes["emails"],
          display_emails: display_emails,
          orders: orders
        }
      end

      def get_user(aggregated_users, id)
        if aggregated_users.present?
          user = aggregated_users.select{|u| u.id == id}.first
          return user if user.present?
        end
        # Fetch from DB if not found in aggregated users
        begin
          user_service = UsersModule::V1::UserService.new(@params)
          user = user_service.get_user_from_user_id(id)
        rescue => e
          return nil
        end
      end

      def user_orders(user, last_n_orders = '')
        return [] if user.blank?
        order_dao = MarketplaceOrdersModule::V1::OrderDao.new
        order_mapper = MarketplaceOrdersModule::V1::OrderMapper
        orders = order_dao.get_all_orders_of_user(user, last_n_orders)
        order_hash = order_mapper.map_multiple_orders_to_array(orders, {slot_details: true})
      end

      def map_user_basic_details(user, options={})
        result = {
          id: user.id,
          first_name: user.first_name || '',
          last_name: user.last_name || '',
          phone_number: user.phone_number,
          referral_code: user.referral_code,
          referral_limit: user.referral_limit,
          benefit_limit: user.benefit_limit,
          status: get_status_hash(user.workflow_state)
        }
        result[:emails] = UsersModule::V1::EmailDecorator.get_customer_type_email_hash(user.emails) if options[:email_details].present?
        result[:allowed_events] = UsersModule::V1::ModelStates::UserEvents.get_allowed_events(user) if options[:allowed_events_details].present?
        return result
      end

      def map_minimal_details_of_user(user)
        {
          id: user.id,
          first_name: user.first_name || '',
          last_name: user.last_name || '',
          phone_number: user.phone_number,
          status: get_status_hash(user.workflow_state)
        }
      end

      def map_users_to_minimal_details_array(users)
        return [] if users.blank?
        users_array = []
        users.each do |user|
          users_array.push(map_minimal_details_of_user(user))
        end
        users_array
      end

      def get_status_hash(status)
        UsersModule::V1::ModelStates::UserStates.get_status_hash_by_id(status)
      end

    end # End of UserMapper module
  end
end
