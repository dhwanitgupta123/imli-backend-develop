#
# Module to handle all the functionalities related to email id validation
#
module EmailValidationModule
  #
  # Version1 for email validation module
  #
  module V1
    #
    # Module validates if gmail_id is already in use or not
    #
    module VerifyGmailIdHelper
      VALID_GMAIL_REGEX = CommonModule::V1::RegexUtil::VALID_GMAIL_REGEX
      CACHE = CommonModule::V1::Cache
      #
      # This function checks if gmail id is already in use or not
      #
      # @param gmail_id [String] Gmail_Id
      #
      # @return [Boolean] true if already in use else false
      #
      def self.verify_if_already_in_use(gmail_id)
        return true if (gmail_id =~ VALID_GMAIL_REGEX).nil?

        user_name = get_user_name(gmail_id)

        key = create_key(user_name)

        # Read in Redis cache is O(1)
        if exists?(key)
          return true
        end

        return false
      end

      # 
      # Delete gmail id from cache
      # 
      # @param gmail_id [String] gmail_id
      #
      def self.delete(gmail_id)

        user_name = get_user_name(gmail_id)

        key = create_key(user_name)
        CACHE.delete(key)
      end

      # 
      # This function write gmail id in redis to mark it already in use
      #
      # @param gmail_id [String] Gmail_Id
      #
      def self.mark_gmail_id_in_use(gmail_id)

        return nil if (gmail_id =~ VALID_GMAIL_REGEX).nil?
        
        user_name = get_user_name(gmail_id)
        key = create_key(user_name)
        write_key(key)
      end

      #
      # This will remove @gmail.com from end of string
      # and remove the '.' from the remaining part
      #
      # @param gmail_id [String] gmail_id
      #
      # @return [String] user name
      #
      def self.get_user_name(gmail_id)
        gmail_id_without_gmail_postfix = gmail_id.gsub('@gmail.com','')
        split_by_plus = gmail_id_without_gmail_postfix.split('+')
        gmail_id_before_first_plus = split_by_plus[0]
        gmail_id_before_first_plus.delete('.')
      end

      #
      # Write the key, value pair in storage
      #
      # @param gmail_id_without_dots [String] key for user_name
      #
      def self.write_key(key)
        CACHE.write key: key, value: '1'
      end

      #
      # Check if gmail id exists in storage
      #
      # @param gmail_id_without_dots [String] key for user_name
      #
      # @return [Boolean] true if key is in storage else false
      #
      def self.exists?(key)
        CACHE.exists?(key)
      end

      #
      # Create key to save in cache
      #
      # @param gmail_id_without_dots [String] user_name
      #
      # @return [String] appending with prefix
      #
      def self.create_key(user_name)
        prefix = CACHE.read('GMAIL_ID_PREFIX') || 'GMAIL_ID::'
        prefix + user_name
      end

    end
  end
end
