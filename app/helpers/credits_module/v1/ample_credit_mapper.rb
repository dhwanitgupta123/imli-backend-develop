module CreditsModule
  module V1
    module AmpleCreditMapper
      # 
      # map ample_credit model to hash
      #
      # @param ample_credit [Model] ample_credit
      # 
      # @return [Hash] mapped hash from ampe_credit model
      #
      def self.map_ample_credit_to_hash(ample_credit)
        
        return {} if ample_credit.blank?

        {
          id: ample_credit.id,
          balance: ample_credit.balance,
          currency: ample_credit.currency,
          max_limit: ample_credit.max_limit,
          min_limit: ample_credit.min_limit
        }
      end

      # 
      # map ample_credit_benefit model to hash
      #
      # @param ample_credit_benefit [Model] ample_credit_benefit
      # 
      # @return [Hash] mapped hash from ampe_credit_benefit model
      #
      def self.map_ample_credit_benefit_to_hash(ample_credit_benefit)

        return {} if ample_credit_benefit.blank?

        return {
          id: ample_credit_benefit.id,
          benefit_limit: ample_credit_benefit.benefit_limit,
          benefit_count: ample_credit_benefit.benefit_count,
          refer_limit: ample_credit_benefit.refer_limit,
          refer_count: ample_credit_benefit.refer_count
        }
      end

      # 
      # return hash with ample_credit aand ample_credit_benefit
      #
      # @param ample_credit_and_benefit [Hash] hash of ample_credit and ample_credit_benefit model
      # 
      # @return [Hash] mapped hash from ampe_credit_benefit and ample_credit model
      #
      def self.map_ample_credit_and_benefit_to_hash(ample_credit_and_benefit)
        return {} if ample_credit_and_benefit.blank?

        ample_credit = map_ample_credit_to_hash(ample_credit_and_benefit[:ample_credit])
        ample_credit_benefit = map_ample_credit_benefit_to_hash(ample_credit_and_benefit[:ample_credit_benefit])
        return {ample_credit: ample_credit, ample_credit_benefit: ample_credit_benefit}
      end
    end
  end
end
