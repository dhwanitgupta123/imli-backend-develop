module CreditsModule
  module V1
    module AmpleCreditHelper

      CACHE_UTIL = CommonModule::V1::Cache
      CONTENT_UTIL = CommonModule::V1::Content

      #
      # get current balance in ample_credit
      #
      # @param ample_credit [Model] ample_credit
      #
      # @return [Decimal] return ample credit balance
      #
      def self.get_current_ample_credit_of_user(ample_credit)

        return BigDecimal.new(0) if ample_credit.blank?

        return ample_credit.balance
      end

      def self.get_pre_condition_hash(ample_credit, billing_amount)

        message = ''

        allowed_amount = BigDecimal.new(0)

        minimum_billing_amount = CACHE_UTIL.read_int('MINIMUM_ORDER_AMOUNT_FOR_CREDITS') || 2000

        if ample_credit.balance <= 0
          is_applicable = false
          message = ''
        elsif billing_amount >= minimum_billing_amount
          max_allowed_credits = CACHE_UTIL.read_int('MAX_ALLOWED_CREDITS') || 1000

          allowed_amount = [max_allowed_credits, billing_amount, ample_credit.balance.to_d].min

          allowed_amount = [allowed_amount, BigDecimal.new(0)].max

          is_applicable = true
        else
          is_applicable = false
          extra_amount = minimum_billing_amount - billing_amount
          message = CONTENT_UTIL::ORDER_MORE_FOR_CREDITS % {extra_amount: extra_amount}
        end

        return {
          allowed_amount: allowed_amount,
          is_applicable: is_applicable,
          message: message
        }
      end
    end
  end
end
