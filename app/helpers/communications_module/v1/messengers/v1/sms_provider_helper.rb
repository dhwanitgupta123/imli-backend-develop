#
# Helper to store CONSTANT for different SMS provider
# 
# @author [anuj]
#
module CommunicationsModule
  module V1
    module Messengers
      module V1
        module SmsProviderHelper
          GUPSHUP = 0
          ROUTESMS = 1
        end
      end
    end
  end
end