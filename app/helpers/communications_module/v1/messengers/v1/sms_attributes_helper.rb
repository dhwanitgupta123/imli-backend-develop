#
# Helper to to provide attributes for the SMS service to send SMS
# 
# @author [anuj]
#
module CommunicationsModule
  module V1
    module Messengers
      module V1
        module SmsAttributesHelper
          
          #
          # Function to setup sms template
          #
          # Parameters::
          #   * message attributes [object] contains message & phone number
          #
          def self.get_sms_attributes(message_attributes, template_name)
            template = render_template(message_attributes, template_name)
            return {
              message: template,
              phone_number: '91' + message_attributes['phone_number']
            }
          end

          # 
          # function to render respective sms template
          #
          def self.render_template(message_attributes, template_name)
            file = File.join(Rails.root,
                             'app',
                             'views',
                             'communications_module',
                             'sms_templates',
                             template_name + '.liquid'
                            )
            file = File.read(file)
            @template = Liquid::Template.parse(file)
            template = @template.render(message_attributes)
            return template
          end
        end
      end
    end
  end
end