#
# Module to handle functionalities related to Communications
#
module CommunicationsModule
  module V1
    module VoiceModule
      module V1
        #
        # Module to generate responses for various VoiceCommunication APIs
        #
        module VoiceCommunicationMapper
          extend self

          def get_status_hash(status)
            CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationStates.get_detail_hash_by_value(status)
          end

          def get_communication_direction_type(call_direction)
            CommunicationsModule::V1::VoiceModule::V1::ModelStates::V1::VoiceCommunicationDirectionTypes.get_voice_communication_direction_type_by_value(call_direction)
          end

          def map_voice_communication_basic_details(voice_communication, options={})
            result = {
              id: voice_communication[:id],
              sid: voice_communication[:sid],
              start_time: voice_communication[:start_time],
              end_time: voice_communication[:end_time],
              recording_url: voice_communication[:recording_url] || '',
              duration: voice_communication[:duration] || '',
              from: voice_communication[:from_phone_number],
              to: voice_communication[:to_phone_number],
              status: get_status_hash(voice_communication[:status]),
              call_direction: get_communication_direction_type(voice_communication[:call_direction])
            }
            return result
          end

        end
      end
    end
  end
end
