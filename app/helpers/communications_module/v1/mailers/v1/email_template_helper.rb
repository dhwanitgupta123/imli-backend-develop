#
# Helper to generate flat hash for email templates data points
# 
# @author [anuj]
#
module CommunicationsModule
  module V1
    module Mailers
      module V1
        module EmailTemplateHelper

          # 
          # funciton to create flat hash for welcome email template data points
          # 
          # Paramaters::
          #   * user [object] User for which we have to prep flat hash
          # 
          # @return [hash] flat hash with template data points
          def self.welcome_template_hash(user)
            template_hash = {
              "email_type" => CommonModule::V1::Cache.read('WELCOME'),
              "first_name" => user.first_name, 
              "last_name" => user.last_name, 
              "email_id" => user.emails.last.email_id,
              "authentication_token" => user.emails.last.authentication_token
                      }
            return template_hash
          end
        end
      end
    end
  end
end