module LoggingModule
  module V1
    module EventLoggerHelper
      extend self

      EVENT_LOG_CHANNEL = CommonModule::V1::Cache.read('EVENT_LOG_CHANNEL') || 'logevents'

      #
      # Modify the args, create dump and topic hash to log in kafka
      #
      def create_record(args)

        args = modify_args(args)

        return {dump: args.to_json, topic: EVENT_LOG_CHANNEL}
      end

      private

      def modify_args(args)

        args['time'] = CommonModule::V1::DateTimeUtil.get_current_time('utc').strftime("%Y-%m-%dT%H:%M:%SZ") if args['time'].blank?
        args['database'] = 'NA' if args['database'].blank?
        args['table'] = 'NA' if args['table'].blank?
        args['record'] = 'NA' if args['record'].blank?
        args['user'] = 'NA' if args['user'].blank?
        args['changes'] = 'NA' if args['changes'].blank?
        return args
      end
    end
  end
end
