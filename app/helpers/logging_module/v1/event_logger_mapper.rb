module LoggingModule
  module V1
    module EventLoggerMapper
      extend self

      def update_events_response(events)
        return [] if events.blank?

        events.each do |event|
          user = UsersModule::V1::User.find_by(id: event['user']) if event['user'].present?
          event['user'] = UsersModule::V1::UserServiceHelper.get_user_minimal_details(user) if user.present?
        end

        return events
      end
    end
  end
end
