module LoggingModule
  module V1
    module EventQueryHelper
      extend self

      EVENT_DATASOURCE = 'logevents-kafka'

      def build_select_query(args)

        query = get_query
        add_pagination_spec(query, args)
        add_default_select_filters(query, args)
        query.add_filter{table == args[:table]} if args[:table].present?
        query.add_filter{record.regexp('\{id='+ args[:id].to_s + ',')} if args[:id].present?

        query.add_filter{user == args[:user].to_s} if args[:user].present?

        add_interval(query, args)
        return query
      end

      private

      def add_interval(query, args)

        return if args[:from_time].blank? || args[:to_time].blank?

        date_time_util = CommonModule::V1::DateTimeUtil

        from_timestamp = date_time_util.convert_epoch_to_datetime(args[:from_time], true) # in utc
        to_timestamp = date_time_util.convert_epoch_to_datetime(args[:to_time], true) # in utc

        query.add_interval(from_timestamp, to_timestamp)
      end

      def add_default_select_filters(query, args)
        db_name = args[:database] || get_db_name
        query.add_filter{database == db_name}
      end

      def add_pagination_spec(query, args)
        last_n = args[:last_n] || 10
        descending = args[:descending] || true
        query.add_properties({queryType: 'select', pagingSpec: {threshold: last_n}, descending: descending})
      end

      def get_query
        druid_client = Imli::Druid::DruidClient.new({datasource: EVENT_DATASOURCE})
        query = Imli::Druid::Query.new({datasource: EVENT_DATASOURCE, druid_client: druid_client})
        return query
      end

      def get_db_name
        Rails.env + '-' + Rails.configuration.database_configuration[Rails.env]['database']
      end
    end
  end
end
