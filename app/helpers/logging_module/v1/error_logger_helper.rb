module LoggingModule
  module V1
    module ErrorLoggerHelper
      extend self

      ERROR_LOG_CHANNEL = CommonModule::V1::Cache.read('ERROR_LOG_CHANNEL') || 'logerrors'

      #
      # Modify the args, create dump and topic hash to log in kafka
      #
      def create_record(args)

        args = modify_args(args)

        return {dump: args.to_json, topic: ERROR_LOG_CHANNEL}
      end

      private

      def modify_args(args)

        new_args = {}

        new_args['time'] = CommonModule::V1::DateTimeUtil.get_current_time('utc').strftime("%Y-%m-%dT%H:%M:%SZ")
        new_args['error_message'] = args['error']['message']
        new_args['backtrace'] = args['error']['backtrace']
        new_args['error_class'] = args['error']['name']
        new_args['url'] = args['request']['url']
        new_args['params'] = args['request']['params']
        new_args['path'] = args['request']['path']

        return new_args
      end
    end
  end
end
