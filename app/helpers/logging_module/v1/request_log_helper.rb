module LoggingModule
  module V1
    module RequestLogHelper

      ACCESS_CONTROL_HELPER = UsersModule::V1::AccessControlHelper

      # 
      # This function maps the params to one hash those are required to log
      # @param params [hash] request parameters
      # @param request [object] request object
      # @param response [object] response object
      # @param status [String] status code
      # @param controller [object] controller object
      # 
      # @return [hash] hash consisting information to save in activity log 
      # 
      def self.create_log_params(params)
        request = params[:request]
        response = params[:response]
        controller = params[:controller]
        app_details = ACCESS_CONTROL_HELPER.fetch_app_details_from_request_headers(request)
        log_params = {}
        log_params['request_ip'] = request.ip
        log_params['request_url'] = request.url
        log_params['request_query_string'] = request.query_string
        log_params['api_name'] = "#{controller.controller_name}::#{controller.action_name}"
        log_params['response_status'] = params[:status_code]
        log_params['response_error'] = (response.present? && response[:error].present?) ? response[:error][:message] : ''
        log_params['phone_number'] = params[:phone_number]
        log_params['time'] = params[:time]
        log_params['api_spawn_time'] = params[:api_spawn_time].to_s if params[:api_spawn_time].present?
        log_params['device_id'] = app_details[:device_id]
        log_params['app_version'] = app_details[:app_version]
        log_params['platform_type'] = app_details[:platform_type]
        return log_params
      end
    end
  end
end