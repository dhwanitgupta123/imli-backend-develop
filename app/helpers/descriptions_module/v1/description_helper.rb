module DescriptionsModule
  module V1
    module DescriptionHelper


      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      # 
      # This function returns the hash of description from top
      # to bottom node
      #
      # @param description [Model] parent description
      # 
      # @return [Hash] hash of description tree
      #
      def self.get_description_tree(description)

        return {} if description.blank?

        description_hash = {
          id: description.id,
          heading: description.heading,
          data: description.data
        }

        if description.childs.blank?
          return description_hash
        end

        childs = description.childs
        
        childs_array = []

        childs.each do |child|
          childs_array.push(get_description_tree(child))
        end

        return description_hash.merge(childs: childs_array)
      end

      def self.segregate_new_childs(updated_description)
        new_childs = []
        to_update_childs = []

        return {new_childs: new_childs, to_update_childs: to_update_childs} if updated_description.blank?

        updated_description.each do |description|
          if description[:id].blank?
            new_childs.push(description) 
          else
            to_update_childs.push(description)
          end
        end

        return {new_childs: new_childs, to_update_childs: to_update_childs}
      end

      def self.validate_resource(resource, resource_id)

        raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT::INVALID_RESOURCE_MAPPING) if resource.blank? || resource_id.blank?
        begin
          resource = resource.constantize
          object = resource.find(resource_id)
        rescue => e
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT::INVALID_RESOURCE_MAPPING + ' ' + e.message)
        end

        return object
      end
    end
  end
end
