#
# Module to handle functionalities related to Ample Credits
#
module CreditsModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    module DebitFromAmpleCreditsResponse
      extend self

      def build(result)
        user = result
        ample_credits = user_ample_credits(user)

        response = {
          ample_credit: ample_credits
        }
        CreditsModule::V1::AmpleCreditsResponseDecorator.create_ample_credits_response({user: response})
      end

      def user_ample_credits(user)
        CreditsModule::V1::AmpleCreditMapper.map_ample_credit_to_hash(user.ample_credit)
      end

    end
  end
end
