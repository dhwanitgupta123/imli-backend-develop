#
# Module to handle functionalities related to Communications
#
module CommunicationsModule
  module V1
    #
    # GetCommunicationDetailsResponse class
    #
    module GetCommunicationDetailsResponse
      extend self

      MODEL_STATES_ENUM = CommunicationsModule::V1::ModelStates::V1::CommunicationModelTypes
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      def build(communication_details, options={})
        communication_type = options[:communication_type]
        return {} if communication_type.blank?
        response_builder = get_response_builder(communication_type)
        response = response_builder.build(communication_details)
        return response
      end

      def get_response_builder(communication_type)
        case communication_type.to_i
        when MODEL_STATES_ENUM.get_value_by_key('SMS')
          # SMS response class
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new("SMS isn't a logged Communication Type")
        when MODEL_STATES_ENUM.get_value_by_key('NOTIFICATION')
          # NOTIFICATION response class
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new("Notification isn't logged Communication Type")
        when MODEL_STATES_ENUM.get_value_by_key('EMAIL')
          # EMAIL response class
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new("Email isn't logged Communication Type")
        when MODEL_STATES_ENUM.get_value_by_key('VOICE')
          # VOICE response class
          return CommunicationsModule::V1::VoiceModule::V1::GetVoiceCommunicationDetailsResponse
        end
      end

    end

  end
end
