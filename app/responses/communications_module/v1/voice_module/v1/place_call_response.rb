#
# Module to handle functionalities related to Communications
#
module CommunicationsModule
  module V1
    module VoiceModule
      module V1

        #
        # PlaceCallResponse class
        #
        module PlaceCallResponse
          extend self

          def build(result)
            CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationResponseDecorator.create_voice_communication_ok_response(result)
          end

        end
      end

    end
  end
end
