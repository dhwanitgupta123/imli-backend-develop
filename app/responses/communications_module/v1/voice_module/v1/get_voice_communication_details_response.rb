#
# Module to handle functionalities related to Communications
#
module CommunicationsModule
  module V1
    module VoiceModule
      module V1

        #
        # GetVoiceCommunicationDetailsResponse class
        #
        module GetVoiceCommunicationDetailsResponse
          extend self

          def build(result)
            result = voice_communication_details(result)

            CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationResponseDecorator.create_voice_communication_ok_response(result)
          end

          def voice_communication_details(voice_communication)
            voice_communication_mapper = CommunicationsModule::V1::VoiceModule::V1::VoiceCommunicationMapper
            voice_communication_mapper.map_voice_communication_basic_details(voice_communication)
          end

        end
      end

    end
  end
end
