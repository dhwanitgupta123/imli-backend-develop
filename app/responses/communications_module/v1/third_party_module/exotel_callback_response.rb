#
# Module to handle functionalities related to Communications
#
module CommunicationsModule
  module V1
    module ThirdPartyModule
      module ExotelCallbackResponse
        extend self

        def build(result)
          CommunicationsModule::V1::ThirdPartyModule::ExotelCommunicationResponseDecorator.create_exotel_communication_ok_response(result)
        end
      end
    end
  end
end
