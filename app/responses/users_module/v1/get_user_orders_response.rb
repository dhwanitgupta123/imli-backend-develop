#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    module GetUserOrdersResponse
      extend self

      def build(result)
        user = result
        orders = user_orders(user)

        result = {
          orders: orders || []
        }
        UsersModule::V1::UserResponseDecorator.create_get_user_details_response({user: result})
      end

      def user_orders(user)
        order_dao = MarketplaceOrdersModule::V1::OrderDao.new
        order_mapper = MarketplaceOrdersModule::V1::OrderMapper
        orders = order_dao.get_all_orders_of_user(user)
        order_hash = order_mapper.map_multiple_orders_to_array(orders, {payment_details: true, context_details: true})
      end

    end
  end
end
