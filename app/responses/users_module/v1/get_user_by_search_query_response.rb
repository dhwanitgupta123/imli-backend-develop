#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    module GetUserBySearchQueryResponse
      extend self

      def build(result)
        user_mapper = UsersModule::V1::UserMapper
        user_response_decorator = UsersModule::V1::UserResponseDecorator
        response = user_mapper.map_get_users_by_search_query_response(result)
        user_response_decorator.create_get_users_by_search_query_response(response)
      end

    end
  end
end
