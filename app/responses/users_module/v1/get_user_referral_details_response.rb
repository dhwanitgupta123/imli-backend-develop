#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    module GetUserReferralDetailsResponse
      extend self

      def build(result)
        user = result
        referred_by_hash = referred_by_details(user)
        referrals_array = user_referral_details(user)
        referral_benefit = referral_benefit_details(user)
        referral_used_in_first_order = is_referral_used_in_first_order(user)

        result = {
          referral_code: user.referral_code,
          referred_by: referred_by_hash,
          referees: referrals_array,
          referral_benefit: referral_benefit,
          referral_used_in_first_order: referral_used_in_first_order
        }
        UsersModule::V1::UserResponseDecorator.create_get_user_details_response({user: result})
      end

      def referred_by_details(user)
        referred_by_user = user.referrer
        return {} if referred_by_user.blank?
        UsersModule::V1::UserMapper.map_minimal_details_of_user(referred_by_user)
      end

      def user_referral_details(user)
        referees = UsersModule::V1::UserReferralHelper.get_referrees_of_user(user)
        return [] if referees.blank?
        UsersModule::V1::UserMapper.map_users_to_minimal_details_array(referees)
      end

      def referral_benefit_details(user)
        ample_credit_benefit = CommonModule::V1::BenefitService.get_ample_credit_benefit_of_user(user)
        CreditsModule::V1::AmpleCreditMapper.map_ample_credit_benefit_to_hash(ample_credit_benefit)
      end

      def is_referral_used_in_first_order(user)
        UsersModule::V1::UserOrderHelper.is_referral_used_in_first_order(user)
      end

    end
  end
end
