#
# Module to handle functionalities related to User
#
module UsersModule
  module V1

    #
    # DeleteRoleResponse class
    #
    module DeleteRoleResponse
      extend self

      def build(result)
        UsersModule::V1::RoleResponseDecorator.create_delete_role_ok_response()
      end

    end
  end
end
