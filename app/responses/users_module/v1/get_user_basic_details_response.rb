#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    module GetUserBasicDetailsResponse
      extend self

      def build(result)
        user = result
        recent_order = user_recent_order(user)
        user_hash = user_details(user)

        result = user_hash.merge({
          recent_order: recent_order
        })
        UsersModule::V1::UserResponseDecorator.create_get_user_details_response({user: result})
      end

      def user_details(user)
        user_mapper = UsersModule::V1::UserMapper
        user_mapper.map_user_basic_details(user, {email_details: true})
      end

      def user_recent_order(user)
        order_dao = MarketplaceOrdersModule::V1::OrderDao.new
        order_mapper = MarketplaceOrdersModule::V1::OrderMapper
        order_limit = 1
        orders = order_dao.get_all_orders_of_user(user, order_limit)
        return {} if orders.blank?
        recent_order = orders.first
        return order_mapper.map_order_to_hash(recent_order, {payment_details: true, context_details: true, slot_details: true})
      end

    end
  end
end
