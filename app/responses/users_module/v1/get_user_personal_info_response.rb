#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    module GetUserPersonalInfoResponse
      extend self

      def build(result)
        user = result
        user_hash = user_details(user)
        profile = user_profile(user)
        emails = user_emails(user)
        order_stats = user_orders_stats(user)

        result = user_hash.merge({
          profile: profile,
          emails: emails,
          order_stats: order_stats
        })
        UsersModule::V1::UserResponseDecorator.create_get_user_details_response({user: result})
      end

      def user_details(user)
        user_mapper = UsersModule::V1::UserMapper
        user_mapper.map_user_basic_details(user, {allowed_events_details: true})
      end

      def user_profile(user)
        profile_decorator = UsersModule::V1::ProfileResponseDecorator
        profile_decorator.get_profile_hash(user.profile)
      end

      def user_emails(user)
        UsersModule::V1::EmailMapper.map_emails_to_array(user.emails)
      end

      def user_orders_stats(user)
        user_order_helper = UsersModule::V1::UserOrderHelper
        order_stats = user_order_helper.get_user_orders_stats(user)
        return order_stats
      end

    end
  end
end
