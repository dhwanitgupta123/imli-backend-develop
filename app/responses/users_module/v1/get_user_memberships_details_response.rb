#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    module GetUserMembershipsDetailsResponse
      extend self

      def build(result)
        user = result
        memberships = user_memberships(user)

        result = {
          memberships: memberships || []
        }
        UsersModule::V1::UserResponseDecorator.create_get_user_details_response({user: result})
      end

      def user_memberships(user)
        membership_dao = UsersModule::V1::MembershipDao.new
        memberships = membership_dao.get_all_memberships_of_user(user)
        UsersModule::V1::MembershipMapper.map_memberships_to_array(memberships, {plan_details: true, payment_details: true})
      end

    end
  end
end
