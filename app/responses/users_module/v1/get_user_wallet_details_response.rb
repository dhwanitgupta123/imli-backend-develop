#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    module GetUserWalletDetailsResponse
      extend self

      def build(result)
        user = result
        ample_credits = user_ample_credits(user)

        result = {
          ample_credit: ample_credits
        }
        UsersModule::V1::UserResponseDecorator.create_get_user_details_response({user: result})
      end

      def user_ample_credits(user)
        CreditsModule::V1::AmpleCreditMapper.map_ample_credit_to_hash(user.ample_credit)
      end

    end
  end
end
