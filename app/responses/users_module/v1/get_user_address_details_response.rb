#
# Module to handle functionalities related to User
#
require 'imli_singleton'
module UsersModule
  module V1

    #
    # Get current balance in ample credit of user
    #
    module GetUserAddressDetailsResponse
      extend self

      def build(result)
        user = result
        addresses = user_addresses(user)
        address_limit = user_address_limit(user)

        result = {
          addresses: addresses || [],
          address_limit: address_limit || 0
        }
        UsersModule::V1::UserResponseDecorator.create_get_user_details_response({user: result})
      end

      def user_addresses(user)
        address_helper = AddressModule::V1::AddressHelper
        address_helper.get_addresses_hash(user.addresses)
      end

      def user_address_limit(user)
        UsersModule::V1::UserAddressHelper.get_address_limit_of_user(user)
      end

    end
  end
end
