module CreditsModule
  module V1
    class AmpleCreditsResponseDecorator < BaseModule::V1::BaseResponseDecorator

      def self.create_ample_credits_response(response = {})
        response = {
          payload: {
            user: response[:user]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

    end
  end
end
