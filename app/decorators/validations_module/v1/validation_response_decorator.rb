#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc 
#
module ValidationsModule
  #
  # Version1 for validation module 
  #
  module V1
    #
    # Class to create JSON response for validations
    #
    class ValidationResponseDecorator < BaseModule::V1::BaseResponseDecorator


    end
  end
end
