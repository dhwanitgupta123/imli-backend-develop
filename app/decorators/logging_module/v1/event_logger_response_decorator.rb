#
# Module handles all the functionality related to LoggingModule
#
module LoggingModule
  #
  # LoggingModule Version 1
  #
  module V1
    class EventLoggerResponseDecorator < BaseModule::V1::BaseResponseDecorator

      #
      # Return all events
      #
      def self.create_events_response(events)
        {
          payload: {
            events: events
          },
          response: CommonModule::V1::ResponseCodes::SUCCESS
        }
      end
    end
  end
end
