#
# Module to handle functionalities related to transactions
#
module TransactionsModule
  #
  # Version1 for transactions module
  #
  module V1

    class PaymentTransactionsResponseDecorator < BaseModule::V1::BaseResponseDecorator

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      GATEWAY_TYPE = TransactionsModule::V1::GatewayType

      def self.create_citrus_response(response)
        if response[:payload].present?
          response = {
            payload: {
              transaction: {
                id: response[:payload][:transaction][:id],
                payment_status: response[:payload][:transaction][:payment_status],
                gateway: GATEWAY_TYPE::CITRUS_WEB,
                citrus_web: {
                  url: response[:payload][:transaction][:url]
                }
              }
            },
            response: RESPONSE_CODES_UTIL::SUCCESS
          }
        end
        return response
      end
    end
  end
end
