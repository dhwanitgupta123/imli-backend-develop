module UsersModule
  module V1
    #
    # Class to create JSON response of membership
    #
    class MembershipResponseDecorator < BaseModule::V1::BaseResponseDecorator

      MEMBERSHIP_STATE = UsersModule::V1::ModelStates::MembershipStates
      PLAN_HELPER = CommonModule::V1::PlanHelper
      MEMBERSHIP_PAYMENT = UsersModule::V1::MembershipModule::V1::MembershipPaymentHelper
      MEMBERSHIP_HELPER = UsersModule::V1::MembershipHelper

      #
      # Function to create response for membership 
      #
      def self.create_valid_membership_response(membership_hash)
        response = {
          payload: {
            membership: membership_hash
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Function to create  hash of active/pending membership of a user
      #
      def self.get_user_membership_hash(memberships)
        membership_array = []
        return membership_array if memberships.blank?
        last_expired_membership = MEMBERSHIP_HELPER.get_last_expired_membership(memberships)
        memberships = memberships.sort{|a,b| a.workflow_state <=> b.workflow_state}
        memberships.each do |membership|
          if MEMBERSHIP_STATE::ALLOWED_STATES.include?(membership.workflow_state)
            membership_array.push(get_membership_hash(membership))
          end
        end
        membership_array.push(get_membership_hash(last_expired_membership)) if last_expired_membership.present?
        return membership_array
      end

      # 
      # function to create hash of membership object
      #
      def self.get_membership_hash(membership)
        return {} if membership.blank?
        plan = PLAN_HELPER.get_plan_details(membership.plan)
        payment = MEMBERSHIP_PAYMENT.get_payments_detail(membership.membership_payments)
        response = {
          id: membership.id,
          starts_at: membership.starts_at.to_i || '',
          expires_at: membership.expires_at.to_i || '',
          status: membership.workflow_state || '',
          quantity: membership.quantity || 0,
          membership_payments: payment || [],
          plan: plan || {}
        }
        return response
      end

      #
      # Function to get hash for membership bought as a third party service
      #
      def self.get_membership_order_hash(membership)
        return {} if membership.blank?
        plan = PLAN_HELPER.get_plan_details(membership.plan)
        payment = MEMBERSHIP_PAYMENT.get_payment_hash(membership.membership_payments.first)
        hash = {
          id: membership.id,
          label: plan[:name] + ' (' + plan[:validity_label] + ')',
          amount: payment[:net_total],
          plan: plan
        }
        return hash
      end

      def self.create_user_membership_payment_details_response(membership, payment_mode_details = {})
        membership = get_membership_hash(membership)
        payment_details = 
        response = {
          payload: {
            membership: membership || {},
            payment_details: payment_mode_details
          },
          :response => RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_user_membership_response(membership)
        membership = get_membership_hash(membership)
        payment_details = 
        response = {
          payload: {
            membership: membership || {}
          },
          :response => RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end
    end
  end
end
