module UsersModule
  module V1
    #
    # Class to create JSON response of Emails
    #
    class EmailDecorator < BaseModule::V1::BaseResponseDecorator

      ACCOUNT_TYPE = UsersModule::V1::AccountType

      # 
      # Function to create array of hash of emails of customer type
      #
      def self.get_customer_type_email_hash(emails)
        emails_array = []
        return emails_array if emails.blank?
        emails.each do |email|
          if email.account_type == ACCOUNT_TYPE::CUSTOMER
            emails_array.push(get_email_hash(email))
          end
        end
        return emails_array
      end

      # 
      # function to create hash of email object
      #
      def self.get_email_hash(email)
        return {} if email.blank?
        response = {
          id: email.id,
          email_id: email.email_id || '',
          is_primary: email.is_primary || false,
          account_type: email.account_type || '',
          verified: email.verified || false,
          expires_at: email.expires_at.to_i || ''
        }
        return response
      end
    end
  end
end
