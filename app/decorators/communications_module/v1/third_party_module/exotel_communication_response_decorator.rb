module CommunicationsModule
  module V1
    module ThirdPartyModule
      class ExotelCommunicationResponseDecorator < CommunicationsModule::V1::CommunicationResponseDecorator

        def self.create_exotel_communication_ok_response(result)
          create_ok_response(result)
        end
      end
    end
  end
end
