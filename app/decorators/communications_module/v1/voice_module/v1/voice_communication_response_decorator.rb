module CommunicationsModule
  module V1
    module VoiceModule
      module V1
        class VoiceCommunicationResponseDecorator < CommunicationsModule::V1::CommunicationResponseDecorator

          def self.create_voice_communication_ok_response(result)
            create_ok_response(result)
          end
        end
      end
    end
  end
end
