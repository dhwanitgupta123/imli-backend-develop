module PaymentModule
  module V1
    class PaytmPaymentResponseDecorator < BaseModule::V1::BaseResponseDecorator

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      GATEWAY_TYPE = TransactionsModule::V1::GatewayType

      # 
      # Send ok response
      # 
      def self.create_ok_response(args)
        response = {
          payload: {
            transaction: {
              id: args[:transaction_id],
              gateway: TransactionsModule::V1::GatewayType::PAYTM,
              paytm: args
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end
    end
  end
end
