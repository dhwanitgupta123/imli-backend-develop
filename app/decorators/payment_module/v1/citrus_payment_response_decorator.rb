module PaymentModule
  module V1
    class CitrusPaymentResponseDecorator < BaseModule::V1::BaseResponseDecorator

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      # 
      # Send ok response
      # 
      def self.create_ok_response(args)
        response = {
          payload: {
            transaction: 
            {
              url: args[:url] + '?transaction_id=' + args[:transaction_id],
              id: args[:transaction_id],
              payment_status: args[:payment_status]
            }
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      # 
      # Send transaction failed error response
      # 
      def self.create_transaction_failed_response(message = '')
        response = {
          :error => {:message => CONTENT_UTIL::CITRUS_TRANSACTION_FAILED},
          :response => RESPONSE_CODES_UTIL::UN_AUTHORIZED
        }
        return response
      end
    end
  end
end
