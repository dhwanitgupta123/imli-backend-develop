module CommonModule
  module V1
    class CarouselResponseDecorator < BaseModule::V1::BaseResponseDecorator
      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      CAROUSEL_HELPER = CommonModule::V1::CarouselHelper

      #
      # Create OK response
      #
      def self.create_carousel_ok_response(carousel)
        carousel_hash = CAROUSEL_HELPER.get_carousel_hash(carousel)
        response = {
          payload: {
            carousel: carousel_hash 
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_all_carousels_details_ok_response(carousels_array)
        response = {
          payload: {
            carousels_types: carousels_array  
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      def self.create_get_all_carousels_ok_response(carousels_array)
        response = {
          payload: {
            carousels: carousels_array  
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

    end
  end
end
