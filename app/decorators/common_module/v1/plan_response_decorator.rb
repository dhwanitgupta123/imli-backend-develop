module CommonModule
  module V1
    class PlanResponseDecorator < BaseModule::V1::BaseResponseDecorator
      # Define utils globally with versioning
      # Kept them as global constants, so that they are specific
      # to this class only
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      PLAN_HELPER = CommonModule::V1::PlanHelper

      #
      # Create OK response with PLAN hash
      #
      def self.create_response_plan_hash(plan)
        response = {
          payload: {
            plan: PLAN_HELPER.get_plan_details(plan)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Create OK response with Array of PLANs
      #
      def self.create_response_plans_array(plans)
        response = {
          payload: {
            plans: PLAN_HELPER.get_plans_array(plans)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Create OK response with Array of PLANs for active plans
      #
      def self.create_response_of_active_plans_array(plans)
        response = {
          payload: {
            plans: PLAN_HELPER.get_plans_array_for_user(plans)
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end

      #
      # Function to return response with all the available plans if membership has expired
      #
      def self.create_available_plans_response(plans, selected_plan = nil)
        response = {
          payload: {
            available_plans: PLAN_HELPER.get_plans_array_for_user(plans, selected_plan)
          },
          error: {
            message: CONTENT_UTIL::MEMBERSHIP_EXPIRED
          },
          response: RESPONSE_CODES_UTIL::PRE_CONDITION_REQUIRED
        }
        return response
      end
    end
  end
end
