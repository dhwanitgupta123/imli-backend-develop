namespace :get do
  module Cohort
    require 'csv'

    def self.init
      @user_index = {}
      @month_index = {}
      puts 'please enter period for cohorts in number of months'
      @period = (STDIN.gets.strip).to_i
      puts 'please enter year'
      @year = (STDIN.gets.strip).to_i
      puts 'please enter starting month number'
      @month = (STDIN.gets.strip).to_i
      create_month_index
      create_cohorts_csv
    end

    def self.create_month_index
      
      orders = MarketplaceOrdersModule::V1::OrderDao.get_completed_orders
      orders = orders.order('order_date DESC')
      @start_date = Time.zone.now.change(year: @year, month: @month, day: 1, hour: 0, min: 0, sec: 0)
      @last_date = orders[0].order_date
      start_time = @start_date
      period_offset = 0
      while start_time < @last_date do
        orders_in_period = orders.where("order_date >= ? AND order_date < ?", start_time, (start_time + @period.months))
        orders_in_period.each do |order|
          @month_index[period_offset] = [] if @month_index[period_offset].blank?
          @month_index[period_offset] = @month_index[period_offset] | [order.user_id] if @user_index[order.user_id].blank?
          user_data = @user_index[order.user_id] ||  {}
          user_data[period_offset] = user_data[period_offset] || 0
          user_data[period_offset] += 1

          @user_index[order.user_id] = user_data
        end
        start_time = start_time + @period.months
        period_offset+=1
      end
    end

    def self.get_month_name(month)
      month = month + @month
      year = @year
      if month > 12
        year = year + month/12
        month = month % 12
      end
      year = year%100
      return  I18n.t("date.abbr_month_names")[month] + '\'' + year.to_s
    end

    def self.get_row_name(month)
      if @period > 1
        row = [get_month_name(month*@period) + '-' + get_month_name(((month+1)*@period)-1)]
      else
        row = [get_month_name(month*@period)]
      end
      return row
    end

    def self.create_cohorts_csv

      output_file = CSV.open('cohorts.csv','w')
      output_file << ['User retention'] 
      output_file << ['Cohorts every ' + @period.to_s + (@period > 1 ? ' months' : ' month') + ' from ' + @month.to_s + '/' + @year.to_s]
      output_file << ['', 'No. of Users'] + @month_index.keys.map {|month| month.to_s}

      @month_index.keys.each do |month|
        row = get_row_name(month)
        order_data = monthly_order_retention(month, @month_index.keys)
        row += order_data
        output_file << row
      end

      output_file << []
      output_file << ['Order per user retention']
      output_file << ['Cohorts every ' + @period.to_s + (@period > 1 ? ' months' : ' month') + ' from ' + @month.to_s + '/' + @year.to_s]
      output_file << ['', 'No. of Users'] + @month_index.keys.map {|month| month.to_s}

      @month_index.keys.each do |month|
        row = get_row_name(month)
        order_data = monthly_order_per_retained_user(month, @month_index.keys)
        row += order_data
        output_file << row
      end

      output_file.close
    end

    def self.monthly_order_retention(month, months)

      stats = []

      users_started_in_month = @month_index[month]

      stats.push(users_started_in_month.length)

      months.each do |m|
        next if m < month
        sum = 0
        users_started_in_month.each do |user|
          sum += 1 if  @user_index[user][m].present?
        end
        user_per_month = users_started_in_month.length != 0 ? sum.to_f/users_started_in_month.length.to_f : 0
        stats.push(user_per_month.round(2))
      end
      return stats
    end

    def self.monthly_order_per_retained_user(month, months)
      stats = []

      users_started_in_month = @month_index[month]

      stats.push(users_started_in_month.length)

      months.each do |m|
        next if m < month
        sum = 0
        user_ordered = 0
        users_started_in_month.each do |user|
          sum += @user_index[user][m] if  @user_index[user][m].present?
          user_ordered += 1 if @user_index[user][m].present?
        end
        monthly_order_per_user = user_ordered != 0 ? sum.to_f/user_ordered.to_f : 0
        stats.push(monthly_order_per_user.round(2))
      end
      return stats
    end
  end
  desc 'This task get the User retention & order per user retention cohorts'
  task cohorts: :environment do
      Cohort.init
  end
end
