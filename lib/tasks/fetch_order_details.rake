namespace :fetch do
  require 'csv'
  desc 'This task populate order_details (its product details, its user details, etc) for all the orders till date'
  task order_details: :environment do
    output_file = CSV.open('order_details.csv','w')
    order_stats_file = CSV.open('revenue_details.csv', 'w')

    @order_model = MarketplaceOrdersModule::V1::Order
    @order_product_service_instance = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService.new
    @order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
    @order_helper = MarketplaceOrdersModule::V1::OrderHelper
    @payment_dao = MarketplaceOrdersModule::V1::PaymentDao
    @payment_helper = MarketplaceOrdersModule::V1::PaymentHelper

    total_sp = BigDecimal('0')
    total_mrp = BigDecimal('0')
    total_savings = BigDecimal('0')

    #### HEADERS ###
    header = ['order_id', 'order_date', 'user_id', 'phone_number', 'name', 'cart_id', 'order_product_id', 'mrp', 'selling_price', 'additional_savings', 'final_price', 'savings', 'vat', 'service_tax', 'taxes', 'discount',
              'quantity', 'product_name', 'product_size', 'mpsp_id', 'Department', 'Category', 'Sub_Category', 'Brand_Codes', 'Brand_Pack_Quantity', 'Brand_Pack_Company']
    output_file << header

    stats_header = ['order_id', 'order_date', 'user_id', 'phone_number', 'name', 'pincode', 'total_mrp', 'cart_savings', 'cart_total',  'delivery_charges', 'billing_amount', 'grand_total',
      'discount', 'net_total', 'total_savings', 'payment_mode', 'payment_status']
    order_stats_file << stats_header

    puts 'Start fetching Order details..........'

    orders = @order_model.all.eager_load(:user, :payments, carts: :order_products).
        where.not(status: [@order_state::INITIATED[:value], @order_state::ORDER_CANCELLED[:value], @order_state::CANCELLED_BY_ADMIN[:value]])

    orders.each do |order|
      puts 'Processing Order: ' + order.id.to_s
      cart = order.cart
      user = order.user
      order_date = @order_helper.get_order_date(order)
      order_date = Time.at(order_date.to_i)
      order_products = @order_product_service_instance.get_active_order_products_in_carts(cart)
      order_products.each do |op|
        mpsp = op.marketplace_selling_pack
        mpsp_sub_category = mpsp.mpsp_sub_category
        mpsp_category = mpsp_sub_category.mpsp_parent_category
        mpsp_department = mpsp_category.mpsp_department
        mpbps = mpsp.marketplace_brand_packs
        mpsp_mpbps = mpsp.marketplace_selling_pack_marketplace_brand_packs

        bp_codes = ''
        bp_company = ''
        mpbps.each do |mpbp|
          bp_codes = bp_codes + mpbp.brand_pack.brand_pack_code.to_s + '/'
          bp_company = bp_company + mpbp.brand_pack.product.sub_brand.brand.company.name.to_s + '/'
        end
        bp_codes = bp_codes[0..-2]
        bp_company = bp_company[0..-2]


        bp_quantity = ''
        mpsp_mpbps.each do |mpsp_mpbp|
          bp_quantity = bp_quantity + mpsp_mpbp.quantity.to_s + '/'
        end
        bp_quantity = bp_quantity[0..-2]

        row = [order.id, order_date, user.id, user.phone_number, user.first_name, cart.id, op.id, op.mrp, op.selling_price, op.additional_discount, op.final_price, op.savings, op.vat, op.service_tax, op.taxes, op.discount, op.quantity,
          mpsp.display_name, mpsp.display_pack_size, mpsp.id, mpsp_department.label, mpsp_category.label, mpsp_sub_category.label, bp_codes, bp_quantity, bp_company]
        output_file << row
      end

      payment = @payment_dao.new.get_orders_payment_as_per_its_mode(order)
      payment_display_labels = @payment_helper.get_payment_display_labels(payment)

      order_area = order.address.area
      stats_row = [order.id, order_date, user.id, user.phone_number, user.first_name, order_area.pincode, cart.total_mrp, cart.cart_savings, cart.cart_total, payment.delivery_charges, payment.billing_amount, payment.grand_total,
        payment.discount, payment.net_total, payment.total_savings, payment_display_labels[:mode_display_name], payment_display_labels[:status_display_name] ]

      order_stats_file << stats_row
    end
    puts 'Total orders count: ' + orders.count.to_s
    puts 'Details fetched successfully.........'
    output_file.close
    order_stats_file.close
  end

  desc 'This task populate order_vat_details (vat info per order wise) for all the orders till date'
  task order_vat_details: :environment do
    output_file = CSV.open('order_vat_details.csv','w')

    @order_model = MarketplaceOrdersModule::V1::Order
    @order_product_service_instance = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService.new
    @order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
    @order_helper = MarketplaceOrdersModule::V1::OrderHelper
    @payment_dao = MarketplaceOrdersModule::V1::PaymentDao
    @payment_helper = MarketplaceOrdersModule::V1::PaymentHelper
    @gateway_type = TransactionsModule::V1::GatewayType
    @payment_type = MarketplaceOrdersModule::V1::PaymentType

    total_sp = BigDecimal('0')
    total_mrp = BigDecimal('0')
    total_savings = BigDecimal('0')

    #### HEADERS ###
    stats_header = ['order_id', 'bridge link', 'order_date', 'order_status', 'phone_number', 'total products in cart', 'total_mrp', 'cart_total',  'delivery_charges', 'grand_total',
      'discount', 'billing_amount', 'ample_credits', 'net_total', 'total_savings', 'final payment_mode', 'final gateway', 'final payment_status',
      'citrus', 'razorpay', 'mswipe', 'ezetap', 'cash', 'pending',
      'vat 0', 'vat 5', 'vat 5.5', 'vat 12.5', 'vat 14.5', 'vat 25', 'total vat']
    output_file << stats_header

    puts 'Start fetching Order details..........'

    orders = @order_model.all.eager_load(:user, :payments, carts: :order_products).
        where.not(status: [@order_state::INITIATED[:value], @order_state::ORDER_CANCELLED[:value], @order_state::CANCELLED_BY_ADMIN[:value]])

    orders.each do |order|
      puts 'Processing Order: ' + order.id.to_s
      cart = order.cart
      user = order.user
      order_date = @order_helper.get_order_date(order)
      order_date = Time.at(order_date.to_i).to_date.to_s
      order_products = @order_product_service_instance.get_active_order_products_in_carts(cart)
      total_order_products = order_products.count

      vat_0 = vat_5 = vat_55 = vat_125 = vat_145 = vat_25 = total_vat_amount = BigDecimal.new('0.0')
      order_products.each do |op|

        vat_percentage = op.vat.to_f

        if vat_percentage < 1
          vat_percentage = (op.vat * 100).to_f
        end

        sp = op.final_price
        vat_amount = sp * (vat_percentage / (100 + vat_percentage))
        vat_amount = vat_amount.to_f

        case vat_percentage.to_f
        when 0
          vat_0 += vat_amount
        when 5
          vat_5 += vat_amount
        when 5.5
          vat_55 += vat_amount
        when 12.5
          vat_125 += vat_amount
        when 14.5
          vat_145 += vat_amount
        when 25
          vat_25 += vat_amount
        else
          raise 'vat percentage mismatch for ' + op.id.to_s + ' and vat: ' + vat_percentage.to_s
        end

      end

      total_vat_amount = vat_0 + vat_5 + vat_55 + vat_125 + vat_145 + vat_25

      payment = @payment_dao.new.get_orders_payment_as_per_its_mode(order)
      payment_display_labels = @payment_helper.get_payment_display_labels(payment)
      order_state = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates.get_order_state_by_value(order.status)
      bridge_link = 'http://bridge.buyample.com/orders/marketplace-orders/' + order.order_id

      transaction_details = TransactionsModule::V1::TransactionHelper.get_transaction_details_for_payment(payment)
      display_gateway = transaction_details[:display_gateway]

      payments = @payment_dao.new.get_active_payments_for_order(order)
      citrus = razorpay = mswipe = ezetap = cash = pending = BigDecimal.new('0')

      payments.each do |p|
        amount = get_amount_as_per_type(p)
        transaction_details = TransactionsModule::V1::TransactionHelper.get_transaction_details_for_payment(p)
        gateway = transaction_details[:gateway]

        case gateway.to_i
        when @gateway_type::CITRUS_WEB
          citrus += amount
        when @gateway_type::RAZORPAY
          razorpay += amount
        when @gateway_type::M_SWIPE
          mswipe += amount
        when @gateway_type::EZETAP
          ezetap += amount
        when @gateway_type::CASH
          cash += amount
        else
          pending += amount
        end
      end

      stats_row = [order.id, bridge_link, order_date, order_state[:label], user.phone_number, total_order_products, cart.total_mrp, cart.cart_total, payment.delivery_charges, payment.grand_total,
        payment.discount, payment.billing_amount, payment.ample_credit_amount, payment.net_total, payment.total_savings, payment_display_labels[:mode_display_name], display_gateway, payment_display_labels[:status_display_name],
        citrus.round(2), razorpay.round(2), mswipe.round(2), ezetap.round(2), cash.round(2), pending.round(2),
        vat_0.round(2), vat_5.round(2), vat_55.round(2), vat_125.round(2), vat_145.round(2), vat_25.round(2), total_vat_amount.round(2) ]

      output_file << stats_row
    end
    puts 'Total orders count: ' + orders.count.to_s
    puts 'Vat Details fetched successfully.........'
    output_file.close
  end

  def self.get_amount_as_per_type(payment)
    amount = 0
    if payment.payment_type == @payment_type.get_id_by_key('PAY')
      amount = payment.payable_total
    else
      amount = -1 * payment.payable_total
    end
    return amount
  end

end
