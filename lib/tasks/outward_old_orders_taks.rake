#Commenting as not in use any more
# 
# namespace :outward do

#   module OutwardOldOrders

#     def self.init
#       initialize_services
#       outward_old_orders
#     end

#     def self.initialize_services
#       @order = MarketplaceOrdersModule::V1::Order
#       @order_panel_service = MarketplaceOrdersModule::V1::OrderPanelService.new
#       @order_states = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
#     end

#     def self.outward_old_orders
#       time_of_deployment = Time.zone.now.change(month: 02, day: 06, hour: 13)
#       not_allowed_states = [
#         @order_states::INITIATED[:value],
#         @order_states::CANCELLED_BY_ADMIN[:value],
#         @order_states::ORDER_CANCELLED[:value]
#       ]
#       orders = @order.where("order_date < ?", time_of_deployment).where.not(status: not_allowed_states)
#       if orders.present?
#         orders.each do |order|
#           @order_panel_service.out_warehouse_brand_packs(order)
#         end
#       end        
#     end
#   end

#   desc 'Rake task to outward products from start of Ample to deployment day 6th feb'
#   task :old_orders => :environment do
#     OutwardOldOrders.init
#     puts "#{Time.zone.now} - Success!"
#   end
# end
