namespace :update do
  desc "This tasks update the pincode data"
  task :pincode_data => :environment do
    area_model = AddressModule::V1::Area
    active_state = AddressModule::V1::ModelStates::V1::AreaStates::ACTIVE
    inactive_state = AddressModule::V1::ModelStates::V1::AreaStates::INACTIVE

    areas = area_model.all

    areas.each do |area|
      area.name = 'dummy' # as presence is true can't set it empty string
      area.save
    end

    STDOUT.puts 'please enter full path of csv'
    csv_file_path = STDIN.gets.strip

    count = 0

    CSV.foreach(csv_file_path) do |row|
      if count == 0
        count = 1
        next
      end

      pincode = row[1]
      next if pincode.blank?
      area = area_model.find_by_pincode(pincode)
      if area.nil?
        populate_pincode(row)
        area = area_model.find_by_pincode(pincode)
      end

      if area.name == 'dummy'
        area.name = row[13].strip
      else
        area.name = area.name + '/' + row[13].strip
      end

      if row[3] == '1'
        area.status = active_state
      else
        area.status = inactive_state
      end

      puts 'Failed to save area with pincode ' + pincode.to_s if area.save == false
    end

    pincode_index_service = SearchModule::V1::PincodeIndexService.new
    pincode_index_service.initialize_index('true')

  end

  def populate_pincode(row)
    active_state = AddressModule::V1::ModelStates::V1::AreaStates::ACTIVE
    inactive_state = AddressModule::V1::ModelStates::V1::AreaStates::INACTIVE
    request = {}
    request[:pincode] = row[1]
    request[:area] = 'dummy'
    request[:city] = row[5]
    request[:state] = row[6]
    request[:country] = 'India'
    if row[3] == '1'
      request[:status] = active_state
    else
      request[:status] = inactive_state
    end

    area_service = AddressModule::V1::AreaService.new
    return area_service.create_area(request)
  end
end
