namespace :populate do
  module PurchaseOrder
    require 'csv'
    def self.init
      puts 'please enter full path of csv of Bill details 1'
      bill_details_1 = STDIN.gets.strip
      puts 'please enter full path of csv of Bill details 2'
      bill_details_2 = STDIN.gets.strip
      puts 'please enter your phone number'
      phone_number = STDIN.gets.strip
      count = 0
      @field_map = {}

      self.initialize_classes

      puts 'Loading table....................................................'
      @bill_one={}
      user = @user_model.where(phone_number: phone_number).first
      return if user.blank?
      USER_SESSION[:user_id] = user.id
      CSV.foreach(bill_details_1) do |row_1|
        if count == 0
          self.map_bill_details_1_fields(row_1)
          count = 1
          next
        end 
        if row_1[@bill_one[:po_id]].blank? || row_1[@bill_one[:vendor_code]].blank?
          next
        end
        begin
          inventory = @distributor_dao.get_distributor_by_vendor_code(row_1[@bill_one[:vendor_code]]).inventory
        rescue @custom_error_util::ResourceNotFoundError
          next
        end
        warehouse = @warehouse_model.first
        billing_address = warehouse.warehouse_addresses.first
        shipping_address = warehouse.warehouse_addresses.last
        #
        # Create Inventory Order
        #
        inventory_order = @inventory_order_service.transactional_create_purchase_order({
          warehouse: {
            id: warehouse.id
          },
          inventory: {
            id: inventory.id
          },
          address: {
            shipping_address: {
              id: shipping_address.id
            },
            billing_address: {
              id: billing_address.id
            }
          }
        })
        procurement_id = row_1[@bill_one[:po_id]]
        products_array = []
        #
        # Add products to Purchase Order
        #
        @bill_two = {}
        count = 0
        CSV.foreach(bill_details_2) do |row_2|
          if count == 0
            self.map_bill_details_2_fields(row_2)
            count = 1
            next
          end
          if row_2[@bill_two[:po_id]].blank? || row_2[@bill_two[:bp_code]].blank?
            next
          end
          if procurement_id == row_2[@bill_two[:po_id]]
            begin
              brand_pack = @brand_pack_dao.get_brand_pack_by_code(row_2[@bill_two[:bp_code]])
              ibp = @ibp_dao.ibp_of_brand_pack_for_a_vendor(inventory, brand_pack)
            rescue @custom_error_util::ResourceNotFoundError
              next
            end
            quantity = row_2[@bill_two[:quantity]].gsub(',','')
            products_array.push({
              inventory_brand_pack_id: ibp.id,
              quantity_ordered: quantity
            })
          end
        end
        inventory_order = @inventory_order_service.transactional_update_purchase_order({
          id: inventory_order.id,
          is_inter_state: false,
          warehouse: {
            id: warehouse.id
          },
          inventory: {
            id: inventory.id
          },
          address: {
            shipping_address: {
              id: shipping_address.id
            },
            billing_address: {
              id: billing_address.id
            }
          },
          products: products_array
        })
        #
        # Place Purchase order
        #
        inventory_order = @inventory_order_service.transactional_place_purchase_order({
          id: inventory_order.id
        })
        #
        # Receive Purchase Order
        #
        inventory_order = @inventory_order_service.change_purchase_order_state({
          id: inventory_order.id,
          event: @order_event::RECEIVE_PURCHASE_ORDER
        })
        products_array = []
        #
        # Verify Purchase Order
        #
        @bill_two={}
        count = 0
        CSV.foreach(bill_details_2) do |row_2|
          if count == 0
            self.map_bill_details_2_fields(row_2)
            count = 1
            next
          end
          if row_2[@bill_two[:po_id]].blank? || row_2[@bill_two[:bp_code]].blank?
            next
          end
          if procurement_id == row_2[@bill_two[:po_id]]
            begin
              brand_pack = @brand_pack_dao.get_brand_pack_by_code(row_2[@bill_two[:bp_code]])
              ibp = @ibp_dao.ibp_of_brand_pack_for_a_vendor(inventory, brand_pack)
              iop = inventory_order.inventory_order_products.where(inventory_brand_pack_id: ibp.id).first
            rescue @custom_error_util::ResourceNotFoundError
              next
            end
            quantity = row_2[@bill_two[:quantity]].gsub(',','')
            cost_price = row_2[@bill_two[:cost_price]] || "0.0"
            products_array.push({
              id: iop.id,
              quantity_received: quantity,
              mrp: row_2[@bill_two[:mrp]].gsub(',',''),
              cost_price: cost_price.gsub(',','')
            })
          end
        end
        inventory_order = @inventory_order_service.transactional_verify_purchase_order({
          id: inventory_order.id,
          bill_no: row_1[@bill_one[:bill_no]],
          payment: {
            total_octrai_value: '0.0'
          },
          products: products_array
        })
        #
        # Stock Purchase Order
        #
        inventory_order = @inventory_order_service.transactional_stock_purchase_order({
          id: inventory_order.id,
          warehouse: {
            id: warehouse.id
          }
        })
      end

      puts 'Success............................................'
    end

    #
    # Initializing classes
    #
    def self.initialize_classes
      params = {version: 1}
      @distributor_dao = SupplyChainModule::V1::DistributorDao.new
      @warehouse_model = SupplyChainModule::V1::Warehouse
      @inventory_order_service = InventoryOrdersModule::V1::InventoryOrderService.new
      @brand_pack_dao = MasterProductModule::V1::BrandPackDao.new
      @ibp_dao = InventoryProductModule::V1::InventoryBrandPackDao.new
      @order_event = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderEvents
      @user_model = UsersModule::V1::User
      @custom_error_util = CommonModule::V1::CustomErrors
    end

    #
    # This function map details from bill details 1 csv
    #
    def self.map_bill_details_1_fields(row)
      index = 0
      row.each do |col|
        if col.present?
          case col.downcase
          when 'procurement_id'
            @bill_one[:po_id] = index
          when 'vendor_id'
            @bill_one[:vendor_code] = index
          when 'bill_no'
            @bill_one[:bill_no] = index
          end
          index += 1
        end
      end
    end

    #
    # This function map details from bill details 2 csv
    #
    def self.map_bill_details_2_fields(row)
      index = 0
      row.each do |col|
        if col.present?
          case col.downcase
          when 'procurement_id'
            @bill_two[:po_id] = index
          when 'bp_code'
            @bill_two[:bp_code] = index
          when 'quantity_in_bill'
            @bill_two[:quantity] = index
          when 'mrp'
            @bill_two[:mrp] = index
          when 'price_excluding_tax'
            @bill_two[:cost_price] = index
          end
          index += 1
        end
      end
    end
  end
  desc 'This task creates PO from the CSV file and stocks it'
  task purchase_order: :environment do
      PurchaseOrder.init
      USER_SESSION = {}
  end
end
