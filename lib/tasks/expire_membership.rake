namespace :expire do

  module ExpireMembership

    def self.init
      membership_dao = UsersModule::V1::MembershipDao.new
      membership_service = UsersModule::V1::MembershipService.new
      membership_model = UsersModule::V1::Membership.new
      active_memberships = membership_dao.get_all_active_memberships
      active_memberships.each do |membership|
        user = membership.users.first
        order_dao = MarketplaceOrdersModule::V1::OrderDao.new
        user_order_count = order_dao.get_exact_order_numbers_of_user(user)
        membership_expire_order_count = CommonModule::V1::Cache.read_int('MEMBERSHIP_EXPIRE_ORDER_COUNT')
        if user_order_count >= membership_expire_order_count
          membership_model.update_membership(membership, { expires_at: (Time.zone.now + 3.days) })
        end
      end
    end
  end

  desc 'Rake task to expire membership of Users who have placed 3 or more orders already'
  task :membership => :environment do
    puts "Started"
    ExpireMembership.init
    puts "#{Time.zone.now} - Success!"
  end
end
