namespace :trigger do
  module UpdateStock
    require 'csv'
    def self.init
      STDOUT.puts 'please enter full path of csv'
      csv_file_path = STDIN.gets.strip
      
      count = 0
      @field_map = {}

      self.initialize_classes
      self.initialize_constants

      puts 'Loading table....................................................'
      warehouse = @warehouse_model.all.first
      CSV.foreach(csv_file_path) do |row|
        if count == 0
          self.map_brand_pack_fields(row)
          count = 1
          next
        end 
        next if row[@field_map[:bp_code]].blank?
        begin
          brand_pack = @brand_pack_dao.get_brand_pack_by_code(row[@field_map[:bp_code]])
        rescue @custom_error_util::ResourceNotFoundError
          next
        end
        if row[@field_map[:threshold]].present?
          update_wbp(brand_pack, warehouse, row[@field_map[:threshold]])
        end
      end

      puts 'Success............................................'
    end

    #
    # Function to update WBP with stock threshold and outboudn freq
    #
    def self.update_wbp(brand_pack, warehouse, stock_threshold)
      wbp = @warehouse_brand_pack_dao.get_wbp_by_parameters(brand_pack, warehouse)
      if wbp.present?
        @warehouse_brand_pack_dao.update({
          wbp: {
            threshold_stock_value: stock_threshold,
            outbound_frequency_period_in_days: @outbound_frequency_period_in_days
          }
        }, wbp)
      end
    end

    #
    # Initializing classes
    #
    def self.initialize_classes
      params = {version: 1}
      @brand_pack_dao = MasterProductModule::V1::BrandPackDao.new
      @warehouse_brand_pack_dao = WarehouseProductModule::V1::WarehouseBrandPackDao.new
      @warehouse_model = SupplyChainModule::V1::Warehouse
      @custom_error_util = CommonModule::V1::CustomErrors
    end

    #
    # Initializing constants
    #
    def self.initialize_constants
      @outbound_frequency_period_in_days = 30
    end

    #
    # This function map master data fields from the CSV
    #
    def self.map_brand_pack_fields(row)
      index = 0
      row.each do |col|
        next if col.blank?
        case col.downcase
        when 'bp_code'
          @field_map[:bp_code] = index
        when 'threshold'
          @field_map[:threshold] = index
        end
        index += 1
      end
    end
  end
  desc 'This task populates the distributor pack code for each brand pack'
  task stock_threshold: :environment do
      UpdateStock.init
  end
end
