namespace :create do
  desc 'Creates the controller'
  task all_apis: :environment do

    STDOUT.puts 'Enter engine name like SupplyChain, Location (without suffix Engine)'
    @engine_name = STDIN.gets.strip

    STDOUT.puts 'Enter module name like MarketplaceOrders, MasterProducts (without suffix Module)'
    @module_name = STDIN.gets.strip

    STDOUT.puts 'Enter controller name like BrandPack, OrderProduct'
    @controller_name = STDIN.gets.strip

    @output_folder = 'generated_apis'
    FileUtils.mkdir_p(@output_folder)

    @controller_variable_name = @controller_name.underscore.upcase

    @module_name_with_underscore = @module_name.underscore.upcase
    @module_name_with_underscore_and_downcase = @module_name_with_underscore.downcase

    @engine_name_with_underscore_and_downcase = @engine_name.underscore.downcase 

    create_routes
    create_controller
    create_apis
    create_permissions
    create_service
    create_dao
    create_model
    create_mapper
    create_decorator
  end # End of all_directories task

  def create_routes
    file_name = @controller_variable_name.downcase.pluralize + '_routes.rb'
    create_file('routes_template.rb', file_name)
  end

  def create_controller
    file_name = @controller_variable_name.downcase.pluralize + '_controller.rb'
    create_file('controller_template.rb', file_name)
  end

  def create_apis
    create_add_api
    create_update_api
    create_change_state_api
    create_get_api
    create_get_all_api
  end

  def create_permissions
    file_name = @controller_variable_name.downcase.pluralize + '_permissions.yml'
    create_file('permissions_template.yml', file_name)
  end

  def create_service
    file_name = @controller_variable_name.downcase + '_service.rb'
    create_file('service_template.rb', file_name)
  end

  def create_dao
    file_name = @controller_variable_name.downcase + '_dao.rb'
    create_file('dao_template.rb', file_name)
  end

  def create_model
    file_name = @controller_variable_name.downcase + '.rb'
    create_file('model_template.rb', file_name)
  end

  def create_mapper
    file_name = @controller_variable_name.downcase + '_mapper.rb'
    create_file('mapper_template.rb', file_name)
  end

  def create_decorator
    file_name = @controller_variable_name.downcase + '_response_decorator.rb'
    create_file('decorator_template.rb', file_name)
  end

  def create_add_api
    file_name = 'add_' + @controller_variable_name.downcase + '_api.rb'
    create_file('add_api_template.rb', file_name)
  end

  def create_update_api
    file_name = 'update_' + @controller_variable_name.downcase + '_api.rb'
    create_file('update_api_template.rb', file_name)
  end

  def create_change_state_api
    file_name = 'change_' + @controller_variable_name.downcase + '_state_api.rb'
    create_file('change_state_api_template.rb', file_name)
  end

  def create_get_api
    file_name = 'get_' + @controller_variable_name.downcase + '_api.rb'
    create_file('get_api_template.rb', file_name)
  end

  def create_get_all_api
    file_name = 'get_all_' + @controller_variable_name.downcase + '_api.rb'
     create_file('get_all_api_template.rb', file_name)
  end

  def create_file(template_file, file_name)

    file_template = File.new(File.join(Rails.root, "templates", template_file), "r")

    caps_controller_name = @controller_name.upcase

    file_text = file_template.read

    file_text = file_text.gsub('MODULENAME', @module_name)
    file_text = file_text.gsub('CAPSCONTROLLERNAMES', @controller_variable_name.pluralize.upcase)
    file_text = file_text.gsub('CAPSCONTROLLERNAME', @controller_variable_name)
    file_text = file_text.gsub('small_controller_names', @controller_variable_name.downcase.pluralize)
    file_text = file_text.gsub('small_controller_name', @controller_variable_name.downcase)
    file_text = file_text.gsub('CONTROLLERNAMEPLURAL', @controller_name.pluralize)
    file_text = file_text.gsub('CONTROLLERNAME', @controller_name)
    file_text = file_text.gsub('MODULE_NAME', @module_name_with_underscore)
    file_text = file_text.gsub('small_module_name', @module_name_with_underscore_and_downcase)
    file_text = file_text.gsub('ENGINENAME', @engine_name)
    api_file = File.new(File.join(Rails.root,@output_folder ,file_name), 'w')
    api_file.write(file_text)
  end

  #####################################################
  #     TASK: Create all relevant directories         #
  #####################################################

  desc 'Creates the required directory as per x_engine/app/**/y_module/version/ '
  task all_directories: :environment do

    STDOUT.puts 'Enter engine name like SupplyChain, Location'
    @engine_name = STDIN.gets.strip

    STDOUT.puts 'Enter module name like MarketplaceOrders, MasterProducts (without module suffix)'
    @module_name = STDIN.gets.strip

    STDOUT.puts 'Enter version name like v1, v2'
    @version_number = STDIN.gets.strip

    @engine_name_with_underscore = @engine_name.underscore.downcase + '_engine'
    @module_name_with_underscore = @module_name.underscore.downcase + '_module'
    @version_number = @version_number.downcase

    @output_folder = Rails.root.to_s + '/engines/' + @engine_name_with_underscore + '/app'
    @module_folder = @module_name_with_underscore + '/' + @version_number

    @api_directory = @output_folder + '/api/' + @module_folder + '/'
    @controllers_directory = @output_folder + '/controllers/' + @module_folder + '/'
    @services_directory = @output_folder + '/services/' + @module_folder + '/'
    @dao_directory = @output_folder + '/data_access_object/' + @module_folder + '/'
    @decorator_directory = @output_folder + '/decorators/' + @module_folder + '/'
    @model_directory = @output_folder + '/models/' + @module_folder + '/'
    @helper_directory = @output_folder + '/helpers/' + @module_folder + '/'

    STDOUT.puts 'Following directories will be generated'
    STDOUT.puts '*******************************************'
    STDOUT.puts @api_directory
    STDOUT.puts @controllers_directory
    STDOUT.puts @services_directory
    STDOUT.puts @dao_directory
    STDOUT.puts @decorator_directory
    STDOUT.puts @model_directory
    STDOUT.puts @helper_directory
    STDOUT.puts '*******************************************'
    STDOUT.puts 'Do you want to proceed? Yes/No'
    @proceed = STDIN.gets.strip
    if @proceed.upcase == 'YES'
      puts 'Processing your request...'
      FileUtils.mkdir_p(@api_directory)
      FileUtils.mkdir_p(@controllers_directory)
      FileUtils.mkdir_p(@services_directory)
      FileUtils.mkdir_p(@dao_directory)
      FileUtils.mkdir_p(@decorator_directory)
      FileUtils.mkdir_p(@model_directory)
      FileUtils.mkdir_p(@helper_directory)
      puts 'Request completed :-) '
    elsif @proceed.upcase == 'NO'
      puts 'Your request is canceled'
    else
      puts 'Invalid key'
    end
  end # End of all_directories task

end
