namespace :fetch do
  desc 'This task fetch consul settings from Consul server as per the environment and then store it
  in respective redis store'
  
  module ConsulSettingsModule
    def self.init
      puts '********************************************'
      puts '********** LOADING CONSTANTS ***************'
      puts '********************************************'
      fetch_constants_and_store_individually_in_redis

      puts '********************************************'
      puts '********** LOADING SETTINGS ****************'
      puts '********************************************'
      fetch_settings_and_store_in_redis

    end

    def self.fetch_settings_and_store_in_redis
      # Fetch FEATURES value from Consul and store it in redis
      SettingsModule::V1::SettingsUtil.get_and_locally_store_complete_settings
    end

    def self.fetch_constants_and_store_individually_in_redis
      # Put ALL common constants in redis cache, irrespective of current environment
      # common constants can be find in config/initializers/constants.yml file
      # under key 'all'
      static_files = [
        { consul_path: '/backend/imli-backend/default/default/CONSTANTS', file_path: "config/initializers/users_module/constants.yml" },
        { consul_path: '/backend/imli-backend/default/default/EMAIL_CONSTANTS' , file_path: "config/initializers/communications_module/email_constants.yml" }
      ]
      static_files.each do |static_file|
        puts '--------------------------------------------'
        file_hash = DistributedStoreModule::V1::DistributedStoreUtil.fetch_environment_specific_config_from_distributed_store(static_file[:consul_path])
        if file_hash.blank?
          file_path = static_file[:file_path]
          if File.file?(file_path)
            file_hash = YAML.load_file(file_path)[Rails.env]
            puts '[SUCCESS]: YML - Config fetched from local YML file successfuly'
          end
        end
        write_individual_values_to_redis(file_hash)
      end
    end

    def write_individual_values_to_redis(hash)
      return {} if hash.blank?
      hash.each do |key, value|
        write_key_value_to_redis(key, value)
      end
    end

    #
    # Write Key Value pair to Redis
    #
    # @param key [String] [Key for which value is to be stored]
    # @param value [Object] [Value to be stored]
    #
    def write_key_value_to_redis(key, value)
      CommonModule::V1::Cache.write({
        key: key.to_s,
        value: value.to_s
        })
    end


  end

  task consul_settings: :environment do
    ConsulSettingsModule.init
  end
end