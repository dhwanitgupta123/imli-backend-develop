namespace :search_module do
  namespace :initialize do
    desc 'Initialize the user index this will overrite the existing index'
    task user_index: :environment do
      user_index_service = SearchModule::V1::UserDetailsIndexService
      user_index_service.initialize_index(true)
    end

  end
end
