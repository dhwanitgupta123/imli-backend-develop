namespace :populate do
  desc 'populate area data'
  task area_data: :environment do
    populate_table_path = File.join(LocationEngine::Engine.root, 'scripts/populate_table.rb')
    require populate_table_path

    puts 'Loading table ........................................................'
    populate_table = PopulateTable.new
    populate_table.populate_area_from_csv
    puts 'Successful ............................................................'
    pincode_index_service = SearchModule::V1::PincodeIndexService.new
    pincode_index_service.initialize_index('true')
  end
end
