namespace :fetch do
  require 'csv'
  desc 'This task populate referral data'
  task referral_data: :environment do
    puts 'Start fetching Order details..........'

    file = CSV.open('referral_data.csv', 'w')

    file << [ 'id', 'phone', 'name', 'first_order_id', 'applied_credits_in_first_order', 'first_order_date',
              'no_of_completed_orders' , 'no_of_orders_in_which_referral_applied', 'total_credits_applied' ]

    users = UsersModule::V1::User.all

    payment_dao = MarketplaceOrdersModule::V1::PaymentDao.new({})

    users.each do |user|
      next if user.orders.blank?
      completed_orders = MarketplaceOrdersModule::V1::OrderDao.get_completed_orders(user.orders).order('order_date')
      next if completed_orders.blank?
      first_order = completed_orders[0]
      payment = payment_dao.get_last_successful_payment(first_order)
      if payment.blank?
        payment = payment_dao.get_pending_payment_for_order(first_order)
      end
      in_first_order_referral_applied = payment.ample_credit_amount
      total_order_in_which_credit_applied = 0
      total_credits = 0
      completed_orders.each do |order|
        payment = payment_dao.get_last_successful_payment(order)
        if payment.blank?
          payment = payment_dao.get_pending_payment_for_order(order)
        end
        total_order_in_which_credit_applied += 1 if payment.ample_credit_amount > 0
        total_credits += payment.ample_credit_amount
      end
      file << [user.id, user.phone_number, user.first_name, first_order.order_id, in_first_order_referral_applied,
               first_order.order_date, completed_orders.count, total_order_in_which_credit_applied, total_credits]
    end
    file.close
    puts 'Details fetched successfully......... check public/referral_data.csv'
  end
end
