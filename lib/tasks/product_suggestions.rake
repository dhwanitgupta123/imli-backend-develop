namespace :product do

  module ProductSuggestions

    def self.init

      puts 'List of all the products suggested by Users, started at:' + Time.zone.now.to_s
      initialize_services
      send_product_suggestion_mail
    end

    def self.initialize_services
      @product_suggestion_service = CommonModule::V1::ProductSuggesterService.new({})
      @cache_util = CommonModule::V1::Cache
    end

    def self.send_product_suggestion_mail
      response = @product_suggestion_service.send_last_x_hours_product_suggestion_mail(@cache_util.read('DEFAULT_TIME_RANGE').to_i)
      puts response
    end
  end

  desc 'Rake task to send mail of llist of products suggested by user'
  task :suggestions => :environment do
    ProductSuggestions.init
    puts "#{Time.zone.now} - Success!"
  end
end
