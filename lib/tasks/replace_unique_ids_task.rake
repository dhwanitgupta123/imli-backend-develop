namespace :replace do
  require 'csv'
  desc 'This task take input csv file and unique ids file and replace unique ids values in csv file 
       it is just replacing data of unique_ids_path in master_data_path unique_ids column'
  task :unique_ids, [:master_data_path, :unique_ids_path] do |t, args|
    unique_ids = CSV.open(args[:unique_ids_path])
    output_file = CSV.open('master_data_with_unique_ids.csv','w')

    count = 0
    unique_id_column = 0

    CSV.foreach(args[:master_data_path]) do |row|
      if count == 0
        unique_id_column = row.index('Unique_ids')
        count = 1
        output_file << row
        next
      end
      unique_id = unique_ids.readline

      row[unique_id_column] = unique_id[0] if unique_id.present?
      output_file << row
    end

    output_file.close
    unique_ids.close

    # Ensure all other data is same as before
    after_file = CSV.open('master_data_with_unique_ids.csv','r')
    CSV.foreach(args[:master_data_path]) do |before_row|
      
      after_row = after_file.readline
      break unless after_row.present?

      after_row.delete_at(unique_id_column)
      before_row.delete_at(unique_id_column)
      if before_row != after_row
        puts 'FAILED for row ' + before_row.to_s
        break
      end
    end
  end
end
