#
#  COMMENTING THIS TASK AS THIS IS NOT IN USE
# 

# namespace :update do
#   module UpdateDepartmentCategory
#     def self.init
#       require 'csv'
#       STDOUT.puts 'please enter full path of csv'
#       csv_file_path = STDIN.gets.strip

#       STDOUT.puts 'please enter base path for images'
#       @base_image_path = STDIN.gets.strip

#       initialize_models
#       initialize_services

#       count = 0
#       @field_map = {}

#       CSV.foreach(csv_file_path) do |row|
#         if count == 0
#           map_master_data_fields(row)
#           count = 1
#           next
#         end

#         break if row[@field_map[:department_name]].blank?

#         department = update_department_priority(row)
#         department.status = 1
#         department.save
#         category = update_category_priority(row, department.id)
#         update_sub_category_priority(row, category.id)
#       end
#     end

#     def self.initialize_models
#       @category_model = CategorizationModule::V1::Category
#       @department_model = CategorizationModule::V1::Department
#     end

#     def self.initialize_services
#       @department_service = CategorizationModule::V1::DepartmentService.new({})
#     end

#     def self.get_department_args(row)
#       {
#         label: row[@field_map[:department_name]],
#         priority: row[@field_map[:department_priority]].to_i,
#         status: 1,
#         images: upload_image_locally(row[@field_map[:department_id]])
#       }
#     end

#     def self.update_department_priority(row)
#       department = @department_model.get_department_by_label(row[@field_map[:department_name]])
#       department_args = get_department_args(row)
#       if department.nil?
#         department = @department_service.create_department_with_details(department_args)
#         return department
#       end
#       return @department_service.update_department_details(department_args.merge(id: department.id))
#     end

#     def self.update_category_priority(row, department_id)
#       category = @category_model.get_category_by_label_and_department(row[@field_map[:category_name]], department_id)
#       if category.nil?
#         category = CategorizationModule::V1::Category.new
#         category.label = row[@field_map[:category_name]]
#         category.department_id = department_id
#       end
#       category.priority = row[@field_map[:category_priority]].to_i
#       category.status = 1
#       category.save!
#       return category
#     end

#     def self.update_sub_category_priority(row, category_id)
#       sub_category = @category_model.get_sub_category_by_label_and_category_id(row[@field_map[:sub_category_name]], category_id)
#       if sub_category.nil?
#         sub_category = CategorizationModule::V1::Category.new
#         sub_category.label = row[@field_map[:sub_category_name]]
#         sub_category.parent_category_id = category_id
#       end
#       sub_category.priority = row[@field_map[:sub_category_priority]].to_i
#       sub_category.status = 1
#       sub_category.save!
#     end

#     def self.map_master_data_fields(row)
#       index = 0
#       row.each do |col|
#         case col.downcase
#         when 'department_order'
#           @field_map[:department_priority] = index
#         when 'department'
#           @field_map[:department_name] = index
#         when 'category'
#           @field_map[:category_name] = index
#         when 'category_order'
#           @field_map[:category_priority] = index
#         when 'sub-category_order'
#           @field_map[:sub_category_priority] = index
#         when 'sub-category'
#           @field_map[:sub_category_name] = index
#         when 'department_id'
#           @field_map[:department_id] = index
#         end
#         index += 1
#       end
#     end

#     def self.upload_image_locally(image_id)
      
#       absolute_path = @base_image_path + '/' + image_id

#       unless File.directory?(absolute_path)
#         return []
#       end

#       image_service = ImageServiceModule::V1::ImageService.new
#       return image_service.upload_images_from_directory(absolute_path)
#     end
#   end

#   desc 'This task creates the category, sub category and department and update it if already preseny
#         ASSUMING unique keys are:
#         1. For department department_name is unique
#         2. For category category_name and department id is unique
#         3. For sub category sub_category_name and category id is unique
#       '
#   task departmer_category: :environment do
#     UpdateDepartmentCategory.init
#   end
# end
