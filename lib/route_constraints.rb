# Constraint for routing on both routes.rb and module level routes
# @author Sumit <sumitchhuttani007@gmail.com>
class RouteConstraints

  # 
  # Initialize parameters to class level scope
  # @param options [Hash] [Hash containing version, predicate and default]
  # 
  def initialize(options)
    @version = options[:version]
    @default = options[:default]
    @predicate = options[:predicate] || method(:dummy_predicate)
  end

  # 
  # Matches the initialized version parameter with passed
  # args based upon predicate (initialized before)
  # @param args [Object] [Any kind of Object can be passed here]
  # 
  # @return [Boolean] [true if default is true or predicate returns true]
  def matches?(args)
    @default || @predicate.call(@version, args)
  end

  # 
  # A default predicate to maintain consistency
  # @param a [Object] [Anything]
  # @param b [Object] [Anything]
  # 
  # @return [Boolean] [Always return false]
  def dummy_predicate(a,b)
    false
  end
end