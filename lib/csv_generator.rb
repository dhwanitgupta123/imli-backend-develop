require 'csv'
class CsvGenerator

  BASE_FILE_PATH = "#{Rails.root}/public/"

  def initialize(file_name, options = {})
    @file_name = file_name + '.csv'
    @mode = options[:mode] || 'w'
    root_path = options[:root_path] || BASE_FILE_PATH
    @csv_path = root_path + @file_name
    open_csv
  end

  #
  # Set headers for CSV
  #
  # @param headers_array = [] [Array] [Array of strings]
  #
  def set_headers(headers_array = [])
    @csv << headers_array
  end

  #
  # Push individual row to the csv
  #
  # @param row = [] [Array] [Array of values to be inserted in csv]
  #
  def push_row(row = [])
    @csv << row
  end

  #
  # Generate CSV and return it with its attributes
  #
  # @return [Hash] [Hash containing file path, file name and csv]
  #
  def generate
    return {} if @csv.blank?

    close_csv
    return {file_path: @csv_path, filename: @file_name, csv: @csv}
  end

  private
  def open_csv
    begin
      @csv = CSV.open(@csv_path, @mode)
    rescue => e
      CommonModule::V1::Logger.log_exception(e)
      raise e
    end
  end

  def close_csv
    begin
      @csv.close
    rescue => e
      CommonModule::V1::Logger.log_exception(e)
      raise e
    end
  end

end
