module ImliWeblab
  class << self

    #
    # Get value for the feature corresponds to given key
    # It is dynamic experiment value whose value will be either '1' or '0'.
    # Value will be determined by user's experiment algorithm and other filters
    # associated with that experiment.
    # For more details: check '..services/settings_module/v1/feature_settings_service.rb'
    #
    # @param key [String] [Key name for which value is to be fetched]
    #
    # @return [String] [value '1' or '0' based on configurations]
    #
    def get_feature_value(key)
      imli_weblab_service = SettingsModule::V1::ImliWeblabService
      imli_weblab_service.get_feature_value(key)
    end

  end
end