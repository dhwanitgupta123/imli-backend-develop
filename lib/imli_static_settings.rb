module ImliStaticSettings
  class << self

    #
    # Get value for the feature corresponds to given key
    # It is static settings whose value will be either '1' or '0',
    # based on a static map (configured through consul or yml)
    #
    # @param key [String] [Key name for which value is to be fetched]
    #
    # @return [String] [value '1' or '0' based on configurations]
    #
    def get_feature_value(key)
      imli_setting_service = SettingsModule::V1::ImliStaticSettingService
      imli_setting_service.get_feature_value(key)
    end

  end
end