require 'druid/query'
module Imli
  module Druid
    class Query
      attr_accessor :druid_query

      SERVICE = 'broker'

      #
      # This function initialize druid query with initial configs
      #
      def initialize(args)
        source = SERVICE + '/' + args[:datasource]
        @druid_query = ::Druid::Query.new(source)
        @druid_client = args[:druid_client]
        raise CommonModule::V1::CustomErrors::InvalidArgumentsError.new('Invalid druid client') if @druid_client.blank?
      end

      #
      # This function add properties of druid query
      #
      # @params args [Hash] containing hash with key = property name and value = property value
      #
      def add_properties(args)
        args.keys.each do |key|
          @druid_query.properties[key] = args[key]
        end
        return self
      end

      #
      # This function clean all the filters from druid query
      #
      def clear_filters
        if @druid_query.present? && @druid_query.properties.present? && @druid_query.properties[:filter].present?
          @druid_query.properties[:filter] = nil
        end
      end

      #
      # This function add filter in query
      #
      # @params can be hash or a block of code
      #
      def add_filter(hash = nil, &block)
        if hash
          @query = @druid_query.filter(hash)
        else
          @query = @druid_query.filter(&block)
        end
        return self
      end

      #
      # Add interval in select query
      #
      # @params from_time [Time] from time in utc
      # @params to_time [Time] to time in utc
      #
      def add_interval(from_time, to_time)
        @query.interval(from_time, to_time)
      end

      #
      # Send query to druid broker node and returns results
      #
      def send
        return @druid_client.send(@druid_query)
      end
    end
  end
end
