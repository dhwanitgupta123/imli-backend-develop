require 'druid'
module Imli
  module Druid
    class DruidClient
      attr_accessor :client

      SERVICE_LOCATION = 'http://kafka.buyample.com:8082/druid/v2'
      ZOOKEEPER_HOST = 'http://kafka.buyample.com:2181'
      SERVICE = 'broker'

      #
      # Initialize druid client with configs
      #
      def initialize(args)
        service_source = SERVICE + '/' + args[:datasource]
        @client = ::Druid::Client.new(ZOOKEEPER_HOST, static_setup: {service_source => SERVICE_LOCATION})
      end

      #
      # This function sends query to druid instance
      #
      # @params query [Druid::Query] query
      #
      def send(query)
        begin
          return @client.send(query)
        rescue => e
          raise CommonModule::V1::CustomErrors::RunTimeError.new(e)
        end
      end
    end
  end
end
