module Imli
  module Druid
    class SelectQueryResponseReader

      def initialize(results)
        @results = results
      end

      def get_events
        return [] if @results.blank?

        event_array = []

        @results.each do |result|
          events = result['events']
          next if events.blank?

          events.each do |event|
            event_array.push(event['event'])
          end
        end

        return event_array
      end
    end
  end
end
