require 'kafka'
require 'kafka/async_producer'

module Imli
  module Logger
    class LoggerClient

      DELIVERY_THRESHOLD = CommonModule::V1::Cache.read_int('KAFKA_DELIVERY_THRSHOLD') || 20
      DELIVERY_INTERVAL = CommonModule::V1::Cache.read_int('KAFKA_DELIVERY_INTERVAL') || 1
      SEED_BROKERS = CommonModule::V1::Cache.read_array('KAFKA_SEED_BROKERS') || ['localhost:9092']
      MAX_RETRIES = 3

      #
      # This function add record in async kafka producer which push message according to async producer
      # configuration
      #
      def self.add_record(args)
        @logger ||= get_logger
        begin
          @logger.produce(args[:dump], topic: args[:topic])
        rescue => e
          ApplicationLogger.error('Failed to log ' + args[:dump].to_s + ' in topic ' + args[:topic])
          CommonModule::V1::CustomErrors::RunTimeError.new('Failed to log record ' + e.message)
        end
      end

      #
      # This function return async kafka producer with initial params
      #
      def self.get_logger

        kafka = Kafka.new(seed_brokers: SEED_BROKERS, logger: Rails.logger)

        producer = kafka.async_producer(
          delivery_threshold: DELIVERY_THRESHOLD,
          delivery_interval: DELIVERY_INTERVAL.minutes.to_i,
          max_retries: MAX_RETRIES
        )

        at_exit { producer.shutdown }

        return producer
      end
    end
  end
end
