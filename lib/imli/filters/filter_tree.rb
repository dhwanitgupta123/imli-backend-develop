module Imli
  module Filters
    module FilterTree
      extend self

      def filter(tree, filters)

        return {} if tree.blank?

        return tree if filters.blank?

        executable_filters = get_filters(tree, filters)

        filtered_tree = tree

        executable_filters.each do |executable_filter|
          filtered_tree =  executable_filter[:filter].filter(executable_filter[:args])
        end

        return filtered_tree
      end

      private

      def get_filters(tree, filters)
        executable_filters = []
        executable_filters.push({args: {tree: tree, include_nodes: filters[:includes] }, filter: Imli::Filters::InclusionFilter}) if filters[:includes].present?
        executable_filters.push({args: {tree: tree, exclude_nodes: filters[:excludes]}, filter: Imli::Filters::ExclusionFilter}) if filters[:excludes].present?
        return executable_filters
      end
    end
  end
end
