module Imli
  module Filters
    module ExclusionFilter
      extend self

      def filter(args)

        tree = args[:tree]
        exclude_nodes = args[:exclude_nodes]

        return tree if exclude_nodes.blank?

        return {} if tree.blank?

        return exclude_tree_nodes(tree, exclude_nodes)
      end

      def exclude_tree_nodes(tree, exclude_nodes)

        nodes = []

        return [] if tree.blank?

        tree.each do |node|
          if exclude_nodes.include?(node[:node_name])
            return []
          else
            next if node[:next].blank?
            node[:next].each do |sub_node|
              excluded_tree = exclude_tree_nodes(node[sub_node.to_sym], exclude_nodes)
              if excluded_tree.present?
                node[sub_node.to_sym] = excluded_tree
              else
                node.delete(sub_node.to_sym)
              end
              nodes.push(node)
            end
          end
        end

        return nodes
      end
    end
  end
end
