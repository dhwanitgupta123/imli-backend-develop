module Imli
  module Filters
    module InclusionFilter
      extend self

      def filter(args)

        tree = args[:tree]
        include_node = args[:include_nodes]

        return {} if include_node.blank?

        return {} if tree.blank?

        tree = find_root_node(tree, include_node[:node_name]) if tree[0][:node_name] != include_node[:node_name]

        return filter_nodes(tree, include_node)
      end

      private


      def filter_nodes(tree, include_node)

        f_trees = []

        return [] if tree.blank?

        tree.each do |node|
          next_node = ''
          next_node = include_node[:next][:node_name].to_sym if include_node[:next].present? && include_node[:next][:node_name].present?
          f_tree = remove_other_childs(node, next_node)
          if include_node[:next].present?
            f_node = filter_nodes(node[next_node], include_node[:next])
            f_tree[include_node[:next][:node_name].to_sym] = f_node if f_node.present?
          end

          f_trees.push(f_tree)
        end

        return f_trees
      end


      def remove_other_childs(node, next_node)

        return node if node[:next].blank?

        node[:next].each do |n_node|
          if next_node != n_node.to_sym
            node.delete(n_node.to_sym)
          end
        end

        return node
      end

      def find_root_node(tree, node_name)

        nodes = []

        return [] if tree.blank?

        tree.each do |node|
          if node_name == node[:node_name]
            nodes.push(node)
          else
            next if node[:next].blank?
            node[:next].each do |sub_node|
              f_node = find_root_node(node[sub_node.to_sym], node_name)
              nodes.push(f_node) if f_node.present?
            end
          end
        end

        return nodes
      end
    end
  end
end
