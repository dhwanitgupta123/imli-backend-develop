#
# This class contain basic query object that helps to create
# complex queries
#
module Imli
  module ElasticSearchClient
    class << self

      MAX_SIZE = 10000

      # 
      # This function takes search_models as input and check if it is
      # already present in repository or not and accordingly save or update the
      # document
      #
      # @param search_models [Array] array of search model
      #
      def index_data(repository, search_models)

        return false if search_models.blank?

        search_models.each do |search_model|
          if get_document_by_id(repository, search_model.attributes[:id]) == false
            save(repository, search_model)
          else
            update(repository, search_model)
          end
        end
      end

      # 
      # Update document in repositroy
      #
      # @param search_model [Object] search model containing attributes to index
      #
      def update(repository, search_model)
        begin
          repository.update(search_model)
        rescue => e
          raise e
        end
      end

      # 
      # Save document in repositroy
      #
      # @param search_model [Object] search model containing attributes to index
      #
      def save(repository, search_model)
        begin
          repository.save(search_model)
        rescue => e
          raise e
        end        
      end

      #
      # This function re-initialize index, delete the existing one
      #
      def delete_previous_index(repository)
        begin
          repository.create_index! force: true
        rescue => e
          raise e
        end        
      end

      #
      # Return document by given id
      #
      # @param id [Integer] id of search model
      # 
      # @return [Document] this return document if it succeed to find
      # @return [False] if document not found in the repository
      #
      def get_document_by_id(repository, id)
        begin
          repository.find(id)
        rescue Elasticsearch::Persistence::Repository::DocumentNotFound => e
          return false
        end
      end

      #
      # Search query in repository
      #
      def search(repository, options = {})
        search_args = {}
        search_args[:query] = options[:query]
        search_args[:size] = options[:max_results] || MAX_SIZE
        search_args[:min_score] = options[:min_score] if options[:min_score].present?

        begin
          repository.search(search_args).to_a
        rescue => e
          raise e
        end
      end

      #
      # This function checks if marketplace_selling_pack repository connected successfully to elastic search server
      # it returns true if connected successfully else false
      #
      # @return [Boolean] true if exists else false
      #
      def repository_exists?(repository)
        begin
          repository.client.cluster.health
        rescue Faraday::ConnectionFailed
          return false
        end
        true
      end

      #
      # This function check if index with index-name specified in  repository exists in
      # elastic search server of not
      #
      # @return [Boolean] true if index exists else false
      #
      def index_exists?(repository)
        begin
          return false unless repository.client.indices.exists(index: repository.index)
          return true if repository.count > 0
        rescue Faraday::ConnectionFailed
          return false
        end
        return false
      end

    end # End of class
  end
end
