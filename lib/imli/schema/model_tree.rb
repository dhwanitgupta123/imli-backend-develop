require 'rails_erd'
require 'rails_erd/domain'

module Imli
  module Schema
    class ModelTree

      attr_accessor :domain

      def initialize
        load_models
        @domain = RailsERD::Domain.generate
      end

      def find_related_entities(entity)
        entities = []

        entities.push(entity)

        add_entities(entity, entities)

        return entities
      end

      def add_entities(entity, entities)

        return if entity.relationships.blank?

        entity.relationships.each do |rel|
          if rel.destination != entity && !entities.include?(rel.destination)
            entities.push(rel.destination)
            add_entities(rel.destination, entities)
          end
          if rel.source != entity && !entities.include?(rel.source)
            entities.push(rel.source)
            add_entities(rel.source, entities)
          end
        end
      end

      def load_models
        Rails.application.eager_load!
        LocationEngine::Engine.eager_load!
        SupplyChainEngine::Engine.eager_load!
        OrderEngine::Engine.eager_load!
        PricingEngine::Engine.eager_load!
      end

    end
  end
end
