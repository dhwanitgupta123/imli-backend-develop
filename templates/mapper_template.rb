#
# Module to handle all the functionalities related to MODULENAME
#
module MODULENAMEModule
  #
  # Version1 for MODULENAME module
  #
  module V1
    #
    # Map small_controller_name object to Json
    #
    module CONTROLLERNAMEMapper

      def self.map_small_controller_name_to_hash(small_controller_name)
        {
          id: small_controller_name.id,
          status: small_controller_name.status
        }
      end

      def self.map_small_controller_names_to_array(small_controller_names)
        small_controller_name_array = []
        small_controller_names.each do |small_controller_name|
          small_controller_name_array.push(map_small_controller_name_to_hash(small_controller_name))
        end
        small_controller_name_array
      end

      def self.add_small_controller_name_api_response_hash(small_controller_name)
        return {
          small_controller_name: map_small_controller_name_to_hash(small_controller_name)
        }
      end

      def self.get_all_small_controller_names_api_response_array(response)
        return {
          small_controller_names: map_small_controller_names_to_array(response[:elements]),
          page_count: response[:page_count]
        }
      end

      def self.get_small_controller_name_api_response_hash(small_controller_name)
        return {
          small_controller_name: map_small_controller_name_to_hash(small_controller_name)
        }
      end

      def self.update_small_controller_name_api_response_hash(small_controller_name)
        return {
          small_controller_name: map_small_controller_name_to_hash(small_controller_name)
        }
      end

      def self.change_small_controller_name_state_api_response_hash(small_controller_name)
        return {
          small_controller_name: map_small_controller_name_to_hash(small_controller_name)
        }
      end

    end
  end
end