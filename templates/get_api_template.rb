#
# Module to handle all the functionalities related to MODULENAME
#
module MODULENAMEModule
  #
  # Version1 for MODULENAME module
  #
  module V1
    #
    # GetCONTROLLERNAME api, it validates the request, call the
    # small_controller_name service and return the JsonResponse
    #
    class GetCONTROLLERNAMEApi < ENGINENAMEBaseModule::V1::BaseApi
      CAPSCONTROLLERNAME_SERVICE = MODULENAMEModule::V1::CONTROLLERNAMEService
      CAPSCONTROLLERNAME_MAPPER = MODULENAMEModule::V1::CONTROLLERNAMEMapper
      CAPSCONTROLLERNAME_RESPONSE_DECORATOR = MODULENAMEModule::V1::CONTROLLERNAMEResponseDecorator

      def initialize(params='')
        @params = params
        super
      end

      #
      # Return small_controller_name corresponding to id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        small_controller_name_service_class = CAPSCONTROLLERNAME_SERVICE.new(@params)

        begin
          validate_request(request)
          small_controller_name = small_controller_name_service_class.get_small_controller_name_by_id(request[:id])
          get_small_controller_name_api_response = CAPSCONTROLLERNAME_MAPPER.get_small_controller_name_api_response_hash(small_controller_name)
          return CAPSCONTROLLERNAME_RESPONSE_DECORATOR.create_get_small_controller_name_response(get_small_controller_name_api_response)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return CAPSCONTROLLERNAME_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params small_controller_name name
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          return MODULE_NAME_RESPONSE_DECORATOR.create_response_invalid_arguments_error("Id is required parameter")
        end
      end

    end
  end
end