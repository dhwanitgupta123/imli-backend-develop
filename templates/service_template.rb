#
# Module to handle all the functionalities related to MODULENAME
#
module MODULENAMEModule
  #
  # Version1 for MODULENAME module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class CONTROLLERNAMEService < ENGINENAMEBaseModule::V1::BaseService

      CAPSCONTROLLERNAME_DAO = MODULENAMEModule::V1::CONTROLLERNAMEDao

      def initialize(params='')
        @params = params
        @small_controller_name_dao = CAPSCONTROLLERNAME_DAO.new(@params)
        super
      end

      def create_small_controller_name(args)
        create_model(args, @small_controller_name_dao)
      end

      def update_small_controller_name(args)
        update_model(args, @small_controller_name_dao, 'CONTROLLERNAME')
      end

      def get_small_controller_name_by_id(id)
        validate_if_exists(id, @small_controller_name_dao, 'CONTROLLERNAME')
      end

      def get_all_small_controller_name(pagination_params = {})
        get_all_elements(@small_controller_name_dao, pagination_params)
      end

      def change_small_controller_name_state(args)
        change_state(args, @small_controller_name_dao, 'CONTROLLERNAME')
      end
    end
  end
end
