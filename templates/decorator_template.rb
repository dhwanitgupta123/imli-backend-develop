#
# Module to handle all the functionalities related to MODULENAME
#
module MODULENAMEModule
  #
  # Version1 for MODULENAME module
  #
  module V1
    #
    # Map small_controller_name object to Json
    #
    class CONTROLLERNAMEResponseDecorator < BaseModule::V1::BaseResponseDecorator


      #
      # Response for add time slot api
      #
      def self.create_add_small_controller_name_response(response)
        return {
          payload: {
            small_controller_name: response[:small_controller_name]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_get_all_small_controller_names_response(response)
        return {
          payload: {
            small_controller_names: response[:small_controller_names]
          },
          meta: {
            page_count: response[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_get_small_controller_name_response(response)
        return {
          payload: {
            small_controller_name: response[:small_controller_name]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_update_small_controller_name_response(response)
        return {
          payload: {
            small_controller_name: response[:small_controller_name]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_change_small_controller_name_state_response(response)
        return {
          payload: {
            message: 'State changed successfuly'
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

    end
  end
end