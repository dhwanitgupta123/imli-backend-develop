#
# Module to handle all the functionalities related to MODULENAME
#
module MODULENAMEModule
  #
  # Version1 for MODULENAME module
  #
  module V1
    #
    # UpdateCONTROLLERNAME api, it validates the request, call the
    # small_controller_name service and return the JsonResponse
    #
    class UpdateCONTROLLERNAMEApi < ENGINENAMEBaseModule::V1::BaseApi
      CAPSCONTROLLERNAME_SERVICE = MODULENAMEModule::V1::CONTROLLERNAMEService
      CAPSCONTROLLERNAME_MAPPER = MODULENAMEModule::V1::CONTROLLERNAMEMapper
      CAPSCONTROLLERNAME_RESPONSE_DECORATOR = MODULENAMEModule::V1::CONTROLLERNAMEResponseDecorator

      def initialize(params='')
        @params = params
        super
      end

      #
      # Function takes input corresponding to update small_controller_name and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have small_controller_name arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request)
        small_controller_name_service_class = CAPSCONTROLLERNAME_SERVICE.new(@params)

        begin
          validate_request(request)
          small_controller_name = small_controller_name_service_class.update_small_controller_name(request)
          update_small_controller_name_api_response = CAPSCONTROLLERNAME_MAPPER.update_small_controller_name_api_response_hash(small_controller_name)
          return CAPSCONTROLLERNAME_RESPONSE_DECORATOR.create_update_small_controller_name_response(update_small_controller_name_api_response)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return CAPSCONTROLLERNAME_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Request should contain small_controller_name id')
        end
      end
    end
  end
end