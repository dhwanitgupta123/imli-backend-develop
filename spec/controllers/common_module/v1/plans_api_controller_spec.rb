module CommonModule
  # Version 1 module of UsersModule
  module V1
    require 'rails_helper'
    require 'json'

    RSpec.describe CommonModule::V1::PlansApiController, type: :controller do
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:plan_service) { CommonModule::V1::PlanService }
      let(:plan_response_decorator) { CommonModule::V1::PlanResponseDecorator }

      let(:plan) { FactoryGirl.build(:plan) }
      let(:benefit) { FactoryGirl.build(:benefit) }

      # This should return the minimal set of values that should be in the session
      # in order to pass any filters (e.g. authentication) defined in
      # UsersController. Be sure to keep this updated too.
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:stub_before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:stub_after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      before(:each) do
        stub_before_actions
        stub_after_actions
      end

      
      describe 'get all plans' do

        it 'when plans are present in DB' do
          stub_ok_response = plan_response_decorator.create_response_plans_array([ plan ])
          expect_any_instance_of(plan_service).to receive(:get_all_active_plans).and_return(stub_ok_response)
          get :get_all_plans
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'when no plans present in DB' do
          stub_ok_response = plan_response_decorator.create_response_plans_array([])
          expect_any_instance_of(plan_service).to receive(:get_all_active_plans).and_return(stub_ok_response)
          get :get_all_plans
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end
      end
    end
  end
end
