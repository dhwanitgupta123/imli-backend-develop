module CommonModule
  # Version 1 module of CommonModule
  module V1
    require 'rails_helper'

    RSpec.describe CommonModule::V1::EmailInvitesController, type: :controller do
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:email_invites_service) { CommonModule::V1::EmailInvitesService }
      let(:email_invites_response_Decorator) { CommonModule::V1::EmailInvitesResponseDecorator }

      describe 'email_invites' do
        it 'with valid params' do
          stub_ok_response = email_invites_response_Decorator.create_success_response
          expect_any_instance_of(email_invites_service).to receive(:email_invites).and_return(stub_ok_response)
          get :email_invites, params: { email_id: Faker::Internet.email }
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with valid params' do
          stub_bad_response = email_invites_response_Decorator.create_response_bad_request
          expect_any_instance_of(email_invites_service).to receive(:email_invites).and_return(stub_bad_response)
          get :email_invites, params: { email_id: 'abc' }
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with blank params' do
          stub_bad_response = email_invites_response_Decorator.create_response_bad_request
          expect_any_instance_of(email_invites_service).to receive(:email_invites).and_return(stub_bad_response)
          get :email_invites
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end
    end
  end
end
