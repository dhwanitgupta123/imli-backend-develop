module CommonModule
  # Version 1 module of CommonModule
  module V1
    require 'rails_helper'

    RSpec.describe CommonModule::V1::ProductsSuggesterController, type: :controller do


      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:product_suggester_service) { CommonModule::V1::ProductSuggesterService }
      let(:plan_suggester_response_decorator) { CommonModule::V1::ProductSuggesterResponseDecorator }

      # This should return the minimal set of values that should be in the session
      # in order to pass any filters (e.g. authentication) defined in
      # UsersController. Be sure to keep this updated too.
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      
      before(:each) do
        expect_any_instance_of(access_control_controller).to receive(:before_actions).and_return(true)
        expect_any_instance_of(access_control_controller).to receive(:after_actions).and_return(true)
      end

      let(:product_suggestion_params) {
          {
            product_name: Faker::Lorem.word
          }
        }

      let(:product_suggestions_array) { [product_suggestion_params] }

      describe 'update product suggestion' do

        it 'with all valid parameters' do
          stub_ok_response = plan_suggester_response_decorator.create_product_suggestion_ok_response
          expect_any_instance_of(product_suggester_service).to receive(:update_product_suggestions)
                .and_return(stub_ok_response)
          post :update_product_suggestions, product_suggestion: product_suggestion_params
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          stub_bad_response = plan_suggester_response_decorator.create_response_bad_request
          expect_any_instance_of(product_suggester_service).to receive(:update_product_suggestions)
                .and_return(stub_bad_response)
          post :update_product_suggestions, product_suggestion: product_suggestion_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with wrong arguments' do
          stub_bad_response = plan_suggester_response_decorator.create_response_bad_request
          expect_any_instance_of(product_suggester_service).to receive(:update_product_suggestions)
                .and_return(stub_bad_response)
          post :update_product_suggestions, product_suggestion: product_suggestion_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

      end

      describe 'get all product suggestions' do

        let(:get_product_suggestions_params) {
          {
            suggestions: [
              {
                user_id: Faker::Number.number(1),
                product_suggestions: product_suggestions_array
              }
            ]
          }
        }

        it 'returns all product suggestions successfully' do
          stub_ok_response = plan_suggester_response_decorator.create_all_suggestions_response(get_product_suggestions_params)
          expect_any_instance_of(product_suggester_service).to receive(:get_all_product_suggestions)
                .and_return(stub_ok_response)
          get :get_all_product_suggestions, order: 'user_id'
          expect(response).to have_http_status(status_codes_util::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad response' do
          stub_bad_request_response = {
            'error': { message: content_util::INVALID_DATA_PASSED },
            'response': response_codes_util::BAD_REQUEST
          }
          stub_bad_response = plan_suggester_response_decorator.create_response_bad_request
          expect_any_instance_of(product_suggester_service).to receive(:get_all_product_suggestions)
                .and_return(stub_bad_response)

          get :get_all_product_suggestions, order: 'user_id'
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end
    # End of Rspec class
    end
  end
end