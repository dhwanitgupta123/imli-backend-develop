module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe CategorizationModule::V1::MpspDepartmentsController, type: :controller do

      let(:authorize_session) { access_control_controller.any_instance.stub(:authorized_request?).and_return(true) }
      let(:access_control_controller){UsersModule::V1::AccessControlController}
      let(:mpsp_department_service) {CategorizationModule::V1::MpspDepartmentService}
      let(:mpsp_department_params) { { :label => Faker::Name.first_name, event: 1 }}
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:id) { Faker::Number.number(1) }
      before(:each) do
        authorize_session
      end

      describe 'create_mpsp_department' do
        it 'creates mpsp_department successfully' do
          stub_ok_response = {
            'payload': {
              'mpsp_department': {
                'label': Faker::Name.first_name
              }
            },
            'response': response_codes::SUCCESS
          }
          mpsp_department_service.any_instance.stub(:create_mpsp_department).and_return(stub_ok_response)
          post :create_mpsp_department, :mpsp_department => mpsp_department_params
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required params are not present' do
          stub_bad_request_response = {
            'error': { :message => content_util::INSUFFICIENT_DATA },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_department_service.any_instance.stub(:create_mpsp_department).and_return(stub_bad_request_response)
          post :create_mpsp_department, :mpsp_department => {'label' => ''}
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when internal server error happens' do
          stub_run_time_error_response = {
            'error': { :message => content_util::RUN_TIME_ERROR },
            'response': response_codes::INTERNAL_SERVER_ERROR
          }
          mpsp_department_service.any_instance.stub(:create_mpsp_department).and_return(stub_run_time_error_response)
          post :create_mpsp_department, :mpsp_department => mpsp_department_params
          expect(response).to have_http_status(status_codes::INTERNAL_SERVER_ERROR)
        end
      end

      describe 'get_mpsp_department' do
        it 'returns mpsp_department details successfully' do
          stub_ok_response = {
            'payload': {
              'mpsp_department': {
                'label': Faker::Name.first_name,
                'categories':[
                  {
                    'label': Faker::Name.first_name
                  },
                  {
                    'label': Faker::Name.last_name
                  }
                ]
              }
            },
            'response': response_codes::SUCCESS
          }
          mpsp_department_service.any_instance.stub(:get_mpsp_department).and_return(stub_ok_response)
          get :get_mpsp_department, :id => id
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { :message => content_util::INSUFFICIENT_DATA },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_department_service.any_instance.stub(:get_mpsp_department).and_return(stub_bad_request_response)
          get :get_mpsp_department, id: id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when mpsp_department does not exist' do
          stub_bad_request_response = {
            'error': { :message => content_util::DEPARTMENT_NOT_PRESENT },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_department_service.any_instance.stub(:get_mpsp_department).and_return(stub_bad_request_response)
          get :get_mpsp_department, id: id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when internal server error happens' do
          stub_run_time_error_response = {
            'error': { :message => content_util::RUN_TIME_ERROR },
            'response': response_codes::INTERNAL_SERVER_ERROR
          }
          mpsp_department_service.any_instance.stub(:get_mpsp_department).and_return(stub_run_time_error_response)
          get :get_mpsp_department, id: id
          expect(response).to have_http_status(status_codes::INTERNAL_SERVER_ERROR)
        end
      end

      describe 'get_all_mpsp_departments' do
        it 'returns all mpsp_department successfully' do
          stub_ok_response = {
            'payload': {
              'mpsp_departments': [
                {
                  'label': Faker::Name.first_name
                },
                {
                  'label': Faker::Name.first_name
                }
              ] 
            },
            'response': response_codes::SUCCESS
          }   
          mpsp_department_service.any_instance.stub(:get_all_mpsp_departments).and_return(stub_ok_response)
          get :get_all_mpsp_departments
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
        it 'with wrong request' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_department_service.any_instance.stub(:get_all_mpsp_departments).and_return(stub_bad_request_response)
          get :get_all_mpsp_departments
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'change_state' do
        it 'returns deactivated mpsp_department details successfully' do
          stub_ok_response = {
            'payload': {
              'mpsp_department': {
                'label': Faker::Name.first_name
              }
            },
            'response': response_codes::SUCCESS
          }
          mpsp_department_service.any_instance.stub(:change_state).and_return(stub_ok_response)
          put :change_state, id: id, mpsp_department: mpsp_department_params
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_department_service.any_instance.stub(:change_state).and_return(stub_bad_request_response)
          put :change_state, id: id, mpsp_department: mpsp_department_params
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when mpsp_department does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::DEPARTMENT_NOT_PRESENT },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_department_service.any_instance.stub(:change_state).and_return(stub_bad_request_response)
          put :change_state, id: id, mpsp_department: mpsp_department_params
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when preConditionRequired' do
          stub_pre_condition_required_response = {
            'error': { message: 'Department is not active' },
            'response': response_codes::PRE_CONDITION_REQUIRED
          }
          mpsp_department_service.any_instance.stub(:change_state).and_return(
            stub_pre_condition_required_response)
          put :change_state, id: id, mpsp_department: mpsp_department_params
          expect(response).to have_http_status(status_codes::PRE_CONDITION_REQUIRED)
        end
      end

      describe 'update_mpsp_department' do
        it 'returns updated mpsp_department details successfully' do
          stub_ok_response = {
            'payload': {
              'mpsp_department': {
                'label': Faker::Name.first_name
              }
            },
            'response': response_codes::SUCCESS
          }
          mpsp_department_service.any_instance.stub(:update_mpsp_department).and_return(stub_ok_response)
          put :update_mpsp_department, mpsp_department: mpsp_department_params, id: id
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_department_service.any_instance.stub(:update_mpsp_department).and_return(stub_bad_request_response)
          put :update_mpsp_department, mpsp_department: mpsp_department_params.merge(label: ''), id: id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when mpsp_department does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::DEPARTMENT_NOT_PRESENT },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_department_service.any_instance.stub(:update_mpsp_department).and_return(stub_bad_request_response)
          put :update_mpsp_department, mpsp_department: mpsp_department_params, id: id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when internal server error happens' do
          stub_run_time_error_response = {
            'error': { message: content_util::RUN_TIME_ERROR },
            'response': response_codes::INTERNAL_SERVER_ERROR
          }
          mpsp_department_service.any_instance.stub(:update_mpsp_department).and_return(stub_run_time_error_response)
          put :update_mpsp_department, mpsp_department: mpsp_department_params, id: id
          expect(response).to have_http_status(status_codes::INTERNAL_SERVER_ERROR)
        end
      end
    end
  end
end
