module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe CategorizationModule::V1::MpspCategoriesController, type: :controller do

      let(:authorize_session) { access_control_controller.any_instance.stub(:authorized_request?).and_return(true) }
      let(:access_control_controller){UsersModule::V1::AccessControlController}
      let(:event) { { event: 1 } }
      let(:valid_mpsp_category_params) { {:label => Faker::Name.first_name, :mpsp_department_id => Faker::Number.number(1)}}
      let(:valid_mpsp_sub_category_params) { {:label => Faker::Name.first_name, :mpsp_category_id => Faker::Number.number(1)}}
      let(:mpsp_department_service) {CategorizationModule::V1::MpspDepartmentService}
      let(:mpsp_category_service) {CategorizationModule::V1::MpspCategoryService}
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:id) { Faker::Number.number(1) }
      before(:each) do
        authorize_session
      end

      describe "create_category" do
        it "creates category successfully" do
          stub_ok_response = {
            "payload": {
              "mpsp_category": {
                "label": Faker::Name.first_name
              }
            },
            "response": response_codes::SUCCESS
          }
          mpsp_category_service.any_instance.stub(:create_mpsp_category).and_return(stub_ok_response)
          post :create_mpsp_category, :mpsp_category => valid_mpsp_category_params
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it "when required params are not present" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:create_mpsp_category).and_return(stub_bad_request_response)
          post :create_mpsp_category, :mpsp_category => {"label" => ""}
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it "when department id is not present" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes::BAD_REQUEST
          }
          category_params = valid_mpsp_category_params.merge({:mpsp_department_id => ""})
          mpsp_category_service.any_instance.stub(:create_mpsp_category).and_return(stub_bad_request_response)
          post :create_mpsp_category, :mpsp_category => category_params
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it "when department does not exist" do
          stub_bad_request_response = {
            "error": { :message => content_util::DEPARTMENT_NOT_PRESENT },
            "response": response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:create_mpsp_category).and_return(stub_bad_request_response)
          post :create_mpsp_category, :mpsp_category => valid_mpsp_category_params
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it "when internal server error happens" do
          stub_run_time_error_response = {
            "error": { :message => content_util::RUN_TIME_ERROR },
            "response": response_codes::INTERNAL_SERVER_ERROR
          }
          mpsp_category_service.any_instance.stub(:create_mpsp_category).and_return(stub_run_time_error_response)
          post :create_mpsp_category, :mpsp_category => valid_mpsp_category_params
          expect(response).to have_http_status(status_codes::INTERNAL_SERVER_ERROR)
        end
      end

      describe "create_sub_category" do
        it "creates sub category successfully" do
          stub_ok_response = {
            "payload": {
              "mpsp_sub_category": {
                "category": Faker::Name.first_name,
                "label": Faker::Name.first_name
              }
            },
            "response": response_codes::SUCCESS
          }
          mpsp_category_service.any_instance.stub(:create_mpsp_sub_category).and_return(stub_ok_response)
          post :create_mpsp_sub_category, :mpsp_category => valid_mpsp_sub_category_params
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it "when required params are not present" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:create_mpsp_sub_category).and_return(stub_bad_request_response)
          post :create_mpsp_sub_category, :mpsp_category => {"label" => ""}
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it "when category id is not present" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes::BAD_REQUEST
          }
          mpsp_sub_category_params = valid_mpsp_sub_category_params.merge({:mpsp_category_id => ""})
          mpsp_category_service.any_instance.stub(:create_mpsp_sub_category).and_return(stub_bad_request_response)
          post :create_mpsp_sub_category, :mpsp_category => mpsp_sub_category_params
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it "when category does not exist" do
          stub_bad_request_response = {
            "error": { :message => content_util::CATEGORY_NOT_PRESENT },
            "response": response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:create_mpsp_sub_category).and_return(stub_bad_request_response)
          post :create_mpsp_sub_category, :mpsp_category => valid_mpsp_sub_category_params
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it "when internal server error happens" do
          stub_run_time_error_response = {
            "error": { :message => content_util::RUN_TIME_ERROR },
            "response": response_codes::INTERNAL_SERVER_ERROR
          }
          mpsp_category_service.any_instance.stub(:create_mpsp_sub_category).and_return(stub_run_time_error_response)
          post :create_mpsp_sub_category, :mpsp_category => valid_mpsp_sub_category_params
          expect(response).to have_http_status(status_codes::INTERNAL_SERVER_ERROR)
        end
      end

      describe "get_mpsp_category" do
        it "returns mpsp category details successfully" do
          stub_ok_response = {
            "payload": {
              "mpsp_category": {
                "label": Faker::Name.first_name,
                "mpsp_sub_categories":[
                  {
                    "label": Faker::Name.first_name
                  },
                  {
                    "label": Faker::Name.last_name
                  }
                ]
              }
            },
            "response": response_codes::SUCCESS
          }
          mpsp_category_service.any_instance.stub(:get_mpsp_category).and_return(stub_ok_response)
          get :get_mpsp_category, id: id
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it "when required parameters are missing" do
          stub_bad_request_response = {
            "error": { :message => content_util::INSUFFICIENT_DATA },
            "response": response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:get_mpsp_category).and_return(stub_bad_request_response)
          get :get_mpsp_category, id: id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it "when department does not exist" do
          stub_bad_request_response = {
            "error": { :message => content_util::CATEGORY_NOT_PRESENT },
            "response": response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:get_mpsp_category).and_return(stub_bad_request_response)
          get :get_mpsp_category, id: id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it "when internal server error happens" do
          stub_run_time_error_response = {
            "error": { :message => content_util::RUN_TIME_ERROR },
            "response": response_codes::INTERNAL_SERVER_ERROR
          }
          mpsp_category_service.any_instance.stub(:get_mpsp_category).and_return(stub_run_time_error_response)
          get :get_mpsp_category, id: id
          expect(response).to have_http_status(status_codes::INTERNAL_SERVER_ERROR)
        end
      end

      describe "get_all_mpsp_categories" do
        it "returns all mpsp categories successfully" do
          stub_ok_response = {
            "payload": {
              "mpsp_categories": [
                {
                  "label": Faker::Name.first_name
                },
                {
                  "label": Faker::Name.first_name
                }
              ] 
            },
            "response": response_codes::SUCCESS
          }   
          mpsp_category_service.any_instance.stub(:get_all_mpsp_categories).and_return(stub_ok_response)
          get :get_all_mpsp_categories
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)["payload"].to_json).to eq(stub_ok_response[:payload].to_json)
        end
        it 'with wrong request' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:get_all_mpsp_categories).and_return(stub_bad_request_response)
          get :get_all_mpsp_categories
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'change_state' do
        it 'returns changed status category details successfully' do
          stub_ok_response = {
            "payload": {
              "mpsp_category": {
                "label": Faker::Name.first_name
              }
            },
            "response": response_codes::SUCCESS
          }
          mpsp_category_service.any_instance.stub(:change_state).and_return(stub_ok_response)
          put :change_state, id: id, mpsp_category: event
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:change_state).and_return(stub_bad_request_response)
          put :change_state, id: id, mpsp_category: event
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when category does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::CATEGORY_NOT_PRESENT },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:change_state).and_return(stub_bad_request_response)
          put :change_state, id: id, mpsp_category: event
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when preConditionRequired' do
          stub_pre_condition_required_response = {
            'error': { message: 'Category is not active' },
            'response': response_codes::PRE_CONDITION_REQUIRED
          }
          mpsp_category_service.any_instance.stub(:change_state).and_return(
            stub_pre_condition_required_response)
          put :change_state, id: id, mpsp_category: event
          expect(response).to have_http_status(status_codes::PRE_CONDITION_REQUIRED)
        end
      end

      describe 'update_mpsp_category' do
        it 'returns updated department details successfully' do
          stub_ok_response = {
            "payload": {
              "mpsp_category": {
                "label": Faker::Name.first_name
              }
            },
            "response": response_codes::SUCCESS
          }
          mpsp_category_service.any_instance.stub(:update_mpsp_category).and_return(stub_ok_response)
          post :update_mpsp_category, mpsp_category: valid_mpsp_category_params, id: id
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:update_mpsp_category).and_return(stub_bad_request_response)
          put :update_mpsp_category, mpsp_category: valid_mpsp_category_params.merge(label: ''), id: id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when category does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::CATEGORY_NOT_PRESENT },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:update_mpsp_category).and_return(stub_bad_request_response)
          put :update_mpsp_category, mpsp_category: valid_mpsp_category_params, id: id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when internal server error happens' do
          stub_run_time_error_response = {
            'error': { message: content_util::RUN_TIME_ERROR },
            'response': response_codes::INTERNAL_SERVER_ERROR
          }
          mpsp_category_service.any_instance.stub(:update_mpsp_category).and_return(stub_run_time_error_response)
          put :update_mpsp_category, mpsp_category: valid_mpsp_category_params, id: id
          expect(response).to have_http_status(status_codes::INTERNAL_SERVER_ERROR)
        end
      end

      describe 'update_sub_category' do
        it 'returns updated sub_category details successfully' do
          stub_ok_response = {
            "payload": {
              "mpsp_category": {
                "label": Faker::Name.first_name
              }
            },
            "response": response_codes::SUCCESS
          }
          mpsp_category_service.any_instance.stub(:update_mpsp_sub_category).and_return(stub_ok_response)
          put :update_mpsp_sub_category, mpsp_category: valid_mpsp_sub_category_params, id: id
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:update_mpsp_sub_category).and_return(stub_bad_request_response)
          put :update_mpsp_sub_category, mpsp_category: valid_mpsp_sub_category_params.merge(label: ''), id: id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when sub category does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::CATEGORY_NOT_PRESENT },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:update_mpsp_sub_category).and_return(stub_bad_request_response)
          put :update_mpsp_sub_category, mpsp_category: valid_mpsp_sub_category_params, id: id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when internal server error happens' do
          stub_run_time_error_response = {
            'error': { message: content_util::RUN_TIME_ERROR },
            'response': response_codes::INTERNAL_SERVER_ERROR
          }
          mpsp_category_service.any_instance.stub(:update_mpsp_sub_category).and_return(stub_run_time_error_response)
          put :update_mpsp_sub_category, mpsp_category: valid_mpsp_sub_category_params, id: id
          expect(response).to have_http_status(status_codes::INTERNAL_SERVER_ERROR)
        end
      end

      describe 'get_all_mpsp_sub_categories' do
        it 'returns all sub categories successfully' do
          stub_ok_response = {
            'payload': {
              'mpsp_sub_categories': [
                {
                  'label': Faker::Name.first_name
                },
                {
                  'label': Faker::Name.first_name
                }
              ] 
            },
            'response': response_codes::SUCCESS
          }   
          mpsp_category_service.any_instance.stub(:get_all_mpsp_sub_categories).and_return(stub_ok_response)
          get :get_all_mpsp_sub_categories
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'with wrong request' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes::BAD_REQUEST
          }
          mpsp_category_service.any_instance.stub(:get_all_mpsp_sub_categories).and_return(stub_bad_request_response)
          get :get_all_mpsp_sub_categories
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
