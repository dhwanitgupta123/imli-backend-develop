module DescriptionsModule
  module V1
    require 'rails_helper'

    RSpec.describe DescriptionsModule::V1::DescriptionsController, type: :controller do

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_description) { DescriptionsModule::V1::AddDescriptionApi }
      let(:update_description) { DescriptionsModule::V1::UpdateDescriptionApi }
      let(:get_description) { DescriptionsModule::V1::GetDescriptionApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }

      let(:stub_ok_response) {
          {
            payload: {
              descriptions: [
                {
                  heading: Faker::Lorem.characters(6)
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add description' do
        it 'returns created description successfully' do
          add_description.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_description, description: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_description.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_description, description: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update description' do
        it 'returns updated description successfully' do
          update_description.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_description, id: 1, description: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_description.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_description, id:1, description: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get description' do
        it 'with valid argument' do
          get_description.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_description, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'with invalid arguments' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_description.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_description, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
