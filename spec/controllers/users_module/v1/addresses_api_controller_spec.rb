module UsersModule
  module V1
    require 'rails_helper'
    require 'json'

    RSpec.describe UsersModule::V1::AddressesApiController, type: :controller do

      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:user_service) { UsersModule::V1::UserService }
      let(:user_response_decorator) { UsersModule::V1::UserResponseDecorator }
      let(:address_response_decorator) { AddressModule::V1::AddressResponseDecorator }
      let(:stub_before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:stub_after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller){UsersModule::V1::AccessControlController}
      let(:session_token) {Faker::Lorem.characters(64)}

      let(:country) { FactoryGirl.build(:country) }
      let(:state)   { FactoryGirl.build(:state, country: country) }
      let(:city)    { FactoryGirl.build(:city, state: state) }
      let(:area)    { FactoryGirl.build(:area, city: city) }
      let(:user)    { FactoryGirl.build(:user) }
      let(:address) { FactoryGirl.build(:address, user: user, area: area) }

      let(:address_params) {
          {
            nickname: Faker::Company.name,
            address_line1: Faker::Address.building_number,
            address_line2: Faker::Address.street_address,
            landmark: Faker::Address.street_name,
            area_id: Faker::Number.number(1)
          }
        }
      let(:address_request) {{ addresses: [address_params] }}

      before(:each) do
        stub_before_actions
        stub_after_actions
      end

      describe "get all addresses of user #get_all_addresses_of_user" do
        it 'with all valid parameters' do
          stub_ok_response = address_response_decorator.get_all_addresses_of_user_response([])
          user_service.any_instance.stub(:get_all_addresses_of_user).and_return(stub_ok_response)
          get :get_all_addresses_of_user, :session_token => session_token
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with unauthenticated user' do
          stub_unauthenticated_response = user_response_decorator.create_response_unauthenticated_user
          user_service.any_instance.stub(:get_all_addresses_of_user).and_return(stub_unauthenticated_response)
          get :get_all_addresses_of_user
          expect(response).to have_http_status(status_codes_util::UN_AUTHORIZED)
        end
      end

      describe "add address of user #add_address" do
        let(:address_params) {
          {
            nickname: Faker::Company.name,
            address_line1: Faker::Address.building_number,
            address_line2: Faker::Address.street_address,
            landmark: Faker::Address.street_name,
            area_id: Faker::Number.number(1)
          }
        }
        it 'with all valid parameters' do
          stub_ok_response = address_response_decorator.add_address_ok_response(address)
          user_service.any_instance.stub(:add_address).and_return(stub_ok_response)
          post :add_address, user: address_request, session_token: session_token
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          address_params[:address_line1] = nil
          stub_insufficient_response = user_response_decorator.create_response_bad_request
          user_service.any_instance.stub(:add_address).and_return(stub_insufficient_response)
          post :add_address, user: address_request, session_token: session_token
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          address_params[:area_id] = nil
          stub_invalid_response = user_response_decorator.create_response_invalid_data_passed
          user_service.any_instance.stub(:add_address).and_return(stub_invalid_response)
          post :add_address, user: address_request, session_token: session_token
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end

      describe "update user address #update_address" do
        it 'with all valid parameters' do
          country = FactoryGirl.build(:country)
          state = FactoryGirl.build(:state, country: country)
          city = FactoryGirl.build(:city, state: state)
          area = FactoryGirl.build(:area, city: city)
          user = FactoryGirl.build(:user)
          address = FactoryGirl.build(:address, user: user, area: area)

          stub_ok_response = address_response_decorator.add_address_ok_response(address)
          user_service.any_instance.stub(:update_address).and_return(stub_ok_response)
          put :update_address, id: Faker::Number.number(1), user: address_request, session_token: session_token
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          address_params[:address_line1] = nil
          stub_insufficient_response = user_response_decorator.create_response_bad_request
          user_service.any_instance.stub(:update_address).and_return(stub_insufficient_response)
          put :update_address, id: Faker::Number.number(1), user: address_request, session_token: session_token
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          address_params[:area_id] = nil
          stub_invalid_response = user_response_decorator.create_response_invalid_data_passed
          user_service.any_instance.stub(:update_address).and_return(stub_invalid_response)
          put :update_address, id: Faker::Number.number(1), user: address_request, session_token: session_token
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end
    end
  end
end