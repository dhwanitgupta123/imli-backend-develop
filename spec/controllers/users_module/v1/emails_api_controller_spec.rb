module UsersModule
  module V1
    require 'rails_helper'
    require 'json'

    RSpec.describe UsersModule::V1::EmailsApiController, type: :controller do

      let(:content_util) { CommonModule::V1::Content }
      let(:cache_util) { CommonModule::V1::Cache }
      let(:redirection_page) { cache_util.read('BUYAMPLE_HOMEPAGE') }
      #########expect(response).to redirect_to(redirection_page)
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:user_service) { UsersModule::V1::UserService }
      let(:user_response_decorator) { UsersModule::V1::UserResponseDecorator }
      let(:stub_before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:stub_after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller){UsersModule::V1::AccessControlController}
      let(:session_token) {Faker::Lorem.characters(64)}

      before(:each) do
        stub_before_actions
        stub_after_actions
      end

      describe "verify email id" do
        # let(:valid_email_token) { Faker::Lorem.characters(10).to_s }
        # it "approved successfully" do
        #   stub_ok_response = {
        #     "payload": { "user": { "phone_number": Faker::Number.number(10).to_s }},
        #     "response": response_codes_util::SUCCESS
        #   }
        #   user_service.any_instance.stub(:verify_email).and_return(stub_ok_response)
        #   get :verify_email, :token => valid_email_token
        #   expect(response).to have_http_status(status_codes_util::SUCCESS)
        # end

        # it "with insufficient arguments passed" do
        #   stub_insufficient_response = user_response_decorator.create_response_bad_request
        #   user_service.any_instance.stub(:verify_email).and_return(stub_insufficient_response)
        #   get :verify_email, :token => valid_email_token
        #   expect(response).to have_http_status(status_codes_util::SUCCESS)
        # end

        # it "with invalid arguments passed" do
        #   stub_invalid_response = user_response_decorator.create_response_invalid_data_passed
        #   user_service.any_instance.stub(:verify_email).and_return(stub_invalid_response)
        #   get :verify_email, :token => valid_email_token
        #   expect(response).to have_http_status(status_codes_util::SUCCESS)
        # end

        # it "when email is already verified" do
        #   stub_email_verified_response = user_response_decorator.create_response_email_already_verified
        #   user_service.any_instance.stub(:verify_email).and_return(stub_email_verified_response)
        #   get :verify_email, :token => valid_email_token
        #   expect(response).to have_http_status(status_codes_util::SUCCESS)
        # end

        # it "when email token expired" do
        #   stub_timeout_response = user_response_decorator.create_request_timeout_error
        #   user_service.any_instance.stub(:verify_email).and_return(stub_timeout_response)
        #   get :verify_email, :token => valid_email_token
        #   expect(response).to have_http_status(status_codes_util::SUCCESS)
        # end
      end

      describe "resend email token" do
        let(:user_email_params) {
          {
            email_id: Faker::Internet.free_email
          }
        }

        it "with valid arguments" do
          stub_ok_response = user_response_decorator.create_response_token_email_sent_successfully
          user_service.any_instance.stub(:resend_email_token).and_return(stub_ok_response)
          get :resend_email_token, :user => user_email_params
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it "with insufficient arguments passed" do
          stub_insufficient_response = user_response_decorator.create_response_bad_request
          user_service.any_instance.stub(:resend_email_token).and_return(stub_insufficient_response)
          get :resend_email_token, :user => user_email_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it "with invalid data passed or not email associated with passed mail id" do
          stub_invalid_response = user_response_decorator.create_response_invalid_data_passed
          user_service.any_instance.stub(:resend_email_token).and_return(stub_invalid_response)
          get :resend_email_token, :user => user_email_params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it "when error occurs during processing" do
          stub_runtime_response = user_response_decorator.create_response_runtime_error
          user_service.any_instance.stub(:resend_email_token).and_return(stub_runtime_response)
          get :resend_email_token, :user => user_email_params
          expect(response).to have_http_status(status_codes_util::INTERNAL_SERVER_ERROR)
        end

        it "when request does not comes from authentic user" do
          stub_unauthenticate_response = user_response_decorator.create_response_unauthenticated_user
          user_service.any_instance.stub(:resend_email_token).and_return(stub_unauthenticate_response)
          get :resend_email_token, :user => user_email_params
          expect(response).to have_http_status(status_codes_util::UN_AUTHORIZED)
        end
      end
    end
  end
end