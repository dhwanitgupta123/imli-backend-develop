module UsersModule
  # Version 1 module of UsersModule
  module V1
    require 'rails_helper'
    require 'json'

    RSpec.describe UsersModule::V1::ProfilesController, type: :controller do
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:profile_service) { UsersModule::V1::ProfileService }
      let(:profile_response_decorator) { UsersModule::V1::ProfileResponseDecorator }

      let(:profile) { FactoryGirl.build(:profile) }
      let(:user) { FactoryGirl.build(:user) }

      # This should return the minimal set of values that should be in the session
      # in order to pass any filters (e.g. authentication) defined in
      # UsersController. Be sure to keep this updated too.
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:stub_before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:stub_after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      before(:each) do
        stub_before_actions
        stub_after_actions
      end

      describe 'add_pincode' do
        let(:params) {
          {
            profile: {
              pincode: Faker::Number.number(6).to_s
            }
          }
        }

        it 'with all valid parameters' do
          stub_ok_response = profile_response_decorator.create_success_response
          expect_any_instance_of(profile_service).to receive(:add_pincode).and_return(stub_ok_response)
          post :add_pincode, user: params
          expect(response).to have_http_status(status_codes_util::SUCCESS)
        end

        it 'with insufficient arguments' do
          stub_bad_response = profile_response_decorator.create_response_bad_request
          expect_any_instance_of(profile_service).to receive(:add_pincode).and_return(stub_bad_response)
          post :add_pincode, user: params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end

        it 'with invalid arguments' do
          stub_invalid_data_response = profile_response_decorator.create_response_invalid_data_passed
          expect_any_instance_of(profile_service).to receive(:add_pincode).and_return(stub_invalid_data_response)
          post :add_pincode, user: params
          expect(response).to have_http_status(status_codes_util::BAD_REQUEST)
        end
      end
    # End of Rspec class
    end
  end
end
