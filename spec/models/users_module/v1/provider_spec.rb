module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe UsersModule::V1::Provider, type: :model do
      let(:class_path) {UsersModule::V1::ProvidersModule::V1}
      it "is valid" do
        user = UsersModule::V1::User.new(:phone_number => Faker::Number.number(10).to_s)
        user.provider = UsersModule::V1::Provider.new
        user.provider.message = class_path::Message.new(:otp => Faker::Number.number(4))
        should user.provider.save
      end

      it "is invalid without user_id" do
        FactoryGirl.build_stubbed(:provider, user_id: nil).should_not be_valid
      end

      it "is valid if one of the provider is present" do
        user = UsersModule::V1::User.new(:phone_number => Faker::Number.number(10).to_s)
        user.provider = UsersModule::V1::Provider.new
        user.provider.message = class_path::Message.new(:otp => Faker::Number.number(4))
        should user.provider.save
      end

      it "is valid if one of the provider is present" do
        user = UsersModule::V1::User.new(:phone_number => Faker::Number.number(10).to_s)
        user.provider = UsersModule::V1::Provider.new
        user.provider.generic = class_path::Generic.new(:password => Faker::Lorem.characters(7))
        should user.provider.save
      end

      it "is invalid if none of the provider is present" do
        provider = FactoryGirl.build(:provider, message: nil, generic: nil)
        expect(provider).not_to be_valid
      end
      
      it 'is not valid if duplicate user_identity exists' do
        user = UsersModule::V1::User.new(:phone_number => Faker::Number.number(10).to_s)
        user.provider = UsersModule::V1::Provider.new
        user.provider.message = class_path::Message.new(:otp => Faker::Number.number(4))
        user.provider.save

        user.provider = UsersModule::V1::Provider.new
        should_not user.provider.save
      end
    end
  end
end