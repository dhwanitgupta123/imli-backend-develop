module UsersModule
  module V1
    module ProvidersModule
      module V1
        require 'rails_helper'
        
        RSpec.describe UsersModule::V1::ProvidersModule::V1::Message, type: :model do
          it "is valid" do
            FactoryGirl.build_stubbed(:message, provider: FactoryGirl.build_stubbed(:provider) ).should be_valid
          end

          it "is invalid without otp" do
            FactoryGirl.build(:generic, password: nil).should_not be_valid
          end

          it "is invalid without provider_id" do
            FactoryGirl.build(:message, provider_id: nil).should_not be_valid
          end

          it 'is not valid if duplicate provider_id exists' do
            class_path = UsersModule::V1::ProvidersModule::V1
            user = UsersModule::V1::User.new(:phone_number => Faker::Number.number(10).to_s)
            user.provider = UsersModule::V1::Provider.new
            user.provider.message = class_path::Message.new(:otp => Faker::Number.number(4))
            user.provider.message.save

            user.provider.message = class_path::Message.new(:otp => Faker::Number.number(4))    
            should_not user.provider.message.save
          end
        end
      end
    end
  end
end

