module DescriptionsModule
  module V1
    require 'rails_helper'

    RSpec.describe DescriptionsModule::V1::Description, type: :model do
      
      it "is a valid factory" do
        FactoryGirl.create(:description).should be_valid
      end
    end
  end
end
