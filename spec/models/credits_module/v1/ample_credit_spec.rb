module CreditsModule
  module V1
    require 'rails_helper'

    RSpec.describe CreditsModule::V1::AmpleCredit, type: :model do
      
      it "is a valid factory" do
        FactoryGirl.create(:ample_credit).should be_valid
      end

      it "is not a valid factory" do
        FactoryGirl.build(:ample_credit, user: nil).should be_invalid
      end
    end
  end
end
