module LogginModule
  module V1
    require 'rails_helper'

    RSpec.describe LoggingModule::V1::ActivityLog, type: :model do
      
      let(:activity_log) {LoggingModule::V1::ActivityLog}
      let(:custom_errors) {CommonModule::V1::CustomErrors}
      let(:valid_options) { 
        {
      	 :time => Faker::Number.number(4).to_s,
          :api_name => Faker::Internet.slug('user login', '::'),
          :order => "desc",
          :phone_number => Faker::Number.number(10).to_s
        }
      }
      it "is valid" do
        FactoryGirl.build(:activity_log).should be_valid
      end

      context "Recent Activities " do
      	it "with nil options" do
      		expect{activity_log.activities_in_last_x_time_with_api_name_by_order(nil)}.to raise_error(custom_errors::InsufficientDataError)
      	end

      	it "with nill time" do
          options_with_nil_time = valid_options.merge({:time => nil})
          expect{activity_log.activities_in_last_x_time_with_api_name_by_order(nil)}.to raise_error(custom_errors::InsufficientDataError)
      	end
      end
    end
  end
end