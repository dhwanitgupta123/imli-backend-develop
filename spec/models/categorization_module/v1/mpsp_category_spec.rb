module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe CategorizationModule::V1::MpspCategory, type: :model do
      
      it "is a valid factory" do
        FactoryGirl.create(:mpsp_category).should be_valid
      end

      it "is invalid without label" do
        FactoryGirl.build(:mpsp_category, label: nil).should_not be_valid
      end
    end
  end
end
