module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe Department, type: :model do
      it "is a valid factory" do
        FactoryGirl.create(:department).should be_valid
      end

      it "is invalid without label" do
        FactoryGirl.build(:department, label: nil).should_not be_valid
      end

      it ' is invalid if priority is not unique' do
        FactoryGirl.create(:department, label: 'test1', priority: 1)
        expect{ FactoryGirl.create(:department, label: 'test2', priority: 1) }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end
end
