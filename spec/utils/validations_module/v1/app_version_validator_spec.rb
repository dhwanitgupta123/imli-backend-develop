#
# This module is responsible to handle all the pre-request validations
# like user state, user membership state etc
#
module ValidationsModule
  #
  # Version1 for validation module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ValidationsModule::V1::AppVersionValidator do

      let(:app_version_validator) { ValidationsModule::V1::AppVersionValidator.new }
      let(:cache_util) { CommonModule::V1::Cache }
      let(:key_for_fetching_app_valid_for_membership) { 'VERSION_NUMBER_FOR_MEMBERSHIP_FEATURE' }
      let(:user) { FactoryGirl.build(:user) }
      let(:exact_version_number) { 18 }

      context 'validate' do
        it 'with version number less than specified one' do
          USER_SESSION[:platform_type] = 'ANDROID'
          USER_SESSION[:version_number] = exact_version_number - 1
          expect(cache_util).to receive(:read_int).with(key_for_fetching_app_valid_for_membership).
            and_return(exact_version_number)
          response = app_version_validator.validate(user)
          expect(response[:result]).to eq(false)
        end

        it 'with version number more than specified one' do
          USER_SESSION[:platform_type] = 'ANDROID'
          USER_SESSION[:version_number] = exact_version_number + 1
          response = app_version_validator.validate(user)
          expect(response[:result]).to eq(true)
        end

        it 'with platform as WEB' do
          USER_SESSION[:platform_type] = 'WEB'
          response = app_version_validator.validate(user)
          expect(response[:result]).to eq(true)
        end

      end

    end
  end
end
