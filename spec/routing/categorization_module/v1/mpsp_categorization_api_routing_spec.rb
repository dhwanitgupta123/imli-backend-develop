module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe CategorizationModule::V1::MpspCategorizationApiController, type: :routing do
      describe 'routing' do

        it 'routes to #get_all_mpsp_departments_details' do
          expect(:get => 'api/departments').to route_to(
          {'format'=>'json', 
           'controller'=>'categorization_module/v1/mpsp_categorization_api', 
           'action'=>'get_all_mpsp_departments_details'})
        end
      end
    end
  end
end