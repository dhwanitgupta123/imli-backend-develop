module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe CategoriesController, type: :routing do
      describe 'routing' do

        it 'routes to #create_category' do
          expect(:post => 'categories/new').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/categories', 
             'action'=>'create_category'})
        end

        it 'routes to #create_sub_category' do
          expect(:post => 'sub_categories/new').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/categories', 
             'action'=>'create_sub_category'})
        end

        it 'routes to #get_category' do
          expect(:get => 'categories/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/categories', 
             'action'=>'get_category',
             'id'=>'1'})
        end

        it 'routes to #get_all_categories' do
          expect(:get => 'categories').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/categories', 
             'action'=>'get_all_categories'})
        end

        it 'routes to #update_category' do
          expect(:put => 'categories/update/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/categories', 
             'action'=>'update_category',
             'id'=>'1'})
        end

        it 'routes to #sub_category/update/1' do
          expect(:put => 'sub_categories/update/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/categories', 
             'action'=>'update_sub_category',
             'id'=>'1'})
        end

        it 'routes to #change_state' do
          expect(:put => 'categories/state/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/categories', 
             'action'=>'change_state',
             'id'=>'1'})
        end

        it 'routes to #get_category' do
          expect(:get => 'sub_categories/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/categories', 
             'action'=>'get_category',
             'id'=>'1'})
        end

        it 'routes to #get_all_sub_categories' do
          expect(:get => 'sub_categories').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/categories', 
             'action'=>'get_all_sub_categories'})
        end
      end
    end
  end
end
