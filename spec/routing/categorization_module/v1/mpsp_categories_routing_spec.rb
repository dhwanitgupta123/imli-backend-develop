module CategorizationModule
  module V1
    require 'rails_helper'

    RSpec.describe CategorizationModule::V1::MpspCategoriesController, type: :routing do
      describe 'routing' do

        it 'routes to #create_mpsp_category' do
          expect(:post => 'mpsp/categories/new').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_categories', 
             'action'=>'create_mpsp_category'})
        end

        it 'routes to #create_mpsp_sub_category' do
          expect(:post => 'mpsp/sub_categories/new').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_categories', 
             'action'=>'create_mpsp_sub_category'})
        end

        it 'routes to #get_mpsp_category' do
          expect(:get => 'mpsp/categories/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_categories', 
             'action'=>'get_mpsp_category',
             'id'=>'1'})
        end

        it 'routes to #get_all_mpsp_categories' do
          expect(:get => 'mpsp/categories').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_categories', 
             'action'=>'get_all_mpsp_categories'})
        end

        it 'routes to #update_mpsp_category' do
          expect(:put => 'mpsp/categories/update/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_categories', 
             'action'=>'update_mpsp_category',
             'id'=>'1'})
        end

        it 'routes to #mpsp_sub_category/update/1' do
          expect(:put => 'mpsp/sub_categories/update/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_categories', 
             'action'=>'update_mpsp_sub_category',
             'id'=>'1'})
        end

        it 'routes to #change_state' do
          expect(:put => 'mpsp/categories/state/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_categories', 
             'action'=>'change_state',
             'id'=>'1'})
        end

        it 'routes to #get_mpsp_category' do
          expect(:get => 'mpsp/sub_categories/1').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_categories', 
             'action'=>'get_mpsp_category',
             'id'=>'1'})
        end

        it 'routes to #get_all_mpsp_sub_categories' do
          expect(:get => 'mpsp/sub_categories').to route_to(
            {'format'=>'json', 
             'controller'=>'categorization_module/v1/mpsp_categories', 
             'action'=>'get_all_mpsp_sub_categories'})
        end
      end
    end
  end
end
