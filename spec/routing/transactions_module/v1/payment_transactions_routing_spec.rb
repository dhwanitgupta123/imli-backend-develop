module TransactionsModule
  module V1
    require "rails_helper"
    # Routing specs for Address Api Controller
    RSpec.describe TransactionsModule::V1::PaymentTransactionsController, type: :routing do
      describe "routing" do

        it "routes to payment_transactions#initiate_transaction" do
          expect(:post => "transaction/initiate").to route_to(
            {"format"=>"json", 
             "controller"=>"transactions_module/v1/payment_transactions", 
             "action"=>"initiate_transaction"})
        end

        it "routes to payment_transactions#update_transaction" do
          expect(:post => "transaction/update").to route_to(
            {"format"=>"json", 
             "controller"=>"transactions_module/v1/payment_transactions", 
             "action"=>"update_transaction"})
        end
      end
    end
  end
end