module DescriptionsModule
  module V1
    require 'rails_helper'
    # Routing specs for Address Api Controller
    RSpec.describe DescriptionsModule::V1::DescriptionsController, type: :routing do
      describe 'routing' do

        it 'routes to descriptions#add_description' do
          expect(:post => 'descriptions/new').to route_to(
            {'format'=>'json', 
             'controller'=>'descriptions_module/v1/descriptions', 
             'action'=>'add_description'})
        end

        it 'routes to descriptions#update_description' do
          expect(:put => 'descriptions/1').to route_to(
            {
             'format'=>'json', 
             'controller'=>'descriptions_module/v1/descriptions', 
             'action'=>'update_description',
             'id'=>'1'
            })
        end

        it 'routes to descriptions#get_description' do
          expect(:get => 'descriptions/1').to route_to(
            {
             'format'=>'json', 
             'controller'=>'descriptions_module/v1/descriptions', 
             'action'=>'get_description',
             'id'=>'1'
            })
        end
      end
    end
  end
end
