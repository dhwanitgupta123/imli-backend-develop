module UsersModule
  module V1
    require "rails_helper"
    # Routing specs for Address Api Controller
    RSpec.describe UsersModule::V1::ProfilesController, type: :routing do
      describe "routing" do

        it "routes to profiles#add_pincode" do
          expect(:post => "api/profile/pincode").to route_to(
            {"format"=>"json", 
             "controller"=>"users_module/v1/profiles", 
             "action"=>"add_pincode"
            })
        end
      end
    end
  end
end
