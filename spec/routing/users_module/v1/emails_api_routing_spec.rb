module UsersModule
  module V1
    require "rails_helper"
    # Routing specs for Emails api Controller
    RSpec.describe UsersModule::V1::EmailsApiController, type: :routing do
      describe "routing" do

        it "routes to #verify_email" do
          expect(:get => "api/users/verify_email").to route_to(
            { "format"=>"html", 
             "controller"=>"users_module/v1/emails_api", 
             "action"=>"verify_email"
             } )
        end

        it "routes to #resend_email_token" do
          expect(:post => "api/users/resend_email_token").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/emails_api", 
             "action"=>"resend_email_token"
             } )
        end
      end
    end
  end
end