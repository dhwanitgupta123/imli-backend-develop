module UsersModule
  module V1
    require "rails_helper"
    # Routing specs for Address Api Controller
    RSpec.describe UsersModule::V1::AddressesApiController, type: :routing do
      describe "routing" do

        it "routes to users#update_address" do
          expect(:put => "api/users/addresses/:id").to route_to(
            {"format"=>"json", 
             "controller"=>"users_module/v1/addresses_api", 
             "action"=>"update_address",
             "id"=>":id"})
        end

        it "routes to users#add_address" do
          expect(:post => "api/users/addresses/new").to route_to(
            {"format"=>"json", 
             "controller"=>"users_module/v1/addresses_api", 
             "action"=>"add_address"})
        end

        it "routes to users#get_all_addresses_of_user" do
          expect(:get => "api/users/addresses").to route_to(
            {"format"=>"json", 
             "controller"=>"users_module/v1/addresses_api", 
             "action"=>"get_all_addresses_of_user"})
        end
      end
    end
  end
end