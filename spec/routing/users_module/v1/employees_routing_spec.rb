module UsersModule
  module V1
    require "rails_helper"
    # Routing specs for Employees Controller
    RSpec.describe UsersModule::V1::EmployeesController, type: :routing do
      describe "routing" do

        it "routes to #register" do
          expect(:post => "employee/register").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/employees", 
             "action"=>"register"
             } )
        end

        it "routes to #login" do
          expect(:post => "employee/login").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/employees", 
             "action"=>"login"
             } )
        end

        it "routes to #approve" do
          expect(:get => "employee/approve").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/employees", 
             "action"=>"approve"
             } )
        end

        it "routes to employees#add_profile" do
          expect(:post => "employee/add_profile").to route_to(
            {"format"=>"json", 
             "controller"=>"users_module/v1/employees", 
             "action"=>"add_profile"})
        end
      end
    end
  end
end