module UsersModule
  module V1
    require "rails_helper"
    # Routing specs for Roles Controller
    RSpec.describe UsersModule::V1::RolesController, type: :routing do
      describe "routing" do

        it "routes to #create_role" do
          expect(:post => "roles/create").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/roles", 
             "action"=>"create_role"
             } )
        end

        it "routes to #update_role" do
          expect(:put => "roles/1").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/roles", 
             "action"=>"update_role",
             "role_id"=> "1"
             } )
        end

        it "routes to #get_user_role" do
          expect(:get => "roles/user_role").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/roles", 
             "action"=>"get_user_role"
             } )
        end

        it "routes to #get_roles_tree" do
          expect(:get => "roles/role_tree").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/roles", 
             "action"=>"get_roles_tree"
             } )
        end

        it "routes to #get_users_for_role" do
          expect(:get => "roles/1/users").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/roles", 
             "action"=>"get_users_for_role",
             "role_id"=>"1"
             } )
        end

        it "routes to #get_all_permissions" do
          expect(:get => "roles/permissions").to route_to(
            { "format"=>"json", 
             "controller"=>"users_module/v1/roles", 
             "action"=>"get_all_permissions"
             } )
        end
      end
    end
  end
end