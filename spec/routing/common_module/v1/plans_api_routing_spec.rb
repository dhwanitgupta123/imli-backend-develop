module CommonModule
  module V1
    require "rails_helper"
    # Routing specs for Plans API Controller
    RSpec.describe CommonModule::V1::PlansApiController, type: :routing do
      describe "routing" do

        it "routes to #get_all_plans" do
          expect(:get => "/api/plans").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/plans_api", 
             "action"=>"get_all_plans"})
        end
      end
    end
  end
end
