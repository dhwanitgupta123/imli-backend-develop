module CommonModule
  module V1
    require "rails_helper"
    # Routing specs for Carousels Controller
    RSpec.describe CommonModule::V1::ConstantsController, type: :routing do
      describe "routing" do

        it "routes to #get_cluster_constants" do
          expect(:get => "/constants/cluster_constants").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/constants", 
             "action"=>"get_cluster_constants"})
        end
      end
    end
  end
end