module CommonModule
  module V1
    require "rails_helper"
    # Routing specs for Carousels Controller
    RSpec.describe CommonModule::V1::CarouselsController, type: :routing do
      describe "routing" do

        it "routes to #update_carousel" do
          expect(:post => "/carousels/update/1").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/carousels",
             "type"=>"1", 
             "action"=>"update_carousel"})
        end

        it "routes to #get_carousel" do
          expect(:get => "/carousels/1").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/carousels",
             "type"=>"1",
             "action"=>"get_carousel"})
        end

        it "routes to #get_orphan_carousels_types_with_details" do
          expect(:get => "/carousels/types").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/carousels",
             "action"=>"get_orphan_carousels_types_with_details"})
        end

        it "routes to #get_all_carousels" do
          expect(:get => "/carousels").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/carousels",
             "action"=>"get_all_carousels"})
        end
      end
    end
  end
end