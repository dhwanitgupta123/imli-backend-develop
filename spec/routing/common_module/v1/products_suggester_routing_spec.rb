module CommonModule
  module V1
    require "rails_helper"
    # Routing specs for ProductsSuggester Controller
    RSpec.describe CommonModule::V1::ProductsSuggesterController, type: :routing do
      describe "routing" do

        it "routes to #update_product_suggestions" do
          expect(:post => "products/suggestions/update").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/products_suggester", 
             "action"=>"update_product_suggestions"})
        end

        it "routes to #get_all_product_suggestions" do
          expect(:get => "products/suggestions").to route_to(
            {"format"=>"json", 
             "controller"=>"common_module/v1/products_suggester", 
             "action"=>"get_all_product_suggestions"})
        end
        
      end
    end
  end
end