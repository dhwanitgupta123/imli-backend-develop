# @author Sumit <sumitchhuttani007@gmail.com>
# Test cases to test PasswordHelper
module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe UsersModule::V1::PasswordHelper do
      let(:password_helper){UsersModule::V1::PasswordHelper }
      let(:dummy_password) { "dummy password" }

      context "when creating hash" do
        it "and empty password is used" do
          expect(password_helper.create_hash("")).to be nil
        end

        it "should not create duplicate hash" do
          hash_one = password_helper.create_hash(dummy_password)
          hash_two = password_helper.create_hash(dummy_password)

          expect(hash_one).not_to eq(hash_two)
        end

        it "should always create hash of fixed length" do
          hash_one = password_helper.create_hash(dummy_password)
          hash_two = password_helper.create_hash(dummy_password)

          expect(hash_one.length).to eq(hash_two.length)
        end
      end

      context "when validating password" do
        let(:generated_hash){ password_helper.create_hash(dummy_password) }

        it "should validate self created hash" do
          expect(password_helper.validate_password(dummy_password, generated_hash)).to be true
        end

        it "if empty password is passed" do
          expect(password_helper.validate_password("", generated_hash)).to be false
        end

        it "if empty/nil hash is passed" do
          expect(password_helper.validate_password(dummy_password, nil)).to be false
          expect(password_helper.validate_password(dummy_password, "")).to be false
        end

        it "if tampered hash is passed" do
          tampered_hash = generated_hash.slice(0..generated_hash.length-10)
          expect(password_helper.validate_password(dummy_password, tampered_hash)).to be false

        end

        it "if wrong hash is passed" do
          new_hash = password_helper.create_hash("new password")
          expect(password_helper.validate_password(dummy_password, new_hash)).to be false
        end
      end
    end
  end
end