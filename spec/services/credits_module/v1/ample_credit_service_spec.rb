#
# Module to handle all the functionalities related to ample_credit
#
module CreditsModule
  #
  # Version1 for ample_credit module
  #
  module V1
    require 'rails_helper'
    RSpec.describe CreditsModule::V1::AmpleCreditService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:ample_credit_service) { CreditsModule::V1::AmpleCreditService.new({}) }
      let(:ample_credit_dao) { CreditsModule::V1::AmpleCreditDao }
      let(:ample_credit) { FactoryGirl.build(:ample_credit) }


      let(:ample_credit_request) {
        {
          amount: Faker::Number.decimal(2).to_f,
          ample_credit: ample_credit
        }
      }


      context 'credit amount' do
        it 'with valid arguments' do

          before_balance = ample_credit.balance

          response = ample_credit_service.credit(ample_credit_request)

          expect(response.balance.to_s).to eq((before_balance + ample_credit_request[:amount]).to_s)
        end
      end

      context 'debit amount' do
        it 'with valid arguments' do

          before_balance = ample_credit.balance

          response = ample_credit_service.debit(ample_credit_request)

          expect(response.balance.to_s).to eq((before_balance - ample_credit_request[:amount]).to_s)
        end
      end
 
    end
  end
end
