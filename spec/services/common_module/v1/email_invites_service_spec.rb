module CommonModule
  # Module V1 of CommonModule
  module V1
    require 'rails_helper'
    RSpec.describe CommonModule::V1::CarouselService do
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes_util) { CommonModule::V1::StatusCodes }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      let(:email_invites_service) { CommonModule::V1::EmailInvitesService.new({}) }

      describe 'email_invites' do
        it 'with blank params' do
          data = email_invites_service.email_invites({})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with invalid params' do
          request = { email_id: 'abc' }
          data = email_invites_service.email_invites(request)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with valid params' do
          request = { email_id: Faker::Internet.email }
          data = email_invites_service.email_invites(request)
          expect(data[:response]).to eq(response_codes_util::SUCCESS)
        end
      end
    end
  end
end
