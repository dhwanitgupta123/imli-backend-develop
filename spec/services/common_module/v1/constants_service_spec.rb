module CommonModule
  # Module V1 of UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe CommonModule::V1::ConstantsService do

      let(:constants_service) { CommonModule::V1::ConstantsService.new({}) }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }

      it 'with get_cluster_constants' do
        data = constants_service.get_cluster_constants
        expect(data[:response]).to eq(response_codes_util::SUCCESS)
      end
    end
  end
end
