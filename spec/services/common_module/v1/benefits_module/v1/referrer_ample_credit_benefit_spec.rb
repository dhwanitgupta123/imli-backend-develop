module CommonModule
  # Module V1 of UsersModule
  module V1
    module BenefitsModule
      module V1

        require 'rails_helper'
        RSpec.describe CommonModule::V1::BenefitsModule::V1::ReferrerAmpleCreditBenefit do

          let(:referrer_ample_credit_benefit) { CommonModule::V1::BenefitsModule::V1::ReferrerAmpleCreditBenefit.new({}) }
          let(:ample_credit_service) { CreditsModule::V1::AmpleCreditService }
          let(:user) { FactoryGirl.create(:user) }
          let(:benefit) { FactoryGirl.build(:benefit) }

          describe 'apply ' do
            it 'with invalid user' do
              user.referred_by = nil
              user.save

              expect(referrer_ample_credit_benefit.apply({user: user, benefit: benefit})).to eq(false)
            end

            it 'with valid user' do
              user.referred_by = user.id.to_s
              user.save
    
              expect_any_instance_of(referrer_ample_credit_benefit.class).to receive(:is_valid?).and_return(true)
              expect_any_instance_of(ample_credit_service).to receive(:credit).and_return(true)
              expect_any_instance_of(referrer_ample_credit_benefit.class).to receive(:update_referrer_benefit).and_return(true)

              expect(referrer_ample_credit_benefit.apply({user: user, benefit: benefit})).to eq(true)
            end
          end
        end
      end
    end
  end
end
