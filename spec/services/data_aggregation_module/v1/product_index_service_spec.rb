#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    require 'rails_helper'
    RSpec.describe DataAggregationModule::V1::ProductIndexService do

      let(:product_index_service) { DataAggregationModule::V1::ProductIndexService }
      let(:product_index_service_class) { DataAggregationModule::V1::ProductIndexService.new }
      let(:product_repository) { DataAggregationModule::V1::ProductRepository }
      let(:product_search_model) { DataAggregationModule::V1::ProductSearchModel }
      let(:product_data_loader) { DataAggregationModule::V1::DataLoaders::V1::ProductDataLoader }

      context 'update index' do
        it 'should re_index if product-index has 0 document' do
          expect_any_instance_of(product_index_service).to receive(:index_exists?).and_return(false)
          expect_any_instance_of(product_index_service).to receive(:re_initialize_index).and_return(true)
          product_index_service_class.update_index 
        end

        it 'update index ' do
          product_search_models = [product_search_model.new]
          expect_any_instance_of(product_index_service).to receive(:index_exists?).and_return(true)
          expect_any_instance_of(product_data_loader).to receive(:load_data).and_return(product_search_models)
          expect_any_instance_of(product_index_service).to receive(:index_data).and_return(true)
          product_index_service_class.update_index
        end
      end

      context 're_initialize_index' do
        it 'delete previous index and create new with data' do
          product_search_models = [product_search_model.new]
          expect_any_instance_of(product_data_loader).to receive(:load_data).and_return(product_search_models)
          expect_any_instance_of(product_index_service).to receive(:delete_previous_index).and_return(true)
          expect_any_instance_of(product_index_service).to receive(:index_data).and_return(true)
          product_index_service_class.re_initialize_index
        end
      end
    end
  end
end
