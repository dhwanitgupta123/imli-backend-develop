#
# Module to handle functionalities related to data aggregation
#
module DataAggregationModule
  #
  # Version1 for data aggregation module
  #
  module V1
    require 'rails_helper'
    RSpec.describe DataAggregationModule::V1::ApiLogIndexService do

      let(:api_log_index_service) { DataAggregationModule::V1::ApiLogIndexService }
      let(:api_log_index_service_class) { DataAggregationModule::V1::ApiLogIndexService.new }
      let(:api_log_repository) { DataAggregationModule::V1::ApiLogRepository }
      let(:api_log_search_model) { DataAggregationModule::V1::ApiLogSearchModel }
      let(:api_log_data_loader) { DataAggregationModule::V1::DataLoaders::V1::ApiLogDataLoader }

      context 'update index' do
        it 'should re_index if api_log-index has 0 document' do
          expect_any_instance_of(api_log_index_service).to receive(:index_exists?).and_return(false)
          expect_any_instance_of(api_log_index_service).to receive(:re_initialize_index).and_return(true)
          api_log_index_service_class.update_index 
        end

        it 'update index ' do
          api_log_search_models = [api_log_search_model.new]
          expect_any_instance_of(api_log_index_service).to receive(:index_exists?).and_return(true)
          expect_any_instance_of(api_log_data_loader).to receive(:load_data).and_return(api_log_search_models)
          expect_any_instance_of(api_log_index_service).to receive(:index_data).and_return(true)
          api_log_index_service_class.update_index
        end
      end

      context 're_initialize_index' do
        it 'delete previous index and create new with data' do
          api_log_search_models = [api_log_search_model.new]
          expect_any_instance_of(api_log_data_loader).to receive(:load_data).and_return(api_log_search_models)
          expect_any_instance_of(api_log_index_service).to receive(:delete_previous_index).and_return(true)
          expect_any_instance_of(api_log_index_service).to receive(:index_data).and_return(true)
          api_log_index_service_class.re_initialize_index
        end
      end
    end
  end
end