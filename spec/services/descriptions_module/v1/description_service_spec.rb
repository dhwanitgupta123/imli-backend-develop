#
# Module to handle all the functionalities related to description
#
module DescriptionsModule
  #
  # Version1 for description module
  #
  module V1
    require 'rails_helper'
    RSpec.describe DescriptionsModule::V1::DescriptionService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:description_service) { DescriptionsModule::V1::DescriptionService.new({}) }
      let(:description_dao) { DescriptionsModule::V1::DescriptionDao }
      let(:description_helper) { DescriptionsModule::V1::DescriptionHelper }
      let(:description) { FactoryGirl.build(:description) }
      let(:mpsp) { FactoryGirl.build(:marketplace_selling_pack) }
      let(:cache_client) { CacheModule::V1::CacheClient }

      let(:description_request) {
        {
          heading: Faker::Company.name,
          resource: mpsp.class.name,
          resource_id: mpsp.id,
          data: [
            Faker::Company.name
          ]
        }
      }

      let(:update_description_request) {
        {
          id: Faker::Number.number(1),
          description_type: Faker::Number.number(1),
          context: {
            level: Faker::Number.number(1),
            level_id: Faker::Number.number(1)
          }
        }
      }

      let(:change_state_args) {
        {
          id: Faker::Number.number(1),
          event: common_events::ACTIVATE
        }
      }

      let(:pagination_params) {
        {
          global: true
        }
      }

      let(:description_type) {
        {
          description_type: 1,
          display_description_type: 'Banner'
        }
      }
      
      let(:description_level) {
        {
          level: 2,
          display_level: 'Department'
        }
      }

      context 'create description' do
        it 'with valid arguments' do
          expect(description_helper).to receive(:validate_resource).and_return(mpsp) 
          expect_any_instance_of(description_dao).to receive(:create).
            and_return(description)
          response = description_service.create_descriptions(description_request)

          expect(response).to eq(description.childs(true))
        end
      end


      context 'get description by id' do
        it 'with existing description' do
          expect(cache_client).to receive(:get).twice.and_return(nil)
          expect_any_instance_of(description_dao).to receive(:get_by_id).
            and_return(description)
          response = description_service.get_description_tree(Faker::Number.number(1))
          expect(response[:id]).to equal(description.id)
        end

        it 'with non existing description' do
          expect(cache_client).to receive(:get).and_return(nil)
          expect_any_instance_of(description_dao).to receive(:get_by_id).
            and_raise(custom_error_util::ResourceNotFoundError)

          expect{ description_service.get_description_tree(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::ResourceNotFoundError)
        end

        it 'with not a root description' do
          expect(cache_client).to receive(:get).and_return(nil)
          expect_any_instance_of(description_dao).to receive(:get_by_id).
            and_return(description)
          expect(description).to receive(:root?).and_return(false)
          expect{ description_service.get_description_tree(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)        
        end
      end
    end
  end
end
