#
# Module to handle all the functionalities related to Release Management service
#
module ReleaseManagementModule
  #
  # Version1 for description module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ReleaseManagementModule::V1::ReleaseManagementService do
      let(:release_management_service) { ReleaseManagementModule::V1::ReleaseManagementService.new({}) }
      let(:cache_util) { CommonModule::V1::Cache }
      let(:key_for_fetching_app_update_popup_type) { 'APP_UPDATE_POPUP_TYPE' }
      let(:key_for_fetching_app_latest_version) { 'ANDROID_LATEST_VERSION' }
      let(:key_for_fetching_app_update_optional) { 'APP_UPDATE_OPTIONAL' }
      let(:key_for_fetching_app_update_popup_title) { 'APP_UPDATE_POPUP_TITLE' }
      let(:key_for_fetching_app_update_popup_description) { 'APP_UPDATE_POPUP_DESCRIPTION' }
      let(:key_for_fetching_app_update_popup_change_log) { 'APP_UPDATE_POPUP_CHANGE_LOG' }
      let(:key_for_fetching_app_update_popup_image_ids) { 'APP_UPDATE_POPUP_IMAGE_IDS' }
      let(:args){
        {
          'platform': 'ANDROID',
          'version_code': Faker::Number.between(0,50)
        }
      }
      let(:popup_content) {
        {
          'type': 'APP_UPDATE',
          'optional': Faker::Boolean.boolean,
          'title': Faker::Lorem.word,
          'description': Faker::Lorem.sentence,
          'change_log': Faker::Lorem.sentences,
          'images': [Faker::Number.between(0,5)]
        }
      }

      context 'get app update popup content' do

        it 'with invalid platform as WEB' do
          params = args.merge({platform: 'WEB'})

          response = release_management_service.get_app_update_popup_content(params)

          expect(response).to eq({})
        end

        it 'with latest app version' do
          latest_version = args[:version_code]

          expect(cache_util).to receive(:read).with(key_for_fetching_app_latest_version).
            and_return(latest_version)

          response = release_management_service.get_app_update_popup_content(args)

          expect(response).to eq({})
        end

        it 'with outdated app version and valid platform' do
          latest_version = args[:version_code].to_i + 2

          expect(cache_util).to receive(:read).with(key_for_fetching_app_latest_version).twice.
            and_return(latest_version)
          expect(cache_util).to receive(:read).with(key_for_fetching_app_update_popup_type).
            and_return(popup_content[:type])
          expect(cache_util).to receive(:read).with(key_for_fetching_app_update_optional).
            and_return(popup_content[:optional])
          expect(cache_util).to receive(:read).with(key_for_fetching_app_update_popup_title).
            and_return(popup_content[:title])
          expect(cache_util).to receive(:read).with(key_for_fetching_app_update_popup_description).
            and_return(popup_content[:description])
          expect(cache_util).to receive(:read).with(key_for_fetching_app_update_popup_change_log).
            and_return(popup_content[:change_log])
          expect(cache_util).to receive(:read).with(key_for_fetching_app_update_popup_image_ids).
            and_return(popup_content[:images])

          response = release_management_service.get_app_update_popup_content(args)

          expect(response[:target_version]).to eq(latest_version)
          expect(response[:title]).to eq(popup_content[:title])
        end

      end



    end # End of Rspec
  end
end
