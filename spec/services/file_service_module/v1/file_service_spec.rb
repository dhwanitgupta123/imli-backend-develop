#
# Module to handle functionalities related to file upload to S3
#
module FileServiceModule
  #
  # Version1 for file service module
  #
  module V1
    require 'rails_helper'
    RSpec.describe FileServiceModule::V1::FileService do

      let(:version) { 1 }
      let(:file_service) { FileServiceModule::V1::FileService.new(version) }
      let(:upload_data_worker) { FileServiceModule::V1::UploadDataWorker }
      let(:upload_file_worker) { FileServiceModule::V1::UploadFileWorker }
      let(:file_service_util) { FileServiceModule::V1::FileServiceUtil }

      let(:data) { Faker::Lorem.characters(70) }
      let(:key) { Faker::Lorem.characters(7) }
      let(:random_url) { Faker::Internet.url }
      let(:file_url) { Faker::Internet.slug(random_url, key) }
      let(:cache_util) { CommonModule::V1::Cache }

      let(:current_dir) { File.dirname(__FILE__) }
      let(:files_in_current_dir) {  Dir.glob( current_dir + '/*') }
      let(:valid_file_path) { files_in_current_dir[0] }
      let(:invalid_file_path) { Faker::Lorem.characters(70) }

      let(:custom_errors_util) { CommonModule::V1::CustomErrors }

      before(:each) do
        expect(cache_util).to receive(:read).and_return(Faker::Lorem.characters(7))
      end

      context 'upload data to s3' do
        it 'using asynchronous workers' do
          expect(upload_data_worker).to receive(:perform_async).and_return(Faker::Lorem.characters(7))
          data = file_service.upload_data_to_s3_async(data, key)
          expect(data[:path]).to include(key)
        end

        it 'using synchronous method' do
          expect_any_instance_of(file_service_util).to receive(:upload_data).and_return(file_url)
          data = file_service.upload_data_to_s3(data, key)
          expect(data[:url]).to eq(file_url)
        end
      end

      describe 'upload file to s3' do
        context 'using asynchronous worker' do
          it 'with valid file_path' do
            expect(upload_file_worker).to receive(:perform_async).and_return(Faker::Lorem.characters(7))
            data = file_service.upload_file_to_s3_async(valid_file_path, key)
            expect(data[:path]).to include(key)
          end

          it 'with invalid file_path' do
            expect{ file_service.upload_file_to_s3_async(invalid_file_path, key) }.to raise_error(custom_errors_util::FileNotFound)
          end
        end

        context 'using synchronous method' do
          it 'with valid_file_path' do
            expect_any_instance_of(file_service_util).to receive(:upload_file).and_return(file_url)
            data = file_service.upload_file_to_s3(valid_file_path, key)
            expect(data[:url]).to eq(file_url)
          end

          it 'with invalid_file_path' do
            expect{ file_service.upload_file_to_s3(invalid_file_path, key) }.to raise_error(custom_errors_util::FileNotFound)
          end

          it 'throws S3FailedToUpload error' do
            expect_any_instance_of(file_service_util).to receive(:upload_file).and_raise(custom_errors_util::S3FailedToUpload.new)
            expect{ file_service.upload_file_to_s3(valid_file_path, key) }.to raise_error(custom_errors_util::S3FailedToUpload)
          end
        end
      end
    end
  end
end
