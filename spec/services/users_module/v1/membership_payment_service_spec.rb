module UsersModule
  module V1
    module MembershipModule
      module V1
        require 'rails_helper'

        RSpec.describe UsersModule::V1::MembershipModule::V1::MembershipPaymentService do

          let(:membership_payment_service) { UsersModule::V1::MembershipModule::V1::MembershipPaymentService.new }
          let(:payment_model) { UsersModule::V1::MembershipModule::V1::MembershipPayment }
          let(:payment) { FactoryGirl.build(:membership_payment) }
          let(:custom_error_util) { CommonModule::V1::CustomErrors }
          let(:user_service) { UsersModule::V1::UserService }
          let(:transaction_status) { TransactionsModule::V1::TransactionStatus }
          let(:membership_dao) { UsersModule::V1::MembershipDao }
          let(:membership_service) { UsersModule::V1::MembershipService }
          let(:membership_state) { UsersModule::V1::ModelStates::MembershipStates }
          let(:payment_states) { UsersModule::V1::ModelStates::MembershipPaymentStates }
          let(:profile) { FactoryGirl.create(:profile) }
          let(:user) { FactoryGirl.create(:user, profile: profile) }
          let(:benefit) { FactoryGirl.create(:benefit, status: benefit_state::ACTIVE, condition: 'max_address') }
          let(:plan) { FactoryGirl.create(:plan) }
          let(:payment) { FactoryGirl.create(:membership_payment) }
          let(:wrong_payment) { FactoryGirl.create(:membership_payment) }
          let(:membership) { FactoryGirl.create(:membership, plan: plan, workflow_state: membership_state::PAYMENT_PENDING) }
          let(:payment_dao) { UsersModule::V1::MembershipModule::V1::MembershipPaymentDao }
          let(:payment_type) { PaymentModule::V1::PaymentType }
          describe 'new_payment' do
            it 'with valid params' do
              payment_args = {
                grand_total: Faker::Commerce.price,
                billing_amount: Faker::Commerce.price,
                refund: Faker::Commerce.price,
                net_total: Faker::Commerce.price,
              }
              expect(membership_payment_service.new_payment(payment_args)).to be_valid
            end
          end

          describe 'get_payment_by_id' do
            it' with valid args' do
              payment_model.any_instance.should_receive(:get_payment_by_id).and_return(payment)
              expect(membership_payment_service.get_payment_by_id({ id: 1 })).to be_valid
            end

            it' with invalid args' do
              payment_model.any_instance.should_receive(:get_payment_by_id).and_raise(custom_error_util::
                InvalidArgumentsError)
              expect{ membership_payment_service.get_payment_by_id({ id: 1 }) }.to raise_error(
                custom_error_util::ResourceNotFoundError)
            end
          end

          describe 'calculate_membership_payment' do
            it 'with plan = membership.plan' do
              plan = FactoryGirl.build(:plan)
              membership = FactoryGirl.build(:membership, plan: plan)
              args = membership_payment_service.calculate_membership_payment(membership, plan)
              expect(args[:payment_args][:refund]).to eql(0.0)
            end

            it 'with plan != membership.plan' do
              plan = FactoryGirl.build(:plan)
              another_plan = FactoryGirl.build(:plan)
              membership = FactoryGirl.build(:membership, plan: another_plan)
              args = membership_payment_service.calculate_membership_payment(membership, plan)
              expect(args[:payment_args][:refund]).to eql(another_plan.fee.to_f)
            end
          end

          describe 'refund_membership_payment(membership)' do
            it 'with valid' do
              payment_args = membership_payment_service.refund_membership_payment(membership)
              expect(payment_args[:payment_type]).to eql(payment_type.get_id_by_key('REFUND'))
            end
          end

          describe 'final_membership_payment' do
            let(:plan) { FactoryGirl.build(:plan) }
            let(:membership) { FactoryGirl.build(:membership, membership_payments: [FactoryGirl.build(:membership_payment)], plan: plan) }
            let(:payment_args) { { mode: 1 } }
            it 'with valid params' do
              expect_any_instance_of(payment_dao).to receive(:get_pending_payment).and_return(payment)
              response = membership_payment_service.final_membership_payment(membership, payment_args)
              expect(response).to eql(true)
            end
            it 'with invalid params' do
              # Currently, mode is not used and mode_savings are by default 0
              # Hence no need of this spec
              #expect{ membership_payment_service.final_membership_payment(membership, payment_args.merge(mode: 10)) }.to raise_error(
              #  custom_error_util::InvalidArgumentsError)
            end
          end

          describe 'handle_payment' do
            let(:args) {
              {
                user_id: user.id,
                payment_id: payment.id
              }
            }
            let(:wrong_args) {
              {
                user_id: user.id,
                payment_id: wrong_payment.id
              }
            }
            it 'when transaction_status = Success' do
              expect_any_instance_of(payment_dao).to receive(:get_payment_from_id).and_return(payment)
              expect_any_instance_of(membership_service).to receive(:success_membership_payment).and_return(membership)
              response = membership_payment_service.handle_payment(transaction_status::SUCCESS, args)
              expect(response[:payment].status).to eql(payment_states::SUCCESS)
            end

            it 'when transaction_status = FAILED' do
              expect_any_instance_of(payment_dao).to receive(:get_payment_from_id).and_return(payment)
              response = membership_payment_service.handle_payment(transaction_status::FAILED, args)
              expect(response[:payment].status).to eql(payment_states::FAILED)
            end

            it 'when transaction_status = CANCELLED' do
              expect_any_instance_of(payment_dao).to receive(:get_payment_from_id).and_return(payment)
              response = membership_payment_service.handle_payment(transaction_status::CANCELLED, args)
              expect(response[:payment].status).to eql(payment_states::FAILED)
            end

            it 'when transaction_status = Success and wrong payment id' do
              expect_any_instance_of(membership_dao).to receive(:get_on_hold_membership).and_return(membership)
              expect{ membership_payment_service.handle_payment(transaction_status::SUCCESS, wrong_args) }.to raise_error(
                custom_error_util::InvalidArgumentsError)
            end

            it 'when transaction_status = FAILED and wrong payment id' do
              expect{ membership_payment_service.handle_payment(transaction_status::FAILED, wrong_args) }.to_not raise_error
            end

            it 'when transaction_status = CANCELLED and wrong payment id' do
              expect{ membership_payment_service.handle_payment(transaction_status::CANCELLED, wrong_args) }.to_not raise_error
            end
          end
        end
      end
    end
  end
end
