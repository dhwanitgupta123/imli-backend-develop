module UsersModule
  module V1
    module ProvidersModule
      module V1
        require 'rails_helper'
        RSpec.describe UsersModule::V1::ProvidersModule::V1::GenericProviderService do
          let(:generic_service) { UsersModule::V1::ProvidersModule::V1::GenericProviderService.new }
          let(:custom_error_util) { CommonModule::V1::CustomErrors }

          it "creates new generic object successfully " do
            password = Faker::Lorem.characters(8)
            expect(generic_service.new_generic_provider(password)).to_not be_nil
          end
          
          it "if password not present" do
            password = ""
            expect{ generic_service.new_generic_provider(password)}.to raise_error(custom_error_util::InsufficientDataError)
          end
        end
      end
    end
  end
end