module UsersModule
  module V1
    module ProvidersModule
      module V1
        require 'rails_helper'
        RSpec.describe UsersModule::V1::ProvidersModule::V1::MessageProviderService do
          let(:message_service) { UsersModule::V1::ProvidersModule::V1::MessageProviderService.new }
          let(:message_class_path) { UsersModule::V1::ProvidersModule::V1 }
          let(:custom_error_util) { CommonModule::V1::CustomErrors }

          it "creates new message object successfully " do
            expect(message_service.new_message_provider).to_not be_nil
          end

          it "updates message object successfully " do
            message = FactoryGirl.build_stubbed(:message)
            message_class_path::MessageProviderService.any_instance.stub(:update_message_provider).and_return(message)
            expect{ message_service.create_update_message_provider(message)}.to_not raise_error
          end

          it "verifies otp successfully" do
            otp = 5674
            message  = message_class_path::Message.new(:otp => 5674, :expires_at => Time.zone.now + 5.minutes)
            message_service.verify_otp(otp, message).should be_truthy
          end

          it "otp doesn't matches" do
            otp = 5673
            message  = message_class_path::Message.new(:otp => 5674, :expires_at => Time.zone.now + 5.minutes)
            expect{ message_service.verify_otp(otp, message) }.to raise_error(
              custom_error_util::WrongOtpError)
          end

          it "otp has expired " do
            otp = 5674
            message  = message_class_path::Message.new(:otp => 5674, :expires_at => Time.zone.now - 5.minutes)
            expect{ message_service.verify_otp(otp, message) }.to raise_error(
              custom_error_util::AuthenticationTimeoutError)
          end

          it "when resend count has become equal to Maximum limit" do
            message  = FactoryGirl.build_stubbed(:message, resend_count: 10, expires_at: Time.zone.now + 2.minutes)
            expect{ message_service.create_update_message_provider(message) }.to raise_error(
              custom_error_util::ReachedMaximumLimitError)
          end
        end
      end
    end
  end
end