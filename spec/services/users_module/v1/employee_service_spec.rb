module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::EmployeeService do
      
      # Define all used services, helpers, utils at global scope
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:content_util) { CommonModule::V1::Content }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:provider_type_util) { UsersModule::V1::ProviderType }

      let(:provider_type_util) { UsersModule::V1::ProviderType }
      let(:employee_service) { UsersModule::V1::EmployeeService.new(nil)}
      let(:user_service_class) { UsersModule::V1::UserService }
      let(:employee_service_class) { UsersModule::V1::EmployeeService }
      let(:employee_model) { UsersModule::V1::Employee }
      let(:plan_service) { CommonModule::V1::PlanService }
      let(:membership_service) { UsersModule::V1::MembershipService}

      let(:message) { FactoryGirl.build_stubbed(:message) }
      let(:generic) { FactoryGirl.build_stubbed(:generic) }
      let(:provider) { FactoryGirl.build_stubbed(:provider, message: message) }
      let(:user) { FactoryGirl.build(:user, provider: provider) }
      let(:employee) { FactoryGirl.build(:employee, user: user) }

      let(:manager_provider) { FactoryGirl.build_stubbed(:provider, generic: generic)}
      let(:manager_user) { FactoryGirl.build(:user, provider: manager_provider)}
      let(:manager) { FactoryGirl.build(:employee) }
      let(:plan) { FactoryGirl.build_stubbed(:plan) }
      let(:membership) { FactoryGirl.build_stubbed(:membership) }

      let(:email) { FactoryGirl.build_stubbed(:email, :account_type => AccountType::EMPLOYEE) }
      let(:invalid_phone_number) { "12345" }
      let(:request_type) { provider_type_util::GENERIC }
      let(:membership_args) { { membership: membership } }
      let(:valid_employee_params) {
          {
          :team => Faker::Lorem.characters(5),
          :designation => Faker::Lorem.characters(5),
          :manager_phone_number => Faker::Number.number(10).to_s
          }
        }
      let(:valid_user_params) {
        {
        :phone_number => Faker::Number.number(10).to_s,
        :password => Faker::Internet.password(8),
        :email_id => Faker::Internet.free_email,
        :first_name => Faker::Lorem.characters(7),
        :last_name => Faker::Lorem.characters(7)
        }
      }

      let(:valid_employee_request) {
        {
        employee_params: valid_employee_params,
        user_params: valid_user_params
        }
      }

      describe "register employee" do
        it "if no data passed" do
          data = employee_service.register(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it "if invalid phone number passed" do
          valid_employee_request[:user_params][:phone_number] = invalid_phone_number
          data = employee_service.register(valid_employee_request)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it "if invalid manager phone number passed" do
          valid_employee_request[:employee_params][:manager_phone_number] = invalid_phone_number
          data = employee_service.register(valid_employee_request)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it "if no email id passed" do
          valid_employee_request[:user_params][:email_id] = nil
          data = employee_service.register(valid_employee_request)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it "if no team or designation is passed" do
          valid_employee_request[:employee_params][:team] = nil
          data = employee_service.register(valid_employee_request)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        context "with all parameters" do
          
          it "and phone number of already existing employee" do
            existing_employee = employee
            user_service_class.any_instance.stub(:get_user_from_phone_number).with(
              valid_user_params[:phone_number]).and_return(user)
            user_service_class.any_instance.stub(:get_employee_for_user).with(user).and_return(existing_employee)
            
            data = employee_service.register(valid_employee_request)
            expect(data[:error][:message]).to eq(content_util::EXISTING_EMPLOYEE)
            expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
          end

          it "and manager phone number of invalid employee" do
          plan_service.any_instance.should_receive(:get_plan_by_key).and_return(plan)
          expect_any_instance_of(membership_service).to receive(:subscribe_user_to_membership).and_return(membership_args)
            user_service_class.any_instance.stub(:get_user_from_phone_number){
              raise custom_error_util::EmployeeNotFoundError.new
            }
            
            data = employee_service.register(valid_employee_request)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
            
          end

          context "and valid details" do
            before(:example){
              # for non existing employee
              user_service_class.any_instance.stub(:get_user_from_phone_number).with(valid_user_params[:phone_number]){
                raise custom_error_util::EmployeeNotFoundError.new
              }
              # for valid existing manager
              user_service_class.any_instance.stub(:get_user_from_phone_number).with(
                valid_employee_params[:manager_phone_number]).and_return(manager_user)
              user_service_class.any_instance.stub(:get_employee_for_user).with(manager_user).and_return(manager)
            }

            it "but user registration failed" do
              user_service_class.any_instance.should_receive(:decide_register_login_for_user){
                raise custom_error_util::RunTimeError.new
              }
              data = employee_service.register(valid_employee_request)
              expect(data[:error][:message]).to eq(content_util::RUN_TIME_ERROR)
              expect(data[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
            end

            it "with already registered user and generic provider" do
              user_service_class.any_instance.should_receive(:decide_register_login_for_user){
                raise custom_error_util::ExistingUserError.new
              }
              data = employee_service.register(valid_employee_request)
              expect(data[:error][:message]).to eq(content_util::EXISTING_USER)
              expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
            end

            context "and valid user registration" do
              before(:example){
                # for successfull user registration
                user_service_class.any_instance.should_receive(:decide_register_login_for_user).and_return(user)
              }

              it "with invalid email registration" do
                valid_employee_request[:user_params][:email_id] = "@test.com"
                data = employee_service.register(valid_employee_request)
                expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
              end

              it "with valid email registration but invalid employee creation" do
                user_service_class.any_instance.should_receive(:update_user_info_details).and_return(user)
                employee_model.stub(:create_employee).and_raise(custom_error_util::RunTimeError)
                data = employee_service.register(valid_employee_request)
                expect(data[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
              end

              it "and valid employee registration" do
                user_service_class.any_instance.should_receive(:update_user_info_details).and_return(user)
                employee_service.should_receive(:create_employee_with_details).and_return(employee)
                employee_model.any_instance.should_receive(:set_state_to_pending).and_return(true)
                # Temporarily FIX for automatic approval of employee
                user_service_class.any_instance.stub(:get_email_with_type).and_return(email)
                employee_service_class.any_instance.stub(:approve_employee).and_return(true)
                data = employee_service.register(valid_employee_request)
                expect(data[:response]).to eq(response_codes_util::SUCCESS)
              end
            end
          end

        end
      end

      describe "approve employee" do
        let(:token) { Faker::Lorem.characters(20) }
        let(:example) { employee.set_state_to_pending }
        it "if no params passed" do
          data = employee_service.approve(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it "when token not associated with any user" do
          user_service_class.any_instance.should_receive(:authenticate_email).with(token){
            raise custom_error_util::InvalidDataError.new
          }
          data = employee_service.approve({token: token})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it "when token is associated with user but no employee is found" do
          user_service_class.any_instance.should_receive(:authenticate_email).with(token).and_return(user)
          user_service_class.any_instance.should_receive(:get_employee_for_user).with(user){
            raise custom_error_util::EmployeeNotFoundError.new
          }
          data = employee_service.approve({token: token})
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        context "with token associated with valid employee" do
          before(:example) {
            user_service_class.any_instance.should_receive(:authenticate_email).with(token).and_return(user)
            user_service_class.any_instance.should_receive(:get_employee_for_user).with(user).and_return(employee)
          }
          it "but its state was inactive" do
            employee_model.any_instance.stub(:inactive?).and_return(true)
            data = employee_service.approve({token: token})
            expect(data[:error][:message]).to eq(content_util::INACTIVE_STATE)
            expect(data[:response]).to eq(response_codes_util::PRE_CONDITION_REQUIRED)
          end

          it "and state as pending" do
            employee_model.any_instance.stub(:inactive?).and_return(false)
            # since its not iteractive with the db, we can just verify that
            # set_state_to_active function is called
            employee_model.any_instance.should_receive(:set_state_to_active)

            data = employee_service.approve({token: token})
            expect(data[:response]).to eq(response_codes_util::SUCCESS)
          end
        end
      end

      describe "login employee" do
        let(:session_token) { Faker::Lorem.characters(10) }
        let(:valid_user_params) {
          {
          :phone_number => Faker::Number.number(10).to_s,
          :password => Faker::Internet.password(8),
          } 
        }
        let(:valid_employee_credentials) {
          {
          employee_credentials: valid_user_params
          }
        }
        let(:valid_user_response) {
          {
          user_params: valid_user_params,
          request_type: provider_type_util::GENERIC
          }
        }

        it "if no data passed" do
          data = employee_service.login(nil)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it "if no phone number passed" do
          valid_employee_credentials[:employee_credentials][:phone_number] = nil
          data = employee_service.login(valid_employee_credentials)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it "if no password passed" do
          valid_employee_credentials[:employee_credentials][:password] = nil
          data = employee_service.login(valid_employee_credentials)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it "if invalid phone number passed" do
          valid_employee_credentials[:employee_credentials][:phone_number] = invalid_phone_number
          data = employee_service.login(valid_employee_credentials)
          expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        context "with valid parameters" do

          it "and invalid employee" do
            user_service_class.any_instance.should_receive(:get_user_from_phone_number).with(
              valid_user_params[:phone_number]){
              raise custom_error_util::UserNotFoundError.new
            }
            data = employee_service.login(valid_employee_credentials)
            expect(data[:error][:message]).to eq(content_util::WRONG_PHONE_NUMBER)
            expect(data[:response]).to eq(response_codes_util::UN_AUTHORIZED)
          end

          it "and valid but not active employee" do
            user_service_class.any_instance.should_receive(:get_user_from_phone_number).with(
              valid_user_params[:phone_number]).and_return(user)
            user_service_class.any_instance.should_receive(:get_employee_for_user).with(user).and_return(employee)
            
            employee_model.any_instance.should_receive(:active?).and_return(false)
            data = employee_service.login(valid_employee_credentials)
            expect(data[:response]).to eq(response_codes_util::PRE_CONDITION_REQUIRED)
          end

          context "and active employee" do
            before(:example){
              user_service_class.any_instance.should_receive(:get_user_from_phone_number).with(
                valid_user_params[:phone_number]).and_return(user)
              user_service_class.any_instance.should_receive(:get_employee_for_user).with(user).and_return(employee)
              
              employee_model.any_instance.should_receive(:active?).and_return(true)
            }

            it "but wrong password" do
              user_service_class.any_instance.should_receive(:login_for_user).with(
                {user_params: valid_user_params, request_type: request_type}){
                raise custom_error_util::WrongPasswordError.new
              }
              data = employee_service.login(valid_employee_credentials)
              expect(data[:error][:message]).to eq(content_util::WRONG_PASSWORD)
              expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
            end

            it "when user service failed to process the request" do
              user_service_class.any_instance.should_receive(:login_for_user).with(
                {user_params: valid_user_params, request_type: request_type}){
                raise custom_error_util::RunTimeError.new
              }
              data = employee_service.login(valid_employee_credentials)
              expect(data[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
            end

            it "and currect password" do
              user_service_class.any_instance.should_receive(:login_for_user).with(
                {user_params: valid_user_params, request_type: request_type}).and_return(session_token)
              data = employee_service.login(valid_employee_credentials)
              expect(data[:response]).to eq(response_codes_util::SUCCESS)
            end
          end
        end
      end

      describe "add employee profile" do
        context "with invalid parameters" do
          before(:example){
            # for a valid User session
            user_service_class.any_instance.stub(:get_user_from_user_id).with(user){
              raise custom_error_util::UnAuthenticatedUserError.new
            }
            employee_service.should_receive(:find_user_using_session).and_return(user)
          }
          it "if no data passed" do
            data = employee_service.add_profile(nil)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it "if invalid manager phone number passed" do
            valid_employee_request[:employee_params][:manager_phone_number] = invalid_phone_number
            data = employee_service.add_profile(valid_employee_request)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end

          it "if no team or designation is passed" do
            valid_employee_request[:employee_params][:team] = nil
            data = employee_service.add_profile(valid_employee_request)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
          end
        end

        context "with all parameters" do

          it "and manager phone number of invalid employee" do
            user_service_class.any_instance.stub(:get_user_from_user_id).with(user){
              raise custom_error_util::UnAuthenticatedUserError.new
            }
            employee_service.should_receive(:find_user_using_session).and_return(user)
            user_service_class.any_instance.stub(:get_user_from_phone_number){
              raise custom_error_util::EmployeeNotFoundError.new
            }
            
            data = employee_service.add_profile(valid_employee_request)
            expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
            
          end

          context "and valid details" do
            before(:example){
              # for valid existing manager
              employee_service.should_receive(:find_user_using_session).and_return(user)
              user_service_class.any_instance.stub(:get_user_from_user_id).with(user)
              user_service_class.any_instance.stub(:get_user_from_phone_number).with(
                valid_employee_params[:manager_phone_number]).and_return(manager_user)
              user_service_class.any_instance.stub(:get_employee_for_user).with(manager_user).and_return(manager)
            }

            it "but user registration failed" do
              user_service_class.any_instance.stub(:update_user_info_details).and_raise(
                custom_error_util::RunTimeError)
              data = employee_service.add_profile(valid_employee_request)
              expect(data[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
            end

            context "and valid user registration" do
              it "with invalid email registration" do
                valid_employee_request[:user_params][:email_id] = "@test.com"
                data = employee_service.add_profile(valid_employee_request)
                expect(data[:error][:message]).to eq(content_util::INVALID_EMAIL_ID)
                expect(data[:response]).to eq(response_codes_util::BAD_REQUEST)
              end

              it "with valid email registration but invalid employee creation" do
                user_service_class.any_instance.stub(:update_user_info_details).and_return(user)
                employee_model.stub(:create_employee).and_raise(custom_error_util::RunTimeError.new)
                data = employee_service.add_profile(valid_employee_request)
                expect(data[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
              end

              it "and valid employee registration" do
                user_service_class.any_instance.stub(:update_user_info_details).and_return(user)
                employee_service.should_receive(:create_employee_with_details).and_return(employee)
                data = employee_service.add_profile(valid_employee_request)
                expect(data[:response]).to eq(response_codes_util::SUCCESS)
              end
            end
          end
        end
      end

    end
  end
end