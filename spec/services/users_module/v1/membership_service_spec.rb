module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::MembershipService do
      let(:version_params) { { version: 1 } }

      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:content_util) { CommonModule::V1::Content }
      let(:plan_service) { CommonModule::V1::PlanService }
      let(:membership_service) { UsersModule::V1::MembershipService.new(version_params) }
      let(:membership_state) { UsersModule::V1::ModelStates::MembershipStates }
      let(:plan_state) { CommonModule::V1::ModelStates::V1::PlanStates}
      let(:user_service) { UsersModule::V1::UserService }
      let(:membership_model) { UsersModule::V1::Membership }
      let(:payment_service) { UsersModule::V1::MembershipModule::V1::MembershipPaymentService }
      let(:membership_dao) { UsersModule::V1::MembershipDao }
      let(:user_notif_service) { UsersModule::V1::UserNotificationService }
      # Using FactoryGirl.build_stubbed because user.id is needed in test cases
      # which can only be provided by FG.create OR FG.build_stubbed
      let(:profile) { FactoryGirl.create(:profile) }
      let(:user) { FactoryGirl.create(:user, profile: profile) }
      let(:benefit) { FactoryGirl.create(:benefit, status: benefit_state::ACTIVE, condition: 'max_address') }
      let(:plan) { FactoryGirl.create(:plan) }
      let(:payment) { FactoryGirl.create(:membership_payment) }
      let(:wrong_payment) { FactoryGirl.create(:membership_payment) }
      let(:membership) { FactoryGirl.create(:membership, plan: plan, workflow_state: membership_state::ACTIVE) }
      let(:transaction) { FactoryGirl.create(:transaction) }
      let(:subscribe_request) { [{ plan: { id: plan.id } }] }
      let(:transaction_status) { TransactionsModule::V1::TransactionStatus }
      let(:stub_ok_response) {
        {
          user: {
            memberships: [{
              id: 1,
              status: 1,
              plan: {
                name: 'test',
                benefits: [
                  {
                    name: 'test'
                  }
                ]
              }
            }]
          }
        }
      }

      describe 'subscribe_to_membership' do
        it 'when empty params' do
          response = membership_service.subscribe_to_membership([{}])
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'when plan is inactive' do
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          expect_any_instance_of(plan_service).to receive(:get_plan_by_id).and_return(plan)
          response = membership_service.subscribe_to_membership(subscribe_request)
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'when plan is ACTIVE & membership of same plan is active' do
          new_membership = membership
          new_membership.workflow_state = membership_state::INACTIVE
          new_membership.save
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          expect_any_instance_of(membership_model).to receive(:new_membership).and_return(new_membership)
          expect_any_instance_of(membership_model).to receive(:save_membership).and_return(membership)
          payment_service.any_instance.should_receive(:new_payment).and_return(payment)
          plan = FactoryGirl.build_stubbed(:plan, status: plan_state::ACTIVE)
          membership = FactoryGirl.build_stubbed(:membership, plan: plan, workflow_state: membership_state::ACTIVE)
          expect_any_instance_of(membership_dao).to receive(:get_only_active_membership).and_return(membership)
          expect_any_instance_of(plan_service).to receive(:get_plan_by_id).and_return(plan)
          response =  membership_service.subscribe_to_membership(subscribe_request)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'when plan is ACTIVE & membership is in intermediate state but of diff plan' do
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          expect_any_instance_of(membership_model).to receive(:new_membership).and_return(membership)
          expect_any_instance_of(membership_model).to receive(:save_membership).and_return(membership)
          membership = FactoryGirl.build_stubbed(:membership, plan: plan, workflow_state: membership_state::PENDING)
          plan = FactoryGirl.build_stubbed(:plan, status: plan_state::ACTIVE)
          expect_any_instance_of(membership_dao).to receive(:get_only_active_membership).and_return(membership)
          membership_service.stub(:change_state).and_return(membership)
          expect_any_instance_of(plan_service).to receive(:get_plan_by_id).and_return(plan)
          response =  membership_service.subscribe_to_membership(subscribe_request)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'when no membership is associated' do
          expect_any_instance_of(membership_model).to receive(:new_membership).and_return(membership)
          expect_any_instance_of(membership_model).to receive(:save_membership).and_return(membership)
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          membership = FactoryGirl.build_stubbed(:membership, plan: plan, workflow_state: membership_state::PENDING)
          plan = FactoryGirl.build_stubbed(:plan, status: plan_state::ACTIVE)
          expect_any_instance_of(membership_dao).to receive(:get_only_active_membership).and_return(nil)
          expect_any_instance_of(plan_service).to receive(:get_plan_by_id).and_return(plan)
          membership_service.should_receive(:change_state).and_return(membership)
          response =  membership_service.subscribe_to_membership(subscribe_request)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'when membership is expired' do
          expect_any_instance_of(membership_model).to receive(:new_membership).and_return(membership)
          expect_any_instance_of(membership_model).to receive(:save_membership).and_return(membership)
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          membership = FactoryGirl.build_stubbed(:membership, plan: plan, workflow_state: membership_state::EXPIRED)
          plan = FactoryGirl.build_stubbed(:plan, status: plan_state::ACTIVE)
          expect_any_instance_of(membership_dao).to receive(:get_only_active_membership).and_return(membership)
          expect_any_instance_of(plan_service).to receive(:get_plan_by_id).and_return(plan)
          membership_service.should_receive(:change_state).and_return(membership)
          response =  membership_service.subscribe_to_membership(subscribe_request)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'payment_initiate' do
        request = [
          {
            payment:{
              mode: 1
            }
          }
        ]
        it 'when blank request' do
          response = membership_service.payment_initiate([{}])
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'when mode is blank' do
          response = membership_service.payment_initiate([{ payment: { mode: nil } }])
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'when valid params' do
          payment_pending_membership = membership
          payment_pending_membership.workflow_state = membership_state::PAYMENT_PENDING
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          expect_any_instance_of(membership_dao).to receive(:get_payment_pending_membership).and_return(payment_pending_membership)
          payment_service.any_instance.should_receive(:final_membership_payment).and_return(payment)
          response = membership_service.payment_initiate(request)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end
      end

      describe 'payment_success' do
        request = [
          {
            payment:{
              id: 1
            }
          }
        ]
        it 'when blank request' do
          response = membership_service.payment_success([{}])
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'when mode is blank' do
          response = membership_service.payment_success([{ payment: { id: nil } }])
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'when valid params' do
          on_hold_membership = membership
          on_hold_membership.workflow_state = membership_state::ON_HOLD
          membership.membership_payments << payment
          membership.users << user
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          expect_any_instance_of(membership_dao).to receive(:get_on_hold_membership).and_return(on_hold_membership)
          payment_service.any_instance.should_receive(:get_payment_by_id).and_return(payment)
          membership_service.should_receive(:expire_active_and_pending_membership).and_return(membership)
          expect_any_instance_of(user_notif_service).to receive(:notify_membership_subscribed).and_return(true)
          response = membership_service.payment_success(request)
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'when wrong payment id' do
          on_hold_membership = membership
          on_hold_membership.workflow_state = membership_state::ON_HOLD
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          expect_any_instance_of(membership_dao).to receive(:get_on_hold_membership).and_return(on_hold_membership)
          payment_service.any_instance.should_receive(:get_payment_by_id).and_return(wrong_payment)
          response = membership_service.payment_success(request)
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end
      end

      describe 'validate' do
        it 'with valid membership' do
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          expect_any_instance_of(membership_dao).to receive(:get_pending_or_active_membership).and_return(membership)
          response = membership_service.validate_membership
          expect(response[:response]).to eq(response_codes_util::SUCCESS)
        end

        it 'with expired membership' do
          expect_any_instance_of(user_service).to receive(:get_user_from_user_id).and_return(user)
          expect_any_instance_of(membership_dao).to receive(:get_pending_or_active_membership).and_return(nil)
          response = membership_service.validate_membership
          expect(response[:response]).to eq(response_codes_util::PRE_CONDITION_REQUIRED)
        end
      end

      describe 'cancel_membership' do
        it 'with blank params' do
          response = membership_service.cancel_membership({})
          expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
        end

        it 'with invalid membership id' do
          expect{ membership_service.cancel_membership({id: Faker::Number.number(1)}) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with valid args and full refund' do
          expect_any_instance_of(membership_dao).to receive(:get_membership_by_id).and_return(membership)
          expect_any_instance_of(payment_service).to receive(:new_payment).and_return(payment)
          expect_any_instance_of(payment_service).to receive(:get_membership_payment_transaction).and_return(transaction)
          expect_any_instance_of(payment_service).to receive(:partial_or_full_refund_transaction).and_return(transaction_status::FULLY_REFUNDED)
          expect_any_instance_of(payment_service).to receive(:refund_membership_amount).and_return(transaction_status::FULLY_REFUNDED)
          expect_any_instance_of(user_notif_service).to receive(:notify_membership_cancelled).and_return(true)
          response = membership_service.cancel_membership({ id: Faker::Number.number(1) })
          expect(response[:response]).to eql(response_codes_util::SUCCESS)
        end

        it 'with valid args and partial refund' do
          expect_any_instance_of(membership_dao).to receive(:get_membership_by_id).and_return(membership)
          expect_any_instance_of(payment_service).to receive(:new_payment).and_return(payment)
          expect_any_instance_of(payment_service).to receive(:get_membership_payment_transaction).and_return(transaction)
          expect_any_instance_of(payment_service).to receive(:partial_or_full_refund_transaction).and_return(transaction_status::PARTIALLY_REFUNDED)
          expect_any_instance_of(payment_service).to receive(:refund_membership_amount).and_return(transaction_status::PARTIALLY_REFUNDED)
          expect_any_instance_of(user_notif_service).to receive(:notify_membership_cancelled).and_return(true)
          response = membership_service.cancel_membership({ id: Faker::Number.number(1) })
          expect(response[:response]).to eql(response_codes_util::SUCCESS)
        end

        it 'with valid args and incorrect txn status' do
          expect_any_instance_of(membership_dao).to receive(:get_membership_by_id).and_return(membership)
          expect_any_instance_of(payment_service).to receive(:new_payment).and_return(payment)
          expect_any_instance_of(payment_service).to receive(:get_membership_payment_transaction).and_return(transaction)
          expect_any_instance_of(payment_service).to receive(:partial_or_full_refund_transaction).and_return(transaction_status::FULLY_REFUNDED)
          expect_any_instance_of(payment_service).to receive(:refund_membership_amount).and_return(transaction_status::PARTIALLY_REFUNDED)
          response = membership_service.cancel_membership({ id: Faker::Number.number(1) })
          expect(response[:response]).to eql(response_codes_util::BAD_REQUEST)
        end

        it 'with no transaction' do
          expect_any_instance_of(membership_dao).to receive(:get_membership_by_id).and_return(membership)
          expect_any_instance_of(payment_service).to receive(:new_payment).and_return(payment)
          expect_any_instance_of(payment_service).to receive(:get_membership_payment_transaction).and_return(nil)
          expect_any_instance_of(payment_service).to receive(:partial_or_full_refund_transaction).and_return(transaction_status::FULLY_REFUNDED)
          response = membership_service.cancel_membership({ id: Faker::Number.number(1) })
          expect(response[:response]).to eql(response_codes_util::BAD_REQUEST)
        end
      end

      # describe 'payment_failed' do
      #   request = [
      #     {
      #       payment:{
      #         id: 1
      #       }
      #     }
      #   ]
      #   it 'when blank request' do
      #     response = membership_service.payment_failed([{}])
      #     expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
      #   end

      #   it 'when mode is blank' do
      #     response = membership_service.payment_failed([{ payment: { id: nil } }])
      #     expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
      #   end

      #   it 'when valid params' do
      #     payment_pending_membership = membership
      #     payment_pending_membership.workflow_state = membership_state::PAYMENT_PENDING
      #     membership.membership_payments << payment
      #     user_service.any_instance.should_receive(:get_user_from_user_id).and_return(user)
      #     expect_any_instance_of(membership_dao).to receive(:get_payment_pending_membership).and_return(payment_pending_membership)
      #     payment_service.any_instance.should_receive(:get_payment_by_id).and_return(payment)
      #     response = membership_service.payment_failed(request)
      #     expect(response[:response]).to eq(response_codes_util::SUCCESS)
      #   end

      #   it 'when wrong payment id' do
      #     payment_pending_membership = membership
      #     payment_pending_membership.workflow_state = membership_state::PAYMENT_PENDING
      #     user_service.any_instance.should_receive(:get_user_from_user_id).and_return(user)
      #     expect_any_instance_of(membership_dao).to receive(:get_payment_pending_membership).and_return(payment_pending_membership)
      #     payment_service.any_instance.should_receive(:get_payment_by_id).and_return(wrong_payment)
      #     response = membership_service.payment_failed(request)
      #     expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
      #   end
      # end
    end
  end
end
