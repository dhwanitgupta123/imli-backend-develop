#
# Module to handle all the functionalities related to description
#
module SettingsModule
  #
  # Version1 for description module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SettingsModule::V1::SettingService do
      let(:settings_service) { SettingsModule::V1::SettingService.new({}) }
      let(:feature_settings_service) { SettingsModule::V1::FeatureSettingsService }
      let(:constant_settings_service){ SettingsModule::V1::ConstantSettingsService }

      let(:map_settings_class) { SettingsModule::V1::StaticSettings::MapSettings }
      let(:feature_settings_class) { SettingsModule::V1::StaticSettings::FeatureSettings }
      let(:constant_settings_class) { SettingsModule::V1::StaticSettings::ConstantSettings }
      let(:dynamic_settings_class) { SettingsModule::V1::DynamicSettings::DynamicSettings }
      let(:shield_service_class) { SettingsModule::V1::ShieldService }
      let(:release_management_service_class) { ReleaseManagementModule::V1::ReleaseManagementService }

      context 'get static settings' do
        let(:map_settings) {
          {
            'DUMMY_STATES': Faker::Lorem.word
          }
        }
        let(:feature_settings) {
          {
            'DUMMY_FEATURE': Faker::Number.number(2)
          }
        }
        let(:constant_settings) {
          {
            'DUMMY_CONSTANT': Faker::Number.number(2)
          }
        }
        it 'with valid settings present' do
          expect_any_instance_of(map_settings_class).to receive(:fetch).
            and_return(map_settings)
          expect_any_instance_of(feature_settings_class).to receive(:fetch).
            and_return(feature_settings)
          expect_any_instance_of(constant_settings_class).to receive(:fetch).
            and_return(constant_settings)
          response = settings_service.get_all_static_settings

          expect(response['MAP']).to eq(map_settings)
          expect(response['FEATURES']).to eq(feature_settings)
          expect(response['CONSTANTS']).to eq(constant_settings)
        end

        it 'with NO settings present' do
          expect_any_instance_of(map_settings_class).to receive(:fetch).
            and_return({})
          expect_any_instance_of(feature_settings_class).to receive(:fetch).
            and_return({})
          expect_any_instance_of(constant_settings_class).to receive(:fetch).
            and_return({})
          response = settings_service.get_all_static_settings

          expect(response['MAP']).to eq({})
          expect(response['FEATURES']).to eq({})
          expect(response['CONSTANTS']).to eq({})
        end
      end

      describe 'get dynamic settings' do
        let(:treatment_value) { Faker::Number.number(2) }
        let(:dynamic_settings) {
          {
            'DUMMY_KEY': 'DUMMY_EXPERIMENT_NAME'
          }
        }
        let(:user_specific_features) {
          {
            'DUMMY_KEY': treatment_value
          }
        }
        let(:user_constants) {
          {
          'DUMMY_CONSTANT': Faker::Number.number(2)
          }
        }
        let(:args){
          {
            'platform': 'ANDROID',
            'version_code': Faker::Number.between(0,50)
          }
        }

        it 'with valid dynamic settings present' do
          expect_any_instance_of(dynamic_settings_class).to receive(:fetch).
            and_return(dynamic_settings)
          expect(feature_settings_service).to receive(:fetch_user_specific_features).
            with(dynamic_settings).and_return(user_specific_features)
          expect(constant_settings_service).to receive(:fetch_user_specific_constants).
            and_return(user_constants)

          response = settings_service.get_all_dynamic_settings(args)

          expect(response['FEATURES'][:DUMMY_KEY]).to eq(treatment_value)
        end

        it 'with invalid treatment' do
          expect_any_instance_of(dynamic_settings_class).to receive(:fetch).
            and_return(dynamic_settings)
          expect(feature_settings_service).to receive(:fetch_user_specific_features).
            with(dynamic_settings).and_return({'DUMMY_KEY': '0'})
          expect(constant_settings_service).to receive(:fetch_user_specific_constants).
            and_return(user_constants)

          response = settings_service.get_all_dynamic_settings(args)

          expect(response['FEATURES'][:DUMMY_KEY]).to eq('0')
        end

        context 'with valid treatment' do

          before(:example){
            expect_any_instance_of(dynamic_settings_class).to receive(:fetch).
              and_return(dynamic_settings)
            expect(feature_settings_service).to receive(:fetch_user_specific_features).
              with(dynamic_settings).and_return(user_specific_features)
            expect(constant_settings_service).to receive(:fetch_user_specific_constants).
              and_return(user_constants)
          }
          let(:app_update_popup_content){
            {
              'title': Faker::Lorem.word,
              'description': Faker::Lorem.sentence,
              'change_log': Faker::Lorem.sentences
            }
          }

          it 'and valid app update popup component present' do
            expect_any_instance_of(release_management_service_class).to receive(:get_app_update_popup_content).
              with({platform: args[:platform], version_code: args[:version_code]}).
              and_return(app_update_popup_content)

            response = settings_service.get_all_dynamic_settings(args)

            expect(response['COMPONENTS'][:POPUPS]).to include(app_update_popup_content)
          end

          it 'and NO app update popup component present' do
            expect_any_instance_of(release_management_service_class).to receive(:get_app_update_popup_content).
              with({platform: args[:platform], version_code: args[:version_code]}).
              and_return({})

            response = settings_service.get_all_dynamic_settings(args)

            expect(response['COMPONENTS'][:POPUPS]).to_not include(app_update_popup_content)
          end

        end

        
      end

    end
  end
end
