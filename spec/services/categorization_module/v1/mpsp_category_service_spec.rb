module CategorizationModule
  module V1
    require "rails_helper"
    RSpec.describe CategorizationModule::V1::MpspCategoryService do
      let(:version) { 1 }
      let(:mpsp_category_service) { CategorizationModule::V1::MpspCategoryService.new(version) }
      let(:mpsp_department_service) { CategorizationModule::V1::MpspDepartmentService }
      let(:mpsp_category_model) {CategorizationModule::V1::MpspCategory}
      let(:mpsp_category_dao) { CategorizationModule::V1::MpspCategoryDao }
      let(:mpsp_department_model) {CategorizationModule::V1::MpspDepartment}
      let(:mpsp_category_params) { { "id": Faker::Number.number(1), "label": Faker::Name.first_name, "mpsp_department_id": Faker::Number.number(1), 'priority': 1 } }
      let(:mpsp_sub_category_params){ { "id": Faker::Number.number(1), "label": Faker::Name.first_name, "mpsp_category_id": Faker::Number.number(1), 'priority': 1 } }
      let(:mpsp_department) {FactoryGirl.create(:mpsp_department)}
      let(:mpsp_category) { FactoryGirl.create(:mpsp_category, mpsp_department: mpsp_department)}
      let(:mpsp_sub_category) { FactoryGirl.create(:mpsp_category, parent_mpsp_category: mpsp_category)}
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      
      describe "create_mpsp_category" do
        it "with correct resquest" do          
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_return(mpsp_department)
          response = mpsp_category_service.create_mpsp_category(mpsp_category_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it "with wrong request" do
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_return(mpsp_department)
          response = mpsp_category_service.create_mpsp_category(mpsp_category_params.merge(label: nil))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it "with wrong request" do
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_return(mpsp_department)
          response = mpsp_category_service.create_mpsp_category(mpsp_category_params.merge(priority: nil))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it "when mpsp_department doesn't exists" do
          response = mpsp_category_service.create_mpsp_category(mpsp_category_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe "create_mpsp_sub_category" do
        it "with correct resquest" do          
          mpsp_category_model.stub(:find_by_mpsp_category_id).and_return(mpsp_category)
          response = mpsp_category_service.create_mpsp_sub_category(mpsp_sub_category_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it "with wrong request" do
          mpsp_sub_category_params = {"label": "","mpsp_category_id": Faker::Number.number(1) }
          mpsp_category_model.stub(:find_by_mpsp_category_id).and_return(mpsp_category)
          response = mpsp_category_service.create_mpsp_sub_category(mpsp_sub_category_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it "when mpsp_category doesn't exists" do
          response = mpsp_category_service.create_mpsp_category(mpsp_sub_category_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe "get_mpsp_category" do
        it "with correct resquest" do
          id = Faker::Number.number(1)
          mpsp_category_model.stub(:find_by_mpsp_category_id).and_return(mpsp_category)
          response = mpsp_category_service.get_mpsp_category(id)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it "when mpsp_category doesn't exists" do
          id = Faker::Number.number(1)
          response = mpsp_category_service.get_mpsp_category(id)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe "get_all_mpsp_categories" do
        let(:paginated_params) { { state: 1, per_page: 20, page_no: 1, sort_by: 'label', order: 'ASC' } }
        it "with correct request" do
          response = mpsp_category_service.get_all_mpsp_categories(paginated_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end

        it "with wrong request" do
          response = mpsp_category_service.get_all_mpsp_categories(paginated_params.merge({ order: 'xyz' }))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'update_mpsp_category' do
        it 'with correct request' do
          mpsp_category_model.stub(:find_by_mpsp_category_id).and_return(mpsp_category)
          mpsp_department_model.stub(:find_by_mpsp_department_id).and_return(mpsp_department)
          mpsp_category_model.any_instance.stub(:update_mpsp_category).and_return(mpsp_category)
          response = mpsp_category_service.update_mpsp_category(mpsp_category_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'when mpsp_department doesnt exists' do
          mpsp_category_model.stub(:find_by_mpsp_category_id).and_return(mpsp_category)
          expect_any_instance_of(mpsp_department_service).to receive(:get_mpsp_department_by_id).
            and_raise(custom_error_util::DepartmentNotFoundError.new)
          response = mpsp_category_service.update_mpsp_category(mpsp_category_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'when mpsp_category doesnt exists' do
          mpsp_category_model.stub(:find_by_mpsp_category_id).and_raise(custom_error_util::CategoryNotFoundError)
          response = mpsp_category_service.update_mpsp_category(mpsp_category_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end

      describe 'change_state' do
        let(:params) { { "id": Faker::Number.number(1), "event": 1 } }
        let(:inavlid_params) { { "id": Faker::Number.number(1), "event": 10 } }
        it 'with correct request' do
          expect(mpsp_category_dao).to receive(:activate_products_within_categories).and_return(true)
          mpsp_category_model.stub(:find_by_mpsp_category_id).and_return(mpsp_category)
          response = mpsp_category_service.change_state(params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end
        it 'when mpsp_category doesnt exists' do
          mpsp_category_model.stub(:find_by_mpsp_category_id).and_raise(custom_error_util::CategoryNotFoundError)
          response = mpsp_category_service.change_state(params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'when event doesnt exists' do
          mpsp_category_model.stub(:find_by_mpsp_category_id).and_return(mpsp_category)
          response = mpsp_category_service.change_state(inavlid_params)
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
        it 'when transition not allowed' do
          mpsp_category_model.stub(:find_by_mpsp_category_id).and_return(mpsp_category)
          mpsp_category_model.any_instance.stub(:trigger_event).and_raise(custom_error_util::PreConditionRequiredError)
          response = mpsp_category_service.change_state(params)
          expect(response[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end
      end

      describe "get_all_mpsp_sub_categories" do
        let(:paginated_params) { { state: 1, per_page: 20, page_no: 1, sort_by: 'label', order: 'ASC' } }
        it "with correct request" do
          response = mpsp_category_service.get_all_mpsp_sub_categories(paginated_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end

        it "with wrong request" do
          response = mpsp_category_service.get_all_mpsp_sub_categories(paginated_params.merge({ order: 'xyz' }))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end
    end
  end
end
