module CategorizationModule
  module V1
    require 'rails_helper'
    RSpec.describe CategorizationApiService do
      let(:version) { 1 }
      let(:categorization_api_service) { CategorizationModule::V1::CategorizationApiService.new(version) }
      let(:response_codes) { CommonModule::V1::ResponseCodes }

      describe 'get_all_departments_details which are active' do
        let(:paginated_params) { { sort_by: 'label' } }
        it 'with correct request' do
          response = categorization_api_service.get_all_departments_details(paginated_params)
          expect(response[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with wrong request' do
          response = categorization_api_service.get_all_departments_details(paginated_params.merge({ order: 'xyz' }))
          expect(response[:response]).to eq(response_codes::BAD_REQUEST)
        end
      end
    end
  end
end
