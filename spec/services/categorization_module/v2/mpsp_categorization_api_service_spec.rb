module CategorizationModule
  module V2
    require 'rails_helper'
    RSpec.describe CategorizationModule::V2::MpspCategorizationApiService do
      let(:version) { 1 }
      let(:mpsp_categorization_api_service) { CategorizationModule::V2::MpspCategorizationApiService.new(version) }
      let(:marketplace_selling_pack_dao) { MarketplaceProductModule::V1::MarketplaceSellingPackDao }
      let(:mpsp_category_service) { CategorizationModule::V2::MpspCategoryService }
      let(:response_codes) { CommonModule::V1::ResponseCodes }

      describe 'get_products_for_mpsp_category' do
        it 'with active entities ' do
          mpsp_category = FactoryGirl.create(:mpsp_category, status: 1)
          mpsp_sub_category = FactoryGirl.create(:mpsp_category, status: 1, mpsp_department: nil, mpsp_parent_category: mpsp_category)
          marketplace_selling_pack = FactoryGirl.create(:marketplace_selling_pack, status: 1, mpsp_sub_category: mpsp_sub_category)
          expect(marketplace_selling_pack_dao).to receive(:is_out_of_stock?).and_return(false)
          expect_any_instance_of(mpsp_category_service).to receive(:get_display_products_for_mpsp_sub_category).and_return([marketplace_selling_pack])
          response = mpsp_categorization_api_service.get_products_for_mpsp_category({ id: mpsp_category.id })
          expect(response[:payload][:products].blank?).to eq(false) 
        end
        
        it 'with inactive entities ' do 
          mpsp_category = FactoryGirl.create(:mpsp_category, status: 1)
          mpsp_sub_category = FactoryGirl.create(:mpsp_category, mpsp_department: nil, mpsp_parent_category: mpsp_category)
          marketplace_selling_pack = FactoryGirl.create(:marketplace_selling_pack, status: 1, mpsp_sub_category: mpsp_sub_category)
          response = mpsp_categorization_api_service.get_products_for_mpsp_category({ id: mpsp_category.id })
          expect(response[:payload][:products].blank?).to eq(true) 
        end
      end
    end
  end
end
