#
# Module to handle all the functionalities related to description
#
module UsersModule
  #
  # Version1 for validate user membership module
  #
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::ValidateUserMembershipApi do
    
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:membership_service) { UsersModule::V1::MembershipService }
      let(:validate_membership_api) { UsersModule::V1::ValidateUserMembershipApi.new }
      let(:membership_response_decorator) { UsersModule::V1::MembershipResponseDecorator }
      let(:plan_response_decorator) { CommonModule::V1::PlanResponseDecorator }
      let(:benefit) { FactoryGirl.build_stubbed(:benefit) }
      let(:plan) { FactoryGirl.build_stubbed(:plan, benefits: [benefit]) }

      context 'enact ' do

        it 'with valid membership' do
          membership_hash = {
            membership: {
              id: 1,
              status: 1,
              plan: {
                name: 'test',
                benefits: [
                  {
                    name: 'test'
                  }
                ]
              }
            }
          }
          stub_valid_membership_response = membership_response_decorator.create_valid_membership_response(membership_hash)
          expect_any_instance_of(membership_service).to receive(:validate_membership).
            and_return(stub_valid_membership_response)
          data = validate_membership_api.enact
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with invalid membership' do
          stub_in_valid_membership_response = plan_response_decorator.create_available_plans_response([plan])
          expect_any_instance_of(membership_service).to receive(:validate_membership).
            and_return(stub_in_valid_membership_response)
          data = validate_membership_api.enact
          expect(data[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end
      end
    end
  end
end
