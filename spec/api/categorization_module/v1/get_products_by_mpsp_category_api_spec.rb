#
# Module to handle all the functionalities related to description
#
module CategorizationModule
  #
  # Version1 for description module
  #
  module V1
    require 'rails_helper'
    RSpec.describe CategorizationModule::V1::GetProductsByMpspCategoryApi do

      let(:mpsp_categorization_api_service_v1) { CategorizationModule::V1::MpspCategorizationApiService }
      let(:mpsp_categorization_api_service_v2) { CategorizationModule::V2::MpspCategorizationApiService }
      let(:get_products_by_mpsp_category_api) { CategorizationModule::V1::GetProductsByMpspCategoryApi.new }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:request) {
        {
          id: Faker::Number.number(1)
        }
      }

      let(:stub_ok_response) {
        {
          payload: {
            filters: {
              categories: []
            },
            products: []
          },
          :response => response_codes::SUCCESS
        }

      }

      context 'enact ' do

        it 'with valid args for app version number <= 18' do
          expect_any_instance_of(get_products_by_mpsp_category_api.class).to receive(:bump_version?).and_return(false)
          expect_any_instance_of(mpsp_categorization_api_service_v1).to receive(:get_products_for_mpsp_category).
            and_return(stub_ok_response)
          data = get_products_by_mpsp_category_api.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with valid args for app version number > 18' do
          expect_any_instance_of(get_products_by_mpsp_category_api.class).to receive(:bump_version?).and_return(true)
          expect_any_instance_of(mpsp_categorization_api_service_v2).to receive(:get_products_for_mpsp_category).
            and_return(stub_ok_response)
          data = get_products_by_mpsp_category_api.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

      end
    end
  end
end
