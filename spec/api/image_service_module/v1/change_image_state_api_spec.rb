#
# Module to handle functionalities related to image upload to S3
#
module ImageServiceModule
  #
  # Version1 for image service module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ImageServiceModule::V1::ChangeImageStateApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:image_service) { ImageServiceModule::V1::ImageService }
      let(:image) { FactoryGirl.build(:image) }
      let(:change_image_state_api) { ImageServiceModule::V1::ChangeImageStateApi.new(version) }

      let(:change_state_request) {
        {
          id: Faker::Number.number(1),
          event: Faker::Number.between(1, 3)
        }
      }

      context 'enact ' do

        it 'with nil event' do
          data = change_image_state_api.enact(change_state_request.merge(event: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(image_service).to receive(:change_image_state).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = change_image_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with precondition error args' do
          expect_any_instance_of(image_service).to receive(:change_image_state).
            and_raise(custom_errors_util::PreConditionRequiredError.new)
          data = change_image_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(image_service).to receive(:change_image_state).
            and_return(image)
          data = change_image_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
