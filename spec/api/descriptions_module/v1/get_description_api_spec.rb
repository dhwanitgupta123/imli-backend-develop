#
# Module to handle all the functionalities related to description
#
module DescriptionsModule
  #
  # Version1 for description module
  #
  module V1
        require 'rails_helper'
    RSpec.describe DescriptionsModule::V1::GetDescriptionApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:description_service) { DescriptionsModule::V1::DescriptionService }
      let(:description) { FactoryGirl.build(:description) }
      let(:get_description_api) { DescriptionsModule::V1::GetDescriptionApi.new(version) }

      let(:description_request) {
        {
          id: Faker::Number.number(1)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = get_description_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end


        it 'with invalid args' do
          expect_any_instance_of(description_service).to receive(:get_description_tree).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_description_api.enact(description_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(description_service).to receive(:get_description_tree).
            and_return(description)
          data = get_description_api.enact(description_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
