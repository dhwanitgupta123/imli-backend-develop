module Imli
  module Filters
    require 'rails_helper'
    RSpec.describe Imli::Filters::ExclusionFilter do
      let(:exclusion_filter) {Imli::Filters::ExclusionFilter}
      let(:tree) {
        [
          {
            id: 1,
            node_name: 'A',
            next: ['Bs'],
            Bs: [
              {
                id: 1,
                node_name: 'B',
              }
            ]
          },
        ]
      }
      let(:filters) {
        {
          includes: {
            node_name: 'A',
            next: {
              node_name: 'B'
            }
          },
          excludes: [ 'B' ]
        }
      }

      context 'return tree' do
        it 'as it is on excludes node is blank' do
          filtered_tree = exclusion_filter.filter({tree: tree, exclude_nodes: nil})
          expect(tree).to eq(filtered_tree)
        end

        it 'blank is input tree is blank' do
          filtered_tree = exclusion_filter.filter({tree: {}, exclude_nodes: filters[:excludes]})
          expect({}).to eq(filtered_tree)
        end

        it  'with excluded nodes' do
          filtered_tree = exclusion_filter.filter({tree: tree, exclude_nodes: filters[:excludes]})
          filter_tree = tree.dup
          filter_tree[0].delete(:Bs)
          expect(filter_tree).to eq(filtered_tree)
        end
      end
    end
  end
end

