module Imli
  module Filters
    require 'rails_helper'
    RSpec.describe Imli::Filters::InclusionFilter do
      let(:inclusion_filter) {Imli::Filters::InclusionFilter}
      let(:tree) {
        [
          {
            id: 1,
            node_name: 'A',
            next: ['Bs'],
            Bs: [
              {
                id: 1,
                node_name: 'B',
              }
            ]
          },
        ]
      }
      let(:filters) {
        {
          includes: {
            node_name: 'A',
            next: {
              node_name: 'B'
            }
          }
        }
      }

      context 'include nodes' do
        it 'when include node is blank' do
          filtered_tree = inclusion_filter.filter({tree: tree, include_nodes: nil})
          expect({}).to eq(filtered_tree)
        end

        it 'when whole tree is present in include node' do
          filtered_tree = inclusion_filter.filter({tree: tree, include_nodes: filters[:includes]})
          expect(tree).to eq(filtered_tree)
        end

        it 'when tree is blank' do
          filtered_tree = inclusion_filter.filter({tree: {}, include_nodes: filters[:includes]})
          expect({}).to eq(filtered_tree)
        end

        it  'when sub nodes is present' do
          sub_include_nodes = filters.dup
          sub_include_nodes[:includes].delete(:next)
          filtered_tree = inclusion_filter.filter({tree: tree, include_nodes: sub_include_nodes[:includes]})
          filter_tree = tree.dup
          filter_tree[0].delete(:Bs)
          expect(filter_tree).to eq(filtered_tree)
        end
      end
    end
  end
end

