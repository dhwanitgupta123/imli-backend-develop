module Imli
  module Filters
    require 'rails_helper'
    RSpec.describe Imli::Filters::FilterTree do
      let(:exclusion_filter) {Imli::Filters::ExclusionFilter}
      let(:inclusion_filter) {Imli::Filters::InclusionFilter}
      let(:filter_tree) {Imli::Filters::FilterTree}
      let(:tree) {
        [
          {
            id: 1,
            node_name: 'A',
            next: ['Bs'],
            Bs: [
              {
                id: 1,
                node_name: 'B',
              }
            ]
          },
        ]
      }
      let(:filters) {
        {
          includes: {
            node_name: 'A',
            next: {
              node_name: 'B'
            }
          },
          excludes: [ 'B' ]
        }
      }

      context 'apply filters' do
        it 'apply all the filters with valids params' do
          expect(inclusion_filter).to receive(:filter).and_return(tree)
          expect(exclusion_filter).to receive(:filter).and_return(tree)
          filter_tree.filter(tree, filters)
        end

        it 'apply only include filters' do
          expect(inclusion_filter).to receive(:filter).and_return(tree)
          expect(exclusion_filter).to receive(:filter).exactly(0).times
          include_filters = filters.dup
          include_filters.delete(:excludes)
          filter_tree.filter(tree, include_filters)
        end

        it 'apply only exclude filters' do
          expect(exclusion_filter).to receive(:filter).and_return(tree)
          expect(inclusion_filter).to receive(:filter).exactly(0).times
          exclude_filters = filters.dup
          exclude_filters.delete(:includes)
          filter_tree.filter(tree, exclude_filters)
        end

        it 'wont apply any filters' do
          expect(exclusion_filter).to receive(:filter).exactly(0).times
          expect(inclusion_filter).to receive(:filter).exactly(0).times
          filter_tree.filter(tree, {})
        end
      end
    end
  end
end

