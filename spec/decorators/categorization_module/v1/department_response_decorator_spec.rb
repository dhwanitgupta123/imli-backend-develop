module CategorizationModule
  module V1
    require 'rails_helper'
    RSpec.describe DepartmentResponseDecorator do
      let(:department_response_decorator) { CategorizationModule::V1::DepartmentResponseDecorator }
      let(:department) { FactoryGirl.build_stubbed(:department) }
      let(:response_codes) {CommonModule::V1::ResponseCodes}

      it "with create_ok_response" do
        response = department_response_decorator.create_ok_response(department)
        expect(response[:response]).to eq(response_codes::SUCCESS)
      end

      it "with create_department_details_response" do
        response = department_response_decorator.create_department_details_response(department)
        expect(response[:response]).to eq(response_codes::SUCCESS)
      end

      it "with create_all_departments_response" do
        args = { 
                  departments: [FactoryGirl.build_stubbed(:department),FactoryGirl.build_stubbed(:department)],
                  page_count: Faker::Number.number(2)
                }
        response = department_response_decorator.create_all_departments_response(args)
        expect(response[:response]).to eq(response_codes::SUCCESS)
      end

      it "with create_response_department_not_found_error" do
        response = department_response_decorator.create_response_department_not_found_error('error message')
        expect(response[:response]).to eq(response_codes::BAD_REQUEST)
      end
    end
  end
end