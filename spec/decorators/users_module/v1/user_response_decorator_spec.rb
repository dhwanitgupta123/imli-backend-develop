module UsersModule
  module V1
    require 'rails_helper'
    RSpec.describe UsersModule::V1::UserResponseDecorator do
      let(:user_response_decorator) { UsersModule::V1::UserResponseDecorator }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }

      let(:message) { FactoryGirl.build_stubbed(:message) }
      let(:provider) { FactoryGirl.build_stubbed(:provider, message: message) }
      let(:user) { FactoryGirl.build(:user, provider: provider) }
      let(:time_remaining) {"around 1 Hour"}
      
      it "with create_ok_response" do
        response = user_response_decorator.create_ok_response(user)
        expect(response[:response]).to eq(response_codes_util::SUCCESS)
      end
      it "with create_active_user_status_response" do
        response = user_response_decorator.create_active_user_status_response(user)
        expect(response[:response]).to eq(response_codes_util::SUCCESS)
      end
      it "with create_user_status_response" do
        response = user_response_decorator.create_user_status_response(user)
        expect(response[:response]).to eq(response_codes_util::SUCCESS)
      end
      it "with create_session_token_response" do
        response = user_response_decorator.create_session_token_response(user)
        expect(response[:response]).to eq(response_codes_util::SUCCESS)
      end
      it "with create_response_bad_request" do
        response = user_response_decorator.create_response_bad_request
        expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
      end
      it "with create_response_pre_condition_required" do
        response = user_response_decorator.create_response_pre_condition_required('message')
        expect(response[:response]).to eq(response_codes_util::PRE_CONDITION_REQUIRED)
      end
      it "with create_wrong_otp_request" do
        response = user_response_decorator.create_wrong_otp_request
        expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
      end
      it "with create_wrong_referral_code_request" do
        response = user_response_decorator.create_wrong_referral_code_request
        expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
      end
      it "with create_wrong_password_request" do
        response = user_response_decorator.create_wrong_password_request
        expect(response[:response]).to eq(response_codes_util::BAD_REQUEST)
      end
      it "with create_response_runtime_error" do
        response = user_response_decorator.create_response_runtime_error
        expect(response[:response]).to eq(response_codes_util::INTERNAL_SERVER_ERROR)
      end
      it "with create_exisiting_user_error" do
        response = user_response_decorator.create_exisiting_user_error
        expect(response[:response]).to eq(response_codes_util::UN_AUTHORIZED)
      end
      it "with create_response_unauthenticated_user" do
        response = user_response_decorator.create_response_unauthenticated_user
        expect(response[:response]).to eq(response_codes_util::UN_AUTHORIZED)
      end
      it "with create_request_timeout_error" do
        response = user_response_decorator.create_request_timeout_error
        expect(response[:response]).to eq(response_codes_util::AUTHENTICATION_TIMEOUT)
      end
      it "with create_not_found_error" do
        response = user_response_decorator.create_not_found_error
        expect(response[:response]).to eq(response_codes_util::NOT_FOUND)
      end
      it "with create_too_many_request_error" do
        response = user_response_decorator.create_too_many_request_error
        expect(response[:response]).to eq(response_codes_util::TOO_MANY_REQUESTS)
      end
      it "with create_blocked_user_response" do
        response = user_response_decorator.create_blocked_user_response(time_remaining)
        expect(response[:response]).to eq(response_codes_util::UN_AUTHORIZED)
      end
    end
  end
end