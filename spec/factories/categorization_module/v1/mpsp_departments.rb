module CategorizationModule
  module V1    
    FactoryGirl.define do
      factory :mpsp_department,:class => CategorizationModule::V1::MpspDepartment do 
        label Faker::Name.first_name
        images [Faker::Number.number(1)]
        priority Faker::Number.number(1)
      end
    end
  end
end
