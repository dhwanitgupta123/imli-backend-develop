module CategorizationModule
  module V1
    FactoryGirl.define do
      factory :mpsp_category,:class => CategorizationModule::V1::MpspCategory do 
        label Faker::Name.first_name
        priority Faker::Number.number(1)
      end
    end
  end
end
