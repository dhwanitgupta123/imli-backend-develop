module UsersModule
  module V1
    FactoryGirl.define do
      factory :membership, :class => UsersModule::V1::Membership do
        starts_at DateTime.now
        expires_at DateTime.now + 1
        quantity 1
        association :plan, factory: :plan
        factory :membership_with_payment do
          after(:create) do |membership|
            create(:payment, membership: membership)
          end
        end
        factory :membership_with_user do
          after(:create) do |membership|
            create(:user, membership: membership)
          end
        end
      end
    end
  end
end
