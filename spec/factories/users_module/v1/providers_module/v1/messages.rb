module UsersModule
  module V1
    module ProvidersModule
      module V1
        FactoryGirl.define do
          factory :message, :class => UsersModule::V1::ProvidersModule::V1::Message do
            otp Faker::Number.number(4).to_s
            expires_at Faker::Time.between(DateTime.now - 1, DateTime.now)
          end
        end
      end
    end
  end
end
