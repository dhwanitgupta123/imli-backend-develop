module UsersModule
  module V1
    FactoryGirl.define do
      factory :employee, :class => UsersModule::V1::Employee do
        association     :user, factory: :user
        designation     Faker::Lorem.word
        team            Faker::Lorem.word
      end

    end
  end
end