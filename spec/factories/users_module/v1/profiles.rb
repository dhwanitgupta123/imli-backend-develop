module UsersModule
  module V1
    FactoryGirl.define do
      factory :profile, :class => UsersModule::V1::Profile do
        savings Faker::Commerce.price
        pincode Faker::Number.number(6).to_s
      end
    end
  end
end
