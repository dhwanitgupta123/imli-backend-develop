module UsersModule
  module V1
    FactoryGirl.define do
      factory :user, :class => UsersModule::V1::User do 
        phone_number Faker::Number.number(10).to_s
        first_name  Faker::Name.first_name
        last_name Faker::Name.last_name
        benefit_limit 5
        referral_limit 20
      end
    end
  end
end
