module CreditsModule
  module V1
    FactoryGirl.define do
      factory :ample_credit,:class => CreditsModule::V1::AmpleCredit do 
        balance Faker::Number.decimal(2)
        association :user, factory: :user
      end
    end
  end
end
