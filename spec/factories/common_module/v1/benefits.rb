module CommonModule
  module V1
    FactoryGirl.define do
      factory :benefit, :class => CommonModule::V1::Benefit do
        name Faker::Lorem.word
        details Faker::Lorem.sentence
        condition Faker::Lorem.word
        reward_type Faker::Lorem.word
        reward_value Faker::Number.number(2).to_s
        benefit_type Faker::Number.between(1, 1)
        refer_limit Faker::Number.number(2)
        benefit_limit Faker::Number.number(2)
        benefit_to_user Faker::Number.between(1,2)
        benefit_to_referrer Faker::Number.between(1,2)
        trigger_point_of_user Faker::Number.between(1,2)
        trigger_point_of_referrer Faker::Number.between(1,2)
      end
    end
  end
end