module LoggingModule
  module V1
    require "rails_helper"
    RSpec.describe RequestLogWorker, type: :analyzer do
      let(:content_util){CommonModule::V1::Content}
      let(:status_code_util){CommonModule::V1::StatusCodes}

      let(:log) { 
        {
          "api_name" => Faker::Internet.slug('user login', '::'),
          "phone_number" => Faker::Number.number(10).to_s,
          "request_ip" => Faker::Internet.ip_v6_address,
          "request_url" => Faker::Internet.url,
          "request_query_string" => Faker::Lorem.word,
          "response_error" => content_util::OK_RESPONSE,
          "response_status" => status_code_util::SUCCESS
        } 
      }
      let(:request_log_worker) { LoggingModule::V1::RequestLogWorker}

      it "enqueues a UserActivityAnalyzer worker" do
        request_log_worker.perform_async(log)
        expect(request_log_worker).to have_enqueued_job(log)
      end
          
      #to check the queue name
      it { is_expected.to be_processed_in :default }
    end
  end
end