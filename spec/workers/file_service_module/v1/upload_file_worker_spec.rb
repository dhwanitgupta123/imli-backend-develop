#
# Module to handle functionalities related to file upload to S3
#
module FileServiceModule
  #
  # Version1 for file service module
  #
  module V1
    require "rails_helper"
    RSpec.describe UploadFileWorker, type: :worker do

      let(:upload_file_worker) { FileServiceModule::V1::UploadFileWorker }
      let(:args) {
        {
          'file_name' => Faker::Lorem.characters(15),
          'key' => Faker::Lorem.characters(5),
          'options' => {}
        }
      }
      it 'enqueues a UploadFile worker' do
        upload_file_worker.perform_async(args)
        expect(upload_file_worker).to have_enqueued_job(args)
      end
          
      # to check the queue name
      it { is_expected.to be_processed_in :file_queue }
    end
  end
end