#
# Module to handle functionalities related to file upload to S3
#
module FileServiceModule
  #
  # Version1 for file service module
  #
  module V1
    require "rails_helper"
    RSpec.describe UploadDataWorker, type: :worker do

      let(:upload_data_worker) { FileServiceModule::V1::UploadDataWorker }
      let(:args) {
        {
          'key' => Faker::Lorem.characters(5),
          'options' => {
            'body' => Faker::Lorem.characters(50)
          }
        }
      }

      it 'enqueues a UploadData worker' do
        upload_data_worker.perform_async(args)
        expect(upload_data_worker).to have_enqueued_job(args)
      end
          
      # to check the queue name
      it { is_expected.to be_processed_in :file_queue }
    end
  end
end