module AnalyzerModule
  module V1
    module UserAnalyzer
      module V1

        require "rails_helper"
        RSpec.describe AnalyzerModule::V1::UserAnalyzer::V1::UserActivityAnalyzerWorker, type: :analyzer do
          let(:phone_number) {Faker::Number.number(10).to_s}
          let(:activity_analyzer_worker) { AnalyzerModule::V1::UserAnalyzer::V1::UserActivityAnalyzerWorker}

          it "enqueues a UserActivityAnalyzer worker" do
            activity_analyzer_worker.perform_async(phone_number)
            expect(activity_analyzer_worker).to have_enqueued_job(phone_number)
          end
            
          #to check the queue name
          it { is_expected.to be_processed_in :default }
        end
      end
    end
  end
end
