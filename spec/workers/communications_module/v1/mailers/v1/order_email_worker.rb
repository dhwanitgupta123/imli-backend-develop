module CommunicationsModule
  module V1
    module Mailers
      module V1
        require "rails_helper"
        RSpec.describe OrderEmailWorker, type: :mailer do
          let(:email_type) { CommunicationsModule::V1::Mailers::V1::EmailType }
          let(:email_attributes) { 
            {
              "email_type" => email_type::PLACED_ORDER,
              "to_first_name" => Faker::Name.first_name,
              "to_email_id" => Faker::Internet.email
            } 
          }

          let(:email_worker) { CommunicationsModule::V1::Mailers::V1::OrderEmailWorker }

          it "enqueues a Email worker" do
            email_worker.perform_async(email_attributes)
            expect(email_worker).to have_enqueued_job(email_attributes)
          end

          #to check the queue name
          it { is_expected.to be_processed_in :email_queue }

          #to check how many retries can be done
          it { is_expected.to be_retryable 5 }

          #to check if a job should save the error backtrace when there is a failure in it's execution
          it { is_expected.to save_backtrace }
        end
      end
    end
  end
end