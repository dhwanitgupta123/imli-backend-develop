#
# Module to handle functionalities related to transactions
#
module TransactionsModule
  #
  # Version1 for transactions module
  #
  module V1
    require 'rails_helper'
    RSpec.describe TransactionsModule::V1::TransactionsDao, type: :dao do
      let(:transaction_dao) { TransactionsModule::V1::TransactionsDao.new }
      let(:transaction_request) {
        {
          payment_id: Faker::Number.number(1),
          payment_type: Faker::Number.number(1),
          status: Faker::Number.number(1),
          user_id: Faker::Number.number(1),
          user_email_id: Faker::Number.number(1)
        }
      }
      let(:update_transaction_request) {
        {
          status: Faker::Number.number(1)
        }
      }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      context 'create transaction' do
        it 'with valid params' do
          transaction = transaction_dao.create_transaction(transaction_request)
          expect(transaction.user_id).to eq(transaction_request[:user_id].to_i)
          transaction.destroy
        end
      end

      context 'update transaction' do
        it 'with valid params' do
          transaction = transaction_dao.create_transaction(transaction_request)
          updated_transaction = transaction_dao.update_transaction(update_transaction_request, transaction)
          expect(updated_transaction.status).to eq(update_transaction_request[:status].to_i)
          transaction.destroy
        end
      end

    end
  end
end