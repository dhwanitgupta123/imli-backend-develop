#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for marketplace pricing module
  #
  module V1
    #
    # DAO for MarketplaceSellingPackLadderPricing table
    #
    class MarketplaceSellingPackLadderPricingDao < PricingBaseModule::V1::BaseDao

      MPSP_LADDER_PRICING_MODEL = MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricing

      # 
      # return new object of new MPSP ladder pricing
      #
      def new_mpsp_ladder_pricing(args, max_quantity)
        ladder_array_hash = generate_ladder_hash(args, max_quantity)
        return create_mpsp_ladder(ladder_array_hash)
      end

      #
      # Function to create ladder with extrapolated hash
      #
      def create_mpsp_ladder(ladder_array_hash)
        ladder_array = []
        ladder_array_hash.each do |ladder_hash|
          ladder_array.push(MPSP_LADDER_PRICING_MODEL.new(model_params(ladder_hash, MPSP_LADDER_PRICING_MODEL)))
        end
        return ladder_array
      end

      # 
      # update MPSP ladder pricing details
      #
      def update_mpsp_ladder_pricing(args, mpsp_ladder_pricings, max_quantity)
        ladder_array_hash = generate_ladder_hash(args, max_quantity)
        ladder_array = []
        if mpsp_ladder_pricings.present?
          if max_quantity.to_i > mpsp_ladder_pricings.length
            new_mpsp_ladder_pricings = increase_ladder_pricing(max_quantity, mpsp_ladder_pricings.last)
            mpsp_ladder_pricings = mpsp_ladder_pricings + new_mpsp_ladder_pricings
          end
          ladder_array = update_ladder(ladder_array_hash, mpsp_ladder_pricings)
        else
          ladder_array = create_mpsp_ladder(ladder_array_hash)
        end
        return ladder_array
      end

      #
      # Funciton to update ladder object iteratively
      # 
      def update_ladder(ladder_array_hash, mpsp_ladder_pricings)
        index = 0
        ladder_array = []
        ladder_array_hash.each do |ladder_hash|
          ladder_array.push(update_model(ladder_hash, mpsp_ladder_pricings[index], 'Marketplace selling pack ladder pricing'))
          index += 1
        end
        return ladder_array
      end

      #
      # Function to create array of ladder hash
      #
      def generate_ladder_hash(args, max_quantity)
        max_quantity = max_quantity.to_i
        return [] if max_quantity == 0
        ladder_array_hash = []
        args[:ladder] =  sort_ladder_pricing(args[:ladder])
        initial_args = {
          selling_price: args[:base_selling_price],
          additional_savings: BigDecimal('0.0'),
          checkpoint: false
        }
        counter = 1
        while args[:ladder][0][:quantity].to_i > counter
          initial_args = initial_args.merge(quantity: counter)
          ladder_array_hash.push(initial_args)
          counter += 1
        end
        ladder_index = 0
        for i in counter..max_quantity
          if args[:ladder][ladder_index].present? && args[:ladder][ladder_index][:quantity].to_i == i
            ladder = args[:ladder][ladder_index]
            ladder[:selling_price] = BigDecimal(ladder[:selling_price])
            ladder[:additional_savings] = args[:mrp] - ladder[:selling_price] - args[:savings]
            ladder[:checkpoint] = true
            ladder_index += 1
          end
          ladder_array_hash.push({
            quantity: i,
            selling_price: ladder[:selling_price],
            additional_savings: ladder[:additional_savings],
            checkpoint: ladder[:checkpoint]
          })
          ladder[:checkpoint] = false
        end
        return ladder_array_hash
      end

      # 
      # Function to sort array based on it's quantity
      #
      def sort_ladder_pricing(ladders)
        ladders = ladders.sort_by{|ladder| ladder[:quantity].to_i}
        return ladders
      end

      #
      # Fetch ladder pricing with respect to the passed quantity
      #    
      def get_mpsp_ladder_pricing_details(quantity, mpsp_ladder_pricings)
        mpsp_ladder_pricings.where(quantity: quantity).first
      end

      # 
      # Function to create more ladder for MPSP, if existing ladder is
      # shorter than the new Max quantity
      #
      def increase_ladder_pricing(max_quantity, ladder)
        quantity = ladder.quantity + 1
        new_ladder_array = []
        while quantity <= max_quantity do
          args = {
            marketplace_selling_pack_pricing_id: ladder.marketplace_selling_pack_pricing_id,
            quantity: quantity,
            selling_price: ladder.selling_price,
            additional_savings: ladder.additional_savings 
          }
          quantity += 1
          begin
            new_ladder = MPSP_LADDER_PRICING_MODEL.new(model_params(args, MPSP_LADDER_PRICING_MODEL))
            new_ladder.save!
            new_ladder_array.push(new_ladder)
          rescue ActiveRecord::RecordInvalid => e
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              'Not able to create Marketplace selling pack ladder pricing, ' + e.message)
          end
        end
        return new_ladder_array
      end

    end
  end
end
