#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
	#
	# Version1 for marketplace pricing module
	#
	module V1
		#
		# DAO for MarketplaceBrandPackPricing table
		#
		class MarketplaceBrandPackPricingDao < PricingBaseModule::V1::BaseDao

			MPBP_PRICING_MODEL = MarketplacePricingModule::V1::MarketplaceBrandPackPricing

			# 
			# return new object of new MPBP pricing
			#
			def new_marketplace_brand_pack_pricing(args)
				return MPBP_PRICING_MODEL.new(model_params(args, MPBP_PRICING_MODEL))
			end

			# 
			# update MPBP pricing details
			#
			def update_marketplace_brand_pack_pricing(args, mpbp_pricing)
				return update_model(args, mpbp_pricing, 'Marketplace brand pack pricing')
			end
		end
	end
end
