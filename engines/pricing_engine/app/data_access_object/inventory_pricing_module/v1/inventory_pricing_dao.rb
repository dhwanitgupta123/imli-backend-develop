#
# Module to handle all the functionalities related to inventory pricing
#
module InventoryPricingModule
  #
  # Version1 for inventory pricing module
  #
  module V1
    #
    # DAO for InventoryPricing table
    #
    class InventoryPricingDao < PricingBaseModule::V1::BaseDao

      INVENTORY_PRICING_MODEL = InventoryPricingModule::V1::InventoryPricing

      # 
      # return new object of new_inventory pricing
      #
      def new_inventory_pricing(args)
        return INVENTORY_PRICING_MODEL.new(model_params(args, INVENTORY_PRICING_MODEL))
      end

      # 
      # update inventory pricing details
      #
      def update_inventory_pricing(args, inventory_pricing)
        return update_model(args, inventory_pricing, 'Inventory pricing')
      end
    end
  end
end
