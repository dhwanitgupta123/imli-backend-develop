#
# Module to handle all the functionalities related to seller pricing
#
module SellerPricingModule
  #
  # Version1 for seller pricing module
  #
  module V1
    #
    # DAO for SellerPricing table
    #
    class SellerPricingDao < PricingBaseModule::V1::BaseDao

      SELLER_PRICING_MODEL = SellerPricingModule::V1::SellerPricing

      # 
      # return new object of new_seller pricing
      #
      def new_seller_pricing(args)
        return SELLER_PRICING_MODEL.new(model_params(args, SELLER_PRICING_MODEL))
      end

      # 
      # update seller pricing details
      #
      def update_seller_pricing(args, seller_pricing)
        return update_model(args, seller_pricing, 'Seller pricing')
      end
    end
  end
end
