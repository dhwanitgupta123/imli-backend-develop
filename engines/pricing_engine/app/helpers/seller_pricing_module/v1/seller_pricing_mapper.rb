#
# Module to handle all the functionalities related to pricing
#
module SellerPricingModule
  #
  # Version1 for seller pricing module
  #
  module V1
    #
    # Map seller_pricing object to Json
    #
    module SellerPricingMapper

    	def self.map_seller_pricing_to_hash(seller_pricing)
        return {} if seller_pricing.blank?

    		response = {
    			spat: seller_pricing.spat,
    			margin: seller_pricing.margin,
    			service_tax: seller_pricing.service_tax,
    			vat: seller_pricing.vat,
    			spat_per_unit: seller_pricing.spat_per_unit
    		}
    		return response
    	end
    end
  end
end
