#
# Module to handle all the functionalities related to pricing
#
module InventoryPricingModule
  #
  # Version1 for inventory pricing module
  #
  module V1
    #
    # Map inventory_pricing object to Json
    #
    module InventoryPricingMapper

    	def self.map_inventory_pricing_to_hash(inventory_pricing)
        return {} if inventory_pricing.blank?

    		response = {
    			net_landing_price: inventory_pricing.net_landing_price.round(2),
    			margin: inventory_pricing.margin.round(2),
    			vat: inventory_pricing.vat.round(2),
          cst: inventory_pricing.cst.round(2),
          octrai: inventory_pricing.octrai.round(2)
    		}
    		return response
    	end
    end
  end
end
