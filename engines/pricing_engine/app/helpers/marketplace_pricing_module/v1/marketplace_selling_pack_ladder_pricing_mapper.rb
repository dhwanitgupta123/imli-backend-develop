#
# Module to handle all the functionalities related to marketplace pricing
#
module MarketplacePricingModule
  #
  # Version1 for marketplace pricing module
  #
  module V1
    #
    # Map marketplace selling pack ladder pricing object to Json
    #
    module MarketplaceSellingPackLadderPricingMapper

      def self.map_checkpoint_marketplace_selling_pack_ladder_pricing_to_hash(ladders, max_quantity)
        ladder_pricing = []
        return ladder_pricing if ladders.blank?

        index = 0
        ladders = ladders.sort{|a,b| a.quantity <=> b.quantity}
        while index < max_quantity do
          if ladders[index].checkpoint
            response = {
              quantity: ladders[index].quantity,
              selling_price: ladders[index].selling_price,
              additional_savings: ladders[index].additional_savings
            }
            ladder_pricing.push(response)
          end
          index += 1
        end
        
        return ladder_pricing
      end

      def self.map_marketplace_selling_pack_ladder_pricing_to_hash(ladders, max_quantity)
        ladder_pricing = []
        return ladder_pricing if ladders.blank?

        index = 0
        ladders = ladders.sort{|a,b| a.quantity <=> b.quantity}
        while index < max_quantity do
          response = {
            quantity: ladders[index].quantity,
            selling_price: ladders[index].selling_price,
            additional_savings: ladders[index].additional_savings,
            checkpoint: ladders[index].checkpoint
          }
          ladder_pricing.push(response)
          index += 1
        end
        
        return ladder_pricing
      end
    end
  end
end
