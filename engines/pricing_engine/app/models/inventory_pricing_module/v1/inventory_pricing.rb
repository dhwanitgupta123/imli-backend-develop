#
# Module to handle all the functionalities related to inventory pricing
#
module InventoryPricingModule
  #
  # Version1 for inventory pricing module
  #
  module V1
    #
    # Model for InventoryPricing table
    #
    class InventoryPricing < BaseModule::BaseModel

      belongs_to :inventory_brand_pack, class_name: 'InventoryProductModule::V1::InventoryBrandPack'

    end
  end
end
