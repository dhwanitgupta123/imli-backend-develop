$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "pricing_engine/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "pricing_engine"
  s.version     = PricingEngine::VERSION
  s.authors     = ["Anuj Gangwar"]
  s.email       = ["anuj.gangwar2007@gmail.com"]
  s.homepage    = "https://www.buyample.com"
  s.summary     = "Summary of PricingEngine."
  s.description = "Description of PricingEngine."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.3"

  s.test_files = Dir["spec/**/*"]
end
