#
# Module to handle all the functionalities related to marketplace pricing module
#
module MarketplacePricingModule
  #
  # Version1 for MPSP Pricing module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplacePricingModule::V1::MarketplaceSellingPackPricingService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:mpsp_pricing_service) { MarketplacePricingModule::V1::MarketplaceSellingPackPricingService.new }
      let(:mpsp_pricing_dao) { MarketplacePricingModule::V1::MarketplaceSellingPackPricingDao }
      let(:mpsp_ladder_pricing_dao) { MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricingDao }
      let(:mpsp_ladder_pricing) { FactoryGirl.build(:marketplace_selling_pack_ladder_pricing) }
      let(:mpsp_pricing) { FactoryGirl.build(:marketplace_selling_pack_pricing) }
      let(:args) {
        {
          mrp: Faker::Commerce.price.to_d,
          base_selling_price: Faker::Commerce.price.to_d,
          savings: Faker::Commerce.price.to_d,
          service_tax: Faker::Commerce.price.to_d,
          vat: Faker::Commerce.price.to_d,
          discount: Faker::Commerce.price.to_d,
          taxes: Faker::Commerce.price.to_d,
          ladder: [
            {
              quantity: 1,
              selling_price: Faker::Commerce.price.to_d,
            },
            {
              quantity: 2,
              selling_price: Faker::Commerce.price.to_d,
            }
          ]
        }
      }
      let(:marketplace_brand_pack_quantity_array) {
        [
          {
            marketplace_brand_pack: FactoryGirl.build_stubbed(:marketplace_brand_pack),
            quantity: Faker::Number.number(1).to_i
          },
          {
            marketplace_brand_pack: FactoryGirl.build_stubbed(:marketplace_brand_pack),
            quantity: Faker::Number.number(1).to_i
          }
        ]
      }
      let(:max_quantity) { 10 }
      let(:is_ladder_pricing_active) { true }

      describe 'new_marketplace_selling_pack_pricing' do

        it 'with valid params' do
          expect(mpsp_pricing_service.new_marketplace_selling_pack_pricing(
            args, marketplace_brand_pack_quantity_array, max_quantity, is_ladder_pricing_active)).to_not be_nil
        end

        it 'when base_selling_price not present' do
          pricing = mpsp_pricing_service.new_marketplace_selling_pack_pricing(
            args.merge(base_selling_price: nil), marketplace_brand_pack_quantity_array, max_quantity)
          mpbp1_sp = marketplace_brand_pack_quantity_array[0][:marketplace_brand_pack].marketplace_brand_pack_pricing.selling_price
          mpbp2_sp = marketplace_brand_pack_quantity_array[1][:marketplace_brand_pack].marketplace_brand_pack_pricing.selling_price
          mpbp1_quantity = marketplace_brand_pack_quantity_array[0][:quantity]
          mpbp2_quantity = marketplace_brand_pack_quantity_array[1][:quantity]
          mpbp_total_selling_price = mpbp1_sp * mpbp1_quantity + mpbp2_sp * mpbp2_quantity
          mpbp_total_savings = args[:mrp] - mpbp_total_selling_price
          expect(pricing.base_selling_price).to eq(mpbp_total_selling_price)
          expect(pricing.savings).to eq(mpbp_total_savings)
        end

        it 'when mrp not present' do
          pricing = mpsp_pricing_service.new_marketplace_selling_pack_pricing(
            args.merge(mrp: nil), marketplace_brand_pack_quantity_array, max_quantity)
          mpbp1_mrp = marketplace_brand_pack_quantity_array[0][:marketplace_brand_pack].marketplace_brand_pack_pricing.mrp
          mpbp2_mrp = marketplace_brand_pack_quantity_array[1][:marketplace_brand_pack].marketplace_brand_pack_pricing.mrp
          mpbp1_quantity = marketplace_brand_pack_quantity_array[0][:quantity]
          mpbp2_quantity = marketplace_brand_pack_quantity_array[1][:quantity]
          mpbp_total_mrp = mpbp1_mrp * mpbp1_quantity + mpbp2_mrp * mpbp2_quantity
          mpbp_total_savings = mpbp_total_mrp - args[:base_selling_price]
          expect(pricing.mrp).to eq(mpbp_total_mrp)
          expect(pricing.savings).to eq(mpbp_total_savings)
        end
      end

      describe 'update_marketplace_selling_pack_pricing' do

        it 'with valid params' do
          mpsp_pricing.marketplace_selling_pack_ladder_pricings << mpsp_ladder_pricing
          expect(mpsp_pricing_service.update_marketplace_selling_pack_pricing(
            args, mpsp_pricing, max_quantity, is_ladder_pricing_active)).to_not be_nil
        end

        it 'when ladder pricing not present' do
          expect(mpsp_pricing_service.update_marketplace_selling_pack_pricing(
            args, mpsp_pricing, max_quantity, is_ladder_pricing_active)).to_not be_nil
        end

        it 'when ladder dao throws error' do
          mpsp_ladder_pricing_dao.any_instance.stub(:update_mpsp_ladder_pricing).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ mpsp_pricing_service.update_marketplace_selling_pack_pricing(
            args, mpsp_pricing, max_quantity, is_ladder_pricing_active) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'when pricing dao throws error' do
          mpsp_pricing_dao.any_instance.stub(:update_marketplace_selling_pack_pricing).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ mpsp_pricing_service.update_marketplace_selling_pack_pricing(
            args, mpsp_pricing, max_quantity) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end    
      end
    end
  end
end
