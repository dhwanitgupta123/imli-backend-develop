#
# Module to handle all the functionalities related to inventory pricing module
#
module InventoryPricingModule
  #
  # Version1 for inventory Pricing module
  #
  module V1
    require 'rails_helper'
    RSpec.describe InventoryPricingModule::V1::InventoryPricingDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:inventory_pricing_dao) { InventoryPricingModule::V1::InventoryPricingDao.new }
      let(:base_dao) { PricingBaseModule::V1::BaseDao }
      let(:inventory_pricing) { FactoryGirl.build(:inventory_pricing) }
      let(:args) {
        {
          net_landing_price: Faker::Commerce.price.to_d,
          margin: Faker::Commerce.price.to_d,
          service_tax: Faker::Commerce.price.to_d,
          vat: Faker::Commerce.price.to_d,
          spat_per_unit: Faker::Commerce.price.to_d
        }
      }

      describe 'new_inventory_pricing' do
        it 'with valid params' do
          expect(inventory_pricing_dao.new_inventory_pricing(
            args)).to_not be_nil
        end
      end

      describe 'update_inventory_pricing' do
        it 'with valid params' do
          expect(inventory_pricing_dao.update_inventory_pricing(
            args, inventory_pricing)).to_not be_nil
        end

        it 'when ladder dao throws error' do
          base_dao.any_instance.should_receive(:update_model).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ inventory_pricing_dao.update_inventory_pricing(
            args, inventory_pricing) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end   
      end
    end
  end
end
