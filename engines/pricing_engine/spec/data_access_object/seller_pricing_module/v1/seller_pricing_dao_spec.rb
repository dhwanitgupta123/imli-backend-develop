#
# Module to handle all the functionalities related to seller pricing module
#
module SellerPricingModule
  #
  # Version1 for seller Pricing module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SellerPricingModule::V1::SellerPricingDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:seller_pricing_dao) { SellerPricingModule::V1::SellerPricingDao.new }
      let(:base_dao) { PricingBaseModule::V1::BaseDao }
      let(:seller_pricing) { FactoryGirl.build(:seller_pricing) }
      let(:args) {
        {
          spat: Faker::Commerce.price.to_d,
          margin: Faker::Commerce.price.to_d,
          service_tax: Faker::Commerce.price.to_d,
          vat: Faker::Commerce.price.to_d,
          spat_per_unit: Faker::Commerce.price.to_d
        }
      }

      describe 'new_seller_pricing' do
        it 'with valid params' do
          expect(seller_pricing_dao.new_seller_pricing(
            args)).to_not be_nil
        end
      end

      describe 'update_seller_pricing' do
        it 'with valid params' do
          expect(seller_pricing_dao.update_seller_pricing(
            args, seller_pricing)).to_not be_nil
        end

        it 'when ladder dao throws error' do
          base_dao.any_instance.should_receive(:update_model).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ seller_pricing_dao.update_seller_pricing(
            args, seller_pricing) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end   
      end
    end
  end
end
