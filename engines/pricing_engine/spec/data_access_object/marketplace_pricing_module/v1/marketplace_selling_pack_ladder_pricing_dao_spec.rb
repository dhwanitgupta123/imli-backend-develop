#
# Module to handle all the functionalities related to marketplace pricing module
#
module MarketplacePricingModule
  #
  # Version1 for MPSP Pricing module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricingDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:mpsp_ladder_pricing_dao) { MarketplacePricingModule::V1::MarketplaceSellingPackLadderPricingDao.new }
      let(:mpsp_ladder_pricing) { [FactoryGirl.build(:marketplace_selling_pack_ladder_pricing), FactoryGirl.build(:marketplace_selling_pack_ladder_pricing, quantity: 2)] }
      let(:mpsp_pricing) { FactoryGirl.build(:marketplace_selling_pack_pricing) }
      let(:args) {
        {
          mrp: Faker::Commerce.price.to_d,
          base_selling_price: Faker::Commerce.price.to_d,
          savings: Faker::Commerce.price.to_d,
          ladder: [
            {
              quantity: 1,
              selling_price: Faker::Commerce.price.to_d,
            },
            {
              quantity: 2,
              selling_price: Faker::Commerce.price.to_d,
            }
          ]
        }
      }
      let(:max_quantity) { 10 }

      describe 'new_mpsp_ladder_pricing' do
        it 'with valid params' do
          expect(mpsp_ladder_pricing_dao.new_mpsp_ladder_pricing(
            args, max_quantity)).to_not be_nil
        end
      end

      describe 'update_mpsp_ladder_pricing' do
        it 'with valid params' do
          expect(mpsp_ladder_pricing_dao.update_mpsp_ladder_pricing(
            args, mpsp_ladder_pricing, max_quantity)).to_not be_nil
        end

        it 'when ladder pricing not present' do
          expect(mpsp_ladder_pricing_dao.update_mpsp_ladder_pricing(
            args, nil, max_quantity)).to_not be_nil
        end

        it 'when ladder dao throws error' do
          mpsp_ladder_pricing_dao.should_receive(:increase_ladder_pricing).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ mpsp_ladder_pricing_dao.update_mpsp_ladder_pricing(
            args, mpsp_ladder_pricing, max_quantity) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end   
      end

      describe 'get_mpsp_ladder_pricing_details' do
        it 'when valid params' do
          mpsp_pricing.marketplace_selling_pack_ladder_pricings << mpsp_ladder_pricing
          mpsp_pricing.save!
          ladder = mpsp_ladder_pricing_dao.get_mpsp_ladder_pricing_details(1, mpsp_pricing.marketplace_selling_pack_ladder_pricings)
          expect(ladder.quantity).to eq(1)
          mpsp_ladder_pricing.each do |mpsp_ladder|
            mpsp_ladder.delete
          end
          mpsp_pricing.delete
        end
      end
    end
  end
end
