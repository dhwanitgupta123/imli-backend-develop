#
# Module to handle all the functionalities related to warehouse pricing chain
#
module WarehousePricingModule
  #
  # Version1 for warehouse pricing module
  #
  module V1
    require 'rails_helper'

    RSpec.describe WarehousePricingModule::V1::WarehousePricing, type: :model do

      it 'should be valid' do
      warehouse_pricing = FactoryGirl.build_stubbed(:warehouse_pricing)
        expect(warehouse_pricing).to be_valid
      end
    end
  end
end
