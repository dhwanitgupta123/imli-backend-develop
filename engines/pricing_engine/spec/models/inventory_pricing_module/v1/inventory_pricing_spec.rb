#
# Module to handle all the functionalities related to inventory pricing chain
#
module InventoryPricingModule
  #
  # Version1 for inventory pricing module
  #
  module V1
    require 'rails_helper'

    RSpec.describe InventoryPricingModule::V1::InventoryPricing, type: :model do

      subject { FactoryGirl.build(:inventory_pricing) }

      it { should be_valid }
    end
  end
end
