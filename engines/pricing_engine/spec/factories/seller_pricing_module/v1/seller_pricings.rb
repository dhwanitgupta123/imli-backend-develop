#
# Module to handle all the functionalities related to seller pricing
#
module SellerPricingModule
  #
  # Version1 for warehouse pricing module
  #
  module V1
    FactoryGirl.define do
      factory :seller_pricing, class: SellerPricingModule::V1::SellerPricing do
        spat Faker::Commerce.price.to_d
        margin Faker::Commerce.price.to_d
        service_tax Faker::Commerce.price.to_d
        vat Faker::Commerce.price.to_d
        spat_per_unit Faker::Commerce.price.to_d
      end
    end
  end
end
