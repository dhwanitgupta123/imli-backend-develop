#
# Module to handle all the functionalities related to inventory pricing
#
module InventoryPricingModule
  #
  # Version1 for inventory pricing module
  #
  module V1
    FactoryGirl.define do
      factory :inventory_pricing, class: InventoryPricingModule::V1::InventoryPricing do
        net_landing_price Faker::Commerce.price.to_d
        margin Faker::Commerce.price.to_d
        service_tax Faker::Commerce.price.to_d
        vat Faker::Commerce.price.to_d
        spat_per_unit Faker::Commerce.price.to_d
      end
    end
  end
end
