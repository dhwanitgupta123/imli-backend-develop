source 'https://rubygems.org'

gemspec

# Use postgresql as the database for Active Record
gem 'pg'

#Using mongoid to use MongoDB
gem 'mongoid', '~> 5.0.0'

group :test do

  # gem to add functionalities -> attachment type in ActiveRecord:Base, image manipulation and image upload
  gem 'paperclip'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # gem to use elastic search API's
  gem 'elasticsearch-persistence'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # gem for Factory Girl, which is used to populate model data easily while testing with rspec
  gem 'factory_girl_rails'

  # ge to do rspec testing
  gem 'rspec-rails', '3.3.3'

  # Guard gem to test file which is currently being modified in runtime
  gem 'guard-rspec', '4.6.4'

  # gem to create fake dummy data
  gem 'faker'

  # liquid templating engine
  gem 'liquid'

  gem 'workflow', git: 'git@github.com:imlitech/workflow.git',
                  branch: 'feature_state_to_integer'

  gem 'rspec-sidekiq'

  gem 'simplecov', :require => false
end

# gem to generate swagger docs
gem 'swagger-docs', :git => 'git@github.com:imlitech/swagger-docs.git', :branch => 'feature/change_path'
