# Order Engine CONFIG initialization should be done before engine initialization
# Load all environment specific application config variables into ORDER_ENGINE_CONFIG variable

order_engine_config_file = OrderEngine::Engine.root.join("config", "order_engine_config.yml").to_s

# Global variable ORDER_ENGINE_CONFIG to store all environment specific config
# This is similar to APP_CONFIG in application
ORDER_ENGINE_CONFIG = {}

if File.exists?(order_engine_config_file)
  YAML.load_file(order_engine_config_file)[Rails.env].each do |key, value|
    ORDER_ENGINE_CONFIG[key.to_s] = value.to_s
  end # end YAML.load_file
end # end if

# 
# Checks if redis is blank, which oughts tobe,
# hence initialized it with Redis configuration
# read from the order_engine_config_file
# 
if $redis.blank?
  puts '--> Initializing Redis Cache from Order Engine'
  $redis = Redis.new(
    :host => ORDER_ENGINE_CONFIG["redis_host"],
    :port => ORDER_ENGINE_CONFIG["redis_port"]
  )
end

puts '--> Initializing Order Engine resources'

#
# Write Key Value pair to Redis
#
# @param key [String] [Key for which value is to be stored]
# @param value [Object] [Value to be stored]
#
def write_key_value_to_redis(key, value)
  CommonModule::V1::Cache.write({
    key: key.to_s,
    value: value.to_s
    })
end
# Load payment details file into cache
# Since common Cache utility is not available at this point, so writing
# values in Rails cache which internally uses REDIS (presently)
payment_file_path = OrderEngine::Engine.root.join("config", "payment_details.yml").to_s
if File.file?(payment_file_path)
  payment_details_file  = YAML::load_file(payment_file_path)  # Returns as a HASH
  # Store into Redis after converting into JSON string
  write_key_value_to_redis("PAYMENT_DETAILS", JSON.generate(payment_details_file))
else
  puts '[ERROR]: Unable to load payment details file'
end