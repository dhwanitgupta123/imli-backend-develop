module CacheModule
  module V1
    require 'rails_helper'
    RSpec.describe CacheModule::V1::TimeSlotCacheClient do

      let(:time_slot_cache_client) { CacheModule::V1::TimeSlotCacheClient }
      let(:cache_util) { CommonModule::V1::Cache }
      let(:cache_client) { CacheModule::V1::CacheClient }
      let(:time_slot) { FactoryGirl.build_stubbed(:time_slot) }
      let(:order) { FactoryGirl.build_stubbed(:order) }
      let(:preferred_date) { CommonModule::V1::DateTimeUtil.convert_epoch_to_date(Time.zone.now.to_i) }
      let(:args) {
        {
          order: order,
          time_slot: time_slot,
          preferred_date: preferred_date
        }
      }
      context 'function slot_for_order_exists?' do
        it ' return false if key not present in redis' do
          expect(cache_util).to receive(:read).and_return(nil)
          data = time_slot_cache_client.slot_for_order_exists?(args)
          expect(data).to eq(false)
        end

        it ' return true if key present in redis' do
          expect(cache_util).to receive(:read).and_return(1)
          data = time_slot_cache_client.slot_for_order_exists?(args)
          expect(data).to eq(true)
        end
      end

      context 'function get_freezed_time_slots' do
        it ' will return count of freezed time slots for the particular date' do
          time_slot_key = time_slot_cache_client.get_key(args)
          expect(cache_util).to receive(:get_keys_by_pattern).and_return([time_slot_key])
          data = time_slot_cache_client.get_freezed_time_slots(time_slot, preferred_date)
          expect(data).to eq([time_slot_key].count)
        end
      end

      context 'function freeze_time_slot_for_order' do
        it ' will freeze time_slot for order on preferred_delivery_date' do
          expect(cache_client).to receive(:set).and_return(true)
          data = time_slot_cache_client.freeze_time_slot_for_order(args)
          expect(data).to eq(true)
        end
      end

      context 'function get_freezed_time_slot' do
        it ' will return freezed_time_slot if present' do
          expect(cache_client).to receive(:get).and_return(1)
          data = time_slot_cache_client.get_freezed_time_slot(args)
          expect(data).to eq(1)
        end
      end

      context 'function get_time_slot_ttl' do
        it ' will return ttl of time_slot of order' do
          ttl = Faker::Number.between(1, 9).minutes.to_i
          expect(cache_util).to receive(:get_ttl).and_return(ttl)
          data = time_slot_cache_client.get_time_slot_ttl(args)
          expect(data).to eq(ttl)
        end
      end

      context 'function remove_time_slot' do
        it ' will remove the time_slot for order' do
          expect(cache_util).to receive(:delete).and_return(true)
          data = time_slot_cache_client.remove_time_slot(args)
          expect(data).to eq(true)
        end
      end
    end
  end
end
