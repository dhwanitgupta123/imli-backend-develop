module MarketplaceOrdersModule
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceOrdersModule::V1::UpdateOrderContextApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:order_context_service) { MarketplaceOrdersModule::V1::OrderContextService }
      let(:update_order_context_api) { MarketplaceOrdersModule::V1::UpdateOrderContextApi.new(version) }
      let(:order_context) { FactoryGirl.build(:order_context) }
      let(:order_context_request) {
        {
          order_id: Faker::Number.number(1),
          order_user_tag: Faker::Number.number(1)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = update_order_context_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(order_context_service).to receive(:record_order_context).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = update_order_context_api.enact(order_context_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(order_context_service).to receive(:record_order_context).
            and_return(order_context)
          data = update_order_context_api.enact(order_context_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
