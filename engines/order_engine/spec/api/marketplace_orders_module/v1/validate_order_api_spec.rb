#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceOrdersModule::V1::ValidateOrderApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:post_order_validation_service) { MarketplaceOrdersModule::V1::PostOrderValidationService }
      let(:order) { FactoryGirl.build(:order) }
      let(:orders_response_decorator) {MarketplaceOrdersModule::V1::OrdersResponseDecorator}

      let(:validate_order_api) { MarketplaceOrdersModule::V1::ValidateOrderApi.instance(version) }

      context 'enact ' do
        it 'with invalid args' do
          data = validate_order_api.enact({id: nil})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args and valid order' do
          expect_any_instance_of(post_order_validation_service).to receive(:apply_post_order_validations).
            and_return({order: order, is_valid: true})
          data = validate_order_api.enact({ id: Faker::Number.number(1) })
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end

        it 'with valid args and in valid order' do
          expect_any_instance_of(post_order_validation_service).to receive(:apply_post_order_validations).
            and_return({order: order, is_valid: false})
          expect(orders_response_decorator).to receive(:create_invalid_order_response).
            and_return({response: response_codes::PRE_CONDITION_REQUIRED})
          data = validate_order_api.enact({ id: Faker::Number.number(1) })
          expect(data[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end
      end
    end
  end
end
