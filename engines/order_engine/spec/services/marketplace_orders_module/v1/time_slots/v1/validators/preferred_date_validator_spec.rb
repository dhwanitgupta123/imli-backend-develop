#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        module Validators
          require 'rails_helper'
          RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::PreferredDateValidator do
            let(:preferred_date_validator) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::PreferredDateValidator}
            let(:time_slot) {FactoryGirl.build_stubbed(:time_slot)}
            let(:dates) {TimeSlotsModule::V1::ValidationsUtil.get_start_and_final_dates(time_slot.min_slot_offset, time_slot.max_slot_offset)}

            it 'it will return false if preferred_date is less than start_available_date' do
              expect(preferred_date_validator.validate({time_slot:time_slot, preferred_date: dates[:start_available_date] - 1})).to eq(false)
            end

            it 'it will return false if preferred_date is greater than final_available_date' do
              expect(preferred_date_validator.validate({time_slot: time_slot, preferred_date: dates[:final_available_date] + 1})).to eq(false)
            end

            it 'it will return true if preferred_date is between start and final date' do
              expect(preferred_date_validator.validate({time_slot: time_slot, preferred_date: dates[:final_available_date] - 1})).to eq(true)
            end
          end
        end
      end
    end
  end
end
