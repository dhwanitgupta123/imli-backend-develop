#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        module Validators
          require 'rails_helper'
          RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TotalSlotLimitValidator do
            let(:total_slot_limit_validator) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TotalSlotLimitValidator}
            let(:time_slot) {FactoryGirl.build_stubbed(:time_slot)}
            let(:cache_time_slot_manipulator) {MarketplaceOrdersModule::V1::TimeSlots::V1::CacheTimeSlotManipulator}
            let(:order_time_slot_manipulator) {MarketplaceOrdersModule::V1::TimeSlots::V1::OrderTimeSlotManipulator}

            it 'will return false if total slot count is greater than time_slot order limit' do
              a = Faker::Number.number(1).to_i
              b = time_slot.order_limit.to_i + 1
              expect(cache_time_slot_manipulator).to receive(:get_freezed_time_slots_for_preferred_date).and_return(a)
              expect(order_time_slot_manipulator).to receive(:get_allocated_slots).and_return(b)
              expect(total_slot_limit_validator.validate({time_slot: time_slot, preferred_date: Time.zone.now.to_date})).to eq(false)
            end

            it 'will return true if total slot count is less than time_slot order limit' do
              a = Faker::Number.number(1).to_i
              b = time_slot.order_limit.to_i - 1 - a
              expect(cache_time_slot_manipulator).to receive(:get_freezed_time_slots_for_preferred_date).and_return(a)
              expect(order_time_slot_manipulator).to receive(:get_allocated_slots).and_return(b)
              expect(total_slot_limit_validator.validate({time_slot: time_slot, preferred_date: Time.zone.now.to_date})).to eq(true)
            end
          end
        end
      end
    end
  end
end
