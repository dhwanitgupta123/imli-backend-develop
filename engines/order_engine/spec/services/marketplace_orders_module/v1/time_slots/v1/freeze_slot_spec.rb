#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        require 'rails_helper'
        RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::FreezeSlot do
          let(:time_slot_date_bucket_dao) {MarketplaceOrdersModule::V1::TimeSlotDateBucketDao}
          let(:time_slot_validations) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotValidations}
          let(:freeze_slot) {MarketplaceOrdersModule::V1::TimeSlots::V1::FreezeSlot}
          let(:slot_already_freezed_validator) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotAlreadyFreezedValidator}
          let(:slot_belongs_to_same_order) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotBelongsToSameOrder}
          let(:time_slot_validations) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotValidations}
          let(:update_time_slot_data) {MarketplaceOrdersModule::V1::TimeSlots::V1::UpdateTimeSlotData}
          let(:validations_util) {TimeSlotsModule::V1::ValidationsUtil}
          let(:time_slot) {FactoryGirl.build_stubbed(:time_slot)}
          let(:order) {FactoryGirl.build_stubbed(:order)}
          let(:args) {
            {
              time_slot: time_slot,
              order: order,
              preferred_date: Time.zone.now.to_date
            }
          }

          context 'time slot freezed before, ' do
            it 'raise slot unavailable error if validation fails' do
              expect(slot_already_freezed_validator).to receive(:validate).and_return(true)
              expect(time_slot_validations).to receive(:apply).and_return(false)
              expect {freeze_slot.freeze(args)}.to raise_error(CommonModule::V1::CustomErrors::SlotUnavailable)
            end

            it 'raise slot belongs to same order' do
              expect(slot_already_freezed_validator).to receive(:validate).and_return(true)
              expect(time_slot_validations).to receive(:apply).and_return(true)
              expect(slot_belongs_to_same_order).to receive(:validate).and_return(true)
              expect {freeze_slot.freeze(args)}.to raise_error(CommonModule::V1::CustomErrors::SlotBelongsToSameOrder)
            end

            it 'freeze slot if all validation passes' do
              expect(slot_already_freezed_validator).to receive(:validate).and_return(true)
              expect(time_slot_validations).to receive(:apply).and_return(true)
              expect(slot_belongs_to_same_order).to receive(:validate).and_return(false)
              expect(update_time_slot_data).to receive(:update).and_return(true)
              freeze_slot.freeze(args)
            end
          end

          context 'time slot not freezed before, ' do
            it 'raise slot unavailable error if validation fails' do
              expect(slot_already_freezed_validator).to receive(:validate).and_return(false)
              expect(time_slot_validations).to receive(:apply).and_return(false)
              expect {freeze_slot.freeze(args)}.to raise_error(CommonModule::V1::CustomErrors::SlotUnavailable)
            end


            it 'freeze slot if all validation passes' do
              expect(slot_already_freezed_validator).to receive(:validate).and_return(false)
              expect(time_slot_validations).to receive(:apply).and_return(true)
              expect(update_time_slot_data).to receive(:update).and_return(true)
              freeze_slot.freeze(args)
            end
          end
        end
      end
    end
  end
end

