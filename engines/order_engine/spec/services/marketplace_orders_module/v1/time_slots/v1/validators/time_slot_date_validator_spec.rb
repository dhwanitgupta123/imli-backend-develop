#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        module Validators
          require 'rails_helper'
          RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotDateValidator do
            let(:time_slot_date_bucket_dao) {MarketplaceOrdersModule::V1::TimeSlotDateBucketDao}
            let(:time_slot_date_validator) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotDateValidator}
            let(:time_slot) {FactoryGirl.build_stubbed(:time_slot, unavailable_dates: [Time.zone.now.to_date])}

            it 'will return true if time_slot is blank' do
              expect(time_slot_date_validator.validate({preferred_date: Time.zone.now.to_date})).to eq(true)
            end

            it 'will return true if preferred_date is blank' do
              expect(time_slot_date_validator.validate({time_slot: time_slot})).to eq(true)
            end

            it 'will return true if preferred_date is not present in time_slot unavailable dates' do
              expect(time_slot_date_validator.validate({preferred_date: Time.zone.now.to_date + 1, time_slot: time_slot})).to eq(true)
            end

            it 'wil return false if preferred_date is present in time_slot unavailable_dates' do
              expect(time_slot_date_validator.validate({preferred_date: Time.zone.now.to_date, time_slot: time_slot})).to eq(false)
            end
          end
        end
      end
    end
  end
end
