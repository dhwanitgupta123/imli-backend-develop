#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        module Validators
          require 'rails_helper'
          RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotAlreadyFreezedValidator do
            let(:slot_already_freezed_validator) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotAlreadyFreezedValidator}
            let(:time_slot_cache_client) {CacheModule::V1::TimeSlotCacheClient}

            it 'it will return false if not present in cache' do
              expect(time_slot_cache_client).to receive(:slot_for_order_exists?).and_return(false)
              expect(slot_already_freezed_validator.validate({})).to eq(false)
            end

            it 'it will return true if present in cache' do
              expect(time_slot_cache_client).to receive(:slot_for_order_exists?).and_return(true)
              expect(slot_already_freezed_validator.validate({})).to eq(true)
            end
          end
        end
      end
    end
  end
end
