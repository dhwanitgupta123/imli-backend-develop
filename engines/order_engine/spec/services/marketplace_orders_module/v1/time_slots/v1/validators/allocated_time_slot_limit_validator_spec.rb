#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        module Validators
          require 'rails_helper'
          RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::AllocatedTimeSlotLimitValidator do
            let(:time_slot_date_bucket_dao) {MarketplaceOrdersModule::V1::TimeSlotDateBucketDao}
            let(:allocated_time_slot_limit_validator) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::AllocatedTimeSlotLimitValidator}
            let(:time_slot) {FactoryGirl.build_stubbed(:time_slot)}
            let(:args) {
              {
                time_slot: time_slot
              }
            }

            it 'it will return false on order count exceeding limit' do
              expect(time_slot_date_bucket_dao).to receive(:allocated_time_slots).and_return(time_slot.order_limit)
              expect(allocated_time_slot_limit_validator.validate(args)).to eq(false)
            end

            it 'it will return true on order count less than limit' do
              expect(time_slot_date_bucket_dao).to receive(:allocated_time_slots).and_return(time_slot.order_limit - 1)
              expect(allocated_time_slot_limit_validator.validate(args)).to eq(true)
            end
          end
        end
      end
    end
  end
end
