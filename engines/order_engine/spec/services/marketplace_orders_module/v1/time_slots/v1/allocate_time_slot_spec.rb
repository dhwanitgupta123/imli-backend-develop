#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        require 'rails_helper'
        RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::AllocateTimeSlot do
          let(:time_slot_date_bucket_dao) {MarketplaceOrdersModule::V1::TimeSlotDateBucketDao}
          let(:time_slot_validations) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotValidations}
          let(:allocate_time_slot) {MarketplaceOrdersModule::V1::TimeSlots::V1::AllocateTimeSlot}
          let(:time_slot) {FactoryGirl.build_stubbed(:time_slot)}
          let(:order) {FactoryGirl.build_stubbed(:order)}
          let(:args) {
            {
              time_slot: time_slot,
              order: order,
              preferred_date: Time.zone.now.to_date
            }
          }

          context 'allocate time slot' do
            it 'raise exception if validations fails' do
              expect(time_slot_validations).to receive(:apply).and_return(false)
              expect{ allocate_time_slot.allocate(args) }.to raise_error(CommonModule::V1::CustomErrors::SlotUnavailable)
            end

            it 'raise exception if slot is unavailable' do
              expect(time_slot_validations).to receive(:apply).and_return(true)
              expect(time_slot_date_bucket_dao).to receive(:update_if_available).
                          and_raise(CommonModule::V1::CustomErrors::SlotUnavailable)
              expect{ allocate_time_slot.allocate(args) }.to raise_error(CommonModule::V1::CustomErrors::SlotUnavailable)
            end

            it 'return true if all validations pass and slot is assigned' do
              expect(time_slot_validations).to receive(:apply).and_return(true)
              expect(time_slot_date_bucket_dao).to receive(:update_if_available).and_return(true)
              expect(allocate_time_slot.allocate(args)).to eq(true)
            end
          end

        end
      end
    end
  end
end

