#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        module Validators
          require 'rails_helper'
          RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotBelongsToSameOrder do
            let(:slot_belong_to_same_order) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotBelongsToSameOrder}
            let(:order) {FactoryGirl.build_stubbed(:order)}
            let(:time_slot) {FactoryGirl.build_stubbed(:time_slot)}

            it 'it will return false if time_slot not assigned to order' do
              order = FactoryGirl.build_stubbed(:order, time_slot: nil)
              expect(slot_belong_to_same_order.validate({order: order})).to eq(false)
            end

            it 'it will return false if preferred date is not equal to order delivery date' do
              expect(slot_belong_to_same_order.validate({order: order, time_slot: order.time_slot, preferred_date: order.preferred_delivery_date + 1})).to eq(false)
            end

            it 'it will return fase if time_slot is not equal to orders time_slot' do
              expect(slot_belong_to_same_order.validate({order: order, preferred_date: order.preferred_delivery_date})).to eq(false)
            end

            it 'it will return true if time_slot and preferred date are equal' do
              expect(slot_belong_to_same_order.validate({order: order, time_slot: order.time_slot, preferred_date: order.preferred_delivery_date})).to eq(true)
            end
          end
        end
      end
    end
  end
end
