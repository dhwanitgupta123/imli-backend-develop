#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        module Validators
          require 'rails_helper'
          RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotBelongsToGeoClusterValidator do
            let(:time_slot_belongs_to_geo_cluster_validator) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotBelongsToGeoClusterValidator}
            let(:order) {FactoryGirl.build_stubbed(:order)}
            let(:time_slot) {FactoryGirl.build_stubbed(:time_slot)}
            let(:time_slot_dao) {MarketplaceOrdersModule::V1::TimeSlotDao}

            it 'it will return false if time slot not present in same goe cluster' do
              expect(time_slot_dao).to receive(:get_active_slots_by_address).and_return([order.time_slot])
              expect(time_slot_belongs_to_geo_cluster_validator.validate({order: order, time_slot: time_slot})).to eq(false)
            end

            it 'it will return true if time slot present in same goe cluster' do
              expect(time_slot_dao).to receive(:get_active_slots_by_address).and_return([order.time_slot])
              expect(time_slot_belongs_to_geo_cluster_validator.validate({order: order, time_slot: order.time_slot})).to eq(true)
            end

          end
        end
      end
    end
  end
end
