#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        require 'rails_helper'
        RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::CacheTimeSlotManipulator do
          let(:time_slot_date_bucket_dao) {MarketplaceOrdersModule::V1::TimeSlotDateBucketDao}
          let(:time_slot_cache_client) {CacheModule::V1::TimeSlotCacheClient}
          let(:time_slot_validations) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotValidations}
          let(:cache_time_slot_manipulator) {MarketplaceOrdersModule::V1::TimeSlots::V1::CacheTimeSlotManipulator}
          let(:cache_util) {CommonModule::V1::Cache}
          let(:freezing_ttl) {Faker::Number.between(1,10)}
          let(:freezing_ttl_in_seconds) {freezing_ttl.minutes.to_i}
          let(:time_slot) {FactoryGirl.build_stubbed(:time_slot)}
          let(:order) {FactoryGirl.build_stubbed(:order)}
          let(:args) {
            {
              time_slot: time_slot,
              order: order,
              preferred_date: Time.zone.now.to_date
            }
          }

          it 'delete slot from cache if present' do
            expect(time_slot_cache_client).to receive(:remove_time_slot).and_return(true)
            expect(cache_time_slot_manipulator.delete(args)).to eq(true)
          end

          context 'time slot' do
            it 'freezed if not present in cache' do
              expect(time_slot_cache_client).to receive(:get_freezed_time_slot).and_return(nil)
              expect(time_slot_cache_client).to receive(:freeze_time_slot_for_order).and_return(true)
              cache_time_slot_manipulator.freeze(args)
            end

            it 'not freeze if already in freezed state' do
              expect(time_slot_cache_client).to receive(:get_freezed_time_slot).and_return(true)
              cache_time_slot_manipulator.freeze(args)
            end
          end

          context 'freezing ttl' do
            it 'updated if remaining ttl is less than FREEZING_TTL_IN_SECONDS' do
              expect(time_slot_cache_client).to receive(:get_time_slot_ttl).and_return(freezing_ttl_in_seconds - 1)
              expect(cache_util).to receive(:read_int).and_return(freezing_ttl)
              expect(time_slot_cache_client).to receive(:freeze_time_slot_for_order).and_return(true)
              cache_time_slot_manipulator.update_freezed_ttl(args)
            end

            it 'not updated if remaining ttl is greater than FREEZING_TTL_IN_SECONDS' do
              expect(time_slot_cache_client).to receive(:get_time_slot_ttl).and_return(freezing_ttl_in_seconds + 1)
              expect(cache_util).to receive(:read_int).and_return(freezing_ttl)
              cache_time_slot_manipulator.update_freezed_ttl(args)
            end
          end
        end
      end
    end
  end
end

