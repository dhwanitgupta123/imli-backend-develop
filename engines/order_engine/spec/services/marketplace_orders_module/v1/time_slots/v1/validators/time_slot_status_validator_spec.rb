#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    module TimeSlots
      module V1
        module Validators
          require 'rails_helper'
          RSpec.describe MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotStatusValidator do
            let(:time_slot_status_validator) {MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotStatusValidator}
            let(:time_slot) {FactoryGirl.build_stubbed(:time_slot)}
            let(:time_slot_states) {MarketplaceOrdersModule::V1::ModelStates::V1::TimeSlotStates}

            it 'will return true if time_slot is active' do
              time_slot = FactoryGirl.build_stubbed(:time_slot, status: time_slot_states::ACTIVE)
              expect(time_slot_status_validator.validate({time_slot: time_slot})).to eq(true)
            end

            it 'will return true if time_slot is inactive' do
              time_slot = FactoryGirl.build_stubbed(:time_slot, status: time_slot_states::INACTIVE)
              expect(time_slot_status_validator.validate({time_slot: time_slot})).to eq(false)
            end
          end
        end
      end
    end
  end
end
