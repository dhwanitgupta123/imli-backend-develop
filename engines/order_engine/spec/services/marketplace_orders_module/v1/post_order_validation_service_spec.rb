#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceOrdersModule::V1::PostOrderValidationService do
      let(:version) { 1 }
      let(:post_order_validation_service) {MarketplaceOrdersModule::V1::PostOrderValidationService.instance(version)}
      let(:time_slot_validator) {MarketplaceOrdersModule::V1::OrderValidationModule::V1::TimeSlotValidator}
      let(:time_slot_availability_service) {MarketplaceOrdersModule::V1::TimeSlotAvailabilityService}
      let(:order_dao) {MarketplaceOrdersModule::V1::OrderDao}
      let(:order) {FactoryGirl.build(:order)}

      describe 'apply_post_order_validations' do
        it 'with slot not present' do
          expect_any_instance_of(time_slot_availability_service).
            to receive(:check_if_atleast_one_slot_available_for_order?).and_return(true)
          expect_any_instance_of(order_dao).to receive(:get_order_by_id).and_return(order)
          expect(time_slot_validator).to receive(:apply_validation).and_return(false)
          data = post_order_validation_service.apply_post_order_validations(order)
          expect(data[:is_valid]).to eq(false)
        end

        it 'with slot present ' do
          expect_any_instance_of(time_slot_availability_service).
            to receive(:check_if_atleast_one_slot_available_for_order?).and_return(true)
          expect_any_instance_of(order_dao).to receive(:get_order_by_id).and_return(order)
          expect(time_slot_validator).to receive(:apply_validation).and_return(true)
          data = post_order_validation_service.apply_post_order_validations(order)
          expect(data[:is_valid]).to eq(true)
        end

        it 'will return true if no slots available in that zone' do
          expect_any_instance_of(time_slot_availability_service).
            to receive(:check_if_atleast_one_slot_available_for_order?).and_return(false)
          expect_any_instance_of(order_dao).to receive(:get_order_by_id).and_return(order)
          data = post_order_validation_service.apply_post_order_validations(order)
          expect(data[:is_valid]).to eq(true)
        end
      end
    end
  end
end
