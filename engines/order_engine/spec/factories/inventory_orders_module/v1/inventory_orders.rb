module InventoryOrdersModule
  module V1
    FactoryGirl.define do
      factory :inventory_order, :class => InventoryOrdersModule::V1::InventoryOrder do
        association :user, factory: :user
        association :warehouse, factory: :warehouse
        association :inventory, factory: :inventory
        order_id Faker::Lorem.characters(10)
        status Faker::Number.number(1)
      end
    end
  end
end
