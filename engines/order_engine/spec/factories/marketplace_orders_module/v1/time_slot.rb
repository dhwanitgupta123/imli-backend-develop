module MarketplaceOrdersModule
  module V1
    FactoryGirl.define do
      factory :time_slot, :class => MarketplaceOrdersModule::V1::TimeSlot do
        label Faker::Lorem.characters(10)
        order_limit Faker::Number.number(1)
        buffer_limit Faker::Number.number(1)
        status Faker::Number.number(1)
        from_time Time.zone.now
        to_time Time.zone.now + Faker::Number.between(1, 9).hours
        unavailable_dates [Time.zone.now.to_date]
        unavailable_days [Faker::Number.between(0, 6)]
        min_slot_offset Faker::Number.between(24, 100)
        max_slot_offset Faker::Number.between(144, 400)
      end
    end
  end
end
