module MarketplaceOrdersModule
  module V1
    FactoryGirl.define do
      factory :time_slot_date_bucket, :class => MarketplaceOrdersModule::V1::TimeSlotDateBucket do
        association :time_slot, factory: :time_slot
        label Faker::Lorem.characters(10)
        order_count Faker::Number.number(1)
        buffer_count Faker::Number.number(1)
        slot_date Time.zone.now
      end
    end
  end
end
