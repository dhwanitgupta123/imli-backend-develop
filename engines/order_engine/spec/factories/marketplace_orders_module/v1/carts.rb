module MarketplaceOrdersModule
  module V1
    FactoryGirl.define do
      factory :cart, :class => MarketplaceOrdersModule::V1::Cart do
        association :user, factory: :user, strategy: :build_stubbed
        user_cart_status Faker::Number.number(1)
        cart_total Faker::Commerce.price
        cart_savings Faker::Commerce.price
      end
    end
  end
end
