module MarketplaceOrdersModule
  module V1
    FactoryGirl.define do
      factory :order_context, :class => MarketplaceOrdersModule::V1::OrderContext do
        order_id Faker::Number.number(1)
        app_version Faker::Lorem.word
        platform_type Faker::Number.number(1)
        order_user_tag Faker::Number.number(1)
      end
    end
  end
end
