module MarketplaceOrdersModule
  module V1
    FactoryGirl.define do
      factory :order, :class => MarketplaceOrdersModule::V1::Order do
        association :user, factory: :user, strategy: :build_stubbed
        association :address, factory: :address, strategy: :create
        association :time_slot, factory: :time_slot, strategy: :create
        order_id Faker::Lorem.characters(10)
        ship_by Faker::Time.backward(1)
        status Faker::Number.number(1)
        preferred_delivery_date Time.zone.now.to_date
      end
    end
  end
end
