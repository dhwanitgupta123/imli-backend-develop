$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "order_engine/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "order_engine"
  s.version     = OrderEngine::VERSION
  s.authors     = ["Kartik Verma"]
  s.email       = ["kartx111@gmail.com"]
  s.homepage    = "https://www.trawly.com"
  s.summary     = "Summary of OrderEngine."
  s.description = "Description of OrderEngine."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.3"

  s.test_files = Dir["spec/**/*"]
end
