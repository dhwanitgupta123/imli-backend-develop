#
# Module to handle all the functionalities related to Decorators
#
module BaseOrdersModule
  #
  # Version1 for base module
  #
  module V1
    #
    # Base module to inject generic utils
    #
    class BaseResponseDecorator < BaseModule::V1::BaseResponseDecorator

      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes

      def initialize(params='')
        @params = params
      end

    end
  end
end
