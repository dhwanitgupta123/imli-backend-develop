module MarketplaceOrdersModule
  module V1
    class CartsResponseDecorator < BaseOrdersModule::V1::BaseResponseDecorator

      def self.state_change_response(cart_hash, order_extras_hash, products_change_message)
        return {
          payload: {
            cart: cart_hash,
            extras: order_extras_hash
          },
          error: {
            message: products_change_message
          },
          response: RESPONSE_CODES_UTIL::CONFLICT_IN_STATE
        }
      end

      def self.success_response(cart_hash, order_extras_hash)
        return {
          payload: {
            cart: cart_hash,
            extras: order_extras_hash
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

    end
  end
end
