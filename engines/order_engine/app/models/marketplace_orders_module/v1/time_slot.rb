#
# Module to handle all the functionalities related to orders
#
  module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    # Model for time slots table
    class TimeSlot < BaseModule::BaseModel

      TIME_SLOT_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::TimeSlotStates
      CONTENT = CommonModule::V1::Content
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors

      has_many :orders

      belongs_to :geographical_cluster, class_name: 'GeographicalClustersModule::V1::GeographicalCluster'

      validate :validate_overlapping_time_slots

      validate :validate_slot_offsets

      validates :from_time, presence: true
      validates :to_time, presence: true

      before_create :init_times_in_seconds
      before_update :update_times_in_seconds

      #
      # Workflow to define states of the Payment
      #
      # Initial State => PENDING
      #
      # State Diagram::
      #   PENDING --payment_fail--> FAILED
      #   PENDING --payment_success--> SUCCESS
      #
      # * payment_fail, payment_success are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do

        state :inactive, TIME_SLOT_STATES::INACTIVE do
          event :activate, transitions_to: :active
          event :soft_delete, transitions_to: :deleted
        end

        before_transition do |from, to, triggering_event, *event_args|
          if to == :active && validate_overlapping_time_slots(true) == false
            raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(@message)
          end
        end
        state :active, TIME_SLOT_STATES::ACTIVE do
          event :deactivate, transitions_to: :inactive
          event :soft_delete, transitions_to: :deleted
        end
        state :deleted, TIME_SLOT_STATES::DELETED
      end

      #
      # Trigger time slot state change
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
           raise CUSTOM_ERROR_UTIL::PreConditionRequiredError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end


      def validate_overlapping_time_slots(ignore_inactive_status = false)

        return true if ignore_inactive_status == false && self.status != TIME_SLOT_STATES::ACTIVE

        if self.geographical_cluster_id.present?

          geographical_cluster = self.geographical_cluster
          already_present_time_slots = geographical_cluster.time_slots.where(status: TIME_SLOT_STATES::ACTIVE)

          return true if already_present_time_slots.blank?

          unavailable_weekdays = self.unavailable_days

          available_weekdays = DateTimeModule::V1::DayEnums::DAYS - (unavailable_weekdays || [])

          if available_weekdays.blank?
            errors.add(:unavailable_weekdays, 'No available days provided')
            return false
          end

          available_weekdays.each do |day|
            if validate_slot_in_day(already_present_time_slots, day) == false
              return false
            end
          end
        end
        return true
      end

      def validate_slot_in_day(time_slots, day)

        time_slots.each do |time_slot|
          next if time_slot.id == self.id
          available_weekdays = DateTimeModule::V1::DayEnums::DAYS - (time_slot.unavailable_days || [])
          next if available_weekdays.blank? || !available_weekdays.include?(day)
          return false if slot_overlap?(time_slot)
        end

        return true
      end

      def slot_overlap?(time_slot)
        date_time_util = CommonModule::V1::DateTimeUtil
        if (date_time_util.compare_hour_and_minute(self.from_time, time_slot.from_time) > 0 &&
            date_time_util.compare_hour_and_minute(self.from_time, time_slot.to_time) < 0) ||
              (date_time_util.compare_hour_and_minute(self.to_time, time_slot.from_time) > 0 &&
                date_time_util.compare_hour_and_minute(self.to_time, time_slot.to_time) < 0)

          formatted_from_time = date_time_util.get_readable_time(time_slot.from_time.in_time_zone, "%H:%M")
          formatted_to_time = date_time_util.get_readable_time(time_slot.to_time.in_time_zone, "%H:%M")
          @message = 'Overlapping slot with  ' + time_slot.label + ' from time ' + formatted_from_time.to_s +
            ' to time ' + formatted_to_time.to_s
          errors.add(:base, @message)
          return true
        end

        return false

      end

      def validate_slot_offsets

        min_expected_slot_offset = CommonModule::V1::Cache.read_int('MIN_SLOT_OFFSET') || 24

        if self.min_slot_offset.blank? || self.min_slot_offset < min_expected_slot_offset
          errors.add(:base, 'Min slot offset should be greater than ' + min_expected_slot_offset.to_s)
          return false
        end

        minimum_max_expected_slot_offset = CommonModule::V1::Cache.read_int('MAX_SLOT_OFFSET') || 144

        if self.max_slot_offset.blank? || self.max_slot_offset < minimum_max_expected_slot_offset
          errors.add(:base, 'Max slot offset should be greater than ' + minimum_max_expected_slot_offset.to_s)
          return false
        end

        return true
      end

      #
      # This function runs before create and initialize from_time_in_seconds and to_time_in_seconds
      #
      def init_times_in_seconds
        self.from_time_in_seconds = CommonModule::V1::DateTimeUtil.convert_time_to_seconds(self.from_time.in_time_zone)
        self.to_time_in_seconds = CommonModule::V1::DateTimeUtil.convert_time_to_seconds(self.to_time.in_time_zone)
      end

      #
      # This function runs before update and update from_time_in_seconds and to_time_in_seconds
      # if from_time and to_time changes
      #
      def update_times_in_seconds
        if self.changed? && (self.changes[:from_time].present? || self.changes[:to_time].present?)
          init_times_in_seconds
        end
      end
    end
  end
end
