#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module ModelStates

      module V1
        # Class for Payment states
        class ThirdPartyOrderStates

          PENDING     = 1
          PLACED     = 2
          DISCARDED   = 3
          CANCELLED   = 4

          #
          # Get display name for Third party order state
          #
          # @param state [Integer] [State of third_party_order]
          #
          # @return [String] [3p Order state display name]
          #
          def self.get_third_party_order_state_display_name(state)
            display_name = ''
            return display_name if state.blank?

            case state.to_i
              when PENDING
                display_name = 'PENDING'
              when PLACED
                display_name = 'PLACED'
              when DISCARDED
                display_name = 'DISCARDED'
              when CANCELLED
                display_name = 'CANCELLED'
            end
            return display_name
          end

        end
      end
    end
  end
end
