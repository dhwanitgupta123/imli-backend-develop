#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module ModelStates

      module V1
        # Class for cart states
        class CartStates

          EMPTY_CART  = 1
        	ACTIVE      = 2
          CHECKED_OUT = 3
          DISCARDED   = 4

   	    end
      end
    end
  end
end