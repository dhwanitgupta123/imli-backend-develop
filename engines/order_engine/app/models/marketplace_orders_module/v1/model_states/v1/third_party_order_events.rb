#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    module ModelStates

      module V1
        # Class for Payment Events
        class ThirdPartyOrderEvents

          PLACE              = 1
          DISCARD_ORDER      = 2
          CANCEL_ORDER       = 3

        end
      end
    end
  end
end
