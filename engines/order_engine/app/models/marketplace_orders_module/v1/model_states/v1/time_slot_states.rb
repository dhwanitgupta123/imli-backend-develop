#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module ModelStates

      module V1
        # Class for cart states
        class TimeSlotStates

          ACTIVE      = 1
          INACTIVE    = 2
          DELETED     = 3

        end
      end
    end
  end
end