#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    module ModelStates

      module V1
        class CartEvents

          ADD_TO_CART = 1
          EMPTY_OUT   = 2
          CHECKOUT    = 3
          DISCARD_CART = 4

        end
      end
    end
  end
end