#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module MarketplaceCartsModule
      module V1

        module ModelStates
          module V1
            # Class for order product states
            class OrderProductStates

              ACTIVE      = 1
              INACTIVE    = 2

       	    end
          end
        end
      end
    end
  end
end