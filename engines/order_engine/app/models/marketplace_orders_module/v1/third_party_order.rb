#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for marketplace orders module
  #
  module V1
    # Model for orders table
    class ThirdPartyOrder < ActiveRecord::Base
      CUSTOM_ERRORS_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      THIRD_PARTY_ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::ThirdPartyOrderStates

      belongs_to :order
      validates :order, presence: true

      #
      # Workflow to define states of the Order
      #
      # Initial State => Initiated
      #
      #
      # * initiate, place are the event which triggers the state transition
      #
      include Workflow
      workflow_column :status
      workflow do
        state :pending, THIRD_PARTY_ORDER_STATES::PENDING do
          event :place, transitions_to: :placed
          event :cancel_order, transitions_to: :order_cancelled
          event :discard_order, transitions_to: :discarded
        end
        state :placed, THIRD_PARTY_ORDER_STATES::PLACED do
          # If Imli Team wants to cancel the placed order manually from Panel
          event :cancel_order, transitions_to: :order_cancelled
        end
        state :discarded, THIRD_PARTY_ORDER_STATES::DISCARDED
        state :order_cancelled, THIRD_PARTY_ORDER_STATES::CANCELLED
      end

      #
      # Trigger ORDER state change
      #
      def trigger_event(event)
        event = 'self.' + event + '!'
        begin
          eval(event)
        rescue Workflow::NoTransitionAllowed => e
          raise CUSTOM_ERRORS_UTIL::PreConditionRequiredError.new(CONTENT_UTIL::EVENT_NOT_ALLOWED)
        end
      end

    end
  end
end

