#
# Module to handle all the functionalities related to Models
#
module BaseOrdersModule
  #
  # Version1 for base module
  #
  module V1
    #
    # Base module to inject generic utils
    #
    class BaseModel
      def initialize(params='')
      end
    end
  end
end