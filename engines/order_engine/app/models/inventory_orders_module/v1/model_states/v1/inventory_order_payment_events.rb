#
# Module to handle all the functionalities related to inventory orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    module ModelStates
      module V1
        #
        # Class for inventory Order payment events
        #
        class InventoryOrderPaymentEvents
          PARTIAL_PAYMENT   = 1
          COMPLETE_PAYMENT = 2
        end
      end
    end
  end
end
