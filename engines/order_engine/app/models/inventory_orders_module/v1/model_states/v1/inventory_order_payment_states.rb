#
# Module to handle all the functionalities related to inventory orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    module ModelStates
      module V1
        #
        # Class for inventory Order payment states
        #
        class InventoryOrderPaymentStates
          PENDING      = 1
          PARTIAL      = 2
          COMPLETED    = 3
        end
      end
    end
  end
end
