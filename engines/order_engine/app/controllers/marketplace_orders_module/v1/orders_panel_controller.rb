#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Defining Orders Panel Controller
    #
    class OrdersPanelController < BaseModule::V1::ApplicationController

      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper

      #
      # before actions to be executed
      #
      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end
      #
      # initialize OrdersController Class
      #
      def initialize
        @get_all_orders = MarketplaceOrdersModule::V1::GetAllOrdersApi
        @get_order_by_order_id = MarketplaceOrdersModule::V1::GetOrderByOrderIdApi
        @get_packing_details_by_order_id = MarketplaceOrdersModule::V1::GetPackingDetailsByOrderIdApi
        @get_invoice_by_order_id = MarketplaceOrdersModule::V1::GetInvoiceByOrderIdApi
        @get_aggregated_products = MarketplaceOrdersModule::V1::GetAggregatedProductsApi
        @change_state_api = MarketplaceOrdersModule::V1::ChangeStateApi
        # Order Edit APIs
        @edit_order_address_api = MarketplaceOrdersModule::V1::EditOrderAddressApi
        @edit_order_time_slot_api = MarketplaceOrdersModule::V1::EditOrderTimeSlotApi
        @edit_order_cart_api = MarketplaceOrdersModule::V1::EditOrderCartApi
        @update_order_context_api = MarketplaceOrdersModule::V1::UpdateOrderContextApi
      end

      ##################################################################################
      #                             get_all_orders API                                 #
      ##################################################################################

      #
      # function to get all active order details with minimal details
      #
      # GET marketplace/orders
      #
      # Response::
      #   * send details of all orders
      #
      def get_all_orders
        get_all_orders_api = @get_all_orders.new(@deciding_params)
        response = get_all_orders_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_all_orders do
        summary 'It returns minimal details of all orders'
        notes 'get_all_orders API returns the minimal details of all orders'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :from_time, :integer, :optional, 'Epoch timestamp of from date-time'
        param :query, :to_time, :integer, :optional, 'Epoch timestamp of TO date-time'
        param :query, :page_no, :integer, :optional, 'page no from which to get all orders'
        param :query, :per_page, :integer, :optional, 'how many orders to display in a page'
        param :query, :state, :integer, :optional, 'to fetch orders based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                           get_order_by_order_id API                            #
      ##################################################################################

      #
      # function to get a marketplace order
      #
      # GET marketplace/orders/:order_id
      #
      # Request::
      #   * order_id [string] order_id of order which needs to be retrieved
      #
      # Response::
      #   * sends ok response to the panel with order details
      #
      def get_order_by_order_id
        get_order_by_order_id_api = @get_order_by_order_id.new(@deciding_params)
        response = get_order_by_order_id_api.enact({ order_id: get_order_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_order_by_order_id do
        summary 'It return a order with details'
        notes 'get_order_by_order_id API returns a single order details corresponding to the order_id'
        param :path, :order_id, :string, :required, 'order_id to be fetched'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or order not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                         get_aggregated_products API                            #
      ##################################################################################

      #
      # function to get aggregated products for pickers to pick
      #
      # GET marketplace/orders/aggregated
      #
      # Response::
      #   * sends ok response to the list of aggregated product and mails it
      #
      def get_aggregated_products
        get_aggregated_products = @get_aggregated_products.new(@deciding_params)
        response = get_aggregated_products.enact(get_aggregated_products_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_aggregated_products do
        summary 'It returns aggregated products with quantity details to be used by picker'
        notes 'get_aggregated_products API returns aggregated products with quantity details to be used by picker'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :from_time    , :integer, :optional, 'Epoch Unix Time Stamp'
        param :query, :to_time      , :integer, :optional, 'Epoch Unix Time Stamp'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                             change_state API                                   #
      ##################################################################################

      #
      # function to change state of Orders
      #
      # put /orders/state/:id
      #
      # Request:: request object contain
      #   * id [integer] id of Order whose state is to be changed
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      _LogActivity_
      def change_state
        change_state_api = @change_state_api.new(@deciding_params)
        response = change_state_api.enact(change_state_params)
        send_response(response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :change_state do
        summary 'It changes the state of Orders'
        notes 'change_state API takes event & id as input and triggers the event to change the state of the Order'
        param :path, :id, :integer, :required, 'Order to update'
        param :body, :order_change_state, :order_change_state, :required, 'change_state request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of order'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :order_change_state do
        description 'request schema for /change_state API'
        property :order, :order_change_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :order_change_state_hash do
        description 'event to be triggered'
        property :event, :integer, :required, 'event to trigger status change'
      end


      ##################################################################################
      #                             bulk_change_state API                                   #
      ##################################################################################

      #
      # function to change state of Orders
      #
      # put /orders/bulk/update_state
      #
      # Request:: request object contain
      #   * id [integer] id of Order whose state is to be changed
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      def bulk_change_state
        bulk_change_state_api = MarketplaceOrdersModule::V1::BulkChangeStateApi.instance(@deciding_params)
        response = bulk_change_state_api.enact(bulk_change_state_params)
        send_response(response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :bulk_change_state do
        summary 'It changes the state of Orders'
        notes 'change_state API takes event & ids as input and triggers the event to change the state of the Orders'
        param :path, :id, :integer, :required, 'Order to update'
        param :body, :bulk_change_state, :bulk_change_state, :required, 'change_state request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of order'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :bulk_change_state do
        description 'request schema for /change_state API'
        property :order_context, :order_context_hash, :optional, 'hash of order_context'
        property :order, :bulk_change_state_hash, :required, 'hash of event to trigger state change'
        property :additional_info, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :additional_info_hash } }
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :bulk_change_state_hash do
        description 'event to be triggered'
        property :event, :integer, :required, 'event to trigger status change'
        property :ids, :array, :optional, 'array of order ids', { 'items': { 'type': 'integer' } }
      end

      swagger_model :additional_info_hash do
        description 'details of image id and corresponding priority'
        property :key, :string, :required, 'reference to image'
        property :task, :integer, :required, 'priority of image'
        property :params, :string, :optional, 'priority of image'
      end

      ##################################################################################
      #                         edit_order_time_slot API                               #
      ##################################################################################

      #
      # function to change state of Orders
      #
      # PUT /orders/:id/time_slot
      #
      # Request:: request object contain
      #   * id [integer] id of Order which is to be updated
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      _LogActivity_
      def edit_order_time_slot
        edit_order_time_slot_api = @edit_order_time_slot_api.new(@deciding_params)
        edit_order_time_slot_response = edit_order_time_slot_api.enact(edit_order_time_slot_params)
        send_response(edit_order_time_slot_response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :edit_order_time_slot do
        summary 'It changes the already placed Order'
        notes 'It takes various updated hashed and then update current order accordingly'
        param :path, :id, :integer, :required, 'Order to update'
        param :body, :order_time_slot_edit_hash, :order_time_slot_edit_hash, :required, 'order_edit request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of order'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :order_time_slot_edit_hash do
        description 'request schema for /order_edit API'
        property :order, :order_hash_for_order_time_slot_edit, :required, 'hash of order containing various attributes hashes'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :order_hash_for_order_time_slot_edit do
        description 'details of order_params_for_place_order'
        property :preferred_slot, :preferred_slot_hash_for_order_time_slot_edit, :required, 'preferred slot for the order'
      end
      swagger_model :preferred_slot_hash_for_order_time_slot_edit do
        description 'details of preferred_slot_hash_for_order_time_slot_edit'
        property :slot_date, :string, :required, 'slot date'
        property :time_slot, :time_slot_hash_for_order_time_slot_edit, 'time slot hash'
      end
      swagger_model :time_slot_hash_for_order_time_slot_edit do
        description 'details of time slot hash'
        property :id, :integer, :required, 'id of the chosen slot for the order'
      end

      ##################################################################################
      #                        get_packing_details_by_order_id API                     #
      ##################################################################################

      #
      # function to get packing details of marketplace order
      #
      # GET marketplace/orders/packing_details/:order_id
      #
      # Request::
      #   * order_id [string] order_id of order which needs to be retrieved
      #
      # Response::
      #   * sends ok response to the panel with packing details
      #
      def get_packing_details_by_order_id
        get_packing_details_by_order_id_api = @get_packing_details_by_order_id.new(@deciding_params)
        response = get_packing_details_by_order_id_api.enact({ order_id: get_order_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_packing_details_by_order_id do
        summary 'It returns packing details of an order'
        notes 'get_packing_details_by_order_id API returns a single order packing details corresponding to the order_id'
        param :path, :order_id, :string, :required, 'order_id to be fetched'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or order not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                           get_invoice_by_order_id API                          #
      ##################################################################################

      #
      # function to get invoice of marketplace order
      #
      # GET marketplace/orders/invoice/:order_id
      #
      # Request::
      #   * order_id [string] order_id of order which needs to be retrieved
      #
      # Response::
      #   * sends ok response to the panel with invoice
      #
      def get_invoice_by_order_id
        get_invoice_by_order_id_api = @get_invoice_by_order_id.new(@deciding_params)
        response = get_invoice_by_order_id_api.enact({ order_id: get_order_id_from_params })
        send_response(response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_invoice_by_order_id do
        summary 'It returns invoice of an order'
        notes 'get_invoice_by_order_id API returns invoice corresponding to the order_id'
        param :path, :order_id, :string, :required, 'order_id to be fetched'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or order not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ##################################################################################
      #                         edit_order_address API                                 #
      ##################################################################################

      #
      # function to change address of Orders
      #
      # PUT /orders/:id/address
      #
      # Request:: request object contain
      #   * id [integer] id of Order which is to be updated
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      _LogActivity_
      def edit_order_address
        edit_order_address_api = @edit_order_address_api.new(@deciding_params)
        edit_order_address_response = edit_order_address_api.enact(edit_order_address_params)
        send_response(edit_order_address_response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :edit_order_address do
        summary 'It changes the already placed Order'
        notes 'It takes various updated hashed and then update current order accordingly'
        param :path, :id, :integer, :required, 'Order to update'
        param :body, :order_address_edit_hash, :order_address_edit_hash, :required, 'order_address_edit request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of order'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :order_address_edit_hash do
        description 'request schema for /order_edit API'
        property :order, :order_hash_for_order_address_edit, :required, 'hash of order containing various attributes hashes'
        property :session_token, :string, :required, 'session token to authenticate user'
      end
      swagger_model :order_hash_for_order_address_edit do
        description "params for updating address of the order"
        property :address, :address_hash_for_order_address_edit, :required, "address hash with address details"
      end
      swagger_model :address_hash_for_order_address_edit do
        description "Address hash with address related details of user"
        property :id, :integer, :required, 'Address id'
        property :recipient_name, :string, :required, 'Name of the order recipient'
        property :nickname, :string, :optional, "Address nickname"
        property :address_line1, :string, :optional, "User Flat & Building name"
        property :address_line2, :string, :optional, "User Locality Name"
        property :landmark, :string, :optional, "Address landmark"
        property :pincode, :string, :optional, "Address pincode"
      end


      ##################################################################################
      #                         edit_order_cart API                                    #
      ##################################################################################

      #
      # function to change state of Orders
      #
      # PUT /orders/:id/cart
      #
      # Request:: request object contain
      #   * id [integer] id of Order which is to be updated
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      _LogActivity_
      def edit_order_cart
        edit_order_cart_api = @edit_order_cart_api.new(@deciding_params)
        edit_order_cart_response = edit_order_cart_api.enact(edit_order_cart_params)
        send_response(edit_order_cart_response)
      end

      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :edit_order_cart do
        summary 'It changes the already placed Order'
        notes 'It takes various updated hashed and then update current order accordingly'
        param :path, :id, :integer, :required, 'Order to update'
        param :body, :order_cart_edit_hash, :order_cart_edit_hash, :required, 'order_edit request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of order'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :order_cart_edit_hash do
        description 'request schema for /order_edit API'
        property :order, :order_hash_for_order_cart_edit, :required, 'hash of order containing various attributes hashes'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :order_hash_for_order_cart_edit do
        description 'details of order_params_for_place_order'
        property :cart, :cart_hash_for_order_cart_edit, :required, 'preferred slot for the order'
      end
      swagger_model :cart_hash_for_order_cart_edit do
        description 'details of cart_hash_for_order_cart_edit'
        property :id, :integer, :required, 'Id of the cart'
        property :order_products, :array, :required, 'MRP of MPSP',
                { 'items': { 'type': :order_products_array_for_order_cart_edit } }
      end
      swagger_model :order_products_array_for_order_cart_edit do
        description 'details of order product with quantity and MPSP to make OrderProduct'
        property :quantity, :integer, :required, 'quantity of order product'
        property :additional_discount, :required, 'additional discount to be applied on product'
        property :product, :product_hash_for_order_cart_edit, :required, 'product hash of MPSP'
      end
      swagger_model :product_hash_for_order_cart_edit do
        description 'details of MPSP with its id'
        property :id, :integer, :required, 'id reference to MPSP'
      end

      ##################################################################################
      #                         update_order_context API                               #
      ##################################################################################

      #
      # function to change state of Orders
      #
      # PUT /orders/:id/context
      #
      # Request:: request object contain
      #   * id [integer] id of Order which is to be updated
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      def update_order_context
        update_order_context_api = @update_order_context_api.new(@deciding_params)
        update_order_context_response = update_order_context_api.enact(update_order_context_params)
        send_response(update_order_context_response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :update_order_context do
        summary 'It update order context'
        notes 'update_order_context API update order context'
        param :path, :id, :integer, :required, 'Order to update'
        param :body, :update_order_context_hash, :update_order_context_hash, :required, 'update_order_context_hash'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_order_context_hash do
        description 'request schema for /update_order_context API'
        property :order_context, :order_context_hash, :required, 'hash of order_context'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :order_context_hash do
        description 'order_context_hash'
        property :order_user_tag, :integer, :required, 'Tag for order usage'
        property :delivery_boy, :string, :optional, 'name of delivery boy'
        property :box_tags, :array, :optional, 'array of boxes', { 'items': { 'type': 'string' } }
      end


      ##################################################################################
      #                          Reschedule_order API                                    #
      ##################################################################################

      #
      # Function to reschedule order
      #
      # POST /order/:id/reschedule
      #
      # Request::
      #   * order [hash] it contains preferred slot hash
      # check private function reschedule_order_params for more details
      #
      # Response::
      #   * sends ok response to the user
      #
      _LogActivity_
      def reschedule_order

        reschedule_order_api =  MarketplaceOrdersModule::V1::RescheduleOrderApi.instance(@deciding_params)
        reschedule_order_request = reschedule_order_params.merge({
                                                               order_id: params[:id]
                                                             })
        response = reschedule_order_api.enact(reschedule_order_request)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :reschedule_order do
        summary 'It reschedules order for the user'
        notes 'reschedules order as per the passed preferred date and time slot'
        param :path, :id, :integer, :required, 'id of order which is to be scheduled'
        param :body, :reschedule_order_request, :reschedule_order_request, :required,
              'place_order_request request'
        response :bad_request, 'if request params are incorrect or api fails to place order'
        response :forbidden, 'User does not have permissions to access this page'
        response :not_found, 'Order not found'
        response :run_time_error, 'if api fails to place order or update cart'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :reschedule_order_request do
        description 'request schema for /reschedule order API'
        property :order, :order_params_for_reschedule_order, :required,
                 'order_params_for_reschedule_order model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :order_params_for_reschedule_order do
        description 'details of order_params_for_reschedule_order'
        property :preferred_slot, :preferred_slot_hash_for_reschedule_order, :required, 'preferred slot for the order'
      end
      swagger_model :preferred_slot_hash_for_reschedule_order do
        description 'details of preferred_slot_hash_for_schedule_order'
        property :slot_date, :string, :required, 'slot date'
        property :time_slot, :time_slot_hash_for_rescheduled_order, 'time slot hash'
      end
      swagger_model :time_slot_hash_for_rescheduled_order do
        description 'details of time slot hash'
        property :id, :integer, :required, 'id of the chosen slot for the order'
      end

      ##################################################################################
      #                          GetAvailableSlotsForOrder                             #
      ##################################################################################

      #
      # Function to reschedule order
      #
      # GET orders/:id/available_slots
      #
      # Response::
      #   * sends ok response to the user
      #
      def get_available_slots_for_order
        get_available_slots_for_order =  MarketplaceOrdersModule::V1::GetAvailableSlotsForOrderApi.
                                                                                      instance(@deciding_params)
        response = get_available_slots_for_order.enact({ order_id: params[:id] })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_available_slots_for_order do
        summary 'It return a order with details'
        notes 'get_order_by_order_id API returns a single order details corresponding to the order_id'
        param :path, :id, :integer, :required, 'Order for which slots required'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or order not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      def get_orders_by_delivery_filters
        get_orders_by_delivery_filters =  MarketplaceOrdersModule::V1::GetOrdersByDeliveryFiltersApi.
          instance(@deciding_params)
        response = get_orders_by_delivery_filters.enact(delivery_filter_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :orders_panel, 'MarketplaceOrdersModule APIs'
      swagger_api :get_orders_by_delivery_filters do
        summary 'It returns minimal details of all orders'
        notes 'get_all_orders API returns the minimal details of all orders'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :from_time, :integer, :optional, 'Epoch timestamp of from date-time'
        param :query, :to_time, :integer, :optional, 'Epoch timestamp of TO date-time'
        param :query, :page_no, :integer, :optional, 'page no from which to get all orders'
        param :query, :per_page, :integer, :optional, 'how many orders to display in a page'
        param :query, :unallocated, :boolean, :optional, 'returns orders which are not scheduled'
        param :query, :zones, :array, :optional, 'array of zones', { 'items': { 'type': 'integer' } }
        param :query, :states, :array, :optional, 'array of states', { 'items': { 'type': 'integer' } }
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      ##################################################################################
      #                           get_dispatch_list API                                #
      ##################################################################################

      #
      # function to get dispatch list for given order IDs
      # It generats dispatch list as per the given order ids
      # and return it in pdf and csv format as requested
      #
      # GET /marketplace/orders/dispatch_list
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated Order
      #    else returns BadRequest response
      #
      def get_dispatch_list
        get_dispatch_list_api = MarketplaceOrdersModule::V1::GetDispatchListApi.instance(@deciding_params)
        get_dispatch_list_response = get_dispatch_list_api.enact(get_dispatch_list_params)
        send_response(get_dispatch_list_response)
      end

      swagger_api :get_dispatch_list do
        summary 'It returns dispatch list for the given order ids'
        notes 'get_dispatch_list API for generating dispatch list as per the given order ids
        and return it in pdf and csv format as requested'
        param :query, :csv, :string, :optional, '1 or 0 based on required list needed in csv format or not', { 'default_value': '0' }
        param :query, :pdf, :string, :optional, '1 or 0 based on required list needed in pdf format or not', { 'default_value': '0' }
        param :query, :ids, :array, :required, 'order ids for which list is to be generated'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # Params should contain hash with keys 1. order, 2. order_context, 3. additional_info
      # request[:order] contains key ids -> array of ids and event
      # request[:order_context] contains update metadeta for the orders
      # request[:additional_info] contains array of hash which contains key, task and params of the corresponding task
      #
      def bulk_change_state_params
        if params[:additional_info].present? && params[:additional_info].is_a?(String)
          params[:additional_info] = JSON.parse(params[:additional_info])
        end

        if params[:additional_info].present? && params[:additional_info].is_a?(Array)
          params[:additional_info].each_with_index do |val, index|
            if params[:additional_info][index][:params].present? && params[:additional_info][index][:params].is_a?(String)
              params[:additional_info][index][:params] = JSON.parse(params[:additional_info][index][:params])
            end
          end
        end
        {
          order_context: params.require(:order_context).permit(:order_user_tag, :delivery_boy, :box_tags => []),
          order: params.require(:order).permit(:event, :ids => []),
          additional_info: params[:additional_info]
        }
      end

      def delivery_filter_params
        if params[:zones].present? && params[:zones].is_a?(String)
          params[:zones] = JSON.parse(params[:zones])
        end
        if params[:states].present? && params[:states].is_a?(String)
          params[:states] = JSON.parse(params[:states])
        end
        if params[:unallocated].present? && params[:unallocated].is_a?(String)
          params[:unallocated] = CommonModule::V1::GeneralHelper.string_to_boolean(params[:unallocated])
        end
        params.permit(:from_time, :to_time, :page_no, :per_page, :unallocated, :states => [], :zones => [])
      end

      def pagination_params
        params.permit(:from_time, :to_time, :page_no, :per_page, :state, :sort_by, :order)
      end

      def get_order_id_from_params
        return params.require(:order_id)
      end

      def get_aggregated_products_params
        return params.permit(:from_time,:to_time)
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:order).permit(:event).merge({ id: params[:id] })
      end

      #
      # white list parameters for edit order address request.
      #
      def edit_order_address_params
        params.require(:order).permit(:address => [:id, :nickname, :address_line1,
          :address_line2, :landmark, :recipient_name, :pincode]).merge({ id: params[:id] })
      end

      #
      # white list parameters for edit order time slot request.
      #
      def edit_order_time_slot_params
        params.require(:order).permit(preferred_slot: [:slot_date, time_slot: [:id]]).merge({ id: params[:id] })
      end

      #
      # white list parameters for edit order time slot request.
      #
      def edit_order_cart_params
        if params.present? && params[:order].present? && params[:order][:cart].present?
          # Parse order_products and new_products array from string to JSON (if any)
          if params[:order][:cart][:order_products].present? && params[:order][:cart][:order_products].is_a?(String)
            params[:order][:cart][:order_products] = JSON.parse(params[:order][:cart][:order_products])
          end
          if params[:order][:cart][:new_products].present? && params[:order][:cart][:new_products].is_a?(String)
            params[:order][:cart][:new_products] = JSON.parse(params[:order][:cart][:new_products])
          end
        end
        # WHITELIST releavant attributes
        params.require(:order).permit(cart: [:id, order_products: [:quantity, :additional_discount, product: [:id]],
                      new_products: [:quantity, product: [:id]]]).merge({ id: params[:id] })
      end

      #
      # white list parameters for updating order context
      #
      def update_order_context_params
        if params.present? && params[:order_context].present? && params[:order_context][:box_tags].present?
          if params[:order_context][:box_tags].is_a?(String)
            params[:order_context][:box_tags] = JSON.parse(params[:order_context][:box_tags])
          end
        end
        params.require(:order_context).permit(:order_user_tag, :delivery_boy, :box_tags => []).merge({order_id: params[:id]})
      end

      def reschedule_order_params
        params.require(:order).permit(preferred_slot: [:slot_date, time_slot: [:id]])
      end

      #
      # white list parameters for get dispatch list
      #
      def get_dispatch_list_params
        if params.present? && params[:ids].present? && params[:ids].is_a?(String)
          params[:ids] = JSON.parse(params[:ids])
        end
        params.permit(:csv, :pdf, :ids => [])
      end

    end #End of class
  end
end
