#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    
    class GetAllInventoryOrdersApi < BaseOrdersModule::V1::BaseApi

      INVENTORY_ORDER_SERVICE = InventoryOrdersModule::V1::InventoryOrderService
      INVENTORY_ORDERS_RESPONSE_DECORATOR = InventoryOrdersModule::V1::InventoryOrdersResponseDecorator
      INVENTORY_ORDER_HELPER = InventoryOrdersModule::V1::InventoryOrderHelper
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Return all the orders
      #
      # @return [JsonResponse]
      #
      def enact(request)
        begin
          validate_request(request)
          inventory_order_service = INVENTORY_ORDER_SERVICE.new(@params)
          response = inventory_order_service.get_all_orders_in_interval(request)
          orders_array = INVENTORY_ORDER_HELPER.create_orders_array_with_short_details(response[:inventory_orders])
          return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_inventory_orders_response(orders_array, response[:page_count])
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end

      # 
      # Validate incoming request
      #
      # @param request [JSON] [Hash id of warehouse of which PO are requested]
      #
      # @raise [InsufficientDataError] [if warehouse id is not present]
      #
      def validate_request(request)
        error_array = []
        error_array.push('Warehouse id') if request[:warehouse_id].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end
    end
  end
end
