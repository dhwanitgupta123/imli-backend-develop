#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    
    class UploadInvoiceImageApi < BaseOrdersModule::V1::BaseApi

      INVENTORY_ORDER_SERVICE = InventoryOrdersModule::V1::InventoryOrderService
      INVENTORY_ORDERS_RESPONSE_DECORATOR = InventoryOrdersModule::V1::InventoryOrdersResponseDecorator
      INVENTORY_ORDER_HELPER = InventoryOrdersModule::V1::InventoryOrderHelper
      GENERAL_HELPER = CommonModule::V1::GeneralHelper

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details]
      # Request:  
      # {
      #   inventory_order: {
      #     image: []
      #   }
      # }
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        validate_request(request)
        inventory_order_service = INVENTORY_ORDER_SERVICE.new(@params)
        response = inventory_order_service.upload_purchase_order_invoice_image(request)
        inventory_order_hash = INVENTORY_ORDER_HELPER.get_inventory_order_hash(response)
        return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_inventory_order_details_response(inventory_order_hash)
      end

      # 
      # Validate incoming request
      #
      # @param request [integer] [ID of Purchase order]
      #
      # @raise [InsufficientDataError] [ID is not present]
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Inventory Order ID'})
        end
      end
    end
  end
end
