#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    
    class MailPurchaseOrderApi < BaseOrdersModule::V1::BaseApi

      INVENTORY_ORDER_SERVICE = InventoryOrdersModule::V1::InventoryOrderService
      INVENTORY_ORDERS_RESPONSE_DECORATOR = InventoryOrdersModule::V1::InventoryOrdersResponseDecorator
      INVENTORY_ORDER_HELPER = InventoryOrdersModule::V1::InventoryOrderHelper

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [integer] [ID of Purchase order]
      # 
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        validate_request(request)
        inventory_order_service = INVENTORY_ORDER_SERVICE.new(@params)
        inventory_order_service.mail_purchase_order(request)
        return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_success_response
      end

      # 
      # Validate incoming request
      #
      # @param request [integer] [ID of Purchase order]
      #
      # @raise [InsufficientDataError] [ID is not present]
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Inventory Order ID'})
        end
      end
    end
  end
end