#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    
    class UpdateInventoryOrderApi < BaseOrdersModule::V1::BaseApi

      INVENTORY_ORDER_SERVICE = InventoryOrdersModule::V1::InventoryOrderService
      INVENTORY_ORDERS_RESPONSE_DECORATOR = InventoryOrdersModule::V1::InventoryOrdersResponseDecorator
      INVENTORY_ORDER_HELPER = InventoryOrdersModule::V1::InventoryOrderHelper
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      GENERAL_HELPER = CommonModule::V1::GeneralHelper

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details]
      # Request:  
      # {
      #   inventory_order: {
      #     warehouse: {
      #       id: '2'
      #     },
      #     inventory: {
      #       id: '3'
      #     },
      #     address: {
      #       shipping_address: {
      #         id: '4'
      #       },
      #       billing_address: {
      #         id: '5'
      #       }
      #     },
      #     products: [
      #       {
      #         inventory_brand_pack_id: '6',
      #         quantity_ordered: '7'
      #       },
      #       {
      #         inventory_brand_pack_id: '8',
      #         quantity_ordered: '9'
      #       }
      #     ]
      #   }
      # }
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          request[:is_inter_state] = GENERAL_HELPER.string_to_boolean(request[:is_inter_state])
          inventory_order_service = INVENTORY_ORDER_SERVICE.new(@params)
          response = inventory_order_service.transactional_update_purchase_order(request)
          inventory_order_hash = INVENTORY_ORDER_HELPER.get_inventory_order_hash(response)
          return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_inventory_order_details_response(inventory_order_hash)
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
            return INVENTORY_ORDERS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
        end
      end
    end
  end
end
