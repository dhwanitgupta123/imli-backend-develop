#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    
    class PaymentSuccessfulApi < BaseOrdersModule::V1::BaseApi

      PAYMENT_SERVICE = MarketplaceOrdersModule::V1::PaymentService
      PAYMENTS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::PaymentsResponseDecorator
      PAYMENT_MODES = MarketplaceOrdersModule::V1::PaymentModes
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details]
      # Request:
      #  - payment
      #   {
      #     - order
      #       * id : order id for which payment is to be made
      #     - mode
      #     - delivery_charges
      #     - grand_total
      #     - discount
      #     - billing_total
      #     - payment_mode_savings
      #     - net_total
      #     - total_savings
      #   }
      #
      # @return [Response] [response to be sent to the user]
      #
      def enact(request)
        begin
          validate_request(request)
          payment_service = PAYMENT_SERVICE.new(@params)
          # Need to add delivery cost dynamically in Mapper/Helper/Decorator
          response = payment_service.transactional_payment_successful(request)
          return PAYMENTS_RESPONSE_DECORATOR.create_response_payment_hash(response)

          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return PAYMENTS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
            return PAYMENTS_RESPONSE_DECORATOR.create_response_bad_request(e.message)
          rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
            return PAYMENTS_RESPONSE_DECORATOR.create_not_found_error(e.message)
          # If unable to change state of CART (PreConditionRequired), then its a Internal Server Error
          rescue CUSTOM_ERROR_UTIL::RunTimeError, CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
            return PAYMENTS_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        end
      end

      # 
      # Validate incoming request
      #
      # @param request [JSON] [Hash containing order products array]
      #
      # @raise [InsufficientDataError] [if order products key not present]
      #
      def validate_request(request)
        error_array = []
        error_array.push('Payment id') if request.blank? || request[:id].blank?

        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end
    end
  end
end