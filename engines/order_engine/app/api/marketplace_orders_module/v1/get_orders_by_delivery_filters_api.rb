require 'imli_singleton'
module MarketplaceOrdersModule
  module V1
    class GetOrdersByDeliveryFiltersApi < BaseOrdersModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        order_delivery_service = MarketplaceOrdersModule::V1::OrderDeliveryService.instance(@params)

        response = order_delivery_service.get_orders_by_filters(request)
        orders = response[:elements]
        page_count = response[:page_count]
        order_array = MarketplaceOrdersModule::V1::OrderDeliveryMapper.map_order_by_date_bucket(orders)
        return MarketplaceOrdersModule::V1::OrdersResponseDecorator.create_orders_response(order_array, page_count)
      end
    end
  end
end
