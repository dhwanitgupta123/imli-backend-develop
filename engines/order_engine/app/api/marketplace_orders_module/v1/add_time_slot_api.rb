#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # AddTimeSlot api to add new time slot
    #
    class AddTimeSlotApi < BaseOrdersModule::V1::BaseApi

      TIME_SLOT_SERVICE = MarketplaceOrdersModule::V1::TimeSlotService
      TIME_SLOTS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::TimeSlotsResponseDecorator
      TIME_SLOT_MAPPER = MarketplaceOrdersModule::V1::TimeSlotMapper

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details
      #
      # @return [JSON] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)

        validate_request(request)
        time_slot_service = TIME_SLOT_SERVICE.new(@params)
        time_slot = time_slot_service.add_time_slot(request)
        add_time_slot_response = TIME_SLOT_MAPPER.add_time_slot_response_hash(time_slot)
        return TIME_SLOTS_RESPONSE_DECORATOR.create_add_time_slot_response(add_time_slot_response)
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        error_array = []
        error_array.push('From time') if request[:from_time].blank?
        error_array.push('To time') if request[:to_time].blank?
        error_array.push('Display Label') if request[:label].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end

    end
  end
end
