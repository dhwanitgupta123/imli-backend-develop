#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # Change order state api, it validates the request, call the
    # Order panel service and return the JsonResponse
    #
    class ChangeStateApi < BaseOrdersModule::V1::BaseApi
      ORDER_PANEL_SERVICE = MarketplaceOrdersModule::V1::OrderPanelService
      ORDER_MAPPER = MarketplaceOrdersModule::V1::OrderMapper
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change order state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, Order change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        begin
          validate_request(request)
          order_panel_service = ORDER_PANEL_SERVICE.new(@params)
          order = order_panel_service.transactional_change_state(request)
          #order = ORDER_MAPPER.map_minimal_order_to_hash(order)
          return ORDERS_RESPONSE_DECORATOR.create_single_order_response(order)
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
          rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
            return ORDERS_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
          rescue CUSTOM_ERROR_UTIL::ResourceNotFoundError, CUSTOM_ERROR_UTIL::RecordNotFoundError => e
            return ORDERS_RESPONSE_DECORATOR.create_not_found_error(e.message)
        end
      end

      private

      #
      # Validates the required params state change event
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:event].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
      end
    end
  end
end
