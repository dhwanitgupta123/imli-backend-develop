#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # GetAllTimeSlotsApi api to get all time slots
    #
    class GetAllTimeSlotsApi < BaseOrdersModule::V1::BaseApi

      TIME_SLOT_SERVICE = MarketplaceOrdersModule::V1::TimeSlotService
      TIME_SLOTS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::TimeSlotsResponseDecorator
      TIME_SLOT_MAPPER = MarketplaceOrdersModule::V1::TimeSlotMapper

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details
      #
      # @return [JSON] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)

        time_slot_service = TIME_SLOT_SERVICE.new(@params)
        response = time_slot_service.get_all_time_slots(request)
        get_all_time_slots_response = TIME_SLOT_MAPPER.get_all_time_slots_response_hash(response)
        return TIME_SLOTS_RESPONSE_DECORATOR.create_get_all_time_slots_response(get_all_time_slots_response)
      end

    end # End of class
  end
end
