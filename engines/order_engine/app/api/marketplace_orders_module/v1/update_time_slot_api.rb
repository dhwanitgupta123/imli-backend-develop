#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # AddTimeSlot api to add new time slot
    #
    class UpdateTimeSlotApi < BaseOrdersModule::V1::BaseApi

      TIME_SLOT_SERVICE = MarketplaceOrdersModule::V1::TimeSlotService
      TIME_SLOTS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::TimeSlotsResponseDecorator
      TIME_SLOT_MAPPER = MarketplaceOrdersModule::V1::TimeSlotMapper

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details
      #
      # @return [JSON] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)

        validate_request(request)
        time_slot_service = TIME_SLOT_SERVICE.new(@params)
        response = time_slot_service.update_time_slot(request)
        update_time_slot_response = TIME_SLOT_MAPPER.update_time_slot_response_hash(response)
        return TIME_SLOTS_RESPONSE_DECORATOR.create_update_time_slot_response(update_time_slot_response)
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        # Throw a common error with all above caught errors
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new('Request cannot be blank')
        end

      end

    end
  end
end
