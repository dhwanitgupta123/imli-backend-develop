#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    class UpdateCartApi < BaseOrdersModule::V1::BaseApi

      CART_SERVICE = MarketplaceOrdersModule::V1::CartService
      ORDERS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::OrdersResponseDecorator
      CARTS_RESPONSE_DECORATOR = MarketplaceOrdersModule::V1::CartsResponseDecorator
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params)
        @params = params
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details]
      # Request:
      #  - cart
      #   {
      #     - order_products(Array)
      #       [{
      #         * id: order product id
      #         * quantity: quantity of the order product (Exact quantity in integer)
      #         * product:
      #           {
      #             * id: selected product id (MPSP id)
      #           }
      #       }]
      #   }
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        begin
          validate_request(request)
          carts_service = CART_SERVICE.new(@params)
          checkout = request[:checkout] || false # if checkout not sent, then take default as false
          # Need to add delivery cost dynamically in Mapper/Helper/Decorator
          result = carts_service.transactional_update_cart(request)
          result = result.merge({is_checkout: checkout})
          response =  MarketplaceOrdersModule::V1::UpdateCartResponse.build(result)
          return response
        # Major exceptions will be caught by general ExceptionHandler annotation
        rescue CUSTOM_ERROR_UTIL::UnAuthenticatedUserError => e
          return CARTS_RESPONSE_DECORATOR.create_not_found_error(e.message)
        # If unable to change state of CART (PreConditionRequired), then its a Internal Server Error
        rescue CUSTOM_ERROR_UTIL::RunTimeError, CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return CARTS_RESPONSE_DECORATOR.create_response_runtime_error(e.message)
        end
      end

      #
      # Validate incoming request
      #
      # @param request [JSON] [Hash containing order products array]
      #
      # @raise [InsufficientDataError] [if order products key not present]
      #
      def validate_request(request)
        error_array = []
        error_array.push('Order Products Array') if request[:order_products].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end
    end
  end
end
