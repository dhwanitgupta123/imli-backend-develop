require 'imli_singleton'
#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    class GetDispatchListApi < BaseOrdersModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        validate_request(request)
        order_data_service = MarketplaceOrdersModule::V1::OrderDataService.new(@params)
        orders = order_data_service.get_orders_for_dispatch_list(request[:ids])

        pdf = request[:pdf]
        csv = request[:csv]

        orders_dispatch_list_generator = MarketplaceOrdersModule::V1::OrdersDispatchListGeneratorUtil
        response = orders_dispatch_list_generator.generate_dispatch_list_links({
          orders: orders,
          pdf_needed: request[:pdf],
          csv_needed: request[:csv]
          })

        return @order_response_decorator.create_dispatch_list_response(response)
      end

      #
      # Validate incoming request
      #
      def validate_request(request)
        if request[:ids].blank?
          raise @custom_error_util::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Atleast one order id'})
        end
        # Atleast one should be present : csv=true OR pdf=true
        if request[:csv].blank? && request[:pdf].blank?
          raise @custom_error_util::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'CSV or PDF'})
        end
      end
    end
  end
end
