require 'imli_singleton'
#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    class RescheduleOrderApi < BaseOrdersModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      #
      # @return [Response] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)

        validate_request(request)
        order_service = MarketplaceOrdersModule::V1::OrderService.new(@params)

        response = order_service.reschedule_order(request)

        if response[:slot_available] == true
          return @order_response_decorator.create_rescheduled_order_response(response[:order])
        else
          return @order_response_decorator.create_reschedule_order_with_time_slots_response(response[:order])
        end
      end

      #
      # Validate incoming request
      #
      # @param request [JSON] [Hash containing cart id and address id ]
      #
      # @raise [InsufficientDataError] [if cart id or address id not present]
      #
      def validate_request(request)
        if request[:preferred_slot].blank?
          raise @custom_error_util::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Preferred Slot'})
        end
        preferred_slot = request[:preferred_slot]
        error_array = []
        error_array.push('Slot date') if preferred_slot[:slot_date].blank?
        error_array.push('Time slot') if preferred_slot[:time_slot].blank?
        error_array.push('Time slot details') if preferred_slot[:time_slot].present? && preferred_slot[:time_slot][:id].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise @custom_error_util::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end
    end
  end
end
