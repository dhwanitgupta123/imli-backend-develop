require 'imli_singleton'
#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # GetAllOrders api, it calls the
    # order_panel service and return the JsonResponse
    #
    class ValidateOrderApi < BaseOrdersModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = {})
        @params = params
        super
      end

      #
      # Return success if post order vlidation pass else return error
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)
        validate_request(request)
        post_order_validation_service = MarketplaceOrdersModule::V1::PostOrderValidationService.instance(@params)
        result = post_order_validation_service.apply_post_order_validations(request[:id])
        if result[:is_valid] == true
          return @order_response_decorator.create_short_order_response(result[:order])
        else
          return @order_response_decorator.create_invalid_order_response(result[:order])
        end
      end

      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Order id'})
        end
      end
    end
  end
end
