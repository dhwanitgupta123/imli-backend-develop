require 'imli_singleton'
#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # Change order state api, it validates the request, call the
    # Order panel service and return the JsonResponse
    #
    class GetPaymentDetailsApi < BaseOrdersModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @response_builder = MarketplaceOrdersModule::V1::GetPaymentDetailsResponse
        @params = params
        super
      end

      #
      # Function which takes Order id in request and return its all allowed
      # payment-details
      #
      # @param request [Hash] Request object, Order change state params
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)
        validate_request(request)
        payment_context_service = get_service
        response = payment_context_service.get_allowed_payment_modes_for_order(request)
        @response_builder.build(response)
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CommonModule::V1::CustomErrors::InvalidArgumentsError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'id'})
        end
      end

      #
      # Get appropriate service as per the platform and app-version
      # This is done because old apps support only Razorpay gateway
      # and new apps support many (Razorpay, paytm, etc.)
      #
      def get_service
        platform_types = CommonModule::V1::PlatformType
        if (USER_SESSION[:platform_type] == platform_types.get_id_by_type('ANDROID') &&
          USER_SESSION[:version_number] >= (version_number || 21)) ||
          USER_SESSION[:platform_type] == platform_types.get_id_by_type('WEB')

          return MarketplaceOrdersModule::V2::PaymentContextService.instance(@params)
        else
          return MarketplaceOrdersModule::V1::PaymentContextService.instance(@params)
        end
      end

    end
  end
end
