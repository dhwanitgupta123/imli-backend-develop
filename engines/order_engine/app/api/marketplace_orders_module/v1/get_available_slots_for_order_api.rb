require 'imli_singleton'
#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # GetAllTimeSlotsApi api to get all time slots
    #
    class GetAvailableSlotsForOrderApi < BaseOrdersModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Execute the request and return response
      #
      # @param request [JSON] [Hash containing cart details
      #
      # @return [JSON] [response to be sent to the user]
      #
      _ExceptionHandler_
      def enact(request)
        order_service = MarketplaceOrdersModule::V1::OrderService.new(@params)
        time_slot_service = MarketplaceOrdersModule::V1::TimeSlotService.new(@params)

        order = order_service.get_order_by_order_id(request[:order_id])
        response = time_slot_service.get_slots_inclusive_of_buffer(order)

        return MarketplaceOrdersModule::V1::TimeSlotsResponseDecorator.create_available_slot_response(response)
      end

    end # End of class
  end
end
