require 'imli_singleton'
#
# Module to handle all the functionalities related to Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1
    #
    # Change order state api, it validates the request, call the
    # Order panel service and return the JsonResponse
    #
    class BulkChangeStateApi < BaseOrdersModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Function takes input corresponding to change order state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, Order change state params
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)
        validate_request(request)
        bulk_order_state_change_service = MarketplaceOrdersModule::V1::BulkOrderStateChangeService.instance(@params)
        response = bulk_order_state_change_service.change_state_and_proccess_data(request)

        if !response[:pre_condition].nil? && response[:pre_condition] == false
          return MarketplaceOrdersModule::V1::OrdersResponseDecorator.create_bulk_state_change_pre_check_fail_response(response)
        else
          return MarketplaceOrdersModule::V1::OrdersResponseDecorator.create_bulk_change_state_response(response)
        end
      end

      private

      #
      # Validates the required params state change event
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:order][:event].blank?
          raise CommonModule::V1::CustomErrors::InvalidArgumentsError.new(CommonModule::V1::Content::EVENT_MISSING)
        end
      end
    end
  end
end
