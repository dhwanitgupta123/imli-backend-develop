module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module OrderTimeSlotManipulator
          extend self

          #
          # This function update the orders time_slot and preferred_delivery_date
          #
          def update(args)

            order = args[:order]
            return MarketplaceOrdersModule::V1::OrderDao.update_order({
                                                                 preferred_delivery_date: args[:preferred_date],
                                                                 time_slot: args[:time_slot]
                                                               }, order)

          end

          #
          # This function delete time_slot and preferred_delivery_date from order
          #
          def delete(args)

            update({
                    order: args[:order],
                    time_slot: nil,
                    preferred_date: nil
                   })
          end

          #
          # This function returns number of orders allocated in time_slot and preferred_date
          #
          def get_allocated_slots(time_slot, preferred_date)
            MarketplaceOrdersModule::V1::TimeSlotDateBucketDao.allocated_time_slots(time_slot, preferred_date)
          end

          def get_total_count_of_assigned_slots(time_slot, preferred_date)
            MarketplaceOrdersModule::V1::TimeSlotDateBucketDao.get_total_count_of_assigned_slots(time_slot, preferred_date)
          end
        end
      end
    end
  end
end
