module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          module OverallSlotLimitValidator
            extend self
            #
            # This function check if time_slot in preferred_date is available or not
            #
            # @params args [Hash] containing time_slot and preferred_date
            #
            def validate(args)
              time_slot = args[:time_slot]

              overall_allocated_slots = get_freezed_buffered_and_allocated_time_slots(time_slot, args[:preferred_date])

              return true if overall_allocated_slots < ( time_slot.order_limit || 0) + ( time_slot.buffer_limit || 0)

              return false
            end

            private

            #
            # This function returns the sum of orders which has time_slot = time_slot AND preferred_delivery_date =
            # preferred_date + Count of orders which has freeze the time_slot in preferred_date +
            # orders which has time_slot = time_slot AND preferred_delivery_date =
            # preferred_date in buffered bucket of time_slot
            #
            def get_freezed_buffered_and_allocated_time_slots(time_slot, preferred_date)

              freezed_slots = MarketplaceOrdersModule::V1::TimeSlots::V1::CacheTimeSlotManipulator.
                get_freezed_time_slots_for_preferred_date(time_slot, preferred_date)

              total_assigned_slots = MarketplaceOrdersModule::V1::TimeSlots::V1::OrderTimeSlotManipulator.
                get_total_count_of_assigned_slots(time_slot, preferred_date)


              return ( total_assigned_slots  || 0 ) + ( freezed_slots  || 0 )
            end
          end
        end
      end
    end
  end
end
