module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          module TimeSlotDateValidator
            extend self

            #
            # This function check if preferred_date present in time_slot unavailable dates
            # if present then return false else return true
            #
            # @params args [Hash] containing time_slot and preferred_date
            #
            def validate(args)

              time_slot = args[:time_slot]
              preferred_date = args[:preferred_date]

              return true if preferred_date.blank? || time_slot.blank?

              unavailable_dates = time_slot.unavailable_dates

              return false if unavailable_dates.present? && unavailable_dates.include?(preferred_date)

              return true
            end
          end
        end
      end
    end
  end
end
