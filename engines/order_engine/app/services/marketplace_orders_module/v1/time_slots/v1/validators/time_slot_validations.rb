module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          module TimeSlotValidations
            extend self

            #
            # Apply all the validations with AND condition
            #
            def apply(args, validators)

              validators.each do |validator|
                return false if validator.validate(args) == false
              end

              return true
            end
          end
        end
      end
    end
  end
end
