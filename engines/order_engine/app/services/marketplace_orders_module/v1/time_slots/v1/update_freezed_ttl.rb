module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module UpdateFreezedTtl
          extend self

          #
          # This function apply pre_validation before updating freezing ttl and if validations passes
          # it update the ttl else free slot
          #
          # @params args [Hash] it contains order, time_slot and preferred_date
          #
          #
          def update_freezed_ttl(args)
            slot_already_freezed_validator = MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotAlreadyFreezedValidator

            if slot_already_freezed_validator.validate(args) == true
              apply_validation_for_already_freezed_slot(args)
              MarketplaceOrdersModule::V1::TimeSlots::V1::CacheTimeSlotManipulator.update_freezed_ttl(args)
            else
              apply_validation_for_nonfreezed_slot(args)
              MarketplaceOrdersModule::V1::TimeSlots::V1::CacheTimeSlotManipulator.freeze(args)
            end
          end

          private

          def apply_validation_for_already_freezed_slot(args)
            validators = get_slot_availability_validators | [MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::AllocatedTimeSlotLimitValidator]
            validate(args, validators)
          end

          def apply_validation_for_nonfreezed_slot(args)
            validators = get_slot_availability_validators | [MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TotalSlotLimitValidator]
            validate(args, validators)
          end

          def validate(args, validators)

            is_valid = MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotValidations.apply(args, validators)

            if is_valid == false
              TimeSlotsModule::V1::ValidationsUtil.clear_time_slot_data(args)
              raise CommonModule::V1::CustomErrors::SlotUnavailable
            end
          end

          def get_slot_availability_validators
            return TimeSlotsModule::V1::ValidationsUtil.get_slot_availability_validators
          end

        end
      end
    end
  end
end
