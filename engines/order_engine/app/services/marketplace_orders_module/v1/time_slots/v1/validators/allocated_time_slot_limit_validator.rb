module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          #
          # This function validate order_count limit corresponding to time_slot and preferred_data
          # and return true if count < limit else false
          #
          # @params args [Hash] containing time_slot and preferred_date
          #
          module AllocatedTimeSlotLimitValidator
            extend self

            def validate(args)
              time_slot = args[:time_slot]
              allocated_time_slots = MarketplaceOrdersModule::V1::TimeSlotDateBucketDao.
                                                                allocated_time_slots(time_slot, args[:preferred_date])

              return true if allocated_time_slots < time_slot.order_limit

              return false
            end
          end
        end
      end
    end
  end
end
