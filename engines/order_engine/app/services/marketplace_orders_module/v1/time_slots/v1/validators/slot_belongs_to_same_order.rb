module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          module SlotBelongsToSameOrder
            extend self

            #
            # This function validate if input time_slot and preferred_date belongs to order or not
            #
            # @params args [Hash] containing order, time_slot and preferred_date
            #
            def validate(args)
              order = args[:order]
              assigned_slot = order.time_slot
              assigned_preferred_delivery_date = order.preferred_delivery_date

              return false if assigned_slot.blank?

              return true if args[:time_slot] == assigned_slot && assigned_preferred_delivery_date == args[:preferred_date]

              return false
            end
          end
        end
      end
    end
  end
end
