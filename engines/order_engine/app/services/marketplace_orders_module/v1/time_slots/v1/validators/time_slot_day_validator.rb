module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          module TimeSlotDayValidator
            extend self

            #
            # This function check if preferred_date day present in time_slot unavailable days
            # if present then return false else return true
            #
            # @params args [Hash] containing time_slot and preferred_date
            #
            def validate(args)

              time_slot = args[:time_slot]
              preferred_date = args[:preferred_date]

              return true if preferred_date.blank? || time_slot.blank?

              day_name = preferred_date.strftime('%A')
              day_key = DateTimeModule::V1::DayEnums.get_day_by_value(day_name)

              return false if time_slot.unavailable_days.present? && time_slot.unavailable_days.include?(day_key)

              return true
            end
          end
        end
      end
    end
  end
end
