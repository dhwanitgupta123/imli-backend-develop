module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          module SlotAlreadyFreezedValidator
            extend self

            #
            # Check if time_slot for order at particular delivery date is already present
            #
            # @param args [Hash] consist of time_slot ->  time_slot model
            # slot_date ->  time slot date and order -> order model
            #
            # @return [Boolean] true if time slot already present for order
            #
            def validate(args)
              return true if CacheModule::V1::TimeSlotCacheClient.slot_for_order_exists?(args)
              return false
            end
          end
        end
      end
    end
  end
end
