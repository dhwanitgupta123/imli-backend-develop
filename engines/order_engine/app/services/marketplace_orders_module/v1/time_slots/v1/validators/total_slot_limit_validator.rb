module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          module TotalSlotLimitValidator
            extend self

            #
            # This function check if time_slot in preferred_date is available or not
            #
            # @params args [Hash] containing time_slot and preferred_date
            #
            def validate(args)
              time_slot = args[:time_slot]

              freezed_and_allocated_time_slots = get_freezed_and_allocated_time_slots(time_slot, args[:preferred_date])

              return true if freezed_and_allocated_time_slots < time_slot.order_limit

              return false
            end

            private

            def get_freezed_and_allocated_time_slots(time_slot, preferred_date)

              freezed_slots = MarketplaceOrdersModule::V1::TimeSlots::V1::CacheTimeSlotManipulator.
                                                get_freezed_time_slots_for_preferred_date(time_slot, preferred_date)

              allocated_slots = MarketplaceOrdersModule::V1::TimeSlots::V1::OrderTimeSlotManipulator.
                                                get_allocated_slots(time_slot, preferred_date)

              return allocated_slots + freezed_slots
            end
          end
        end
      end
    end
  end
end
