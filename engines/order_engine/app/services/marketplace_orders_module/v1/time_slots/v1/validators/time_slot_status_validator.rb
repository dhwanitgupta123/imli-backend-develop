module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          module TimeSlotStatusValidator
            extend self

            #
            # This function check if time_slot is active or not
            #
            # @params args [Hash] containing time_slot
            #
            def validate(args)

              return args[:time_slot].active?
            end
          end
        end
      end
    end
  end
end
