module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module AllocateTimeSlot
          extend self

          #
          # This function apply pre_validation before allocating time slot to order
          #
          # @params args [Hash] it contains order, time_slot and preferred_date
          #
          def allocate(args)
            apply_validations(args)
            MarketplaceOrdersModule::V1::TimeSlotDateBucketDao.update_if_available(args)
            return true
          end

          #
          # This function apply pre_validation before allocating time slot
          # and it update if buffer limit or normal order limit is available
          #
          # @params args [Hash] it contains order, time_slot and preferred_date
          #
          def allocate_considering_buffers(args)
            apply_validations_for_reschedule_delivery(args)
            MarketplaceOrdersModule::V1::TimeSlotDateBucketDao.update_if_buffer_or_allocated_slot_available(args)
            return true
          end

          private

          def apply_validations_for_reschedule_delivery(args)
            validators = get_reschedule_slot_availability_validators
            validate(args, validators)
          end

          def apply_validations(args)
            validators = get_slot_availability_validators
            validate(args, validators)
          end

          def validate(args, validators)

            is_valid = MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotValidations.apply(args, validators)

            raise CommonModule::V1::CustomErrors::SlotUnavailable if is_valid == false

            return true
          end

          def get_slot_availability_validators
            return TimeSlotsModule::V1::ValidationsUtil.get_slot_availability_validators
          end

          def get_reschedule_slot_availability_validators
            return TimeSlotsModule::V1::ValidationsUtil.get_reschedule_slot_availability_validators
          end

        end
      end
    end
  end
end
