module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module CacheTimeSlotManipulator
          extend self

          #
          # This function delete the time_slot key for order from cache
          #
          # @param args [Hash] consist of time_slot ->  time_slot model
          # slot_date ->  time slot date
          #
          def delete(args)
            CacheModule::V1::TimeSlotCacheClient.remove_time_slot(args)
          end

          #
          # This function add the key in cache with ttl
          #
          # @param args [Hash] consist of time_slot ->  time_slot model, ttl -> time to live for key
          # slot_date ->  time slot date and order -> order model
          #
          # @return [Boolean] true if time slot already present for order
          #
          def freeze(args)
            if CacheModule::V1::TimeSlotCacheClient.get_freezed_time_slot(args).blank?
              CacheModule::V1::TimeSlotCacheClient.freeze_time_slot_for_order(args)
            end
          end

          #
          # This function return number of order in a particular time_slot and preferred_date
          #
          # @params time_slot [Model] time slot model
          # @params preferred_date [Date] scheduled date
          #
          def get_freezed_time_slots_for_preferred_date(time_slot, preferred_date)
            CacheModule::V1::TimeSlotCacheClient.get_freezed_time_slots(time_slot, preferred_date)
          end

          #
          # This function updates the freezed ttl if that is less than freezing ttl
          # used by payment_initiate to update it
          #
          # @param args [Hash] consist of time_slot ->  time_slot model
          # slot_date ->  time slot date
          #
          def update_freezed_ttl(args)

            freezing_ttl = CommonModule::V1::Cache.read_int('FREEZING_TTL') || 5
            freezing_ttl_in_seconds = freezing_ttl.minutes.to_i

            ttl = CacheModule::V1::TimeSlotCacheClient.get_time_slot_ttl(args)
            if ttl < freezing_ttl_in_seconds
              CacheModule::V1::TimeSlotCacheClient.freeze_time_slot_for_order(args)
            end
          end
        end
      end
    end
  end
end
