module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          module PreferredDateValidator
            extend self

            #
            # This function check if preferred_date is between start and final date
            #
            # @params args [Hash] containing time_slot and preferred_date
            #
            def validate(args)
              time_slot = args[:time_slot]
              preferred_date = args[:preferred_date]

              dates = TimeSlotsModule::V1::ValidationsUtil.
                                get_start_and_final_dates(time_slot.min_slot_offset, time_slot.max_slot_offset)

              from_date_time = CommonModule::V1::DateTimeUtil.concat_date_and_time(time_slot.from_time, preferred_date)
              to_date_time = CommonModule::V1::DateTimeUtil.concat_date_and_time(time_slot.to_time, preferred_date)

              return false if from_date_time < dates[:start_available_time] || to_date_time > dates[:final_available_time]

              return true
            end
          end
        end
      end
    end
  end
end
