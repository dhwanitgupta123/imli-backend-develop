module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module FreezeSlot
          extend self

          #
          # This function apply pre_validation before freezing time slots and if validations passes
          # it freeze the time slot
          #
          # @params args [Hash] it contains order, time_slot and preferred_date
          #
          #
          def freeze(args)
            slot_already_freezed_validator = MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotAlreadyFreezedValidator

            if slot_already_freezed_validator.validate(args) == true
              apply_validation_for_already_freezed_slot(args)
            else
              apply_validation_for_nonfreezed_slot(args)
            end

            MarketplaceOrdersModule::V1::TimeSlots::V1::UpdateTimeSlotData.update(args)
          end

          private

          def apply_validation_for_already_freezed_slot(args)
            validators = get_slot_availability_validators | [MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::AllocatedTimeSlotLimitValidator]
            validate(args, validators)

            if MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotBelongsToSameOrder.validate(args) == true
              raise CommonModule::V1::CustomErrors::SlotBelongsToSameOrder
            end
          end

          def apply_validation_for_nonfreezed_slot(args)
            validators = get_slot_availability_validators | [MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TotalSlotLimitValidator]
            validate(args, validators)
          end

          def validate(args, validators)

            is_valid = MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotValidations.apply(args, validators)

            raise CommonModule::V1::CustomErrors::SlotUnavailable if is_valid == false
          end

          def get_slot_availability_validators
            return TimeSlotsModule::V1::ValidationsUtil.get_slot_availability_validators
          end

        end
      end
    end
  end
end
