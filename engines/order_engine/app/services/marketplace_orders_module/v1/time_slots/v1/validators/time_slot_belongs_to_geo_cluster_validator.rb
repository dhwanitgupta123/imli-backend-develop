module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module Validators
          module TimeSlotBelongsToGeoClusterValidator
            extend self

            #
            # This function check if time_slot belongs to same geographical cluster in which order.address.area
            # belongs
            #
            # @params args [Hash] containing time_slot and order
            #
            def validate(args)
              order = args[:order]
              time_slot = args[:time_slot]

              active_slots = MarketplaceOrdersModule::V1::TimeSlotDao.get_active_slots_by_address(order.address)

              return  active_slots.include?(time_slot)
            end
          end
        end
      end
    end
  end
end
