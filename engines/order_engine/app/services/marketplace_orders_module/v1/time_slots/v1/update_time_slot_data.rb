module MarketplaceOrdersModule
  module V1
    module TimeSlots
      module V1
        module UpdateTimeSlotData
          extend self

          FREEZING_TTL = CommonModule::V1::Cache.read_int('FREEZING_TTL') || 5
          FREEZING_TTL_IN_SECONDS = FREEZING_TTL.minutes.to_i

          #
          # This function remove time_slot_data corresponding to assigned slot and
          #
          # @params args [Hash] it contains order, time_slot and preferred_date
          #
          #
          def update(args)

            if args[:order].time_slot.present?
              order = args[:order]
              MarketplaceOrdersModule::V1::TimeSlots::V1::CacheTimeSlotManipulator.delete({
                                                                            order: order,
                                                                            preferred_date: order.preferred_delivery_date,
                                                                            time_slot: order.time_slot
                                                                                          })
            end

            order = MarketplaceOrdersModule::V1::TimeSlots::V1::OrderTimeSlotManipulator.update(args)

            MarketplaceOrdersModule::V1::TimeSlots::V1::CacheTimeSlotManipulator.
                                                              freeze(args.merge(ttl: FREEZING_TTL_IN_SECONDS))


            return order
          end


        end
      end
    end
  end
end
