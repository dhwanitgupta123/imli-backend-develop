require 'imli_singleton'
module MarketplaceOrdersModule

  module V1

    class ThirdPartyOrderService < BaseOrdersModule::V1::BaseService
      include ImliSingleton

      #
      # Initialize CART service with passed params
      #
      def initialize(params = {})
        @params = params
        @third_party_order_dao = MarketplaceOrdersModule::V1::ThirdPartyOrderDao.instance(@params)
        @custom_error_util = CommonModule::V1::CustomErrors
      end

      #
      # Link third party orders with the given order id
      #
      # @param order_id [Integer] [id of the order which is to be linked to]
      # @param third_party_orders_array [Array] [Array of ThirdPartyOrder class objects]
      #
      # @return [Boolean] [true after linking is done successfully]
      #
      def link_third_party_orders(order, third_party_orders_array)
        return true if third_party_orders_array.blank?
        user = order.user
        order_id = order.id
        third_party_orders_array.each do |third_party_order|
          if !third_party_order.valid?(user)
            label = third_party_order.display_label
            raise @custom_error_util::InvalidArgumentsError.new(CONTENT_UTIL::UNABLE_TO_ATTACH % {label: label})
          end
          third_party_order.reference_key
          args = third_party_order.get_hash
          args = args.merge({order_id: order_id})
          # Discard any old pending linking order and create new pending one
          @third_party_order_dao.discard_any_pending_order_by_type(order_id, third_party_order.reference_key)
          @third_party_order_dao.create(args)
        end
        return true
      end

      def discard_all_pending_third_party_orders(order)
        return true if order.blank?
        orders = @third_party_order_dao.get_pending_orders_by_order_id(order.id)
        orders.each do |pending_order|
          @third_party_order_dao.change_state_to_discarded(pending_order)
        end
      end

      #
      # Get pending third party orders with respect to the ample order id
      #
      # @param order_id [Integer] [id of the ample order]
      #
      # @return [Array] [Array of third_party_order model objects]
      #
      def get_any_pending_orders(order_id)
        @third_party_order_dao.get_pending_orders_by_order_id(order_id)
      end

      #
      # Get placed and pending third party orders with respect to the ample order id
      #
      # @param order_id [Integer] [id of the ample order]
      #
      # @return [Array] [Array of third_party_order model objects]
      #
      def get_placed_or_pending_orders(order_id)
        @third_party_order_dao.get_placed_or_pending_orders_by_order_id(order_id)
      end

      #
      # Compute total amount of the third party orders
      #
      # @param third_party_orders [Array] [Array of third_party_order model objects]
      #
      # @return [BigDecimal] [Total amount of all the third party orders]
      #
      def compute_total_amount_for_orders(third_party_orders)
        amount = BigDecimal.new('0')
        return amount if third_party_orders.blank?
        third_party_orders.each do |order|
          amount += order.amount
        end
        return amount
      end

      #
      # Place third party orders and pass them options hash
      #
      # @param third_party_orders [Array] [Array of third party order objects]
      # @param options [Hash] [Hash containing appropriate attributes to be sent to third party services]
      #
      # options hash contains:
      #  * order
      #  * user
      #  * payment
      #
      def place_third_party_orders(third_party_orders, options)
        return true if third_party_orders.blank?
        aggregator = MarketplaceOrdersModule::V1::ThirdPartyOrderAggregator
        third_party_orders.each do |order|
          third_party_order_instance = aggregator.get_third_party_order_instance(order)
          if third_party_order_instance.present?
            third_party_order_instance.notify_placed_order(order, options)
          end
          # DO NOT send 3p_orders to placed state now. Send them after payment succeeds
          # @third_party_order_dao.change_state_to_placed(order)
        end
      end

      #
      # Handle payment success third party orders and pass them options hash
      #
      # @param third_party_orders [Array] [Array of third party order objects]
      # @param options [Hash] [Hash containing appropriate attributes to be sent to third party services]
      #
      # options hash contains:
      #  * order
      #  * user
      #  * payment
      #
      def handle_payment_success_for_third_party_orders(third_party_orders, options)
        return true if third_party_orders.blank?
        aggregator = MarketplaceOrdersModule::V1::ThirdPartyOrderAggregator
        third_party_orders.each do |order|
          third_party_order_instance = aggregator.get_third_party_order_instance(order)
          if third_party_order_instance.present?
            third_party_order_instance.notify_payment_success(order, options)
          end
          # After payment is done, then only send 3P_orders to PLACED state
          @third_party_order_dao.change_state_to_placed(order)
        end
      end

      #
      # Cancel third party orders and pass them options hash
      #
      # @param third_party_orders [Array] [Array of third party order objects]
      # @param options [Hash] [Hash containing appropriate attributes to be sent to third party services]
      #
      # options hash contains:
      #  * order
      #  * user
      #  * payment
      #  * payment_done: flag on basis of which refund needs to initiate
      #
      def cancel_third_party_orders(third_party_orders, options)
        return true if third_party_orders.blank?
        aggregator = MarketplaceOrdersModule::V1::ThirdPartyOrderAggregator
        third_party_orders.each do |order|
          third_party_order_instance = aggregator.get_third_party_order_instance(order)
          if third_party_order_instance.present?
            third_party_order_instance.notify_cancel_order(order, options)
          end
          @third_party_order_dao.change_state_to_cancelled(order)
        end
      end

      def fetch_class_as_per_reference_type(reference_type)
        MarketplaceOrdersModule::V1::ThirdPartyOrderReferenceMap.get_reference_by_id(reference_type)
      end

      def fetch_reference_id_by_display_type(type)
        MarketplaceOrdersModule::V1::ThirdPartyOrderReferenceMap.get_id_by_type(type)
      end

    end
  end
end
