module MarketplaceOrdersModule
  module V1
    module Tasks
      module Orders
        module GenerateDispatchOrderList
          extend self

          def apply(args)
            return MarketplaceOrdersModule::V1::OrdersDispatchListGeneratorUtil.generate_dispatch_list_links(args)
          end
        end
      end
    end
  end
end
