module MarketplaceOrdersModule

  module V1
    module OrderValidationModule
      module V1
        module TimeSlotValidator
          extend self

          #
          # This function simply return false if time_slot is blank else return true
          #
          def apply_validation(order)
            return false if order.time_slot.blank?
            return true
          end
        end
      end
    end
  end
end
