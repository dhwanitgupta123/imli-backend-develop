#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
  	#
  	# Orders Panel service class
  	#
  	class OrderPanelService < BaseOrdersModule::V1::BaseService

      ORDER_SERVICE = MarketplaceOrdersModule::V1::OrderService
      ORDER_PRODUCTS_SERVICE = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService
      PAYMENT_COMPUTATION_SERVICE = MarketplaceOrdersModule::V1::PaymentComputationService
      PAYMENT_SERVICE = MarketplaceOrdersModule::V1::PaymentService
      USER_SERVICE = UsersModule::V1::UserService
  		ORDER_DAO = MarketplaceOrdersModule::V1::OrderDao
      CART_SERVICE = MarketplaceOrdersModule::V1::CartService
      CART_DAO = MarketplaceOrdersModule::V1::CartDao
      PAYMENT_DAO = MarketplaceOrdersModule::V1::PaymentDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper
      ORDER_MAPPER = MarketplaceOrdersModule::V1::OrderMapper
      EMAIL_TYPE = CommunicationsModule::V1::Mailers::V1::EmailType
      AGGREGATED_PRODUCTS_INVOICE_HELPER = MarketplaceOrdersModule::V1::AggregatedProductsInvoiceHelper
      CACHE_UTIL = CommonModule::V1::Cache
      ORDER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::OrderEmailWorker
      ORDER_NOTIFICATION = MarketplaceOrdersModule::V1::OrderNotificationService
      MEMBERSHIP_SERVICE = UsersModule::V1::MembershipService
      ADDRESS_SERVICE = AddressModule::V1::AddressService
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      ORDER_STATE_HELPER = MarketplaceOrdersModule::V1::OrderStateTransitionHelper
      TEMPLATE_TYPES = CommonModule::V1::TemplateTypes
      INVOICE_HELPER = MarketplaceOrdersModule::V1::InvoiceHelper
      ORDER_DATA_SERVICE = MarketplaceOrdersModule::V1::OrderDataService
      FILE_SERVICE_HELPER = FileServiceModule::V1::FileServiceHelper
      PAYMENT_MODES = MarketplaceOrdersModule::V1::PaymentModes
      ORDER_STOCKING_HELPER = MarketplaceOrdersModule::V1::OrderStockingHelper
      BENEFIT_SERVICE = CommonModule::V1::BenefitService
      TRIGGER_ENUM_UTIL = TriggersModule::V1::TriggerEnum
      AMPLE_CREDIT_SERVICE = CreditsModule::V1::AmpleCreditService
      PAYMENT_TYPE = MarketplaceOrdersModule::V1::PaymentType

      #
      # initialize function to initialize params on class creation]
      #
  		def initialize(params='')
        super
        @params = params
        @order_dao = ORDER_DAO.new(params)
      end

      #
      # get_all_orders function
      # @param pagination_params [Hash] pagination params can be passed, else will be treated blank
      #
      # @return [Array] returns all orders
      #
      def get_all_orders(pagination_params = {})
        get_all_elements(@order_dao, pagination_params)
      end

      #
      # get_all_orders function
      # Timeline:
      #   - to_time or current time
      #   - from_time or (till_time - 1)
      #
      # @param pagination_params [Hash] pagination params can be passed, else will be treated blank
      #
      # @return [Array] returns all orders
      #
      def get_all_orders_in_interval(request_params = {})
        default_to_time = Time.zone.now
        # Initialize and validate time params
        time_params = initialize_order_time_filters(
          {
            from_time: request_params[:from_time],
            to_time: request_params[:to_time]
          },
          default_to_time)
        response = get_all_elements_between_interval(@order_dao, request_params, time_params)
        return response
      end

      #
      # get_packing_details_by_order_id function
      # @param order_id [String] order_id for which packing details are needed
      #
      def get_packing_details_by_order_id(order_id)
        order = @order_dao.get_by_field('order_id', order_id)
        begin
          order_data_service = ORDER_DATA_SERVICE.new(@params)

          aggregated_products = order_data_service.compute_aggregated_products_by_order(order)

          # Building PDF
          relative_template_path = TEMPLATE_TYPES::SINGLE_ORDER_PACKING_DETAILS[:relative_location]
          pdf_file_name = 'Packer_mail_' + order.order_id

          pdf_file_params = {
              products: aggregated_products,
              order_id: order.order_id
              }
          pdf_generator = PdfGenerator.new()
          pdf_details = pdf_generator.generate_pdf(pdf_file_name, pdf_file_params, relative_template_path)

          # Upload to S3 and get signed URL
          signed_pdf_expiry_duration = 5.minutes
          signed_packer_pdf_link = FILE_SERVICE_HELPER.upload_file_to_s3_and_get_signed_url(
            pdf_details[:file_path], pdf_details[:filename], signed_pdf_expiry_duration)
          File.delete(pdf_details[:file_path]);

          return {order_packing_details: signed_packer_pdf_link}
        rescue => e
            ApplicationLogger.debug('Failed to generate packer list for order_id : ' + order.order_id + ' , ' + e.message)
        end
      end

      #
      # get_invoice_by_order_id function
      # @param order_id [String] order_id for which invoice is needed
      #
      def get_invoice_by_order_id(order_id)
        order = @order_dao.get_by_field('order_id', order_id)
        begin
          relative_template_path = TEMPLATE_TYPES::ORDER_INVOICE[:relative_location]
          pdf_file_name = CACHE_UTIL.read('INVOICE_ATTACHMENT_NAME') % {order_id: order.order_id}
          pdf_file_params = INVOICE_HELPER.get_invoice_hash_for_invoice_generation(order)

          assets = {
            'AMPLE_BW_LOGO' => CACHE_UTIL.read('AMPLE_BW_LOGO'),
            'BUYAMPLE_HOMEPAGE' => CACHE_UTIL.read('BUYAMPLE_HOMEPAGE'),
            'SELLER_NAME' => CACHE_UTIL.read('SELLER_NAME'),
            'SELLER_ADDRESS_LINE_1' => CACHE_UTIL.read('SELLER_ADDRESS_LINE_1'),
            'SELLER_ADDRESS_LINE_2' => CACHE_UTIL.read('SELLER_ADDRESS_LINE_2'),
            'TIN_VAT_NUMBER' => CACHE_UTIL.read('TIN_VAT_NUMBER'),
            'CONVENIENCE_FEES_LABEL' => CACHE_UTIL.read('CONVENIENCE_FEES_LABEL'),
            'CONVENIENCE_FEES_SHORT_DESCRIPTION' => CACHE_UTIL.read('CONVENIENCE_FEES_SHORT_DESCRIPTION')
          }

          pdf_file_params = pdf_file_params.merge(assets)
          pdf_generator = PdfGenerator.new()
          pdf_details = pdf_generator.generate_pdf(pdf_file_name, pdf_file_params, relative_template_path)

          # Upload to S3 and get signed URL
          signed_pdf_expiry_duration = 5.minutes
          signed_invoice_pdf_link = FILE_SERVICE_HELPER.upload_file_to_s3_and_get_signed_url(
            pdf_details[:file_path], pdf_details[:filename], signed_pdf_expiry_duration)
          File.delete(pdf_details[:file_path]);

          return {invoice_link: signed_invoice_pdf_link}
        rescue => e
            ApplicationLogger.debug('Failed to generate invoice for order_id : ' + order.order_id + ' , ' + e.message)
        end
      end

      #
      # get_order_by_order_id function
      # @param field_value [String] field_value to be checked
      #
      def get_order_by_order_id(order_id)
        #TODO: This function can be extended to a generalized field typ too.
        order = @order_dao.get_order_by_order_id(order_id)
        return order
        #validate_if_exists(field_value, @order_dao, 'Order')
      end

      #
      # get_aggregated_products function is API function
      # @param params = {} [Hash] for future extendibility of this function to pass
      #   from_time and till_time parameters
      #
      # @return [Array] aggregated products
      #
      def get_aggregated_products(params = {})
        time_params = initialize_and_validate_time(params)

        from_time = params[:from_time]
        till_time = params[:till_time]
        response = get_aggregated_products_in_interval({
          from_time: from_time,
          till_time: till_time
          })
      end

      def get_aggregated_products_with_persisted_order(params)
        time_params = initialize_and_validate_time(params)

        from_time = time_params[:from_time]
        till_time = time_params[:till_time]

        last_aggregated_order_id = CacheModule::V1::OrderAggregationCacheClient.get_last_aggregated_order()
        unless last_aggregated_order_id.blank?
          last_aggregated_order = @order_dao.get_order_by_order_id(last_aggregated_order_id)
          from_time = last_aggregated_order.order_date + 1.second
          ApplicationLogger.debug('Using last persisted order date: ' + from_time.to_s)
        end

        response = get_aggregated_products_in_interval({
          from_time: from_time,
          till_time: till_time
          })
      end

      #
      # get_aggregated_products_in_interval function is a
      #  logic function for get_aggregated_products API
      #
      # @param params = {} [Hash] for future extendibility of this function to pass
      #   from_time and till_time parameters
      #
      # @return [Array] aggregated products by quantity
      #
      def get_aggregated_products_in_interval(params = {})
        order_products_service = ORDER_PRODUCTS_SERVICE.new(@params)

        from_time = params[:from_time]
        till_time = params[:till_time]

        return [] if (from_time.blank? || till_time.blank?)

        # fetched orders in PLACED state in the given Time interval
        placed_orders_in_interval = @order_dao.get_placed_orders_in_interval({
          from_time: from_time,
          till_time: till_time
          })

        unless placed_orders_in_interval.blank?
          # Persisting last order aggregated
          last_aggregated_order = placed_orders_in_interval.last
          CacheModule::V1::OrderAggregationCacheClient.set_last_aggregated_order(last_aggregated_order.order_id)
          ApplicationLogger.debug('Persisted last order in Cache:' + last_aggregated_order.order_id.to_s)
        end

        # all corresponding placed orders minimal details
        placed_orders_minimal_details = ORDER_MAPPER.map_orders_to_array(placed_orders_in_interval)

        # all corresponding carts are fetched associated to placed orders
        carts_array = ORDER_HELPER.get_carts_array_from_orders(placed_orders_in_interval)

        # all active order products in fetched carts are fetched
        active_order_products = order_products_service.get_active_order_products_in_carts(carts_array)

        # groups all order products fetched by marketplace_selling_pack and
        # adds up their quantities
        aggregated_order_products = active_order_products.group(:marketplace_selling_pack).sum(:quantity)

        # aggregated brand packs are fetched from the aggregated order products (i.e. MPSPs)
        aggregated_brand_packs_array =
          ORDER_MAPPER.map_aggregated_products_to_array(aggregated_order_products)

        # duplicated brand packs are removed and added for single instanced Brand Pack key
        # and serial numbers are generated
        aggregated_brand_packs_by_quantity =
          AGGREGATED_PRODUCTS_INVOICE_HELPER.map_aggregated_products_invoice_by_quantity_to_array(aggregated_brand_packs_array)

         # get product availibility details
        aggregated_brand_packs_with_stock = AGGREGATED_PRODUCTS_INVOICE_HELPER.get_products_stock_availability(aggregated_brand_packs_by_quantity)

        # group the brand packs by category
        aggregated_brand_packs_by_category = AGGREGATED_PRODUCTS_INVOICE_HELPER.group_brand_packs_to_category(aggregated_brand_packs_with_stock)

        # used to send aggregated orders email with required params passed
        send_aggregated_orders_mail({
          from_time: from_time,
          till_time: till_time,
          placed_orders_minimal_details: placed_orders_minimal_details,
          aggregated_products: aggregated_brand_packs_by_category })

        return aggregated_brand_packs_by_quantity
      end

      #
      # send_aggregated_orders_mail function
      # - Sends email with the required params
      #
      # @param params [Hash]
      # - [String] from_time
      # - [String] till_time
      # - [Array] aggregated_products
      # - [Array] placed_orders_minimal_details
      #
      def send_aggregated_orders_mail(params)
        begin
          from_time = params[:from_time]
          till_time = params[:till_time]
          aggregated_products = params[:aggregated_products]
          placed_orders_minimal_details = params[:placed_orders_minimal_details]

          email_ids = CACHE_UTIL.read('IMLI_TEAM_MAIL_IDS')
          to_name = CACHE_UTIL.read('IMLI_TEAM_NAME')

          formatted_from_time = from_time.strftime("%a, %d %b '%y (%I:%M%P)") # Prints in format: Sun, 17 Nov '91 (06:15am)
          formatted_till_time = till_time.strftime("%a, %d %b '%y (%I:%M%P)") # Prints in format: Sun, 17 Nov '91 (06:15am)

          email_subject = CACHE_UTIL.read('AGGREGATED_PRODUCTS_LIST_SUBJECT') + '[' + formatted_from_time.to_s + ' to ' + formatted_till_time.to_s + ']'

          # pdf needed only if the products are present
          pdf_needed = (aggregated_products.blank?) ? false : true

          mail_attributes = {
            placed_orders_minimal_details: placed_orders_minimal_details,
            aggregated_products: aggregated_products,
            from_time: formatted_from_time,
            till_time: formatted_till_time,
            email_type: EMAIL_TYPE::AGGREGATED_PRODUCTS[:type],
            subject: email_subject.to_s,
            to_first_name: to_name,
            to_email_id: email_ids.to_s,
            pdf_needed: pdf_needed,
            multiple: true
          }
          # Send mail to IMLI orderManagement a mail
          # regarding this invoice for pickers to pick the brand packs required

          ORDER_EMAIL_WORKER.perform_async(mail_attributes)
        rescue => e
          ApplicationLogger.debug('Failed to inform aggregation reporting to IMLI Team : ' + ' , ' + e.message)
        end
      end

      #
      # Initialize Order Time Filters
      #   - to_time: passed to_time (or default time passed)
      #   - from_time: passed from_time (or to_time minus 1 day)
      #
      # @param params [JSON] [Hash of interval params]
      # @param default_to_time [DateTime] [default value of to time]
      #
      # @return [JSON] [Hash of from_time and to_time]
      #
      def initialize_order_time_filters(params, default_to_time)
        to_time = params[:to_time]
        from_time = params[:from_time]

        final_to_time = to_time.present? ? Time.zone.at(to_time.to_i) : default_to_time
        final_from_time = from_time.present? ? Time.zone.at(from_time.to_i) : (final_to_time - 1.day)

        # Take starting of from_time to ending of to_time into account
        final_from_time = final_from_time.beginning_of_day
        final_to_time = final_to_time.end_of_day
        return ({till_time: final_to_time, from_time: final_from_time})
      end

      #
      # initialize_and_validate_time function
      # @param params [Hash]
      # - Contains:
      #   - to_time    [String] (optional) - format: Epoch Unix Time Stamp
      #   - from_time  [String] (optional) - format: Epoch Unix Time Stamp
      #
      # If none of the above params is specified, the default time is picked up
      # for any of the missing params.
      # Then, if due to till Time < from Time, defalt times are used.
      #
      # @return [type] [description]
      def initialize_and_validate_time(params)
        till_time = Time.zone.now.change(hour: 06, minutes: 00)
        from_time = till_time - 1.day

        unless params[:to_time].blank?
          # override till_time params if present
          till_time = Time.zone.at(params[:to_time].to_i)
        end
        unless params[:from_time].blank?
          # override from_time params if present
          from_time = Time.zone.at(params[:from_time].to_i)
        end

        # checks if new custom time line is valid time line
        # else default 6am to 6am localzone timeline is utilized
        if (till_time < from_time)
          till_time = Time.zone.now.change(hour: 06, minutes: 00)
          from_time = till_time - 1.day
          puts CONTENT::USE_DEFAULT_TIME_PERIOD
        end

        return ({till_time: till_time, from_time: from_time})
      end

      #
      # Transational function to change the state of Order
      #
      def transactional_change_state(args)
        transactional_function = Proc.new do |args|
          return change_state(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Function to change state of Order & trigger notifications correspondingly
      #
      def change_state(params)
        order = validate_if_exists(params[:id], @order_dao, 'Order')
        pre_change_state_process(params, order)
        order = @order_dao.change_state(order, params[:event])
        order_notification = ORDER_NOTIFICATION.new
        order_notification.trigger_user_communication(order, params[:event])
        post_change_state_process(params, order)
        return order
      end

      #
      # Edit time slot for an order
      #
      # @param order_params [JSON] [New time slot params and order id]
      #
      # @return [Object] [updated order]
      #
      def edit_order_time_slot(order_params)
        order_service = ORDER_SERVICE.new(@params)
        id = order_params[:id]
        preferred_slot = order_params[:preferred_slot]
        # Validate whether order exists and then update its preferred date and slot
        order = validate_if_exists(id, @order_dao, 'Order')
        order = order_service.update_preferred_slot_for_order(order, preferred_slot)
        return order
      end

      #
      # Edit time slot for an order
      #
      # @param order_params [JSON] [New time slot params and order id]
      #
      # @return [Object] [updated order]
      #
      def edit_order_address(order_params)
        order_service = ORDER_SERVICE.new(@params)
        order_dao = ORDER_DAO.new(@params)
        id = order_params[:id]
        address = order_params[:address]
        # Validate whether order exists and then update address of the associated user
        order = validate_if_exists(id, @order_dao, 'Order')
        if order.address_id.to_i != address[:id].to_i
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NOT_VALID_ORDER_ADDRESS)
        end
        user = order.user
        # Go for address updation only if request is valid
        if (is_valid_request_for_update_address?(order))
          begin
            # Update user's address
            address_service = ADDRESS_SERVICE.new(@params)
            address = address_service.create_new_and_persist_old_address_for_user({
              user: user,
              address: address
              })
            # Update this order with updated_address
            order = order_dao.update_order({ address: address }, order)
          rescue => e
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
          end
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NOT_VALID_REQUEST_FOR_ADDRESS_UPDATE)
        end
        return order
      end

      #
      # Edit Order's cart
      #
      # @param order_params [JSON] [Hash containing order params]
      #
      # Order Params structure::
      #  id: id of the order which is to be editted
      #  cart: Hash containing
      #    id: id of the cart
      #    order_products: Array of order_product hash [{}, {}]
      #      product: Product hash containing MPSP id {id: mpsp_id}
      #      quantity: Quantity of the product
      #      additional_discount: additional discount to be put
      #
      #
      # @return [type] [description]

      # Run This block within transaction
      _RunInTransactionBlock_
      def edit_order_cart(order_params)
        order_service = ORDER_SERVICE.new(@params)
        cart_service = CART_SERVICE.new(@params)
        order_dao = ORDER_DAO.new(@params)
        cart_dao = CART_DAO.new(@params)
        id = order_params[:id]
        cart_hash = order_params[:cart]
        # Validate whether order exists and then update address of the associated user
        order = validate_if_exists(id, @order_dao, 'Order')
        # Eager load complete order details before proceeding
        order = ORDER_DAO.load_order_details_data(order)
        cart = cart_dao.get_by_id(cart_hash[:id])
        if order.cart != cart
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NOT_VALID_ORDER_CART)
        end
        # Eager load cart before proceeding further
        cart = CART_DAO.load_cart_details(cart)
        # Go for cart updation only if request is valid
        if (is_valid_request_for_update_cart?(order))
          begin
            # Move old cart to Discard state, add updated order products to
            # new cart. Recompute cart stats, recompute payment details
            order_products_array = cart_hash[:order_products]
            new_products_array = cart_hash[:new_products]
            user = order.user
            previous_cart = order.cart
            new_cart = cart_dao.create_new_cart(user)

            # Copy old order products to cart
            new_cart = cart_service.copy_order_products_to_new_cart(new_cart, previous_cart, order_products_array)
            # Add new order products
            new_products_array.each do |order_product|
              cart_service.validate_update_cart(order_product)
              new_cart = cart_service.update_order_product_to_cart(order_product, new_cart)
            end

            cart_service.monitor_cart_status(new_cart)
            new_cart = cart_dao.checkout_cart(new_cart)
            cart_dao.discard_cart(previous_cart)
            new_cart = cart_dao.re_compute_cart_stats(new_cart)
            order = order_dao.assign_cart_to_order(new_cart, order)
            # Update payment of that order
            update_payment_for_order({user: user, cart: new_cart, order: order})
            # Update Stocking as per new cart
            ORDER_STOCKING_HELPER.update_stock_based_on_previous_cart(previous_cart, new_cart)
          rescue => e
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e.message)
          end
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NOT_VALID_REQUEST_FOR_CART_UPDATE)
        end
        return order
      end

      #
      # Update payment for Order as per the newly updated cart
      #
      # @param args [JSON] [Hash containing certain must attributes
      #  like user, cart, order]
      #
      # Params::
      #   user: User for the order
      #   cart: Newly updated cart
      #   order: Order whose payment is to be updated
      #
      # @return [Object] [New payment object with updated details]
      #
      def update_payment_for_order(args)
        payment_service = PAYMENT_SERVICE.new(@params)
        payment_computation_service = PAYMENT_COMPUTATION_SERVICE.new(@params)
        payment_dao = PAYMENT_DAO.new(@params)
        user = args[:user]
        cart = args[:cart]
        order = args[:order]
        # Fetch last successful payment of the given ORDER
        old_payment = payment_dao.get_last_successful_payment(order)
        # Remaining computation of payment will be computed using COD mode
        payment_computation_mode = PAYMENT_MODES::CARD_ON_DELIVERY

        payment_hash = payment_computation_service.recompute_payment_details({user: user, cart: cart, order: order, payment_mode: payment_computation_mode})

        payment_hash = apply_discounts_in_payment(order, payment_hash, user)

        new_payment = payment_computation_service.reassign_new_payment_as_per_amount_diff({order: order, old_payment: old_payment, new_payment_hash: payment_hash})

        return new_payment
      end

      #
      # This funtion apply ample credit discount
      #
      # @param order [Model] order of corresponding payment
      # @param payment_hash [Hash] new payment hash
      # @param user [Model] user model
      #
      # @return [Hash] updated payment_hash
      #
      def apply_discounts_in_payment(order, payment_hash, user)

        payment_dao = PAYMENT_DAO.new(@params)

        successful_and_pending_payments = payment_dao.get_successful_and_pending_payments(order)
        ample_credit_amount = get_ample_credit_amount_if_applied(successful_and_pending_payments)
        payment_hash = update_ample_credit_in_payment(payment_hash, ample_credit_amount, user) if ample_credit_amount.present?

        return payment_hash
      end

      #
      # This function update ample credit in payment
      #
      # @param payment_hash [Hash] payment hash
      # @param ample_credit_amount [Decimal] ample credit amount applied in payment
      # @param user [Model] user model
      #
      # @return [Hash] updated payment_hash
      #
      def update_ample_credit_in_payment(payment_hash, ample_credit_amount, user)
        if payment_hash[:net_total] < ample_credit_amount
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not Implemented Usecase: billing amount should be greater than ample credit user')
        end
        payment_hash[:net_total] -= ample_credit_amount
        payment_hash[:ample_credit_amount] = ample_credit_amount
        return payment_hash
      end

      #
      # This function checks if ample credit is applied in all the pending and
      # @param successful_and_pending_payments [type] [description]
      #
      # @return [type] [description]
      def get_ample_credit_amount_if_applied(successful_and_pending_payments)
        return 0 if successful_and_pending_payments.blank?

        successful_and_pending_payments.each do |payment|
          return payment.ample_credit_amount if payment.ample_credit_amount.present? && payment.ample_credit_amount > 0.0
        end

        return 0
      end

      #
      # Function to perform processes after the state change of order
      #
      def pre_change_state_process(params, order)
        case params[:event].to_i
          when ORDER_EVENTS::PACKING_DONE
            unblock_warehouse_brand_packs(order)
        end
      end


      #
      # Function to perform processes after the state change of order
      #
      def post_change_state_process(params, order)

        case params[:event].to_i
          when ORDER_EVENTS::FULFILLMENT_SUCCESSFUL
            if first_order_fulfilled?(order.user)
              first_order_fulfilled_tasks(order.user)
            end
          when ORDER_EVENTS::PACKING_DONE
            out_warehouse_brand_packs(order)
          when ORDER_EVENTS::FORCE_CANCEL
            cancel_all_third_party_orders(order)
            credit_ample_credits(order)
            inward_warehouse_brand_packs(order)
            unblock_warehouse_brand_packs(order)
            cutback_time_slot_entry(order)
            reactivate_ample_sample_membership(order.user)
          when ORDER_EVENTS::COMPLETE_ORDER
            # Update user savings
            update_user_savings(order)
            if first_order_completed?(order.user)
              first_order_completed_tasks(order)
            end
            expire_pending_expiry_ample_sample_membership(order.user)
          when ORDER_EVENTS::CANCEL_ORDER
            # No need for inward brand packs because this event
            # won't get triggered after packed state
            cancel_all_third_party_orders(order)
            credit_ample_credits(order)
            cutback_time_slot_entry(order)
            reactivate_ample_sample_membership(order.user)
            unblock_warehouse_brand_packs(order)
        end
      end

      #
      # This function free the time slot, it decrement the time_slot_bucket
      #
      # @param order [Model] order
      #
      def cutback_time_slot_entry(order)

        return true if order.time_slot.blank? || order.preferred_delivery_date.blank?

        MarketplaceOrdersModule::V1::TimeSlotDateBucketDao.
          update_allocated_time_slots(order.time_slot, order.preferred_delivery_date, -1)

        MarketplaceOrdersModule::V1::TimeSlots::V1::OrderTimeSlotManipulator.delete({ order: order })

      end

      #
      # credit the amount from ample_credit of user
      #
      # @param ample_credit_amount [Decimal] amount to credit
      # @param user [Model] user from whom aple credit amount to credit
      #
      def credit_ample_credits(order)

        ample_credit_service = AMPLE_CREDIT_SERVICE.new(@params)
        payment_dao = PAYMENT_DAO.new(@params)
        user = order.user
        successful_and_pending_payments = payment_dao.get_successful_and_pending_payments(order)

        return if successful_and_pending_payments.blank?

        successful_and_pending_payments.each do |payment|
          if payment.ample_credit_amount.present? && payment.ample_credit_amount > 0
            ample_credit_service.credit({ample_credit: user.ample_credit, amount: payment.ample_credit_amount})
            ApplicationLogger.debug('Crediting amount ' + payment.ample_credit_amount.to_s + ' due to cancel of order ' + order.order_id.to_s)
            return
          end
        end
      end

      #
      # Function to check if it was User's first order
      #
      def first_order_completed?(user)
        order_dao = ORDER_DAO.new(@params)
        return order_dao.first_order_completed?(user)
      end

      #
      # task to check for after first order completion
      #
      # @param user [Model] user model
      #
      def first_order_completed_tasks(order)
        user = order.user
        # Apply any pending benefits to referrer
        benefit_service = BENEFIT_SERVICE.new(@params)
        benefit_service.apply_benefits_to_referrer({
              user: user,
              trigger: TRIGGER_ENUM_UTIL.get_trigger_id_by_trigger('AFTER_FIRST_ORDER')
            })
      end


      #
      # Cancel all third party orders associated with given order
      #
      # @param user [Model] order model
      #
      def cancel_all_third_party_orders(order)
        return true if order.blank?
        begin
          order_dao = MarketplaceOrdersModule::V1::OrderDao
          payment_done = order_dao.is_payment_pending?(order) ? false : true
          payment = order.final_payment
          user = order.user
          args = {payment: payment, order: order, user: user, payment_done: payment_done}
          third_party_order_service = MarketplaceOrdersModule::V1::ThirdPartyOrderService.instance(@params)
          third_party_orders = third_party_order_service.get_placed_or_pending_orders(order.id)
          third_party_order_service.cancel_third_party_orders(third_party_orders, args)
        rescue => e
          CommonModule::V1::Logger.log_exception(e)
        end
      end

      #
      # Update user savings as per its order's total savings
      #
      def update_user_savings(order)
        payment_dao = PAYMENT_DAO.new(@params)
        user = order.user
        payment = payment_dao.get_orders_payment_as_per_its_mode(order)
        # function call to update user's total savings
        payment_service = PAYMENT_SERVICE.new(@params)
        payment_service.update_user_total_savings(user, payment.total_savings)
      end

      #
      # Function to check if it was User's first order
      #
      def first_order_fulfilled?(user)
        order_dao = ORDER_DAO.new(@params)
        return order_dao.first_order_fulfilled?(user)
      end

      def first_order_fulfilled_tasks(user)
        membership_service  = MEMBERSHIP_SERVICE.new(@params)
        if user.referred_by.present?
          membership_service.reward_referral_one_month_membership(user.referred_by)
        end
      end

      #
      # is valid request for update address
      #
      # @param order [Object] [Order whose address update request comes]
      #
      # @return [Boolean] [true if valid request, else false]
      #
      def is_valid_request_for_update_address?(order)
        # For now, returning TRUE for all requests.
        # Later on, we need to check whether user has alrady ordered in this particular
        # address before. In short, <user, order, address> combination should not exist beforehand
        return true
      end

      #
      # is valid request for update cart for order
      #
      # @param order [Object] [Order whose address update request comes]
      #
      # @return [Boolean] [true if valid request, else false]
      #
      def is_valid_request_for_update_cart?(order)
        return true
      end

      #
      # Function to mark outward of the products being packer for MPSP order
      #
      def out_warehouse_brand_packs(order)
        ORDER_STOCKING_HELPER.outward_warehouse_brand_packs(order.cart)
      end

      #
      # Function to mark inward of the products when the order if focrfully cancelled by the
      # warehouse user after it's being packed
      #
      def inward_warehouse_brand_packs(order)
        ORDER_STOCKING_HELPER.inward_warehouse_brand_packs(order.cart)
      end

      #
      # Function to block MPSP after order is placed
      #
      def block_warehouse_brand_packs(order)
          ORDER_STOCKING_HELPER.block_warehouse_brand_packs(order.cart)
      end

      #
      # Function to unblock MPSP after placed order is cancelled
      #
      def unblock_warehouse_brand_packs(order)
          ORDER_STOCKING_HELPER.unblock_warehouse_brand_packs(order.cart)
      end

      #
      # Function to reactivate the membership of User if Nth order placed is cancelled
      # and n is membership expiry count
      #
      def reactivate_ample_sample_membership(user)
        order_dao = ORDER_DAO.new
        order_count = order_dao.get_current_number_of_order_of_user(user)
        if order_count == CACHE_UTIL.read_int('MEMBERSHIP_EXPIRE_ORDER_COUNT')
          membership_service = MEMBERSHIP_SERVICE.new
          membership_service.reactivate_pending_expiry_ample_sample_membership(user)
        end
      end

      #
      # Function to expire the pending expiry membership of user upon completion of order
      #
      def expire_pending_expiry_ample_sample_membership(user)
        order_dao = ORDER_DAO.new
        order_count = order_dao.get_exact_order_numbers_of_user(user)
        if order_count == CACHE_UTIL.read_int('MEMBERSHIP_EXPIRE_ORDER_COUNT')
          membership_service = MEMBERSHIP_SERVICE.new
          membership_service.expire_pending_expiry_ample_sample_membership(user)
        end
      end

      def self.send_monthly_account_summary_to_users
        ApplicationLogger.debug('Users Account Summary started at: ' + Time.zone.now.to_s)
        user_service = USER_SERVICE.new({})
        order_data_service = ORDER_DATA_SERVICE.new({})
        monthly_mails_initiated = 0
        previous_month_datetime = Time.zone.now.prev_month
        month = previous_month_datetime.month
        year = previous_month_datetime.year
        active_users = user_service.get_all_users_by_status(UsersModule::V1::ModelStates::UserStates::ACTIVE)
        # For Testing
        # active_users = {users: UsersModule::V1::User.where(phone_number:'7032991136')}
        if active_users[:users].present?
          active_users[:users].each do |user|
            monthly_orders_count = 0
            monthly_savings = 0
            monthly_order_amount = 0

            completed_user_orders_place_in_last_month =
              order_data_service.get_orders_for_user_by_calendar_month(user, MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates::COMPLETED[:value], month, year)

            monthly_orders_count = completed_user_orders_place_in_last_month.count

            # Process if User's Monthly Order Count > 0
            if monthly_orders_count > 0
              completed_user_orders_place_in_last_month.each do |user_order|
                monthly_savings += user_order.payment.total_savings
                monthly_order_amount += user_order.cart.cart_total
              end
              mail_attributes = {
                monthly_orders: monthly_orders_count.to_s,
                monthly_savings: monthly_savings.round.to_s,
                monthly_order_amount: monthly_order_amount.round.to_s,
                total_lifetime_savings: user.profile.savings.round.to_s
                }
              UsersModule::V1::UserNotificationService.send_monthly_account_summary_email(user, previous_month_datetime, mail_attributes)
              monthly_mails_initiated += 1
            end
          end
          ApplicationLogger.debug('Number of Processed Active Users for monthly Account Summary: ' + active_users[:users].count.to_s)
          ApplicationLogger.debug('Number of Processed Mails for monthly Account Summary: ' + monthly_mails_initiated.to_s)
          ApplicationLogger.debug('[Success] Users Account Summary ended at: ' + Time.zone.now.to_s)
        else
          ApplicationLogger.debug('No Active Users')
        end
      end

  	end
  end
end
