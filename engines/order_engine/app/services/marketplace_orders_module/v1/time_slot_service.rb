module MarketplaceOrdersModule

  module V1

    class TimeSlotService < BaseOrdersModule::V1::BaseService

      SENTRY_LOGGER = CommonModule::V1::Logger
      TIME_SLOT_DAO = MarketplaceOrdersModule::V1::TimeSlotDao
      TIME_SLOT_HELPER = MarketplaceOrdersModule::V1::TimeSlotHelper
      DATE_TIME_UTIL = CommonModule::V1::DateTimeUtil
      TIME_SLOT_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::TimeSlotEvents
      TIME_SLOT_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::TimeSlotStates

      #
      # Initialize TimeSlot service with passed params
      #
      def initialize(params = {})
        @geographical_cluster_dao = GeographicalClustersModule::V1::GeographicalClusterDao
        @params = params
      end


      def get_slots_inclusive_of_buffer(order)
        validators = get_validators_for_time_slots_inclusive_of_buffers
        dates = TimeSlotsModule::V1::ValidationsUtil.get_state_and_final_dates_for_reschedule_delivery(order)

        return get_available_slots_for_order(order, validators, dates)
      end

      #
      # Get available slots for ORDER
      #
      # @param order [Object] [Order whose available slots are to be computed]
      #
      # @return [Array] [Array of the available slots]
      #
      def get_available_slots_for_order(order, validators = get_default_time_slot_validators, dates = {})

        begin
          active_slots = TIME_SLOT_DAO.get_active_slots_by_address(order.address)
        rescue => e
          SENTRY_LOGGER.log_exception(e)
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not a valid address')
        end

        #
        # Here min offset is global config value MIN_SLOT_OFFSET and max offset is global config value MAX_SLOT_OFFSET
        #
        if dates.blank?
          dates = TimeSlotsModule::V1::ValidationsUtil.get_start_and_final_dates
        end

        date_iterator = dates[:start_available_date]
        final_available_date = dates[:final_available_date]

        available_slots = []
        atleast_one_available = false

        while date_iterator <= final_available_date
          per_day_hash = {}
          # Insert EPOCH date into the response hash
          per_day_hash['slot_date'] = DATE_TIME_UTIL.convert_date_time_to_epoch(date_iterator)
          per_day_hash['slot_date_label'] = date_iterator.strftime('%a , %d %b %Y')  # Keep it as per format
          time_slots_array = []
          active_slots.each do |slot|

            slot_hash = TIME_SLOT_HELPER.get_time_slot_hash(slot)
            preferred_date = DATE_TIME_UTIL.convert_epoch_to_date(per_day_hash['slot_date'])

            if !validate_time_slots(order, slot, preferred_date, validators)
              slot_hash['available'] = false
            else
              slot_hash['available'] = true
              atleast_one_available = true
            end

            if MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotBelongsToSameOrder.validate({
                                                        order: order,
                                                        time_slot: slot,
                                                        preferred_date: DATE_TIME_UTIL.
                                                                convert_epoch_to_date(per_day_hash['slot_date'])
                                                                                                       })
              slot_hash['selected'] = true
              slot_hash['available'] = true
              atleast_one_available = true
            else
              slot_hash['selected'] = false
            end
            time_slots_array.push(slot_hash)
          end
          per_day_hash['time_slots'] = time_slots_array
          available_slots.push(per_day_hash) if time_slots_array.present?
          date_iterator = date_iterator.next_day
        end
        return atleast_one_available == true ? available_slots : []
      end

      def validate_time_slots(order, slot, preferred_date, validators)

        args = {
          order: order,
          time_slot: slot,
          preferred_date: preferred_date
        }

        validators.each do |validator|
          return false if !validator.validate(args)
        end

        return true
      end

      #
      # Add time slot
      #
      # @param time_slot_params [JSON] [Hash containing time slot arguments]
      #
      # @return [Object] [created Time slot]
      #
      _RunInTransactionBlock_
      def add_time_slot(time_slot_params)
        if time_slot_params[:geographical_cluster_id].present?
          validate_if_exists(time_slot_params[:geographical_cluster_id], @geographical_cluster_dao.instance(@params),
                              'Geographical Cluster')
        end
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        from_time = DATE_TIME_UTIL.convert_epoch_to_time(time_slot_params[:from_time].to_i)
        to_time = DATE_TIME_UTIL.convert_epoch_to_time(time_slot_params[:to_time].to_i)

        time_slot_params[:unavailable_dates] = convert_epoc_to_dates(time_slot_params[:unavailable_dates]) if time_slot_params[:unavailable_dates].present?

        time_slot_params[:order_limit] = time_slot_params[:order_limit].present? ? time_slot_params[:order_limit] : 10
        time_slot_params[:buffer_limit] = time_slot_params[:buffer_limit].present? ? time_slot_params[:buffer_limit] : 0

        add_time_slot_args = {
          from_time: from_time,
          to_time: to_time,
          label: time_slot_params[:label],
          unavailable_days: time_slot_params[:unavailable_days] || [],
          unavailable_dates: time_slot_params[:unavailable_dates] || [],
          order_limit: time_slot_params[:order_limit],
          buffer_limit: time_slot_params[:buffer_limit],
          min_slot_offset: time_slot_params[:min_slot_offset],
          max_slot_offset: time_slot_params[:max_slot_offset]
        }

        add_time_slot_args.merge!(geographical_cluster_id: time_slot_params[:geographical_cluster_id]) if time_slot_params[:geographical_cluster_id].present?
        time_slot = time_slot_dao.create_time_slot(add_time_slot_args)
        return time_slot
      end

      #
      # Update time slot
      # LOGIC:: If from_time and to_time are changed, then we should move old
      # time slot to DELETED state and create a new one with updated params
      #
      # @param time_slot_params [JSON] [Hash of time slot]
      #
      # @return [Object] [updated time slot]
      #
      _RunInTransactionBlock_
      def update_time_slot(time_slot_params)
        if time_slot_params[:geographical_cluster_id].present?
          validate_if_exists(time_slot_params[:geographical_cluster_id], @geographical_cluster_dao.instance(@params),
                             'Geographical Cluster')
        end
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        id = time_slot_params[:id]
        time_slot = time_slot_dao.get_by_id(id)

        time_slot_params[:unavailable_dates] = convert_epoc_to_dates(time_slot_params[:unavailable_dates]) if time_slot_params[:unavailable_dates].present?

        if time_slot_params[:from_time].present? && time_slot_params[:to_time].present?
          time_slot_params[:from_time] = DATE_TIME_UTIL.convert_epoch_to_time(time_slot_params[:from_time].to_i)
          time_slot_params[:to_time] = DATE_TIME_UTIL.convert_epoch_to_time(time_slot_params[:to_time].to_i)
          # If from time and to time are changed, then SOFT_DELETE the old one
          # and create a new one with updated params
          if DATE_TIME_UTIL.compare_hour_and_minute(time_slot.from_time, time_slot_params[:from_time]) != 0 ||
            DATE_TIME_UTIL.compare_hour_and_minute(time_slot.to_time, time_slot_params[:to_time]) != 0
            new_time_slot = time_slot.dup
            soft_delete_time_slot(time_slot) if time_slot.status != TIME_SLOT_STATES::DELETED
            time_slot = new_time_slot
            time_slot.save!
          end
        end

        time_slot = time_slot_dao.update_time_slot(time_slot_params, time_slot)

        return time_slot
      end

      #
      # Get time slot
      #
      # @param time_slot_params [JSON] [Hash containing time slot id]
      #
      # @return [Object] [time slot]
      #
      def get_time_slot(time_slot_params)
        time_slot_id = time_slot_params[:id]
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        time_slot = time_slot_dao.get_by_id(time_slot_id)
        return time_slot
      end

      #
      # Get all filtered and paginated time slots
      #
      # @param time_slot_params [JSON] [Hash containing pagination params]
      #
      # @return [Array] [Array of time slot objects]
      #
      def get_all_time_slots(pagination_params = {})
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        pagination_params = pagination_params.merge({
          sort_by: 'from_time',
          order: 'ASC'
          })
        time_slots = time_slot_dao.get_all(pagination_params)
        return time_slots
      end

      #
      # Change state of time slot
      #
      # @param time_slot_params [JSON] [Hash containing time slot id and event]
      #
      # @return [Object] [corresponding time slot]
      #
      _RunInTransactionBlock_
      def change_time_slot_state(args)
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        change_state(args, time_slot_dao, 'Time Slot')
      end

      #
      # SOFT DELETE time slot by changing its state
      #
      # @param time_slot_params [Object] [time slot which is to be deleted]
      #
      # @return [Object] [corresponding time slot]
      #
      _RunInTransactionBlock_
      def soft_delete_time_slot(time_slot)
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        time_slot_dao.change_state(time_slot, TIME_SLOT_EVENTS::SOFT_DELETE)
      end

      #
      # Check whether current time lies between the given slot
      #
      # @param slot [Object] [Time slot]
      # @param time [Time] [time which is to be checked]
      #
      # @return [Boolean] [true is time lies in slot else false]
      #
      def time_lies_within_slot(slot, time)
        return true if DATE_TIME_UTIL.compare(time, slot.from_time) == 1 && DATE_TIME_UTIL.compare(time, slot.to_time) == -1
        return false
      end

      #
      # Convert array of epoc seconds to array of dates
      #
      # @param unavailable_dates [Array] array of epoch
      #
      # @return [Array] array of dates of corresponding epochs
      #
      def convert_epoc_to_dates(unavailable_dates)
        return [] if unavailable_dates.blank?

        for i in 0..unavailable_dates.length - 1
          unavailable_dates[i] = DATE_TIME_UTIL.convert_epoch_to_date(unavailable_dates[i])
        end

        return unavailable_dates
      end

      private

      def get_default_time_slot_validators
        return [
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::PreferredDateValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotDateValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TotalSlotLimitValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotDayValidator
        ]
      end

      def get_validators_for_time_slots_inclusive_of_buffers
        return [
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::OrderDateValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotDateValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::OverallSlotLimitValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotDayValidator
        ]
      end

    end
  end
end
