module MarketplaceOrdersModule

  module V1

    class OrderService < BaseOrdersModule::V1::BaseService
    # Including Transaction Helper library
    require 'transaction_helper'
      #
      # initialize CartsService Class
      #
      CART_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::CartEvents
      USER_SERVICE = UsersModule::V1::UserService
      CART_DAO = MarketplaceOrdersModule::V1::CartDao
      ORDER_DAO = MarketplaceOrdersModule::V1::OrderDao
      TIME_SLOT_DAO = MarketplaceOrdersModule::V1::TimeSlotDao
      ORDER_PRODUCTS_SERVICE = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService
      ADDRESS_DAO = AddressModule::V1::AddressDao
      PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content
      DATE_TIME_UTIL = CommonModule::V1::DateTimeUtil
      CACHE_UTIL = CommonModule::V1::Cache
      FREEZING_TTL = 5 * 60
      DECREMENT = -1

      #
      # Initialize CART service with passed params
      #
      def initialize(params = {})
        @params = params
      end

      #
      # Execute Place Order functionality with Transactional block.
      # If anything fails, cart will come back to its previous status
      #
      # @param place_order_params [JSON] [Hash containing cart and address hashes]
      #
      # @return [JSON] [Hash containing order and payment details]
      #
      def transactional_place_order(place_order_params)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return place_order(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: place_order_params
          })
        transaction_block.run();
      end

      #
      # Place ORDER by creating a new Order object and
      # linking cart and user with it
      #
      # @param place_order_params [JSON] [Hash containing cart and address hashes]
      #
      # @return [JSON] [Hash containing order and payment details]
      #
      def place_order(place_order_params)
        user_service = USER_SERVICE.new(@params)
        cart_dao = CART_DAO.new(@params)
        order_dao = ORDER_DAO.new(@params)
        order_products_service = ORDER_PRODUCTS_SERVICE.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        @cart = cart_dao.find_existing_cart(user)

        validate_user_cart(@cart, place_order_params[:cart][:id])
        # Re compute cart stats
        recomputed_order_products_hash = order_products_service.recompute_order_products(@cart.order_products)
        @cart = cart_dao.re_compute_cart_stats(@cart)

        address = fetch_address_for_user(place_order_params[:address][:id], user)
        previous_initiated_order = order_dao.fetch_initiated_order_for_user(user)
        if previous_initiated_order.present?
          if previous_initiated_order.cart.id != @cart.id
            ApplicationLogger.debug('User CART' + @cart.id.to_s + ' and INITIATED Order CART ' + previous_initiated_order.cart.id.to_s + ' mismatch')
          end
          order = previous_initiated_order
        else
          # Create new order
          order = order_dao.create_new_order({
            carts: [ @cart ],
            user: user,
            address: address
            })
          order_id = generate_order_id(order)
          order = order_dao.update_order_id(order, order_id)
        end
        # Record order date. Although state of ORDER will be INITIATED by now, but still
        # we need to show it on panel, and hence need order_date for it
        order = order_dao.record_placed_date(order, Time.zone.now)
        # Attach any additional third party orders
        order = attach_any_third_party_orders(order, place_order_params[:extras])
        # Generate user and cart specific payment details
        billing_details = PAYMENT_HELPER.fetch_user_billing_details({user: user, cart: order.cart, order: order})
        payment_mode_details = []
        #payment_mode_details = PAYMENT_HELPER.fetch_cart_specific_payment_details(order.cart, billing_details[:billing_amount])
        # Move CART to checked out state AFTER Payment Successfull
        # Till that time let it be in Active State
        # cart_dao.change_state(@cart, CART_EVENTS::CHECKOUT)
        ample_credit = user.ample_credit

        return { order: order, payment_details: payment_mode_details, billing_details: billing_details, ample_credit: ample_credit, additional_details: recomputed_order_products_hash[:additional_details]}
      end

      #
      # Schedule users order
      #
      # @param order_params [JSON] [Hash containing preferred slot and order id]
      #
      # @return [Object] [updated order]
      #
      _RunInTransactionBlock_
      def schedule_order(order_params)
        id = order_params[:id]
        preferred_slot = order_params[:preferred_slot]
        user_service = USER_SERVICE.new(@params)
        order_dao = ORDER_DAO.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])

        order = order_dao.get_users_order_with_id(user, id)

        begin
          order = update_preferred_slot_for_order(order, preferred_slot, preferred_slot[:freeze_slot])
        rescue CUSTOM_ERROR_UTIL::SlotUnavailable => e
          return { order: order, slot_available: false}
        rescue CUSTOM_ERROR_UTIL::SlotBelongsToSameOrder => e
          return {order: order, slot_available: true}
        end

        return {order: order, slot_available: true}
      end

      #
      # ReSchedule users order
      #
      # @param order_params [JSON] [Hash containing preferred slot and order id]
      #
      # @return [Object] [updated order]
      #
      _RunInTransactionBlock_
      def reschedule_order(order_params)

        preferred_slot = order_params[:preferred_slot]
        order_id = order_params[:order_id]

        order_dao = ORDER_DAO.new(@params)
        order = order_dao.get_order_by_order_id(order_id)

        date_time = DATE_TIME_UTIL.convert_epoch_to_datetime(preferred_slot[:slot_date].to_s)
        preferred_date = date_time.to_date
        preferred_time_slot = fetch_time_slot(preferred_slot[:time_slot][:id])

        assigned_slot = order.time_slot
        assigned_preferred_delivery_date = order.preferred_delivery_date

        if MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::SlotBelongsToSameOrder.validate({
                                                                                      order: order,
                                                                                      time_slot: preferred_time_slot,
                                                                                      preferred_date: preferred_date
                                                                                                   })
          return {order: order, slot_available: true}
        end


        begin

          MarketplaceOrdersModule::V1::TimeSlots::V1::AllocateTimeSlot.allocate_considering_buffers({
                                                                            order: order,
                                                                            preferred_date: preferred_date,
                                                                            time_slot: preferred_time_slot
                                                                                })

          if assigned_slot.present?
            MarketplaceOrdersModule::V1::TimeSlotDateBucketDao.
                                update_allocated_time_slots(assigned_slot, assigned_preferred_delivery_date, DECREMENT)
          end

          MarketplaceOrdersModule::V1::TimeSlots::V1::OrderTimeSlotManipulator.update({
                                                                            order: order,
                                                                            preferred_date: preferred_date,
                                                                            time_slot: preferred_time_slot
                                                                                      })

          ## Commenting this out as content is nor ready yet
          # MarketplaceOrdersModule::V1::RescheduleOrderCommunications.communicate_user({
          #                                                                      order: order,
          #                                                                      updated_slot: preferred_time_slot,
          #                                                                      updated_delivery_date: preferred_date
          #                                                                    })
        rescue CUSTOM_ERROR_UTIL::SlotUnavailable => e
          return { order: order, slot_available: false}
        end

        return {order: order, slot_available: true}
      end

      #
      # Get all active orders
      #
      def get_active_orders
        user_service = USER_SERVICE.new(@params)
        order_dao = ORDER_DAO.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])

        orders = order_dao.get_non_delivered_orders(user)
        return { orders: orders }
      end

      #
      # Get all previous orders
      #
      def get_previous_orders
        user_service = USER_SERVICE.new(@params)
        order_dao = ORDER_DAO.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])

        orders = order_dao.get_delivered_orders(user)
        return { orders: orders }
      end

      #
      # Get complete details of ORDER
      # it validates the order and return Order object
      #
      def get_order_details(args)
        user_service = USER_SERVICE.new(@params)
        order_dao = ORDER_DAO.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])

        order = order_dao.get_users_order_with_id(user, args[:id])
        if order.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::ORDER_NOT_BELONG_TO_USER)
        end
        return { order: order }
      end

      #
      # Validates user cart
      #
      # @param user_cart [Object] [Cart of the user]
      # @param cart_id [Integer] [Requested ID]
      #
      def validate_user_cart(user_cart, cart_id)
        # CART state is Empty
        if user_cart.blank? || user_cart.empty_cart?
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::NO_ACTIVE_CART)
        end
        if user_cart.id.to_i != cart_id.to_i
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_CART)
        end
        cart_service = MarketplaceOrdersModule::V1::CartService
        # It returns true if cart is valid for placing order
        # ELSE, It raises exception with the reason of "why it is not valid ?"
        cart_service.is_cart_valid_for_placing_order?(user_cart)
      end

      #
      # Fetch address for user corresponds to the passed address ID
      #
      # @param address_id [Integer] [address ID of the user]
      # @param user [Object] [Requested user]
      #
      # @return [Object] [Address object if matches otherwise empty object]
      #
      def fetch_address_for_user(address_id, user)
        address_dao = ADDRESS_DAO.new
        address = address_dao.get_address_for_user(address_id, user)
        if address.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_ADDRESS)
        end
        return address
      end

      #
      # Generates Order ID (a unique number)
      #
      def generate_order_id(order)
        # It's just a random string concatenation, but will surely be unique
        order_prefix = CACHE_UTIL.read('MARKETPLACE_ORDER_PREFIX') || 'AMPLE'
        return order_prefix + '-' + '28' + order.id.to_s + '-91' + order.user.id.to_s
      end

      #
      # Update preferred slot for order
      #
      # @param order [Object] [Order whose slot is to be changed]
      # @param preferred_slot [JSON] [Hash of preferred slot details, date and slot id]
      #
      # @return [object] [Updated order]
      #
      def update_preferred_slot_for_order(order, preferred_slot, freeze_slot)

        return order if order.blank? || preferred_slot.blank?
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        # Convert epoch request to DateTime and then to DATE Object

        date_time = DATE_TIME_UTIL.convert_epoch_to_datetime(preferred_slot[:slot_date].to_s)
        preferred_date = date_time.to_date
        preferred_time_slot = fetch_time_slot(preferred_slot[:time_slot][:id])

        if freeze_slot == true
          update_freezed_slots(order, preferred_date, preferred_time_slot)
        else
          allocate_slot(order, preferred_date, preferred_time_slot)
        end

      end

      #
      # This function update the time slot bucket order_count, if allocated limit not reached to max
      #
      # @param order [Model] order model
      # @param preferred_date [String] preferred date of the slot
      # @param time_slot[Model] time slot model
      #
      # @return [Model] updated order
      #
      # @raise [SlotUnavailable] if slot is not available, slot max limit for the preferred date reached
      #
      def allocate_slot(order, preferred_date, time_slot)

        args = {
          order: order,
          preferred_date: preferred_date,
          time_slot: time_slot
        }

        assigned_slot = order.time_slot

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Time slot already assigned to order') if assigned_slot.present?

        begin
          MarketplaceOrdersModule::V1::TimeSlots::V1::AllocateTimeSlot.allocate(args)
        rescue
          TimeSlotsModule::V1::ValidationsUtil.clear_time_slot_data(args)
          CUSTOM_ERROR_UTIL::SlotUnavailable
        end

        return MarketplaceOrdersModule::V1::TimeSlots::V1::OrderTimeSlotManipulator.update(args)

      end

      #
      # This function update the freezed slot in cache if not already present in cache
      #
      # @param order [Model] order model
      # @param preferred_date [String] preferred date of the slot
      #
      # @return [Model] updated order
      #
      # @raise [SlotUnavailable] if slot is not available, slot max limit for the preferred date reached
      #
      def update_freezed_slots(order, preferred_date, time_slot)

        args = {
          order: order,
          time_slot: time_slot,
          preferred_date: preferred_date
        }

        begin
          MarketplaceOrdersModule::V1::TimeSlots::V1::FreezeSlot.freeze(args)
        rescue CommonModule::V1::CustomErrors::SlotUnavailable
          TimeSlotsModule::V1::ValidationsUtil.clear_time_slot_data(args)
          raise CommonModule::V1::CustomErrors::SlotUnavailable
        end

      end

      #
      # Fetch time slot for the order. Also, validate the fetched time slot
      # should be Active
      #
      # @param time_slot_id [Integer] [Time slot id]
      #
      # @return [Object] [fetched TimeSlot object]
      #
      # @raise [InvalidArgumentsError] [if invalid id passed, or validations failed]
      #
      def fetch_active_preferred_time_slot(time_slot_id)
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        begin
          preferred_time_slot = time_slot_dao.get_by_id(time_slot_id)
          if preferred_time_slot.active?
            return preferred_time_slot
          else
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::TIME_SLOT_EXPIRED)
          end
        rescue CUSTOM_ERROR_UTIL::RecordNotFoundError => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_TIME_SLOT)
        end
      end

      #
      # Fetches time slot with respect to time_slot_id
      #
      # @param time_slot_id [Integer] [id of the time slot]
      #
      # @return [Object] [Time slot model object]
      # @raise [SlotUnavailable] [if record not found in db]
      #
      def fetch_time_slot(time_slot_id)
        time_slot_dao = TIME_SLOT_DAO.new(@params)
        begin
          return time_slot_dao.get_by_id(time_slot_id)
        rescue CUSTOM_ERROR_UTIL::RecordNotFoundError => e
          raise CUSTOM_ERROR_UTIL::SlotUnavailable
        end
      end

      #
      # Attach third party orders with our marketplace order
      #  * It iterate over the extras hash and convert them to ThirdPartyOrderClass objects
      #  * Call third_party_order_service to create DB entry and link third_party_orders to marketplace order
      #
      # @param order [Object] [Marketplace order object]
      # @param order_extras_params [Hash] [Hash containing extra attributes came from the request]
      #
      # @return [Object] [Marketplace order model object]
      #
      def attach_any_third_party_orders(order, order_extras_params)
        third_party_order_service = MarketplaceOrdersModule::V1::ThirdPartyOrderService.instance(@params)
        third_party_order_service.discard_all_pending_third_party_orders(order)
        return order if order_extras_params.blank?
        aggregator = MarketplaceOrdersModule::V1::ThirdPartyOrderAggregator
        array = []
        third_party_orders = aggregator.get_third_party_orders(order_extras_params)
        if third_party_orders.present?
          third_party_order_service = MarketplaceOrdersModule::V1::ThirdPartyOrderService.instance(@params)
          third_party_order_service.link_third_party_orders(order, third_party_orders)
        end
        return order
      end

      #
      # This function returns order by order_id else throws exception raised by order dao
      #
      def get_order_by_order_id(order_id)
        order_dao = MarketplaceOrdersModule::V1::OrderDao.new(@params)

        return order_dao.get_order_by_order_id(order_id) # raise exception
      end

      def get_allowed_payment_details_of_order(request)
        user_service = USER_SERVICE.new(@params)
        cart_dao = CART_DAO.new(@params)
        order_dao = ORDER_DAO.new(@params)
        id = request[:id].to_i
        preferred_gateway = request[:gateway] || TransactionsModule::V1::GatewayType::RAZORPAY
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        cart = cart_dao.find_existing_cart(user)

        order = order_dao.get_users_order_with_id(user, id)
        validate_user_cart(cart, order.cart.id)

        # Generate user and cart specific payment details
        billing_details = PAYMENT_HELPER.fetch_user_billing_details({user: order.user, cart: order.cart, order: order})
        allowed_payment_modes = PAYMENT_HELPER.fetch_cart_specific_payment_details(
          order.cart,
          billing_details[:billing_amount],
          {preferred_gateway: preferred_gateway}
        )
        return {
          preferred_gateway: preferred_gateway,
          allowed_modes: allowed_payment_modes || []
        }
      end

    end # End of class
  end
end
