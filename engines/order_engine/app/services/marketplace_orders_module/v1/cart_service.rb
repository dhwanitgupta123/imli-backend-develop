module MarketplaceOrdersModule

  module V1

    class CartService < BaseOrdersModule::V1::BaseService
    # Including Transaction Helper library
    require 'transaction_helper'
      #
      # initialize CartsService Class
      #
      CART_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::CartEvents
      USER_SERVICE = UsersModule::V1::UserService
      ORDER_PRODUCTS_SERVICE = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsService
      MARKETPLACE_SELLING_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceSellingPackService
      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao
      CART_DAO = MarketplaceOrdersModule::V1::CartDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      ELIGIBLE_POSITIVE_MAX_QUANTITY = 1
      ELIGIBLE_NEGATIVE_MAX_QUANTITY = -1

      #
      # Initialize CART service with passed params
      #
      def initialize(params={})
        @params = params
      end

      #
      # Execute Update cart functionality with Transactional block.
      # If anything fails, cart will come back to its previous status
      #
      # @param update_cart_params [JSON] [Hash containing order products Array]
      #
      # @return [JOSN] [Hash containing cart and its active products]
      def transactional_update_cart(update_cart_params)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return update_cart(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: update_cart_params
          })
        transaction_block.run();
      end

      #
      # Get users active CART
      #
      # @return [JOSN] [Hash containing cart and its active products]
      #
      def get_users_cart
        user_service = USER_SERVICE.new(@params)
        cart_dao = CART_DAO.new(@params)
        order_products_service = ORDER_PRODUCTS_SERVICE.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        @cart = cart_dao.find_existing_cart(user)
        # If CART is blank, throw not found error
        if @cart.blank?
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT_UTIL::NO_ACTIVE_CART)
        end
        recomputed_order_products_hash = order_products_service.recompute_order_products(@cart.order_products)
        active_products = monitor_cart_status(@cart)
        # Re compute cart stats & ordered products
        @cart = cart_dao.re_compute_cart_stats(@cart)
        # pass active products back to API for creating response
        return {
          cart: @cart,
          order_products: active_products,
          additional_details: recomputed_order_products_hash[:additional_details]
        }
      end

      #
      # Update CART by updating its order products
      #
      # @param update_cart_params [JSON] [Hash containing order products Array]
      #
      # @return [JOSN] [Hash containing cart and its active products]
      def update_cart(update_cart_params)
        # Take default as checkout:false
        is_checkout = update_cart_params[:checkout] || false

        user_service = USER_SERVICE.new(@params)
        cart_dao = CART_DAO.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        cart = cart_dao.find_existing_cart(user)
        # If CART is blank, then create new cart
        if cart.blank?
          cart = cart_dao.create_new_cart(user)
          cart_dao.change_cart_status_to_active(cart)
        end
        # Iterate over OrderProducts array and update each into cart
        order_products = update_cart_params[:order_products]
        order_products.each do |order_product|
          validate_update_cart(order_product)
          cart = update_order_product_to_cart(order_product, cart)
        end
        recomputed_order_products_hash = {}
        if is_checkout
          order_products_service = ORDER_PRODUCTS_SERVICE.new(@params)
          recomputed_order_products_hash = order_products_service.recompute_order_products(cart.order_products)
        end
        # Check cart attributes and update its status accordingly
        # Return active products of the cart to reduce DB queries
        active_products = monitor_cart_status(cart)
        # pass active products back to API for creating response
        return {
          cart: cart,
          order_products: active_products,
          additional_details: recomputed_order_products_hash[:additional_details]
        }
      end

      #
      # Update order product to cart
      #
      # @param order_product [JSON] [Hash containing order_product_id, quantity and MPSP details (id)]
      #
      # @return [type] [description]
      def update_order_product_to_cart(order_product, cart)
        marketplace_selling_pack_service = MARKETPLACE_SELLING_PACK_SERVICE.new

        mpsp_id = order_product[:product][:id]
        quantity = order_product[:quantity]
        additional_discount = order_product[:additional_discount] || BigDecimal.new('0.0')

        # Expecting mpsp to contain hash of marketplace_selling_pack
        marketplace_selling_pack = marketplace_selling_pack_service.get_marketplace_selling_pack_by_id(mpsp_id)

        # Validate quantity to not to be greater than maximum allowed
        quantity = validate_quantity(quantity, marketplace_selling_pack)

        # Update mpsp to Order product
        order_products = update_mpsp_to_cart(cart, {marketplace_selling_pack: marketplace_selling_pack, quantity: quantity, additional_discount: additional_discount })

        # Update cart savings and other details
        cart = update_cart_details({
          previous_order_product: order_products[:previous_order_product],
          updated_order_product: order_products[:updated_order_product]
          }, cart)
        return cart
      end

      #
      # Update MPSP to order product. Creates a new order product is not present already
      # and assign MPSP details to it.
      #
      # @param cart [Object] [Cart in which mpsp is to be updated]
      # @param mpsp_params [JSON] [Hash containing mpsp_object, quantity and additional_discount]
      #
      # MPSP Params::
      #   marketplace_selling_pack : MPSP-Object which is to be added
      #   quantity : Quantity of the mpsp
      #   additional_discount : Additional discount to be applied on mpsp
      #
      # @return [Integer] [previous quantity of the order product]
      #
      def update_mpsp_to_cart(cart, mpsp_params)
        cart_dao = CART_DAO.new(@params)
        order_products_service = ORDER_PRODUCTS_SERVICE.new(@params)
        marketplace_selling_pack_dao = MARKETPLACE_SELLING_PACK_DAO.new

        marketplace_selling_pack = mpsp_params[:marketplace_selling_pack]
        quantity = mpsp_params[:quantity]
        additional_discount = mpsp_params[:additional_discount]
        # Check if Product exists in cart or not. If exists, then update the same
        # OrderProduct or else create a new one
        order_product = cart_dao.mpsp_exists_in_cart?({
          cart: cart,
          marketplace_selling_pack: marketplace_selling_pack
        })
        pricing = marketplace_selling_pack_dao.get_savings_and_pricing_for_mpsp(quantity, marketplace_selling_pack)
        previous_order_product = nil
        if order_product.blank?
          # Create order product only when quantity is non zero
          if quantity != 0
            updated_order_product = order_products_service.create_order_products({
              cart: cart,
              marketplace_selling_pack: marketplace_selling_pack,
              quantity: quantity,
              mrp: pricing[:mrp],
              selling_price: pricing[:selling_price],
              savings: pricing[:savings],
              vat: pricing[:vat],
              service_tax: pricing[:service_tax],
              taxes: pricing[:taxes],
              discount: pricing[:discount],
              additional_discount: additional_discount
            })
          else
            # Commenting the below log as its number is too much and is spoiling the current logs
            #ApplicationLogger.warn('Create Order Product request came with quantity zero for product id: ' + marketplace_selling_pack.id.to_s + ' in cart: ' + @cart.id.to_s)
          end
        else
          previous_order_product = order_product.dup
          # Update Order product only if it's quantity changed
          # This is to optimize the query when complete CART snapshot is sent
          # everytime with the query
          if previous_order_product.quantity != quantity
            updated_order_product = order_products_service.add_mpsp_to_cart(
              order_product,
              {
                marketplace_selling_pack: marketplace_selling_pack,
                quantity: quantity,
                mrp: pricing[:mrp],
                selling_price: pricing[:selling_price],
                savings: pricing[:savings],
                vat: pricing[:vat],
                service_tax: pricing[:service_tax],
                taxes: pricing[:taxes],
                discount: pricing[:discount],
                additional_discount: additional_discount
              }
            )
          else
            updated_order_product = previous_order_product
          end
        end
        return { previous_order_product: previous_order_product, updated_order_product: updated_order_product }
      end

      #
      # Update cart details (Cart savings and cart total)
      # @param args [JSON] [Hash containing previous_quantity of product, Product object(MPSP)
      # , current_quantity of product]
      #
      def update_cart_details(args, cart)
        cart_dao = CART_DAO.new(@params)

        cart = cart_dao.update_cart_savings_and_total({
          cart: cart,
          previous_order_product: args[:previous_order_product],
          updated_order_product: args[:updated_order_product]
          })
        return cart
      end

      #
      # Get active orders of the cart
      # @param cart [Object] [Cart for which active order products are needed]
      #
      # @return [Array] [Array of all active order products]
      #
      def get_active_order_products_in_cart(cart)
        cart_dao = CART_DAO.new(@params)
        active_order_product = cart_dao.active_products_in_cart(cart)
      end

      #
      # Validate update cart params
      # It validates presence of order product id, its quantity and product hash along with its id.
      #
      # @param order_product [JSON] [Hash containing order product details: id, quantity and product hash]
      #
      # @raise [InsufficientDataError] [if order product id, quantity or product hash/its id are absent]
      #
      def validate_update_cart(order_product)
        if order_product[:quantity].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Order Product quantity'})
        end
        if order_product[:product].blank? or order_product[:product][:id].blank?
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: 'Product ID'})
        end
      end

      #
      # Monitor Cart status and return its active products
      # If cart has Zero active order products, then change its state to EMPTY
      # Else, change its state to ACTIVE, (if not already)
      #
      # @return [Array] [Array of all active products of that cart]
      def monitor_cart_status(cart)
        cart_dao = CART_DAO.new(@params)
        active_products = cart_dao.active_products_in_cart(cart)
        if active_products.blank?
          if !cart.empty_cart?
            cart_dao.change_cart_status_to_empty(cart)
          end
          cart = cart_dao.nullify_cart_stats(cart)
        else
          # Cart is re-building, so Its previous state would be still EMPTY
          if cart.empty_cart?
            cart_dao.change_cart_status_to_active(cart)
          end
        end
        return active_products
      end


      #
      # Copy order products from old cart to new cart, with the updated attributes
      # from products_array.
      # Logic: Fetch order product from old cart as per the mpsp_id present in products hash
      # Copy that order_product in new cart and apply new attributes from products hash
      #
      # @param new_cart [Object] [New cart in which products to be added]
      # @param old_cart [Object] [Old cart from which products are to be copied]
      # @param products_array [Array] [Array of products hash containing mpsp_id, quantity, and additional_discount]
      #
      # products_array: Hash of
      #   - quantity: new quantity of the order_product
      #   - additional_discount: additional discount to be applied on order product
      #   - product: Hash of
      #     - id: mpsp_id of the product to be added
      #
      # @return [Object] [New cart in which products are added]
      #
      def copy_order_products_to_new_cart(new_cart, old_cart, products_array)
        # As per the ids from product hash, fetch order products from old cart
        # Copy these order products into new cart
        return new_cart if products_array.blank?

        cart_dao = CART_DAO.new(@params)
        order_products_service = ORDER_PRODUCTS_SERVICE.new(@params)

        products_array.each do |op|
          mpsp_id = op[:product][:id]
          quantity = op[:quantity]
          additional_discount = op[:additional_discount]
          # Fetch order product from old cart corresponds to mpsp-id
          old_order_product = cart_dao.mpsp_exists_in_cart?({cart: old_cart, marketplace_selling_pack: mpsp_id})
          next if old_order_product.blank?

          new_order_product = old_order_product.dup
          # Ignore products whose quantity are zero
          if quantity == 0
            next
          end
          order_products_service.update_order_product(new_order_product, {
            cart: new_cart,
            quantity: quantity,
            additional_discount: additional_discount
          })
        end
        return new_cart
      end

      #
      # Is cart valid for placing order. Check for certain validation on cart before placing its order
      # Forex: Check whether cart amount is greater than a limited amount
      # It will raise exception in case of validation fails. The exception will be raised by depending
      # function with proper message stating why this cart is not valid.
      #
      # @param cart [object] [Cart model object whose validity is to be checked]
      #
      # @return [Boolean] [true if cart is valid else RAISE EXCEPTION]
      #
      def self.is_cart_valid_for_placing_order?(cart)
        if ImliStaticSettings.get_feature_value('ENABLE_RESTRICTED_AMOUNT_ON_CART') == '1'
          cart_computation_service = MarketplaceOrdersModule::V1::CartComputationService.instance(@params)
          # It will raise exception with appropriate message if not valid
          cart_computation_service.is_cart_total_valid?(cart)
        end
        return false
      end

      ###################################
      #     PRIVATE FUNCTIONS           #
      ###################################

      private

      #
      # Validates quantity
      # Incoming quantity should not be negative and it should not exceed MPSP max quantity
      #
      # @param quantity [Integer] [requested quantity of the order product]
      # @param marketplace_selling_pack [Object] [MPSP corresponds to the requested order product]
      #
      # @raise [InvalidArgumentsError] [if quantity is negative or exceeds max allowed quantity of MPSP]
      #
      def validate_quantity(quantity, marketplace_selling_pack)
        mpsp_mapper = MarketplaceProductModule::V1::MarketplaceSellingPackMapper
        max_quantity = mpsp_mapper.get_max_quantity(marketplace_selling_pack)
        if quantity.to_i > max_quantity
          ApplicationLogger.debug("Order product quantity: " + quantity.to_s + " is greater than max_quantity: " + max_quantity.to_s)
          return max_quantity
        end
        # If incoming quantity is negative, then raise Exception
        if quantity.to_i < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.
          new("Quantity can't be negative for product: " + marketplace_selling_pack.id.to_s)
        end
        return quantity
        # Allowing only +1 and -1 quantity. But for Ladder pricing, its not needed anymore
        # if quantity.to_i > ELIGIBLE_POSITIVE_MAX_QUANTITY.to_i
        #   quantity = ELIGIBLE_POSITIVE_MAX_QUANTITY
        # end
        # if quantity.to_i < ELIGIBLE_NEGATIVE_MAX_QUANTITY.to_i
        #   quantity = ELIGIBLE_NEGATIVE_MAX_QUANTITY
        # end
      end

    end # End of class
  end
end
