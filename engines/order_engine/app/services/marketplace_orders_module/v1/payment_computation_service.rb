module MarketplaceOrdersModule

  module V1

    class PaymentComputationService < BaseOrdersModule::V1::BaseService

      PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
      PAYMENT_SERVICE = MarketplaceOrdersModule::V1::PaymentService
      PAYMENT_DAO = MarketplaceOrdersModule::V1::PaymentDao
      PAYMENT_TYPE = MarketplaceOrdersModule::V1::PaymentType
      PAYMENT_MODES = MarketplaceOrdersModule::V1::PaymentModes
      PAYMENT_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::PaymentStates
      PAYMENT_INSTANCE = MarketplaceOrdersModule::V1::PaymentInstances

      #
      # Initialize PAYMENT service with passed params
      #
      def initialize(params={})
        @params = params
        super
      end

      #
      # Recompute payment details for the order as per the given mode
      #
      # @param args [JSON] [Hash containing certain must attributes
      #  like user, cart, order and payment-mode]
      #
      # @return [JSON] [Hash containing latest payment details]
      #
      def recompute_payment_details(args)
        # User for computing user/membership specific discounts
        user = args[:user]
        # Cart for computing effective delivery charge
        cart = args[:cart]
        # Order for computing nth order discounts
        order = args[:order]
        # Payment mode for computing payment specific savings
        payment_mode = args[:payment_mode]

        computed_billing_details = PAYMENT_HELPER.fetch_user_billing_details({user: user, cart: cart, order: order})
        computed_payment_mode_details = compute_payment_details_as_per_mode({
          mode: payment_mode,
          billing_amount: computed_billing_details[:billing_amount],
          savings: cart.cart_savings
        })

        payment_details = {
          delivery_charges: computed_billing_details[:delivery_charges],
          convenience_charges: computed_billing_details[:convenience_charges],
          grand_total: computed_billing_details[:grand_total],
          discount: computed_billing_details[:discount],
          billing_amount: computed_billing_details[:billing_amount],
          total_vat_tax: BigDecimal.new('0.0'),
          total_service_tax: BigDecimal.new('0.0'),
          payment_mode_savings: computed_payment_mode_details[:payment_mode_savings],
          gift_card: BigDecimal.new('0.0'),
          total_savings: computed_payment_mode_details[:total_savings],
          net_total: computed_payment_mode_details[:net_total],
          payable_total: computed_payment_mode_details[:payable_total]
        }
        return payment_details
      end


      #
      # Reassign new payment as per the amount diff
      #
      # @param args [JSON] [Hash containing order, old payment and new_payment_hash]
      #   order: Order for which new payment is to be created
      #   old_payment: Last successful payment (can be nil sometimes)
      #   new_payment_hash: Hash of final updated payment details
      #
      # @return [Object] [New payment object]
      #
      def reassign_new_payment_as_per_amount_diff(args)
        order = args[:order]
        old_payment = args[:old_payment]
        new_payment_hash = args[:new_payment_hash]

        if old_payment.blank?
          # since creating new payment therefore making payablee_total = net_total
          # because discounts are already applied on net_total
          # example ample_credits
          new_payment_hash[:payable_total] = new_payment_hash[:net_total]

          # No old successful payment was there, hence need to create/merge COD payment
          new_payment = create_new_payment_for_order(order, new_payment_hash)
          return new_payment
        end

        if new_payment_hash[:net_total].to_d == old_payment.net_total.to_d
          # Nothing to do. Payment object remains same
          # But since there might be changes in delivery_charges OR discounts.
          # So, assign new hash to exsisting payment
          discard_any_pending_refund_payment(order)
          discard_any_pending_cod_payment(order)
          new_payment = update_payment_as_per_new_hash(old_payment, new_payment_hash)
        else
          # Create new payment with diff amount
          new_payment = create_new_payment_with_diff(old_payment, new_payment_hash)
          # Note: Need to Trigger API for Razorpay/Citrus in case of Refund SEPARATELY
        end
        return new_payment
      end

      #
      # Create new COD payment for the given order as per the computed payment details
      #
      # @param order [Object] [Order for which payment needs to be created]
      # @param new_payment_hash [JSON] [Hash of newly computed payment details]
      #
      # new_payment_hash should contain following keys (not must):
      # grand_total, delivery_charges, discount, billing_amount, payment_mode_savings, total_vat_tax,
      # total_service_tax, total_savings, net_total, payable_total
      #
      # @return [Object] [New payment object]
      #
      def create_new_payment_for_order(order, new_payment_hash)
        payment_dao = PAYMENT_DAO.new(@params)
        pending_cod_payment = payment_dao.get_pending_cod_payment(order)
        if pending_cod_payment.present?
          new_payment = update_payment_as_per_new_hash(pending_cod_payment, new_payment_hash)
          return new_payment
        end
        # No pending COD payment present, CREATE a new one
        # with updated payment details hash
        payment_args = new_payment_hash.merge({
          order: order,
          mode: PAYMENT_MODES::CARD_ON_DELIVERY,
          payment_instance: PAYMENT_INSTANCE.get_instance_by_mode(PAYMENT_MODES::CARD_ON_DELIVERY)
        })
        new_payment = create_new_payment_as_per_new_hash(payment_args)
        return new_payment
      end

      #
      # Create new payment with diff of the old_payment and newly computed payment details
      # Also, In case diff becomes negative, then need to record a REFUND payment
      #
      # @param old_payment [Object] [previous payment object]
      # @param new_payment_hash [JSON] [Hash of newly computed payment details]
      #
      # new_payment_hash should contain following keys (not must):
      # grand_total, delivery_charges, discount, billing_amount, payment_mode_savings, total_vat_tax,
      # total_service_tax, total_savings, net_total, payable_total
      #
      # @return [Object] [New payment object]
      #
      def create_new_payment_with_diff(old_payment, new_payment_hash)
        payment_dao = PAYMENT_DAO.new(@params)
        order = old_payment.order
        payment_difference = new_payment_hash[:net_total] - old_payment.net_total
        if payment_difference < 0
            # Need to REFUND the money back to customer
            discard_any_pending_cod_payment(order)
            payment_difference = (-1 * payment_difference)
            new_payment_hash = new_payment_hash.merge({
              payable_total: payment_difference
            })
            new_payment = create_refund_payment(order, new_payment_hash)
            return new_payment
        else
          # Need to COLLECT the money from customer
          discard_any_pending_refund_payment(order)
          new_payment_hash = new_payment_hash.merge({
            payable_total: payment_difference
          })
          # Check for any pending COD payments
          pending_cod_payment = payment_dao.get_pending_cod_payment(order)
          if pending_cod_payment.present?
            new_payment = discard_old_payment_and_create_new(pending_cod_payment, new_payment_hash)
            return new_payment
          else
            payment_args = new_payment_hash.merge({
              order: order,
              mode: PAYMENT_MODES::CARD_ON_DELIVERY,
              payment_instance: PAYMENT_INSTANCE.get_instance_by_mode(PAYMENT_MODES::CARD_ON_DELIVERY),
              payable_total: payment_difference
            })
            new_payment = create_new_payment_as_per_new_hash(payment_args)
            return new_payment
          end

        end
      end

      #
      # Create new REFUND payment for given order and new computed payment details
      #
      # @param order [Object] [Order for which REFUND is to be made]
      # @param new_payment_hash [JSON] [Hash of newly computed payment details]
      #
      # new_payment_hash should contain following keys (not must):
      # grand_total, delivery_charges, discount, billing_amount, payment_mode_savings, total_vat_tax,
      # total_service_tax, total_savings, net_total, payable_total
      #
      # @return [Object] [New payment object]
      #
      def create_refund_payment(order, new_payment_hash)
        payment_dao = PAYMENT_DAO.new(@params)
        pending_refund_payment = payment_dao.get_pending_refund_payment(order)
        if pending_refund_payment.blank?
          payment_args = new_payment_hash.merge({
            order: order,
            mode: PAYMENT_MODES::CARD_ON_DELIVERY,
            payment_instance: PAYMENT_INSTANCE.get_instance_by_mode(PAYMENT_MODES::CARD_ON_DELIVERY),
            payment_type: PAYMENT_TYPE.get_id_by_key('REFUND')
          })
          new_payment = create_new_payment_as_per_new_hash(payment_args)
        else
          new_payment = update_payment_as_per_new_hash(pending_refund_payment, new_payment_hash)
        end
        return new_payment
      end

      #
      # Discard old payment and create new one as per the updated hash
      #
      # @param old_payment [Object] [previous payment object]
      # @param new_payment_hash [JSON] [Hash of newly computed payment details]
      #
      # new_payment_hash should contain following keys (not must):
      # grand_total, delivery_charges, discount, billing_amount, payment_mode_savings, total_vat_tax,
      # total_service_tax, total_savings, net_total, payable_total
      #
      # @return [Object] [New payment object]
      #
      def discard_old_payment_and_create_new(old_payment, new_payment_hash)
        payment_dao = PAYMENT_DAO.new(@params)
        new_payment = payment_dao.discard_old_payment_and_create_new(old_payment, {mode: old_payment.mode})
        new_payment = update_payment_as_per_new_hash(new_payment, new_payment_hash)
        return new_payment
      end

      #
      # Update given payment as per the passed computed details hash
      #
      # @param payment [Object] [payment object which is to be updated]
      # @param new_payment_hash [JSON] [Hash of newly computed payment details]
      #
      # new_payment_hash should contain following keys (not must):
      # grand_total, delivery_charges, discount, billing_amount, payment_mode_savings, total_vat_tax,
      # total_service_tax, total_savings, net_total, payable_total
      #
      # @return [Object] [New payment object]
      #
      def update_payment_as_per_new_hash(payment, new_payment_hash)
        payment_dao = PAYMENT_DAO.new(@params)
        new_payment = payment_dao.update_payment(new_payment_hash, payment)
        return new_payment
      end

      #
      # Create new payment object as per the computed details
      #
      # @param new_payment_hash [JSON] [Hash of newly computed payment details]
      #
      # new_payment_hash should contain following keys (not must):
      # grand_total, delivery_charges, discount, billing_amount, payment_mode_savings, total_vat_tax,
      # total_service_tax, total_savings, net_total, payable_total
      #
      # @return [Object] [New payment object]
      #
      def create_new_payment_as_per_new_hash(new_payment_hash)
        payment_dao = PAYMENT_DAO.new(@params)
        new_payment = payment_dao.create_new_payment(new_payment_hash)
        return new_payment
      end

      #
      # Discard any pending REFUND payment of that order
      #
      # @param order [Object] [Order of the user]
      #
      def discard_any_pending_refund_payment(order)
        payment_dao = PAYMENT_DAO.new(@params)
        pending_refund_payment = payment_dao.get_pending_refund_payment(order)
        if pending_refund_payment.present?
          payment_dao.discard_payment(pending_refund_payment)
        end
      end

      #
      # Discard any pending COD payment of that order
      #
      # @param order [Object] [Order of the user]
      #
      def discard_any_pending_cod_payment(order)
        payment_dao = PAYMENT_DAO.new(@params)
        pending_cod_payment = payment_dao.get_pending_cod_payment(order)
        if pending_cod_payment.present?
          payment_dao.discard_payment(pending_cod_payment)
        end
      end

      #
      # Create new payment if not paid completely. This function will be called
      # while recording partial payment of a pending payment
      #
      # @param payment [Object] [payment which was pending and need to be paid]
      # @param args [JSON] [Hash containing paid amount]
      #
      # @return [type] [description]
      def create_new_payment_if_not_paid_completely(payment, args)
        payment_dao = PAYMENT_DAO.new(@params)
        paid_amount = args[:paid_amount]
        return payment if paid_amount.blank?

        payable_total = payment.payable_total
        amount_diff = payable_total.to_d - paid_amount.to_d

        if amount_diff > 0
          # If amount recorded is less than the payable_total, then we need to collect extra money from customer,
          # OR in case of refund, then need to refund more money
          payment = payment_dao.update_payment({
            payable_total: paid_amount.to_d
            }, payment)
          if payment.payment_type == PAYMENT_TYPE.get_id_by_key('PAY')
            new_payment = create_new_pending_payment_with_amount_diff(payment, {payable_total_diff: amount_diff, payment_type: PAYMENT_TYPE.get_id_by_key('PAY')})
          else
            new_payment = create_new_pending_payment_with_amount_diff(payment, {payable_total_diff: amount_diff, payment_type: PAYMENT_TYPE.get_id_by_key('REFUND')})
          end
          return new_payment
        # Nothing to do if complete amount was already paid
        elsif amount_diff == 0
          return payment
        else
          # If amount recorded is greater than the payable_total, then we need to refund amount
          # if already paid. OR we need to collect money if refunded extra
          payment = payment_dao.update_payment({
            payable_total: paid_amount.to_d
            }, payment)
          payment_difference = (-1 * amount_diff)
          if payment.payment_type == PAYMENT_TYPE.get_id_by_key('PAY')
            new_payment = create_new_pending_payment_with_amount_diff(payment, {payable_total_diff: payment_difference, payment_type: PAYMENT_TYPE.get_id_by_key('REFUND')})
          else
            new_payment = create_new_pending_payment_with_amount_diff(payment, {payable_total_diff: payment_difference, payment_type: PAYMENT_TYPE.get_id_by_key('PAY')})
          end
          return new_payment
          #raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::AMOUNT_MORE_THAN_PAYABLE)
        end
      end

      #
      # Create a new PENDING COD payment with all attributes similar to passed payment object
      # but with certain new args (amount diff)
      #
      # args:
      #   - payable_total_diff: Amount diff to be paid extra
      #
      # @param payment [Object] [already existing payment object]
      # @param args [JSON] [Hash containing new args to update]
      #
      # @return [Object] [Newly created payment object]
      #
      def create_new_pending_payment_with_amount_diff(payment, args)
        payment_dao = PAYMENT_DAO.new(@params)
        payable_total_diff = args[:payable_total_diff]
        payment_type = args[:payment_type]
        new_payment = payment.dup
        new_payment = update_payment_as_per_new_hash(new_payment, {
          status: PAYMENT_STATES::PENDING,
          mode: PAYMENT_MODES::CARD_ON_DELIVERY,
          payment_instance: PAYMENT_INSTANCE.get_instance_by_mode(PAYMENT_MODES::CARD_ON_DELIVERY),
          payment_type: payment_type,
          payable_total: payable_total_diff
          })
        return new_payment
      end

      def get_third_party_orders_amount(order)
        third_party_order_service = MarketplaceOrdersModule::V1::ThirdPartyOrderService.instance(@params)
        pending_3p_orders = third_party_order_service.get_placed_or_pending_orders(order.id)
        amount = third_party_order_service.compute_total_amount_for_orders(pending_3p_orders)
        return amount
      end

      def compute_payment_details_as_per_mode(args)
        mode = args[:mode]
        bank = args[:bank]
        billing_amount = args[:billing_amount]
        savings = args[:savings]

        payment_mode_class = MarketplaceOrdersModule::V1::PaymentModesReferenceMap.get_reference_by_mode(mode)
        payment_mode = payment_mode_class.new
        computed_payment_mode_details = PAYMENT_HELPER.compute_payment_mode_details({
          payment_mode: payment_mode,
          bank_id: bank,
          billing_amount: billing_amount,
          savings: savings
        })
        return computed_payment_mode_details
      end

    end # End of class
  end
end
