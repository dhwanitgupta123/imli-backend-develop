require 'imli_singleton'
module MarketplaceOrdersModule

  module V1

    class TimeSlotAvailabilityService < BaseOrdersModule::V1::BaseService
      include ImliSingleton

      #
      # Initialize TimeSlot service with passed params
      #
      def initialize(params = {})
        @params = params
      end

      def check_if_atleast_one_slot_available_for_order?(order)
        time_slot_service = MarketplaceOrdersModule::V1::TimeSlotService.new(@params)
        available_slots = time_slot_service.get_available_slots_for_order(order)
        return true if available_slots.present?

        return false
      end
    end
  end
end
