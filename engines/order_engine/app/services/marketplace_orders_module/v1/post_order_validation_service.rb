require 'imli_singleton'
module MarketplaceOrdersModule

  module V1

    class PostOrderValidationService < BaseOrdersModule::V1::BaseService
      include ImliSingleton

      def initialize(params = {})
        @params = params
      end

      #
      # This function apply all the post order validation
      #
      def apply_post_order_validations(id)
        order_dao = MarketplaceOrdersModule::V1::OrderDao.new(@params)
        order = order_dao.get_order_by_id(id)

        time_slot_availability_service = MarketplaceOrdersModule::V1::TimeSlotAvailabilityService.instance(@params)
        #
        # THIS IS EXCEPTIONAL CASE HENCE PUTTING HERE ONLY, THIS IS TILL CREATING TIME SLOTS IN ALL ZONES
        #
        # This validation check if there is atleast one available slot for order or not
        # if not then return to continue without time_slot workflow
        #
        if time_slot_availability_service.check_if_atleast_one_slot_available_for_order?(order) == false
          return {is_valid: true, order: order}
        end

        order_validators = get_validator_classes

        order_validators.each do |order_validator|
          return {is_valid: false, order: order} if order_validator.apply_validation(order) == false
        end

        return {is_valid: true, order: order}
      end

      #
      # Return all the validator classes which implements apply_validation function
      #
      def get_validator_classes
        return [
          MarketplaceOrdersModule::V1::OrderValidationModule::V1::TimeSlotValidator
        ]
      end
    end
  end
end
