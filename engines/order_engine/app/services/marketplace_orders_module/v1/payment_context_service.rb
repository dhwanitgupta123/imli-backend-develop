require 'imli_singleton'
module MarketplaceOrdersModule

  module V1

    class PaymentContextService < BaseOrdersModule::V1::BaseService
      include ImliSingleton

      #
      # Initialize PaymentContext service with passed params
      #
      def initialize(params = {})
        @params = params
      end

      def get_allowed_payment_modes_for_order(request)
        order_id = request[:id].to_i
        # Hard code gateway value to Razorpay
        gateway = TransactionsModule::V1::GatewayType::RAZORPAY

        order_service = MarketplaceOrdersModule::V1::OrderService.new(@params)
        return order_service.get_allowed_payment_details_of_order({id: order_id, gateway: gateway})
      end

    end
  end
end
