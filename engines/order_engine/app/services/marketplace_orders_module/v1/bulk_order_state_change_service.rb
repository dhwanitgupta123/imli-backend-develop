require 'imli_singleton'
module MarketplaceOrdersModule
  module V1
    class BulkOrderStateChangeService < BaseOrdersModule::V1::BaseService
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @order_dao = MarketplaceOrdersModule::V1::OrderDao.new(@params)
        @order_notification_service = MarketplaceOrdersModule::V1::OrderNotificationService.new(@params)
        @order_context_dao = MarketplaceOrdersModule::V1::OrderContextService.new(@params)
        super
      end

      _RunInTransactionBlock_
      def change_state_and_proccess_data(args)
        orders = @order_dao.get_orders_for_dispatch_list(args[:order][:ids])

        result = apply_pre_processors(args.merge(orders: orders))

        if result[:pre_condition] == false
          return result
        end

        orders = bulk_change_state(args.merge(orders: orders))

        return {
          orders: orders,
          additional_info: apply_post_processors(orders, args[:additional_info])
        }
      end

      #
      # This function change orders state and update context
      #
      def bulk_change_state(args)
        orders = args[:orders]
        return if orders.blank?
        order_panel_service = MarketplaceOrdersModule::V1::OrderPanelService.new(@params)

        orders.each do |order|
          order_panel_service.change_state({ id: order.id, event: args[:order][:event]})
          @order_context_dao.record_order_context(args[:order_context].merge(order_id: order.id))
        end

        return orders
      end

      #
      # This function apply post process to get additional data
      #
      # @params orders [Array Of Orders] orders array
      # @params additional_info [ArrayOfHash] each hash contains task_id, key and params
      #
      def apply_post_processors(orders, additional_info)

        return if orders.blank? || additional_info.blank?

        results = {}

        additional_info.each do |info|
          task = MarketplaceOrdersModule::V1::ChangeStateTaskEnum.get_task_by_id(info[:task])
          results[info[:key]] = task.apply(info[:params].merge(orders: orders))
        end

        return results
      end

      #
      # This function validates if orders current states allow to perform input event on them or not
      # if any one of them fails to pass then it return { pre_condition: false } with orders which are having
      # issue else it returns true
      #
      # @params args [Hash] it contains orders and event to be applied on them
      #
      def apply_pre_processors(args)
        orders = args[:orders]
        return if orders.blank?

        invalid_order_ids = []

        orders.each do |order|
          if !MarketplaceOrdersModule::V1::ModelStates::V1::OrderEvents.is_event_allowed?(order, args[:order][:event])
            invalid_order_ids.push(order.order_id)
          end
        end

        if invalid_order_ids.present?
          return { pre_condition: false, invalid_order_ids: invalid_order_ids }
        end

        return { pre_condition: true }
      end
    end
  end
end
