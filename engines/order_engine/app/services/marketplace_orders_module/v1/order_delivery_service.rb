require 'imli_singleton'
module MarketplaceOrdersModule
  module V1
    class OrderDeliveryService < BaseOrdersModule::V1::BaseService
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @order_dao = MarketplaceOrdersModule::V1::OrderDao.new(@params)
        super
      end

      #
      # This function filter order according to the input query filters
      # filters have top level segregation on the basis of :unallocated = true or false
      # if filters[:unallocated] == true then this will return orders which are not scheduled after applying
      # zone filters if present
      # else this function returns orders after applying zone and time_slot filters according to the presence of
      # filters
      #
      def get_orders_by_filters(filters)

        if filters[:unallocated] == true
          if filters[:zones].present?
            return @order_dao.get_unallocated_orders_by_zones(filters)
          else
            return @order_dao.get_unallocated_orders(filters)
          end
        else
          filters = get_modified_filters(filters)
          if filters[:zones].present?
            return @order_dao.get_aggregated_orders_by_zone_and_time_slots(filters)
          else
            return @order_dao.get_aggregated_orders_by_time_slots(filters)
          end
        end
      end

      private

      #
      # This function extract time and date from input timestamp,
      # add start_date = which is from_date + 1 , end_date = which is to_date - 1 (so that in query we can get all
      # the time slots b/w start and end date) + (delivery_date=from_date and to_time > slot.from_time) +
      # (delivery_date=to_date and from_time < slot.to_time)
      #
      def get_modified_filters(filters)

        if filters[:from_time].blank? || filters[:to_time].blank?
          raise CommonModule::V1::CustomErrors::InvalidArgumentsError.new("From time and To Time are required fields")
        end

        date_time_util = CommonModule::V1::DateTimeUtil

        from_timestamp = date_time_util.convert_epoch_to_datetime(filters[:from_time]) # in ist
        to_timestamp = date_time_util.convert_epoch_to_datetime(filters[:to_time]) # in ist

        if from_timestamp.to_i > to_timestamp.to_i
          raise CommonModule::V1::CustomErrors::InvalidArgumentsError.new("To time should be greater than from time")
        end

        filters[:from_date] = from_timestamp.to_date
        filters[:to_date] = to_timestamp.to_date

        if filters[:from_date] == filters[:to_date]
          filters[:start_date] = filters[:end_date] = filters[:from_date]
        else
          filters[:start_date] = filters[:from_date] + 1.day
          filters[:end_date] = filters[:to_date] - 1.day
        end

        filters[:from_time] = date_time_util.convert_time_to_seconds(from_timestamp)
        filters[:to_time] = date_time_util.convert_time_to_seconds(to_timestamp)

        return filters
      end
    end
  end
end
