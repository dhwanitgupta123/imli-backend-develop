require 'imli_singleton'
module MarketplaceOrdersModule

  module V1

    class CartComputationService < BaseOrdersModule::V1::BaseService
      include ImliSingleton
      #
      # Initialize CART service with passed params
      #
      def initialize(params={})
        @params = params
      end


      #
      # Get convenience charges for the cart
      # Convenience fee is stored as a LADDER w.r.t order number and minimum cart amount
      #
      # * Fetch convenience fees as per the user's order number
      # * Check for cart current total
      # * Return convenience fees accordingly as per the ladder
      #
      def get_convenience_charge(cart)
        cart_total = cart.cart_total
        default_convenience_fees = CACHE_UTIL.read('DEFAULT_CONVENIENCE_FEES') || BigDecimal.new('99.0')

        convenience_charges = default_convenience_fees.to_d
        convenience_cost_hash = get_convenience_cost_for_user(cart.user)

        # Convert hash to JSON
        convenience_cost = JSON.parse(JSON.generate(convenience_cost_hash))
        if convenience_cost.present?
          convenience_charges = BigDecimal.new('0.0') if cart_total.to_d == 0
          convenience_fees_array = convenience_cost["CONVENIENCE_FEES"]
          # Example Convenience fees array:
          # convenience_fees_array = [{min_amount: 1999, fees: 99}, {min_amount: 4999, fees: 49}]
          #
          convenience_hash = fetch_convenience_hash_as_per_cart_amount(convenience_fees_array, cart_total)
          # Convert hash to JSON
          convenience_hash = JSON.parse(JSON.generate(convenience_hash))
          convenience_charges = convenience_hash["fees"].to_d
        end

        return convenience_charges
      end

      #
      # Get convenience cost for user based on its current number of order
      #
      # @param user [Object] [User model object]
      #
      # @return [JSON] [Hash of order cost]
      #
      def get_convenience_cost_for_user(user)
        convenience_cost = {}
        order_dao = MarketplaceOrdersModule::V1::OrderDao.new(@params)
        order_number = order_dao.get_current_number_of_order_of_user(user) 
        convenience_cost_hash = get_convenience_cost(order_number) if order_number.present?
        return convenience_cost_hash
      end

      #
      # Get convenience cost based on order number
      # Fetch convenience cost Array from constants and then iterate over it
      # to find hash as per the order number. Also, convenience_cost is interpolated
      # if its corresponding value is not present in array
      #
      # @param order_number [Integer] [number of Order]
      #
      # @return [JSON] [Hash of order cost]
      #
      def get_convenience_cost(current_order_number)
        convenience_cost_array = CACHE_UTIL.read_array('CONVENIENCE_COST')   # CONVENIENCE_COST is key for cached ConvenienceCost Array
        if convenience_cost_array.present?
          convenience_cost_array = convenience_cost_array.sort{|o1, o2| o1["ORDER_NUMBER"].to_i <=> o2["ORDER_NUMBER"].to_i}
          
          # If current_order_number is greater than the last order number of hash,
          # then directly return the last one
          if current_order_number.to_i >= convenience_cost_array.last["ORDER_NUMBER"].to_i
            return convenience_cost_array.last
          end
          # Iterate over Array to get appropriate cost hash
          convenience_cost_array.each_with_index do |convenience_cost_hash, index|
            o_number = convenience_cost_hash["ORDER_NUMBER"].to_i
            next if current_order_number.to_i > o_number.to_i
            return convenience_cost_hash if current_order_number.to_i == o_number.to_i
            return convenience_cost_array[index-1] if current_order_number.to_i < o_number.to_i
          end
          return convenience_cost_array.last
        end
        # If order cost array not defined, then return default values
        return get_default_convenience_cost
      end

      #
      # Return default convenience cost array as per the default values
      #
      def get_default_convenience_cost
        # If order cost array not defined, then return default values
        default_restricted_amount = CACHE_UTIL.read('DEFAULT_RESTRICTED_AMOUNT') || BigDecimal.new('1999')
        default_convenience_min_amount = CACHE_UTIL.read('DEFAULT_CONVENIENCE_MIN_AMOUNT') || BigDecimal.new('1999')
        default_convenience_fees = CACHE_UTIL.read('DEFAULT_CONVENIENCE_FEES') || BigDecimal.new('99')
        return {
          "RESTRICTED_AMOUNT" => default_restricted_amount.to_d,
          "CONVENIENCE_FEES" => [{
              "min_amount" => default_convenience_min_amount.to_d,
              "fees" => default_convenience_fees.to_d
            }]
        }
      end

      #
      # Get convenience hash as per cart total
      #
      # Iterate over convenience fees array, to find hash as per the given cart total.
      # Logic: if CartTotal lies between min_amount of 2 hashes, then lower one is picked
      # Ex: array = [{min_amount: 1499, fees: 99}, {min_amount: 2499, fees: 0}]
      # cart_total = 1100 gives nil (DEFAULT HASH)
      # cart_total = 1600 gives first hash
      # cart_total = 2800 gives last hash
      #
      # @param convenience_fees_array [Array] [Ladder array of convenience fees]
      # @param cart_total [Decimal] [Total amount of the cart]
      #
      # @return [JSON] [Hash of convenience fees ]
      #
      def fetch_convenience_hash_as_per_cart_amount(convenience_fees_array, cart_total)

        if convenience_fees_array.present? && cart_total.present?
          # Sort in descending order
          sorted_array = convenience_fees_array.sort{|c1,c2| c2["min_amount"].to_d <=> c1["min_amount"].to_d}
          # Search in the array to find a value which is lesser than the cart_total amount
          # Ex: sorted_array = [{min_amount: 2499, fees: 0}, {min_amount: 1499, fees: 99}]
          # cart_total = 1100 gives nil
          # cart_total = 1600 gives last hash (1499)
          # cart_total = 2800 gives first hash (2499)
          #
          required_hash = sorted_array.bsearch{ |c| cart_total.to_d >= c["min_amount"].to_d }
          return required_hash if required_hash.present?
        end
        # If cart amount is less than any min amount specified in the 
        # convenience fees hash, then return default values
        default_convenience_min_amount = CACHE_UTIL.read('DEFAULT_CONVENIENCE_MIN_AMOUNT') || BigDecimal.new('1999.0')
        default_convenience_fees = CACHE_UTIL.read('DEFAULT_CONVENIENCE_FEES') || BigDecimal.new('99.0')
        return {
          "min_amount" => default_convenience_min_amount.to_d,
          "fees" => default_convenience_fees.to_d
        }
      end

      def is_cart_total_valid?(cart)
        custom_error_util = CommonModule::V1::CustomErrors
        content_util = CommonModule::V1::Content

        convenience_cost_hash = get_convenience_cost_for_user(cart.user)
        # Convert hash to JSON
        convenience_cost = JSON.parse(JSON.generate(convenience_cost_hash))
        if convenience_cost.present?
          cart_total = cart.cart_total
          restricted_amount = convenience_cost["RESTRICTED_AMOUNT"]
          if cart_total.to_d > restricted_amount.to_d
            return true
          else
            raise custom_error_util::PreConditionRequiredError.new(content_util::CART_AMOUNT_MUST_BE_MORE % {amount: restricted_amount})
          end
        end
        return true
      end


    end #End of class
  end
end