module InventoryOrdersModule

  module V1

    class InventoryOrderNotificationService < BaseOrdersModule::V1::BaseService

      USER_SERVICE = UsersModule::V1::UserService
      PURCHASE_ORDER_HELPER = InventoryOrdersModule::V1::PurchaseOrderHelper
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      PURCHASE_ORDER_EMAIL_WORKER = CommunicationsModule::V1::Mailers::V1::PurchaseOrderEmailWorker

      #
      # Initialize order service with passed params
      #
      def initialize(params={})
        @params = params
      end

      #
      # Function to automate the mail sending to the Vendor
      #
      def mail_purchase_order(inventory_order, request)
        user_service = USER_SERVICE.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        category_manager_email = USER_SERVICE_HELPER.get_employee_mail(user)
        email_ids = get_vendor_email_id(inventory_order.inventory)
        email_subject = PURCHASE_ORDER_HELPER.get_po_placed_mail_subject(inventory_order.inventory)
        from_email_id = CACHE_UTIL.read('PURCHASE_ORDER_SENDER_EMAIL_ID')
        template_params = PURCHASE_ORDER_HELPER.generate_purchase_order_param(inventory_order, request[:cp_optional])
        pdf_template_path = File.join(Rails.root,
                             'app',
                             'views',
                             'inventory_module',
                             'v1',
                             'purchase_order.liquid'
                            )
        mail_attributes = {
          email_type: EMAIL_TYPE::PLACED_PURCHASE_ORDER[:type], 
          subject: email_subject.to_s,
          to_email_id: email_ids,
          from: from_email_id.to_s,
          cc_email_id: category_manager_email.email_id,
          pdf_template_path: pdf_template_path,
          pdf_needed: true,
          multiple: true
        }
        PURCHASE_ORDER_EMAIL_WORKER.perform_async(mail_attributes.merge(template_params))
      end

      #
      # Function to notify category managers that the PO is stocked
      #
      def notify_purchase_order_verified(inventory_order)
        user_service = USER_SERVICE.new(@params)
        user = user_service.get_user_from_user_id(USER_SESSION[:user_id])
        category_manager_email = USER_SERVICE_HELPER.get_employee_mail(inventory_order.user)
        email_subject = PURCHASE_ORDER_HELPER.get_po_verified_mail_subject(inventory_order)
        cc_email_id = CACHE_UTIL.read('PURCHASE_ORDER_CC_EMAIL_ID')
        from_email_id = CACHE_UTIL.read('PURCHASE_ORDER_SENDER_EMAIL_ID')
        template_params = PURCHASE_ORDER_HELPER.generate_po_invoice_difference_params(inventory_order)
        final_bill_amount = InventoryOrdersModule::V1::InventoryOrderPaymentHelper.get_net_total_of_inventory_order(
          inventory_order.inventory_order_payment)
        pdf_template_path = File.join(Rails.root,
                             'app',
                             'views',
                             'inventory_module',
                             'v1',
                             'po_invoice_conflicts.liquid'
                            )
        mail_attributes = {
          email_type: EMAIL_TYPE::VERIFIED_PURCHASE_ORDER[:type],
          subject: email_subject,
          to_email_id: category_manager_email.email_id,
          cc_email_id: cc_email_id,
          from: from_email_id.to_s,
          category_manager_name: inventory_order.user.first_name,
          stockist: user.first_name,
          purchase_order_no: inventory_order.order_id,
          bill_no: inventory_order.bill_no,
          pdf_template_path: pdf_template_path,
          pdf_needed: true,
          link: CACHE_UTIL.read('EMBER_HOST') + "/supply_chain/warehouses/orders/" + inventory_order.id.to_s,
          net_total: final_bill_amount
        }
        PURCHASE_ORDER_EMAIL_WORKER.perform_async(mail_attributes.merge(template_params))
      end

      #
      # Fucntion to send weekly fill rate report of each vendor
      #
      def send_weekly_fill_rate_report(cumulative_inventory_order_products_hash)

        email_subject = 'Weekly fill rate report ' + Time.zone.now.strftime("%d %b, %Y")
        to_email_id = CACHE_UTIL.read('INVENTORY_REPORT')
        mail_attributes = {
          email_type: EMAIL_TYPE::PURCHASE_ORDER_FILL_RATE[:type],
          subject: email_subject,
          to_email_id: to_email_id,
          csv_needed: true,
          csv_data: cumulative_inventory_order_products_hash
        }
        PURCHASE_ORDER_EMAIL_WORKER.perform_async(mail_attributes)
      end

      #
      # Function to send the dump of purchse order
      #
      def send_purchase_order_dump(po_dummp)
        email_subject = 'Verified Purchase order dump ' + Time.zone.now.strftime("%d %b, %Y")
        to_email_id = CACHE_UTIL.read('PURCHASE_ORDER_SENDER_EMAIL_ID')
        mail_attributes = {
          email_type: EMAIL_TYPE::PURCHASE_ORDER_DUMP[:type],
          subject: email_subject,
          to_email_id: to_email_id,
          csv_needed: true,
          csv_data: po_dummp
        }
        PURCHASE_ORDER_EMAIL_WORKER.perform_async(mail_attributes)
      end

      #
      # Function to send the list of out of stock products
      #
      def send_out_of_stock_products_report(out_of_stock_products)
        email_subject = 'Out of stock products report ' + Time.zone.now.strftime("%d %b, %Y")
        to_email_id = CACHE_UTIL.read('PURCHASE_ORDER_SENDER_EMAIL_ID')
        mail_attributes = {
          email_type: EMAIL_TYPE::OUT_OF_STOCK_PRODUCTS_REPORT[:type],
          subject: email_subject,
          to_email_id: to_email_id,
          csv_needed: true,
          csv_data: out_of_stock_products
        }
        PURCHASE_ORDER_EMAIL_WORKER.perform_async(mail_attributes)
      end

      #
      # Function to return the email id of the vendor
      #
      def get_vendor_email_id(inventory)
        email_ids = inventory.distributor.email_ids
        if email_ids.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{field: 'Email ID of Vendor'})
        else
          return email_ids
        end
      end
    end
  end
end
