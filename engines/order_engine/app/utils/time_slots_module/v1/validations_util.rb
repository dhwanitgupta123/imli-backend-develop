module TimeSlotsModule
  module V1
    module ValidationsUtil
      extend self

      MAX_OFFSET_LIMIT_FOR_RESCHEDULE = CommonModule::V1::Cache.read_int('MAX_OFFSET_LIMIT_FOR_RESCHEDULE') || 360
      MIN_SLOT_OFFSET = CommonModule::V1::Cache.read_int('MIN_SLOT_OFFSET') || 24
      MAX_SLOT_OFFSET = CommonModule::V1::Cache.read_int('MAX_SLOT_OFFSET') || 144
      #
      # This function returns basic validators for time_slot
      #
      #
      def get_slot_availability_validators
        return [
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotStatusValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::PreferredDateValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotDateValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotDayValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotBelongsToGeoClusterValidator
        ]
      end

      #
      # This function returns basic validators for time_slot
      #
      #
      def get_reschedule_slot_availability_validators
        return [
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotStatusValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::OrderDateValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotDateValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotDayValidator,
          MarketplaceOrdersModule::V1::TimeSlots::V1::Validators::TimeSlotBelongsToGeoClusterValidator
        ]
      end

      #
      # Returns starting and final available dates
      #
      def get_start_and_final_dates(min_slot_offset = nil, max_slot_offset = nil)

        min_slot_offset = MIN_SLOT_OFFSET if min_slot_offset.blank?
        max_slot_offset = MAX_SLOT_OFFSET if max_slot_offset.blank?

        date_time_util = CommonModule::V1::DateTimeUtil

        current_time = date_time_util.get_current_time(false)

        start_available_time = current_time + date_time_util.convert_to_seconds(min_slot_offset)
        final_available_time = current_time + date_time_util.convert_to_seconds(max_slot_offset)

        start_available_date = start_available_time.to_date
        final_available_date = final_available_time.to_date

        return {
                  start_available_date: start_available_date,
                  final_available_date:final_available_date,
                  start_available_time: start_available_time,
                  final_available_time: final_available_time
        }
      end

      def get_state_and_final_dates_for_reschedule_delivery(order)

        start_available_time = order.order_date
        final_available_time = order.order_date + MAX_OFFSET_LIMIT_FOR_RESCHEDULE.hours

        return {
          start_available_date: start_available_time.to_date,
          final_available_date:final_available_time.to_date,
          start_available_time: start_available_time,
          final_available_time: final_available_time
        }
      end

      #
      # This function clears data from cache and db of corresponding order, time_slot and preferred_date
      #
      def clear_time_slot_data(args)
        MarketplaceOrdersModule::V1::TimeSlots::V1::CacheTimeSlotManipulator.delete(args)
        MarketplaceOrdersModule::V1::TimeSlots::V1::OrderTimeSlotManipulator.delete(args)
      end
    end
  end
end
