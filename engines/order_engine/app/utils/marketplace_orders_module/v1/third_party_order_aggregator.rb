module MarketplaceOrdersModule
  module V1
    module ThirdPartyOrderAggregator
      extend self

      # 
      # Get third party orders class Array corresponding to 
      # passed hash of third_party_order attributes
      # Request Hash Example:
      #   hash = {membership: {id: 1} }
      # Response Array Example:
      #   array [ <MarketplaceOrdersModule::V1::ThirdPartyOrders::V1::MembershipOrder:: @attributes , @client..> ]
      #
      # @param orders_hash [Hash] [Hash containing third party order details hash]
      #
      # @return [Array] [Array of ThirdPartyOrder class objects]
      #
      def get_third_party_orders(orders_hash)
        return [] if orders_hash.blank?
        third_party_orders = []
        orders_hash.each do |key, value|
          reference_class = get_reference_by_key(key)
          next if reference_class.blank?
          third_party_order = reference_class.new(value)
          third_party_orders.push(third_party_order)
        end
        return third_party_orders
      end

      def get_third_party_order_instance(order)
        return nil if order.blank?
        reference_class = get_reference_by_id(order.reference_type)
        third_party_order = reference_class.new({id: order.reference_id })
      end

      def deserialize_third_party_order_to_hash(order)
        key = get_key_by_id(order.reference_type)
        instance = get_third_party_order_instance(order)
        details = instance.get_details_from_client
        value = details.merge({
          id: order.reference_id,
          label: order.label,
          amount: order.amount
        })
        return {
          key: key,
          value: value
        }
      end

      def get_reference_by_key(key)
        MarketplaceOrdersModule::V1::ThirdPartyOrderReferenceMap.get_reference_by_key(key)
      end

      def get_reference_by_id(id)
        MarketplaceOrdersModule::V1::ThirdPartyOrderReferenceMap.get_reference_by_id(id)
      end

      def get_key_by_id(id)
        MarketplaceOrdersModule::V1::ThirdPartyOrderReferenceMap.get_key_by_id(id)
      end

    end
  end
end
