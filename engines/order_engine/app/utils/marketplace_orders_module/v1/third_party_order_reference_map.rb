module MarketplaceOrdersModule
  module V1
    module ThirdPartyOrderReferenceMap
      extend self

      MAP = [
        { 
          id: 1,
          key: 'membership',
          type: 'USER_MEMBERSHIP',
          reference: 'MarketplaceOrdersModule::V1::ThirdPartyOrders::V1::MembershipOrder'
        }
      ]

      def get_id_by_type(type)
        references = MAP.select {|hash| hash[:type] == type.to_s}
        return references[0][:id] if references.present?
        return nil
      end

      def get_reference_by_key(key)
        references = MAP.select {|hash| hash[:key] == key.to_s}
        return eval(references[0][:reference]) if references.present?
        return nil
      end

      def get_reference_by_id(id)
        references = MAP.select {|hash| hash[:id] == id.to_i}
        return eval(references[0][:reference]) if references.present?
        return nil
      end

      def get_key_by_id(id)
        references = MAP.select {|hash| hash[:id] == id.to_i}
        return references[0][:key] if references.present?
        return nil
      end

    end
  end
end
