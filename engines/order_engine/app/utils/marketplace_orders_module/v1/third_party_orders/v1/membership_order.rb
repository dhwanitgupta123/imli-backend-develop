module MarketplaceOrdersModule
  module V1
    module ThirdPartyOrders
      module V1
        class MembershipOrder < MarketplaceOrdersModule::V1::ThirdPartyOrders::V1::ThirdPartyOrderInterface
          attr_accessor :attributes

          KEY = 'USER_MEMBERSHIP'
          LABEL = 'Membership'

          def initialize(args)
            validate(args)
            @client = UsersModule::V1::MembershipOrderService
            @attributes = {
              id: args[:id]
            }
          end

          def display_label
            LABEL
          end

          def get_hash
            details = get_details_from_client
            return {
              reference_id: membership_id,
              reference_type: reference_key,
              amount: details[:amount] || BigDecimal.new('0'),
              label: details[:label] || ''
            }
          end

          #
          # Call Membership service client to check whether this membership
          # belongs to requester's user_id
          #
          def valid?(user)
            return false if user.blank?
            client_service = get_service
            client_service.is_valid_membership_request?({user: user, id: membership_id})
          end

          #
          # Notify Membership Service CLient about order placed
          #
          def notify_placed_order(order, options)
            client_service = get_service
            begin
              client_service.place_order(order.reference_id, place_order_request(options))
            rescue => e
              @sentry_logger.log_exception(e)
            end
          end

          #
          # Notify Membership Service CLient about order payment success
          #
          def notify_payment_success(order, options)
            client_service = get_service
            begin
              client_service.capture_payment(order.reference_id, place_order_request(options))
            rescue => e
              @sentry_logger.log_exception(e)
            end
          end

          #
          # Notify Membership Service CLient about order cancel
          #
          def notify_cancel_order(order, options)
            client_service = get_service
            payment_done = options[:payment_done] || false
            begin
              client_service.cancel_order(order.reference_id, place_order_request(options).merge(payment_done: payment_done))
            rescue => e
              @sentry_logger.log_exception(e)
            end
          end

          #
          # Get membership details from Membership Service Client
          #
          def get_details_from_client
            client_service = get_service
            begin
              client_service.get_membership_properties(membership_id)
            rescue => e
              @sentry_logger.log_exception(e)
              raise @custom_error_util::RunTimeError.new('Unable to attach Membership')
            end
          end

          def validate(args)
            if args[:id].blank?
              raise @custom_error_util::InvalidArgumentsError.new('Membership id should be present')
            end
          end

          def get_service
            # Making call to MembershipOrder Service V1
            @client.new({version: 'v1'})
          end

          def membership_id
            @attributes[:id]
          end

          def reference_key
            fetch_reference_id_by_display_type(KEY)
          end

          def place_order_request(options)
            order = options[:order]
            user = options[:user]
            payment = options[:payment]

            return {
              order: {
                id: order.id
              },
              payment: {
                id: payment.id
              },
              user: {
                id: user.id
              }
            }
          end

        end # End of class
      end
    end
  end
end
