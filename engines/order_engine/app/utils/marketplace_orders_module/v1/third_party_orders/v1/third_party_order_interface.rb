module MarketplaceOrdersModule
  module V1
    module ThirdPartyOrders
      module V1
        class ThirdPartyOrderInterface

          def initialize(args)
            @custom_error_util = CommonModule::V1::CustomErrors
            @sentry_logger = CommonModule::V1::Logger
          end

          def fetch_reference_id_by_display_type(type)
            MarketplaceOrdersModule::V1::ThirdPartyOrderReferenceMap.get_id_by_type(type)
          end

        end # End of class
      end
    end
  end
end