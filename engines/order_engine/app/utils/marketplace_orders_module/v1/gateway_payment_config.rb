module MarketplaceOrdersModule
  module V1
    module GatewayPaymentConfig
      extend self

      MAP = [
        {
          gateway: TransactionsModule::V1::GatewayType::RAZORPAY,
          allowed_modes: ['PAY_ONLINE', 'CARD_ON_DELIVERY', 'WALLET'] #Array is sorted as per the priority
        },
        {
          gateway: TransactionsModule::V1::GatewayType::CITRUS_WEB,
          allowed_modes: ['DEBIT_CARD', 'CREDIT_CARD', 'NET_BANKING', 'CARD_ON_DELIVERY', 'WALLET']
        }
      ]

      def get_allowed_payment_modes_as_per_gateway(gateway)
        references = MAP.select {|hash| hash[:gateway].to_i == gateway.to_i}
        return references[0][:allowed_modes] if references.present?
        return []
      end


    end
  end
end
