module MarketplaceOrdersModule
  module V1
    module PaymentMode
      module V1
        class PayOnline < BaseMode
          attr_accessor :label
          attr_accessor :savings_value
          attr_accessor :savings_type
          attr_accessor :available_banks

          KEY = 'PAY_ONLINE'
          LABEL = 'Online'

          #
          # Constructor
          # It initializes Feature instance name, experiment and filters classes
          #
          # @param options = {} [JSON] [Hash which might contain some config value in future]
          #
          def initialize(options = {})
            set_parameters(options)
            super
          end

          def label
            return @label if @label.present?
            return LABEL
          end

          def set_parameters(options)
            config = get_payment_config(KEY)
            set_basic_details(config)
            set_savings(config)
          end

        end
      end
    end
  end
end
