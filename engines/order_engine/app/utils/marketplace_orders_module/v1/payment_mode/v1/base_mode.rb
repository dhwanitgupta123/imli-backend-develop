module MarketplaceOrdersModule
  module V1
    module PaymentMode
      module V1
        class BaseMode

          def initialize(params={})

          end

          def set_basic_details(config)
            @label = config['label'].to_s
          end

          #
          # Fetch payment details hash
          # For structure, refer config/payment_details.yml
          #
          # @return [JSON] [Hash containing payment details hash]
          #
          def fetch_payment_details_hash
            cache_util = CommonModule::V1::Cache
            payment_details = cache_util.read_json('PAYMENT_DETAILS')
            if payment_details.blank?
              payment_file_path = OrderEngine::Engine.root.join("config", "payment_details.yml").to_s
              if File.file?(payment_file_path)
                payment_details  = YAML::load_file(payment_file_path)  # Returns as a HASH
                cache_util.write({key: 'PAYMENT_DETAILS', value: JSON.generate(payment_details)})
              end
            end
            return payment_details
          end

          def get_payment_config(key='')
            payment_details = fetch_payment_details_hash
            payment_details[key]
          end

          def set_savings(config)
            default_savings_value = BigDecimal.new('0.0')
            default_savings_type = CommonModule::V1::AmountType.get_id_by_key('RUPEES')
            # Fetch savings details from config
            @savings_value = config['savings_value'].to_d || default_savings_value
            @savings_type = get_amount_type(config['savings_type']) || default_savings_type
          end

          def set_bank_details(config)
            available_banks_config = config['available_banks']
            return [] if available_banks_config.blank?
            available_banks_array = []
            available_banks_config.each do |bank_config|
              bank_id = MarketplaceOrdersModule::V1::BankReferenceMap.get_id_by_key( bank_config['bank'])
              bank_details = MarketplaceOrdersModule::V1::BankReferenceMap.get_bank_details_by_id(bank_id)
              bank_details = bank_details.merge({
                bank: bank_id,
                is_offer: bank_config['is_offer'] || 0,
                discount: bank_config['discount'] || BigDecimal.new('0.0'),
                discount_type: get_amount_type(bank_config['discount_type'])
              })
              available_banks_array.push(bank_details)
            end

            @available_banks = available_banks_array
          end

          def computed_bank_discount_details(net_total, savings_till_now)
            available_banks_with_details = []
            @available_banks.each do |bank|
              discount_details = compute_net_total_and_savings(net_total,
                {
                  amount_value: bank[:discount],
                  amount_type: bank[:discount_type]
                })
              total_savings = discount_details[:savings_amount] + savings_till_now
              bank_details_hash = bank.merge({
                discount_amount: discount_details[:savings_amount],
                net_total: discount_details[:net_total],
                total_savings: total_savings
              })
              available_banks_with_details.push(bank_details_hash)
            end
            return available_banks_with_details
          end

          def compute_payment_details_for_mode_savings(net_total, bank_id = nil)
            mode_details = compute_net_total_and_savings(net_total, {amount_value: @savings_value, amount_type: @savings_type})
            if bank_id.present?
              return compute_payment_details_for_bank(mode_details[:net_total], mode_details[:savings_amount], bank_id)
            end
            return mode_details
          end

          def compute_payment_details_for_bank(net_total, savings_till_now,  bank_id)
            default_payment_details = {net_total: net_total, savings_amount: savings_till_now}
            return default_payment_details if bank_id.blank? || @available_banks.blank?
            # Fetch bank as per the id given
            selected_banks = @available_banks.select { |hash| hash[:bank].to_i == bank_id.to_i}
            selected_bank = selected_banks[0] if selected_banks.present?
            return default_payment_details if selected_bank.blank?

            # Compute bank discount
            discount_details = compute_net_total_and_savings(net_total,
                                                             {
                                                               amount_value: selected_bank[:discount],
                                                               amount_type: selected_bank[:discount_type]
                                                             })
            total_savings = discount_details[:savings_amount] + savings_till_now
            return {net_total: discount_details[:net_total], savings_amount: total_savings}
          end

          def compute_net_total_and_savings(net_total, args)
            amount_value = args[:amount_value] || 0
            amount_type = args[:amount_type]
            net_total = 0 if net_total.blank?
            savings_amount = 0
            case amount_type.to_i
              when get_amount_type('RUPEES')
                savings_amount = amount_value.to_d
              when get_amount_type('PERCENTAGE')
                savings_amount = (amount_value.to_d * net_total )/ 100
              else
                savings_amount = 0
            end
            net_total = net_total.to_d - savings_amount.to_d
            return {net_total: net_total, savings_amount: savings_amount}
          end

          def get_amount_type(amount_type)
            default_amount_type = CommonModule::V1::AmountType.get_id_by_key('RUPEES')
            CommonModule::V1::AmountType.get_id_by_key(amount_type) || default_amount_type
          end


        end
      end
    end
  end
end

