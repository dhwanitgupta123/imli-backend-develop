module MarketplaceOrdersModule
  module V1
    module ChangeStateTaskEnum

      CHANGE_STATE_TASKS = [
        {
          id: 1,
          task: 'MarketplaceOrdersModule::V1::Tasks::Orders::GenerateDispatchOrderList'
        }
      ]

      def self.get_task_by_id(id)
        change_state_task = CHANGE_STATE_TASKS.select {|task| task[:id] == id.to_i}
        return eval(change_state_task[0][:task]) if change_state_task.present?
        return ''
      end
    end
  end
end
