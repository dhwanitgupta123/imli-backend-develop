module MarketplaceOrdersModule
  module V1
    module BankReferenceMap
      extend self

      CACHE_UTIL = CommonModule::V1::Cache

      MAP = [
        {
          id: 1,
          key: 'PAYTM',
          name: 'Paytm Wallet',
          label: CACHE_UTIL.read('PAYTM_LABEL'),
          description: CACHE_UTIL.read('PAYTM_DESCRIPTION'),
          images: CACHE_UTIL.read_array('PAYTM_IMAGE_IDS'),
          terms_and_conditions_label: CACHE_UTIL.read('PAYTM_TERMS_LABEL'),
          terms_and_conditions_url: CACHE_UTIL.read('PAYTM_TERMS_URL'),
          gateway: 'TransactionsModule::V1::GatewayType::PAYTM'
        },
        {
          id: 2,
          key: 'CITRUS_MONEY',
          name: 'Citrus money',
          label: CACHE_UTIL.read('CITRUS_LABEL'),
          description: CACHE_UTIL.read('CITRUS_DESCRIPTION'),
          images: CACHE_UTIL.read_array('CITRUS_IMAGE_IDS'),
          terms_and_conditions_label: CACHE_UTIL.read('CITRUS_TERMS_LABEL'),
          terms_and_conditions_url: CACHE_UTIL.read('CITRUS_TERMS_URL'),
          gateway: 'TransactionsModule::V1::GatewayType::CITRUS_WEB'
        }
      ]

      def get_id_by_key(key)
        references = MAP.select {|hash| hash[:key] == key.to_s}
        return references[0][:id] if references.present?
        return nil
      end

      def get_key_by_id(id)
        references = MAP.select {|hash| hash[:id] == id.to_i}
        return references[0][:key] if references.present?
        return nil
      end

      def get_bank_details_by_id(id)
        references = MAP.select {|hash| hash[:id] == id.to_i}
        bank = references[0] if references.present?
        return {} if bank.blank?
        label = bank[:label] || ''
        name = bank[:name] || ''
        gateway = eval(bank[:gateway])
        description = bank[:description]
        images = get_images_array(bank[:images])
        terms_and_conditions_label = bank[:terms_and_conditions_label]
        terms_and_conditions_url = bank[:terms_and_conditions_url]
        return {label: label, name: name, gateway: gateway, description: description, images: images,
                terms_and_conditions_label: terms_and_conditions_label, terms_and_conditions_url: terms_and_conditions_url}
      end

      #
      # Fetch all size image urls based on the IDs stored in our cache
      #
      # @return [Array] [Array of image hashes with priority and urls]
      #
      def get_images_array(image_ids)
        return ImageServiceModule::V1::ImageServiceHelper.get_images_hash_by_ids(image_ids) if image_ids.present?
        return []
      end

    end
  end
end
