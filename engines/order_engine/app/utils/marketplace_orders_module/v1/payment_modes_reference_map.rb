module MarketplaceOrdersModule
  module V1
    module PaymentModesReferenceMap
      extend self

      MAP = [
        {
          mode: 1,
          key: 'PAY_ONLINE',
          reference: 'MarketplaceOrdersModule::V1::PaymentMode::V1::PayOnline'
        },
        {
          mode: 2,
          key: 'DEBIT_CARD',
          reference: 'MarketplaceOrdersModule::V1::PaymentMode::V1::DebitCard'
        },
        {
          mode: 3,
          key: 'CREDIT_CARD',
          reference: 'MarketplaceOrdersModule::V1::PaymentMode::V1::CreditCard'
        },
        {
          mode: 7,
          key: 'NET_BANKING',
          reference: 'MarketplaceOrdersModule::V1::PaymentMode::V1::NetBanking'
        },
        {
          mode: 4,
          key: 'CARD_ON_DELIVERY',
          reference: 'MarketplaceOrdersModule::V1::PaymentMode::V1::CardOnDelivery'
        },
        {
          mode: 8,
          key: 'WALLET',
          reference: 'MarketplaceOrdersModule::V1::PaymentMode::V1::Wallet'
        }
      ]

      def get_mode_by_key(key)
        references = MAP.select {|hash| hash[:key] == key.to_s}
        return references[0][:mode] if references.present?
        return nil
      end

      def get_reference_by_key(key)
        references = MAP.select {|hash| hash[:key] == key.to_s}
        return eval(references[0][:reference]) if references.present?
        return nil
      end

      def get_reference_by_mode(mode)
        references = MAP.select {|hash| hash[:mode] == mode.to_i}
        return eval(references[0][:reference]) if references.present?
        return nil
      end

      def get_key_by_id(mode)
        references = MAP.select {|hash| hash[:mode] == mode.to_i}
        return references[0][:key] if references.present?
        return nil
      end

    end
  end
end
