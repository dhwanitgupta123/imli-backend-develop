require 'pdf_generator'
require 'csv_generator'
require 'application_logger'
#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Util to generate orders dispatch list
    #
    module OrdersDispatchListGeneratorUtil
      extend self

      #
      # Generate Dispatch list links based on given args
      # Fetch appropriate hash from passed orders model array, and generate
      # pdf or csv as per the passed attributes
      #
      # @param args [Hash] [Hash containing orders, pdf_needed flag, and csv_needed flag]
      #
      # @return [Hash] [Hash containing pdf_link and csv_link]
      #
      def generate_dispatch_list_links(args)
        orders = args[:orders]
        pdf_needed = args[:pdf_needed]
        csv_needed = args[:csv_needed]

        return {} if orders.blank?
        return {} if (pdf_needed.blank? || pdf_needed.to_i == 0) && (csv_needed.blank? || csv_needed.to_i == 0)

        orders_array = get_orders_array_for_dispatch_list(orders)
        pdf_link = pdf_needed.present? && pdf_needed.to_i == 1 ? get_pdf_link(orders_array) : ''
        csv_link = csv_needed.present? && csv_needed.to_i == 1 ? get_csv_link(orders_array) : ''
        return {pdf: pdf_link, csv: csv_link}
      end

      #
      # Get PDF link based on orders array (Array of Orders hash containing slots, user and address hash too)
      #
      # @param orders_array [Array] [Array containing orders hash]
      #
      # @return [String] [Signed url of generated pdf]
      #
      def get_pdf_link(orders_array)
        return '' if orders_array.blank?
        begin
          relative_template_path = CommonModule::V1::TemplateTypes::DISPATCH_LIST_PDF[:relative_location]
          pdf_file_name = CommonModule::V1::Cache.read('DISPATCH_LIST_NAME') || 'orders_dispatch_list'
          pdf_file_params = {orders: orders_array}

          pdf_generator = PdfGenerator.new
          pdf_details = pdf_generator.generate_pdf(pdf_file_name, pdf_file_params, relative_template_path)

          # Upload to S3 and get signed URL
          signed_pdf_expiry_duration = 5.minutes
          signed_dispatch_list_pdf_link = upload_to_s3_and_get_signed_url(pdf_details[:file_path], pdf_details[:filename], signed_pdf_expiry_duration)
          delete_file_from_local(pdf_details[:file_path])

          return signed_dispatch_list_pdf_link
        rescue => e
          CommonModule::V1::Logger.log_exception(e)
          ApplicationLogger.debug('Failed to generate dispatch list pdf ' + e.message)
          return ''
        end
      end

      #
      # Get CSV link based on orders array (Array of Orders hash containing slots, user and address hash too)
      #
      # @param orders_array [Array] [Array containing orders hash]
      #
      # @return [String] [Signed url of generated pdf]
      #
      def get_csv_link(orders_array)
        return '' if orders_array.blank?
        begin
          csv_file_name = CommonModule::V1::Cache.read('DISPATCH_LIST_NAME') || 'orders_dispatch_list'

          csv_generator = CsvGenerator.new(csv_file_name)
          header = ['Sr No.', 'Delivery boy', 'Order ID', 'Zone', 'Slot Date', 'Slot Time', 'Payment Mode', 'Payment Status', 'Payment amount', 'Box Details', 'Box count', 'User Address', 'User Name', 'User Phone Number', 'Remarks and Signature']
          csv_generator.set_headers(header)
          orders_array.each_with_index do |order, index|
            if order[:preferred_slot].present? && order[:preferred_slot][:time_slot].present?
              slot_date = order[:preferred_slot][:slot_date_label]
              slot_time = order[:preferred_slot][:time_slot][:label]
            else
              slot_date = 'NA'
              slot_time = 'NA'
            end
            if order[:order_context].present? && order[:order_context][:box_tags].present?
              box_tags = (order[:order_context][:box_tags] || []).join(' ')
              box_count = order[:order_context][:box_tags].count || 0
            end
            row = [index+1, order[:order_context][:delivery_boy] , order[:order_id], order[:address][:zone], slot_date, slot_time,
                  order[:payment][:display_labels][:mode_display_name], order[:payment][:display_labels][:status_display_name], order[:payment][:payable_total].to_f,
                  box_tags || '', box_count || 0,
                  order[:address_string], order[:user][:first_name], order[:user][:phone_number]]
            csv_generator.push_row(row)
          end
          generated_csv = csv_generator.generate

          # Upload to S3 and get signed URL
          signed_csv_expiry_duration = 5.minutes
          signed_dispatch_list_csv_link = upload_to_s3_and_get_signed_url(generated_csv[:file_path], generated_csv[:filename], signed_csv_expiry_duration)
          delete_file_from_local(generated_csv[:file_path])

          return signed_dispatch_list_csv_link
        rescue => e
          CommonModule::V1::Logger.log_exception(e)
          ApplicationLogger.debug('Failed to generate dispatch list csv ' + e.message)
          return ''
        end
      end

      #
      # Upload file to S3 and get signed url
      #
      # @param file_path [String] [complete path of the file which is to be uploaded]
      # @param file_name [String] [name of the file which is to be uploaded]
      # @param expiry_duration [Time] [expiry time of the generated file, ex: 5.minutes, 2.hours]
      #
      # @return [String] [URL of the S3 where file has been uploaded]
      #
      def upload_to_s3_and_get_signed_url(file_path, file_name, expiry_duration)
        FileServiceModule::V1::FileServiceHelper.upload_file_to_s3_and_get_signed_url(
          file_path, file_name, expiry_duration)
      end

      #
      # Delete file from local store
      #
      # @param file_path [String] [local complete path of the file]
      #
      def delete_file_from_local(file_path)
        begin
          File.delete(file_path)
        rescue => e
          CommonModule::V1::Logger.log_exception(e)
          # Do Nothing. Eat the exception
        end
      end

      #
      # Get array of orders hash which contains slots, user and address details
      #
      # @param orders [Array] [Array of Order model]
      #
      # @return [Array] [Array of order hash]
      #
      def get_orders_array_for_dispatch_list(orders)
        orders_array = []
        orders.each do |order|
          orders_array.push(dispatch_list_params(order))
        end
        orders_array
      end

      #
      # Get dipatch list parameters
      #
      # @param order [Model] [Order model]
      #
      # @return [Hash] [Hash containing order, user and address details]
      #
      def dispatch_list_params(order)
        order_hash = MarketplaceOrdersModule::V1::OrderHelper.get_shot_order_details(order)
        user_hash = UsersModule::V1::UserServiceHelper.get_user_minimal_details(order.user)
        address_hash = AddressModule::V1::AddressResponseDecorator.create_address_hash(order.address)
        # Fetch zone and merge it to address hash
        zone = AddressModule::V1::AddressHelper.get_zone_of_address(order.address)
        address_hash = address_hash.merge({zone: zone})

        payment_hash = MarketplaceOrdersModule::V1::PaymentMapper.get_payment_hash(order.payment)
        order_context_hash = MarketplaceOrdersModule::V1::OrderContextMapper.map_order_context_to_hash(order.order_context)

        order_hash = order_hash.merge({
          address: address_hash,
          address_string: order_address_string(address_hash),
          user: user_hash,
          payment: payment_hash,
          order_context: order_context_hash
        })
        return order_hash
      end

      #
      # Generate combined address string of the order
      #
      def order_address_string(address_hash)
        address_hash[:recipient_name] + ' ' + (address_hash[:address_line1] || '') + ', ' + (address_hash[:address_line2] || '') +
        ', ' + (address_hash[:landmark] || '') + ', ' + address_hash[:pincode] + '. ' + address_hash[:area] + ', ' +
        address_hash[:city]
      end

    end #End of class
  end
end
