module MarketplaceOrdersModule
  module V1
    module RescheduleOrderCommunications
      extend self

      READABLE_TIME_FORMAT = '%I:%M%p'

      def communicate_user(args)
        sms_user(args)
        notify_user(args)
      end

      def sms_user(args)

        order = args[:order]
        user = order.user

        begin
          date_time_util = CommonModule::V1::DateTimeUtil

          updated_slot = args[:updated_slot]
          updated_delivery_date = args[:updated_delivery_date]

          message_attributes = {
            phone_number: user.phone_number,
            first_name: user.first_name,
            order_id: order.order_id,
            from_time: date_time_util.get_readable_time(updated_slot.from_time.in_time_zone, READABLE_TIME_FORMAT),
            to_time: date_time_util.get_readable_time(updated_slot.to_time.in_time_zone, READABLE_TIME_FORMAT),
            delivery_date: updated_delivery_date.to_formatted_s(:long_ordinal)
          }

          sms_type = CommunicationsModule::V1::Messengers::V1::SmsType

          CommunicationsModule::V1::Messengers::V1::SmsWorker.
                                perform_async(message_attributes, sms_type::RESCHEDULE_ORDER)

        rescue => e
          ApplicationLogger.debug('Failed to send SMS for placed order to user : ' + user.id.to_s + ' , ' + e.message)
        end
      end

      def notify_user(args)
        order = args[:order]
        user = order.user
        begin
          date_time_util = CommonModule::V1::DateTimeUtil

          updated_slot = args[:updated_slot]
          updated_delivery_date = args[:updated_delivery_date]

          params = {
            username: 'parse_user_' + user.id.to_s,
            from_time: date_time_util.get_readable_time(updated_slot.from_time.in_time_zone, READABLE_TIME_FORMAT),
            to_time: date_time_util.get_readable_time(updated_slot.to_time.in_time_zone, READABLE_TIME_FORMAT),
            delivery_date: updated_delivery_date.to_formatted_s(:long_ordinal),
            notification_type: CommunicationsModule::V1::Notifications::V1::NotificationType::RESCHEDULE_ORDER[:type],
            order_id: order.order_id,
            first_name: user.first_name
          }

          CommunicationsModule::V1::Notifications::V1::OrderNotificationWorker.perform_async(params)
        rescue => e
          ApplicationLogger.debug('Failed to push notification for placed order to user : ' + user.id.to_s + ' , ' + e.message)
        end
      end
    end
  end
end
