module InventoryOrdersModule
  module V1
    class InventoryOrdersUtil < BaseModule::V1::BaseUtil

      #
      # Function to send the PO fill report to the Sourcing team
      #
      def self.get_po_fill_reports(time_params = {})
        period_in_days = CommonModule::V1::Cache.read_int('PO_FILL_RATE_TIME_PERIOD_IN_DAYS')
        inventory_order_products = get_aggregated_inventory_order_products(time_params, period_in_days)
        cumulative_inventory_order_products = get_cumulative_inventory_order_products(inventory_order_products)
        cumulative_inventory_order_products_hash = get_cumulative_inventory_order_products_hash(cumulative_inventory_order_products)
        inventory_order_notification_service = InventoryOrdersModule::V1::InventoryOrderNotificationService.new
        inventory_order_notification_service.send_weekly_fill_rate_report(cumulative_inventory_order_products_hash)
      end

      #
      # Function to get aggregated inventory order products for Inventory order verified in a time period
      #
      def self.get_aggregated_inventory_order_products(time_params, period_in_days)
        inventory_order_array = get_aggregated_verified_purchase_orders(time_params, period_in_days)
        return [] if inventory_order_array.blank?
        inventory_order_products_array = []
        inventory_order_array.each do |inventory_order|
          inventory_order_products = InventoryOrdersModule::V1::InventoryOrderProductsService.new.get_active_inventory_order_products(inventory_order)
          inventory_order_products.each do |inventory_order_product|
            inventory_order_products_array.push(inventory_order_product)
          end
        end
        return inventory_order_products_array
      end

      #
      # Function to get verified purchase_order_of a particular time period
      #
      def self.get_aggregated_verified_purchase_orders(time_params, period_in_days)
        inventory_order_states = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderStates
        inventory_order_service = InventoryOrdersModule::V1::InventoryOrderService.new
        time_params = verify_time_period_params(time_params, period_in_days)
        inventory_order_verified_state_attributes = inventory_order_states.get_verified_state_attributes_in_interval(time_params)
        inventory_order_array = []
        inventory_order_verified_state_attributes.each do |inventory_order_verified_state_attribute|
          inventory_order_array.push(inventory_order_verified_state_attribute.order_id)
        end
        inventory_order_array = inventory_order_array.uniq
        return [] if inventory_order_array.blank?
        purchase_order_array = []
        inventory_order_array.each do |inventory_order_id|
          purchase_order = inventory_order_service.get_inventory_order(inventory_order_id)
          purchase_order_array.push(purchase_order)
        end
        return purchase_order_array
      end

      #
      # Function to get cumulative quantity of inventory order products corresponding to each vendors
      #
      def self.get_cumulative_inventory_order_products(inventory_order_products)
        return [] if inventory_order_products.blank?
        cumulative_inventory_order_products = {}
        inventory_order_products.each do |inventory_order_product|
          vendor = inventory_order_product.inventory_order.inventory
          brand_pack = inventory_order_product.inventory_brand_pack.brand_pack
          if cumulative_inventory_order_products[vendor].blank?
            cumulative_inventory_order_products[vendor] = {}
          end
          if cumulative_inventory_order_products[vendor][brand_pack].blank?
            cumulative_inventory_order_products[vendor][brand_pack] = {
              quantity_ordered: 0,
              quantity_received: 0,
              mrp_ordered: 0,
              mrp_received: 0,
              net_landing_price_ordered: 0,
              net_landing_price_received: 0,
              count: 0
            }
          end
          inventory_brand_pack_pricing = inventory_order_product.inventory_brand_pack.inventory_pricing
          cumulative_inventory_order_products[vendor][brand_pack][:quantity_ordered] += inventory_order_product.quantity_ordered
          cumulative_inventory_order_products[vendor][brand_pack][:quantity_received] += inventory_order_product.quantity_received
          cumulative_inventory_order_products[vendor][brand_pack][:mrp_ordered] += brand_pack.mrp
          cumulative_inventory_order_products[vendor][brand_pack][:mrp_received] += inventory_order_product.mrp
          cumulative_inventory_order_products[vendor][brand_pack][:net_landing_price_ordered] += (inventory_brand_pack_pricing.net_landing_price + inventory_brand_pack_pricing.net_landing_price * inventory_brand_pack_pricing.vat / 100)
          cumulative_inventory_order_products[vendor][brand_pack][:net_landing_price_received] += (inventory_order_product.cost_price + inventory_order_product.vat_tax_value)
          cumulative_inventory_order_products[vendor][brand_pack][:count] += 1
        end
        return cumulative_inventory_order_products
      end

      #
      # Function to get the hash of CUmulative hash of inventory order products
      #
      def self.get_cumulative_inventory_order_products_hash(cumulative_inventory_order_products)
        fill_rate_of_products = []
        return fill_rate_of_products if cumulative_inventory_order_products.blank?
        vendors = cumulative_inventory_order_products.keys
        header = ["bp_code", "sku", "company_name", "distributor_name", "department_name", "category_name", "sub_category_name", "MRP ordered", "MRP received", "Net Landing Price ordered", "Net landing price received", "cumulative_qty_ordered", "cumulative_qty_received", "fill_rate"]
        fill_rate_of_products.push(header)
        vendors.each do |vendor|
          if vendor.distributor.company.present?
            company_name = vendor.distributor.company.name
          else
            company_name = 'ALL'
          end
          brand_packs = cumulative_inventory_order_products[vendor].keys
          brand_packs.each do |brand_pack|
            count = cumulative_inventory_order_products[vendor][brand_pack][:count]
            quantity_ordered = cumulative_inventory_order_products[vendor][brand_pack][:quantity_ordered]
            quantity_received = cumulative_inventory_order_products[vendor][brand_pack][:quantity_received]
            mrp_ordered = (cumulative_inventory_order_products[vendor][brand_pack][:mrp_ordered] / count).round(2)
            mrp_received = (cumulative_inventory_order_products[vendor][brand_pack][:mrp_received] / count).round(2)
            net_landing_price_ordered = (cumulative_inventory_order_products[vendor][brand_pack][:net_landing_price_ordered] / count).round(2)
            net_landing_price_received = (cumulative_inventory_order_products[vendor][brand_pack][:net_landing_price_received] / count).round(2)
            fill_rate = quantity_received * 100 / quantity_ordered
            bp_code = brand_pack.brand_pack_code
            sku = brand_pack.sku + 'x' + brand_pack.sku_size  + brand_pack.unit
            distributor_name = vendor.distributor.name
            department_name = brand_pack.sub_category.parent_category.department.label
            category_name = brand_pack.sub_category.parent_category.label
            sub_category_name = brand_pack.sub_category.label
            row = [bp_code, sku, company_name, distributor_name, department_name, category_name, sub_category_name, mrp_ordered, mrp_received, net_landing_price_ordered, net_landing_price_received, quantity_ordered, quantity_received, fill_rate]
            fill_rate_of_products.push(row)
          end
        end
        return [{ data: fill_rate_of_products, name: 'fill_rate_report.csv' }]
      end

      #
      # Function to generate dump of PO in period of time and send it to the business team
      #
      def self.get_purchase_order_dump
        po_dump = generate_po_details_sheet
        send_purchase_order_dump(po_dump) if po_dump.present?
      end

      #
      # Function to generate the dump of PO in a period of time
      #
      def self.generate_po_details_sheet
        po_dump_period = CommonModule::V1::Cache.read_int('PURCHASE_ORDER_DUMP_PERIOD')
        to_time = Time.zone.now
        time_params = {
          to_time: to_time,
          from_time: to_time - po_dump_period.days
        }
        purchase_orders = get_aggregated_verified_purchase_orders(time_params, po_dump_period)
        return [] if purchase_orders.blank?

        bill_details_1 = []
        header = ['po number', 'receiveing date', 'bill number', 'Source name', 'vendor code', 'trade discount(%)', 'total amount(Incl Tax)', 'total_amount (Excl Tax)', 'total Tax']
        bill_details_1 << header

        bill_details_2 = []
        header = ['po_number', 'bill_number', 'bp_code', 'sku', 'quantity_ordered', 'quantity_received', 'mrp', 'price_excluding_tax', 'vat(%)', 'scheme_discount(%)','net_landing_price', 'total_amount']
        bill_details_2 << header

        purchase_orders.each do |purchase_order|
          payment = InventoryOrdersModule::V1::InventoryOrderPaymentHelper.get_inventory_order_payment_hash(purchase_order.inventory_order_payment)
          po = InventoryOrdersModule::V1::InventoryOrderHelper.get_short_order_details(purchase_order)
          po_number = po[:order_id]
          receiving_date = Time.zone.at(po[:order_date]).strftime("%d.%m.%Y")
          bill_number = po[:bill_no]
          source_name = po[:inventory][:name]
          vendor_code = po[:inventory][:vendor_code]
          trade_discount = payment[:trade_discount]
          total_amount_excl = payment[:total_cost_price] - payment[:trade_discount_value]
          total_amount_incl = payment[:net_total]
          total_tax = payment[:total_tax]
          row = [po_number, receiving_date, bill_number, source_name, vendor_code, trade_discount, total_amount_incl, total_amount_excl, total_tax]
          bill_details_1 << row
          pops = InventoryOrdersModule::V1::InventoryOrderProductsHelper.get_inventory_order_products_hash(purchase_order.inventory_order_products)
          pops.each do |pop|
            bp_code = pop[:inventory_brand_pack][:brand_pack][:brand_pack_code]
            sku = pop[:inventory_brand_pack][:brand_pack][:sku] + ' ' + pop[:inventory_brand_pack][:brand_pack][:retailer_pack] + 'x' + pop[:inventory_brand_pack][:brand_pack][:sku_size] + pop[:inventory_brand_pack][:brand_pack][:unit]
            quantity_ordered = pop[:quantity_ordered]
            quantity_received = pop[:quantity_received]
            mrp = pop[:mrp]
            price_excluding_tax = pop[:cost_price]
            vat = pop[:vat]
            scheme_discount = pop[:scheme_discount]
            net_landing_price = pop[:cost_price] * (1 + pop[:vat] * 0.01)
            total_amount = pop[:total_cost_price] * (1 + pop[:vat] * 0.01)
            row = [po_number, bill_number, bp_code, sku, quantity_ordered, quantity_received, mrp, price_excluding_tax, vat, scheme_discount, net_landing_price, total_amount]
            bill_details_2 << row
            row =[]
          end
        end
        return [{ data: bill_details_1, name: 'bill_details_1.csv' }, { data: bill_details_2, name: 'bill_details_2.csv' }]
      end

      def self.send_purchase_order_dump(po_dump)
        inventory_order_notification_service = InventoryOrdersModule::V1::InventoryOrderNotificationService.new
        inventory_order_notification_service.send_purchase_order_dump(po_dump)
      end

      #
      # Function to verify the time period
      #
      def self.verify_time_period_params(params, period_in_days)
        from_time = params[:from_time]
        to_time = params[:to_time]
        to_time = Time.zone.now if to_time.blank?
        from_time = to_time - period_in_days.days if from_time.blank?
        if from_time > to_time
          to_time = Time.zone.now
          from_time = to_time - period_in_days.days
        end
        return { from_time: from_time.to_i, to_time: to_time.to_i }
      end
    end
  end
end