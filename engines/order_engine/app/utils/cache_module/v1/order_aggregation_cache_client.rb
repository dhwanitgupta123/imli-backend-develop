#
# This module is responsible to handle all model caches
#
module CacheModule
  #
  # Version1 for validation module
  #
  module V1
    #
    # Order Aggregation cache client
    #
    class OrderAggregationCacheClient < CacheModule::V1::CacheClient

      NAMESPACE = 'MARKETPLACE_ORDERS_MODULE::AGGREGATION::'
      LAST_AGGREGATED_ORDER_ID_KEY = 'LAST_AGGREGATED_ORDER_ID'

      #
      # This function gets last aggregated order id
      #
      # @return [String] order id of last aggregated order
      #
      def self.get_last_aggregated_order
        return get({key: LAST_AGGREGATED_ORDER_ID_KEY}, NAMESPACE)
      end

      #
      # This function set last aggregated order id
      #
      # @param last_aggregated_order_id [String] it contains order id which was last one to be aggregated
      #
      def self.set_last_aggregated_order(last_aggregated_order_id)
        set({key: LAST_AGGREGATED_ORDER_ID_KEY, value: last_aggregated_order_id}, NAMESPACE)
      end

      #
      # This function removes the last aggregated order from redis
      #
      def self.remove_last_aggregated_order
        remove({key: LAST_AGGREGATED_ORDER_ID_KEY}, NAMESPACE)
      end

      ######### PRIVATE ##########

      private

    end
  end
end
