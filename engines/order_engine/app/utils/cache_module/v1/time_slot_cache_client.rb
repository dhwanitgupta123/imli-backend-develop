#
# This module is responsible to handle all model caches
#
module CacheModule
  #
  # Version1 for validation module
  #
  module V1
    #
    # categorization cache client
    #
    class TimeSlotCacheClient < CacheModule::V1::CacheClient

      NAMESPACE = 'TIME_SLOTS'
      ORDER_PREFIX = 'O'
      DATE_PREFIX = 'D'
      TIME_SLOT_PREFIX = 'T'

      #
      # This function return true if time_slot of particular date and order exists in redis
      # else it return false
      #
      # @param args [Hash] it contains order, preferred_date and time_slot
      #
      # @return [Boolean] true if key exists else false
      #
      def self.slot_for_order_exists?(args)

        key = get_key(args)
        return true if CACHE_UTILS.read(key, NAMESPACE).present?
        return false
      end

      #
      # This function return count of orders which has time_slot = given time slot
      # and preferred_delivery_date = slot_date
      #
      # @param time_slot [Model] Time slot model
      # @param slot_date [Date] slot date
      #
      # @return [Integer] count of orders
      #
      def self.get_freezed_time_slots(time_slot, slot_date)
        time_slot_id = TIME_SLOT_PREFIX + time_slot.id.to_s
        slot_date = DATE_PREFIX + slot_date.to_s

        key = time_slot_id + slot_date

        freezed_time_slots = CACHE_UTILS.get_keys_by_pattern(key, NAMESPACE)
        return freezed_time_slots.count
      end

      #
      # This function freeze time_slot for an order with given delivery date
      #
      # @param args [Hash] it contains order, preferred_date and time_slot
      #
      def self.freeze_time_slot_for_order(args)

        key = get_key(args)
        set({key: key, value: 1}, NAMESPACE, args[:ttl])
      end

      #
      # This function return freezed time slot for order
      #
      # @param args [Hash] it contains order, preferred_date and time_slot
      #
      # @return [Integer] value, default 1 value just to confirm key exists
      #
      def self.get_freezed_time_slot(args)
        key = get_key(args)
        get({key: key}, NAMESPACE)
      end

      #
      # This function return ttl for the time_slot for order
      #
      # @param args [Hash] it contains order, preferred_date and time_slot
      #
      # @return [Integer] ttl value
      #
      def self.get_time_slot_ttl(args)
        key = get_key(args)
        CACHE_UTILS.get_ttl(key, NAMESPACE)
      end

      #
      # This function remove the time_slot for order from redis
      #
      # @param args [Hash] it contains order, preferred_date and time_slot
      #
      def self.remove_time_slot(args)
        key = get_key(args)
        CACHE_UTILS.delete(key, NAMESPACE)
      end

      ######### PRIVATE ##########

      private

      #
      # This function will ceate unique key given order, slot_date and time_slot
      # ex => time_slot_id = 1, slot_date = 12-12-2016, order_id=101
      # then return key will be -> T1D12-12-2016O101
      # Concating in this fassion as we may need to find orders with time_slot T and date D
      # or we may need to find order in time slot T
      #
      def self.get_key(args)
        return '' if args[:order].blank? || args[:time_slot].blank? || args[:preferred_date].blank?

        order_id =  ORDER_PREFIX + args[:order].id.to_s
        slot_date = DATE_PREFIX + args[:preferred_date].to_s
        time_slot_id = TIME_SLOT_PREFIX + args[:time_slot].id.to_s

        return time_slot_id + slot_date + order_id
      end
    end
  end
end
