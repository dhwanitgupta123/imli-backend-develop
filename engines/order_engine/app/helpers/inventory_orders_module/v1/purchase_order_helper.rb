#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Helper to create attributes hash for the Purchase order PDF generation
    #
    module PurchaseOrderHelper

      INVENTORY_ORDER_HELPER = InventoryOrdersModule::V1::InventoryOrderHelper
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper
      DISTRIBUTOR_MAPPER = SupplyChainModule::V1::DistributorMapper
      INVENTORY_ORDER_PRODUCTS_HELPER = InventoryOrdersModule::V1::InventoryOrderProductsHelper
      CACHE = CommonModule::V1::Cache
      GENERAL_HELPER = CommonModule::V1::GeneralHelper

      #
      # Function to create attributes for Purchase Order PDF/Mails
      #
      def self.generate_purchase_order_param(inventory_order, cp_optional=false)
        cp_optional = GENERAL_HELPER.string_to_boolean(cp_optional)
        inventory_order_details = INVENTORY_ORDER_HELPER.get_inventory_order_hash(inventory_order)
        @is_inter_state = inventory_order_details[:is_inter_state]
        supplier_address = INVENTORY_MAPPER.get_inventory_address_details(inventory_order.inventory.inventory_address)
        supplier_details = DISTRIBUTOR_MAPPER.map_distributor_to_hash(inventory_order.inventory.distributor)
        product_details = get_product_details(inventory_order_details[:products])
        net_total = product_details[:net_total] + inventory_order_details[:payment][:total_octrai_value] - inventory_order_details[:payment][:trade_discount_value]
        params = {
          cp_optional: cp_optional,
          po_number: inventory_order_details[:order_id],
          shipping_address: inventory_order_details[:address][:shipping_address],
          billing_address: inventory_order_details[:address][:billing_address],
          supplier_address: supplier_address,
          products: product_details[:products_array],
          total_quantity:  product_details[:total_quantity],
          total_octrai_tax: inventory_order_details[:payment][:total_octrai_value],
          total_tax: product_details[:total_tax],
          total_cost_price: product_details[:total_cost_price],
          trade_discount: inventory_order_details[:payment][:trade_discount],
          trade_discount_value: inventory_order_details[:payment][:trade_discount_value],
          grand_total: inventory_order_details[:payment][:grand_total],
          net_total: net_total,
          WAREHOUSE_TIN_NO: CACHE.read('TIN_VAT_NUMBER'),
          IMLI_LOGO_BLACK: CACHE.read('IMLI_LOGO_BLACK'),
          WAREHOUSE_NAME: CACHE.read('SELLER_NAME'),
          WAREHOUSE_ADDRESS_LINE_1: CACHE.read('SELLER_ADDRESS_LINE_1'),
          WAREHOUSE_ADDRESS_LINE_2: CACHE.read('SELLER_ADDRESS_LINE_2'),
          warehouse_cst_no: '',
          supplier_tin_no: supplier_details[:tin_no],
          supplier_cst_no: supplier_details[:cst_no],
          po_date: Time.zone.at(inventory_order_details[:order_date]).strftime("%d.%m.%Y"),
          scheme_discount_present: product_details[:scheme_discount_present]
        }
        return params
      end

      #
      # Function to generate PDF of PO
      #
      def self.generate_purchase_order(inventory_order, cp_optional)
        params = generate_purchase_order_param(inventory_order, cp_optional)
        generate_purchase_order_pdf(params)
      end

      #
      # Function to get the Products hash required for the Purchase order
      #
      def self.get_product_details(products)
        scheme_discount_present = 0
        products_array = []
        total_quantity = 0
        total_tax = BigDecimal('0')
        net_total = BigDecimal('0')
        total_cost_price = BigDecimal('0')
        total_scheme_discount = BigDecimal('0')
        return { products_array: [] } if products.blank?
        products.each do |product|
          
          if product[:scheme_discount] > 0 && scheme_discount_present == 0
            scheme_discount_present = 1
          end
          tax = product[:vat]
          tax_value = product[:vat_value]
          if @is_inter_state
            tax = product[:cst]
            tax_value = product[:cst_value]
          end
          if product[:quantity_received].present?
            quantity = product[:quantity_received]
          else
            quantity = product[:quantity_ordered]
          end
          total_quantity += quantity
          total_value = ((product[:cost_price] - product[:scheme_discount_value]) * quantity).round(2)
          product_detail = {
            brand_pack_code: product[:inventory_brand_pack][:brand_pack][:brand_pack_code],
            distributor_pack_code: product[:inventory_brand_pack][:distributor_pack_code],
            sku: product[:inventory_brand_pack][:brand_pack][:sku],
            sku_size: product[:inventory_brand_pack][:brand_pack][:sku_size],
            unit: product[:inventory_brand_pack][:brand_pack][:unit],
            retailer_pack: product[:inventory_brand_pack][:brand_pack][:retailer_pack],
            mrp: product[:mrp],
            cost_price: product[:cost_price],
            quantity: quantity,
            tax: tax,
            net_landing_price: product[:net_landing_price],
            scheme_discount: product[:scheme_discount],
            tax_value: tax_value,
            value: total_value,
            net_total: product[:net_total]
          }
          total_tax += tax_value
          total_cost_price += total_value
          total_scheme_discount += product[:scheme_discount_value]
          products_array.push(product_detail)
        end
        net_total = total_tax + total_cost_price
        return {
          products_array: products_array,
          total_quantity: total_quantity,
          total_tax: total_tax,
          total_cost_price: total_cost_price,
          net_total: net_total,
          total_scheme_discount: total_scheme_discount,
          scheme_discount_present: scheme_discount_present
        }
      end

      #
      # Function to generate Params which compares the attribute of PO & Invoice
      #
      def self.generate_po_invoice_difference_params(inventory_order)
        inventory_order_product_details = INVENTORY_ORDER_PRODUCTS_HELPER.get_inventory_order_products_hash(inventory_order.inventory_order_products)
        product_details = get_product_details_difference(inventory_order_product_details)
        params = {
          products: product_details
        }
        return params
      end

      #
      # Function to get the Products hash required for the finding diff b/w PO & Invoice
      #
      def self.get_product_details_difference(products)
        products_array = []
        return products_array if products.blank?
        products.each do |product|
          ibp_basic_cost = product[:inventory_brand_pack][:pricing][:net_landing_price]
          vat = product[:inventory_brand_pack][:pricing][:vat]
          landingprice_ibp = ibp_basic_cost * ( 1 + (vat/100))
          product_detail = {
            bp_code: product[:inventory_brand_pack][:brand_pack][:brand_pack_code],
            sku: product[:inventory_brand_pack][:brand_pack][:sku],
            retailer_pack: product[:inventory_brand_pack][:brand_pack][:retailer_pack],
            sku_size: product[:inventory_brand_pack][:brand_pack][:sku_size],
            unit: product[:inventory_brand_pack][:brand_pack][:unit],
            mrp_ibp: product[:inventory_brand_pack][:brand_pack][:mrp],
            mrp_invoice: product[:mrp],
            costprice_ibp: ibp_basic_cost,
            costprice_invoice: product[:cost_price],
            landingprice_ibp: landingprice_ibp.round(2),
            landingprice_invoice: product[:net_landing_price],
            quantity_ordered: product[:quantity_ordered],
            quantity_received: product[:quantity_received]
          }
          products_array.push(product_detail)
        end
        return products_array
      end


      #
      # Function to generate PDF of purchase order and store it locally
      #
      def self.generate_purchase_order_pdf(params)
        file = File.join(Rails.root,
                             'app',
                             'views',
                             'inventory_module',
                             'v1',
                             'purchase_order.liquid'
                            )
        file = File.read(file)
        template = Liquid::Template.parse(file)
        params = ActiveSupport::HashWithIndifferentAccess.new(params)
        template = template.render(params)
        wicked_pdf_instance = WickedPdf.new
        pdf = wicked_pdf_instance.pdf_from_string(template)
        file_path = "#{Rails.root}/public/"
        filename = "#{params[:po_number]}.pdf"
        pdf_path = file_path + filename
        File.open(pdf_path, 'wb') do |file|
          file << pdf
        end
        return { file_path: pdf_path, filename: filename }
      end

      #
      # Get PO Subject when it's placed for email
      #
      def self.get_po_placed_mail_subject(inventory)
        company = inventory.distributor.company
        company_name = ( company.name if company.present? ) || inventory.name
        date = Time.zone.now.strftime("%d.%m.%Y")
        subject = 'Ample PO | ' + company_name + ' | ' + date
        return subject
      end

      #
      # Get PO Subject when it's verified for email
      def self.get_po_verified_mail_subject(inventory_order)
        company = inventory_order.inventory.distributor.company
        company_name = '(' + (( company.name if company.present? ) || 'ALL') + ')'
        return 'Ample PO | ' + inventory_order.inventory.name + company_name + inventory_order.id.to_s + ' | Verified'
      end
    end
  end
end
