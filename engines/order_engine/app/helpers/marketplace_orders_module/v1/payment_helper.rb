module MarketplaceOrdersModule

  module V1

    module PaymentHelper
      CART_HELPER = MarketplaceOrdersModule::V1::CartHelper
      CACHE_UTIL = CommonModule::V1::Cache
      PAYMENT_MODES = MarketplaceOrdersModule::V1::PaymentModes
      PAYMENT_TYPE = MarketplaceOrdersModule::V1::PaymentType
      PAYMENT_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::PaymentStates
      PAYMENT_INSTANCES = MarketplaceOrdersModule::V1::PaymentInstances
      PAYMENT_SERVICE = MarketplaceOrdersModule::V1::PaymentService
      PENDING = 1
      SUCCESS = 3


      #
      # Fetch payment details hash for Invoice
      # Logic:
      # - If payment SUCCESS:
      #     Total: net_total, Paid: net_total, Due: '0'
      #
      # - If payment PENDING:
      #     Total: net_total, Paid: x, Due: y
      #
      #     -computation:
      #       if payment_type == 'PAY'
      #         x = net_total - payable_total, y = payable_total
      #
      #       if payment_type == 'REFUND'
      #         x = net_total + payable_total, y = payable_total
      #
      #
      # @return [JSON] [Hash containing payment details hash]
      #
      def self.compute_payment_details_for_invoice(payment)
        return {} if payment.blank?
        due_hash = {}

        case payment.status.to_i
        when PENDING
          payment_type_key = PAYMENT_TYPE.get_key_by_id(payment.payment_type)
          if payment_type_key.present?
            case payment_type_key.to_s

            # PAYMENT TYPE IS PAY
            when 'PAY'
              paid = payment.net_total - payment.payable_total
              due_hash = {
                amount: payment.payable_total,
                label: 'PAY'
              }

            # PAYMENT TYPE IS REFUND
            when 'REFUND'
              paid = payment.net_total + payment.payable_total
              due_hash = {
                amount: payment.payable_total,
                label: 'REFUND'
              }
            end
          end

          invoice_payment_hash = {
            total: payment.net_total,
            paid: paid,
            due_hash: due_hash,
          }
        when SUCCESS
          invoice_payment_hash = {
            total: payment.net_total,
            paid: payment.net_total,
            due_hash: {
              amount: '0.0',
              label: 'PAY'
            }
          }
        else
          # UNKNOWN STATES (IF ANY)
          invoice_payment_hash = {
            total: payment.net_total,
            paid: payment.net_total - payment.payable_total,
            due_hash: {
              amount: payment.payable_total,
              label: 'PAY'
            }
          }
        end

        return invoice_payment_hash
      end

      #
      # Fetch payment details hash
      # For structure, refer config/payment_details.yml
      #
      # @return [JSON] [Hash containing payment details hash]
      #
      def self.fetch_payment_details_hash
        payment_details = CACHE_UTIL.read_json('PAYMENT_DETAILS')
        if payment_details.blank?
          payment_file_path = OrderEngine::Engine.root.join("config", "payment_details.yml").to_s
          if File.file?(payment_file_path)
            payment_details  = YAML::load_file(payment_file_path)  # Returns as a HASH
            CACHE_UTIL.write({key: 'PAYMENT_DETAILS', value: JSON.generate(payment_details)})
          end
        end
        return payment_details
      end

      #
      # Fetch cart specific payment details
      #
      # @param cart [Object] [Cart model object]
      # @param billing_amount [BigDecimal] [billing amount over which payment mode discounts need to apply]
      #
      # @return [JSON] [Hash containing mode, net_total and total_savings]
      #
      def self.fetch_cart_specific_payment_details(cart, billing_amount, options={})
        preferred_gateway = options[:preferred_gateway] || TransactionsModule::V1::GatewayType::RAZORPAY
        allowed_modes_keys = MarketplaceOrdersModule::V1::GatewayPaymentConfig.get_allowed_payment_modes_as_per_gateway(preferred_gateway)

        details_array = []
        allowed_modes_keys.each do |key|
          mode = MarketplaceOrdersModule::V1::PaymentModesReferenceMap.get_mode_by_key(key)
          payment_mode_class = MarketplaceOrdersModule::V1::PaymentModesReferenceMap.get_reference_by_key(key)
          payment_mode = payment_mode_class.new

          payment_mode_details = compute_payment_mode_details({payment_mode: payment_mode,
                                                               billing_amount: billing_amount,
                                                               savings: cart.cart_savings
                                                              })

          payment_mode_hash = {
            mode: mode.to_i,
            label: payment_mode.label,
            savings: payment_mode.savings_value,
            savings_type: payment_mode.savings_type,
            savings_amount: payment_mode_details[:payment_mode_savings],
            net_total: payment_mode_details[:net_total],
            total_savings: payment_mode_details[:total_savings]
          }
          if payment_mode.available_banks.present?
            payment_mode_hash['available_banks'] = payment_mode.computed_bank_discount_details(payment_mode_details[:net_total],  payment_mode_details[:total_savings])
          end

          details_array.push(payment_mode_hash)
        end
        return details_array
      end

      #
      # Fetch user billing details.
      # These are the details which are before payment is made and it computes
      # user specific discount and apply over grand total amount
      #
      # @param user [Object] [User model object for which billing details need to compute]
      # @param cart [Object] [Cart model object belongs to user]
      #
      # @return [JSON] [Hash containing delivery_charges, grand_total, discount, billing_amount]
      #
      def self.fetch_user_billing_details(args)
        user = args[:user]
        cart = args[:cart]
        order = args[:order]
        cart_payable_total = CART_HELPER.compute_cart_payable_total(cart)
        # Fetch 3rd party orders cost and then add it to
        # get the final grand_total
        payment_computation_service = MarketplaceOrdersModule::V1::PaymentComputationService.new('')
        third_party_orders_amount = payment_computation_service.get_third_party_orders_amount(order) || BigDecimal.new('0')

        # Compute grand_total, billing_amount, and
        grand_total = cart_payable_total[:grand_total] + third_party_orders_amount
        user_specific_discount = compute_users_discount(user)
        billing_amount = grand_total - user_specific_discount

        return {
          delivery_charges: cart_payable_total[:delivery_charges],
          convenience_charges: cart_payable_total[:convenience_charges],
          grand_total: grand_total,
          discount: user_specific_discount,
          billing_amount: billing_amount
        }
      end

      #
      # Compute payment mode details based on passed hash payment-mode-details hash,
      # billing amount and cart savings
      #
      # @return [JSON] [Hash of net_total, total_savings, and payment_mode_savings]
      #
      def self.compute_payment_mode_details(args)
        payment_mode = args[:payment_mode]
        bank_id = args[:bank_id]
        billing_amount = args[:billing_amount]
        cart_savings = args[:savings]

        # Compute final_net_total and payment_mode_savings as per the stored config
        # and current billing_amount
        computed_details = payment_mode.compute_payment_details_for_mode_savings(billing_amount, bank_id)

        # Compute additional discount
        payment_mode_discount = computed_details[:savings_amount]

        payment_mode_savings_value = payment_mode_discount.round(2)
        net_total = (computed_details[:net_total]).round(2)
        total_savings = (cart_savings + payment_mode_discount).round(2)
        # As per the calculation, consider user has to pay the computed net_total
        # amount. In case of any exceptions, this need to be handled in service layer
        payable_total = net_total

        return { net_total: net_total, payable_total: payable_total, total_savings: total_savings, payment_mode_savings: payment_mode_savings_value }
      end

      #
      # Compute users discount (based on its previledges and membership)
      #
      def self.compute_users_discount(user)
        # Need to return Users Discount with respect to its membership
        return BigDecimal.new('0.0')
      end

      #
      # Get array of all supported offline mode of payment
      # Response should be array of Hash<id: mode_id, label: mode_name>
      #
      def self.get_offline_modes
        offline_modes = PAYMENT_MODES.get_offline_payment_modes
        offline_modes_array = []
        offline_modes.each do |mode|
          offline_modes_array.push({
            id: mode,
            label: PAYMENT_MODES.get_payment_mode_display_name(mode)
            })
        end
        return offline_modes_array
      end

      def self.get_offline_modes_with_gateways
        return PAYMENT_MODES.get_offline_modes_with_gateways
      end

      #
      # Get display labels for payment
      #
      # @param payment [Object] [Payment model object]
      #
      # @return [JSON] [Hash of display names of payment mode and payment status]
      #
      def self.get_payment_display_labels(payment)
        return {} if payment.blank?
        mode_display_name = PAYMENT_MODES.get_payment_mode_display_name(payment.mode)
        status_display_name = PAYMENT_STATES.get_payment_state_display_name(payment.status)
        instance_display_name = PAYMENT_INSTANCES.get_display_instance_by_id(payment.payment_instance)
        type_display_name = PAYMENT_TYPE.get_display_label_by_id(payment.payment_type)
        return {
          mode_display_name: mode_display_name,
          status_display_name: status_display_name,
          instance_display_name: instance_display_name,
          type_display_name: type_display_name
        }
      end

      #
      # Create payment hash from payment object
      #
      def self.get_payment_hash(payment)
        payment_hash = MarketplaceOrdersModule::V1::PaymentMapper.get_payment_hash(payment)
        return payment_hash
      end

      #
      # @param order [Object] [Order model object]
      #
      # @return [Object] [Payment object]
      #
      def self.get_orders_payment_as_per_its_mode(order)
        payment_service = PAYMENT_SERVICE.new({})
        payment = payment_service.get_orders_payment_as_per_its_mode(order)
        return payment
      end

    end
  end
end
