#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Map order object to Json
    #
    module OrderDeliveryMapper
      extend self
      extend ActionView::Helpers::DateHelper

      AGO_POSTFIX = ' Ago'
      TO_GO_POSTFIX = ' To Go'

      #
      # This function returns order group by delivery date
      # Ex -> {
      #    "1 Day" -> [ order_hash1, order_hash2 ........ ]
      # }
      #
      #
      def map_order_by_date_bucket(aggregated_orders)
        aggregated_orders_array = fetch_required_details(aggregated_orders)

        date_bucket  = {}

        aggregated_orders_array.each do |order|

         diff = order[:time_diff]

          if date_bucket[diff].present?
            date_bucket[diff].push(order)
          else
            date_bucket[diff] = [order]
          end
        end

        return date_bucket
      end

      #
      # This function map each order to order_hash and return array of modified order hash
      #
      def fetch_required_details(aggregated_orders)
        aggregated_orders_array = []

        aggregated_orders.each do |order|
          aggregated_orders_array.push(get_minimal_hash_for_order(order))
        end

        return aggregated_orders_array
      end

      #
      # This function create order hash required to return in delivery panel
      #
      def get_minimal_hash_for_order(order)

        preferred_slot_hash = MarketplaceOrdersModule::V1::TimeSlotHelper.get_complete_slot_details(order)
        geographical_cluster_hash = ''

        if order.address.present? && order.address.area.geographical_cluster.present? &&
          order.address.area.geographical_cluster.parent_cluster.present?
          geographical_cluster_hash = order.address.area.geographical_cluster.parent_cluster.attributes
        end

        user_hash = UsersModule::V1::UserServiceHelper.get_user_minimal_details(order.user)
        address_hash = AddressModule::V1::AddressResponseDecorator.create_address_hash(order.address)

        preferred_delivery_date = order.preferred_delivery_date.to_date if order.preferred_delivery_date.present?

        diff = CommonModule::V1::Content::UNALLOCATED_ORDER_TAG

        if order.preferred_delivery_date.present? && order.time_slot.present?
          diff = distance_of_time_in_words(get_order_from_time(order), CommonModule::V1::DateTimeUtil.get_current_time(true))
          postfix = AGO_POSTFIX
          if order.preferred_delivery_date > CommonModule::V1::DateTimeUtil.get_current_time(true)
            postfix = TO_GO_POSTFIX
          end
          diff += postfix
        end

        return {
          id: order.id,
          order_id: order.order_id,
          preferred_slot: preferred_slot_hash,
          status: MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates.get_order_state_by_value(order.status),
          preferred_delivery_date: preferred_delivery_date || '',
          geographical_cluster: geographical_cluster_hash,
          user: user_hash,
          address: address_hash,
          time_diff: diff
        }
      end

      #
      # Create timestamp from time_slot.from_time and order.preferred_delivery_date
      #
      def get_order_from_time(order)
        return CommonModule::V1::DateTimeUtil.concat_date_and_time(order.time_slot.from_time, order.preferred_delivery_date)
      end
    end
  end
end
