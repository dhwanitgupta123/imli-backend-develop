#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Map order object to Json
    #
    module ThirdPartyOrderMapper
      extend self

      #
      # Map third party orders to attribute hash
      # key name should be as per the third_party_order type
      #
      # @param third_party_orders [Array] [Array of ThirdPartyOrder model]
      #
      # @return [Hash] [Hash of orders hashes]
      #
      def map_third_party_orders_to_hash(third_party_orders)
        return {} if third_party_orders.blank?
        response = {}
        aggregator = MarketplaceOrdersModule::V1::ThirdPartyOrderAggregator
        third_party_orders.each do |order|
          order_hash = aggregator.deserialize_third_party_order_to_hash(order)
          response[order_hash[:key]] = order_hash[:value]
        end
        return response
      end

    end
  end
end
