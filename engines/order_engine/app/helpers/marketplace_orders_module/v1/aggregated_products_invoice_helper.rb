#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Map order object to Json
    #
    module AggregatedProductsInvoiceHelper

      #
      # map_aggregated_products_invoice_by_quantity_to_array function
      # - Since, aggregated_products Array can have multiple Brand Pack entries with
      #   same/mutliple quantities, hence the duplication is removed with quantities added
      #
      # @param aggregated_products [Array] aggregated brand packs
      #
      # - Contains Hash of following type:
      #  { BrandPack(Model) => quantity }
      #  where key being BP Model and value being quantity of BP model.
      #  There can be multiple same entries, as in:
      #  aggregated_products =
      # [
      #  { BP1(Model) => quantity1 },
      #  { BP1(Model) => quantity1 },
      #  { BP1(Model) => quantity2 },
      #  { BP2(Model) => quantity1 },
      # ]
      #
      # Summary-
      # This function returns an aggregated products array of products to procure
      # i.e. corresponding BrandPacks
      #
      # @return [Array] aggregated_products_array
      #
      def self.map_aggregated_products_invoice_by_quantity_to_array(aggregated_products)
        return [] if aggregated_products.blank?

        totalled_aggregation = Hash.new(0)
        aggregated_products.each do |aggregated_product|
          totalled_aggregation[aggregated_product[:brand_pack]] += aggregated_product[:units_to_procure]
        end

        aggregated_products_array = totalled_aggregation.collect{ |brand_pack, quantity| {
          units_to_procure: quantity,
          brand_pack_code: brand_pack.brand_pack_code,
          display_name: brand_pack.sku,
          sku_size: brand_pack.sku_size,
          unit: brand_pack.unit,
          retail_pack_size: brand_pack.retailer_pack,
          mrp: brand_pack.mrp,
          category_label: brand_pack.sub_category.parent_category.label || 'Others'
          }
        }

        # Sort the aggregation by category ids
        aggregated_products_array_sorted_by_category =
          aggregated_products_array.sort_by{ |hash| hash['category_label'] }

        return aggregated_products_array
      end

      #
      # group_brand_packs_to_category function to map brand_packs to category
      # @param order_products [type] [description]
      #
      # @return [Hash] of the form:
      # {
      #  category_label: [
      #     {
      #       order_product1
      #     },
      #     {
      #       order_product2
      #     },
      #     ..
      #     ...
      #
      #   ],
      #
      #   ..
      #   ...
      #
      # }
      #
      #
      def self.group_brand_packs_to_category(order_products)
        return [] if order_products.blank?
        aggregated_brand_packs_by_category = order_products.group_by {|brand_pack| brand_pack[:category_label] }
        return aggregated_brand_packs_by_category
      end

      #
      # Function to check if the products are available in the inventory and merge the availbility details
      #
      def self.get_products_stock_availability(order_products)
        return if order_products.blank?
        order_products_with_availability = []
        order_products.each do |order_product|
          order_products_with_availability.push(order_product.merge(get_product_stock_availability(order_product)))
        end
        return order_products_with_availability
      end

      #
      # Function to get individual product stock details
      #
      def self.get_product_stock_availability(order_product)
        wbp_service = WarehouseProductModule::V1::WarehouseBrandPackService.new
        availability_details = wbp_service.get_product_stock_availability(order_product)
        blocked = availability_details[:blocked] > 0 ? availability_details[:blocked] : 0
        available = availability_details[:available] + blocked
        units_to_procure = order_product[:units_to_procure]
        jit_count = (available - units_to_procure) > 0 ? 0 : (available > 0 ? (units_to_procure - available) : units_to_procure)
        return {
          available: available,
          jit_count: jit_count
        }
      end
    end
  end
end
