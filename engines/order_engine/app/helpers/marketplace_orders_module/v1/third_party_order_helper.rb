#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Map order object to Json
    #
    module ThirdPartyOrderHelper
      extend self

      def get_active_or_pending_third_party_orders(order)
        return [] if order.blank?
        third_party_order_dao = MarketplaceOrdersModule::V1::ThirdPartyOrderDao.instance(@params)
        third_party_order_dao.get_placed_or_pending_orders_by_order_id(order.id)
      end

      def is_membership_associated_with_order?(order)
        return false if order.blank?
        third_party_order_dao = MarketplaceOrdersModule::V1::ThirdPartyOrderDao.instance(@params)
        membership_orders = third_party_order_dao.get_membership_orders_associated_with_ample_order(order.id)
        return true if membership_orders.present?
        return false
      end

    end
  end
end