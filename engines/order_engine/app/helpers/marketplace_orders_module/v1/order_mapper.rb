#
# Module to handle all the functionalities related to Marketplace Orders
#
module MarketplaceOrdersModule
  #
  # Version1 for Marketplace orders module
  #
  module V1
    #
    # Map order object to Json
    #
    module OrderMapper

      USER_RESPONSE_DECORATOR = UsersModule::V1::UserResponseDecorator
      ADDRESS_RESPONSE_DECORATOR = AddressModule::V1::AddressResponseDecorator
      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper
      ORDER_PRODUCTS_DAO = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsDao
      THIRD_PARTY_ORDER_MAPPER = MarketplaceOrdersModule::V1::ThirdPartyOrderMapper
      THIRD_PARTY_ORDER_HELPER = MarketplaceOrdersModule::V1::ThirdPartyOrderHelper

      #
      # map_minimal_order_to_hash function
      # @param order [Order Model] Order for which minimal details hash is required
      #
      # @return [Hash] response
      #
      # ****************************************
      #               DEPRECATED
      # ****************************************
      # Use map_order_to_hash and pass options hash as per your requirement
      # 
      def self.map_minimal_order_to_hash(order)
        {
          id: order.id,
          order_id: order.order_id,
          order_date: CommonModule::V1::DateTimeUtil.convert_date_time_to_epoch(order.order_date),
          status: get_state_hash(order)
        }
      end

      #
      # Map order to Hash based on passed options
      #
      # @param order [Object] [Order model object]
      # @param options = {} [Hash] [Hash containing filter args]
      #
      # options hash can contain:
      #   * address_details: true (to include address hash)
      #   * user_details: true (to include user hash)
      #   * cart_details: true (to include cart hash)
      #   * slot_details: true (to include time slot hash)
      #   * payment_details: true (to include payment hash)
      #   * state_attributes_details: true (to include state attributes hash)
      #
      # @return [Hash] [Hash containing order details along with other dependent details]
      #
      def self.map_order_to_hash(order, options = {})
        return {} if order.blank?
        result = map_minimal_order_to_hash(order)
        result[:address] = order_address_hash(order) if options[:address_details].present?
        result[:user] = order_user_hash(order) if options[:user_details].present?
        result[:preferred_slot] = order_slot_hash(order) if options[:slot_details].present?
        result[:cart] = order_cart_hash(order) if options[:cart_details].present?
        result[:state_attributes] = order_state_attributes_hash(order) if options[:state_attributes_details].present?
        result[:payment] = order_payment_hash(order) if options[:payment_details].present?
        result[:context] = order_context_hash(order) if options[:context_details].present?
        return result
      end

      def self.get_state_hash(order)
        order_states = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
        order_states.get_order_state_by_value(order.status)
      end

      def self.order_address_hash(order)
        address_decorator = AddressModule::V1::AddressResponseDecorator
        address_decorator.create_address_hash(order.address)
      end

      def self.order_user_hash(order)
        user_decorator = UsersModule::V1::UserResponseDecorator
        user_decorator.get_user_details_for_order(order.user)
      end

      def self.order_slot_hash(order)
        time_slot_helper = MarketplaceOrdersModule::V1::TimeSlotHelper
        time_slot_helper.get_complete_slot_details(order)
      end

      def self.order_cart_hash(order)
        cart_helper = MarketplaceOrdersModule::V1::CartHelper
        cart_helper.get_cart_hash(order.cart)
      end

      def self.order_payment_hash(order)
        payment_helper = MarketplaceOrdersModule::V1::PaymentHelper
        payment_helper.get_payment_hash(order.final_payment)
      end

      def self.order_state_attributes_hash(order)
        order_state_transition_helper = MarketplaceOrdersModule::V1::OrderStateTransitionHelper
        order_state_transition_helper.get_complete_state_attributes_hash(order.state_attributes)
      end

      def self.order_context_hash(order)
        MarketplaceOrdersModule::V1::OrderContextMapper.map_order_context_to_hash(order.order_context)
      end

      #
      # map_detailed_order_to_hash function
      # @param order [Order Model] Order for which more order details hash is required
      #
      # @return [Hash] response
      #
      def self.map_detailed_order_to_hash(order)
        user = USER_RESPONSE_DECORATOR.get_user_details(order.user)
        address = ADDRESS_RESPONSE_DECORATOR.create_address_hash(order.address)
        {
          id: order.id,
          order_id: order.order_id,
          ship_by: order.ship_by,
          status: order.status,
          user: user,
          address: address

        }
      end

      def self.map_multiple_orders_to_array(orders, options={})
        orders_array = []
        orders.each do |order|
          orders_array.push(map_order_to_hash(order, options))
        end
        orders_array
      end

      #
      # map_aggregated_products_to_array function
      # @param aggregated_order_products [Array] aggregated order products
      #  - Contains Hash of following type:
      #  { MPSP(Model) => quantity }
      #  where key being MPSP Model and value being quantity of MPSP model.
      #  There can be multiple same entries, as in:
      # aggregated_order_products =
      # [
      #  { MPSP1(Model) => quantity1 },
      #  { MPSP1(Model) => quantity1 },
      #  { MPSP1(Model) => quantity2 },
      #  { MPSP2(Model) => quantity1 },
      # ]
      #
      # Summary-
      # This function returns an aggregated products array of products to procure
      # i.e. corresponding BrandPacks
      #
      # @return [Array] aggregated_products_array
      #
      def self.map_aggregated_products_to_array(aggregated_order_products)
        aggregated_products_array = []
        return aggregated_products_array if aggregated_order_products.blank?

        aggregated_order_products.each do |aggregated_order_product, quantity|
          corresponding_mpsp_mpbp_entries = aggregated_order_product.marketplace_selling_pack_marketplace_brand_packs

          corresponding_mpsp_mpbp_entries.each do |mpsp_mpbp_entry|
            mpbp_detail = get_mpsp_detail_hash(mpsp_mpbp_entry, quantity)
            aggregated_products_array.push(mpbp_detail)
          end
        end

        return aggregated_products_array
      end

      #
      # get_mpsp_detail_hash function to give hash of a mpsp with quantity computed
      #
      # @param mpsp_mpbp_entry [MPSP_MPBP Model] entry of marketplace_selling_pack_marketplace_brand_pack
      # @param quantity [String] quantity ordered of parent MPSP
      #
      # @return [response]
      #
      def self.get_mpsp_detail_hash(mpsp_mpbp_entry, quantity)
        quantity_ordered = quantity
        units_to_procure = quantity.to_i * (mpsp_mpbp_entry.quantity).to_i
        response = {
          brand_pack: mpsp_mpbp_entry.marketplace_brand_pack.brand_pack,
          units_to_procure: units_to_procure
        }
        return response
      end

      def self.map_orders_to_array(orders)
        orders_array = []
        orders.each do |order|
          orders_array.push(ORDER_HELPER.get_minimal_order_details(order))
        end
        orders_array
      end

      def self.map_orders_by_delivery_fiters_to_array(orders)
        orders_array = []
        orders.each do |order|
          orders_array.push(ORDER_HELPER.get_order_delivery_hash(order))
        end
        orders_array
      end

      def self.map_order_to_array(order)
        [ map_detailed_order_to_hash(order) ]
      end

      #
      # Map Array of Order hashes with complete details
      #
      # @param order [Array] [Array of Order model object]
      #
      # @return [Array] [Array of Order hash]
      #
      def self.create_orders_array_with_complete_details(orders)
        return [] if orders.blank?
        orders_array = []
        orders.each do |order|
          orders_array.push(get_order_details(order) )
        end
        return orders_array
      end

      #
      # map_products_to_array function
      # @param order_products [Array] order products
      #
      # Summary-
      # This function returns an aggregated products array of products to procure
      # i.e. corresponding BrandPacks
      #
      # @return [Array] aggregated_products_array
      #
      def self.map_products_to_array(order_products)
        aggregated_products_array = []
        return aggregated_products_array if order_products.blank?
        # Eager load order products till master products
        order_products = ORDER_PRODUCTS_DAO.load_order_products_till_master_products(order_products)

        order_products.each do |order_product|
          corresponding_mpsp_mpbp_entries = order_product.marketplace_selling_pack.marketplace_selling_pack_marketplace_brand_packs

          corresponding_mpsp_mpbp_entries.each do |mpsp_mpbp_entry|
            mpbp_detail = get_mpsp_detail_hash(mpsp_mpbp_entry, order_product.quantity)
            aggregated_products_array.push(mpbp_detail)
          end
        end
        return aggregated_products_array
      end

      def self.edit_order_time_slot_response_hash(order)
        order_hash = ORDER_HELPER.get_order_details(order)
        return {
          order: order_hash
        }
      end

      def self.edit_order_address_response_hash(order)
        order_hash = ORDER_HELPER.get_order_details(order)
        return {
          order: order_hash
        }
      end

      def self.edit_order_cart_response_hash(order)
        order_hash = ORDER_HELPER.get_order_details(order)
        return {
          order: order_hash
        }
      end

      def self.get_third_party_orders_hash(order)
        return [] if order.blank?
        third_party_orders = THIRD_PARTY_ORDER_HELPER.get_active_or_pending_third_party_orders(order)
        orders_hash = THIRD_PARTY_ORDER_MAPPER.map_third_party_orders_to_hash(third_party_orders)
      end

    end # End of class
  end
end
