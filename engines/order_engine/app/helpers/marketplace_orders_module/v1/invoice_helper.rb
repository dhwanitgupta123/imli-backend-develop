module MarketplaceOrdersModule

  module V1

    module InvoiceHelper

      ADDRESS_DECORATOR = AddressModule::V1::AddressResponseDecorator
      USER_SERVICE_HELPER = UsersModule::V1::UserServiceHelper
      ORDER_HELPER = MarketplaceOrdersModule::V1::OrderHelper
      ORDER_MAPPER = MarketplaceOrdersModule::V1::OrderMapper
      CART_HELPER = MarketplaceOrdersModule::V1::CartHelper
      CART_DAO = MarketplaceOrdersModule::V1::CartDao
      CACHE_UTIL = CommonModule::V1::Cache
      PAYMENT_HELPER = MarketplaceOrdersModule::V1::PaymentHelper
      DATE_TIME_UTIL = CommonModule::V1::DateTimeUtil

      #
      # Get invoice details for invoice generation
      #
      def self.get_invoice_hash_for_invoice_generation(order)
        return {} if order.blank?
        user = order.user
        user_hash = USER_SERVICE_HELPER.get_user_details(order.user)

        order_hash = ORDER_HELPER.get_order_details(order)
        cart_hash = CART_HELPER.get_cart_freezed_details(order.cart)
        third_party_orders_hash = get_third_party_orders_hash_for_invoice(order)

        products_hash = update_order_product_specification(order)
        cart_hash = cart_hash.merge({total_products: products_hash[:total_quantity]})
        address_hash = ADDRESS_DECORATOR.create_address_hash(order.address)
        primary_email = USER_SERVICE_HELPER.get_primary_mail(order.user)

        order_hash = ORDER_HELPER.get_order_details(order)
        time_slot_hash = get_formatted_slot_details(order)

        order_hash.merge!({
          preferred_slot: time_slot_hash
          })

        # Prints in format: Thursday, 20 November 2015
        order_date = DATE_TIME_UTIL.convert_epoch_to_time(order_hash[:order_date]).strftime("%A, %d %B %Y")

        payment = PAYMENT_HELPER.get_orders_payment_as_per_its_mode(order)
        invoice_payment_hash = PAYMENT_HELPER.compute_payment_details_for_invoice(payment)

        # Boolean flag for making a check in Liquid template
        is_preferred_slot_present = order_hash[:preferred_slot].blank? ? false : true
        order_hash[:preferred_slot].merge!({
          is_preferred_slot_present: is_preferred_slot_present
          })

        invoice_hash = {
          id: order.id,
          order_id: order.order_id,
          order_status: order.status,
          first_name: user.first_name,
          cart: cart_hash,
          products: products_hash[:order_products],
          order: order_hash,
          payment: order_hash[:payment],
          invoice_payment_hash: invoice_payment_hash,
          address: address_hash,
          user: user_hash,
          email_id: primary_email.email_id,
          order_date: order_date,
          extras: third_party_orders_hash
        }

        return invoice_hash
      end

      #
      # Update Order Product specification.
      # Iterate over order products and compute its effective pricing based
      # on its quantity
      #
      def self.update_order_product_specification(order)
        cart_dao = CART_DAO.new
        order_products_array = []
        total_quantity = 0
        cart = order.cart
        return [] if cart.blank? || cart.order_products.blank?
        # Get active products from cart
        order_products = cart_dao.active_products_in_cart(cart)
        # Iterate over Order products and fetch all displayable entities
        order_products.each do |order_product|
          order_product_hash = compute_order_product_pricing_details(order_product)
          total_quantity += order_product.quantity
          order_products_array.push(order_product_hash)
        end
        return { order_products: order_products_array, total_quantity: total_quantity }
      end

      def self.get_formatted_slot_details(order)
        return {} if order.blank? || order.preferred_delivery_date.blank? || order.time_slot.blank?

        preferred_slot_hash = {
          slot_date: order.preferred_delivery_date.strftime("%A, %d %B %Y"),
          time_slot: get_formatted_time_slot_hash(order.time_slot)
        }
        return preferred_slot_hash
      end

      #
      # Get formatted time slot hash
      #
      # @param slot [Object] [Time slot model object]
      #
      # @return [JSON] [Hash of relevant key-values(formatted) of time slot]
      #
      def self.get_formatted_time_slot_hash(time_slot)
        return {} if time_slot.blank?

        time_slot_hash = {
          id: time_slot.id,
          from_time: DATE_TIME_UTIL.get_readable_time(time_slot.from_time.in_time_zone, '%I:%M%P'),
          to_time: DATE_TIME_UTIL.get_readable_time(time_slot.to_time.in_time_zone, '%I:%M%P'),
          label: time_slot.label
        }
        return time_slot_hash
      end

      #
      # Compute single order product pricing detail (based on quantity and ladder pricing)
      #
      def self.compute_order_product_pricing_details(order_product)
        cart_dao = CART_DAO.new
        quantity = order_product.quantity
        mpsp = order_product.marketplace_selling_pack
        order_product_details = cart_dao.get_savings_and_total_details_for_mpsp(order_product)
        # Compute savings including additional discount
        savings = order_product_details[:total_savings] || BigDecimal.new('0.0')
        additional_discount = order_product_details[:additional_discount] || BigDecimal.new('0.0')
        final_savings = savings + additional_discount
        return {
          id: mpsp[:id],
          quantity: quantity,
          display_name: mpsp[:display_name],
          display_pack_size: mpsp[:display_pack_size],
          total_mrp: order_product_details[:total_mrp],
          total_savings: savings,
          total_selling_price: order_product_details[:total_selling_price],
          additional_discount: additional_discount,
          final_savings: final_savings,
          final_price: order_product_details[:final_price]
        }
      end

      def self.get_third_party_orders_hash_for_invoice(order)
        return {} if order.blank?
        third_party_orders_json = {}
        third_party_orders_hash = ORDER_MAPPER.get_third_party_orders_hash(order)
        if third_party_orders_hash.blank?
          third_party_orders_json[:membership] = {}
        else
          third_party_orders_json[:membership] = JSON.parse(third_party_orders_hash['membership'].to_json)
        end

        # Boolean flag for making a check in Liquid template for Membership being part of Order
        is_membership_ordered = third_party_orders_json[:membership].blank? ? false : true
        third_party_orders_json[:membership].merge!({
          is_membership_ordered: is_membership_ordered
          })
        return third_party_orders_json
      end

    end
  end
end
