module MarketplaceOrdersModule

  module V1

    module PaymentMapper
      CART_HELPER = MarketplaceOrdersModule::V1::CartHelper
      CACHE_UTIL = CommonModule::V1::Cache
      PAYMENT_MODES = MarketplaceOrdersModule::V1::PaymentModes
      PAYMENT_TYPE = MarketplaceOrdersModule::V1::PaymentType
      PAYMENT_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::PaymentStates
      PAYMENT_INSTANCES = MarketplaceOrdersModule::V1::PaymentInstances
      PAYMENT_SERVICE = MarketplaceOrdersModule::V1::PaymentService
      TRANSACTION_HELPER = TransactionsModule::V1::TransactionHelper

      #
      # Get display labels for payment
      #
      # @param payment [Object] [Payment model object]
      #
      # @return [JSON] [Hash of display names of payment mode and payment status]
      #
      def self.get_payment_display_labels(payment)
        return {} if payment.blank?
        mode_display_name = PAYMENT_MODES.get_payment_mode_display_name(payment.mode)
        status_display_name = PAYMENT_STATES.get_payment_state_display_name(payment.status)
        instance_display_name = PAYMENT_INSTANCES.get_display_instance_by_id(payment.payment_instance)
        type_display_name = PAYMENT_TYPE.get_display_label_by_id(payment.payment_type)
        return {
          mode_display_name: mode_display_name,
          status_display_name: status_display_name,
          instance_display_name: instance_display_name,
          type_display_name: type_display_name
        }
      end

      #
      # Create payment hash from payment object
      #
      def self.get_payment_hash(payment)
        return {} if payment.blank?
        display_labels = get_payment_display_labels(payment)
        payment_hash = {
          id: payment.id,
          mode: payment.mode,
          bank_id: payment.bank_id,
          payment_instance: payment.payment_instance,
          status: payment.status,
          total_vat_tax: payment.total_vat_tax.round(2),
          total_service_tax: payment.total_service_tax.round(2),
          delivery_charges: payment.delivery_charges.round(2),
          convenience_charges: payment.convenience_charges.round(2),
          grand_total: payment.grand_total.round(2),
          discount: payment.discount.round(2),
          billing_amount: payment.billing_amount.round(2),
          payment_mode_savings: payment.payment_mode_savings.round(2),
          net_total: payment.net_total.round(2),
          payable_total: payment.payable_total.round(2),
          payment_type: payment.payment_type,
          total_savings: payment.total_savings.round(2),
          display_labels: display_labels,
          credit_amount: payment.ample_credit_amount.round(2)
        }
        return payment_hash
      end

      #
      # Get payment hash along with transaction hash related to it
      #
      # @param payment [Object] [Payment model object which is to be mapped to hash]
      #
      # @return [JSON] [Hash containing payment details and transaction hash within it]
      #
      def self.get_payment_hash_with_transaction_details(payment)
        payment_details = get_payment_hash(payment)
        payment_details = payment_details.merge({
          transaction: TRANSACTION_HELPER.get_transaction_details_for_payment(payment)
        })
        return payment_details
      end

      #
      # Map payments to Array of payment hash
      #
      def self.map_payments_to_array(payments)
        return [] if payments.blank?
        payments_array = []
        payments.each do |payment|
          payments_array.push(get_payment_hash(payment))
        end
        return payments_array
      end

      #
      # Map payments to Array of payment hash containing transaction details too
      #
      def self.map_payments_to_array_with_transaction_details(payments)
        return [] if payments.blank?
        payments_array = []
        payments.each do |payment|
          payment_details = get_payment_hash_with_transaction_details(payment)
          payments_array.push(payment_details)
        end
        return payments_array
      end


    end
  end
end
