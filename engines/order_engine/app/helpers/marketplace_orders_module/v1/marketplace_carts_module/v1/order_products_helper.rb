#
# Module to handle all the functionalities related to orders
#
module MarketplaceOrdersModule
  #
  # Version1 for orders module
  #
  module V1

    module MarketplaceCartsModule
      module V1
        # Model for order_products table
        module OrderProductsHelper

          def self.get_freezed_pricing_of_order_product_in_mpsp_pricing_format(order_product)
            marketplace_selling_pack_helper = MarketplaceProductModule::V1::MarketplaceSellingPackHelper
            return {} if order_product.blank?
            mpsp = order_product.marketplace_selling_pack
            quantity = order_product.quantity
            return {} if quantity == 0
            total_selling_price = order_product.final_price
            total_selling_price_per_qty = (total_selling_price / quantity).round(2)
            additional_discount_per_qty = (order_product.additional_discount / quantity).round(2)
            total_savings = order_product.savings + additional_discount_per_qty

            pricing_hash = {
              mrp: order_product.mrp,
              base_selling_price: total_selling_price_per_qty,
              savings: total_savings,
              discount: order_product.discount
            }

            if marketplace_selling_pack_helper.is_ladder_active(mpsp)
              ladder_pricing_hash = [
                {
                  quantity: order_product.quantity,
                  selling_price: total_selling_price_per_qty,
                  additional_savings: BigDecimal.new('0.0'),
                  checkpoint: true
                }
              ]
              pricing_hash['ladder_pricing'] = ladder_pricing_hash
            end
            return pricing_hash
          end

          def self.get_inactive_and_out_of_stock_order_products(product_change_details)
            out_of_stock_array = product_change_details[:out_of_stock] || []
            product_inactive_array = product_change_details[:product_inactive] || []
            return out_of_stock_array | product_inactive_array
          end

          def self.generate_products_change_message(product_change_details)
            return '' if product_change_details.blank?
            price_change_array = product_change_details[:price_change]
            out_of_stock_array = product_change_details[:out_of_stock]
            product_inactive_array = product_change_details[:product_inactive]

            # Can use the order_products arrays in future to specify exact product name
            # For now, sending back only constant strings
            str = ''
            str += generate_price_change_string if price_change_array.present?
            str += ' Also, ' if str.present?
            str += generate_products_inactive_string if (product_inactive_array.present? || out_of_stock_array.present?)
            str += ' ' + CommonModule::V1::Content::CART_VERIFICATION_MESSAGE if str.present?
            str = str.capitalize if str.present?

            return str
          end

          def self.generate_price_change_string
            return CommonModule::V1::Content::PRICES_CHANGE_MESSAGE
          end

          def self.generate_products_inactive_string
            return CommonModule::V1::Content::PRODUCT_STATE_CHANGE_MESSAGE
          end

        end
      end
    end
  end
end
