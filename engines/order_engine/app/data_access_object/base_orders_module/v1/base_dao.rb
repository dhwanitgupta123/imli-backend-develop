#
# Module to handle all the functionalities related to Dao's
#
module BaseOrdersModule
  #
  # Version1 for base module
  #
  module V1
    #
    # Base module to inject generic utils
    #
    class BaseDao

      PAGINATION_UTIL = CommonModule::V1::Pagination
      ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT_UTIL = CommonModule::V1::Content

      def initialize(params='')
        @custom_error_util = CommonModule::V1::CustomErrors
        @content = CommonModule::V1::Content
      end

      #
      # get the mode by id
      #
      # @param id [Integer] model id
      # @param model [Model] model
      #
      # @return [Model]  model if model exists else return nil
      #
      def get_model_by_id(model, id)
        model.find_by(id: id)
      end

      #
      # get the models by year of date_column
      #
      # @param month [Integer] calendar year
      # @param date_column [String] date_column in model
      # @param model [Model] model
      #
      # @return [Array] array of model object
      #
      def model_by_year(model, date_column, year)
        model.where("extract(year from #{date_column}) = ?", year)
      end

      #
      # get the models by month of date_column
      #
      # @param month [Integer] calendar month
      # @param date_column [String] date_column in model
      # @param model [Model] model
      #
      # @return [Array] array of model object
      #
      def model_by_month(model, date_column, month)
        model.where("extract(month from #{date_column}) = ?", month)
      end

      #
      # get the model by field
      #
      # @param field [String] field value
      # @param model [Model] model
      # @param field_name String] field name
      #
      # @return [Model]  model if model exists else return nil
      #
      def get_model_by_field(model, field_name, field_value)
        model_instance = model.find_by(field_name => field_value)
        if model_instance.blank?
          raise @custom_error_util::ResourceNotFoundError.new(@content::RESOURCE_NOT_FOUND % {resource: model})
        end
        return model_instance
      end

      #
      # get all the records in the table
      #
      # @param model [Model] model
      #
      # @return [Array] array of model object
      #
      def get_all_element(model, pagination_params = {})
        set_pagination_properties(pagination_params, model)
        if @state.blank?
          elements = model.where.not(status: ORDER_STATES::INITIATED[:value]).order(@sort_order)
        else
          elements = model.where({ status: @state }).order(@sort_order)
        end
        page_count = (elements.count / @per_page).ceil
        elements = elements.limit(@per_page).offset((@page_no - 1) * @per_page)
        return {elements: elements, page_count: page_count}
      end

      #
      # get all the records in the table
      #
      # @param model [Model] model
      #
      # @return [Array] array of model object
      #
      def get_all_element_with_interval(model, pagination_params = {}, interval_params = {})
        set_pagination_properties(pagination_params, model)
        set_interval_properties(interval_params)
        if @state.blank?
          elements = model.where.not(status: ORDER_STATES::INITIATED[:value]).order(@sort_order).
                                      where(order_date: @from_time..@till_time)
        else
          elements = model.where({ status: @state }).order(@sort_order).
                                      where(order_date: @from_time..@till_time)
        end
        page_count = (elements.count / @per_page).ceil
        elements = elements.limit(@per_page).offset((@page_no - 1) * @per_page)
        return {elements: elements, page_count: page_count}
      end

      #
      # this function only permit attributes required for the table
      #
      # @return [Hash] filterd params
      #
      def model_params(args, model)
        args = ActionController::Parameters.new(args)
        args.permit(model.attribute_names)
      end

      #
      # setting pagination parameters for the model instance
      #
      def set_pagination_properties(pagination_params, model)
        @per_page = (pagination_params[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (pagination_params[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = pagination_params[:sort_by] || PAGINATION_UTIL::SORT_BY
        order = pagination_params[:order] || PAGINATION_UTIL::ORDER
        @state = pagination_params[:state]

        if @per_page < 0
          @per_page = PAGINATION_UTIL::INFINITE_PER_PAGE.to_f
        elsif @per_page == 0
          @per_page = PAGINATION_UTIL::PER_PAGE.to_f
        end

        @page_no = PAGINATION_UTIL::PAGE_NO.to_f if @page_no <= 0

        sort_by = PAGINATION_UTIL::SORT_BY unless model.attribute_names.include?(sort_by)
        order = PAGINATION_UTIL::ORDER unless PAGINATION_UTIL::ALLOWED_ORDERS.include?(order)

        @sort_order = sort_by + ' ' + order
      end

      def set_interval_properties(interval_params)
        @from_time = interval_params[:from_time]
        @till_time = interval_params[:till_time]
      end

      #
      # @param states [Array] array of valid states or [Integer] particular valid state
      #
      # return model instances existing in a particular states
      #
      def get_model_by_status(states, model)
        model_by_status = model.where({status: states})
      end

      #
      # Create the record
      #
      # @param args [hash] model arguments
      #
      # @return [Model]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create_model(args, model, message='')
        message = 'Model ' if message.blank?
        new_model = model.new(model_params(args, model))

        begin
          new_model.save!
          return new_model
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create ' + message + ', ' + e.message)
        end
      end

      #
      # Update the given model
      #
      # @param args [hash] arguments to update
      # @param model [Model] model
      #
      # @return [Model] updated model
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update_model(args, record, message = '')
        message = 'Model' if message.blank?
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(message +  ' not found') if record.nil? || args.nil?
        begin
          record.update_attributes!(model_params(args, record))
          return record
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update ' + message + ', '+ e.message)
        end
      end


    end
  end
end
