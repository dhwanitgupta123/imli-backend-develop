module MarketplaceOrdersModule

  module V1

    class TimeSlotDao < BaseOrdersModule::V1::BaseDao
      require 'date'

      TIME_SLOT_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::TimeSlotStates
      TIME_SLOT_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::TimeSlotEvents
      TIME_SLOT_MODEL = MarketplaceOrdersModule::V1::TimeSlot

      #
      # Initialize the object with deciding params
      #
      def initialize(params = '')
        super
        @params = params
      end

      #
      # Create new TimeSlot
      #
      # @param args [JSON] [Hash containing from_time, to_time, label]
      #
      # @return [Object] [created time slot]
      #
      # @raise [RunTimeError] [if time slot creation failed due to params passed]
      #
      def create_time_slot(args)

        # Filter args as per the model attributes
        filtered_args = model_params(args, TIME_SLOT_MODEL)
        if args[:unavailable_dates].present?
          filtered_args.merge!(unavailable_dates: args[:unavailable_dates].uniq) if validate_dates?(args[:unavailable_dates])
        end
        if args[:unavailable_days].present?
          filtered_args.merge!(unavailable_days: args[:unavailable_days].uniq) if validate_days?(args[:unavailable_days])
        end

        args = filtered_args
        begin
          time_slot = TIME_SLOT_MODEL.new(args)
          time_slot.save!
          return time_slot
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(e.message)
        end
      end

      #
      # Update already existing time slot
      #
      # @param args [JSON] [Hash containing from_time, to_time, label]
      #
      # @return [Object] [updated time slot]
      #
      # @raise [RunTimeError] [if time slot updation failed due to params passed]
      #
      def update_time_slot(args, time_slot)

        # Filter args as per the model attributes
        filtered_args = model_params(args, TIME_SLOT_MODEL)
        if args[:unavailable_dates].present?
          filtered_args.merge!(unavailable_dates: args[:unavailable_dates].uniq) if validate_dates?(args[:unavailable_dates])
        elsif args[:unavailable_dates].blank?
          filtered_args.merge!(unavailable_dates: [])
        end

        if args[:unavailable_days].present?
          filtered_args.merge!(unavailable_days: args[:unavailable_days].uniq)  if validate_days?(args[:unavailable_days])
        elsif args[:unavailable_days].blank?
          filtered_args.merge!(unavailable_days: [])
        end

        args = filtered_args
        args.delete(:id) if args[:id].present?
        begin
          time_slot.update_attributes!(args)
          return time_slot
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(e.message)
        end
      end

      #
      # Get time slot with respect to passed id (Database table ID like 13, 27, etc.)
      #
      def get_by_id(id)
        begin
          TIME_SLOT_MODEL.find(id.to_i)
        rescue => e
          raise CUSTOM_ERROR_UTIL::RecordNotFoundError.new(e.message)
        end
      end

      def get_by_field(field_name, field_value)
        get_model_by_field(TIME_SLOT_MODEL, field_name, field_value)
      end

      def get_all(pagination_params = {})
        set_pagination_properties(pagination_params, TIME_SLOT_MODEL)
        if @state.blank?
          elements = TIME_SLOT_MODEL.where.not({ status: TIME_SLOT_STATES::DELETED })
        else
          elements = TIME_SLOT_MODEL.where({ status: @state })
        end
        # Now compute total page count based on final filtered elements
        page_count = (elements.count / @per_page).ceil
        # Apply pagination over the filtered result
        elements = elements.order(@sort_order).
                            limit(@per_page).offset((@page_no - 1) * @per_page)
        return {elements: elements, page_count: page_count}
      end

      #
      # setting pagination parameters for the model instance
      #
      def set_pagination_properties(pagination_params, model)
        @per_page = (pagination_params[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (pagination_params[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = pagination_params[:sort_by] || PAGINATION_UTIL::SORT_BY
        order = pagination_params[:order] || PAGINATION_UTIL::ORDER
        @state = pagination_params[:state]
        @state = @state.to_i if @state.present?

        if @state.present? && !COMMON_MODEL_STATES::ALLOWED_STATES.include?(@state)
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_STATE)
        end

        @per_page = PAGINATION_UTIL::PER_PAGE.to_f if @per_page <= 0
        @page_no = PAGINATION_UTIL::PAGE_NO.to_f if @page_no <= 0

        sort_by = PAGINATION_UTIL::SORT_BY unless model.attribute_names.include?(sort_by)
        order = PAGINATION_UTIL::ORDER unless PAGINATION_UTIL::ALLOWED_ORDERS.include?(order)

        @sort_order = sort_by + ' ' + order
      end

      #
      # Get all active slots
      #
      def get_all_active_slots
        active_time_slots = TIME_SLOT_MODEL.where(status: TIME_SLOT_STATES::ACTIVE)
        active_time_slots = active_time_slots.order('from_time ASC')
        return active_time_slots
      end

      #
      # Trigger PAYMENT change_state as per the event passed
      #
      def change_state(time_slot, event)
        case event.to_i
        when TIME_SLOT_EVENTS::ACTIVATE
          time_slot.trigger_event('activate')
        when TIME_SLOT_EVENTS::DEACTIVATE
          time_slot.trigger_event('deactivate')
        when TIME_SLOT_EVENTS::SOFT_DELETE
          time_slot.trigger_event('soft_delete')
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT_UTIL::INVALID_EVENT)
        end
        return time_slot
      end

      #
      # This function returns active slot from give time_slots
      #
      # @param time_slots [Array] array of time slots
      #
      # @return [Array] array of active time slots
      #
      def self.get_active_time_slots(time_slots)
        return [] if time_slots.blank?
        time_slots.where(status: TIME_SLOT_STATES::ACTIVE)
      end

      #
      # This function returns non deleted slot from give time_slots
      #
      # @param time_slots [Array] array of time slots
      #
      # @return [Array] array of active time slots
      #
      def self.get_non_deleted_time_slots(time_slots)
        return [] if time_slots.blank?
        time_slots.where.not(status: TIME_SLOT_STATES::DELETED)
      end

      #
      # This function returns active slot by address
      #
      # @param address [Model] address model
      #
      # @return [Array] array of active time slots
      #
      def self.get_active_slots_by_address(address)

        return nil if address.blank?

        geographical_cluster_dao = GeographicalClustersModule::V1::GeographicalClusterDao

        pincode = address.area.pincode
        root_geographical_cluster = geographical_cluster_dao.get_root_geographical_cluster(pincode)

        return get_active_time_slots(root_geographical_cluster.time_slots.order(:from_time))
      end

      #
      # Validate if the array contains only date types or not
      #
      def validate_dates?(dates)
        dates.each do |date|

          next if date.class == Date

          begin
            Date.parse(date)
          rescue  ArgumentError
            raise CUSTOM_ERROR_UTIL::RunTimeError.new('Invalid Date')
          end
        end

        return true
      end

      #
      # Validate if the array contains valid day
      #
      def validate_days?(days)
        days.each do |day|
          raise CUSTOM_ERROR_UTIL::RunTimeError.new('Invalid Day') unless DateTimeModule::V1::DayEnums::DAYS.include?(day)
        end
        return true
      end
    end #End of class
  end
end
