require 'imli_singleton'
module MarketplaceOrdersModule

  module V1

    class TimeSlotDateBucketDao < BaseOrdersModule::V1::BaseDao
      include ImliSingleton
      #
      # Initialize the object with deciding params
      #
      def initialize(params = '')
        @params = params
        @time_slot_date_bucket_model = MarketplaceOrdersModule::V1::TimeSlotDateBucket
        @custom_error_util = CommonModule::V1::CustomErrors
      end

      #
      # This function return current order_count for a particular time_slot at give date
      #
      # @param slot_date [String] preferred date of the slot
      # @param time_slot[Model] time slot model
      #
      # @return [Integer] order count
      #
      def self.allocated_time_slots(time_slot, slot_date)

        time_slot_date_bucket_model = MarketplaceOrdersModule::V1::TimeSlotDateBucket
        time_slot_date_bucket = time_slot_date_bucket_model.where(slot_date: slot_date, time_slot_id: time_slot.id)

        return 0 if time_slot_date_bucket.blank?
        return time_slot_date_bucket.first.order_count
      end


      def self.get_total_count_of_assigned_slots(time_slot, slot_date)
        time_slot_date_bucket_model = MarketplaceOrdersModule::V1::TimeSlotDateBucket
        time_slot_date_bucket = time_slot_date_bucket_model.where(slot_date: slot_date, time_slot_id: time_slot.id)

        return 0 if time_slot_date_bucket.blank?
        return time_slot_date_bucket.first.order_count + time_slot_date_bucket.first.buffer_count
      end

      #
      # This function update order count of a given time_slot_date_bucket
      #
      # @param time_slot_date_bucket[Model] time_slot_date_bucket model
      #
      # @return [time_slot_date_bucket] updated time slot date bucket
      #
      def self.update_order_count(time_slot_date_bucket, increment)
        time_slot_date_bucket.order_count += increment
        time_slot_date_bucket.save!
        return time_slot_date_bucket
      end

      #
      # This function update order count of a given time_slot_date_bucket
      #
      # @param time_slot_date_bucket[Model] time_slot_date_bucket model
      #
      # @return [time_slot_date_bucket] updated time slot date bucket
      #
      def self.update_buffer_count(time_slot_date_bucket, increment)
        time_slot_date_bucket.buffer_count += increment
        time_slot_date_bucket.save!
        return time_slot_date_bucket
      end

      #
      # Create time slot date bucket for a give slot and slot_date
      #
      # @param slot_date[String] slot_date
      # @param slot[Model] time slot model
      # @param order_count[Integer] order count to initiate
      #
      # @return [time_slot_date_bucket] created time slot date bucket
      #
      def self.create_time_slot_date_bucket(slot, slot_date)
        time_slot_date_bucket = MarketplaceOrdersModule::V1::TimeSlotDateBucket.new({
                                                                                      slot_date: slot_date,
                                                                                      time_slot_id: slot.id,
                                                                                      order_count: 0,
                                                                                      buffer_count: 0
                                                                                    })
        time_slot_date_bucket.save!
        return time_slot_date_bucket
      end

      #
      # Update time slot date bucket, if slot avaialble
      #
      # @param slot_date[String] slot_date
      # @param slot[Model] time slot model
      #
      # @return [time_slot_date_bucket] created time slot date bucket
      #
      def self.update_if_available(args)
        time_slot = args[:time_slot]
        slot_date = args[:preferred_date]

        time_slot_date_bucket = MarketplaceOrdersModule::V1::TimeSlotDateBucket.
                                                  where(slot_date: slot_date, time_slot_id: time_slot.id)

        if time_slot_date_bucket.blank?
          time_slot_date_bucket = create_time_slot_date_bucket(time_slot, slot_date)
        else
          time_slot_date_bucket = time_slot_date_bucket.first
        end

        time_slot_date_bucket.with_lock do
          if time_slot_date_bucket.order_count >= time_slot.order_limit
            raise CommonModule::V1::CustomErrors::SlotUnavailable
          else
            update_order_count(time_slot_date_bucket, 1)
          end
        end
      end


      def self.update_if_buffer_or_allocated_slot_available(args)
        time_slot = args[:time_slot]
        slot_date = args[:preferred_date]

        time_slot_date_bucket = MarketplaceOrdersModule::V1::TimeSlotDateBucket.
          where(slot_date: slot_date, time_slot_id: time_slot.id)

        if time_slot_date_bucket.blank?
          time_slot_date_bucket = create_time_slot_date_bucket(time_slot, slot_date)
        else
          time_slot_date_bucket = time_slot_date_bucket.first
        end

        time_slot_date_bucket.with_lock do
          if time_slot_date_bucket.order_count < time_slot.order_limit
            update_order_count(time_slot_date_bucket, 1)
          elsif time_slot_date_bucket.buffer_count < time_slot.buffer_limit
            update_buffer_count(time_slot_date_bucket, 1)
          else
            raise CommonModule::V1::CustomErrors::SlotUnavailable
          end
        end
      end

      #
      # This function updates the time slot bucket
      #
      # @param slot_date[String] slot_date
      # @param time_slot[Model] time slot model
      # @param increment [Integer] value to change
      #
      # @return [time_slot_date_bucket] created time slot date bucket
      #
      def self.update_allocated_time_slots(time_slot, slot_date, increment)
        time_slot_date_bucket = MarketplaceOrdersModule::V1::TimeSlotDateBucket.
                                        where(slot_date: slot_date, time_slot_id: time_slot.id)

        raise CommonModule::V1::CustomErrors::ResourceNotFoundError.
                              new('Time slot bucket not found') if time_slot_date_bucket.blank?

        time_slot_date_bucket = time_slot_date_bucket.first

        time_slot_date_bucket.with_lock do
          # Decrement allocation first from buffer than from order_count
          if time_slot_date_bucket.buffer_count + increment >= 0
            update_buffer_count(time_slot_date_bucket, increment)
          elsif time_slot_date_bucket.order_count + increment >= 0
            update_order_count(time_slot_date_bucket, increment)
          end
        end
      end
    end #End of class
  end
end
