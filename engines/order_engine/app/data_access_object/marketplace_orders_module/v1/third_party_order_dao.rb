require 'imli_singleton'
module MarketplaceOrdersModule

  module V1

    class ThirdPartyOrderDao < BaseOrdersModule::V1::BaseDao
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @third_party_order_model = MarketplaceOrdersModule::V1::ThirdPartyOrder
        @third_party_order_events = MarketplaceOrdersModule::V1::ModelStates::V1::ThirdPartyOrderEvents
        @third_party_order_states = MarketplaceOrdersModule::V1::ModelStates::V1::ThirdPartyOrderStates
        super
      end

      def create(args)
        create_model(args, @third_party_order_model, 'Third Party Order')
      end

      def update(args, user)
        update_model(args, user, 'Third Party Order')
      end

      def get_by_id(id)
        get_model_by_id(@third_party_order_model, id)
      end

      def get_placed_or_pending_orders_by_order_id(order_id)
        return [] if order_id.blank?
        @third_party_order_model.where(order: order_id, status: [@third_party_order_states::PENDING, @third_party_order_states::PLACED])
      end

      def get_pending_orders_by_order_id(order_id)
        return [] if order_id.blank?
        @third_party_order_model.where(order: order_id, status: @third_party_order_states::PENDING)
      end

      def get_membership_orders_associated_with_ample_order(order_id)
        orders = get_placed_or_pending_orders_by_order_id(order_id)
        membership_reference_type = MarketplaceOrdersModule::V1::ThirdPartyOrderReferenceMap.get_id_by_type('USER_MEMBERSHIP')
        orders.where(reference_type: membership_reference_type)
      end

      def discard_any_pending_order_by_type(order_id, reference_type)
        order = @third_party_order_model.where(order_id: order_id.to_i, reference_type: reference_type.to_i, status: @third_party_order_states::PENDING).first
        change_state_to_discarded(order)
      end

      def get_all(pagination_params = {})
        get_all_element(@third_party_order_model, pagination_params)
      end

      def change_state_to_discarded(order)
        change_state(order, @third_party_order_events::DISCARD_ORDER) if order.present?
      end

      def change_state_to_placed(order)
        change_state(order, @third_party_order_events::PLACE) if order.present?
      end

      def change_state_to_cancelled(order)
        change_state(order, @third_party_order_events::CANCEL_ORDER) if order.present?
      end

      #
      # Trigger PAYMENT change_state as per the event passed
      #
      def change_state(third_party_order, event)
        case event.to_i
        when @third_party_order_events::PLACE
          third_party_order.trigger_event('place')
        when @third_party_order_events::DISCARD_ORDER
          third_party_order.trigger_event('discard_order')
        when @third_party_order_events::CANCEL_ORDER
          third_party_order.trigger_event('cancel_order')
        else
          raise @custom_error_util::InvalidArgumentsError.new(@content::INVALID_EVENT)
        end
        return third_party_order
      end


    end
  end
end
