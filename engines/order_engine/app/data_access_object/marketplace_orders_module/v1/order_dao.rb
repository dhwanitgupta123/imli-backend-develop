module MarketplaceOrdersModule

  module V1

  	class OrderDao < BaseOrdersModule::V1::BaseDao
      ORDER_STATES = MarketplaceOrdersModule::V1::ModelStates::V1::OrderStates
      ORDER_EVENTS = MarketplaceOrdersModule::V1::ModelStates::V1::OrderEvents

      ORDER_PRODUCT_STATES = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::ModelStates::V1::OrderProductStates
      ORDER_MODEL = MarketplaceOrdersModule::V1::Order
      ORDER_CONTEXT_DAO = MarketplaceOrdersModule::V1::OrderContextDao
      PLATFORM_TYPE_UTIL = CommonModule::V1::PlatformType
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      FINAL_STATES = [ ORDER_STATES::ORDER_CANCELLED[:value], ORDER_STATES::CANCELLED_BY_ADMIN[:value], ORDER_STATES::FAILED[:value], ORDER_STATES::COMPLETED[:value], ORDER_STATES::FULFILLED[:value], ORDER_STATES::FULLY_REFUNDED[:value], ORDER_STATES::PARTIALLY_REFUNDED[:value] ]
      FIRST_ORDER_STATES = [ ORDER_STATES::INITIATED[:value], ORDER_STATES::ORDER_CANCELLED[:value], ORDER_STATES::CANCELLED_BY_ADMIN[:value], ORDER_STATES::PAYMENT_FAILED[:value], ORDER_STATES::FAILED[:value] ]

      #
      # Initialize the object with deciding params
      #
      def initialize(params = '')
        super
        @params = params
      end

      #
      # Create new ORDER of the user
      #
      # @param args [JSON] [Hash containing user, cart and address object]
      #
      # @return [Object] [created order of the user]
      #
      # @raise [RunTimeError] [if order creation failed due to params passed]
      def create_new_order(args)
        order = ORDER_MODEL.new(args)
        order[:status] = ORDER_STATES::INITIATED[:value]
        begin
          order.save!
          return order
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(CONTENT::CREATION_FAILED % {model: 'Order'})
        end
      end

      #
      # Update order object with passed params
      #
      # @param args [JSON] [Hash containing order attributes,
      # Refer order model for attributes]
      #
      # @return [Object] [updated Order object]
      #
      # @raise [RunTimeError] [if Payment object creation failed]
      #
      def update_order(args, order)
        begin
          order.update_attributes!(args)
          return order
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(CONTENT::UPDATION_FAILED % {model: 'Order'})
        end
      end


      #
      # Update order object with passed params
      #
      # @param args [JSON] [Hash containing order attributes,
      # Refer order model for attributes]
      #
      # @return [Object] [updated Order object]
      #
      # @raise [RunTimeError] [if Payment object creation failed]
      #
      def self.update_order(args, order)
        begin
          order.update_attributes!(args)
          return order
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(CONTENT::UPDATION_FAILED % {model: 'Order'})
        end
      end

      #
      # Assign cart to given Order
      #
      # @param new_cart [Object] [Cart model object which is to be linked to order]
      # @param order [Object] [Order model object to whom cart is to be linked]
      #
      # @return [Object] [Updated Order object]
      #
      def assign_cart_to_order(new_cart, order)
        begin
          order.carts << new_cart
          return order
        rescue => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(e.message)
        end
      end

      #
      # Update order_id in the ORDER of the user
      #
      # @param order [Object] [order object of the user]
      # @param order_id [String] [generated unique order id]
      #
      # @return [Object] [updated order of the user]
      #
      # @raise [RunTimeError] [if order updation failed due to params passed]
      def update_order_id(order, order_id)
        begin
          order.order_id = order_id
          order.save!
          return order
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(CONTENT::CREATION_FAILED % {model: 'Order ID'})
        end
      end

      #
      # Get order with respect to passed id (Database table ID like 13, 27, etc.)
      #
      def get_order_by_id(id)
        begin
          ORDER_MODEL.find(id.to_i)
        rescue => e
          raise CUSTOM_ERROR_UTIL::RecordNotFoundError.new(e.message)
        end
      end

      #
      # Get order with respect to passed id (Database table ID like 13, 27, etc.)
      #
      def get_by_id(id)
        begin
          ORDER_MODEL.find(id.to_i)
        rescue => e
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(e.message)
        end
      end

      #
      # Get ORDER with respect to passed id and belongs to given USER
      # AND is in INITIATED state.
      #
      def get_users_initiated_order_with_id(user, order_id)
        return user.orders.
                where({ id: order_id, status: ORDER_STATES::INITIATED[:value] }).first
      end

      #
      # Get ORDER with respect to passed id and belongs to given USER
      # This is just another function to OPTIMIZE Query and do two checks
      #
      def get_users_order_with_id(user, order_id)
        return user.orders.
                where({ id: order_id }).first
      end

      #
      # Get order with respect to passed generated OrderId (like AMPLE-0094-ODE321)
      #
      def get_order_by_order_id(order_id)
        order = ORDER_MODEL.find_by(order_id: order_id.to_s)
        if order.blank?
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT::RESOURCE_NOT_FOUND % {resource: 'Order'})
        end
        return order
      end

      #
      # Get order with belongs to USER and is in INITIATED state
      #
      def fetch_initiated_order_for_user(user)
        # Fetch the first INITIATED Order
        return user.orders.
                where({ status: ORDER_STATES::INITIATED[:value] }).first
      end

      def get_by_field(field_name, field_value)
        get_model_by_field(ORDER_MODEL, field_name, field_value)
      end

      def get_all(pagination_params = {})
        # Checking whether model contains order date for backward compatability with migration
        if(pagination_params[:sort_by].blank? && ORDER_MODEL.attribute_names.include?('order_date'))
          pagination_params = pagination_params.merge({
            sort_by: 'order_date',
            order: 'DESC'
            })
        end
        get_all_element(ORDER_MODEL, pagination_params)
      end

      #
      # Get all ORDERS between a specified interval with pagination
      #
      # @param pagination_params = {} [JSON] [Hash of paginations parameters]
      # @param interval_params = {} [JSON] [Hash of time interval parameters, from_time, to_time]
      #
      # @return [Array] [Array of filtered Order model objects]
      #
      def get_all_orders_between_interval(pagination_params = {}, interval_params = {})
        # Checking whether model contains order date for backward compatability with migration
        if(pagination_params[:sort_by].blank? && ORDER_MODEL.attribute_names.include?('order_date'))
          pagination_params = pagination_params.merge({
            sort_by: 'order_date',
            order: 'DESC'
            })
        end
        get_all_element_with_interval(ORDER_MODEL, pagination_params, interval_params)
      end

      #
      # Get all orders corresponding to user sorted by order date in DESC order
      #
      def get_all_orders_of_user(user, last_n_orders = '')
        if last_n_orders.present?
          user.orders.order('order_date DESC').limit(last_n_orders.to_i)
        else
          user.orders.order('order_date DESC')
        end
      end

      #
      # Get all intermediate ORDERs (considered as ACTIVE ORDERs)
      # Do not show INITIATED Orders in Active Orders
      #
      def get_non_delivered_orders(user)
        filtered_orders = user.orders.
                where.not({ status: [ORDER_STATES::INITIATED[:value], FINAL_STATES] })
        filtered_orders = sort_by_order_date(filtered_orders)
        return filtered_orders
      end

      #
      # Sort complete collection by order date attribute in DESC order
      #
      # @param orders [Array] [ActiveRecord collection Array of Order objects]
      #
      # @return [Array] [Sorted array of Order objects]
      #
      def sort_by_order_date(orders)
        # For backward compatability with migration, need to check order_date
        # key presence in model
        if orders.present? && ORDER_MODEL.attribute_names.include?('order_date')
          orders = orders.order('order_date DESC')
        end
        return orders
      end

      #
      # Get all Orders by provided states
      # @param states [Array] array of valid states
      #
      def get_orders_by_status_in_interval(params)
        states = params[:states]
        from_time = params[:from_time]
        till_time = params[:till_time]
        return ORDER_MODEL.where({ status: states}).where(updated_at: from_time..till_time)
      end

      #
      # Get all final state ORDERs (considered as PREVIOUS ORDERs/COMPLETED ORDERs)
      #
      def get_delivered_orders(user)
        # Fetch the first INITIATED Order
        filtered_orders = user.orders.
                where({ status: FINAL_STATES })
        filtered_orders = sort_by_order_date(filtered_orders)
        return filtered_orders
      end

      def get_placed_orders_in_interval(params)
        pagination_params = params[:pagination_params]
        from_time = params[:from_time]
        till_time = params[:till_time]
        order_state = ORDER_STATES::PLACED[:value]

        placed_orders_in_interval = ORDER_MODEL.where({ status: order_state}).where("order_date >= ? AND order_date < ?", from_time, till_time).order("order_date ASC")
        return placed_orders_in_interval
      end

      #
      # Function to check it was a user's first Order
      #
      def first_order_placed?(user)
        orders = user.orders.where.not({ status: FIRST_ORDER_STATES })
        if orders.count == 1
          return true
        else
          return false
        end
      end

      #
      # Function to check if Order is the First fulfilled order of user
      #
      def first_order_fulfilled?(user)
        orders = user.orders.where(status: ORDER_STATES::FULFILLED[:value])
        if orders.count == 1
          return true
        else
          return false
        end
      end

      #
      # Function to check if user has only one completed order
      #
      def first_order_completed?(user)
         orders = user.orders.where(status: [ORDER_STATES::FULFILLED[:value], ORDER_STATES::COMPLETED[:value]])
         return orders.count == 1
      end

      #
      # Function to check whether this is USER's first order or not
      # IF count of orders which are not in INITIAL states, ( OR can say
      # are either placed, delivered, dispatched, etc.) is equal to ZERO
      # Then this is the user's first order
      #
      # @param user [Object] [User model object]
      #
      # @return [Boolean] [true if user's first order]
      #
      def is_first_order_of_user?(user)
        return false if user.blank?
        orders = user.orders.where.not({ status: FIRST_ORDER_STATES })
        if orders.count == 0
          return true
        else
          return false
        end
      end

      #
      # Function to get number of order of the user (1st order, 2nd order, etc. etc.)
      # Check total number of orders till now placed by user. THis order will
      # be that computed value + 1.
      #
      # @param user [Object] [User model object]
      #
      # @return [Integer] [current number of order of the user]
      #
      def get_current_number_of_order_of_user(user)
        return nil if user.blank?
        # orders.count ORDERS are placed, so this order will be 1 more than that value
        return get_exact_order_numbers_of_user(user) + 1
      end

      #
      # Function to get number of order of the user (1st order, 2nd order, etc. etc.)
      # Check total number of orders till now placed by user.
      #
      # @param user [Object] [User model object]
      #
      # @return [Integer] [current number of order of the user]
      #
      def get_exact_order_numbers_of_user(user)
        return 0 if user.blank?
        orders = user.orders.where.not({ status: FIRST_ORDER_STATES })
        return orders.count.to_i
      end

      #
      # Record order placed date in order_date attribute
      #
      # @param order [Object] [Order whose placed date is to be recorded]
      # @param time [DateTime] [time which is to be recorded]
      #
      # @return [Object] [updated Order]
      #
      def record_placed_date(order, time = nil)
        # Putting check for backward compatibility with order_date migration
        if ORDER_MODEL.attribute_names.include?('order_date')
          order.order_date = time || Time.zone.now
          order.save!
        end
        return order
      end

      #
      # Trigger ORDER change_state as per the event passed
      #
      def change_state(order, event)

        case event.to_i
        when ORDER_EVENTS::PLACE
          order.trigger_event('place')
        when ORDER_EVENTS::PAYMENT_FAILED
          order.trigger_event('payment_failed')
        when ORDER_EVENTS::APPROVED_BY_MARKETPLACE
          order.trigger_event('approved_by_marketplace')
        when ORDER_EVENTS::APPROVED_BY_SELLER
          order.trigger_event('approved_by_seller')
        when ORDER_EVENTS::APPROVED_BY_THIRD_PARTY_SELLER
          order.trigger_event('approved_by_third_party_seller')
        when ORDER_EVENTS::ABORT
          order.trigger_event('abort')
        when ORDER_EVENTS::APPROVED_BY_WAREHOUSE
          order.trigger_event('approved_by_warehouse')
        when ORDER_EVENTS::APPROVED_BY_FULFILLMENT_CENTER
          order.trigger_event('approved_by_fulfillment_center')
        when ORDER_EVENTS::DISPATCH
          order.trigger_event('dispatch')
        when ORDER_EVENTS::FAIL_DELIVERY
          order.trigger_event('fail_delivery')
        when ORDER_EVENTS::RETRY_DELIVERY
          order.trigger_event('retry_delivery')
        when ORDER_EVENTS::DELIVER
          order.trigger_event('deliver')
        when ORDER_EVENTS::NO_RETRY_PAYMENT
          order.trigger_event('no_retry_payment')
        when ORDER_EVENTS::RETRY_SUCCESSFUL
          order.trigger_event('retry_successful')
        when ORDER_EVENTS::FULFILLMENT_SUCCESSFUL
          order.trigger_event('fulfillment_successful')
        when ORDER_EVENTS::RETURN_ORDER
          order.trigger_event('return_order')
        when ORDER_EVENTS::RETURN_REJECTED
          order.trigger_event('return_rejected')
        when ORDER_EVENTS::RETURN_APPROVED
          order.trigger_event('return_approved')
        when ORDER_EVENTS::RETURN_CANCELLED
          order.trigger_event('return_cancelled')
        when ORDER_EVENTS::INITIATE_REFUND
          order.trigger_event('initiate_refund')
        when ORDER_EVENTS::REFUND_FAILED
          order.trigger_event('refund_failed')
        when ORDER_EVENTS::REFUND_SUCCESSFUL
          order.trigger_event('refund_successful')
        when ORDER_EVENTS::RETRY_REFUND
          order.trigger_event('retry_refund')
        when ORDER_EVENTS::REFUND_PARTIAL
          order.trigger_event('refund_partial')
        when ORDER_EVENTS::REFUND_FULLY
          order.trigger_event('refund_fully')
        when ORDER_EVENTS::CANCEL_ORDER
          order.trigger_event('cancel_order')
        when  ORDER_EVENTS::PACKING_DONE
          order.trigger_event('packing_done')
        when  ORDER_EVENTS::FORCE_CANCEL
          order.trigger_event('force_cancel')
        when ORDER_EVENTS::COMPLETE_ORDER
          order.trigger_event('complete_order')
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end

        return order
      end

      #
      # This function join all tables required to return all information
      # corresponding to order, address, user, cart and payments
      #
      # @param brand_packs [ActiveRecordRelationArray] orders
      #
      # @return [ActiveRecordRelationArray] Joined object containing
      #
      def self.get_order_and_user_data(orders)
        return [] if orders.blank?

        orders.eager_load(address: :area).eager_load(:user, :carts, :payments, :third_party_orders)
      end

      def self.load_order_details_data(order)
        return nil if order.blank?

        ORDER_MODEL.eager_load(:user, :payments, address: [area: [city: [state: :country]]]).where(id: order.id).first

      end

      #
      # This function return completed orders and if inputs orders present then it will filter complete
      # orders and return
      #
      # @param orders = [] [Array] Array of order
      #
      # @return [Array] Array of order
      #
      def self.get_completed_orders(orders = [])

        if orders.blank?
          ORDER_MODEL.where.not(status: [ORDER_STATES::INITIATED[:value], ORDER_STATES::ORDER_CANCELLED[:value],
                                         ORDER_STATES::CANCELLED_BY_ADMIN[:value]])
        else
          orders.where.not(status: [ORDER_STATES::INITIATED[:value], ORDER_STATES::ORDER_CANCELLED[:value],
                                         ORDER_STATES::CANCELLED_BY_ADMIN[:value]])
        end
      end

      #
      # Get all Orders by provided states
      # @param states [Array] array of valid states or [Integer] particular valid state
      #
      def get_orders_by_status(states)
        orders_by_status = get_model_by_status(states, ORDER_MODEL)
        return orders_by_status
      end

      def self.is_payment_pending?(order)
        payment_dao = MarketplaceOrdersModule::V1::PaymentDao.new(@params)
        pending_payments = payment_dao.get_pending_payments(order)
        if pending_payments.present?
          return true
        else
          return false
        end
      end

      #
      # This function handles 2 cases
      # 1st when start_date == end_date -> Then query will fetch time slots of only single day of date from_date
      # 2nd when start_date != end_date -> Results -> time slots of from_date + all slots b/w start to end date +
      #                                               time_slots of to_date
      # And condition on geocluster as order should belongs to input zones array
      #
      # Return aggregated order which contain order->[time_slot, address -> area -> geographical_cluster]
      #
      def get_aggregated_orders_by_zone_and_time_slots(filters)

        states = filters[:states].present? ? filters[:states] : ORDER_STATES.get_all_order_state_values

        set_pagination_properties(filters, ORDER_MODEL)

        if filters[:from_date] != filters[:to_date]
          total_count = ORDER_MODEL.eager_load(:time_slot, address: [area: :geographical_cluster]).
            where('(preferred_delivery_date BETWEEN ? AND ?) OR (preferred_delivery_date=? AND time_slots.to_time_in_seconds>=?) OR
            (preferred_delivery_date=? AND time_slots.from_time_in_seconds<=?)', filters[:start_date], filters[:end_date], filters[:from_date],
            filters[:from_time], filters[:to_date], filters[:to_time]).where(status: states).
            where(geographical_clusters: {parent_cluster_id: filters[:zones]}).count

          orders = ORDER_MODEL.eager_load(:time_slot, user: :emails, address: [area: [:geographical_cluster, city: [state: :country]]]).
            where('(preferred_delivery_date BETWEEN ? AND ?) OR (preferred_delivery_date=? AND time_slots.to_time_in_seconds>=?) OR
            (preferred_delivery_date=? AND time_slots.from_time_in_seconds<=?)', filters[:start_date], filters[:end_date], filters[:from_date],
            filters[:from_time], filters[:to_date], filters[:to_time]).
            where(geographical_clusters: {parent_cluster_id: filters[:zones]}).where(status: states).
            order('preferred_delivery_date', 'time_slots.from_time_in_seconds').limit(@per_page).offset((@page_no - 1) * @per_page)
        else
          total_count = ORDER_MODEL.eager_load(:time_slot, address: [area: :geographical_cluster]).
            where('(preferred_delivery_date=?) AND (time_slots.to_time_in_seconds BETWEEN ? AND ? OR time_slots.from_time_in_seconds BETWEEN ? AND ?)',
            filters[:from_date], filters[:from_time], filters[:to_time], filters[:from_time], filters[:to_time]).
            where(geographical_clusters: {parent_cluster_id: filters[:zones]}).where(status: states).count

          orders = ORDER_MODEL.eager_load(:time_slot, user: :emails, address: [area: [:geographical_cluster, city: [state: :country]]]).
            where('(preferred_delivery_date=?) AND (time_slots.to_time_in_seconds BETWEEN ? AND ? OR time_slots.from_time_in_seconds BETWEEN ? AND ?)',
            filters[:from_date], filters[:from_time], filters[:to_time], filters[:from_time], filters[:to_time]).
            where(geographical_clusters: {parent_cluster_id: filters[:zones]}).where(status: states).
            order('preferred_delivery_date', 'time_slots.from_time_in_seconds').limit(@per_page).offset((@page_no - 1) * @per_page)
        end

        page_count = (total_count / @per_page).ceil

        return {elements: orders, page_count: page_count}
      end

      # This function handles 2 cases
      # 1st when start_date == end_date -> Then query will fetch time slots of only single day of date from_date
      # 2nd when start_date != end_date -> Results -> time slots of from_date + all slots b/w start to end date +
      #                                               time_slots of to_date
      #
      # Return aggregated order which contain order->[time_slot, address -> area -> geographical_cluster]
      #
      def get_aggregated_orders_by_time_slots(filters)

        states = filters[:states].present? ? filters[:states] : ORDER_STATES.get_all_order_state_values

        set_pagination_properties(filters, ORDER_MODEL)

        if filters[:from_date] != filters[:to_date]
          total_count = ORDER_MODEL.eager_load(:time_slot, address: :area).
            where('(preferred_delivery_date BETWEEN ? AND ?) OR (preferred_delivery_date=? AND time_slots.to_time_in_seconds>=?) OR
            (preferred_delivery_date=? AND time_slots.from_time_in_seconds<=?)', filters[:start_date], filters[:end_date], filters[:from_date],
                  filters[:from_time], filters[:to_date], filters[:to_time]).where(status: states).count

          orders = ORDER_MODEL.eager_load(:time_slot, user: :emails, address: [area: [:geographical_cluster, city: [state: :country]]]).
            where('(preferred_delivery_date BETWEEN ? AND ?) OR (preferred_delivery_date=? AND time_slots.to_time_in_seconds>=?) OR
            (preferred_delivery_date=? AND time_slots.from_time_in_seconds<=?)', filters[:start_date], filters[:end_date], filters[:from_date],
            filters[:from_time], filters[:to_date], filters[:to_time]).where(status: states).
            order('preferred_delivery_date', 'time_slots.from_time_in_seconds').limit(@per_page).offset((@page_no - 1) * @per_page)
        else
          total_count = ORDER_MODEL.eager_load(:time_slot, address: :area).
            where('(preferred_delivery_date=? ) AND (time_slots.to_time_in_seconds BETWEEN ? AND ? OR time_slots.from_time_in_seconds BETWEEN ? AND ?) ',
            filters[:from_date], filters[:from_time], filters[:to_time], filters[:from_time], filters[:to_time]).
            where(status: states).count

          orders = ORDER_MODEL.eager_load(:time_slot, user: :emails, address: [area: [:geographical_cluster, city: [state: :country]]]).
            where('(preferred_delivery_date=? ) AND (time_slots.to_time_in_seconds BETWEEN ? AND ? OR time_slots.from_time_in_seconds BETWEEN ? AND ?) ',
            filters[:from_date], filters[:from_time], filters[:to_time], filters[:from_time], filters[:to_time]).
            where(status: states).order('preferred_delivery_date', 'time_slots.from_time_in_seconds').limit(@per_page).offset((@page_no - 1) * @per_page)
        end
        page_count = (total_count / @per_page).ceil

        return {elements: orders, page_count: page_count}
      end

      #
      # This function returns unscheduled placed orders
      #
      # Return aggregated order which contain order->[address -> area -> geographical_cluster]
      #
      def get_unallocated_orders(filters)

        states = filters[:states].present? ? filters[:states] : ORDER_STATES.get_all_order_state_values

        set_pagination_properties(filters, ORDER_MODEL)

        total_count = ORDER_MODEL.where(time_slot: nil, status: states).count
        orders = ORDER_MODEL.eager_load(user: :emails, address: [area: [:geographical_cluster, city: [state: :country]]]).where(time_slot: nil,
                                        status: states).limit(@per_page).
                                        offset((@page_no - 1) * @per_page)

        page_count = (total_count / @per_page).ceil

        return {elements: orders, page_count: page_count}
      end

      #
      # This function returns unscheduled placed orders after applying zones filter
      #
      # Return aggregated order which contain order->[address -> area -> geographical_cluster]
      #
      def get_unallocated_orders_by_zones(filters)

        states = filters[:states].present? ? filters[:states] : ORDER_STATES.get_all_order_state_values

        set_pagination_properties(filters, ORDER_MODEL)

        total_count = ORDER_MODEL.eager_load(address: [area: :geographical_cluster]).
          where(time_slot: nil, status: states).
          where(geographical_clusters: {parent_cluster_id: filters[:zones]}).count

        orders = ORDER_MODEL.eager_load(user: :emails, address: [area: [:geographical_cluster, city: [state: :country]]]).
          where(time_slot: nil, status: states).
          where(geographical_clusters: {parent_cluster_id: filters[:zones]}).limit(@per_page).offset((@page_no - 1) * @per_page)

        page_count = (total_count / @per_page).ceil

        return {elements: orders, page_count: page_count}
      end

      # This function returns orders those status changed on date change_state_date
      #
      def self.get_order_by_placed_date_and_status(status, change_state_date)

        epoch1 = change_state_date.beginning_of_day.to_i
        epoch2 = change_state_date.end_of_day.to_i

        possible_regex = RegexForRange.get_regex_for_range(epoch1, epoch2)

        query = '\"' + status.to_s + '\":\"{\\\"time\\\":\\\"' + possible_regex + '\\\"}\"'

        ORDER_MODEL.where(query)
      end

      def get_orders_for_dispatch_list(ids)
        ORDER_MODEL.eager_load(:time_slot, user: [:emails], address: [area: [city: [state: :country]]]).
            where(id: ids)
      end

      def self.get_orders_by_ids(ids)
        ORDER_MODEL.where(id: ids)
      end

      def self.get_first_order_of_user(user)
        orders = user.orders.where.not({ status: FIRST_ORDER_STATES }).
              order('order_date ASC').first
      end

      def self.get_order_status_count(user)
        user.orders.group(:status).count
      end

      #
      # Get user Orders by calendar year
      # @param year [Integer] calendar year
      # @param states [Array] array of valid states or [Integer] particular valid state
      #
      # @return [Array] array of model object
      #
      def get_user_orders_by_year(user, states, year)
        date_column = 'order_date'
        orders_by_year = user.orders.where("extract(year from #{date_column}) = ?", year).where({status: states})
        return orders_by_year
      end

      #
      # Get user Orders by provided states
      # @param month [Integer] calendar month
      # @param year [Integer] calendar year
      # @param states [Array] array of valid states or [Integer] particular valid state
      #
      # @return [Array] array of model object
      #
      def get_user_orders_by_calendar_month(user, states, month, year)
        date_column = 'order_date'
        orders_by_month = user.orders.where("extract(month from #{date_column}) = ?", month).where("extract(year from #{date_column}) = ?", year).where({status: states})
        return orders_by_month
      end

  	end #End of class
  end
end
