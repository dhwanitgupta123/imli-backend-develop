#
# Module to handle all the functionalities related to inventory Orders
#
module InventoryOrdersModule
  #
  # Version1 for inventory orders module
  #
  module V1
    #
    # Class to implement functionalities related to Data object of Iventory order products
    #
    class InventoryOrderProductsDao < BaseOrdersModule::V1::BaseDao
      INVENTORY_ORDER_PRODUCT_STATES = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderProductStates
      INVENTORY_ORDER_PRODUCT_EVENTS = InventoryOrdersModule::V1::ModelStates::V1::InventoryOrderProductEvents
      INVENTORY_ORDER_PRODUCT_MODEL = InventoryOrdersModule::V1::InventoryOrderProduct
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      #
      # Initialize Order product DAO with passed params
      #
      def initialize(params='')
        @params = params
      end
      
      #
      # Create a new Order Product
      #
      # @param params [JSON] [Hash containing quantity, cart and MPSP]
      #
      # @raise [RunTimeError] [if creation failed]
      def create_inventory_order_products(params)
        inventory_order_product = INVENTORY_ORDER_PRODUCT_MODEL.new(params)
        begin
          inventory_order_product.save!
          # After creation, recompute order product state
          update_inventory_order_product_state(inventory_order_product)
          return inventory_order_product
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::RunTimeError.new(e.message)
        end
      end

      #
      # Change state of Order product as per the event sent
      #
      def change_state(inventory_order_product, event)
        case event.to_i
          when INVENTORY_ORDER_PRODUCT_EVENTS::ACTIVATE
            inventory_order_product.trigger_event('activate')
          when INVENTORY_ORDER_PRODUCT_EVENTS::DEACTIVATE
            inventory_order_product.trigger_event('deactivate')
          else
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
        return inventory_order_product
      end

      # 
      # Update a order product as per the passed attributes
      #
      # @param args [JSON] [Hash containing quantity, cart or MPSP]
      # @param order_product [Object] [Order product object]
      # 
      # @return [Object] [Order product]
      def update_inventory_order_products(args, inventory_order_product)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Inventory cannot be null') if inventory_order_product.nil? || args.nil?
        begin
          inventory_order_product.update_attributes!(args)
          update_inventory_order_product_state(inventory_order_product)
          return inventory_order_product
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update order_product')
        end
      end

      #
      # Function to return all the inventory order products who's buying status is active
      #
      def get_active_inventory_order_products(inventory_order)
        inventory_order_products = INVENTORY_ORDER_PRODUCT_MODEL
          .where({buying_status: INVENTORY_ORDER_PRODUCT_STATES::ACTIVE, inventory_order: inventory_order})
        return inventory_order_products
      end

      #
      # Function to get inventory order product by it's id
      #
      def get_inventory_order_product_by_id(inventory_order_product_id)
        begin
          inventory_order_product = INVENTORY_ORDER_PRODUCT_MODEL.find(inventory_order_product_id)
        rescue ActiveRecord::ResourceNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(e)
        end
      end

      private

      #
      # Update Order product state
      # Make a order product INACTIVE if its quantity is 0
      # Make it ACTIVE if it has a positive quantity and was previously inactive
      #
      # @param order_product [Object] [order product whose state is to be updated]
      #
      def update_inventory_order_product_state(inventory_order_product)
        # If Order Product is INACTIVE and a new quantity is added, then ACTIVATE it again
        if inventory_order_product.inactive? && inventory_order_product.quantity_ordered.to_i > 0
          event = INVENTORY_ORDER_PRODUCT_EVENTS::ACTIVATE
          change_state(inventory_order_product, event)
        end
        # If Order Product's quantity is 0, then DEACTIVATE it
        if inventory_order_product.active? && inventory_order_product.quantity_ordered.to_i <= 0
          event = INVENTORY_ORDER_PRODUCT_EVENTS::DEACTIVATE
          change_state(inventory_order_product, event)
        end
      end
    end
  end
end