#
# Module to handle functionalities related to Orders
#
module MarketplaceOrdersModule
  module V1

    #
    # Get allowed payment details of order
    #
    module UpdateCartResponse
      extend self

      def build(result)
        cart = result[:cart]
        additional_details = result[:additional_details]
        is_checkout = result[:is_checkout]

        # Fetch cart details with pricing info
        cart_hash = get_cart_details(cart, {is_checkout: is_checkout, additional_details: additional_details})

        # Fetch third party order hashes
        order = cart.order
        order_extras_hash = get_order_extras(order)

        products_change_message = get_products_change_message(additional_details)

        cart_response_decorator = MarketplaceOrdersModule::V1::CartsResponseDecorator
        if products_change_message.present?
          return cart_response_decorator.state_change_response(cart_hash, order_extras_hash, products_change_message)
        else
          return cart_response_decorator.success_response(cart_hash, order_extras_hash)
        end
      end

      def get_cart_details(cart, options)
        is_checkout = options[:is_checkout]
        additional_details = options[:additional_details]
        # Fetch cart details with pricing info
        cart_hash = MarketplaceOrdersModule::V1::CartHelper.get_cart_complete_details(cart, false)
        # In case of checkout-update-call, recompute all order_products again (done in cart_service)
        # and append those modified products to the response with qty 0 to notify frontend app
        if is_checkout
          additional_order_products = get_inactive_and_out_of_stock_order_products(additional_details)
          cart_hash[:order_products] = cart_hash[:order_products] | additional_order_products
        end
        return cart_hash
      end

      def get_inactive_and_out_of_stock_order_products(additional_details)
        order_products_helper = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsHelper
        order_products = order_products_helper.get_inactive_and_out_of_stock_order_products(additional_details) || []
        op_response_decorator = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsResponseDecorator
        order_products_array = op_response_decorator.get_complete_order_products_array(order_products, false)
        return order_products_array
      end

      def get_order_extras(order)
        third_party_orders = MarketplaceOrdersModule::V1::ThirdPartyOrderHelper.get_active_or_pending_third_party_orders(order)
        orders_hash = MarketplaceOrdersModule::V1::ThirdPartyOrderMapper.map_third_party_orders_to_hash(third_party_orders)
      end

      def get_products_change_message(additional_details)
        products_change_message = ''
        if additional_details.present?
          order_products_helper = MarketplaceOrdersModule::V1::MarketplaceCartsModule::V1::OrderProductsHelper
          products_change_message = order_products_helper.generate_products_change_message(additional_details)
        end
        return products_change_message
      end

    end
  end
end
