#
# Module to handle functionalities related to Orders
#
module MarketplaceOrdersModule
  module V1

    #
    # Get allowed payment details of order
    #
    module GetPaymentDetailsResponse
      extend self

      def build(result)
        MarketplaceOrdersModule::V1::OrdersResponseDecorator.create_get_payment_details_response({
          payment_context: result
        })
      end

    end
  end
end
