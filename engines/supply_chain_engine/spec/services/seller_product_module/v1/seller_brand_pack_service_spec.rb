#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SellerProductModule::V1::SellerBrandPackService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:sbp_service) { SellerProductModule::V1::SellerBrandPackService.new }
      let(:sbp_dao) { SellerProductModule::V1::SellerBrandPackDao }
      let(:seller_dao) { SupplyChainModule::V1::SellerDao }
      let(:wbp_dao) { WarehouseProductModule::V1::WarehouseBrandPackDao }
      let(:brand_pack_dao) { MasterProductModule::V1::BrandPackDao }
      let(:brand_pack) { FactoryGirl.build_stubbed(:brand_pack) }
      let(:inventory_brand_pack) { FactoryGirl.build_stubbed(:inventory_brand_pack) }
      let(:wbp) { FactoryGirl.build(:warehouse_brand_pack) }
      let(:seller) { FactoryGirl.build_stubbed(:seller) }
      let(:seller_brand_pack) { FactoryGirl.build_stubbed(:seller_brand_pack) }
      let(:request) {
        {
          seller_id: Faker::Number.number(1),
          warehouse_brand_pack_id: Faker::Number.number(1),
          brand_pack_id: Faker::Number.number(1)

        }
      }

      context 'create seller brand pack' do
        it 'with existing sbp' do
          sbp_dao.any_instance.stub(:create).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ sbp_service.create_seller_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong seller_id' do
          expect{ sbp_service.create_seller_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong warehouse_brand_pack_id' do
          expect{ sbp_service.create_seller_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong brand_pack_id' do
          expect{ sbp_service.create_seller_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'without seller_id' do
          expect{ sbp_service.create_seller_brand_pack(request.merge(seller_id: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'wihtout warehouse_brand_pack_id' do
          expect{ sbp_service.create_seller_brand_pack(
            request.merge(warehouse_brand_pack_id: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'wihtout brand_pack_id' do
          expect{ sbp_service.create_seller_brand_pack(request.merge(brand_pack_id: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with new params ' do
          inventory_brand_pack.brand_pack = brand_pack
          wbp.brand_pack = brand_pack
          seller_dao.any_instance.stub(:get_seller_by_id).and_return(seller)
          brand_pack_dao.any_instance.stub(:get_brand_pack_by_id).and_return(brand_pack)
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_return(wbp)
          sbp_dao.any_instance.stub(:create).and_return(seller_brand_pack)
          expect(sbp_service.create_seller_brand_pack(request)).to_not be_nil
        end
      end

      context 'get all seller brand packs' do
        it 'with wrong sort_by' do
          sbp_dao.any_instance.stub(:paginated_seller_brand_pack).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ sbp_service.get_all_seller_brand_packs(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          sbp_dao.any_instance.stub(:paginated_seller_brand_pack).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ sbp_service.get_all_seller_brand_packs(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          sbp_dao.any_instance.stub(:paginated_seller_brand_pack).and_return(seller_brand_pack)
          expect(sbp_service.get_all_seller_brand_packs(request)).to_not be_nil
        end
      end

      context 'change seller_brand_pack state' do
        it 'with wrong transition flow' do
          sbp_dao.any_instance.stub(:change_state).and_raise(custom_error_util::PreConditionRequiredError)
          expect{ sbp_service.change_state(request.merge(id: 1)) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
        end

        it 'with wrong event' do
          sbp_dao.any_instance.stub(:change_state).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ sbp_service.change_state(request.merge(id: 1)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          sbp_dao.any_instance.stub(:change_state).and_raise(custom_error_util::InsufficientDataError)
          expect{ sbp_service.change_state(request) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end

        it 'with correct request' do
          sbp_dao.any_instance.stub(:change_state).and_return(seller_brand_pack)
          expect(sbp_service.change_state(request.merge(id: 1))).to_not be_nil
        end
      end

      context 'update seller_brand_pack' do

        it 'with wrong update id' do
          sbp_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ sbp_service.update_seller_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong seller_id' do
          seller_dao.any_instance.stub(:get_seller_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          sbp_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          expect{ sbp_service.update_seller_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong warehouse_brand_pack_id' do
          seller_dao.any_instance.stub(:get_seller_by_id).and_return(seller)
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          sbp_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          expect{ sbp_service.update_seller_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong brand_pack_id' do
          seller_dao.any_instance.stub(:get_seller_by_id).and_return(seller)
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_return(wbp)
          brand_pack_dao.any_instance.stub(:get_brand_pack_by_id).and_return(nil)
          sbp_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          expect{ sbp_service.update_seller_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          sbp_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          seller_dao.any_instance.stub(:get_seller_by_id).and_return(seller)
          sbp_dao.any_instance.stub(:update).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ sbp_service.update_seller_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with correct request' do
          inventory_brand_pack.brand_pack = brand_pack
          wbp.brand_pack = brand_pack
          seller_dao.any_instance.stub(:get_seller_by_id).and_return(seller)
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_return(wbp)
          brand_pack_dao.any_instance.stub(:get_brand_pack_by_id).and_return(brand_pack)
          sbp_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          sbp_dao.any_instance.stub(:update).and_return(seller_brand_pack)
          expect(sbp_service.update_seller_brand_pack(request.merge(id: 1))).to_not be_nil
        end
      end

      context 'get seller brand pack' do
        let(:id) { { id: Faker::Number.number(1) } }
        it 'with wrong id' do
          sbp_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ sbp_service.get_seller_brand_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          sbp_dao.any_instance.stub(:get_seller_brand_pack_by_id).and_return(seller_brand_pack)
          expect(sbp_service.get_seller_brand_pack(request)).to_not be_nil
        end
      end
    end
  end
end