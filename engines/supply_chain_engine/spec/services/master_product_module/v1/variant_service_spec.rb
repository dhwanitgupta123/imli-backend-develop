#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::VariantService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:variant_service) { MasterProductModule::V1::VariantService.new }

      let(:variant_dao) { MasterProductModule::V1::VariantDao }

      let(:variant) { FactoryGirl.create(:variant) }

      let(:variant_request) {
        {
          name: Faker::Company.name,
          initials: Faker::Lorem.characters(2)
        }
      }

      context 'create variant' do
        it 'with invalid arguments' do
          expect_any_instance_of(variant_dao).to receive(:create).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{ variant_service.create_variant(variant_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with valid req' do
          variant_dao.any_instance.stub(:update).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          variant_service.create_variant(variant_request)
        end
      end

      context 'update variant' do
        it 'with invalid arguments' do
          variant_dao.any_instance.stub(:update).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{ variant_service.update_variant(variant_request.merge({ id: 1 })) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with valid req' do
          variant_dao.any_instance.stub(:update).and_return(variant)
          variant_service.update_variant(variant_request.merge({ id: variant.id }))
          variant.destroy
        end
      end
    end
  end
end
