#
# Module to handle all the functionalities related to warehouse product product
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe WarehouseProductModule::V1::WarehouseBrandPackService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:wbp_service) { WarehouseProductModule::V1::WarehouseBrandPackService.new }
      let(:wbp_dao) { WarehouseProductModule::V1::WarehouseBrandPackDao }
      let(:warehouse_dao) { SupplyChainModule::V1::WarehouseDao }
      let(:brand_pack_dao) { MasterProductModule::V1::BrandPackDao }
      let(:inventory_brand_pack_dao) { InventoryProductModule::V1::InventoryBrandPackDao }
      let(:seller_brand_pack_service) { SellerProductModule::V1::SellerBrandPackService }
      let(:marketplace_brand_pack_service) { MarketplaceProductModule::V1::MarketplaceBrandPackService }
      let(:seller_service) { SupplyChainModule::V1::SellerService }
      let(:wbp) { FactoryGirl.build(:warehouse_brand_pack) }
      let(:warehouse) { FactoryGirl.build_stubbed(:warehouse) }
      let(:inventory_brand_pack) { FactoryGirl.build_stubbed(:inventory_brand_pack) }
      let(:brand_pack) { FactoryGirl.build_stubbed(:brand_pack) }
      let(:seller) { FactoryGirl.build_stubbed(:seller) }
      let(:seller_brand_pack) { FactoryGirl.build_stubbed(:seller_brand_pack) }
      let(:marketplace_brand_pack) { FactoryGirl.build_stubbed(:marketplace_brand_pack) }
      let(:request) {
        {
          warehouse_id: Faker::Number.number(1),
          inventory_brand_pack_id: Faker::Number.number(1),
          brand_pack_id: Faker::Number.number(1)

        }
      }

      context 'create wbp' do
        it 'with existing wbp' do
          wbp_dao.any_instance.stub(:create).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ wbp_service.create(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong warehouse_id' do
          expect{ wbp_service.create(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong inventory_brand_pack_id' do
          expect{ wbp_service.create(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong brand_pack_id' do
          expect{ wbp_service.create(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'without warehouse_id' do
          expect{ wbp_service.create(request.merge(warehouse_id: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'wihtout inventory_brand_pack_id' do
          expect{ wbp_service.create(request.merge(inventory_brand_pack_id: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'wihtout brand_pack_id' do
          expect{ wbp_service.create(request.merge(brand_pack_id: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with new params ' do
          inventory_brand_pack.brand_pack = brand_pack
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_return(warehouse)
          brand_pack_dao.any_instance.stub(:get_brand_pack_by_id).and_return(brand_pack)
          inventory_brand_pack_dao.any_instance.stub(:get_by_id).and_return(inventory_brand_pack)
          expect_any_instance_of(seller_service).to receive(:get_all_sellers).and_return({seller: [seller]})
          expect_any_instance_of(seller_brand_pack_service).to receive(:create_seller_brand_pack).and_return(seller_brand_pack)
          expect_any_instance_of(marketplace_brand_pack_service).to receive(:create_marketplace_brand_pack).and_return(marketplace_brand_pack)
          wbp_dao.any_instance.stub(:create).and_return(wbp)
          expect(wbp_service.create(request)).to_not be_nil
        end
      end

      context 'get all wbps' do
        it 'with wrong sort_by' do
          wbp_dao.any_instance.stub(:paginated_wbps).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ wbp_service.get_all_wbps(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          wbp_dao.any_instance.stub(:paginated_wbps).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ wbp_service.get_all_wbps(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          wbp_dao.any_instance.stub(:paginated_wbps).and_return(wbp)
          expect(wbp_service.get_all_wbps(request)).to_not be_nil
        end
      end

      context 'change wbp state' do
        it 'with wrong transition flow' do
          wbp_dao.any_instance.stub(:change_state).and_raise(custom_error_util::PreConditionRequiredError)
          expect{ wbp_service.change_state(request.merge(id: 1)) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
        end

        it 'with wrong event' do
          wbp_dao.any_instance.stub(:change_state).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ wbp_service.change_state(request.merge(id: 1)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          wbp_dao.any_instance.stub(:change_state).and_raise(custom_error_util::InsufficientDataError)
          expect{ wbp_service.change_state(request) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end

        it 'with correct request' do
          wbp_dao.any_instance.stub(:change_state).and_return(wbp)
          expect(wbp_service.change_state(request.merge(id: 1))).to_not be_nil
        end
      end

      context 'update wbp' do

        it 'with wrong update id' do
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ wbp_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong warehouse_id' do
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_return(wbp)
          expect{ wbp_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong inventory_brand_pack_id' do
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_return(warehouse)
          inventory_brand_pack_dao.any_instance.stub(:get_by_id).and_return(nil)
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_return(wbp)
          expect{ wbp_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong brand_pack_id' do
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_return(warehouse)
          inventory_brand_pack_dao.any_instance.stub(:get_by_id).and_return(inventory_brand_pack)
          brand_pack_dao.any_instance.stub(:get_brand_pack_by_id).and_return(nil)
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_return(wbp)
          expect{ wbp_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_return(wbp)
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_return(warehouse)
          wbp_dao.any_instance.stub(:update).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ wbp_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with correct request' do
          inventory_brand_pack.brand_pack = brand_pack
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_return(warehouse)
          inventory_brand_pack_dao.any_instance.stub(:get_by_id).and_return(inventory_brand_pack)
          brand_pack_dao.any_instance.stub(:get_brand_pack_by_id).and_return(brand_pack)
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_return(wbp)
          wbp_dao.any_instance.stub(:update).and_return(wbp)
          expect(wbp_service.update(request.merge(id: 1))).to_not be_nil
        end
      end

      context 'get wbp' do
        let(:id) { { id: Faker::Number.number(1) } }
        it 'with wrong id' do
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ wbp_service.get_wbp(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          wbp_dao.any_instance.stub(:get_wbp_by_id).and_return(wbp)
          expect(wbp_service.get_wbp(request)).to_not be_nil
        end
      end
    end
  end
end
