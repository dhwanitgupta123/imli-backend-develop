#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SearchModule::V1::MarketplaceSellingPackIndexService do
      let(:categorization_api_decorator) { CategorizationModule::V1::MpspCategorizationApiResponseDecorator }
      let(:marketplace_selling_pack_dao) { MarketplaceProductModule::V1::MarketplaceSellingPackDao }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_selling_pack) { FactoryGirl.create(:marketplace_selling_pack) }
      let(:marketplace_selling_pack_repository) { SearchModule::V1::MarketplaceSellingPackRepository }
      let(:mpsp_index_service_class) { SearchModule::V1::MarketplaceSellingPackIndexService }
      let(:mpsp_index_cache_client) { CacheModule::V1::MpspIndexCacheClient }
      let(:query_log_worker) { LoggingModule::V1::QueryLogWorker }
      let(:mpsp_hash) {
        {
          id: Faker::Number.number(1),
          display_name: Faker::Lorem.characters(10),
          description: Faker::Lorem.characters(10)
        }
      }
      context 'search in index' do
        it 'with valid repository' do
          marketplace_selling_pack_model = SearchModule::V1::MarketplaceSellingPackSearchModel.new
          marketplace_selling_pack_index_service = SearchModule::V1::MarketplaceSellingPackIndexService.new
          marketplace_selling_pack_array = []
          marketplace_selling_pack_array.push(marketplace_selling_pack)
          expect(categorization_api_decorator).to receive(:create_display_product_hash).and_return(mpsp_hash)
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_all).and_return({ elements: marketplace_selling_pack_array })
          expect_any_instance_of(mpsp_index_service_class).to receive(:repository_exists?).twice.and_return(true)
          expect_any_instance_of(mpsp_index_service_class).to receive(:index_exists?).and_return(true)
          expect_any_instance_of(mpsp_index_service_class).to receive(:delete_previous_index).and_return(true)
          expect_any_instance_of(mpsp_index_service_class).to receive(:save).and_return(true)
          marketplace_selling_pack_index_service.initialize_index('true')
          marketplace_selling_pack_model_reader = SearchModule::V1::MarketplaceSellingPackModelReader.new({})
          expect_any_instance_of(mpsp_index_service_class).to receive(:search).and_return([marketplace_selling_pack_model])

          data = marketplace_selling_pack_index_service.get_results(Faker::Lorem.characters(10))

          expect(data).to eq([marketplace_selling_pack_model])
        end

        it 'with invalid repository' do
          marketplace_selling_pack_index_service = SearchModule::V1::MarketplaceSellingPackIndexService.new
          expect_any_instance_of(mpsp_index_service_class).to receive(:repository_exists?).and_return(false)
          response = marketplace_selling_pack_index_service.initialize_index
          expect(response).to eq(false)
        end
      end

      context 'get paginated results' do
        it 'with cached results' do
          attribute = JSON.parse(marketplace_selling_pack.to_json)
          marketplace_selling_pack_model = SearchModule::V1::MarketplaceSellingPackSearchModel.new({'product' => attribute})
          expect(mpsp_index_cache_client).to receive(:get_mpsp).and_return([marketplace_selling_pack_model])
          expect(query_log_worker).to receive(:perform_async).and_return(true)

          marketplace_selling_pack_index_service = SearchModule::V1::MarketplaceSellingPackIndexService.new
          result_hash = marketplace_selling_pack_index_service.get_paginated_results({query: Faker::Lorem.characters(10)})
          expect(result_hash[:mpsp_model_reader]).not_to be_nil
        end

        it 'without cach results' do
          attribute = JSON.parse(marketplace_selling_pack.to_json)
          marketplace_selling_pack_model = SearchModule::V1::MarketplaceSellingPackSearchModel.new({'product' => attribute})
          expect(mpsp_index_cache_client).to receive(:get_mpsp).and_return([])
          expect_any_instance_of(mpsp_index_service_class).to receive(:get_results).and_return([marketplace_selling_pack_model])
          expect(query_log_worker).to receive(:perform_async).and_return(true)
          expect(mpsp_index_cache_client).to receive(:set_mpsp).and_return(true)

          marketplace_selling_pack_index_service = SearchModule::V1::MarketplaceSellingPackIndexService.new
          result_hash = marketplace_selling_pack_index_service.get_paginated_results({query: Faker::Lorem.characters(10)})
          expect(result_hash[:mpsp_model_reader]).not_to be_nil
        end
      end
    end
  end
end
