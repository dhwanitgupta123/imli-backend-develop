#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::MarketplaceSellingPackService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_selling_pack_service) { MarketplaceProductModule::V1::MarketplaceSellingPackService.new }
      let(:marketplace_brand_pack_dao) { MarketplaceProductModule::V1::MarketplaceBrandPackDao }
      let(:marketplace_selling_pack_dao) { MarketplaceProductModule::V1::MarketplaceSellingPackDao }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:marketplace_selling_pack) { FactoryGirl.build(:marketplace_selling_pack) }
      let(:marketplace_brand_pack) { FactoryGirl.build(:marketplace_brand_pack) }
      let(:mpsp_category) { FactoryGirl.build(:mpsp_category, mpsp_parent_category_id: 1) }
      let(:pricing_service) { MarketplacePricingModule::V1::MarketplaceSellingPackPricingService }
      let(:mpsp_category_dao) { CategorizationModule::V1::MpspCategoryDao }
      let(:brand_pack_dao) { MasterProductModule::V1::BrandPackDao }
      let(:mpsp_index_service) { SearchModule::V1::MarketplaceSellingPackIndexService }
      let(:mpsp_category_model) { CategorizationModule::V1::MpspCategory }
      let(:marketplace_selling_pack_request) {
        {
          marketplace_brand_packs: [ { marketplace_brand_pack_id: 1, quantity: 3 }],
          is_on_sale: true,
          is_ladder_pricing_active: true,
          max_quantity: 5,
          pricing: {
            taxes: Faker::Commerce.price,
            service_tax: Faker::Commerce.price,
            vat: Faker::Commerce.price,
            discount: Faker::Commerce.price,
            base_selling_price: Faker::Commerce.price.to_d,
            savings: Faker::Commerce.price.to_d,
            mrp: Faker::Commerce.price.to_d,
            ladder: [{
              quantity: 1,
              selling_price: Faker::Commerce.price.to_d,
              additional_savings: Faker::Commerce.price.to_d
            }]
          }
        }
      }

      context 'create marketplace_selling_pack' do
        it 'with valid arguments' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:new_marketplace_selling_pack).
            and_return(marketplace_selling_pack)
          expect_any_instance_of(marketplace_brand_pack_dao).to receive(:get_by_id).
            and_return(marketplace_brand_pack)
          expect(mpsp_category_model).to receive(:find_by_mpsp_category_id).and_return(mpsp_category)
          expect_any_instance_of(mpsp_index_service).to receive(:add_document).
            and_return(true)
          expect(marketplace_selling_pack_service.create_marketplace_selling_pack(
            marketplace_selling_pack_request)).to_not be_nil
        end

        it 'with wrong MPBP id' do
          expect(mpsp_category_model).to receive(:find_by_mpsp_category_id).and_return(mpsp_category)
          expect_any_instance_of(marketplace_brand_pack_dao).to receive(:get_by_id).
            and_raise(custom_error_util::InvalidArgumentsError)
          expect{ marketplace_selling_pack_service.create_marketplace_selling_pack(
            marketplace_selling_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid arguments' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:new_marketplace_selling_pack).
            and_raise(custom_error_util::InvalidArgumentsError)
          expect(mpsp_category_model).to receive(:find_by_mpsp_category_id).and_return(mpsp_category)
          expect_any_instance_of(marketplace_brand_pack_dao).to receive(:get_by_id).
            and_return(marketplace_brand_pack)
          expect{ marketplace_selling_pack_service.create_marketplace_selling_pack(
            marketplace_selling_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update marketplace_selling_pack' do
        it 'with valid arguments' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:update).
            and_return(marketplace_selling_pack)
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_by_id).twice.
            and_return(marketplace_selling_pack)
          expect_any_instance_of(pricing_service).to receive(:update_marketplace_selling_pack_pricing).
            and_return(marketplace_selling_pack.marketplace_selling_pack_pricing)
          expect_any_instance_of(mpsp_index_service).to receive(:update_document).
            and_return(true)
          expect(marketplace_selling_pack_service.update_marketplace_selling_pack(
            marketplace_selling_pack_request)).to_not be_nil
        end

        it 'with wrong MPSP id' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_by_id).
            and_raise(custom_error_util::InvalidArgumentsError)
          expect{ marketplace_selling_pack_service.update_marketplace_selling_pack(
            marketplace_selling_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid arguments' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:update).
            and_raise(custom_error_util::InvalidArgumentsError)
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_by_id).twice.
            and_return(marketplace_selling_pack)
          expect{ marketplace_selling_pack_service.update_marketplace_selling_pack(
            marketplace_selling_pack_request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get all mpsp' do
        request = { sort_by: 'id' }
        it 'with wrong sort_by' do
          marketplace_selling_pack_dao.any_instance.stub(:get_all).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ marketplace_selling_pack_service.get_all_marketplace_selling_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          marketplace_selling_pack_dao.any_instance.stub(:get_all).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ marketplace_selling_pack_service.get_all_marketplace_selling_pack(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          marketplace_selling_pack_dao.any_instance.stub(:get_all).and_return(marketplace_selling_pack)
          expect(marketplace_selling_pack_service.get_all_marketplace_selling_pack(request)).to_not be_nil
        end
      end

      context 'change mpsp state' do
        mpsp = FactoryGirl.create(:marketplace_selling_pack)

        let(:change_state_args) {
        {
          id: mpsp.id,
          event: common_events::ACTIVATE
        }
      }
        it 'with wrong transition flow' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_by_id).thrice.
            and_return(marketplace_selling_pack)
          marketplace_selling_pack_dao.any_instance.stub(:change_state).and_raise(
            custom_error_util::PreConditionRequiredError)
          expect{ marketplace_selling_pack_service.change_marketplace_selling_pack_state(
            change_state_args) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
        end

        it 'with wrong event' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_by_id).thrice.
            and_return(marketplace_selling_pack)
          marketplace_selling_pack_dao.any_instance.stub(:change_state).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ marketplace_selling_pack_service.change_marketplace_selling_pack_state(
            change_state_args) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_by_id).thrice.
            and_return(marketplace_selling_pack)
          marketplace_selling_pack_dao.any_instance.stub(:change_state).and_raise(
            custom_error_util::InsufficientDataError)
          expect{ marketplace_selling_pack_service.change_marketplace_selling_pack_state(
            change_state_args) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end

        it 'with correct request' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_by_id).thrice.
            and_return(marketplace_selling_pack)
          marketplace_selling_pack_dao.any_instance.stub(:change_state).and_return(marketplace_selling_pack)
          expect_any_instance_of(mpsp_index_service).to receive(:update_marketplace_selling_pack_status ).
            and_return(true)
          expect(marketplace_selling_pack_service.change_marketplace_selling_pack_state(
            change_state_args)).to_not be_nil
        end
        mpsp.delete
      end

      context 'get mpsp' do
        let(:id) { { id: Faker::Number.number(1) } }
        it 'with wrong id' do
          marketplace_selling_pack_dao.any_instance.stub(:get_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ marketplace_selling_pack_service.get_marketplace_selling_pack_by_id(id) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          marketplace_selling_pack_dao.any_instance.stub(:get_by_id).and_return(marketplace_selling_pack)
          expect(marketplace_selling_pack_service.get_marketplace_selling_pack_by_id(id)).to_not be_nil
        end
      end

      context 'get sub_categories' do
        let(:id) { { id: Faker::Number.number(1) } }
        it 'with given sub_category id in args' do
          expect(marketplace_selling_pack_service.get_mpsp_sub_categories({mpsp_sub_category_id: id})).to equal(id)
        end

        it 'with given category' do
          expect_any_instance_of(mpsp_category_dao).to receive(:get_mpsp_sub_categories_by_mpsp_categories).and_return(id)
          response = marketplace_selling_pack_service.get_mpsp_sub_categories({mpsp_category_id: id})
          expect(response).to equal(id)
        end

        it 'with given department' do
          expect_any_instance_of(mpsp_category_dao).to receive(:get_mpsp_categories_by_mpsp_department).and_return(id)
          expect_any_instance_of(mpsp_category_dao).to receive(:get_mpsp_sub_categories_by_mpsp_categories).and_return(id)
          response = marketplace_selling_pack_service.get_mpsp_sub_categories({mpsp_department_id: id})
          expect(response).to equal(id)
        end
      end

      context 'get_mpsp_by_department_and_category' do
        it 'with sub_category' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_mpsps_by_mpsp_sub_categories).and_return(marketplace_selling_pack)

          response = marketplace_selling_pack_service.get_mpsp_by_mpsp_department_and_mpsp_category({mpsp_sub_category_id: 1})
          expect(response).to_not be_nil
        end

        it 'with nill args' do
          expect_any_instance_of(marketplace_selling_pack_dao).to receive(:get_all).and_return(marketplace_selling_pack)
          expect(marketplace_selling_pack_dao).to receive(:get_aggregated_not_deleted_mpsps).
            and_return(marketplace_selling_pack)
          response = marketplace_selling_pack_service.get_mpsp_by_mpsp_department_and_mpsp_category({})
          expect(response).to_not be_nil
        end
      end
    end
  end
end
