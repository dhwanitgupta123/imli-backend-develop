#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::WarehouseService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:warehouse_service) { SupplyChainModule::V1::WarehouseService.new }
      let(:cash_and_carry_dao) { SupplyChainModule::V1::CashAndCarryDao }
      let(:warehouse_dao) { SupplyChainModule::V1::WarehouseDao }
      let(:c_and_c) { FactoryGirl.build(:cash_and_carry) }
      let(:warehouse) { FactoryGirl.build(:warehouse) }

      let(:request) {
        {
          name: Faker::Company.name,
          cash_and_carry_id: Faker::Number.number(1)
        }
      }

      context 'create warehouse' do
        it 'with existing warehouse' do
          warehouse_dao.any_instance.stub(:create).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ warehouse_service.create(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong cash_and_carry_id' do
          expect{ warehouse_service.create(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'wihtout cash_and_carry_id' do
          expect{ warehouse_service.create(request.merge(cash_and_carry_id: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with new params ' do
          cash_and_carry_dao.any_instance.stub(:get_cash_and_carry_by_id).and_return(c_and_c)
          warehouse_dao.any_instance.stub(:create).and_return(warehouse)
          expect(warehouse_service.create(request)).to_not be_nil
        end
      end

      context 'get all warehouses' do
        it 'with wrong sort_by' do
          warehouse_dao.any_instance.stub(:paginated_warehouse).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ warehouse_service.get_all_warehouses(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          warehouse_dao.any_instance.stub(:paginated_warehouse).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ warehouse_service.get_all_warehouses(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          warehouse_dao.any_instance.stub(:paginated_warehouse).and_return(warehouse)
          expect(warehouse_service.get_all_warehouses(request)).to_not be_nil
        end
      end

      context 'change warehouse state' do
        it 'with wrong transition flow' do
          warehouse_dao.any_instance.stub(:change_state).and_raise(custom_error_util::PreConditionRequiredError)
          expect{ warehouse_service.change_state(request.merge(id: 1)) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
        end

        it 'with wrong event' do
          warehouse_dao.any_instance.stub(:change_state).and_raise(custom_error_util::InvalidArgumentsError)
          expect{ warehouse_service.change_state(request.merge(id: 1)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          warehouse_dao.any_instance.stub(:change_state).and_raise(custom_error_util::InsufficientDataError)
          expect{ warehouse_service.change_state(request) }.to raise_error(
            custom_error_util::InsufficientDataError)
        end

        it 'with correct request' do
          warehouse_dao.any_instance.stub(:change_state).and_return(warehouse)
          expect(warehouse_service.change_state(request.merge(id: 1))).to_not be_nil
        end
      end

      context 'update warehouse' do

        it 'with wrong update id' do
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ warehouse_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong cash_and_carry_id' do
          cash_and_carry_dao.any_instance.stub(:get_cash_and_carry_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_return(warehouse)
          expect{ warehouse_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with wrong request' do
          cash_and_carry_dao.any_instance.stub(:get_cash_and_carry_by_id).and_return(c_and_c)
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_return(warehouse)
          warehouse_dao.any_instance.stub(:update).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ warehouse_service.update(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with correct request' do
          cash_and_carry_dao.any_instance.stub(:get_cash_and_carry_by_id).and_return(c_and_c)
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_return(warehouse)
          warehouse_dao.any_instance.stub(:update).and_return(warehouse)
          expect(warehouse_service.update(request.merge(id: 1))).to_not be_nil
        end
      end

      context 'get warehouse' do
        let(:id) { { id: Faker::Number.number(1) } }
        it 'with wrong id' do
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_raise(
            custom_error_util::InvalidArgumentsError)
          expect{ warehouse_service.get_warehouse(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          warehouse_dao.any_instance.stub(:get_warehouse_by_id).and_return(warehouse)
          expect(warehouse_service.get_warehouse(request)).to_not be_nil
        end
      end
    end
  end
end
