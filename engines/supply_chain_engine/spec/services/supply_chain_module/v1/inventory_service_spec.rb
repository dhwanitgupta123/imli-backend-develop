#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::InventoryService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:inventory_service) { SupplyChainModule::V1::InventoryService.new }

      let(:inventory_dao) { SupplyChainModule::V1::InventoryDao }
      let(:distributor_dao) { SupplyChainModule::V1::DistributorDao }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:distributor) { FactoryGirl.build_stubbed(:distributor) }
      let(:inventory) { FactoryGirl.build(:inventory, distributor: distributor) }

      let(:inventory_request) {
        {
          name: Faker::Company.name,
          distributor_id: distributor.id
        }
      }

      let(:update_inventory_request) {
        {
          id: Faker::Number.number(1),
          name: Faker::Company.name
        }
      }

      let(:change_state_args) {
        {
          id: Faker::Number.number(1),
          event: common_events::ACTIVATE
        }
      }

      context 'create inventory' do
        it 'with valid arguments' do
          expect_any_instance_of(inventory_dao).to receive(:create).
            and_return(inventory)
          expect_any_instance_of(distributor_dao).to receive(:get_by_id).
            and_return(distributor)

          response = inventory_service.create_inventory(inventory_request)

          expect(response).to equal(inventory)
        end

        it 'with invalid arguments' do

          expect_any_instance_of(distributor_dao).to receive(:get_by_id).
            and_return(distributor)
          expect_any_instance_of(inventory_dao).to receive(:create).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{ inventory_service.create_inventory(inventory_request.merge(distributor_id: nil)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update inventory' do
        it 'with non existing inventory' do
          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(nil)
          expect{ inventory_service.update_inventory(update_inventory_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)       
        end

        it 'with existing inventory' do
          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(inventory)
          expect_any_instance_of(inventory_dao).to receive(:update).
            and_return(inventory)
          response = inventory_service.update_inventory(update_inventory_request)
          expect(response).to equal(inventory)
        end
      end

      context 'change inventory state' do

        it 'with existing inventory' do
          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(inventory)
          expect_any_instance_of(inventory_dao).to receive(:change_state).
            and_return(inventory)
          response = inventory_service.change_inventory_state(change_state_args)
          expect(response).to equal(inventory)
        end

        it 'with non existing inventory' do
          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(inventory)
           expect_any_instance_of(inventory_dao).to receive(:change_state).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ inventory_service.change_inventory_state(change_state_args) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get inventory by id' do
        it 'with existing inventory' do
          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(inventory)
          response = inventory_service.get_inventory_by_id(Faker::Number.number(1))
          expect(response).to equal(inventory)
        end

        it 'with non existing inventory' do
          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(nil)
          expect{ inventory_service.get_inventory_by_id(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end
