#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe InventoryProductModule::V1::InventoryBrandPackService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:inventory_brand_pack_service) { InventoryProductModule::V1::InventoryBrandPackService.new }

      let(:inventory_brand_pack_dao) { InventoryProductModule::V1::InventoryBrandPackDao }
      let(:inventory_dao) { SupplyChainModule::V1::InventoryDao }
      let(:brand_pack_dao) { MasterProductModule::V1::BrandPackDao }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:distributor) { FactoryGirl.build_stubbed(:distributor) }
      let(:inventory) { FactoryGirl.build_stubbed(:inventory, distributor: distributor) }
      let(:company) { FactoryGirl.build_stubbed(:company) }
      let(:brand) { FactoryGirl.build_stubbed(:brand, company: company) }
      let(:sub_brand) { FactoryGirl.build_stubbed(:sub_brand, brand: brand) }
      let(:product) { FactoryGirl.build_stubbed(:product, sub_brand: sub_brand) }
      let(:brand_pack) { FactoryGirl.build_stubbed(:brand_pack, product: product) }
      let(:inventory_brand_pack) { FactoryGirl.build(:inventory_brand_pack, inventory: inventory) }

      let(:inventory_brand_pack_request) {
        {
          name: Faker::Company.name,
          inventory_id: inventory.id,
          brand_pack_id: brand_pack.id
        }
      }

      let(:update_inventory_brand_pack_request) {
        {
          id: Faker::Number.number(1),
          brand_pack_id: Faker::Number.number(1),
          inventory_id: Faker::Number.number(1)
        }
      }

      let(:change_state_args) {
        {
          id: Faker::Number.number(1),
          event: common_events::ACTIVATE
        }
      }

      context 'create inventory_brand_pack' do
        it 'with valid arguments' do
          expect_any_instance_of(inventory_brand_pack_dao).to receive(:create).
            and_return(inventory_brand_pack)
          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(inventory)
          expect_any_instance_of(brand_pack_dao).to receive(:get_by_id).
            and_return(brand_pack)
          response = inventory_brand_pack_service.create_inventory_brand_pack(inventory_brand_pack_request)

          expect(response).to equal(inventory_brand_pack)
        end

        it 'with nil inventory id' do

          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(nil)

          expect{ inventory_brand_pack_service.create_inventory_brand_pack(inventory_brand_pack_request.merge(brand_pack_id: nil)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with nil brand_pack id' do

          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(inventory)
          expect_any_instance_of(brand_pack_dao).to receive(:get_by_id).
            and_return(nil)

          expect{ inventory_brand_pack_service.create_inventory_brand_pack(inventory_brand_pack_request.merge(inventory_id: nil)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update inventory_brand_pack' do
        it 'with non existing inventory_brand_pack' do
          expect_any_instance_of(brand_pack_dao).to receive(:get_by_id).
            and_return(brand_pack)
          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(inventory)
          expect_any_instance_of(inventory_brand_pack_dao).to receive(:get_by_id).
            and_return(nil)
          expect{ inventory_brand_pack_service.update_inventory_brand_pack(update_inventory_brand_pack_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)       
        end

        it 'with existing inventory_brand_pack' do
          expect_any_instance_of(inventory_dao).to receive(:get_by_id).
            and_return(inventory)
          expect_any_instance_of(brand_pack_dao).to receive(:get_by_id).
            and_return(brand_pack)
          expect_any_instance_of(inventory_brand_pack_dao).to receive(:get_by_id).
            and_return(inventory_brand_pack)
          expect_any_instance_of(inventory_brand_pack_dao).to receive(:update).
            and_return(inventory_brand_pack)
          response = inventory_brand_pack_service.update_inventory_brand_pack(update_inventory_brand_pack_request)
          expect(response).to equal(inventory_brand_pack)
        end
      end

      context 'change inventory_brand_pack state' do

        it 'with existing inventory_brand_pack' do
          expect_any_instance_of(inventory_brand_pack_dao).to receive(:get_by_id).
            and_return(inventory_brand_pack)
          expect_any_instance_of(inventory_brand_pack_dao).to receive(:change_state).
            and_return(inventory_brand_pack)
          response = inventory_brand_pack_service.change_inventory_brand_pack_state(change_state_args)
          expect(response).to equal(inventory_brand_pack)
        end

        it 'with non existing inventory_brand_pack' do
          expect_any_instance_of(inventory_brand_pack_dao).to receive(:get_by_id).
            and_return(inventory_brand_pack)
           expect_any_instance_of(inventory_brand_pack_dao).to receive(:change_state).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ inventory_brand_pack_service.change_inventory_brand_pack_state(change_state_args) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get inventory_brand_pack by id' do
        it 'with existing inventory_brand_pack' do
          expect_any_instance_of(inventory_brand_pack_dao).to receive(:get_by_id).
            and_return(inventory_brand_pack)
          response = inventory_brand_pack_service.get_inventory_brand_pack_by_id(Faker::Number.number(1))
          expect(response).to equal(inventory_brand_pack)
        end

        it 'with non existing inventory_brand_pack' do
          expect_any_instance_of(inventory_brand_pack_dao).to receive(:get_by_id).
            and_return(nil)
          expect{ inventory_brand_pack_service.get_inventory_brand_pack_by_id(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end
