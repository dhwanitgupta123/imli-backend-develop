#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ClusterModule::V1::ClusterService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:cluster_service) { ClusterModule::V1::ClusterService.new({}) }
      let(:cluster_dao) { ClusterModule::V1::ClusterDao }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:cluster_types) { ClusterModule::V1::ClusterTypes }
      let(:cluster) { FactoryGirl.build(:cluster) }
      let(:cluster_levels) { ClusterModule::V1::ClusterLevels }

      let(:cluster_request) {
        {
          label: Faker::Company.name,
          cluster_type: Faker::Number.number(1),
          context: {
            level: Faker::Number.number(1),
            level_id: Faker::Number.number(1)
          }
        }
      }

      let(:update_cluster_request) {
        {
          id: Faker::Number.number(1),
          cluster_type: Faker::Number.number(1),
          context: {
            level: Faker::Number.number(1),
            level_id: Faker::Number.number(1)
          }
        }
      }

      let(:change_state_args) {
        {
          id: Faker::Number.number(1),
          event: common_events::ACTIVATE
        }
      }

      let(:pagination_params) {
        {
          global: true
        }
      }

      let(:cluster_type) {
        {
          cluster_type: 1,
          display_cluster_type: 'Banner'
        }
      }
      
      let(:cluster_level) {
        {
          level: 2,
          display_level: 'Department'
        }
      }

      context 'create cluster' do
        it 'with valid arguments' do
          expect(cluster_levels).to receive(:get_cluster_level_by_level).and_return(cluster_level) 
          expect_any_instance_of(cluster_dao).to receive(:create).
            and_return(cluster)
          expect(cluster_types).to receive(:get_cluster_type_by_type).and_return(cluster_type) 
          response = cluster_service.create_cluster(cluster_request)

          expect(response).to equal(cluster)
        end
      end

      context 'update cluster' do
        it 'with non existing cluster' do
          expect(cluster_levels).to receive(:get_cluster_level_by_level).and_return(cluster_level) 
          expect(cluster_types).to receive(:get_cluster_type_by_type).and_return(cluster_type) 
          expect_any_instance_of(cluster_dao).to receive(:get_by_id).
            and_return(nil)
          expect{ cluster_service.update_cluster(update_cluster_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)       
        end

        it 'with existing cluster' do
          expect(cluster_levels).to receive(:get_cluster_level_by_level).and_return(cluster_level) 
          expect(cluster_types).to receive(:get_cluster_type_by_type).and_return(cluster_type) 
          expect_any_instance_of(cluster_dao).to receive(:get_by_id).
            and_return(cluster)
          expect_any_instance_of(cluster_dao).to receive(:update).
            and_return(cluster)
          response = cluster_service.update_cluster(update_cluster_request)
          expect(response).to equal(cluster)
        end
      end

      context 'change cluster state' do

        it 'with existing cluster' do
          expect_any_instance_of(cluster_dao).to receive(:get_by_id).
            and_return(cluster)
          expect_any_instance_of(cluster_dao).to receive(:change_state).
            and_return(cluster)
          response = cluster_service.change_cluster_state(change_state_args)
          expect(response).to equal(cluster)
        end

        it 'with non existing cluster' do
          expect_any_instance_of(cluster_dao).to receive(:get_by_id).
            and_return(cluster)
           expect_any_instance_of(cluster_dao).to receive(:change_state).
            and_raise(custom_error_util::InvalidArgumentsError.new)
          expect{ cluster_service.change_cluster_state(change_state_args) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get cluster by id' do
        it 'with existing cluster' do
          expect_any_instance_of(cluster_dao).to receive(:get_by_id).
            and_return(cluster)
          response = cluster_service.get_cluster_by_id(Faker::Number.number(1))
          expect(response).to equal(cluster)
        end

        it 'with non existing cluster' do
          expect_any_instance_of(cluster_dao).to receive(:get_by_id).
            and_return(nil)
          expect{ cluster_service.get_cluster_by_id(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'get filtered clusters' do

        it 'return clusters with level and level_id' do
          clusters = []
          clusters.push(cluster)
          expect_any_instance_of(cluster_dao).to receive(:get_clusters_by_level_and_level_id).
            and_return(clusters)
          response = cluster_service.get_filtered_clusters(pagination_params.merge(global: nil, level: Faker::Company.name, level_id: Faker::Number.number(1)))
          expect(response).to equal(clusters)
        end

        it 'return cluster with level' do
          clusters = []
          clusters.push(cluster)
          expect_any_instance_of(cluster_dao).to receive(:get_clusters_by_level).
            and_return(clusters)
          response = cluster_service.get_filtered_clusters(pagination_params.merge(global: nil, level: Faker::Company.name))
          expect(response).to equal(clusters)
        end

        it 'return cluster with level id' do
          clusters = []
          clusters.push(cluster)
          expect_any_instance_of(cluster_dao).to receive(:get_clusters_by_level_id).
            and_return(clusters)
          response = cluster_service.get_filtered_clusters(pagination_params.merge(global: nil, level_id: Faker::Number.number(1)))
          expect(response).to equal(clusters)
        end

        it 'return all clusters' do
          clusters = []
          clusters.push(cluster)
          expect_any_instance_of(cluster_dao).to receive(:get_all).
            and_return({elements: clusters})
          response = cluster_service.get_filtered_clusters({})
          expect(response[:elements]).to equal(clusters)
        end
      end
    end
  end
end
