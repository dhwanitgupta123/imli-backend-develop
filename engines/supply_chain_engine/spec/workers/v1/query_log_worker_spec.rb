#
# This module is responsible for logging meta data
#
module LoggingModule
  #
  # Version1 for logging module
  #
  module V1
    require 'rails_helper'
    RSpec.describe QueryLogWorker, type: :worker do

      let(:query_log_worker) { LoggingModule::V1::QueryLogWorker }

      it 'enqueues a QueryLogWorker' do
        query_log_worker.perform_async
        expect(query_log_worker).to have_enqueued_job
      end
          
      #to check the queue name
      it { is_expected.to be_processed_in :default }
    end
  end
end
