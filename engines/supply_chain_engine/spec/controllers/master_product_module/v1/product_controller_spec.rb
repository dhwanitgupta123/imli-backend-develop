#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::ProductController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_product) { MasterProductModule::V1::AddProductApi }
      let(:update_product) { MasterProductModule::V1::UpdateProductApi }
      let(:get_products) { MasterProductModule::V1::GetProductsApi }
      let(:get_product) { MasterProductModule::V1::GetProductApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:change_product_state_api) { MasterProductModule::V1::ChangeProductStateApi }
      let(:stub_ok_response) {
          {
            payload: {
              products: [
                {
                  product: {
                    name: Faker::Lorem.characters(6)
                  }
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add product' do
        it 'returns created product successfully' do
          add_product.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_product, product: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_product.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_product, product: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update product' do
        it 'returns updated product successfully' do
          update_product.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_product, id: 1, product: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_product.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_product, id: 1, product: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all products' do
        it 'returns all products successfully' do
          get_products.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_products
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'change product state' do
        it 'return updated product' do
          change_product_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, product: { event: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_product_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, product: { event: 0 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete product' do
        it 'return updated product' do
          change_product_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_product, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_product_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_product, id: 2
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get product' do
        it 'with valid argument' do
          get_product.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_product, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'with invalid arguments' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_product.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_product, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
