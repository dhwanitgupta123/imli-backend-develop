#
# Module to handle all the functionalities related to master product
#
module ClusterModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ClusterModule::V1::ClustersApiController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:get_clusters) { ClusterModule::V1::GetClustersApi }
      let(:get_cluster) { ClusterModule::V1::GetClusterApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:stub_ok_response) {
          {
            payload: {
              clusters: [
                {
                  cluster: {
                    label: Faker::Lorem.characters(6)
                  }
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'get all clusters' do
        it 'returns all clusters successfully' do
          get_clusters.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_clusters
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end


      describe 'get cluster' do
        it 'with valid argument' do
          get_cluster.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_cluster, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'with invalid arguments' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_cluster.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_cluster, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
