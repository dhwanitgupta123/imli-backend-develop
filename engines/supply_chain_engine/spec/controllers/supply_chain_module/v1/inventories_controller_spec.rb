#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::InventoriesController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_inventories) { SupplyChainModule::V1::AddInventoryApi }
      let(:update_inventories) { SupplyChainModule::V1::UpdateInventoryApi }
      let(:get_inventories) { SupplyChainModule::V1::GetInventoriesApi }
      let(:change_inventories_state_api) { SupplyChainModule::V1::ChangeInventoryStateApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:stub_ok_response) {
          {
            payload: {
              inventories: [
                name: Faker::Lorem.characters(6)
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add inventories' do
        it 'returns created inventories successfully' do
          add_inventories.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_inventory, inventory: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_inventories.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_inventory, inventory: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update inventories' do
        it 'returns updated inventories successfully' do
          update_inventories.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_inventory, id: 1, inventory: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_inventories.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_inventory, id: 1, inventory: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all inventoriess' do
        it 'returns all inventoriess successfully' do
          get_inventories.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_inventories
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'change inventories state' do
        it 'return updated inventories' do
          change_inventories_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, inventory: { event: common_events::ACTIVATE }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_inventories_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, inventory: { event: common_events::ACTIVATE }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete inventories' do
        it 'return updated inventories' do
          change_inventories_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_inventory, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_inventories_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_inventory, id: 2
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
