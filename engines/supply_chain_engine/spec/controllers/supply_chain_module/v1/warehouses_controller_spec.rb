#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::WarehousesController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add) { SupplyChainModule::V1::AddWarehouseApi }
      let(:change_state) { SupplyChainModule::V1::ChangeWarehouseStateApi }
      let(:get_all_api) { SupplyChainModule::V1::GetAllWarehouseApi }
      let(:get_api) { SupplyChainModule::V1::GetWarehouseApi }
      let(:update) { SupplyChainModule::V1::UpdateWarehouseApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:stub_ok_response) {
          {
            payload: {
              warehouses: [
                {
                  warehouse: {
                    name: Faker::Lorem.characters(6)
                  }
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add warehouse' do
        it 'returns created warehouse successfully' do
          add.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_warehouse, warehouse: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_warehouse, warehouse: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'change_state warehouse' do
        it 'returns updated warehouse successfully' do
          change_state.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, warehouse: { event: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, warehouse: { event: 1 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when soft delete request is sent' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, warehouse: { event: 3 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all warehouses' do
        it 'returns all warehouses successfully' do
          get_all_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_warehouses, warehouse: { order: 'name' }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'get warehouse' do
        it 'return warehouse successfully' do
          get_api.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_warehouse, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when warehouse does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_warehouse, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update warehouse' do
        it 'returns updated warehouse successfully' do
          update.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_warehouse, id: 1, warehouse: { name: Faker::Name.name }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when warehouse does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_warehouse, id: 1, warehouse: {name: Faker::Name.name }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when request does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_warehouse, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete warehouse' do
        it 'return deleted warehouse successfully' do
          change_state.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_warehouse, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when warehouse does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_state.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_warehouse, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
