#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::CompanyController, type: :controller do
      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:add_company) { SupplyChainModule::V1::AddCompanyApi }
      let(:update_company) { SupplyChainModule::V1::UpdateCompanyApi }
      let(:get_companies) { SupplyChainModule::V1::GetCompaniesApi }
      let(:get_company) { SupplyChainModule::V1::GetCompanyApi }
      let(:change_company_state_api) { SupplyChainModule::V1::ChangeCompanyStateApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }
      let(:stub_ok_response) {
          {
            payload: {
              companies: [
                {
                  company: {
                    name: Faker::Lorem.characters(6)
                  }
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }
        }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'add company' do
        it 'returns created company successfully' do
          add_company.any_instance.stub(:enact).and_return(stub_ok_response)
          post :add_company, company: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          add_company.any_instance.stub(:enact).and_return(stub_bad_request_response)
          post :add_company, company: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update company' do
        it 'returns updated company successfully' do
          update_company.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_company, id: 1, company: { name: Faker::Lorem.characters(6) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_company.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_company, id: 1, company: { name: nil }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get all companies' do
        it 'returns all companies successfully' do
          get_companies.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_all_companies
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end
      end

      describe 'change company state' do
        it 'return updated company' do
          change_company_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          put :change_state, id: 1, company: { event: 1 }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_company_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :change_state, id: 1, company: { event: 0 }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'delete company' do
        it 'return updated company' do
          change_company_state_api.any_instance.stub(:enact).and_return(stub_ok_response)
          delete :delete_company, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'return bad request response' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          change_company_state_api.any_instance.stub(:enact).and_return(stub_bad_request_response)
          delete :delete_company, id: 2
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'get company' do
        it 'with valid argument' do
          get_company.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_company, id: 1
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'with invalid arguments' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_company.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_company, id: 1
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
