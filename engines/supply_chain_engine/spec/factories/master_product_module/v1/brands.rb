#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    FactoryGirl.define do
      factory :brand, class: MasterProductModule::V1::Brand do
        name Faker::Company.name
        code Faker::Lorem.characters(5)
        logo_url Faker::Company.logo
        association :company, factory: :company
        initials Faker::Lorem.characters(2)
      end
    end
  end
end
