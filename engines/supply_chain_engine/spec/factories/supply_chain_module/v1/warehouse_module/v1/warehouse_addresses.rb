module SupplyChainModule::V1
  module WarehouseModule::V1
    FactoryGirl.define do
      factory :warehouse_address, class: WarehouseAddress do
        association :warehouse, factory: :warehouse
        association :area, factory: :area
        warehouse_name ::Faker::Name.name
        address_line1 ::Faker::Address.street_address
        address_line2 ::Faker::Address.secondary_address
        landmark ::Faker::Address.street_name
      end
    end
  end
end
