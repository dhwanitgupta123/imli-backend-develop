#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    FactoryGirl.define do
      factory :distributor, class: SupplyChainModule::V1::Distributor do
        name Faker::Company.name
        contact_number Faker::Number.number(10)
      end
    end
  end
end
