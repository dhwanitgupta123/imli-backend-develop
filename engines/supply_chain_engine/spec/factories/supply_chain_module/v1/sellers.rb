#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    FactoryGirl.define do
      factory :seller, class: SupplyChainModule::V1::Seller do
        name Faker::Company.name
      end
    end
  end
end