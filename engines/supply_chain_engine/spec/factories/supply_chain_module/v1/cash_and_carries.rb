#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    FactoryGirl.define do
      factory :cash_and_carry, class: SupplyChainModule::V1::CashAndCarry do
        name Faker::Company.name
      end
    end
  end
end
