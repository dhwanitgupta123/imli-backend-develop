#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    FactoryGirl.define do
      factory :inventory, class: SupplyChainModule::V1::Inventory do
        name Faker::Company.name
        association :distributor, factory: :distributor
      end
    end
  end
end
