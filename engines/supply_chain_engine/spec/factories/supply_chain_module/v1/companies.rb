#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    FactoryGirl.define do
      factory :company, class: SupplyChainModule::V1::Company do
        name Faker::Company.name
        logo_url Faker::Company.logo
        vat_number Faker::Company.duns_number
        tin_number Faker::Company.duns_number
        cst Faker::Company.duns_number
        account_details Faker::Business.credit_card_number
        trade_promotion_margin Faker::Commerce.price
        data_sharing_margin Faker::Commerce.price
        incentives Faker::Lorem.characters(10)
        reconciliation_period Faker::Time.between(DateTime.now - 1, DateTime.now)
        legal_document_url Faker::Internet.url
        initials Faker::Lorem.characters(2)
      end
    end
  end
end