#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    FactoryGirl.define do
      factory :warehouse, class: SupplyChainModule::V1::Warehouse do
        name Faker::Company.name
        association :cash_and_carry, factory: :cash_and_carry
      end
    end
  end
end
