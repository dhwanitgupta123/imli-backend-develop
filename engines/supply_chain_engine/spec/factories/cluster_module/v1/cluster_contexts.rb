#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Model for clusters table
    #
    FactoryGirl.define do
      factory :cluster_context, class: ClusterModule::V1::ClusterContext do
        level Faker::Lorem.characters(9)
        level_id Faker::Number.number(1)
        association :cluster, factory: :cluster
      end
    end
  end
end
