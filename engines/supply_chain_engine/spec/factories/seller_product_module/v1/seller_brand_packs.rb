#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    FactoryGirl.define do
      factory :seller_brand_pack, class: SellerProductModule::V1::SellerBrandPack do
        association :seller, factory: :seller
        association :brand_pack, factory: :brand_pack
        association :seller_pricing, factory: :seller_pricing
      end
    end
  end
end