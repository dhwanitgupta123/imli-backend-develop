#
# Module to handle all the functionalities related to warehouse product module
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    FactoryGirl.define do
      factory :warehouse_brand_pack, class: WarehouseProductModule::V1::WarehouseBrandPack do
        association :warehouse, factory: :warehouse
        association :brand_pack, factory: :brand_pack
      end
    end
  end
end
