#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe ProductController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_product' do
          expect(:post => 'supply_chain/products/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'master_product_module/v1/product',
              'action' => 'add_product'
            })
        end

        it 'routes to #update_product' do
          expect(:put => 'supply_chain/products/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'master_product_module/v1/product',
              'action'=>'update_product',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_products' do
          expect(:get => 'supply_chain/products').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/product',
              'action' => 'get_all_products'
            })
        end

        it 'routes to #change product state' do
          expect(:put => 'supply_chain/products/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/product',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete product' do
          expect(:delete => 'supply_chain/products/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/product',
              'action' => 'delete_product',
              'id'=>'1'
            })
        end

        it 'routes to #get_product' do
          expect(:get => 'supply_chain/products/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/product',
              'action' => 'get_product',
              'id'=>'1'
            })
        end

      end
    end
  end
end