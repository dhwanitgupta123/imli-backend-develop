#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SubBrandController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_sub_brand' do
          expect(:post => 'supply_chain/sub_brands/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'master_product_module/v1/sub_brand',
              'action' => 'add_sub_brand'
            })
        end

        it 'routes to #update_sub_brand' do
          expect(:put => 'supply_chain/sub_brands/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'master_product_module/v1/sub_brand',
              'action'=>'update_sub_brand',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_sub_brands' do
          expect(:get => 'supply_chain/sub_brands').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/sub_brand',
              'action' => 'get_all_sub_brands'
            })
        end

        it 'routes to #change sub brand state' do
          expect(:put => 'supply_chain/sub_brands/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/sub_brand',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete sub_brand' do
          expect(:delete => 'supply_chain/sub_brands/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/sub_brand',
              'action' => 'delete_sub_brand',
              'id'=>'1'
            })
        end

        it 'routes to #get_sub_brands' do
          expect(:get => 'supply_chain/sub_brands/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'master_product_module/v1/sub_brand',
              'action' => 'get_sub_brand',
              'id'=>'1'
            })
        end

      end
    end
  end
end