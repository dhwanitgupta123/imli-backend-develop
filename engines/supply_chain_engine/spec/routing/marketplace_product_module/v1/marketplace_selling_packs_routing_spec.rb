#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MarketplaceSellingPacksController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_marketplace_selling_pack' do
          expect(:post => 'supply_chain/marketplace_selling_packs/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'marketplace_product_module/v1/marketplace_selling_packs',
              'action' => 'add_marketplace_selling_pack'
            })
        end

        it 'routes to #update_marketplace_selling_pack' do
          expect(:put => 'supply_chain/marketplace_selling_packs/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'marketplace_product_module/v1/marketplace_selling_packs',
              'action'=>'update_marketplace_selling_pack',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_marketplace_selling_packs' do
          expect(:get => 'supply_chain/marketplace_selling_packs').to route_to(
            {
              'format' => 'json',
              'controller' => 'marketplace_product_module/v1/marketplace_selling_packs',
              'action' => 'get_all_marketplace_selling_packs'
            })
        end

        it 'routes to #change marketplace_selling pack state' do
          expect(:put => 'supply_chain/marketplace_selling_packs/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'marketplace_product_module/v1/marketplace_selling_packs',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete marketplace_selling_packs' do
          expect(:delete => 'supply_chain/marketplace_selling_packs/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'marketplace_product_module/v1/marketplace_selling_packs',
              'action' => 'delete_marketplace_selling_pack',
              'id'=>'1'
            })
        end

        it 'routes to #get_marketplace_selling_packs' do
          expect(:get => 'supply_chain/marketplace_selling_packs/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'marketplace_product_module/v1/marketplace_selling_packs',
              'action' => 'get_marketplace_selling_pack',
              'id'=>'1'
            })
        end

        it 'routes to #get_mpsp_by_department_and_category' do
          expect(:get => 'supply_chain/mpsp_by_department_and_category').to route_to(
            {
              'format' => 'json',
              'controller' => 'marketplace_product_module/v1/marketplace_selling_packs',
              'action' => 'get_mpsp_by_department_and_category'
            })
        end
      end
    end
  end
end