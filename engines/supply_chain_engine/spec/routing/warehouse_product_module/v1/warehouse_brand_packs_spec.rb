#
# Module to handle all the functionalities related to warehouse product product
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe WarehouseBrandPacksController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_warehouse_brand_pack' do
          expect(:post => 'supply_chain/warehouse_brand_packs/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'warehouse_product_module/v1/warehouse_brand_packs',
              'action' => 'add_warehouse_brand_pack'
            })
        end

        it 'routes to #change_state' do
          expect(:put => 'supply_chain/warehouse_brand_packs/state/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'warehouse_product_module/v1/warehouse_brand_packs',
              'action'=>'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_warehouse_brand_packs' do
          expect(:get => 'supply_chain/warehouse_brand_packs').to route_to(
            {
              'format' => 'json',
              'controller' => 'warehouse_product_module/v1/warehouse_brand_packs',
              'action' => 'get_all_warehouse_brand_packs'
            })
        end

        it 'routes to #get_warehouse_brand_pack' do
          expect(:get => 'supply_chain/warehouse_brand_packs/1').to route_to(
            {
              'id' => '1',
              'format' => 'json',
              'controller' => 'warehouse_product_module/v1/warehouse_brand_packs',
              'action' => 'get_warehouse_brand_pack'
            })
        end

        it 'routes to #update_warehouse_brand_pack' do
          expect(:put => 'supply_chain/warehouse_brand_packs/1').to route_to(
            {
              'id' => '1',
              'format' => 'json',
              'controller' => 'warehouse_product_module/v1/warehouse_brand_packs',
              'action' => 'update_warehouse_brand_pack'
            })
        end

        it 'routes to #delete_warehouse_brand_pack' do
          expect(:delete => 'supply_chain/warehouse_brand_packs/1').to route_to(
            {
              'id' => '1',
              'format' => 'json',
              'controller' => 'warehouse_product_module/v1/warehouse_brand_packs',
              'action' => 'delete_warehouse_brand_pack'
            })
        end
      end
    end
  end
end