#
# Module to handle all the functionalities related to master product
#
module SupplyChainModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe DistributorsController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_distributor' do
          expect(:post => 'supply_chain/distributors/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'supply_chain_module/v1/distributors',
              'action' => 'add_distributor'
            })
        end

        it 'routes to #update_distributor' do
          expect(:put => 'supply_chain/distributors/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'supply_chain_module/v1/distributors',
              'action'=>'update_distributor',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_distributors' do
          expect(:get => 'supply_chain/distributors').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/distributors',
              'action' => 'get_all_distributors'
            })
        end

        it 'routes to #change distributor state' do
          expect(:put => 'supply_chain/distributors/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/distributors',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete distributor' do
          expect(:delete => 'supply_chain/distributors/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/distributors',
              'action' => 'delete_distributor',
              'id'=>'1'
            })
        end

        it 'routes to #get_distributor' do
          expect(:get => 'supply_chain/distributors/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/distributors',
              'action' => 'get_distributor',
              'id'=>'1'
            })
        end
      end
    end
  end
end