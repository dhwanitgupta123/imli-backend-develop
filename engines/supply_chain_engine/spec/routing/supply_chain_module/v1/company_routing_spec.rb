#
# Module to handle all the functionalities related to master product
#
module SupplyChainModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe CompanyController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_company' do
          expect(:post => 'supply_chain/companies/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'supply_chain_module/v1/company',
              'action' => 'add_company'
            })
        end

        it 'routes to #update_company' do
          expect(:put => 'supply_chain/companies/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'supply_chain_module/v1/company',
              'action'=>'update_company',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_companies' do
          expect(:get => 'supply_chain/companies').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/company',
              'action' => 'get_all_companies'
            })
        end

        it 'routes to #change company state' do
          expect(:put => 'supply_chain/companies/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/company',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete company' do
          expect(:delete => 'supply_chain/companies/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/company',
              'action' => 'delete_company',
              'id'=>'1'
            })
        end

        it 'routes to #get_company' do
          expect(:get => 'supply_chain/companies/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/company',
              'action' => 'get_company',
              'id'=>'1'
            })
        end
      end
    end
  end
end