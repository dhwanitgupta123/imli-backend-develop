#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SellerController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_seller' do
          expect(:post => 'supply_chain/sellers/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'supply_chain_module/v1/seller',
              'action' => 'add_seller'
            })
        end

        it 'routes to #get_all_sellers' do
          expect(:get => 'supply_chain/sellers').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/seller',
              'action' => 'get_all_sellers'
            })
        end

        it 'routes to #get_seller' do
          expect(:get => 'supply_chain/sellers/:id').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/seller',
              'action' => 'get_seller',
              'id' => ':id'
            })
        end

        it 'routes to #change seller state' do
          expect(:put => 'supply_chain/sellers/state/:id').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/seller',
              'action' => 'change_state',
              'id'=> ':id'
            })
        end

        it 'routes to #delete seller' do
          expect(:delete => 'supply_chain/sellers/:id').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/seller',
              'action' => 'delete_seller',
              'id'=> ':id'
            })
        end

        it 'routes to #update_seller' do
          expect(:put => 'supply_chain/sellers/1').to route_to(
            {
              'id' => '1',
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/seller',
              'action' => 'update_seller'
            })
        end

      end
    end
  end
end