#
# Module to handle all the functionalities related to master product
#
module SupplyChainModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe InventoriesController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_inventory' do
          expect(:post => 'supply_chain/inventories/new').to route_to(
            {
              'format' =>'json',
              'controller' => 'supply_chain_module/v1/inventories',
              'action' => 'add_inventory'
            })
        end

        it 'routes to #update_inventory' do
          expect(:put => 'supply_chain/inventories/1').to route_to(
            {
              'format'=>'json',
              'controller'=>'supply_chain_module/v1/inventories',
              'action'=>'update_inventory',
              'id'=>'1'
            })
        end

        it 'routes to #get_all_inventories' do
          expect(:get => 'supply_chain/inventories').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/inventories',
              'action' => 'get_all_inventories'
            })
        end

        it 'routes to #change inventory state' do
          expect(:put => 'supply_chain/inventories/state/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/inventories',
              'action' => 'change_state',
              'id'=>'1'
            })
        end

        it 'routes to #delete inventory' do
          expect(:delete => 'supply_chain/inventories/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/inventories',
              'action' => 'delete_inventory',
              'id'=>'1'
            })
        end

        it 'routes to #get_inventory' do
          expect(:get => 'supply_chain/inventories/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'supply_chain_module/v1/inventories',
              'action' => 'get_inventory',
              'id'=>'1'
            })
        end
      end
    end
  end
end