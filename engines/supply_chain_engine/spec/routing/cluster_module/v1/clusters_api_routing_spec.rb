#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    require 'rails_helper'

    RSpec.describe ClustersApiController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #get_all_clusters' do
          expect(:get => 'clusters').to route_to(
            {
              'format' => 'json',
              'controller' => 'cluster_module/v1/clusters_api',
              'action' => 'get_all_clusters'
            })
        end

        it 'routes to #get_clusters' do
          expect(:get => 'clusters/1').to route_to(
            {
              'format' => 'json',
              'controller' => 'cluster_module/v1/clusters_api',
              'action' => 'get_cluster',
              'id'=>'1'
            })
        end
      end
    end
  end
end