#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MarketplaceSellingPackSearchController, type: :routing do

      # specifying engine routes
      routes { SupplyChainEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #get_marketplace_selling_pack_by_query' do
          expect(:get => 'product/search').to route_to({
              'format' => 'json',
              'controller' => 'search_module/v1/marketplace_selling_pack_search',
              'action' => 'get_marketplace_selling_pack_by_query'
            })
        end
      end
    end
  end
end
