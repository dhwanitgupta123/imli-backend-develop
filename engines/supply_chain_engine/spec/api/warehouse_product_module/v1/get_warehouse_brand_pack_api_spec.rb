#
# Module to handle all the functionalities related to warehouse product product
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe WarehouseProductModule::V1::GetWarehouseBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:wbp_service) { WarehouseProductModule::V1::WarehouseBrandPackService }
      let(:wbp) { FactoryGirl.build(:warehouse_brand_pack) }
      let(:ibp) { FactoryGirl.build_stubbed(:inventory_brand_pack) }
      let(:get_api) { WarehouseProductModule::V1::GetWarehouseBrandPackApi.new(version) }
      let(:request) { Faker::Number.number(1) }
      let(:wbp_mapper) { WarehouseProductModule::V1::WarehouseBrandPackMapper }
      
      context 'enact ' do
        it 'with invalid request' do
          data = get_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          wbp_service.any_instance.stub(:get_wbp).and_raise(
            custom_errors_util::InvalidArgumentsError)
          data = get_api.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          wbp.inventory_brand_packs << ibp
          wbp_service.any_instance.stub(:get_wbp).and_return(wbp)
          expect(wbp_mapper).to receive(:map_wbp_to_hash).and_return(wbp)
          data = get_api.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
