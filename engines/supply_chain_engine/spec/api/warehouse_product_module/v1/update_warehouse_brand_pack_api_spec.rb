#
# Module to handle all the functionalities related to warehouse product product
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe WarehouseProductModule::V1::UpdateWarehouseBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:wbp_service) { WarehouseProductModule::V1::WarehouseBrandPackService }
      let(:wbp) { FactoryGirl.build(:warehouse_brand_pack) }
      let(:ibp) { FactoryGirl.build_stubbed(:inventory_brand_pack) }
      let(:update) { WarehouseProductModule::V1::UpdateWarehouseBrandPackApi.new(version) }
      let(:wbp_mapper) { WarehouseProductModule::V1::WarehouseBrandPackMapper }
      let(:request) {
        {
          id: Faker::Number.number(1),
          warehouse_id: Faker::Number.number(1),
          inventory_brand_pack_id: Faker::Number.number(1),
          brand_pack_id: Faker::Number.number(1),
          threshold_stock_value: Faker::Number.number(1),
          outbound_frequency_period_in_days: Faker::Number.number(1),
          pricing: {
            mrp: Faker::Commerce.price.to_s,
            selling_price: Faker::Commerce.price.to_s,
            service_tax: Faker::Commerce.price.to_s,
            vat: Faker::Commerce.price.to_s,
            cst: Faker::Commerce.price.to_s
          }
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = update.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid request' do
          data = update.enact(request.merge(id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong cst' do
          request[:pricing].merge(cst: -1)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong selling_price' do
          request[:pricing].merge(selling_price: -1)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong service_tax' do
          request[:pricing].merge(service_tax: -1)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong vat' do
          request[:pricing].merge(vat: -1)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong mrp' do
          request[:pricing].merge(mrp: -1)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          wbp_service.any_instance.stub(:update).and_raise(
            custom_errors_util::InvalidArgumentsError)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          wbp.inventory_brand_packs << ibp
          wbp_service.any_instance.stub(:update).and_return(wbp)
          expect(wbp_mapper).to receive(:map_wbp_to_hash).and_return(wbp)
          data = update.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
