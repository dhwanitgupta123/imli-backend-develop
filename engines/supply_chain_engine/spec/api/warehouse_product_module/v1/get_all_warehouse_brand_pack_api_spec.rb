#
# Module to handle all the functionalities related to warehouse product product
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
  	require 'rails_helper'
    RSpec.describe WarehouseProductModule::V1::GetAllWarehouseBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:wbp_service) { WarehouseProductModule::V1::WarehouseBrandPackService }
      let(:ibp) { FactoryGirl.build_stubbed(:inventory_brand_pack) }
      let(:wbp) { { warehouse_brand_pack: [FactoryGirl.build(:warehouse_brand_pack)] } }
      let(:get_all) { WarehouseProductModule::V1::GetAllWarehouseBrandPackApi.new(version) }
      let(:wbp_mapper) { WarehouseProductModule::V1::WarehouseBrandPackMapper }
      let(:request) {
        {
          sort_by: 'name'
        }
      }

      context 'enact ' do
        it 'with wrong order' do
          data = get_all.enact(request.merge({ order: 'xyz' }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong sort_by' do
          data = get_all.enact(request.merge({ sort_by: 'xyz' }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(wbp_service).to receive(:get_all_wbps).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_all.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          wbp[:warehouse_brand_pack].first.inventory_brand_packs << ibp
          expect_any_instance_of(wbp_service).to receive(:get_all_wbps).
            and_return(wbp)
          expect(wbp_mapper).to receive(:map_wbps_to_array).and_return(wbp)
          data = get_all.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
