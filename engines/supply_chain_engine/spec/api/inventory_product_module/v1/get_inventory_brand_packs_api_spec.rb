#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe InventoryProductModule::V1::GetInventoryBrandPacksApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:inventory_brand_pack_mapper) { InventoryProductModule::V1::InventoryBrandPackMapper }
      let(:inventory_brand_pack_service) { InventoryProductModule::V1::InventoryBrandPackService }
      let(:inventory_brand_pack) { FactoryGirl.build(:inventory_brand_pack) }
      let(:get_inventory_brand_packs_api) { InventoryProductModule::V1::GetInventoryBrandPacksApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(inventory_brand_pack_service).to receive(:get_all_inventory_brand_packs).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_inventory_brand_packs_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          inventory_brand_pack_array = []
          inventory_brand_pack_array.push(inventory_brand_pack)
          stub_response = { elements: inventory_brand_pack_array, page_count: Faker::Number.number(1)}
          expect_any_instance_of(inventory_brand_pack_service).to receive(:get_all_inventory_brand_packs).
            and_return(stub_response)
          expect(inventory_brand_pack_mapper).to receive(:map_inventory_brand_packs_to_array).and_return([inventory_brand_pack])
          data = get_inventory_brand_packs_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
