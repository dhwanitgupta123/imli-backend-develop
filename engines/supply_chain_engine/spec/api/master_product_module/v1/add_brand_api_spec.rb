#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::AddBrandApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:brand_service) { MasterProductModule::V1::BrandService }
      let(:brand) { FactoryGirl.build(:brand) }
      let(:add_brand_api) { MasterProductModule::V1::AddBrandApi.new(version) }

      let(:brand_request) {
        {
          name: Faker::Company.name,
          code: Faker::Lorem.characters(5),
          logo_url: Faker::Company.logo,
          company_id: 1,
          initials: Faker::Lorem.characters(2)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_brand_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil company_id' do
          data = add_brand_api.enact(brand_request.merge(company_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(brand_service).to receive(:create_brand).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_brand_api.enact(brand_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(brand_service).to receive(:create_brand).
            and_return(brand)
          data = add_brand_api.enact(brand_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
