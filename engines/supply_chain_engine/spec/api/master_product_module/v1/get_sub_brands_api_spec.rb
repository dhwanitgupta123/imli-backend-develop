#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::GetSubBrandsApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:sub_brand_service) { MasterProductModule::V1::SubBrandService }
      let(:sub_brand) { FactoryGirl.build(:sub_brand) }
      let(:get_sub_brands_api) { MasterProductModule::V1::GetSubBrandsApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(sub_brand_service).to receive(:get_all_sub_brands).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_sub_brands_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          sub_brand_array = []
          sub_brand_array.push(sub_brand)
          stub_response = { elements: sub_brand_array, page_count: Faker::Number.number(1)}
          expect_any_instance_of(sub_brand_service).to receive(:get_all_sub_brands).
            and_return(stub_response)
          data = get_sub_brands_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
