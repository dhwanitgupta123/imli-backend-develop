#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::GetBrandApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:brand_service) { MasterProductModule::V1::BrandService }
      let(:brand) { FactoryGirl.build(:brand) }
      let(:get_brand_api) { MasterProductModule::V1::GetBrandApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          data = get_brand_api.enact({id: nil})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(brand_service).to receive(:get_brand_by_id).
            and_return(brand)
          data = get_brand_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
