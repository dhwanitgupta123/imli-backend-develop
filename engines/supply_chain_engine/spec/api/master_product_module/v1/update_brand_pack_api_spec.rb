#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::UpdateBrandPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:brand_pack_service) { MasterProductModule::V1::BrandPackService }
      let(:brand_pack) { FactoryGirl.build(:brand_pack) }
      let(:update_brand_pack_api) { MasterProductModule::V1::UpdateBrandPackApi.new(version) }
      let(:brand_pack_mapper) { MasterProductModule::V1::BrandPackMapper }

      let(:brand_pack_request) {
        {
          photo_url: Faker::Avatar.image,
          sku: Faker::Company.ein,
          mrp: Faker::Commerce.price,
          sku_size: Faker::Lorem.characters(10),
          unit: Faker::Lorem.characters(5),
          description: Faker::Lorem.sentence,
          shelf_life: Faker::Internet.slug(Faker::Number.between(1, 12).to_s, 'months'),
          product_id: 1,
          initials: Faker::Lorem.characters(2)
        }
      }
      
      context 'enact ' do
        it 'with invalid request' do
          data = update_brand_pack_api.enact({}, 1)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil product_id' do
          data = update_brand_pack_api.enact(brand_pack_request.merge(product_id: nil), 1)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(brand_pack_service).to receive(:update_brand_pack).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = update_brand_pack_api.enact(brand_pack_request, 1)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(brand_pack_service).to receive(:update_brand_pack).
            and_return(brand_pack)
          expect(brand_pack_mapper).to receive(:map_brand_pack_to_hash).and_return(brand_pack)
          data = update_brand_pack_api.enact(brand_pack_request, 1)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
