#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::UpdateProductApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:variant_service) { MasterProductModule::V1::VariantService }
      let(:variant) { FactoryGirl.build(:variant) }
      let(:update_variant_api) { MasterProductModule::V1::UpdateVariantApi.new(version) }

      let(:variant_request) {
        {
          id: 1,
          name: Faker::Company.name,
          initials: Faker::Lorem.characters(2)
        }
      }
      
      context 'enact ' do
        it 'with invalid request' do
          data = update_variant_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil id' do
          data = update_variant_api.enact(variant_request.merge(id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(variant_service).to receive(:update_variant).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = update_variant_api.enact(variant_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(variant_service).to receive(:update_variant).
            and_return(variant)
          data = update_variant_api.enact(variant_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
