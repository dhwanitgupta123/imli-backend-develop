#
# Module to handle all the functionalities related to master sub_brand
#
module MasterProductModule
  #
  # Version1 for master sub_brand module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::ChangeSubBrandStateApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:sub_brand_service) { MasterProductModule::V1::SubBrandService }
      let(:sub_brand) { FactoryGirl.build(:sub_brand) }
      let(:change_sub_brand_state_api) { MasterProductModule::V1::ChangeSubBrandStateApi.new(version) }

      let(:change_state_request) {
        {
          id: Faker::Number.number(1),
          event: Faker::Number.between(1, 3)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = change_sub_brand_state_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil event' do
          data = change_sub_brand_state_api.enact(change_state_request.merge(event: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(sub_brand_service).to receive(:change_sub_brand_state).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = change_sub_brand_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with precondition error args' do
          expect_any_instance_of(sub_brand_service).to receive(:change_sub_brand_state).
            and_raise(custom_errors_util::PreConditionRequiredError.new)
          data = change_sub_brand_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end

        it 'with valid args' do
          expect_any_instance_of(sub_brand_service).to receive(:change_sub_brand_state).
            and_return(sub_brand)
          data = change_sub_brand_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
