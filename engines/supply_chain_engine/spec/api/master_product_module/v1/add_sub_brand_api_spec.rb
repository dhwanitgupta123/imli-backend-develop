#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::AddSubBrandApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:sub_brand_service) { MasterProductModule::V1::SubBrandService }
      let(:sub_brand) { FactoryGirl.build(:sub_brand) }
      let(:add_sub_brand_api) { MasterProductModule::V1::AddSubBrandApi.new(version) }

      let(:sub_brand_request) {
        {
          name: Faker::Company.name,
          logo_url: Faker::Company.logo,
          brand_id: 1,
          initials: Faker::Lorem.characters(2)
        }
      }
      
      context 'enact ' do
        it 'with invalid request' do
          data = add_sub_brand_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil brand_id' do
          data = add_sub_brand_api.enact(sub_brand_request.merge(brand_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(sub_brand_service).to receive(:create_sub_brand).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_sub_brand_api.enact(sub_brand_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(sub_brand_service).to receive(:create_sub_brand).
            and_return(sub_brand)
          data = add_sub_brand_api.enact(sub_brand_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
