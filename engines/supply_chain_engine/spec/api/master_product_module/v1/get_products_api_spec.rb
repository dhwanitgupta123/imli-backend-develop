#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::GetProductsApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:product_service) { MasterProductModule::V1::ProductService }
      let(:product) { FactoryGirl.build(:product) }
      let(:get_products_api) { MasterProductModule::V1::GetProductsApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(product_service).to receive(:get_all_products).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_products_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          product_array = []
          product_array.push(product)
          stub_response = { elements: product_array, page_count: Faker::Number.number(1)}
          expect_any_instance_of(product_service).to receive(:get_all_products).
            and_return(stub_response)
          data = get_products_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
