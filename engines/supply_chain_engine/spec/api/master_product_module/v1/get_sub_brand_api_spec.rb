#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MasterProductModule::V1::GetSubBrandApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:sub_brand_service) { MasterProductModule::V1::SubBrandService }
      let(:sub_brand) { FactoryGirl.build(:sub_brand) }
      let(:get_sub_brand_api) { MasterProductModule::V1::GetSubBrandApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          data = get_sub_brand_api.enact({id: nil})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(sub_brand_service).to receive(:get_sub_brand_by_id).
            and_return(sub_brand)
          data = get_sub_brand_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
