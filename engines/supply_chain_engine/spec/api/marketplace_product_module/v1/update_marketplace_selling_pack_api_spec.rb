#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::UpdateMarketplaceSellingPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_selling_pack_service) { MarketplaceProductModule::V1::MarketplaceSellingPackService }
      let(:marketplace_selling_pack) { FactoryGirl.build_stubbed(:marketplace_selling_pack) }
      let(:update) { MarketplaceProductModule::V1::UpdateMarketplaceSellingPackApi.new(version) }
      let(:mapper) { MarketplaceProductModule::V1::MarketplaceSellingPackMapper }
      let(:marketplace_selling_pack_request) {
        {
          id: 1,
          is_on_sale: true,
          is_ladder_pricing_active: true,
          max_quantity: 5,
          pricing: {
            taxes: Faker::Commerce.price,
            service_tax: Faker::Commerce.price,
            vat: Faker::Commerce.price,
            discount: Faker::Commerce.price,
            base_selling_price: 120,
            savings: Faker::Commerce.price,
            mrp: Faker::Commerce.price,
            ladder: [{
              quantity: 1,
              selling_price: 120,
              additional_savings: Faker::Commerce.price
            }]
          }
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = update.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil id' do
          data = update.enact(marketplace_selling_pack_request.merge(id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong base_selling_price' do
          marketplace_selling_pack_request[:pricing].merge(base_selling_price: -1)
          data = update.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong vat' do
          marketplace_selling_pack_request[:pricing].merge(vat: -1)
          data = update.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong savings' do
          marketplace_selling_pack_request[:pricing].merge(savings: -1)
          data = update.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong mrp' do
          marketplace_selling_pack_request[:pricing].merge(mrp: -1)
          data = update.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong ladder quantity' do
          marketplace_selling_pack_request[:pricing][:ladder][0][:quantity] = -1
          data = update.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong ladder selling_price' do
          marketplace_selling_pack_request[:pricing][:ladder][0][:selling_price] = -1
          data = update.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong ladder additional_savings' do
          marketplace_selling_pack_request[:pricing][:ladder][0][:additional_savings] = -1
          data = update.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'when base selling_price is not equal to ladder first selling price' do
          marketplace_selling_pack_request[:pricing][:ladder][0][:selling_price] = 100
          data = update.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with service throwing exception' do
          marketplace_selling_pack_service.any_instance.stub(:transactional_update_marketplace_selling_pack).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = update.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with service call success' do
          expect_any_instance_of(marketplace_selling_pack_service).to receive(:transactional_update_marketplace_selling_pack).
            and_return(marketplace_selling_pack)
          mapper.stub(:map_marketplace_selling_pack_to_hash).and_return(marketplace_selling_pack)
          data = update.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
