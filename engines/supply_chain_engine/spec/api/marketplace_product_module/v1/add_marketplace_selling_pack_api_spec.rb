#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
  	require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::AddMarketplaceSellingPackApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_selling_pack_service) { MarketplaceProductModule::V1::MarketplaceSellingPackService }
      let(:marketplace_selling_pack) { FactoryGirl.build_stubbed(:marketplace_selling_pack) }
      let(:add_marketplace_selling_pack_api) { MarketplaceProductModule::V1::AddMarketplaceSellingPackApi.new(version) }
      let(:mapper) { MarketplaceProductModule::V1::MarketplaceSellingPackMapper }
      let(:marketplace_selling_pack_request) {
        {
			    marketplace_brand_packs: [ { marketplace_brand_pack_id: 1, quantity: 3 }],
          is_on_sale: true,
          is_ladder_pricing_active: true,
          max_quantity: 5,
          pricing: {
            taxes: Faker::Commerce.price,
            service_tax: Faker::Commerce.price,
            vat: Faker::Commerce.price,
            discount: Faker::Commerce.price,
            base_selling_price: Faker::Commerce.price,
            savings: Faker::Commerce.price,
            mrp: Faker::Commerce.price,
            ladder: [{
              quantity: 1,
              selling_price: Faker::Commerce.price,
              additional_savings: Faker::Commerce.price
            }]
          }
			  }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_marketplace_selling_pack_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil marketplace_brand_packs' do
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request.merge(
            marketplace_brand_packs: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil marketplace_brand_pack' do
          marketplace_selling_pack_request[:marketplace_brand_packs].first.merge(marketplace_brand_pack_id: nil)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil max_quantity' do
          marketplace_selling_pack_request[:marketplace_brand_packs].first.merge(max_quantity: nil)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil quantity' do
          marketplace_selling_pack_request[:marketplace_brand_packs].first.merge(quantity: nil)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil is_ladder_pricing_active' do
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request.merge(is_ladder_pricing_active: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil pricing' do
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request.merge(pricing: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil base_selling_price' do
          marketplace_selling_pack_request[:pricing].merge(base_selling_price: nil)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil vat' do
          marketplace_selling_pack_request[:pricing].merge(vat: nil)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil savings' do
          marketplace_selling_pack_request[:pricing].merge(savings: nil)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil ladder ' do
          marketplace_selling_pack_request[:pricing].merge(ladder: nil)
          marketplace_selling_pack_request[:is_ladder_pricing_active] = true
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil ladder quantity' do
          marketplace_selling_pack_request[:pricing][:ladder][0][:quantity] = nil
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil ladder selling_price' do
          marketplace_selling_pack_request[:pricing][:ladder][0][:selling_price] = nil
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil ladder additional_savings' do
          marketplace_selling_pack_request[:pricing][:ladder][0][:additional_savings] = nil
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil mrp' do
          marketplace_selling_pack_request[:pricing].merge(mrp: nil)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong base_selling_price' do
          marketplace_selling_pack_request[:pricing].merge(base_selling_price: -1)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong vat' do
          marketplace_selling_pack_request[:pricing].merge(vat: -1)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong savings' do
          marketplace_selling_pack_request[:pricing].merge(savings: -1)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong mrp' do
          marketplace_selling_pack_request[:pricing].merge(mrp: -1)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong ladder quantity' do
          marketplace_selling_pack_request[:pricing][:ladder][0][:quantity] = -1
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong ladder selling_price' do
          marketplace_selling_pack_request[:pricing][:ladder][0][:selling_price] = -1
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong ladder additional_savings' do
          marketplace_selling_pack_request[:pricing][:ladder][0][:additional_savings] = -1
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(marketplace_selling_pack_service).to receive(:transactional_create_marketplace_selling_pack).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(marketplace_selling_pack_service).to receive(:transactional_create_marketplace_selling_pack).
            and_return(marketplace_selling_pack)
          mapper.stub(:map_marketplace_selling_pack_to_hash).and_return(marketplace_selling_pack_request)
          data = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
