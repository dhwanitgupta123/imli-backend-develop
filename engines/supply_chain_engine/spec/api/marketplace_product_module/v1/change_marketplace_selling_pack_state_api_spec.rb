#
# Module to handle all the functionalities related to marketplace product
#
module MarketplaceProductModule
  #
  # Version1 for marketplace product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe MarketplaceProductModule::V1::ChangeMarketplaceSellingPackStateApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:marketplace_selling_pack_service) { MarketplaceProductModule::V1::MarketplaceSellingPackService }
      let(:marketplace_selling_pack) { FactoryGirl.build_stubbed(:marketplace_selling_pack) }
      let(:change_state) { MarketplaceProductModule::V1::ChangeMarketplaceSellingPackStateApi.new(version) }
      let(:mapper) { MarketplaceProductModule::V1::MarketplaceSellingPackMapper }

      let(:change_state_request) {
        {
          id: Faker::Number.number(1),
          event: Faker::Number.between(1, 3)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = change_state.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil event' do
          data = change_state.enact(change_state_request.merge(event: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(marketplace_selling_pack_service).to receive(:change_marketplace_selling_pack_state).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = change_state.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with precondition error args' do
          expect_any_instance_of(marketplace_selling_pack_service).to receive(:change_marketplace_selling_pack_state).
            and_raise(custom_errors_util::PreConditionRequiredError.new)
          data = change_state.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end

        it 'with valid args' do
          expect_any_instance_of(marketplace_selling_pack_service).to receive(:change_marketplace_selling_pack_state).
            and_return(marketplace_selling_pack)
          mapper.stub(:map_marketplace_selling_pack_to_hash).and_return(marketplace_selling_pack)
          data = change_state.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
