#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::AddInventoryApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:inventory_service) { SupplyChainModule::V1::InventoryService }
      let(:distributor) { FactoryGirl.build_stubbed(:distributor) }
      let(:inventory) { FactoryGirl.build(:inventory, distributor: distributor) }
      let(:add_inventory_api) { SupplyChainModule::V1::AddInventoryApi.new(version) }

      let(:inventory_request) {
        {
          name: Faker::Company.name,
          distributor_id: distributor.id
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_inventory_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil name' do
          data = add_inventory_api.enact(inventory_request.merge(name: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(inventory_service).to receive(:create_inventory).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_inventory_api.enact(inventory_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(inventory_service).to receive(:create_inventory).
            and_return(inventory)
          data = add_inventory_api.enact(inventory_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
