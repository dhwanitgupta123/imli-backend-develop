#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::GetCompaniesApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:company_service) { SupplyChainModule::V1::CompanyService }
      let(:company) { FactoryGirl.build(:company) }
      let(:get_companies_api) { SupplyChainModule::V1::GetCompaniesApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(company_service).to receive(:get_all_companies).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_companies_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          company_array = []
          company_array.push(company)
          stub_response = { elements: company_array, page_count: Faker::Number.number(1)}
          expect_any_instance_of(company_service).to receive(:get_all_companies).
            and_return(stub_response)
          data = get_companies_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
