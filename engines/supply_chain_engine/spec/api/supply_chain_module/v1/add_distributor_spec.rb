#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::AddDistributorApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:distributor_service) { SupplyChainModule::V1::DistributorService }
      let(:distributor) { FactoryGirl.build(:distributor) }
      let(:add_distributor_api) { SupplyChainModule::V1::AddDistributorApi.new(version) }

      let(:distributor_request) {
        {
          name: Faker::Company.name,
          cst_no: Faker::Lorem.characters(10),
          tin_no: Faker::Lorem.characters(10),
          vendor_code: Faker::Number.number(4)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_distributor_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'when both Tin & Cst no is not present' do
          data = add_distributor_api.enact(distributor_request.merge(cst_no: nil, tin_no: nil))
        end

        it 'when vendor code is not present' do
          data = add_distributor_api.enact(distributor_request.merge(vendor_code: nil))
        end

        it 'with nil name' do
          data = add_distributor_api.enact(distributor_request.merge(name: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(distributor_service).to receive(:create_distributor).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_distributor_api.enact(distributor_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(distributor_service).to receive(:create_distributor_with_inventory).
            and_return(distributor)
          data = add_distributor_api.enact(distributor_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
