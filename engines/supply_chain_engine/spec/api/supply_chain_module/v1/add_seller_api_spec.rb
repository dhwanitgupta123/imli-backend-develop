#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::AddSellerApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:seller_service) { SupplyChainModule::V1::SellerService }
      let(:seller) { FactoryGirl.build(:seller) }
      let(:add_seller) { SupplyChainModule::V1::AddSellerApi.new(version) }

      let(:request) {
        {
          name: Faker::Company.name
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_seller.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil name' do
          data = add_seller.enact(request.merge(name: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(seller_service).to receive(:create_seller).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_seller.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(seller_service).to receive(:create_seller).
            and_return(seller)
          data = add_seller.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end