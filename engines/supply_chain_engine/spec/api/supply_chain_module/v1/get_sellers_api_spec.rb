#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::GetSellersApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:seller_service) { SupplyChainModule::V1::SellerService }
      let(:seller) { { seller: [FactoryGirl.build(:seller)] } }
      let(:get_all) { SupplyChainModule::V1::GetSellersApi.new(version) }

      let(:request) {
        {
          sort_by: 'name'
        }
      }

      context 'enact ' do
        it 'with wrong order' do
          data = get_all.enact(request.merge({ order: 'xyz' }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong sort_by' do
          data = get_all.enact(request.merge({ sort_by: 'xyz' }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(seller_service).to receive(:get_all_sellers).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_all.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(seller_service).to receive(:get_all_sellers).
            and_return(seller)
          data = get_all.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end