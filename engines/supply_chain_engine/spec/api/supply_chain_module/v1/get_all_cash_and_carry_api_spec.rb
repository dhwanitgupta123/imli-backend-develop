#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::GetAllCashAndCarryApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:cash_and_carry_service) { SupplyChainModule::V1::CashAndCarryService }
      let(:c_and_c) { { cash_and_carry: [FactoryGirl.build(:cash_and_carry), FactoryGirl.build(:cash_and_carry)] } }
      let(:get_all_cash_and_carry) { SupplyChainModule::V1::GetAllCashAndCarryApi.new(version) }

      let(:request) {
        {
          sort_by: 'name'
        }
      }

      context 'enact ' do
        it 'with wrong order' do
          data = get_all_cash_and_carry.enact(request.merge({ order: 'xyz' }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with wrong sort_by' do
          data = get_all_cash_and_carry.enact(request.merge({ sort_by: 'xyz' }))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(cash_and_carry_service).to receive(:get_all_cash_and_carry).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_all_cash_and_carry.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(cash_and_carry_service).to receive(:get_all_cash_and_carry).
            and_return(c_and_c)
          data = get_all_cash_and_carry.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
