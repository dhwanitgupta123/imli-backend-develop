#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::AddWarehouseApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:warehouse_service) { SupplyChainModule::V1::WarehouseService }
      let(:warehouse) { FactoryGirl.build(:warehouse) }
      let(:add_warehouse) { SupplyChainModule::V1::AddWarehouseApi.new(version) }

      let(:request) {
        {
          name: Faker::Company.name,
          cash_and_carry_id: Faker::Number.number(1)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = add_warehouse.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil name' do
          data = add_warehouse.enact(request.merge(name: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil cash_and_carry_id' do
          data = add_warehouse.enact(request.merge(cash_and_carry_id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(warehouse_service).to receive(:create).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = add_warehouse.enact(request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(warehouse_service).to receive(:create).
            and_return(warehouse)
          data = add_warehouse.enact(request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
