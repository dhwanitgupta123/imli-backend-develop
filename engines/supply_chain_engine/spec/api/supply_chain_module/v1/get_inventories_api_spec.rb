#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::GetInventoriesApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:inventory_service) { SupplyChainModule::V1::InventoryService }
      let(:inventory) { FactoryGirl.build(:inventory) }
      let(:get_inventories_api) { SupplyChainModule::V1::GetInventoriesApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(inventory_service).to receive(:get_all_inventories).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_inventories_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          inventory_array = []
          inventory_array.push(inventory)
          stub_response = { elements: inventory_array, page_count: Faker::Number.number(1)}
          expect_any_instance_of(inventory_service).to receive(:get_all_inventories).
            and_return(stub_response)
          data = get_inventories_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
