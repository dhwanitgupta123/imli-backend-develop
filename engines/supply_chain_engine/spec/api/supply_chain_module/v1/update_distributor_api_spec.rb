#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::UpdateDistributorApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:distributor_service) { SupplyChainModule::V1::DistributorService }
      let(:distributor) { FactoryGirl.build(:distributor) }
      let(:update_distributor_api) { SupplyChainModule::V1::UpdateDistributorApi.new(version) }

      let(:distributor_request) {
        {
          id: Faker::Number.number(1),
          name: Faker::Company.name
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = update_distributor_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil id' do
          data = update_distributor_api.enact(distributor_request.merge(id: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with service throwing exception' do
          expect_any_instance_of(distributor_service).to receive(:update_distributor).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = update_distributor_api.enact(distributor_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with service call success' do
          expect_any_instance_of(distributor_service).to receive(:update_distributor).
            and_return(distributor)
          data = update_distributor_api.enact(distributor_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
