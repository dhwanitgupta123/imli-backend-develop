#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::ChangeDistributorStateApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:distributor_service) { SupplyChainModule::V1::DistributorService }
      let(:distributor) { FactoryGirl.build(:distributor) }
      let(:change_distributor_state_api) { SupplyChainModule::V1::ChangeDistributorStateApi.new(version) }

      let(:change_state_request) {
        {
          id: Faker::Number.number(1),
          event: Faker::Number.between(1, 3)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = change_distributor_state_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil event' do
          data = change_distributor_state_api.enact(change_state_request.merge(event: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(distributor_service).to receive(:change_distributor_state).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = change_distributor_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with precondition error args' do
          expect_any_instance_of(distributor_service).to receive(:change_distributor_state).
            and_raise(custom_errors_util::PreConditionRequiredError.new)
          data = change_distributor_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end

        it 'with valid args' do
          expect_any_instance_of(distributor_service).to receive(:change_distributor_state).
            and_return(distributor)
          data = change_distributor_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
