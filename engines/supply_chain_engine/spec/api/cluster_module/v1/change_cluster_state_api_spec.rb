#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ClusterModule::V1::ChangeClusterStateApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:cluster_service) { ClusterModule::V1::ClusterService }
      let(:cluster) { FactoryGirl.build(:cluster) }
      let(:change_cluster_state_api) { ClusterModule::V1::ChangeClusterStateApi.new(version) }

      let(:change_state_request) {
        {
          id: Faker::Number.number(1),
          event: Faker::Number.between(1, 3)
        }
      }

      context 'enact ' do
        it 'with invalid request' do
          data = change_cluster_state_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil event' do
          data = change_cluster_state_api.enact(change_state_request.merge(event: nil))
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with invalid args' do
          expect_any_instance_of(cluster_service).to receive(:change_cluster_state).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = change_cluster_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with precondition error args' do
          expect_any_instance_of(cluster_service).to receive(:change_cluster_state).
            and_raise(custom_errors_util::PreConditionRequiredError.new)
          data = change_cluster_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::PRE_CONDITION_REQUIRED)
        end

        it 'with valid args' do
          expect_any_instance_of(cluster_service).to receive(:change_cluster_state).
            and_return(cluster)
          data = change_cluster_state_api.enact(change_state_request)
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
