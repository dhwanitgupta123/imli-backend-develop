#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ClusterModule::V1::GetClusterPanelApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:cluster_service) { ClusterModule::V1::ClusterService }
      let(:cluster) { FactoryGirl.build(:cluster) }
      let(:get_cluster_panel_api) { ClusterModule::V1::GetClusterPanelApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          data = get_cluster_panel_api.enact({id: nil})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(cluster_service).to receive(:get_cluster_by_id).
            and_return(cluster)
          data = get_cluster_panel_api.enact({id: 1})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
