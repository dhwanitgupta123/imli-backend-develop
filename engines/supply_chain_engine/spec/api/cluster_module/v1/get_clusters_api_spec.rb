#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    require 'rails_helper'
    RSpec.describe ClusterModule::V1::GetClustersApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:cluster_service) { ClusterModule::V1::ClusterService }
      let(:cluster) { FactoryGirl.build(:cluster) }
      let(:get_clusters_api) { ClusterModule::V1::GetClustersApi.new(version) }

      context 'enact ' do
        it 'with invalid args' do
          expect_any_instance_of(cluster_service).to receive(:get_filtered_clusters).
            and_raise(custom_errors_util::InvalidArgumentsError.new)
          data = get_clusters_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          cluster_array = []
          cluster_array.push(cluster)
          expect_any_instance_of(cluster_service).to receive(:get_filtered_clusters).
            and_return({elements: cluster_array, page_count: Faker::Number.number(1)})
          data = get_clusters_api.enact({})
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
