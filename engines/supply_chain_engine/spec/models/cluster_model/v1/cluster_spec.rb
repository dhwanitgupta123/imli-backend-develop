#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Model for clusters table
    #
    require 'rails_helper'

    RSpec.describe ClusterModule::V1::Cluster, type: :model do
      let(:cluster) { FactoryGirl.build(:cluster) }

      it 'should be valid' do
        expect(cluster).to be_valid
      end
    end
  end
end