#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SupplyChainModule::V1::Company, type: :model do
      let(:company_name) { Faker::Company.name }
      subject { FactoryGirl.build(:company) }

      it { should be_valid }

      it 'with nil company name' do
        FactoryGirl.build(:company, name: nil).should_not be_valid
      end

      it 'with duplicate names' do
        FactoryGirl.create(:company, name: company_name)
        FactoryGirl.build(:company, name: company_name).should_not be_valid
      end

      it 'with nil initials' do
        FactoryGirl.build(:company, initials: nil).should_not be_valid
      end
    end
  end
end
