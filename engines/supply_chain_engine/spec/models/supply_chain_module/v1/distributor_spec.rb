#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SupplyChainModule::V1::Distributor, type: :model do
      let(:distributor_name) { Faker::Company.name }
      subject { FactoryGirl.build(:distributor) }

      it { should be_valid }

      it 'with nil distributor name' do
        FactoryGirl.build(:distributor, name: nil).should_not be_valid
      end

      it 'with duplicate names' do
        FactoryGirl.create(:distributor, name: distributor_name)
        FactoryGirl.build(:distributor, name: distributor_name).should_not be_valid
      end
    end
  end
end
