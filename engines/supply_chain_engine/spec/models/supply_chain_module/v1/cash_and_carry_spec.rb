#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SupplyChainModule::V1::CashAndCarry, type: :model do
      let(:c_and_c_name) { Faker::Company.name }
      subject { FactoryGirl.build(:cash_and_carry) }

      it { should be_valid }

      it 'with nil name' do
        FactoryGirl.build(:cash_and_carry, name: nil).should_not be_valid
      end

      it 'with duplicate names' do
        FactoryGirl.create(:cash_and_carry, name: c_and_c_name)
        FactoryGirl.build(:cash_and_carry, name: c_and_c_name).should_not be_valid
      end
    end
  end
end
