#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SupplyChainModule::V1::Warehouse, type: :model do
      let(:warehouse_name) { Faker::Company.name }
      subject { FactoryGirl.build(:warehouse) }

      it { should be_valid }

      it 'with nil warehouse name' do
        FactoryGirl.build(:warehouse, name: nil).should_not be_valid
      end

      it 'with duplicate names' do
        FactoryGirl.build(:warehouse, name: warehouse_name)
        expect { FactoryGirl.build(:warehouse, name: warehouse_name) }.to raise_error(
          ActiveRecord::RecordInvalid)
      end
    end
  end
end
