#
# Module to handle all the functionalities related to supply chain
#
module MasterProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MasterProductModule::V1::SubBrand, type: :model do
      let(:company) { FactoryGirl.build_stubbed(:company) }
      let(:brand) { FactoryGirl.build_stubbed(:brand, company: company) }
      subject { FactoryGirl.build(:sub_brand, brand: brand) }

      it { should be_valid }

      it 'with nil brand name' do
        FactoryGirl.build(:sub_brand, brand: brand, name: nil).should_not be_valid
      end

      it 'with nil brand' do
        FactoryGirl.build(:sub_brand, brand: nil).should_not be_valid
      end

      it 'with nil initials' do
        FactoryGirl.build(:sub_brand, initials: nil).should_not be_valid
      end
    end
  end
end
