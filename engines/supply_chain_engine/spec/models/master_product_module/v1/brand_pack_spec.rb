#
# Module to handle all the functionalities related to supply chain
#
module MasterProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MasterProductModule::V1::BrandPack, type: :model do
      let(:company) { FactoryGirl.build_stubbed(:company) }
      let(:brand) { FactoryGirl.build_stubbed(:brand, company: company) }
      let(:sub_brand) { FactoryGirl.build_stubbed(:sub_brand, brand: brand) }
      let(:product) { FactoryGirl.build_stubbed(:product, sub_brand: sub_brand) }
      let(:department) {FactoryGirl.build_stubbed(:department)}
      let(:category) { FactoryGirl.build_stubbed(:category, department: department)}
      let(:sub_category) { FactoryGirl.build_stubbed(:category, parent_category: category)}

      subject { FactoryGirl.build(:brand_pack, product: product, sub_category: sub_category) }

      it { should be_valid }

      it 'with nil product' do
        FactoryGirl.build(:brand_pack, product: nil, sub_category: sub_category).should_not be_valid
      end

      it 'with nil sub_category' do
        FactoryGirl.build(:brand_pack, product: product, sub_category: nil).should_not be_valid
      end

    end
  end
end
