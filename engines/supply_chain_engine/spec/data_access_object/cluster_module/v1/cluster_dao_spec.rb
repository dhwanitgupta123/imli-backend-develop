#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    require 'rails_helper'

    RSpec.describe ClusterModule::V1::ClusterDao, type: :dao do
      let(:cluster_dao) { ClusterModule::V1::ClusterDao.new }
      let(:cluster_context_dao) { ClusterModule::V1::ClusterContextDao }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }
      let(:cluster) { FactoryGirl.create(:cluster) }
      let(:cluster_context) { FactoryGirl.create(:cluster_context) }
      let(:cluster_model) { ClusterModule::V1::Cluster }
      let(:cluster_types) { ClusterModule::V1::ClusterTypes }

      let(:cluster_request) {
        {
          label: Faker::Company.name,
          cluster_type: Faker::Number.number(1),
          context: {
            level: Faker::Company.name,
            level_id: Faker::Number.number(1)
          }
        }
      }

      context 'create cluster' do
        it 'with valid params' do
          expect_any_instance_of(cluster_context_dao).to receive(:create).
            and_return(cluster_context)
          cluster = cluster_dao.create(cluster_request)
          expect(cluster.label).to eq(cluster_request[:label])
        end
      end

      context 'update cluster' do
        it 'with valid arguments' do
          cluster_before = FactoryGirl.create(:cluster)
          new_label = Faker::Company.name
          cluster = cluster_dao.update({ label: new_label }, cluster_before)
          expect(cluster.label).to eq(new_label)
        end
      end

      context 'update cluster state' do
        it 'from inactive to active and active to inactive' do
          cluster = FactoryGirl.create(:cluster)
          cluster = cluster_dao.change_state(cluster, common_events::ACTIVATE)
          expect(cluster.status).to eq(common_states::ACTIVE)
          cluster = cluster_dao.change_state(cluster, common_events::DEACTIVATE)
          expect(cluster.status).to eq(common_states::INACTIVE)
        end

        it 'from active to soft_delete' do
          cluster = FactoryGirl.create(:cluster)
          active_cluster = cluster_dao.change_state(cluster, common_events::ACTIVATE)
          cluster = cluster_dao.change_state(active_cluster, common_events::SOFT_DELETE)
          expect(cluster.status).to eq(common_states::DELETED)
        end

        it 'from inactive to soft_delete' do
          cluster = FactoryGirl.create(:cluster)
          cluster = cluster_dao.change_state(cluster, common_events::SOFT_DELETE)
          expect(cluster.status).to eq(common_states::DELETED)
        end

        it 'raise error change state from S1 to S1' do
          cluster = FactoryGirl.create(:cluster)
          expect{ cluster_dao.change_state(cluster, common_events::DEACTIVATE) }.
          to raise_error(custom_error_util::PreConditionRequiredError)
        end
      end
    end
  end
end
