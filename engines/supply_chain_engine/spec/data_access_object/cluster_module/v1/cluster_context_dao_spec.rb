#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    require 'rails_helper'

    RSpec.describe ClusterModule::V1::ClusterContextDao, type: :dao do

      let(:cluster_context_dao) { ClusterModule::V1::ClusterContextDao.new }
      let(:cluster) { FactoryGirl.create(:cluster) }

      let(:context_request) {
        {
          level: Faker::Number.number(1),
          level_id: Faker::Number.number(1),
          cluster_id: cluster.id
        }
      }

      context 'create cluster context' do
        it 'with valid params' do
          
          context = cluster_context_dao.create(context_request)
          expect(context.level).to eq(context_request[:level].to_i)
        end
      end

      context 'update cluster context' do
        it 'with valid arguments' do
          context_before = cluster_context_dao.create(context_request)
          new_level = Faker::Number.number(1)
          context = cluster_context_dao.update({ level: new_level }, context_before)
          expect(context.level).to eq(new_level.to_i)
        end
      end

    end
  end
end
