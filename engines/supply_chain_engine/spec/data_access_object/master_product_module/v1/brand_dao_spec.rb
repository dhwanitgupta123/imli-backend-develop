#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MasterProductModule::V1::BrandDao, type: :dao do
      let(:brand_dao) { MasterProductModule::V1::BrandDao.new }
      let(:brand_name) { Faker::Company.name }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      let(:brand_request) {
        {
          name: Faker::Company.name,
          code: Faker::Lorem.characters(5),
          logo_url: Faker::Company.logo,
          company: nil,
          initials: Faker::Lorem.characters(2)
        }
      }

      context 'create brand' do
        it 'with valid params' do
          company = FactoryGirl.create(:company)
          brand = brand_dao.create(brand_request.merge(company: company))
          expect(brand.name).to eq(brand_request[:name])
          brand.destroy
          company.destroy
        end

        it 'with invalid name' do
          expect { brand_dao.create(brand_request.merge(name: nil)) }
            .to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update brand' do
        let(:new_name) { Faker::Company.name }
        let(:company) { FactoryGirl.create(:company) }
        let(:brand_before) { FactoryGirl.build(:brand, name: brand_name, company: company) }
        it 'with valid brand' do
          brand = brand_dao.update({ name: new_name }, brand_before)
          expect(brand.name).to eq(new_name)
          brand.destroy
          company.destroy
        end

        it 'with invalid brand name' do
          expect { brand_dao.update(nil, brand_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update brand state' do
        it 'from inactive to active and active to inactive' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, name: brand_name, company: company)
          brand = brand_dao.change_brand_state(brand.id, common_events::DEACTIVATE )
          expect(brand.status).to eq(common_states::INACTIVE)
          brand = brand_dao.change_brand_state(brand.id, common_events::ACTIVATE)
          expect(brand.status).to eq(common_states::ACTIVE)
          brand.delete
        end

        it 'from active to soft_delete' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, name: brand_name, company: company)
          brand = brand_dao.change_brand_state(brand.id, common_events::SOFT_DELETE)
          expect(brand.status).to eq(common_states::DELETED)
          brand.delete
          company.delete
        end

        it 'from inactive to soft_delete' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, name: brand_name, company: company)
          brand = brand_dao.change_brand_state(brand.id, common_events::SOFT_DELETE)
          expect(brand.status).to eq(common_states::DELETED)
          brand.delete
          company.delete
        end

        it 'raise error change state from S1 to S1' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, name: brand_name, company: company)
          expect{ brand_dao.change_brand_state(brand.id, common_events::ACTIVATE) }.
          to raise_error(custom_error_util::PreConditionRequiredError)
          brand.delete
          company.delete
        end
      end

      context 'return brand corresponding to id' do
        it 'return valid brand' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          response = brand_dao.get_brand_by_id(brand.id)
          expect(response).to eq(brand)
          brand.destroy
          company.destroy 
        end

        it 'return nil with invalid args' do
          response = brand_dao.get_brand_by_id(Faker::Lorem.characters(10))
          expect(response).to eq(nil)
        end
      end
    end
  end
end
