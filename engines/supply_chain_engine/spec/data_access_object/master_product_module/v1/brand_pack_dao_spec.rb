#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MasterProductModule::V1::BrandPackDao, type: :dao do
      let(:brand_pack_dao) { MasterProductModule::V1::BrandPackDao.new }
      let(:brand_pack_photo_url) { Faker::Avatar.image }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:image_service_helper) { ImageServiceModule::V1::ImageServiceHelper }

      let(:brand_pack_request) {
        {
          sku: Faker::Company.ein,
          mrp: Faker::Commerce.price,
          sku_size: Faker::Lorem.characters(10),
          unit: Faker::Lorem.characters(5),
          description: Faker::Lorem.sentence,
          shelf_life: Faker::Internet.slug(Faker::Number.between(1, 12).to_s, 'months'),
          product: nil,
          images: [
            {
              'priority' => 1, 'image_id' => 1
            }
          ]
        }
      }

      context 'create brand_pack' do
        it 'with valid params' do
          expect(image_service_helper).to receive(:get_sorted_image_id_array_by_priority).and_return([1])
          expect(image_service_helper).to receive(:validate_images).and_return(true)
          department = FactoryGirl.create(:department)
          category =  FactoryGirl.create(:category, department: department)
          sub_category = FactoryGirl.create(:category, parent_category: category)
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          brand_pack = brand_pack_dao.create(brand_pack_request.merge(product_id: product.id, sub_category_id: sub_category.id))
          expect(brand_pack.mrp).to eq(brand_pack_request[:mrp])
          brand_pack.destroy
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy
          department.destroy
          category.destroy
          sub_category.destroy
        end

        it 'with in valid image ids' do
          expect(image_service_helper).
            to receive(:get_sorted_image_id_array_by_priority).and_return([1])
          expect(image_service_helper).
            to receive(:validate_images).and_return(false)
          expect{ brand_pack_dao.create(brand_pack_request) }.to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update brand_pack' do
        let(:new_mrp) { Faker::Commerce.price }
        let(:department) { FactoryGirl.create(:department) }
        let(:category) { FactoryGirl.create(:category, department: department) }
        let(:sub_category) { FactoryGirl.create(:category, parent_category: category) }

        let(:company) { FactoryGirl.create(:company) }
        let(:brand) { FactoryGirl.create(:brand, company: company) }
        let(:sub_brand) { FactoryGirl.create(:sub_brand, brand: brand) }
        let(:product) { FactoryGirl.create(:product, sub_brand: sub_brand) }
        let(:brand_pack_before) { FactoryGirl.build(:brand_pack, mrp: Faker::Commerce.price, product: product, sub_category: sub_category) }
        it 'with valid brand_pack' do
          expect(image_service_helper).to receive(:get_sorted_image_id_array_by_priority).and_return([1])
          expect(image_service_helper).to receive(:validate_images).and_return(true)
          brand_pack = brand_pack_dao.update({ 
            mrp: new_mrp,
            images: [{'priority' => 1, 'image_id' => 1}]
            },
            brand_pack_before
          )

          expect(brand_pack.mrp).to eq(new_mrp)
          brand_pack.destroy
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy
          department.destroy
          category.destroy
          sub_category.destroy
        end

        it 'with invalid brand_pack name' do
          expect { brand_pack_dao.update(nil, brand_pack_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with in valid image ids' do
          expect(image_service_helper).to receive(:get_sorted_image_id_array_by_priority).and_return([1])
          expect(image_service_helper).to receive(:validate_images).and_return(false)
          expect{ brand_pack_dao.update(brand_pack_dao.update({
            images: [{'priority' => 1, 'image_id' => 1}]
            },
            brand_pack_before
          ))}.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update brand_pack state' do
        
        it 'from inactive to active and active to inactive' do
          department = FactoryGirl.create(:department)
          category =  FactoryGirl.create(:category, department: department)
          sub_category = FactoryGirl.create(:category, parent_category: category)
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          brand_pack = FactoryGirl.create(:brand_pack, product: product, sub_category: sub_category)
          brand_pack = brand_pack_dao.change_brand_pack_state(brand_pack.id, common_events::DEACTIVATE)
          expect(brand_pack.status).to eq(common_states::INACTIVE)
          brand_pack = brand_pack_dao.change_brand_pack_state(brand_pack.id, common_events::ACTIVATE)
          expect(brand_pack.status).to eq(common_states::ACTIVE)
          brand_pack.destroy
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy
          department.destroy
          category.destroy
          sub_category.destroy
        end

        it 'from active to soft_delete' do
          department = FactoryGirl.create(:department)
          category =  FactoryGirl.create(:category, department: department)
          sub_category = FactoryGirl.create(:category, parent_category: category)
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          brand_pack = FactoryGirl.create(:brand_pack, product: product, sub_category: sub_category)
          brand_pack = brand_pack_dao.change_brand_pack_state(brand_pack.id, common_events::SOFT_DELETE)
          expect(brand_pack.status).to eq(common_states::DELETED)
          brand_pack.destroy
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy
          department.destroy
          category.destroy
          sub_category.destroy
        end

        it 'from inactive to soft_delete' do
          department = FactoryGirl.create(:department)
          category =  FactoryGirl.create(:category, department: department)
          sub_category = FactoryGirl.create(:category, parent_category: category)
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          brand_pack = FactoryGirl.create(:brand_pack, product: product, sub_category: sub_category)
          brand_pack = brand_pack_dao.change_brand_pack_state(brand_pack.id, common_events::SOFT_DELETE)
          expect(brand_pack.status).to eq(common_states::DELETED)
          brand_pack.destroy
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy   
          department.destroy
          category.destroy
          sub_category.destroy     
        end

        it 'raise error change state from S1 to S1' do
          department = FactoryGirl.create(:department)
          category =  FactoryGirl.create(:category, department: department)
          sub_category = FactoryGirl.create(:category, parent_category: category)
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          brand_pack = FactoryGirl.create(:brand_pack, product: product, sub_category: sub_category)
          expect{ brand_pack_dao.change_brand_pack_state(brand_pack.id, common_events::ACTIVATE) }.
          to raise_error(custom_error_util::PreConditionRequiredError)
          brand_pack.destroy
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy 
          department.destroy
          category.destroy
          sub_category.destroy       
        end
      end

      context 'return brand pack corresponding to id' do
        it 'return valid brand pack' do
          department = FactoryGirl.create(:department)
          category =  FactoryGirl.create(:category, department: department)
          sub_category = FactoryGirl.create(:category, parent_category: category)
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          product = FactoryGirl.create(:product, sub_brand: sub_brand)
          brand_pack = FactoryGirl.create(:brand_pack, product: product, sub_category: sub_category)
          response = brand_pack_dao.get_brand_pack_by_id(brand_pack.id)
          expect(response).to eq(brand_pack)
          brand_pack.destroy
          product.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy
          department.destroy
          category.destroy
          sub_category.destroy 
        end

        it 'return nil with invalid args' do
          response = brand_pack_dao.get_brand_pack_by_id(Faker::Lorem.characters(10))
          expect(response).to eq(nil)
        end
      end
    end
  end
end
