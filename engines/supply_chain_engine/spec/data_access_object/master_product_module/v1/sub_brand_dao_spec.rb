#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    require 'rails_helper'

    RSpec.describe MasterProductModule::V1::SubBrandDao, type: :dao do
      let(:sub_brand_dao) { MasterProductModule::V1::SubBrandDao.new }
      let(:sub_brand_name) { Faker::Company.name }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }

      let(:sub_brand_request) {
        {
          name: Faker::Company.name,
          logo_url: Faker::Company.logo,
          brand: nil,
          initials: Faker::Lorem.characters(2)
        }
      }

      context 'create sub_brand' do
        it 'with valid params' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = sub_brand_dao.create(sub_brand_request.merge(brand: brand))
          expect(sub_brand.name).to eq(sub_brand_request[:name])
          sub_brand.destroy
          brand.destroy
          company.destroy
        end

        it 'with invalid name' do
          expect { sub_brand_dao.create(sub_brand_request.merge(name: nil)) }
            .to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update sub_brand' do
        let(:new_name) { Faker::Company.name }
        let(:company) { FactoryGirl.create(:company) }
        let(:brand) { FactoryGirl.create(:brand, company: company) }
        let(:sub_brand_before) { FactoryGirl.build(:sub_brand, name: sub_brand_name, brand: brand) }
        it 'with valid sub_brand' do
          sub_brand = sub_brand_dao.update({ name: new_name }, sub_brand_before)
          expect(sub_brand.name).to eq(new_name)
          sub_brand.destroy
          brand.destroy
          company.destroy
        end

        it 'with invalid sub_brand name' do
          expect { sub_brand_dao.update(nil, sub_brand_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update sub brand state' do
        
        it 'from inactive to active and active to inactive' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          sub_brand = sub_brand_dao.change_sub_brand_state(sub_brand.id, common_events::DEACTIVATE)
          expect(sub_brand.status).to eq(common_states::INACTIVE)
          sub_brand = sub_brand_dao.change_sub_brand_state(sub_brand.id, common_events::ACTIVATE)
          expect(sub_brand.status).to eq(common_states::ACTIVE)
          sub_brand.destroy
          sub_brand.destroy
          brand.destroy
          company.destroy
        end

        it 'from active to soft_delete' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          sub_brand = sub_brand_dao.change_sub_brand_state(sub_brand.id, common_events::SOFT_DELETE)
          expect(sub_brand.status).to eq(common_states::DELETED)
          sub_brand.destroy
          brand.destroy
          company.destroy
        end

        it 'from inactive to soft_delete' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          sub_brand = sub_brand_dao.change_sub_brand_state(sub_brand.id, common_events::SOFT_DELETE)
          expect(sub_brand.status).to eq(common_states::DELETED)
          sub_brand.destroy
          brand.destroy
          company.destroy        
        end

        it 'raise error change state from S1 to S1' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          expect{ sub_brand_dao.change_sub_brand_state(sub_brand.id, common_events::ACTIVATE) }.
          to raise_error(custom_error_util::PreConditionRequiredError)
          sub_brand.destroy
          brand.destroy
          company.destroy        
        end
      end

      context 'return sub brand corresponding to id' do
        it 'return valid sub brand' do
          company = FactoryGirl.create(:company)
          brand = FactoryGirl.create(:brand, company: company)
          sub_brand = FactoryGirl.create(:sub_brand, brand: brand)
          response = sub_brand_dao.get_sub_brand_by_id(sub_brand.id)
          expect(response).to eq(sub_brand)
          sub_brand.destroy
          brand.destroy
          company.destroy 
        end

        it 'return nil with invalid args' do
          response = sub_brand_dao.get_sub_brand_by_id(Faker::Lorem.characters(10))
          expect(response).to eq(nil)
        end
      end
    end
  end
end
