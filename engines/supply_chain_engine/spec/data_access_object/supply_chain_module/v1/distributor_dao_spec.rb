#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'

    RSpec.describe SupplyChainModule::V1::DistributorDao, type: :dao do
      let(:distributor_dao) { SupplyChainModule::V1::DistributorDao.new }
      let(:distributor_name) { Faker::Company.name }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }

      let(:distributor_request) {
        {
          name: Faker::Company.name
        }
      }

      context 'create distributor' do
        it 'with valid params' do
          distributor = distributor_dao.create(distributor_request)
          expect(distributor.name).to eq(distributor_request[:name])
          distributor.destroy
        end

        it 'with invalid name' do
          expect { distributor_dao.create(distributor_request.merge(name: nil)) }
            .to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update distributor' do
        let(:new_name) { Faker::Company.name }
        let(:distributor_before) { FactoryGirl.build(:distributor, name: distributor_name) }
        it 'with valid distributor' do
          distributor = distributor_dao.update({ name: new_name }, distributor_before)
          expect(distributor.name).to eq(new_name)
          distributor.destroy
        end

        it 'with invalid distributor name' do
          expect { distributor_dao.update(nil, distributor_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update distributor state' do
        it 'from inactive to active and active to inactive' do
          distributor = FactoryGirl.create(:distributor, name: distributor_name)
          distributor = distributor_dao.change_state(distributor, common_events::ACTIVATE)
          expect(distributor.status).to eq(common_states::ACTIVE)
          distributor = distributor_dao.change_state(distributor, common_events::DEACTIVATE)
          expect(distributor.status).to eq(common_states::INACTIVE)
          distributor.delete
        end

        it 'from active to soft_delete' do
          distributor = FactoryGirl.create(:distributor, name: distributor_name)
          active_distributor = distributor_dao.change_state(distributor, common_events::ACTIVATE)
          distributor = distributor_dao.change_state(active_distributor, common_events::SOFT_DELETE)
          expect(distributor.status).to eq(common_states::DELETED)
          distributor.delete
        end

        it 'from inactive to soft_delete' do
          distributor = FactoryGirl.create(:distributor, name: distributor_name)
          distributor = distributor_dao.change_state(distributor, common_events::SOFT_DELETE)
          expect(distributor.status).to eq(common_states::DELETED)
          distributor.delete
        end

        it 'raise error change state from S1 to S1' do
          distributor = FactoryGirl.create(:distributor, name: distributor_name)
          expect{ distributor_dao.change_state(distributor, common_events::DEACTIVATE) }.
          to raise_error(custom_error_util::PreConditionRequiredError)
          distributor.delete
        end
      end
    end
  end
end
