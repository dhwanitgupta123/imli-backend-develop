#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SupplyChainModule::V1::SellerDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:seller_model) { SupplyChainModule::V1::Seller }

      let(:seller_dao) { SupplyChainModule::V1::SellerDao.new }

      let(:seller) { FactoryGirl.build(:seller) }

      let(:request) {
        {
          name: Faker::Company.name
        }
      }

      context 'create seller' do
        it 'with invalid seller' do
          expect{ seller_dao.create(request.merge(name: nil)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with new params ' do
          seller = seller_dao.create(request)
          expect(seller.name).to eq(seller[:name])
          seller.destroy
        end
      end

      context 'get all seller' do
        let(:pagination_params) { { sort_by: 'name', order: 'ASC' } }
        it 'with wrong sort_by' do
          expect{ seller_dao.paginated_seller(pagination_params.merge(sort_by: 'abc')) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          expect{ seller_dao.paginated_seller(pagination_params.merge(sort_by: 'abc')) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          expect(seller_dao.paginated_seller(pagination_params)).to_not be_nil
        end
      end

      context 'change seller state' do
        let(:seller) { FactoryGirl.create(:seller) }
        let(:change_state) { { id: seller.id, event: 1 }}
        it 'with wrong transition flow' do
          seller_model.any_instance.stub(:trigger_event).and_raise(
            custom_error_util::PreConditionRequiredError)
          expect{ seller_dao.change_state(change_state) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
        end

        it 'with wrong event' do
          expect{ seller_dao.change_state(change_state.merge(event: 5)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
        end

        it 'with correct request' do
          expect(seller_dao.change_state(change_state)).to_not be_nil
        end
      end

      context 'get seller' do
        let(:seller) { FactoryGirl.create(:seller) }
        let(:request) { seller.id }
        it 'with wrong id' do
          expect{ seller_dao.get_seller_by_id(Faker::Number.number(2)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          expect(seller_dao.get_seller_by_id(request)).to_not be_nil
        end

      end

      context 'update seller' do
        let(:seller) { FactoryGirl.create(:seller) }
        let(:request) { { name: Faker::Name.name } }
        it 'with wrong request' do
          expect{ seller_dao.update(request.merge(name: nil), seller) }.to raise_error(
            custom_error_util::InvalidArgumentsError)   
        end

        it 'with correct params' do
          expect(seller_dao.update(request, seller)).to_not be_nil
        end
      end
    end
  end
end