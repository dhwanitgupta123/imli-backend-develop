#
# Module to handle all the functionalities related to warehouse product product
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    require 'rails_helper'
    RSpec.describe WarehouseProductModule::V1::WarehouseBrandPackDao do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:wbp_model) { WarehouseProductModule::V1::WarehouseBrandPack }
      let(:events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:wbp_dao) { WarehouseProductModule::V1::WarehouseBrandPackDao.new }

      context 'create wbp' do
        it 'with nil warehouse_id' do
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          request = {
            wbp: {
              warehouse: nil,
              brand_pack: brand_pack
            },
            inventory_brand_pack: ibp,
            pricing: {
              mrp: Faker::Commerce.price,
              selling_price: Faker::Commerce.price,
              service_tax: Faker::Commerce.price,
              vat: Faker::Commerce.price,
              cst: Faker::Commerce.price
            }
          }
          expect{ wbp_dao.create(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
          ibp.inventory_pricing.delete
          ibp.delete
          brand_pack.delete
        end

        it 'with nil brand_pack_id' do
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse = FactoryGirl.create(:warehouse)
          request = {
            wbp: {
              warehouse: warehouse,
              brand_pack: nil
            },
            inventory_brand_pack: ibp
          }
          expect{ wbp_dao.create(request) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
          ibp.inventory_pricing.delete
          ibp.delete
          brand_pack.delete
          warehouse.delete
        end

        it 'with new params ' do
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse = FactoryGirl.create(:warehouse)
          request = {
            wbp: {
              warehouse: warehouse,
              brand_pack: brand_pack
            },
            inventory_brand_pack: ibp
          }
          warehouse_brand_pack = wbp_dao.create(request)
          expect(warehouse_brand_pack.warehouse).to eq(request[:wbp][:warehouse])
          ibp.inventory_pricing.delete
          ibp.delete
          warehouse_brand_pack.warehouse_pricings.delete_all
          warehouse_brand_pack.delete
          brand_pack.delete
          warehouse.delete
        end
      end

      context 'get all wbps' do
        let(:pagination_params) { { sort_by: 'id', order: 'ASC' } }
        it 'with wrong sort_by' do
          expect{ wbp_dao.paginated_wbps(pagination_params.merge(sort_by: 'abc')) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with wrong order' do
          expect{ wbp_dao.paginated_wbps(pagination_params.merge(sort_by: 'abc')) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          expect(wbp_dao.paginated_wbps(pagination_params)).to_not be_nil
        end
      end

      context 'change wbp state' do
        it 'with wrong transition flow' do
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse = FactoryGirl.create(:warehouse)
          request = {
            wbp: {
              warehouse: warehouse,
              brand_pack: brand_pack
            },
            inventory_brand_pack: ibp
          }
          warehouse_brand_pack = wbp_dao.create(request)
          change_state = { id: warehouse_brand_pack.id, event: events::ACTIVATE }
          wbp_model.any_instance.stub(:trigger_event).and_raise(
            custom_error_util::PreConditionRequiredError)
          expect{ wbp_dao.change_state(change_state) }.to raise_error(
            custom_error_util::PreConditionRequiredError)
          ibp.inventory_pricing.delete
          ibp.delete
          warehouse_brand_pack.warehouse_pricings.delete_all
          warehouse_brand_pack.delete
          brand_pack.delete
          warehouse.delete
        end

        it 'with wrong event' do
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse = FactoryGirl.create(:warehouse)
          request = {
            wbp: {
              warehouse: warehouse,
              brand_pack: brand_pack
            },
            inventory_brand_pack: ibp
          }
          warehouse_brand_pack = wbp_dao.create(request)
          change_state = { id: warehouse_brand_pack.id, event: 5 }
          expect{ wbp_dao.change_state(change_state) }.to raise_error(
            custom_error_util::InvalidArgumentsError)
          ibp.inventory_pricing.delete
          ibp.delete
          warehouse_brand_pack.warehouse_pricings.delete_all
          warehouse_brand_pack.delete
          brand_pack.delete
          warehouse.delete
        end

        it 'with correct request' do
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse = FactoryGirl.create(:warehouse)
          request = {
            wbp: {
              warehouse: warehouse,
              brand_pack: brand_pack
            },
            inventory_brand_pack: ibp
          }
          warehouse_brand_pack = wbp_dao.create(request)
          change_state = { id: warehouse_brand_pack.id, event: events::ACTIVATE }
          expect(wbp_dao.change_state(change_state)).to_not be_nil
          ibp.inventory_pricing.delete
          ibp.delete
          warehouse_brand_pack.warehouse_pricings.delete_all
          warehouse_brand_pack.delete
          brand_pack.delete
          warehouse.delete
        end
      end

      context 'get wbp' do
        it 'with wrong id' do
          expect{ wbp_dao.get_wbp_by_id(Faker::Number.number(2)) }.to raise_error(
            custom_error_util::InvalidArgumentsError)     
        end

        it 'with correct params' do
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse = FactoryGirl.create(:warehouse)
          request = {
            wbp: {
              warehouse: warehouse,
              brand_pack: brand_pack
            },
            inventory_brand_pack: ibp
          }
          warehouse_brand_pack = wbp_dao.create(request)
          expect(wbp_dao.get_wbp_by_id(warehouse_brand_pack.id)).to_not be_nil
          ibp.inventory_pricing.delete
          ibp.delete
          warehouse_brand_pack.warehouse_pricings.delete_all
          warehouse_brand_pack.delete
          brand_pack.delete
          warehouse.delete
        end
      end

      context 'update wbp' do
        it 'with correct params' do
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse = FactoryGirl.create(:warehouse)
          request = {
            wbp: {
              warehouse: warehouse,
              brand_pack: brand_pack
            },
            inventory_brand_pack: ibp
          }
          warehouse_brand_pack = wbp_dao.create(request)
          ibp.brand_pack = brand_pack
          expect(wbp_dao.update({ wbp: { warehouse_id: warehouse.id } }, warehouse_brand_pack)).to_not be_nil
          ibp.inventory_pricing.delete
          ibp.delete
          warehouse_brand_pack.warehouse_pricings.delete_all
          warehouse_brand_pack.delete
          brand_pack.delete
          warehouse.delete 
        end

        it 'with correct params' do
          brand_pack = FactoryGirl.create(:brand_pack)
          ibp = FactoryGirl.create(:inventory_brand_pack, brand_pack: brand_pack)
          warehouse = FactoryGirl.create(:warehouse)
          request = {
            wbp: {
              warehouse: warehouse,
              brand_pack: brand_pack
            },
            inventory_brand_pack: ibp
          }
          warehouse_brand_pack = wbp_dao.create(request)
          ibp.brand_pack = brand_pack
          args = { wbp: { brand_pack_id: brand_pack.id }, ibp: ibp }
          expect(wbp_dao.update(args, warehouse_brand_pack)).to_not be_nil
          ibp.inventory_pricing.delete
          ibp.delete
          warehouse_brand_pack.warehouse_pricings.delete_all
          warehouse_brand_pack.delete
          brand_pack.delete
          warehouse.delete 
        end
      end
    end
  end
end
