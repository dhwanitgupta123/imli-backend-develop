# SupplyChain Engine CONFIG initialization should be done before engine initialization
# Load all environment specific application config variables into SUPPLY_CHAIN_ENGINE_CONFIG variable

supply_chain_engine_config_file = SupplyChainEngine::Engine.root.join("config", "supply_chain_engine_config.yml").to_s

# Global variable SUPPLY_CHAIN_ENGINE_CONFIG to store all environment specific config
# This is similar to APP_CONFIG in application
SUPPLY_CHAIN_ENGINE_CONFIG = {}

if File.exists?(supply_chain_engine_config_file)
  YAML.load_file(supply_chain_engine_config_file)[Rails.env].each do |key, value|
    SUPPLY_CHAIN_ENGINE_CONFIG[key.to_s] = value.to_s
  end # end YAML.load_file
end # end if


#
# Checks if redis is blank, which oughts tobe,
# hence initialized it with Redis configuration
# read from the supply_chain_config_file
#
if $redis.blank?
  puts '--> Initializing Redis Cache from SupplyChain Engine'
  $redis = Redis.new(
    :host => SUPPLY_CHAIN_ENGINE_CONFIG["redis_host"],
    :port => SUPPLY_CHAIN_ENGINE_CONFIG["redis_port"]
  )
end

puts '--> Initializing SupplyChain Engine resources'

#
# Write Key Value pair to Redis
#
# @param key [String] [Key for which value is to be stored]
# @param value [Object] [Value to be stored]
#
def write_key_value_to_redis(key, value)
  CommonModule::V1::Cache.write({
                                  key: key.to_s,
                                  value: value.to_s
                                })
end
