#
# Initializing marketplace-selling-pack-index repository
#
elastic_search_url = SUPPLY_CHAIN_ENGINE_CONFIG['ELASTIC_SEARCH_URL']
MARKETPLACE_SELLING_PACK_INDEX_REPOSITORY = SearchModule::V1::MarketplaceSellingPackRepository.new url: elastic_search_url, log: true