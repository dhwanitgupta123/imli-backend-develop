#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # BrandPack Util to contain utils used for BrandPack
    #
    class BrandPackUtil

      BRAND_PACK_DAO = MasterProductModule::V1::BrandPackDao
      PRODUCT_UTIL = MasterProductModule::V1::ProductUtil

      # 
      # This function determines the brand_packs
      #
      # @param args [Hash] 
      # - it may contain brand_packs to be filtered (if not passed, filters will be put over BrandPack model)
      # - it may contain sub_brands, and more filters (as per future requirements)
      # 
      # @return [Model] brand_packs
      #
      def self.get_brand_packs(args)
        brand_pack_dao = BRAND_PACK_DAO.new

        if args[:sub_brands].present?
          products = PRODUCT_UTIL.filter_products({sub_brands: args[:sub_brands]}) 

          return brand_pack_dao.filter_brand_packs_by_products({
            brand_packs: args[:brand_packs], 
            products: products
            })
        end

        return nil
      end

    end
  end
end
