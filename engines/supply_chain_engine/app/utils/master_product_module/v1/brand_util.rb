#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Brand Util to contain utils used for Brands
    #
    class BrandUtil

      BRAND_DAO = MasterProductModule::V1::BrandDao

      # 
      # This function determines the brands
      #
      # @param args [Hash] it may contain company_id
      # 
      # @return [Model] brands
      #
      def self.get_brands_by_company(company_id)
        brand_dao = BRAND_DAO.new

        return brand_dao.get_brands_by_company(company_id)
      end

    end
  end
end
