#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # SubBrand Util to contain utils used for SubBrands
    #
    class SubBrandUtil

      SUB_BRAND_DAO = MasterProductModule::V1::SubBrandDao
      BRAND_UTIL = MasterProductModule::V1::BrandUtil

      # 
      # This function determines the sub_brands
      #
      # @param args [Hash] it may contain sub_brand_id, brand_id and company_id
      # 
      # @return [Model] SubBrand
      #
      def self.get_sub_brands(args)
        sub_brand_dao = SUB_BRAND_DAO.new

        if args[:sub_brand_id].present?
          return sub_brand_dao.get_sub_brand_by_id(args[:sub_brand_id])
        end

        if args[:brand_id].present?
          return sub_brand_dao.get_sub_brands_by_brands(args[:brand_id])
        end

        if args[:company_id].present?

          brands = BRAND_UTIL.get_brands_by_company(args[:company_id])
          return sub_brand_dao.get_sub_brands_by_brands(brands)
        end

        return nil
      end

    end
  end
end
