#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Utility file for mpsp index
    #
  	class MpspIndexUtil

      PAGINATION_UTIL = CommonModule::V1::Pagination
      DEFAULT_PER_PAGE = 10
      DEFAULT_PAGE_NO = 1

      def self.paginated_result(results, pagination_params)

        per_page = (pagination_params[:per_page] || DEFAULT_PER_PAGE).to_f
        page_no = (pagination_params[:page_no] || DEFAULT_PAGE_NO).to_f

        init_index = (page_no - 1) * per_page

        return results.slice(init_index, per_page)
      end
  	end
  end
end
