#
# This module is responsible to handle all model caches
#
module CacheModule
  #
  # Version1 for validation module
  #
  module V1
    #
    # mpsp index cache client
    #
    class MpspIndexCacheClient < CacheModule::V1::CacheClient

      NAMESPACE = 'MPSP::INDEX::QUERY'
      MPSP_SEARCH_MODEL = SearchModule::V1::MarketplaceSellingPackSearchModel

      TTL = 10 * 60 # 10 minutes cache

      #
      # set mpsp array of hash within query and namespace
      # key for the cache is search_query with namespace and saving it with TTL 10 minutes
      #
      # @params query [String] query string
      # @params results [Array] array of results
      #
      def self.set_mpsp(query, results)

        return if results.blank?

        products = []
        results.each do |result|
          products.push(result.attributes)
        end

        set({
              key: query,
              value: products
            }, NAMESPACE, TTL)
      end

      #
      # get search model array, this function returns array of mpsp search model if key is present in a cache
      #
      # @params query [String] query string
      #
      # @params [MarketplaceSellingPackSearchModel] array of search models
      #
      def self.get_mpsp(query)

        products = get({
              key: query
            }, NAMESPACE)

        return [] if products.blank?

        search_model_array = []

        products.each do |product|
          search_model_array.push(MPSP_SEARCH_MODEL.new(product))
        end

        return search_model_array
      end
    end
  end
end
