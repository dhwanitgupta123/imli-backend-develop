#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Warehouse util class to implement utility functions
    #    
    class WarehouseUtil

      STOCK_SHEET_NAME = 'stock_sheet.csv'
      PROCUREMENT_SHEET_NAME = 'procurement_sheet.csv'
      FILE_SERVICE_HELPER = FileServiceModule::V1::FileServiceHelper

      #
      # Function to generate link of Procurement sheet
      #
      def self.generate_procurement_sheet_link(warehouse_procurement)
        generate_procurement_sheet(warehouse_procurement)
        return upload_to_s3(@path, PROCUREMENT_SHEET_NAME)
      end

      #
      # Function to generate Stock sheet link on s3
      #
      def self.generate_stock_sheet_link(warehouse_stock)
        generate_stock_sheet(warehouse_stock)
        return upload_to_s3(@path, STOCK_SHEET_NAME)
      end

      #
      # Function to upload CSV on s3
      #
      def self.upload_to_s3(path, file_name)
        # Upload to S3 and get signed URL
        signed_csv_expiry_duration = 5.minutes
        signed_csv_link = FILE_SERVICE_HELPER.upload_file_to_s3_and_get_signed_url(
          path, file_name, signed_csv_expiry_duration)
        File.delete(path)
        return signed_csv_link
      end

      #
      # Function to generate procurement sheet locally
      #
      def self.generate_procurement_sheet(warehouse_procurement)
        @path = Rails.root.join(PROCUREMENT_SHEET_NAME)
        output_file = CSV.open(PROCUREMENT_SHEET_NAME,'w')
        ##Header##
        header = ['bp_code', 'sku', 'stock_available', 'mrp', 'threshold', 'x_days', 'order_per_x_days', 'prediction']
        output_file << header
        warehouse_procurement.each do |wbp_procurement|
          bp_code = wbp_procurement[:brand_pack][:brand_pack_code]
          sku = wbp_procurement[:brand_pack][:sku] + ' ' + wbp_procurement[:brand_pack][:sku_size] + wbp_procurement[:brand_pack][:unit]
          stock_available = wbp_procurement[:procurement][:available]
          mrp = wbp_procurement[:brand_pack][:mrp]
          threshold = wbp_procurement[:threshold_stock_value]
          x_days = wbp_procurement[:outbound_frequency_period_in_days]
          order_per_x_days = wbp_procurement[:procurement][:sold_in_x_days]
          prediction = wbp_procurement[:procurement][:prediction]
          row = [bp_code, sku, stock_available, mrp, threshold, x_days, order_per_x_days, prediction]
          output_file << row
          row = []
        end
        output_file.close
      end

      #
      # Function to generate stock sheet locally
      #
      def self.generate_stock_sheet(warehouse_stock)
        @path = Rails.root.join(STOCK_SHEET_NAME)
        output_file = CSV.open(STOCK_SHEET_NAME,'w')
        ##Header##
        header = ['last_audited_date', 'bp_code', 'sku', 'starting_stock_value', 'inward', 'blocked', 'outward', 'expired', 'damaged', '+ve correction', '-ve correction', 'ideal_stock_available']
        output_file << header
        warehouse_stock.each do |wbp_stock|
          last_audited_date = wbp_stock[:stock][:audit_date]
          bp_code = wbp_stock[:brand_pack][:brand_pack_code]
          sku = wbp_stock[:brand_pack][:sku] + ' ' + wbp_stock[:brand_pack][:sku_size] + wbp_stock[:brand_pack][:unit]
          starting_stock_value = wbp_stock[:stock][:last_audit_quantity]
          inward = wbp_stock[:stock][:inward]
          blocked = wbp_stock[:stock][:blocked]
          outward = wbp_stock[:stock][:outward]
          expired = wbp_stock[:stock][:expired]
          damaged = wbp_stock[:stock][:damaged]
          negative_correction = wbp_stock[:stock][:negative_correction]
          positive_correction = wbp_stock[:stock][:positive_correction]
          ideal_stock_available = wbp_stock[:stock][:available]
          row = [last_audited_date, bp_code, sku, starting_stock_value, inward, blocked, outward, expired, damaged, positive_correction, negative_correction, ideal_stock_available]
          output_file << row
          row = []
        end
        output_file.close
      end

      #
      # Update the stock threshold values of WBP
      #
      def self.update_stock_threshold_value
        wbp_service = WarehouseProductModule::V1::WarehouseBrandPackService.new
        wbp_service.update_stock_threshold_value
      end

      def self.update_buffer_stock_value
        wbp_service = WarehouseProductModule::V1::WarehouseBrandPackService.new
        wbp_service.update_buffer_stock_value
      end

      def self.send_out_of_stock_products_report
        out_of_stock_products_array = []
        header = ["bp_code", "company", "department", "category", "sub_category", "sku", "mpsp_afffected", "buffer value", "available"]
        out_of_stock_products_array.push(header)
        data = get_out_of_stock_products
        data.each do |row|
          out_of_stock_products_array.push(row)
        end
        inventory_order_notification_service = InventoryOrdersModule::V1::InventoryOrderNotificationService.new
        inventory_order_notification_service.send_out_of_stock_products_report([{data: out_of_stock_products_array, name: 'out_of_stock_products.csv'}])
      end

      def self.get_out_of_stock_products
        out_of_stock_products_array = []
        wbp_dao = WarehouseProductModule::V1::WarehouseBrandPackDao.new
        warehouse_stock_service = WarehouseProductModule::V1::WarehouseStockService.new
        wbps = wbp_dao.get_non_jit_products
        wbps.each do |wbp|
          brand_pack = wbp.brand_pack
          if brand_pack.is_out_of_stock
            company = brand_pack.product.sub_brand.brand.company.name
            sub_category = brand_pack.sub_category
            category = sub_category.parent_category
            department = category.department
            buffer_stock = wbp.buffer_stock
            pricing = wbp.warehouse_pricings.first
            stock_details = warehouse_stock_service.get_wbp_stock_details(pricing.warehouse_stocks)
            available = stock_details[:available]
            bp_code = brand_pack.brand_pack_code
            sku = brand_pack.sku + ' ' + brand_pack.sku_size + ' ' + brand_pack.unit
            mpsps_affected = get_mpsps_for_corresponding_brand_pack(brand_pack)
            mpsps_affected_name = get_mpsps_affected_name_string(mpsps_affected)
            out_of_stock_products_array.push([bp_code, company, department.label, category.label, sub_category.label, sku, mpsps_affected_name, buffer_stock, available])
          end
        end
        return out_of_stock_products_array
      end

      def self.get_mpsps_for_corresponding_brand_pack(brand_pack)
        marketplace_brand_pack_service = MarketplaceProductModule::V1::MarketplaceBrandPackService.new
        return marketplace_brand_pack_service.get_mpsps_for_corresponding_brand_pack(brand_pack)
      end

      def self.get_mpsps_affected_name_string(mpsps)
        mpsps_affected_name = ''
        return mpsps_affected_name if mpsps.blank?
        mpsps.each do |mpsp|
          mpsps_affected_name += mpsp.display_name + ' ' + mpsp.display_pack_size + ' | '
        end
        mpsps_affected_name = mpsps_affected_name.chomp(' | ')
        return mpsps_affected_name
      end
    end
  end
end
