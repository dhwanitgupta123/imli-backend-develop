#
# Module to handle all the functionalities related to inventory products
#
module InventoryProductModule
  #
  # Version1 for inventory product module
  #
  module V1
    #
    # InventoryBrandPack Util to contain utils used for InventoryBrandPack
    #
    class InventoryBrandPackUtil

      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content

      #
      # Validates the required params inventory_brand_pack
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def self.validate_add_ibp_request(request)
        if request.nil? || request[:inventory_id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVENTORY_MISSING)
        end

        if request[:brand_pack_id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_MISSING)
        end
        if request[:pricing].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_MUST_PRESENT%{ field: 'Pricing details' })
        end
        if request[:pricing][:vat].blank? || request[:pricing][:vat].to_f < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_NON_NEGATIVE%{ field: 'VAT' })
        end
        if request[:pricing][:net_landing_price].blank? || request[:pricing][:net_landing_price].to_f < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_NON_NEGATIVE%{ field: 'Net Landing Price' })
        end
        if request[:pricing][:margin].blank? || request[:pricing][:margin].to_f < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_NON_NEGATIVE%{ field: 'Margin' })
        end
        if request[:pricing][:octrai].blank? || request[:pricing][:octrai].to_f < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Octroi' })
        end
        if request[:pricing][:cst].blank? || request[:pricing][:cst].to_f < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_ONLY_POSITIVE%{ field: 'CST' })
        end
      end

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def self.validate_update_ibp_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVENTORY_BRAND_PACK_MISSING)
        end
        if request[:pricing].present?
          if request[:pricing][:vat].present? && request[:pricing][:vat].to_f < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'VAT' })
          end
          if request[:pricing][:net_landing_price].present? && request[:pricing][:net_landing_price].to_f < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'Net Landing Price' })
          end
          if request[:pricing][:margin].present? && request[:pricing][:margin].to_f < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'Margin' })
          end
          if request[:pricing][:octrai].present? && request[:pricing][:octrai].to_f < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Octroi' })
          end
          if request[:pricing][:cst].present? && request[:pricing][:cst].to_f < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_ONLY_POSITIVE%{ field: 'CST' })
          end
        end
      end
    end
  end
end
