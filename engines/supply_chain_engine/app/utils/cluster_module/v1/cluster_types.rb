#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # This module contain cluster level array and functionalities related to it
    # 
    module ClusterTypes

      #
      # Cluster Types is array of hash where each hash contain 
      # cluster_type -> unique integer representing cluster
      # display_type -> type need to be display in panel 
      #
      CLUSTER_TYPES = [
        {
          cluster_type: 1,
          display_type: 'Banner',
        },
        {
          cluster_type: 2,
          display_type: 'Non Banner'
        },
        {
          cluster_type: 3,
          display_type: 'Mini Banner'
        }
      ]

      # 
      # Return cluster_type hash by given type as input
      #
      # @param type [Integer] type of cluster
      # 
      # @return [hash] cluster_type hash
      #
      def self.get_cluster_type_by_type(type)
        cluster_type_array = CLUSTER_TYPES.select {|cluster_type| cluster_type[:cluster_type] == type.to_i}
        return cluster_type_array[0] if cluster_type_array.present?
        return nil
      end

      # 
      # Return display_type of cluster_types
      #
      # @param type [Integer] type of cluster
      # 
      # @return [String] display_type of cluster
      #
      def self.get_display_type(type)
        cluster_type_array = CLUSTER_TYPES.select {|cluster_type| cluster_type[:cluster_type] == type.to_i}
        return cluster_type_array[0][:display_type] if cluster_type_array.present?
        return ''
      end
    end
  end
end
