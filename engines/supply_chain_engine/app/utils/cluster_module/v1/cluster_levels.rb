#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # This module contain cluster level array and functionalities related to it
    # 
    module ClusterLevels

      #
      # Cluster_Levels array contain hash each containing
      # level and display_level
      # level -> Integer which represents hierarchy of cluster and saved in cluster_contexts table
      # display_level -> corresponding display level used in Panel
      # 
      # 
      CLUSTER_LEVELS = [
        {
          level: 1,
          display_level: 'GLOBAL'
        },
        {
          level: 2,
          display_level: 'DEPARTMENT',
          dao: CategorizationModule::V1::MpspDepartmentDao 
        },
        {
          level: 3,
          display_level: 'CATEGORY',
          dao: CategorizationModule::V1::MpspCategoryDao
        }
      ]

      # 
      # This return hash corresponding to the input level
      #
      # @param level [Integer] level in cluster_level array
      # 
      # @return [Hash] cluster_level hash containing level, display_level and dao as keys
      #
      def self.get_cluster_level_by_level(level)
        cluster_level = CLUSTER_LEVELS.select {|cluster_level| cluster_level[:level] == level.to_i}
        return cluster_level[0] if cluster_level.present?
        return nil
      end

      # 
      # This return display level corresponding to level if it is valid
      # else return default level GLOBAL
      #
      # @param level [Integer] level in cluster_level array
      # 
      # @return [String] display_level corresponding to level 
      #
      def self.get_display_level(level)
        cluster_level = CLUSTER_LEVELS.select {|cluster_level| cluster_level[:level] == level.to_i}
        return cluster_level[0][:display_level] if cluster_level.present?
        return CLUSTER_LEVELS[0][:display_level]
      end

      # 
      # This function return title of the level corresponding to level_id
      #
      # @param level [Integer] level of cluster
      # @param level_id [Integer] level_id of level
      # 
      # @return [String] title of the level corresponding to level_id
      #
      def self.get_level_label(level, level_id)
        cluster_level = get_cluster_level_by_level(level)
        return '' if cluster_level.blank? || cluster_level[:dao].blank?

        return cluster_level[:dao].get_label_by_id(level_id)
      end
    end
  end
end
