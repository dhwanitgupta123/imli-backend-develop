#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # GetClustersApi validates the request, call the
    # cluster service and return the JsonResponse
    #
    class GetClustersPanelApi < SupplyChainBaseModule::V1::BaseApi

      CLUSTER_SERVICE = ClusterModule::V1::ClusterService
      CLUSTER_RESPONSE_DECORATOR = ClusterModule::V1::ClusterResponseDecorator
      CLUSTER_MAPPER = ClusterModule::V1::ClusterPanelMapper
      COMMON_STATES = SupplyChainCommonModule::V1::CommonStates

      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the clusters
      #
      # @return [JsonResponse]
      #
      def enact(request)

        cluster_service = CLUSTER_SERVICE.new(@params)
        begin
          clusters_hash = cluster_service.get_filtered_clusters(request)
          clusters = CLUSTER_MAPPER.map_cluster_array_to_array_of_hash(clusters_hash[:elements])
          return CLUSTER_RESPONSE_DECORATOR.create_clusters_response(clusters, clusters_hash[:page_count])
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return CLUSTER_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      # 
      # validate is status in allowed or not
      #
      def validate_request(request)
        return if request[:state].blank?

        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_STATE) unless COMMON_STATES::ALLOWED_STATES.include?(request[:state].to_i)
      end
    end
  end
end
