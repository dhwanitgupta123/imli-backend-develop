#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    #
    # Get seller Brand Pack api, it validates the request, call the
    # seller brand pack service and return the JsonResponse
    #
    class GetSellerBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      SBP_SERVICE = SellerProductModule::V1::SellerBrandPackService
      SBP_MAPPER = SellerProductModule::V1::SellerBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to get SBP and returns the
      # success or error response
      #
      # @param request [integer] Request SBP id
      #
      # @return [JsonResponse]
      #
      def enact(sbp_id)
        sbp_service = SBP_SERVICE.new
        begin
          validate_request(sbp_id)
          sbp = sbp_service.get_seller_brand_pack(sbp_id)
          sbp_array = SBP_MAPPER.map_seller_brand_pack_to_hash(sbp)
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_single_seller_brand_pack_response(sbp_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params SBP id
      #
      # @param request [integer SBP id]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(sbp_id)
        if sbp_id.blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SELLER_BRAND_PACK_MISSING)
        end
      end
    end
  end
end