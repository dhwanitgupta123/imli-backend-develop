#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    #
    # Change state api, it validates the req, call the
    # seller brand pack service and return the JsonResponse
    #
    class ChangeSellerBrandPackStateApi < SupplyChainBaseModule::V1::BaseApi
      SELLER_BRAND_PACK_SERVICE = SellerProductModule::V1::SellerBrandPackService
      SELLER_BRAND_PACK_MAPPER = SellerProductModule::V1::SellerBrandPackMapper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create seller brand pack and returns the
      # success or error response
      #
      # @param request [Hash] Request object, seller params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        seller_brand_pack_service_class = SELLER_BRAND_PACK_SERVICE.new
        begin
          validate_request(request)
          seller_brand_pack = seller_brand_pack_service_class.change_state(request)
          seller_brand_pack_array = SELLER_BRAND_PACK_MAPPER.map_seller_brand_pack_to_hash(seller_brand_pack)
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_single_seller_brand_pack_response(seller_brand_pack_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::UnAuthorizedUserError => e
          return SELLER_PRODUCT_RESPONSE_DECORATOR.create_information_not_found(e.message)
        end
      end

      private

      #
      # Validates the required params seller_brand_pack name
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end