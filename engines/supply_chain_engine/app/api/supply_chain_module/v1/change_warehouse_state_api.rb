#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Change warehouse state api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class ChangeWarehouseStateApi < SupplyChainBaseModule::V1::BaseApi
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      WAREHOUSE_MAPPER = SupplyChainModule::V1::WarehouseMapper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change warehouse state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, Warehouse change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        warehouse_service = WAREHOUSE_SERVICE.new
        begin
          validate_request(request)
          warehouse = warehouse_service.change_state(request)
          warehouse_array = WAREHOUSE_MAPPER.map_warehouse_to_hash(warehouse)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_warehouse_response(warehouse_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params state change event
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end
