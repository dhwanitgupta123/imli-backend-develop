#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Add warehouse api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class AddWarehouseApi < SupplyChainBaseModule::V1::BaseApi
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      WAREHOUSE_MAPPER = SupplyChainModule::V1::WarehouseMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create warehouse and returns the
      # success or error response
      #
      # @param request [Hash] Request object, Warehouse params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        warehouse_service = WAREHOUSE_SERVICE.new
        begin
          validate_request(request)
          warehouse = warehouse_service.create(request)
          warehouse_array = WAREHOUSE_MAPPER.map_warehouse_to_hash(warehouse)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_warehouse_response(warehouse_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params warehouse name
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:name].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_NAME_MISSING)
        end
        if request[:cash_and_carry_id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::CASH_AND_CARRY_MISSING)
        end
      end
    end
  end
end
