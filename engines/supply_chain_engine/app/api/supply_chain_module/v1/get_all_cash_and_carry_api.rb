#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Get all cash and carry api, it validates the request, call the
    # cash and carry service and return the JsonResponse
    #
    class GetAllCashAndCarryApi < SupplyChainBaseModule::V1::BaseApi
      CASH_AND_CARRY_SERVICE = SupplyChainModule::V1::CashAndCarryService
      CASH_AND_CARRY_MAPPER = SupplyChainModule::V1::CashAndCarryMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the cash and carriers
      #
      # @return [JsonResponse]
      #
      def enact(request)
        c_and_c_service_class = CASH_AND_CARRY_SERVICE.new

        begin
          c_and_cs = c_and_c_service_class.get_all_cash_and_carry(request)
          c_and_c_array = CASH_AND_CARRY_MAPPER.map_cash_and_carrys_to_array(c_and_cs)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_cash_and_carry_response(c_and_c_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
