#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Get seller api, it validates the request, call the
    # seller service and return the JsonResponse
    #
    class GetSellerApi < SupplyChainBaseModule::V1::BaseApi
      SELLER_SERVICE = SupplyChainModule::V1::SellerService
      SELLER_MAPPER = SupplyChainModule::V1::SellerMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to get seller and returns the
      # success or error response
      #
      # @param request [integer] Request, seller id
      #
      # @return [JsonResponse]
      #
      def enact(seller_id)
        seller_service = SELLER_SERVICE.new
        begin
          validate_request(seller_id)
          seller = seller_service.get_seller(seller_id)
          seller_array = SELLER_MAPPER.map_seller_to_hash(seller)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_seller_response(seller_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params seller id to be fetched
      #
      # @param request [seller id]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(seller_id)
        if seller_id.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SELLER_MISSING)
        end
      end
    end
  end
end