#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetCompanies api, it validates the request, call the
    # company service and return the JsonResponse
    #
    class GetCompaniesApi < SupplyChainBaseModule::V1::BaseApi
      COMPANY_SERVICE = SupplyChainModule::V1::CompanyService
      COMPANY_MAPPER = SupplyChainModule::V1::CompanyMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the companies
      #
      # @return [JsonResponse]
      #
      def enact(request)
        company_service_class = COMPANY_SERVICE.new

        begin
          response = company_service_class.get_all_companies(request)
          companies = response[:elements]
          page_count = response[:page_count]
          company_array = COMPANY_MAPPER.map_companies_to_array(companies)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_company_response(company_array, page_count)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
