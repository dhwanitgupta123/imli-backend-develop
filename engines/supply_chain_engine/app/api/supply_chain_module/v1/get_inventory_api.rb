#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetCompanies api, it validates the request, call the
    # inventory service and return the JsonResponse
    #
    class GetInventoryApi < SupplyChainBaseModule::V1::BaseApi
      INVENTORY_SERVICE = SupplyChainModule::V1::InventoryService
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return inventory corresponding to id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        inventory_service_class = INVENTORY_SERVICE.new

        if request.blank? || request[:id].blank?
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::INVENTORY_MISSING)
        end

        begin
          inventory = inventory_service_class.get_inventory_by_id(request[:id])
          inventory = INVENTORY_MAPPER.map_inventory_to_hash(inventory)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_inventory_response(inventory)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
