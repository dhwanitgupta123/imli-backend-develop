#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # warehouse EOD api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class WarehouseEndOfDayApi < SupplyChainBaseModule::V1::BaseApi
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      WAREHOUSE_MAPPER = SupplyChainModule::V1::WarehouseMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to mark EOD of warehouse and returns the
      # success or error response
      #
      # @param request [Hash] Request object, Warehouse ID
      #
      # @return [JsonResponse]
      #
      def enact(request)
        warehouse_service = WAREHOUSE_SERVICE.new
        begin
          validate_request(request)
          warehouse_service.transactional_warehouse_end_of_day(request)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_success_response
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params warehouse ID
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_MISSING)
        end
      end
    end
  end
end
