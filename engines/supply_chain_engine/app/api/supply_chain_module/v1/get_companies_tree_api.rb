#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetCompaniesTree api, calls the
    # company service and return the JsonResponse
    #
    class GetCompaniesTreeApi < SupplyChainBaseModule::V1::BaseApi
      COMPANY_SERVICE = SupplyChainModule::V1::CompanyService
      SUPPLY_CHAIN_RESPONSE_DECORATOR = SupplyChainModule::V1::SupplyChainResponseDecorator
      
      def initialize(params)
        @params = params
        super
      end

      #
      # Function calls company service and returns the tree:
      # caompanies -> brands -> sub_brands tree
      #
      # @return [JsonResponse]
      #
      def enact
        company_service_class = COMPANY_SERVICE.new(@params)
        aggregated_data = company_service_class.get_companies_tree
        return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_aggregated_data_response(aggregated_data)
      end
    end
  end
end
