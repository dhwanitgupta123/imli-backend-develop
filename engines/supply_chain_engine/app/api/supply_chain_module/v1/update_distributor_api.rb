#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # UpdateDistributor api, it validates the request, call the
    # distributor service and return the JsonResponse
    #
    class UpdateDistributorApi < SupplyChainBaseModule::V1::BaseApi
      DISTRIBUTOR_SERVICE = SupplyChainModule::V1::DistributorService
      DISTRIBUTOR_MAPPER = SupplyChainModule::V1::DistributorMapper
      GENERAL_HELPER = CommonModule::V1::GeneralHelper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update distributor and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have distributor arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request)
        distributor_service_class = DISTRIBUTOR_SERVICE.new

        begin
          validate_request(request)
          distributor = distributor_service_class.update_distributor(request)
          distributor = DISTRIBUTOR_MAPPER.map_distributor_to_hash(distributor)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_distributor_response(distributor)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError, CUSTOM_ERROR_UTIL::ResourceNotFoundError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::DISTRIBUTOR_MISSING)
        end
        if request[:contact_number].present?
          unless GENERAL_HELPER.phone_number_valid?(request[:contact_number])
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_PHONE_NUMBER)
          end
        end
        if request[:email_ids].present?
          request[:email_ids].each do |email_id|
            unless GENERAL_HELPER.email_id_valid?(email_id)
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EMAIL_ID)
            end
          end
        end
      end
    end
  end
end
