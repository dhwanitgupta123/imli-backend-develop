#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # AddCompany api, it validates the request, call the
    # company service and return the JsonResponse
    #
    class AddCompanyApi < SupplyChainBaseModule::V1::BaseApi
      COMPANY_SERVICE = SupplyChainModule::V1::CompanyService
      COMPANY_MAPPER = SupplyChainModule::V1::CompanyMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create company and returns the
      # success or error response
      #
      # @param request [Hash] Request object, company params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        company_service_class = COMPANY_SERVICE.new
        begin
          validate_request(request)
          company = company_service_class.create_company(request)
          company = COMPANY_MAPPER.map_company_to_hash(company)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_company_response(company)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params company name
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:name].nil? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::COMPANY_NAME_MISSING)
        end
        if request[:initials].nil? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('request should contain company initials')
        end
      end
    end
  end
end
