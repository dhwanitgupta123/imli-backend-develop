#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Get all warehouse api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class GetAllWarehouseApi < SupplyChainBaseModule::V1::BaseApi
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      WAREHOUSE_MAPPER = SupplyChainModule::V1::WarehouseMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the warehouses
      #
      # @return [JsonResponse]
      #
      def enact(request)
        warehouse_service = WarehouseService.new

        begin
          warehouses = warehouse_service.get_all_warehouses(request)
          warehouse_array = WAREHOUSE_MAPPER.map_warehouses_to_array(warehouses)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_warehouse_response(warehouse_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
