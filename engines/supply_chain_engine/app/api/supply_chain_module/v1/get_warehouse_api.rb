#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Get warehouse api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class GetWarehouseApi < SupplyChainBaseModule::V1::BaseApi
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      WAREHOUSE_MAPPER = SupplyChainModule::V1::WarehouseMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to get warehouse and returns the
      # success or error response
      #
      # @param request [integer] Request, Warehouse id
      #
      # @return [JsonResponse]
      #
      def enact(warehouse_id)
        warehouse_service = WAREHOUSE_SERVICE.new
        begin
          validate_request(warehouse_id)
          warehouse = warehouse_service.get_warehouse(warehouse_id)
          warehouse_array = WAREHOUSE_MAPPER.map_warehouse_to_hash(warehouse)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_warehouse_response(warehouse_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params warehouse id to be fetched
      #
      # @param request [Warehouse id]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(warehouse_id)
        if warehouse_id.blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_MISSING)
        end
      end
    end
  end
end
