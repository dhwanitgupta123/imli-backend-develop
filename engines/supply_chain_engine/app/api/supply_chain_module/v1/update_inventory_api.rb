#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # UpdateInventory api, it validates the request, call the
    # inventory service and return the JsonResponse
    #
    class UpdateInventoryApi < SupplyChainBaseModule::V1::BaseApi
      INVENTORY_SERVICE = SupplyChainModule::V1::InventoryService
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update inventory and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have inventory arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request)
        inventory_service_class = INVENTORY_SERVICE.new

        begin
          validate_request(request)
          inventory = inventory_service_class.update_inventory(request)
          inventory = INVENTORY_MAPPER.map_inventory_to_hash(inventory)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_inventory_response(inventory)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVENTORY_MISSING)
        end
      end
    end
  end
end
