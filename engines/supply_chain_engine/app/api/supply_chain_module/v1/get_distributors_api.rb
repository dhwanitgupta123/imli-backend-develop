#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetDistributors api, it validates the request, call the
    # distributor service and return the JsonResponse
    #
    class GetDistributorsApi < SupplyChainBaseModule::V1::BaseApi
      DISTRIBUTOR_SERVICE = SupplyChainModule::V1::DistributorService
      DISTRIBUTOR_MAPPER = SupplyChainModule::V1::DistributorMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the distributors
      #
      # @return [JsonResponse]
      #
      def enact(request)
        distributor_service_class = DISTRIBUTOR_SERVICE.new

        begin
          response = distributor_service_class.get_all_distributors(request)
          distributors = response[:elements]
          page_count = response[:page_count]
          distributor_array = DISTRIBUTOR_MAPPER.map_distributors_to_array(distributors)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_distributor_response(distributor_array, page_count)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
