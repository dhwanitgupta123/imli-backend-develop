#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Change state api, it validates the req, call the
    # seller service and return the JsonResponse
    #
    class ChangeSellerStateApi < SupplyChainBaseModule::V1::BaseApi
      SELLER_SERVICE = SupplyChainModule::V1::SellerService
      SELLER_MAPPER = SupplyChainModule::V1::SellerMapper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create company and returns the
      # success or error response
      #
      # @param request [Hash] Request object, seller params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        seller_service_class = SELLER_SERVICE.new
        begin
          validate_request(request)
          seller = seller_service_class.change_state(request)
          seller_array = SELLER_MAPPER.map_seller_to_hash(seller)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_seller_response(seller_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message) 
        rescue CUSTOM_ERROR_UTIL::UnAuthorizedUserError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_information_not_found(e.message)
        end
      end

      private

      #
      # Validates the required params seller name
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end