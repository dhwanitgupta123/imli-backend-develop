#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # ChangeCompanyStateApi api, it validates the request, call the
    # company service to change the state of given company id
    #
    class ChangeCompanyStateApi < SupplyChainBaseModule::V1::BaseApi
      COMPANY_SERVICE = SupplyChainModule::V1::CompanyService
      COMPANY_MAPPER = SupplyChainModule::V1::CompanyMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change company state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        company_service_class = COMPANY_SERVICE.new
        begin
          validate_request(request)
          company = company_service_class.change_company_state(request[:id], request[:event].to_i)
          company = COMPANY_MAPPER.map_company_to_hash(company)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_company_response(company)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      private

      #
      # Validates the required params of change state
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::COMPANY_MISSING)
        end
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end
