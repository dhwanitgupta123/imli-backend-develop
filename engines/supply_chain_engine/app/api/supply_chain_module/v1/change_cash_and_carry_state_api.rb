#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Change state api, it validates the req, call the
    # c&c service and return the JsonResponse
    #
    class ChangeCashAndCarryStateApi < SupplyChainBaseModule::V1::BaseApi
      CASH_AND_CARRY_SERVICE = SupplyChainModule::V1::CashAndCarryService
      CASH_AND_CARRY_MAPPER = SupplyChainModule::V1::CashAndCarryMapper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change C&C state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, C&C change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        c_and_c_service_class = CASH_AND_CARRY_SERVICE.new
        begin
          validate_request(request)
          c_and_c = c_and_c_service_class.change_state(request)
          c_and_c_array = CASH_AND_CARRY_MAPPER.map_cash_and_carry_to_hash(c_and_c)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_cash_and_carry_response(c_and_c_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
         rescue CUSTOM_ERROR_UTIL::InsufficientDataError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params status change event 
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end
