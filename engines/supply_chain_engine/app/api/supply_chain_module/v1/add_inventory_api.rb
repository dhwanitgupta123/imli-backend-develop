#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # AddInventory api, it validates the request, call the
    # inventory service and return the JsonResponse
    #
    class AddInventoryApi < SupplyChainBaseModule::V1::BaseApi
      INVENTORY_SERVICE = SupplyChainModule::V1::InventoryService
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create inventory and returns the
      # success or error response
      #
      # @param request [Hash] Request object, inventory params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        inventory_service_class = INVENTORY_SERVICE.new
        begin
          validate_request(request)
          inventory = inventory_service_class.create_inventory(request)
          inventory = INVENTORY_MAPPER.map_inventory_to_hash(inventory)
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_single_inventory_response(inventory)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return SUPPLY_CHAIN_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params inventory name
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:name].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVENTORY_NAME_MISSING)
        end
        if request[:distributor_id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::DISTRIBUTOR_MISSING)
        end
      end
    end
  end
end
