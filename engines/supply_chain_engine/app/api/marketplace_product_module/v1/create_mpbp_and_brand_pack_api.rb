#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # THIS IS TEMPORARY API TO CREATE MPBP WHILE CREATING BRAND_PACK AS THERE IS ONLY
    # ONE INVENTORY
    # ONE WAREHOUSE
    # ONE SELLER
    # HENCE THERE IS ALWAYS ONE TO ONE MAPPING
    #
    # CreateMpbpAndBrandPackApi api, it validates the request, call the
    # brand_pack service and return the JsonResponse
    #
    class CreateMpbpAndBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      
      BRAND_PACK_MAPPER = MasterProductModule::V1::BrandPackMapper
      MARKETPLACE_BRAND_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceBrandPackService

      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create the brand_pack and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have brand_pack params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        begin
          validate_request(request)
          marketplace_brand_pack_service_class = MARKETPLACE_BRAND_PACK_SERVICE.new
          brand_pack = marketplace_brand_pack_service_class.transactional_create_mpbp_and_brand_pack(request)

          brand_pack = BRAND_PACK_MAPPER.map_brand_pack_to_hash(brand_pack)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_brand_pack_response(brand_pack)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params product_id and reference sub_category
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)

        if request.blank? || request[:product_id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PRODUCT_MISSING)
        end

        if request[:sub_category_id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Reference to sub category not provide')
        end
      end
    end
  end
end
