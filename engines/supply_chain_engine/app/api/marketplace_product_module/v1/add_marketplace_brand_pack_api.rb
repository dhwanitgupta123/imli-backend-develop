#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # AddMarketplaceBrandPack api, it validates the request, call the
    # marketplace_brand_pack service and return the JsonResponse
    #
    class AddMarketplaceBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      MARKETPLACE_BRAND_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceBrandPackService
      MARKETPLACE_BRAND_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to create marketplace_brand_pack and returns the
      # success or error response
      #
      # @param request [Hash] Request object, marketplace_brand_pack params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        marketplace_brand_pack_service_class = MARKETPLACE_BRAND_PACK_SERVICE.new
        begin
          validate_request(request)
          marketplace_brand_pack = marketplace_brand_pack_service_class.transactional_create_marketplace_brand_pack(request)
          marketplace_brand_pack = MARKETPLACE_BRAND_PACK_MAPPER.map_marketplace_brand_pack_to_hash(
            marketplace_brand_pack)
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_single_marketplace_brand_pack_response(
            marketplace_brand_pack)
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params marketplace_brand_pack
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:brand_pack_id].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_MISSING)
        end
        if request[:seller_brand_pack_id].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SELLER_BRAND_PACK_MISSING)
        end
        if request[:pricing].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_MUST_PRESENT%{ field: 'Pricing details' })
        end
        if request[:pricing][:selling_price].blank? || request[:pricing][:selling_price] <= 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Selling price' })
        end
        if request[:pricing][:buying_price].blank? || request[:pricing][:buying_price] <= 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Buying price' })
        end
        if request[:pricing][:savings].blank? || request[:pricing][:savings] < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_NON_NEGATIVE%{ field: 'Savings' })
        end
        if request[:pricing][:mrp].blank? || request[:pricing][:mrp] <= 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_ONLY_POSITIVE%{ field: 'MRP' })
        end
        if request[:pricing][:vat].blank? || request[:pricing][:vat] < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_NON_NEGATIVE%{ field: 'VAT' })
        end
        if request[:pricing][:spat].blank? || request[:pricing][:spat] <= 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_ONLY_POSITIVE%{ field: 'SPAT' })
        end
        if request[:pricing][:margin].blank? || request[:pricing][:margin] < 0
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
            CONTENT::FIELD_NON_NEGATIVE%{ field: 'Margin' })
        end
      end
    end
  end
end