#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetMarketplaceSellingPack api, it validates the request, call the
    # marketplace_selling_pack service and return the JsonResponse
    #
    class GetMarketplaceSellingPackApi < SupplyChainBaseModule::V1::BaseApi
      MARKETPLACE_SELLING_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceSellingPackService
      MARKETPLACE_SELLING_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceSellingPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return marketplace_selling_pack corresponding to id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        marketplace_selling_pack_service_class = MARKETPLACE_SELLING_PACK_SERVICE.new

        if request.blank? || request[:id].blank?
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::MARKETPLACE_SELLING_PACK_MISSING)
        end

        begin
          marketplace_selling_pack = marketplace_selling_pack_service_class.get_marketplace_selling_pack_by_id(request[:id])
          marketplace_selling_pack = MARKETPLACE_SELLING_PACK_MAPPER.map_marketplace_selling_pack_to_hash(marketplace_selling_pack)
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_single_marketplace_selling_pack_response(marketplace_selling_pack)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end