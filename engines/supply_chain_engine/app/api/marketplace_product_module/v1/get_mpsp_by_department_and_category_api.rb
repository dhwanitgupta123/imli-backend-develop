#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetMpspByDepartmentAndCategoryApi, this return mpsp after
    # applying filters
    # filters can be department_id, caetegory_id or sub_category_id
    #
    class GetMpspByDepartmentAndCategoryApi < SupplyChainBaseModule::V1::BaseApi

      MARKETPLACE_SELLING_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceSellingPackService
      MARKETPLACE_SELLING_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceSellingPackMapper

      def initialize(params)
        @params = params
        super
      end

      #
      # Return filtered marketplace_selling_pack
      #
      # @return [JsonResponse]
      #
      def enact(request)
        marketplace_selling_pack_service_class = MARKETPLACE_SELLING_PACK_SERVICE.new

        marketplace_selling_packs = marketplace_selling_pack_service_class.get_mpsp_by_mpsp_department_and_mpsp_category(request)
        marketplace_selling_pack_array = MARKETPLACE_SELLING_PACK_MAPPER.map_marketplace_selling_packs_to_array(marketplace_selling_packs, true)
        return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_marketplace_selling_pack_response(marketplace_selling_pack_array, marketplace_selling_pack_array.size)
      end
    end
  end
end