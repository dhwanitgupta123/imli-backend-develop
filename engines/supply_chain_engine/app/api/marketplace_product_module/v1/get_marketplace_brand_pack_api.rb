#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetMarketplaceBrandPack api, it validates the request, call the
    # marketplace_brand_pack service and return the JsonResponse
    #
    class GetMarketplaceBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      MARKETPLACE_BRAND_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceBrandPackService
      MARKETPLACE_BRAND_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return marketplace_brand_pack corresponding to id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        marketplace_brand_pack_service_class = MARKETPLACE_BRAND_PACK_SERVICE.new

        if request.blank? || request[:id].blank?
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::MARKETPLACE_BRAND_PACK_MISSING)
        end

        begin
          marketplace_brand_pack = marketplace_brand_pack_service_class.get_marketplace_brand_pack_by_id(request[:id])
          marketplace_brand_pack = MARKETPLACE_BRAND_PACK_MAPPER.map_marketplace_brand_pack_to_hash(marketplace_brand_pack)
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_single_marketplace_brand_pack_response(marketplace_brand_pack)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end