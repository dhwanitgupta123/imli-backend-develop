#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetMarketplaceBrandPack api, it validates the request, call the
    # marketplace_brand_pack service and return the JsonResponse
    #
    class GetAllMarketplaceBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      MARKETPLACE_BRAND_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceBrandPackService
      MARKETPLACE_BRAND_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the marketplace_brand_pack
      #
      # @return [JsonResponse]
      #
      def enact(request)
        marketplace_brand_pack_service_class = MARKETPLACE_BRAND_PACK_SERVICE.new

        begin
          response = marketplace_brand_pack_service_class.get_all_marketplace_brand_pack(request)
          marketplace_brand_pack = response[:elements]
          page_count = response[:page_count]
          marketplace_brand_pack_array = MARKETPLACE_BRAND_PACK_MAPPER.map_marketplace_brand_packs_to_array(marketplace_brand_pack)
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_marketplace_brand_pack_response(marketplace_brand_pack_array, page_count)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end