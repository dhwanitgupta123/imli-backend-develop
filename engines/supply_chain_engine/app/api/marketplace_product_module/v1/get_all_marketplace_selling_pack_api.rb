#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetMarketplaceSellingPack api, it validates the request, call the
    # marketplace_selling_pack service and return the JsonResponse
    #
    class GetAllMarketplaceSellingPackApi < SupplyChainBaseModule::V1::BaseApi
      MARKETPLACE_SELLING_PACK_SERVICE = MarketplaceProductModule::V1::MarketplaceSellingPackService
      MARKETPLACE_SELLING_PACK_MAPPER = MarketplaceProductModule::V1::MarketplaceSellingPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the marketplace_selling_pack
      #
      # @return [JsonResponse]
      #
      def enact(request)
        marketplace_selling_pack_service_class = MARKETPLACE_SELLING_PACK_SERVICE.new

        begin
          response = marketplace_selling_pack_service_class.get_all_marketplace_selling_pack(request)
          marketplace_selling_packs = response[:elements]
          page_count = response[:page_count]

          marketplace_selling_pack_array = MARKETPLACE_SELLING_PACK_MAPPER.map_marketplace_selling_packs_to_array(marketplace_selling_packs)

          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_marketplace_selling_pack_response(marketplace_selling_pack_array, page_count)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MARKETPLACE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end