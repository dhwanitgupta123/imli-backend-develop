#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Get all warehouse Brand Pack api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class GetAllWarehouseBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      WBP_MAPPER = WarehouseProductModule::V1::WarehouseBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the WBPs
      #
      # @return [JsonResponse]
      #
      def enact(request)
        wbp_service = WBP_SERVICE.new

        begin
          wbps = wbp_service.get_all_wbps(request)
          wbp_array = WBP_MAPPER.map_wbps_to_array(wbps)
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_wbp_response(wbp_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
