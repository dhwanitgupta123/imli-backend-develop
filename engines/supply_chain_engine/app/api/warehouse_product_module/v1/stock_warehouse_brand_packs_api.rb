#
# Module to handle all the functionalities related to warehouse product module
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Stock warehouse Brand Pack api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class StockWarehouseBrandPacksApi < SupplyChainBaseModule::V1::BaseApi
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      WBP_MAPPER = WarehouseProductModule::V1::WarehouseBrandPackMapper
      CONTENT = CommonModule::V1::Content
      def initialize(params)
        @params = params
        super
      end

      #
      # Function to stock the the quantity of WBPs
      #
      # @param request [Hash] Request object, WBP params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        wbp_service = WBP_SERVICE.new
        begin
          validate_request(request)
          wbp_service.transactional_stock_warehouse_brand_packs(request)
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_success_response
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params warehouse id, quantity & date of purchase
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:warehouse_id].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_MISSING)
        end
        request[:params].each do |param|
          if param[:id].blank?
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_BRAND_PACK_MISSING)
          end
          if param[:quantity].blank? || param[:quantity].to_i < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_NON_NEGATIVE%{field: 'Quantity'})
          end
          if param[:date_of_purchase].present?
            if param[:date_of_purchase].to_i > Time.zone.now.to_i
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_PURCHASE_DATE)
            end
          else
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_MUST_PRESENT%{field: 'Date of purchase'})
          end
        end
      end
    end
  end
end
