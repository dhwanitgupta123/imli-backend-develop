#
# Module to handle all the functionalities related to warehouse brand pack
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Update warehouse Brand Pack api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class UpdateWarehouseBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      WBP_MAPPER = WarehouseProductModule::V1::WarehouseBrandPackMapper
      CONTENT = CommonModule::V1::Content
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update WBP and returns the
      # success or error response
      #
      # @param request [Hash] Request object, WBP params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        wbp_service = WBP_SERVICE.new
        begin
          request = validate_request(request)
          wbp = wbp_service.transactional_update_warehouse_brand_pack(request)
          wbp_array = WBP_MAPPER.map_wbp_to_hash(wbp)
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_single_wbp_response(wbp_array)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params warehouse id & IBP id
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_BRAND_PACK_MISSING)
        end
        if request[:brand_pack_id].present?
          if request[:inventory_brand_pack_id].blank?
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVENTORY_BRAND_PACK_MISSING)
          end
        end
        if request[:inventory_brand_pack_id].present?
          if request[:brand_pack_id].blank?
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_PACK_MISSING)
          end
        end
        if request[:threshold_stock_value].present? 
          if request[:threshold_stock_value].to_i < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'Threshold stock value'})
          end
          unless request[:threshold_stock_value].numeric?
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::ONLY_NUMBERS_ALLOWED%{ field: 'Threshold stock value'})
          end
        end
        if request[:outbound_frequency_period_in_days].present? 
          if request[:outbound_frequency_period_in_days].to_i < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::FIELD_NON_NEGATIVE%{ field: 'Outbound Frequency period'})
          end
          unless request[:outbound_frequency_period_in_days].numeric?
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
              CONTENT::ONLY_NUMBERS_ALLOWED%{ field: 'Outbound Frequency period'})
          end
        end
        if request[:pricing].present?
          if request[:pricing][:vat].present?
            request[:pricing][:vat] = BigDecimal(request[:pricing][:vat])
            if request[:pricing][:vat] < 0
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
                CONTENT::FIELD_NON_NEGATIVE%{ field: 'VAT' })
            end
          end
          if request[:pricing][:mrp].present?
            request[:pricing][:mrp] = BigDecimal(request[:pricing][:mrp])
            if request[:pricing][:mrp] <= 0
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
                CONTENT::FIELD_ONLY_POSITIVE%{ field: 'MRP' })
            end
          end
          if request[:pricing][:selling_price].present?
            request[:pricing][:selling_price] = BigDecimal(request[:pricing][:selling_price])
            if request[:pricing][:selling_price] <= 0
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
                CONTENT::FIELD_ONLY_POSITIVE%{ field: 'Selling Price' })
            end
          end
          if request[:pricing][:service_tax].present?
            request[:pricing][:service_tax] = BigDecimal(request[:pricing][:service_tax])
            if request[:pricing][:service_tax] < 0
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
                CONTENT::FIELD_NON_NEGATIVE%{ field: 'Service Tax' })
            end
          end
          if request[:pricing][:cst].present?
            request[:pricing][:cst] = BigDecimal(request[:pricing][:cst])
            if request[:pricing][:cst] < 0
              raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(
                CONTENT::FIELD_NON_NEGATIVE%{ field: 'CST' })
            end
          end
        end
        return request
      end
    end
  end
end
