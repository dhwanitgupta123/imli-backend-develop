#
# Module to handle all the functionalities related to warehouse product module
#
module WarehouseProductModule
  #
  # Version1 for warehouse product module
  #
  module V1
    #
    # Stock warehouse Brand Pack api, it validates the request, call the
    # warehouse service and return the JsonResponse
    #
    class WarehouseBrandPacksStockCorrectionApi < SupplyChainBaseModule::V1::BaseApi
      WAREHOUSE_STOCK_STATES = WarehouseProductModule::V1::ModelStates::V1::WarehouseStockStates
      ALLOWED_STATES = [WAREHOUSE_STOCK_STATES::NEGATIVE_CORRECTION, WAREHOUSE_STOCK_STATES::POSITIVE_CORRECTION]
      CONTENT = CommonModule::V1::Content
      def initialize(params={})
        @params = params
        super
      end

      #
      # Function to stock the the quantity of WBPs
      #
      # @param request [Hash] Request object, WBP params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        wbp_service = WarehouseProductModule::V1::WarehouseBrandPackService.new
        begin
          validate_request(request)
          wbp_service.transactional_stock_warehouse_brand_packs_status(request)
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_success_response
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return WAREHOUSE_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params warehouse id, quantity & date of purchase
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:warehouse_id].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_MISSING)
        end
        request[:params].each do |param|
          if param[:id].blank?
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WAREHOUSE_BRAND_PACK_MISSING)
          end
          if param[:quantity].blank? || param[:quantity].to_i < 0
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::FIELD_NON_NEGATIVE%{field: 'Quantity'})
          end
          unless ALLOWED_STATES.include?(param[:status].to_i)
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::NOT_A_VALID_STOCKING_STATE)
          end
        end
      end
    end
  end
end
