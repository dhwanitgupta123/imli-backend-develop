#
# Module to handle all the functionalities related to Packaging
#
module PackagingModule
  #
  # Version1 for Packaging module
  #
  module V1
    #
    # UpdatePackagingBox api, it validates the request, call the
    # packaging_box service and return the JsonResponse
    #
    class UpdatePackagingBoxApi < SupplyChainBaseModule::V1::BaseApi
      PACKAGING_BOX_SERVICE = PackagingModule::V1::PackagingBoxService
      PACKAGING_BOX_MAPPER = PackagingModule::V1::PackagingBoxMapper
      PACKAGING_BOX_RESPONSE_DECORATOR = PackagingModule::V1::PackagingBoxResponseDecorator

      def initialize(params='')
        @params = params
        super
      end

      #
      # Function takes input corresponding to update packaging_box and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have packaging_box arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request)
        packaging_box_service_class = PACKAGING_BOX_SERVICE.new(@params)

        begin
          validate_request(request)
          packaging_box = packaging_box_service_class.update_packaging_box(request)
          update_packaging_box_api_response = PACKAGING_BOX_MAPPER.update_packaging_box_api_response_hash(packaging_box)
          return PACKAGING_BOX_RESPONSE_DECORATOR.create_update_packaging_box_response(update_packaging_box_api_response)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return PACKAGING_BOX_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Request should contain packaging_box id')
        end
        error_array = []
        error_array.push('Label') if request[:label].blank?
        error_array.push('Length') if request[:length].blank?
        error_array.push('Width') if request[:width].blank?
        error_array.push('Height') if request[:height].blank?
        error_array.push('Cost') if request[:cost].blank?
        # Throw a common error with all above caught errors
        if error_array.present?
          error_string = error_array.join(', ')
          raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT_UTIL::FIELD_MUST_PRESENT % {field: error_string})
        end
      end
    end
  end
end