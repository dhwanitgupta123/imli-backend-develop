#
# Module to handle all the functionalities related to Packaging
#
module PackagingModule
  #
  # Version1 for Packaging module
  #
  module V1
    #
    # GetPackagingBox api, it validates the request, call the
    # packaging_box service and return the JsonResponse
    #
    class GetAllPackagingBoxApi < SupplyChainBaseModule::V1::BaseApi
      PACKAGING_BOX_SERVICE = PackagingModule::V1::PackagingBoxService
      PACKAGING_BOX_MAPPER = PackagingModule::V1::PackagingBoxMapper
      PACKAGING_BOX_RESPONSE_DECORATOR = PackagingModule::V1::PackagingBoxResponseDecorator

      def initialize(params='')
        @params = params
        super
      end

      #
      # Return all the packaging_box
      #
      # @return [JsonResponse]
      #
      def enact(request)
        packaging_box_service_class = PACKAGING_BOX_SERVICE.new(@params)

        begin
          response = packaging_box_service_class.get_all_packaging_box(request)
          get_all_packaging_boxes_api_response = PACKAGING_BOX_MAPPER.get_all_packaging_boxes_api_response_array(response)
          return PACKAGING_BOX_RESPONSE_DECORATOR.create_get_all_packaging_boxes_response(get_all_packaging_boxes_api_response)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return PACKAGING_BOX_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end