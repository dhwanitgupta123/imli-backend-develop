#
# Module to handle all the functionalities related to Packaging
#
module PackagingModule
  #
  # Version1 for Packaging module
  #
  module V1
    #
    # ChangePackagingBoxStateApi api, it validates the request, call the
    # packaging_box service to change the state of given packaging_box id
    #
    class ChangePackagingBoxStateApi < SupplyChainBaseModule::V1::BaseApi
      PACKAGING_BOX_SERVICE = PackagingModule::V1::PackagingBoxService
      PACKAGING_BOX_MAPPER = PackagingModule::V1::PackagingBoxMapper
      PACKAGING_BOX_RESPONSE_DECORATOR = PackagingModule::V1::PackagingBoxResponseDecorator

      def initialize(params='')
        @params = params
        super
      end

      #
      # Function takes input corresponding to change packaging_box state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        packaging_box_service_class = PACKAGING_BOX_SERVICE.new(@params)
        begin
          validate_request(request)
          packaging_box = packaging_box_service_class.change_packaging_box_state({ id: request[:id], event: request[:event].to_i })
          change_packaging_box_state_api_response = PACKAGING_BOX_MAPPER.change_packaging_box_state_api_response_hash(packaging_box)
          return PACKAGING_BOX_RESPONSE_DECORATOR.create_change_packaging_box_state_response(change_packaging_box_state_api_response)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return PACKAGING_BOX_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return PACKAGING_BOX_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      private

      #
      # Validates the required params of change state
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Request should contain packaging_box id')
        end
        if request.blank? || request[:event].blank? 
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end