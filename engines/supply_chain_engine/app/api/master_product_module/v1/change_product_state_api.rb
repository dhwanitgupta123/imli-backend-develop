#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # ChangeProductStateApi api, it validates the request, call the
    # product service to change the state of given product id
    #
    class ChangeProductStateApi < SupplyChainBaseModule::V1::BaseApi
      PRODUCT_SERVICE = MasterProductModule::V1::ProductService
      PRODUCT_MAPPER = MasterProductModule::V1::ProductMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to change product state and returns the
      # success or error response
      #
      # @param request [Hash] Request object, change state params
      #
      # @return [JsonResponse]
      #
      def enact(request)
        product_service_class = PRODUCT_SERVICE.new
        begin
          validate_request(request)
          product = product_service_class.change_product_state(request[:id], request[:event].to_i)
          product = PRODUCT_MAPPER.map_product_to_hash(product)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_product_response(product)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        rescue CUSTOM_ERROR_UTIL::PreConditionRequiredError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_pre_condition_required(e.message)
        end
      end

      private

      #
      # Validates the required params of change state
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil? || request[:id].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PRODUCT_MISSING)
        end
        if request[:event].blank?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_MISSING)
        end
        if request[:action] == 'change_state' && request[:event] == COMMON_EVENTS::SOFT_DELETE
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::EVENT_NOT_ALLOWED)
        end
      end
    end
  end
end
