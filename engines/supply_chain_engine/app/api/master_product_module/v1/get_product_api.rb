#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # GetProduct api, it validates the request, call the
    # product service and return the JsonResponse
    #
    class GetProductApi < SupplyChainBaseModule::V1::BaseApi
      PRODUCT_SERVICE = MasterProductModule::V1::ProductService
      PRODUCT_MAPPER = MasterProductModule::V1::ProductMapper
      def initialize(params)
        super
        @params = params
      end

      #
      # Return product corresponding to given id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        product_service_class = PRODUCT_SERVICE.new

        if request.blank? || request[:id].blank?
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::PRODUCT_MISSING)
        end

        begin
          product = product_service_class.get_product_by_id(request[:id])
          product = PRODUCT_MAPPER.map_product_to_hash(product)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_product_response(product)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
