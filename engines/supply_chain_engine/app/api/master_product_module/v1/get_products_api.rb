#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # GetProducts api, it validates the request, call the
    # product service and return the JsonResponse
    #
    class GetProductsApi < SupplyChainBaseModule::V1::BaseApi
      PRODUCT_SERVICE = MasterProductModule::V1::ProductService
      PRODUCT_MAPPER = MasterProductModule::V1::ProductMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return all the products
      #
      # @return [JsonResponse]
      #
      def enact(request)
        product_service_class = PRODUCT_SERVICE.new

        begin
          response = product_service_class.get_all_products(request)
          products = response[:elements]
          page_count = response[:page_count]
          product_array = PRODUCT_MAPPER.map_products_to_array(products)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_product_response(product_array, page_count)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
