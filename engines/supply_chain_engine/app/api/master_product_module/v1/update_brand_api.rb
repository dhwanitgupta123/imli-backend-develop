#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # UpdateBrand api, it validates the request, call the
    # brand service and return the JsonResponse
    #
    class UpdateBrandApi < SupplyChainBaseModule::V1::BaseApi
      BRAND_SERVICE = MasterProductModule::V1::BrandService
      BRAND_MAPPER = MasterProductModule::V1::BrandMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Function takes input corresponding to update brand and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have arguments to update
      #
      # @return [JsonResponse]
      #
      def enact(request, brand_id)
        brand_service_class = BRAND_SERVICE.new

        begin
          validate_request(request)
          brand = brand_service_class.update_brand(request, brand_id)
          brand = BRAND_MAPPER.map_brand_to_hash(brand)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_single_brand_response(brand)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.nil?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::BRAND_MISSING)
        end
      end
    end
  end
end
