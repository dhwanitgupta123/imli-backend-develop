#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # GetBrands api, it validates the request, call the
    # brand service and return the JsonResponse
    #
    class GetBrandsApi < SupplyChainBaseModule::V1::BaseApi
      BRAND_SERVICE = MasterProductModule::V1::BrandService
      BRAND_MAPPER = MasterProductModule::V1::BrandMapper
      def initialize(params)
        super
        @params = params
      end

      #
      # Return all the brands
      #
      # @return [JsonResponse]
      #
      def enact(request)
        brand_service_class = BRAND_SERVICE.new

        begin
          response = brand_service_class.get_all_brands(request)
          brands = response[:elements]
          page_count = response[:page_count]
          brand_array = BRAND_MAPPER.map_brands_to_array(brands)
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_brand_response(brand_array, page_count)
        rescue CUSTOM_ERROR_UTIL::RecordAlreadyExistsError, CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return MASTER_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
