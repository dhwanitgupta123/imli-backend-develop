#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # GetInventoryBrandPack api, it validates the request, call the
    # inventory_brand_pack service and return the JsonResponse
    #
    class GetInventoryBrandPackApi < SupplyChainBaseModule::V1::BaseApi
      INVENTORY_BRAND_PACK_SERVICE = InventoryProductModule::V1::InventoryBrandPackService
      INVENTORY_BRAND_PACK_MAPPER = InventoryProductModule::V1::InventoryBrandPackMapper
      def initialize(params)
        @params = params
        super
      end

      #
      # Return inventory_brand_pack corresponding to id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        inventory_brand_pack_service_class = INVENTORY_BRAND_PACK_SERVICE.new

        if request.blank? || request[:id].blank?
          return INVENTORY_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(
            CONTENT::INVENTORY_BRAND_PACK_MISSING)
        end

        begin
          inventory_brand_pack = inventory_brand_pack_service_class.get_inventory_brand_pack_by_id(request[:id])
          inventory_brand_pack = INVENTORY_BRAND_PACK_MAPPER.map_inventory_brand_pack_to_hash(inventory_brand_pack)
          return INVENTORY_PRODUCT_RESPONSE_DECORATOR.create_single_inventory_brand_pack_response(inventory_brand_pack)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          return INVENTORY_PRODUCT_RESPONSE_DECORATOR.create_response_invalid_arguments_error(e.message)
        end
      end
    end
  end
end
