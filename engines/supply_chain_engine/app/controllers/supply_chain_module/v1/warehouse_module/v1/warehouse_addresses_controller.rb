module SupplyChainModule::V1
  module WarehouseModule::V1
    class WarehouseAddressesController < BaseModule::V1::ApplicationController
      ADD_ADDRESS_API = SupplyChainModule::V1::WarehouseModule::V1::AddAddressApi
      GET_ALL_ADDRESS_API = SupplyChainModule::V1::WarehouseModule::V1::GetAllAddressApi
      UPDATE_ADDRESS_API = SupplyChainModule::V1::WarehouseModule::V1::UpdateAddressApi
      # 
      # initialize requird services
      #
      def initialize
        @application_helper = CommonModule::V1::ApplicationHelper
      end

      ##################################################################################
      #                                get_all_addresses API                           #
      ##################################################################################
      
      # 
      # To get all addresses of a warehouse
      # 
      # Request::
      #   * session_token: a valid session_token is must
      # 
      # @return [response] sends response to user with addresses hash
      # 
      def get_all_addresses
        deciding_params = @application_helper.get_deciding_params(params)
        get_all_address_api = GET_ALL_ADDRESS_API.new(deciding_params)
        response = get_all_address_api.enact(params[:warehouse_id])
        send_response(response)
      end

      swagger_controller :warehouse_addresses, 'WarehouseModule APIs'
      swagger_api :get_all_addresses do
        param :query, :warehouse_id, :string, :required, 'id of warehouse whose addresses to be fetch'
        param :query, :session_token, :string, :required, 'to authenticate user'
        summary 'It returns all the addresses of the warehouse'
        notes 'API returns the addresses of the requested warehouse'
        response :unauthorized, 'user not authenticated'
      end

      ##################################################################################
      #                                add_address API                                 #
      ##################################################################################
      
      # 
      # To add address of a warehouse
      # 
      # Request::
      #   * address: [hash] warehouse_name, address_line1, address_line2, landmark, area_id
      #     * address_line1, address_line2, area_id is must
      #   * session_token: a valid session_token is must
      # 
      # @return [response] sends response to warehouse with address hash
      # 
      def add_address
        deciding_params = @application_helper.get_deciding_params(params)
        add_address_api = ADD_ADDRESS_API.new(deciding_params)
        response = add_address_api.enact(address_params)
        send_response(response)
      end

      swagger_controller :warehouse_addresses, 'WarehouseModule APIs'
      swagger_api :add_address do
        summary 'It adds warehouse address'
        notes 'add address API adds address of the warehouse. address_line1, address_line2, area_id 
        are compulsory. This API calls the warehouse add address API function enact to
        add address of the warehouse.'
        param :body, :add_warehouse_address_request, :add_warehouse_address_request, :required, 'add address request'
        response :unauthorized, 'User not having correct Role to access the Page'
        response :bad_request, 'Parameters missing/invalid parameters'
        response :internal_server_error, 'run time error happened'
      end

      swagger_model :add_warehouse_address_request do
        description 'Request for adding address of a warehouse'
        property :warehouse, :add_warehouse_address_params, :required, 'warehouse address params with address hash'
        property :session_token, :string, :required, 'session token required for identifying a valid session'
      end
      swagger_model :add_warehouse_address_params do
        description 'params for adding address of a warehouse'
        property :id, :string, :required, 'ID of warehouse of which address is added/updated'
        property :addresses, :array, :required, 'address hash with address details', { 'items': { 'type': :warehouse_address_hash } }
      end

      ##################################################################################
      #                                update_address API                              #
      ##################################################################################
      
      # 
      # To update existing address of a warehouse
      # 
      # Request::
      #   * address: [hash] warehouse_address, address_line1, address_line2, landmark, area_id, address_id
      #     * area_id, address_id is must
      #   * session_token: a valid session_token is must
      # 
      # @return [response] sends response to warehouse with address hash
      # 
      def update_address
        deciding_params = @application_helper.get_deciding_params(params)
        update_address_api = UPDATE_ADDRESS_API.new(deciding_params)
        response = update_address_api.enact(address_params.merge({ address_id: params[:address_id] }))
        send_response(response)
      end

      swagger_controller :warehouse_addresses, 'WarehouseModule APIs'
      swagger_api :update_address do
        summary 'It updates warehouse address'
        notes 'update address API updates address of the warehouse. area_id, address_id 
        are compulsory. This API calls the Warehouse service Service function update_address to
        update address of the warehouse. Area_is is not required, keeping it required 
        field for swagger to work'
        param :path, :address_id, :integer, :required, 'Which address to update'
        param :body, :update_warehouse_address_request, :update_warehouse_address_request, :required, 'update address request'
        response :unauthorized, 'User not having correct Role to access the Page'
        response :bad_request, 'Parameters missing/invalid parameters'
        response :internal_server_error, 'run time error happened'
      end

      swagger_model :update_warehouse_address_request do
        description 'Request for updating address of a warehouse'
        property :warehouse, :update_warehouse_address_params, :required, 'warehouse address params with address details'
        property :session_token, :string, :required, 'session token required for identifying a valid session'
      end
      swagger_model :update_warehouse_address_params do
        description 'params for updating address of a warehouse'
        property :id, :string, :required, 'ID of warehouse of which address is added/updated'
        property :addresses, :array, :required, 'address hash with address details', { 'items': { 'type': :warehouse_address_hash } }
      end
      swagger_model :warehouse_address_hash do
        description 'Address hash with address related details of user'
        property :warehouse_name, :string, :optional, 'Warehouse name'
        property :address_line1, :string, :optional, 'warehouse address'
        property :address_line2, :string, :optional, 'warehouse Locality Name'
        property :landmark, :string, :optional, 'Address landmark'
        property :area_id, :string, :required, 'Area id of the area, fetched from pincode'
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # white list parameters for address request.
      # Filter out: address_id, warehouse_name, address_line1, address_line2, landmark, area_id
      #
      def address_params
        if params[:warehouse].present? &&
            params[:warehouse][:addresses].present? &&
            params[:warehouse][:addresses].is_a?(String)
          params[:warehouse][:addresses] = JSON.parse(params[:warehouse][:addresses])
        end
        params.require(:warehouse).permit(:id, :addresses => [:warehouse_name, :address_line1, 
          :address_line2, :landmark, :area_id, :warehouse_id])
      end
    end
  end
end
