#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Warehouse controller service class to route panel API calls to the Warehouse API
    #
    class WarehousesController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_WAREHOUSE = SupplyChainModule::V1::AddWarehouseApi
      GET_ALL_WAREHOUSE = SupplyChainModule::V1::GetAllWarehouseApi
      CHANGE_STATE = SupplyChainModule::V1::ChangeWarehouseStateApi
      GET_WAREHOUSE = SupplyChainModule::V1::GetWarehouseApi
      UPDATE_WAREHOUSE = SupplyChainModule::V1::UpdateWarehouseApi
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      WAREHOUSE_EOD_API = SupplyChainModule::V1::WarehouseEndOfDayApi
      GET_WAREHOUSE_STOCK_DETAILS = SupplyChainModule::V1::GetWarehouseStockDetailsApi
      GET_WAREHOUSE_PROCUREMENT_PREDICTION = SupplyChainModule::V1::GetWarehouseProcurementPredictionApi
      DOWNLOAD_WAREHOUSE_STOCK_DETAILS = SupplyChainModule::V1::DownloadWarehouseStockDetailsApi
      DOWNLOAD_WAREHOUSE_PROCUREMENT_PREDICTION = SupplyChainModule::V1::DownloadWarehouseProcurementPredictionApi
      def initialize
      end

      #
      # function to add warehouse
      #
      # post /warehouses/new
      #
      # Request:: request object contain
      #   * warehouse_params [hash] it contains details of warehouse
      #
      # Response::
      #   * If action succeed then returns Success Status code with create warehouse
      #    else returns BadRequest response
      #
      def add_warehouse
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_warehouse_api = ADD_WAREHOUSE.new(deciding_params)
        response = add_warehouse_api.enact(warehouse_params)
        send_response(response)
      end

      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :add_warehouse do
        summary 'It creates the warehouse with given input'
        notes 'create warehouse'
        param :body, :add_warehouse_api, :add_warehouse_api, :required, 'add_warehouse request'
        response :bad_request, 'if request params are incorrect or api fails to create warehouse'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :add_warehouse_api do
        description 'request schema for /add_warehouse API'
        property :warehouse, :add_warehouse_hash, :required, 'warehouse model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :add_warehouse_hash do
        description 'details of cash and carry'
        property :name, :string, :required, 'name of cash_and_carry'
        property :cash_and_carry_id, :integer, :required, 'id of cash and carry'
      end

      #
      # function to update warehouse
      #
      # put /warehouses/:id
      #
      # Request:: request object contain
      #   * warehouse_params [hash] it contains details of warehouse
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated warehouse
      #    else returns BadRequest response
      #
      def update_warehouse
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_warehouse_api = UPDATE_WAREHOUSE.new(deciding_params)
        response = update_warehouse_api.enact(warehouse_params.merge(id: params[:id]))
        send_response(response)
      end
      
      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :update_warehouse do
        summary 'It updates the warehouse with given input'
        notes 'update warehouse'
        param :path, :id, :integer, :required, 'Warehouse to update'
        param :body, :add_warehouse_api, :add_warehouse_api, :required, 'update_warehouse request'
        response :bad_request, 'if request params are incorrect or api fails to update warehouse'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change state of warehouse
      #
      # put /warehouses/:id/change_state
      #
      # Request:: request object contain
      #   * id [integer] id of warehouse whose state is to be changed
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated warehouse
      #    else returns BadRequest response
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_state_api = CHANGE_STATE.new(deciding_params)
        response = change_state_api.enact(change_state_params)
        send_response(response)
      end

      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the state of warehouse'
        notes 'change_state API takes event & id as input and triggers the event to change the state of the warehouse'
        param :path, :id, :integer, :required, 'warehouse to update'
        param :body, :warehouse_change_state, :warehouse_change_state, :required, 'change_state request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of warehouse'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :warehouse_change_state do
        description 'request schema for /change_state API'
        property :warehouse, :warehouse_change_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :warehouse_change_state_hash do
        description 'event to be triggered'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to get all warehouses
      #
      # GET /warehouses
      #
      # Response::
      #   * list of warehouses
      #
      def get_all_warehouses
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_all_warehouses_api = GET_ALL_WAREHOUSE.new(deciding_params)
        response = get_all_warehouses_api.enact(pagination_params)
        send_response(response)
      end

      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :get_all_warehouses do
        summary 'It returns details of all the warehouses'
        notes 'API returns the details of all the warehouses'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all warehouses'
        param :query, :per_page, :integer, :optional, 'how many warehouse to display in a page'
        param :query, :state, :integer, :optional, 'to fetch warehouse based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'name' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a warehouses
      #
      # GET /warehouses/:id
      #
      # Response::
      #   * Details of a warehouse including it's warehouse brand packs
      #
      def get_warehouse
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_warehouse_api = GET_WAREHOUSE.new(deciding_params)
        response = get_warehouse_api.enact(params[:id])
        send_response(response)
      end

      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :get_warehouse do
        summary 'It returns details of a warehouse'
        notes 'API returns the details of a warehouse'
        param :path, :id, :integer, :required, 'warehouse to get'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to delete a warehouse
      #
      # DELETE /warehouses/:id
      #
      # Response::
      #   * Success if Warehouse found
      #
      def delete_warehouse
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_state_api = CHANGE_STATE.new(deciding_params)
        response = change_state_api.enact({ id: params[:id], event: COMMON_EVENTS::SOFT_DELETE })
        send_response(response)
      end

      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :delete_warehouse do
        summary 'It soft deletes a warehouse'
        notes 'delete_warehouse API calls ChangeWarehouseStateApi to make its state deleted'
        param :path, :id, :integer, :required, 'warehouse to delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # Function to mark the end of the day of the warehouse
      # 
      # POST /warehouses/eod
      # 
      # Response::
      #   * Success when EOD is marked for all products in response
      #
      def warehouse_end_of_day
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        warehouse_end_of_day_api = WAREHOUSE_EOD_API.new(deciding_params)
        response = warehouse_end_of_day_api.enact(warehouse_end_of_day_params)
        send_response(response)
      end

      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :warehouse_end_of_day do
        summary 'It marks the EOD of the warehouse'
        notes 'It marks the EOD of the warehouse by calculating the final
                amount of WBP present in warehouse'
        param :body, :warehouse_end_of_day_params, :warehouse_end_of_day_params, :required, 'Params for warehouses/eod API'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :warehouse_end_of_day_params do
        description 'request schema for /warehouses/eod API'
        property :warehouse, :warehouse_end_of_day_hash, :required, 'hash warehouse for EOD api'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :warehouse_end_of_day_hash do
        description 'warehouse for which EOD to be done'
        property :id, :string, :required, 'Warehouse on which EOD to be done'
      end

      #
      # function to get warehous stock details
      #
      # GET /warehouses/stock/:id
      #
      # Response::
      #   * Function to get Warehouse's WBPs stock details
      #
      def get_warehouse_stock_details
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_warehouse_stock_details_api = GET_WAREHOUSE_STOCK_DETAILS.new(deciding_params)
        response = get_warehouse_stock_details_api.enact(get_warehouse_stock_details_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :get_warehouse_stock_details do
        summary 'It returns stock details of WBPs of a warehouse'
        notes 'API returns the stock details of WBPs of a warehouse'
        param :path, :id, :integer, :required, 'warehouse ID'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all wbps stock details'
        param :query, :per_page, :integer, :optional, 'how many wbps to display in a page'
        param :query, :department_id, :string, :optional, 'to fetch products based on their department'
        param :query, :category_id, :string, :optional, 'to fetch products based on their category'
        param :query, :sub_category_id, :string, :optional, 'to fetch products based on their sub_category'
        param :query, :company_id, :string, :optional, 'to fetch products based on their company'
        param :query, :brand_id, :string, :optional, 'to fetch products based on their brand'
        param :query, :sub_brand_id, :string, :optional, 'to fetch products based on their sub_brand'
        param :query, :state, :integer, :optional, 'to fetch warehouse based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'name' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'Warehouse does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get warheouse procurement prediction details
      #
      # GET /warehouses/procurement_prediction/:id
      #
      # Response::
      #   * Function to get Warehouse's WBPs procurement details
      #
      def get_warehouse_procurement_prediction
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_warehouse_procurement_prediction_api = GET_WAREHOUSE_PROCUREMENT_PREDICTION.new(deciding_params)
        response = get_warehouse_procurement_prediction_api.enact(get_warehouse_stock_details_params.merge(id: params[:id]))
        send_response(response)
      end

      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :get_warehouse_procurement_prediction do
        summary 'It returns stock procurement details of WBPs of a warehouse'
        notes 'API returns the stock procurement details of WBPs of a warehouse'
        param :path, :id, :integer, :required, 'warehouse ID'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all wbps stock details'
        param :query, :per_page, :integer, :optional, 'how many wbps to display in a page'
        param :query, :department_id, :string, :optional, 'to fetch products based on their department'
        param :query, :category_id, :string, :optional, 'to fetch products based on their category'
        param :query, :sub_category_id, :string, :optional, 'to fetch products based on their sub_category'
        param :query, :company_id, :string, :optional, 'to fetch products based on their company'
        param :query, :brand_id, :string, :optional, 'to fetch products based on their brand'
        param :query, :sub_brand_id, :string, :optional, 'to fetch products based on their sub_brand'
        param :query, :state, :integer, :optional, 'to fetch warehouse based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'name' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'Warehouse does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

       #
      # function to download warehous stock details
      #
      # GET /warehouses/stock/download/:id
      #
      # Response::
      #   * Function to download Warehouse's WBPs stock details
      #
      def download_warehouse_stock_details
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        download_warehouse_stock_details_api = DOWNLOAD_WAREHOUSE_STOCK_DETAILS.new(deciding_params)
        response = download_warehouse_stock_details_api.enact(id: params[:id])
        send_response(response)
      end

      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :download_warehouse_stock_details do
        summary 'It downloads stock details of WBPs of a warehouse'
        notes 'API returns the stock details of WBPs of a warehouse'
        param :path, :id, :integer, :required, 'warehouse ID'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'Warehouse does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to download warheouse procurement prediction details
      #
      # GET /warehouses/procurement_prediction/download/:id
      #
      # Response::
      #   * Function to download Warehouse's WBPs procurement details
      #
      def download_warehouse_procurement_prediction
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        download_warehouse_procurement_prediction_api = DOWNLOAD_WAREHOUSE_PROCUREMENT_PREDICTION.new(deciding_params)
        response = download_warehouse_procurement_prediction_api.enact(id: params[:id])
        send_response(response)
      end

      swagger_controller :warehouses, 'SupplyChainModule APIs'
      swagger_api :download_warehouse_procurement_prediction do
        summary 'It downloads stock procurement details of WBPs of a warehouse'
        notes 'API returns the stock procurement details of WBPs of a warehouse'
        param :path, :id, :integer, :required, 'warehouse ID'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'Warehouse does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private 

      def warehouse_params
        params.require(:warehouse).permit(:name, :cash_and_carry_id)
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:warehouse).permit(:event).merge({ id: params[:id], action: params[:action] })
      end

      #
      # White list params for get_all_cash_and_carry API
      #
      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end

      def get_warehouse_stock_details_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order, :department_id, :category_id, :sub_category_id, :company_id, :brand_id, :sub_brand_id)
      end

      def warehouse_end_of_day_params
        params.require(:warehouse).permit(:id)
      end
    end
  end
end
