#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Cash and carry controller service class to route panel API calls to the Cash & carry API
    #
    class CashAndCarryController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_CASH_AND_CARRY = SupplyChainModule::V1::AddCashAndCarryApi
      GET_ALL_CASH_AND_CARRY = SupplyChainModule::V1::GetAllCashAndCarryApi
      CHANGE_STATE = SupplyChainModule::V1::ChangeCashAndCarryStateApi
      GET_CASH_AND_CARRY = SupplyChainModule::V1::GetCashAndCarryApi
      UPDATE_CASH_AND_CARRY = SupplyChainModule::V1::UpdateCashAndCarryApi
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      def initialize
      end

      #
      # function to add cash & carry
      #
      # post /cash_and_carry/new
      #
      # Request:: request object contain
      #   * cash_and_carry_params [hash] it contains name of C&C
      #
      # Response::
      #   * If action succeed then returns Success Status code with create c&c
      #    else returns BadRequest response
      #
      def add_cash_and_carry
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_cash_and_carry_api = ADD_CASH_AND_CARRY.new(deciding_params)
        response = add_cash_and_carry_api.enact(cash_and_carry_params)
        send_response(response)
      end

      swagger_controller :cash_and_carry, 'SupplyChainModule APIs'
      swagger_api :add_cash_and_carry do
        summary 'It creates the cash_and_carry with given input'
        notes 'create cash_and_carry'
        param :body, :add_cash_and_carry_request, :add_cash_and_carry_request, :required, 'add_cash_and_carry request'
        response :bad_request, 'if request params are incorrect or api fails to create cash_and_carry'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :add_cash_and_carry_request do
        description 'request schema for /add_cash_and_carry_request API'
        property :cash_and_carry, :add_cash_and_carry, :required, 'cash_and_carry model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :add_cash_and_carry do
        description 'details of cash and carry'
        property :name, :string, :required, 'name of cash_and_carry'
      end

      #
      # function to change state of cash & carry
      #
      # put /cash_and_carry/:id/change_state
      #
      # Request:: request object contain
      #   * id [integer] id of c&c whose state is to be changed
      #
      # Response::
      #   * If action succeed then returns Success Status code with update c&c
      #    else returns BadRequest response
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_state_api = CHANGE_STATE.new(deciding_params)
        response = change_state_api.enact(change_state_params)
        send_response(response)
      end

      swagger_controller :cash_and_carry, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the state of cash_and_carry'
        notes 'change_state API takes event & id as input and triggers the event to change the state of the c&c'
        param :path, :id, :integer, :required, 'c&c to update'
        param :body, :change_state, :change_state, :required, 'change_state request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of c&c'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :change_state do
        description 'request schema for /change_state API'
        property :cash_and_carry, :change_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_state_hash do
        description 'event to be triggered'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to get all cash and carry
      #
      # GET /cash_and_carry
      #
      # Response::
      #   * list of companies
      #
      def get_all_cash_and_carry
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_all_cash_and_carry = GET_ALL_CASH_AND_CARRY.new(deciding_params)
        response = get_all_cash_and_carry.enact(pagination_params)
        send_response(response)
      end

      swagger_controller :cash_and_carry, 'SupplyChainModule APIs'
      swagger_api :get_all_cash_and_carry do
        summary 'It returns details of all the cash_and_carry'
        notes 'API returns the details of all the cash_and_carry'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all c&c'
        param :query, :per_page, :integer, :optional, 'how many c&c to display in a page'
        param :query, :state, :integer, :optional, 'to fetch c&c based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'name' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a cash and carry
      #
      # GET /cash_and_carry/:id
      #
      # Response::
      #   * Details of a cash_and_carry including it's warehouse
      #
      def get_cash_and_carry
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_cash_and_carry_api = GET_CASH_AND_CARRY.new(deciding_params)
        response = get_cash_and_carry_api.enact(params[:id])
        send_response(response)
      end

      swagger_controller :cash_and_carry, 'SupplyChainModule APIs'
      swagger_api :get_cash_and_carry do
        summary 'It returns details of a cash_and_carry'
        notes 'API returns the details of a cash_and_carry'
        param :path, :id, :integer, :required, 'cash_and_carry to get'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to delete a cash and carry
      #
      # DELETE /cash_and_carry/:id
      #
      # Response::
      #   * Success if cash and carry found & deleted
      #
      def delete_cash_and_carry
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_state_api = CHANGE_STATE.new(deciding_params)
        response = change_state_api.enact({ id: params[:id], event: COMMON_EVENTS::SOFT_DELETE })
        send_response(response)
      end

      swagger_controller :cash_and_carry, 'SupplyChainModule APIs'
      swagger_api :delete_cash_and_carry do
        summary 'It soft deletes a cash_and_carry'
        notes 'delete_cash_and_carry API calls ChangeCashAndCarryStateApi to make its state deleted'
        param :path, :id, :integer, :required, 'cash_and_carry to delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # put /cash_and_carry/:id
      #
      # Request:: request object contain
      #   * C&C params [hash] it contains details of cash and carry
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated cash and carry
      #    else returns BadRequest response
      #
      def update_cash_and_carry
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_api = UPDATE_CASH_AND_CARRY.new(deciding_params)
        response = update_api.enact(cash_and_carry_params.merge(id: params[:id]))
        send_response(response)
      end
      
      swagger_controller :cash_and_carry, 'SupplyChainModule APIs'
      swagger_api :update_cash_and_carry do
        summary 'It updates the cash_and_carry with given input'
        notes 'update cash_and_carry'
        param :path, :id, :integer, :required, 'cash_and_carry to update'
        param :body, :add_cash_and_carry_request, :add_cash_and_carry_request, :required,
              'update_cash_and_carry request'
        response :bad_request, 'if request params are incorrect or api fails to update cash_and_carry'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private 

      def cash_and_carry_params
        params.require(:cash_and_carry).permit(:name)
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:cash_and_carry).permit(:event).merge({ id: params[:id], action: params[:action] })
      end

      #
      # White list params for get_all_cash_and_carry API
      #
      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end
