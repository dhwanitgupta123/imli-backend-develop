#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Inventory controller service class to route panel API calls to the InventoryApi
    #
    class InventoriesController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_INVENTORY = SupplyChainModule::V1::AddInventoryApi
      UPDATE_INVENTORY = SupplyChainModule::V1::UpdateInventoryApi
      GET_INVENTORIES = SupplyChainModule::V1::GetInventoriesApi
      GET_INVENTORY_API = SupplyChainModule::V1::GetInventoryApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      CHANGE_INVENTORY_STATE_API = SupplyChainModule::V1::ChangeInventoryStateApi
      UPLOAD_INVENTORY_PRODUCTS_API = SupplyChainModule::V1::UploadInventoryProductsApi

      def initialize
      end

      #
      # function to add inventory
      #
      # post /add_inventory
      #
      # Request:: request object contain
      #   * inventory_params [hash] it contains name, distributor_id
      #
      # Response::
      #   * If action succeed then returns Success Status code with create inventory
      #    else returns BadRequest response
      #
      def add_inventory
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_inventory_api = ADD_INVENTORY.new(deciding_params)
        response = add_inventory_api.enact(inventory_params)
        send_response(response)
      end

      swagger_controller :inventory, 'SupplyChainModule APIs'
      swagger_api :add_inventory do
        summary 'It creates the inventory with given input'
        notes 'create inventory'
        param :body, :inventory_request, :inventory_request, :required, 'create_inventory request'
        response :bad_request, 'if request params are incorrect or api fails to create inventory'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :inventory_request do
        description 'request schema for /create_category API'
        property :inventory, :inventory_hash, :required, 'inventory model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :inventory_hash do
        description 'details of inventory'
        property :name, :string, :required, 'name of inventory'
        property :distributor_id, :integer, :required, 'distributor id'
      end

      #
      # function to update inventory
      #
      # post /update_inventory
      #
      # Request:: request object contain
      #   * inventory_params [hash] it contains name, distributor_id
      #
      # Response::
      #   * If action succeed then returns Success Status code with update inventory
      #    else returns BadRequest response
      #
      def update_inventory
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_inventory_api = UPDATE_INVENTORY.new(deciding_params)
        response = update_inventory_api.enact(inventory_params.merge({ id: get_id_from_params }))
        send_response(response)
      end

      swagger_controller :inventory, 'SupplyChainModule APIs'
      swagger_api :update_inventory do
        summary 'It updates the inventory with given input'
        notes 'update inventory'
        param :path, :id, :integer, :required, 'inventory to update'
        param :body, :update_inventory_request, :update_inventory_request, :required, 'create_inventory request'
        response :bad_request, 'if request params are incorrect or api fails to create inventory'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_inventory_request do
        description 'request schema for /create_category API'
        property :inventory, :inventory_hash, :required, 'inventory model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      #
      # function to get all inventories
      #
      # GET /get_all_inventories
      #
      # Response::
      #   * list of inventories
      #
      def get_all_inventories
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_inventories_api = GET_INVENTORIES.new(deciding_params)
        response = get_inventories_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :inventory, 'SupplyChainModule APIs'
      swagger_api :get_all_inventories do
        summary 'It returns details of all the inventories'
        notes 'get_all_inventories API returns the details of all the inventories'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all inventory'
        param :query, :per_page, :integer, :optional, 'how many inventory to display in a page'
        param :query, :state, :integer, :optional, 'to fetch inventories based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end
      
      #
      # function to change the status of inventory
      #
      # PUT /inventory/state/:id
      #
      # Request::
      #   * inventory_id [integer] id of inventory of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_inventory_state_api = CHANGE_INVENTORY_STATE_API.new(deciding_params)
        response = change_inventory_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :inventory, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of inventory'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the inventory & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'inventory_id to be deactivated/activated'
        param :body, :change_inventory_state_request, :change_inventory_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or inventory not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_inventory_state_request do
        description 'request schema for /change_state API'
        property :inventory, :change_inventory_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_inventory_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      def delete_inventory
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_inventory_state_api = CHANGE_INVENTORY_STATE_API.new(deciding_params)
        response = change_inventory_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :inventory, 'SupplyChainModule APIs'
      swagger_api :delete_inventory do
        summary 'It delete the inventory'
        notes 'delete_inventory API change the status of inventory to soft delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :path, :id, :integer, :required, 'inventory_id to be deleted'
        response :bad_request, 'wrong parameters or inventory not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a inventory
      #
      # GET /inventories/:id
      #
      # Request::
      #   * inventory_id [integer] id of inventory of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_inventory
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_inventory_api = GET_INVENTORY_API.new(deciding_params)
        response = get_inventory_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :inventory, 'SupplyChainModule APIs'
      swagger_api :get_inventory do
        summary 'It return a inventory'
        notes 'get_inventory API return a single inventory corresponding to the id'
        param :path, :id, :integer, :required, 'inventory_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or inventory not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to upload inventory sheet & create/update IBPs
      #
      # POST /inventories/products
      #
      # Request::
      #   * file [blob] file to upload and sued to create products
      #   * headers present: bp_code, vendor_id, vendor name, company name, margin, vat, buying price
      #     exclusinve of tax
      #
      # Response::
      #   * sends ok response to the panel
      #
      def upload_products
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        upload_inventory_products_api = UPLOAD_INVENTORY_PRODUCTS_API.new(@params)
        response = upload_inventory_products_api.enact({
          file_path: params[:file].path
          })
        send_response(response)
      end

      private 

      def inventory_params
        params.require(:inventory).permit(:id, :name, :distributor_id)
      end

      def change_state_params
        params.require(:inventory).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end
