#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # seller controller service class to route panel API calls to the SellerApi
    #
    class SellerController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_SELLER = SupplyChainModule::V1::AddSellerApi
      GET_SELLERS = SupplyChainModule::V1::GetSellersApi
      GET_SELLER = SupplyChainModule::V1::GetSellerApi
      CHANGE_STATE = SupplyChainModule::V1::ChangeSellerStateApi
      UPDATE_SELLER = SupplyChainModule::V1::UpdateSellerApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      def initialize
      end

      #
      # function to add seller
      #
      # post /seller/new
      #
      # Request:: request object contain
      #   * seller_params [hash] it contains name of Seller
      #
      # Response::
      #   * If action succeed then returns Success Status code with create seller
      #    else returns BadRequest response
      #
      def add_seller
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_seller_api = ADD_SELLER.new(deciding_params)
        response = add_seller_api.enact(seller_params)
        send_response(response)
      end

      swagger_controller :seller, 'SupplyChainModule APIs'
      swagger_api :add_seller do
        summary 'It creates the seller with given input'
        notes 'create seller'
        param :body, :add_seller_request, :add_seller_request, :required, 'add_seller request'
        response :bad_request, 'if request params are incorrect or api fails to create seller'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :add_seller_request do
        description 'request schema for /add_seller_request API'
        property :seller, :add_seller, :required, 'seller model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :add_seller do
        description 'details of seller'
        property :name, :string, :required, 'name of seller'
      end

      #
      # function to change state of seller
      #
      # post /seller/:id/change_state
      #
      # Request:: request object contain
      #   * id [integer] id of seller whose state is to be changed
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated seller
      #    else returns BadRequest response
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_state_api = CHANGE_STATE.new(deciding_params)
        response = change_state_api.enact(change_state_params)
        send_response(response)
      end

      swagger_controller :seller, 'SupplyChainModule APIs'
      swagger_api :change_state do
        summary 'It changes the state of seller'
        notes 'change_state API takes event & id as input and triggers the event to change the state of the seller'
        param :path, :id, :integer, :required, 'seller to update'
        param :body, :change_state, :change_state, :required, 'change_state request'
        response :bad_request, 'if request params are incorrect or api fails to change status'
        response :precondition_required, 'event cant be triggered based on the current status of seller'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :change_state do
        description 'request schema for /change_state API'
        property :seller, :change_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_state_hash do
        description 'event to be triggered'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to get all sellers
      #
      # GET /seller
      #
      # Response::
      #   * list of sellers
      #
      def get_all_sellers
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_sellers_api = GET_SELLERS.new(deciding_params)
        response = get_sellers_api.enact(pagination_params)
        send_response(response)
      end

      swagger_controller :seller, 'SupplyChainModule APIs'
      swagger_api :get_all_sellers do
        summary 'It returns details of all the sellers'
        notes 'API returns the details of all the sellers'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all seller'
        param :query, :per_page, :integer, :optional, 'how many seller to display in a page'
        param :query, :state, :integer, :optional, 'to fetch seller based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'name' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a seller
      #
      # GET /seller/:id
      #
      # Response::
      #   * Details of a seller including it's SBPs
      #
      def get_seller
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_seller_api = GET_SELLER.new(deciding_params)
        response = get_seller_api.enact(params[:id])
        send_response(response)
      end

      swagger_controller :seller, 'SupplyChainModule APIs'
      swagger_api :get_seller do
        summary 'It returns details of a seller'
        notes 'API returns the details of a seller'
        param :path, :id, :integer, :required, 'seller to get'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to delete a seller
      #
      # DELETE /seller/:id
      #
      # Response::
      #   * Success if seller found & deleted
      #
      def delete_seller
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_state_api = CHANGE_STATE.new(deciding_params)
        response = change_state_api.enact({ id: params[:id], event: COMMON_EVENTS::SOFT_DELETE })
        send_response(response)
      end

      swagger_controller :seller, 'SupplyChainModule APIs'
      swagger_api :delete_seller do
        summary 'It soft deletes a seller'
        notes 'delete_seller API calls ChangeSellerStateApi to make its state deleted'
        param :path, :id, :integer, :required, 'seller to delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'User does not exists'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # put /seller/:id
      #
      # Request:: request object contain
      #   * warehouse_params [hash] it contains details of seller
      #
      # Response::
      #   * If action succeed then returns Success Status code with updated seller
      #    else returns BadRequest response
      #
      def update_seller
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_api = UPDATE_SELLER.new(deciding_params)
        response = update_api.enact(seller_params.merge(id: params[:id]))
        send_response(response)
      end
      
      swagger_controller :seller, 'SupplyChainModule APIs'
      swagger_api :update_seller do
        summary 'It updates the seller with given input'
        notes 'update seller'
        param :path, :id, :integer, :required, 'seller to update'
        param :body, :add_seller_request, :add_seller_request, :required, 'update_seller request'
        response :bad_request, 'if request params are incorrect or api fails to update seller'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private 

      def seller_params
        params.require(:seller).permit(:name)
      end

      #
      # White list params for change state API
      #
      def change_state_params
        params.require(:seller).permit(:event).merge({ id: params[:id], action: params[:action] })
      end

      #
      # White list params for get_all_seller API
      #
      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end

    end
  end
end