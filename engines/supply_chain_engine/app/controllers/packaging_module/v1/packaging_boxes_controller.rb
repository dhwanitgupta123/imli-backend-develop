#
# Module to handle all the functionalities related to Packaging
#
module PackagingModule
  #
  # Version1 for Packaging module
  #
  module V1
    #
    # PackagingBox controller service class to route panel API calls to the PackagingBoxApi
    #
    class PackagingBoxesController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_PACKAGING_BOX = PackagingModule::V1::AddPackagingBoxApi
      UPDATE_PACKAGING_BOX = PackagingModule::V1::UpdatePackagingBoxApi
      GET_ALL_PACKAGING_BOX = PackagingModule::V1::GetAllPackagingBoxApi
      GET_PACKAGING_BOX_API = PackagingModule::V1::GetPackagingBoxApi
      CHANGE_PACKAGING_BOX_STATE_API = PackagingModule::V1::ChangePackagingBoxStateApi
      #
      # Initialize helper classes
      #
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents

      #
      # before actions to be executed
      #
      before_action do
        @deciding_params = APPLICATION_HELPER.get_deciding_params(params)
      end
      #
      # initialize PackagingBoxsController Class
      #
      def initialize
      end

      #
      # function to add packaging_box
      #
      # post /add_packaging_box
      #
      # Request:: request object contain
      #   * packaging_box_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with create packaging_box
      #    else returns BadRequest response
      #
      def add_packaging_box
        add_packaging_box_api = ADD_PACKAGING_BOX.new(@deciding_params)
        response = add_packaging_box_api.enact(packaging_box_params)
        send_response(response)
      end

      swagger_controller :packaging_box, 'PackagingModule APIs'
      swagger_api :add_packaging_box do
        summary 'It creates the packaging_box with given input'
        notes 'create packaging_box'
        param :body, :packaging_box_request, :packaging_box_request, :required, 'create_packaging_box request'
        response :bad_request, 'if request params are incorrect or api fails to create packaging_box'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :packaging_box_request do
        description 'request schema for /create_category API'
        property :packaging_box, :packaging_box_hash, :required, 'packaging_box model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :packaging_box_hash do
        description 'details of packaging_box'
        property :label, :string, :required, 'name of packaging_box'
        property :length, :string, :required, 'length of packaging_box'
        property :width, :string, :required, 'width of packaging_box'
        property :height, :string, :required, 'height of packaging_box'
        property :cost, :string, :required, 'cost of packaging_box'
      end

      #
      # function to update packaging_box
      #
      # post /update_packaging_box
      #
      # Request:: request object contain
      #   * packaging_box_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with update packaging_box
      #    else returns BadRequest response
      #
      def update_packaging_box
        update_packaging_box_api = UPDATE_PACKAGING_BOX.new(@deciding_params)
        response = update_packaging_box_api.enact(packaging_box_params.merge({ id: get_id_from_params }))
        send_response(response)
      end

      swagger_controller :packaging_box, 'PackagingModule APIs'
      swagger_api :update_packaging_box do
        summary 'It updates the packaging_box with given input'
        notes 'update packaging_box'
        param :path, :id, :integer, :required, 'packaging_box to update'
        param :body, :update_packaging_box_request, :update_packaging_box_request, :required, 'create_packaging_box request'
        response :bad_request, 'if request params are incorrect or api fails to create packaging_box'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_packaging_box_request do
        description 'request schema for /create_category API'
        property :packaging_box, :packaging_box_hash, :required, 'packaging_box model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      #
      # function to get all packaging_boxes
      #
      # GET /get_all_packaging_boxes
      #
      # Response::
      #   * list of packaging_boxes
      #
      def get_all_packaging_boxes
        get_packaging_boxes_api = GET_ALL_PACKAGING_BOX.new(@deciding_params)
        response = get_packaging_boxes_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :packaging_box, 'PackagingModule APIs'
      swagger_api :get_all_packaging_boxes do
        summary 'It returns details of all the packaging_boxes'
        notes 'get_all_packaging_boxes API returns the details of all the packaging_boxes'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all packaging_box'
        param :query, :per_page, :integer, :optional, 'how many packaging_box to display in a page'
        param :query, :state, :integer, :optional, 'to fetch packaging_boxes based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change the status of packaging_box
      #
      # PUT /packaging_box/state/:id
      #
      # Request::
      #   * packaging_box_id [integer] id of packaging_box of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        change_packaging_box_state_api = CHANGE_PACKAGING_BOX_STATE_API.new(@deciding_params)
        response = change_packaging_box_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :packaging_box, 'PackagingModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of packaging_box'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
          current status of the packaging_box & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'packaging_box_id to be deactivated/activated'
        param :body, :change_packaging_box_state_request, :change_packaging_box_state_request, :required, 'change_state request'
        response :bad_request, 'wrong parameters or packaging_box not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_packaging_box_state_request do
        description 'request schema for /change_state API'
        property :packaging_box, :change_packaging_box_state_hash, :required, 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_packaging_box_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      #
      # function to delete brand
      #
      # DELETE /packaging_box/state/:id
      #
      # Request::
      #   * packaging_box_id [integer] id of packaging_box which is to be deleted
      #
      # Response::
      #   * sends ok response to the panel
      #
      def delete_packaging_box
        change_packaging_box_state_api = CHANGE_PACKAGING_BOX_STATE_API.new(@deciding_params)
        response = change_packaging_box_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :packaging_box, 'PackagingModule APIs'
      swagger_api :delete_packaging_box do
        summary 'It delete the packaging_box'
        notes 'delete_packaging_box API change the status of packaging_box to soft delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :path, :id, :integer, :required, 'packaging_box_id to be deleted'
        response :bad_request, 'wrong parameters or packaging_box not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a packaging_box
      #
      # GET /packaging_boxes/:id
      #
      # Request::
      #   * packaging_box_id [integer] id of packaging_box of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_packaging_box
        get_packaging_box_api = GET_PACKAGING_BOX_API.new(@deciding_params)
        response = get_packaging_box_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :packaging_box, 'PackagingModule APIs'
      swagger_api :get_packaging_box do
        summary 'It return a packaging_box'
        notes 'get_packaging_box API return a single packaging_box corresponding to the id'
        param :path, :id, :integer, :required, 'packaging_box_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or packaging_box not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      ###############################
      #       Private Functions     #
      ###############################

      private 

      def packaging_box_params
        params.require(:packaging_box).permit(:id, :label, :length, :width, :height, :cost)
      end

      def change_state_params
        params.require(:packaging_box).permit(:event).merge({ id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end
    end
  end
end