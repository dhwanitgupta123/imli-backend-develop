#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # MarketplaceSellingPack controller service class to route panel API calls to the MarketplaceSellingPackApi
    #
    class MarketplaceSellingPacksController < BaseModule::V1::ApplicationController
      #
      # initialize API classes
      #
      ADD_MARKETPLACE_SELLING_PACK = MarketplaceProductModule::V1::AddMarketplaceSellingPackApi
      UPDATE_MARKETPLACE_SELLING_PACK = MarketplaceProductModule::V1::UpdateMarketplaceSellingPackApi
      GET_ALL_MARKETPLACE_SELLING_PACK = MarketplaceProductModule::V1::GetAllMarketplaceSellingPackApi
      GET_MARKETPLACE_SELLING_PACK_API = MarketplaceProductModule::V1::GetMarketplaceSellingPackApi
      GET_MPSP_BY_DEPARTMENT_AND_CATEGORY_API = MarketplaceProductModule::V1::GetMpspByDepartmentAndCategoryApi
      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      CHANGE_MARKETPLACE_SELLING_PACK_STATE_API = MarketplaceProductModule::V1::ChangeMarketplaceSellingPackStateApi

      def initialize
      end

      #
      # function to add marketplace_selling_pack
      #
      # post /add_marketplace_selling_pack
      #
      # Request:: request object contain
      #   * marketplace_selling_pack_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with create marketplace_selling_pack
      #    else returns BadRequest response
      #
      def add_marketplace_selling_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        add_marketplace_selling_pack_api = ADD_MARKETPLACE_SELLING_PACK.new(deciding_params)
        response = add_marketplace_selling_pack_api.enact(marketplace_selling_pack_params)
        send_response(response)
      end

      swagger_controller :marketplace_selling_pack, 'MarketplaceProductModule APIs'
      swagger_api :add_marketplace_selling_pack do
        summary 'It creates the marketplace_selling_pack with given input'
        notes 'create marketplace_selling_pack'
        param :body, :marketplace_selling_pack_request, :marketplace_selling_pack_request, :required,
              'create_marketplace_selling_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_selling_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :marketplace_selling_pack_request do
        description 'request schema for /create_category API'
        property :marketplace_selling_pack, :marketplace_selling_pack_hash, :required,
                 'marketplace_selling_pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :marketplace_selling_pack_hash do
        description 'details of marketplace_selling_pack'
        property :marketplace_brand_packs, :array, :required,
                 'array of marketplace_selling_pack & respective quantity',
                 { 'items': { 'type': :marketplace_brand_pack_with_quantity } }
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }
        property :display_pack_size, :string, :required, 'app display pack size'
        property :display_name, :string, :required, 'app display name'
        property :mpsp_sub_category_id, :string, :required, 'mpsp sub category_id'
        property :primary_tags, :string, :required, 'primary tags to identify the selling branc pack'
        property :secondary_tags, :string, :required, 'scondary tags to identify the selling price'
        property :is_on_sale, :boolean, :required, 'If a product is on sale'
        property :is_ladder_pricing_active, :boolean, :required, 'If product has ladder pricing active'
        property :max_quantity, :integer, :required, 'max quantity of MPSP that can be bought'
        property :pricing, :marketplace_selling_pack_pricing, :required,
                 'it has details of marketplace selling pack'
      end

      swagger_model :marketplace_brand_pack_with_quantity do
        description 'details of MPBP with quantity to make MPSP'
        property :marketplace_brand_pack_id, :integer, :required, 'reference to marketplace_brand_pack'
        property :quantity, :integer, :required,
                 'quantity of marketplace_brand_pack to make marketplace_selling_pack'
      end

      swagger_model :marketplace_selling_pack_pricing do
        description 'details of marketplace_brand_pack pricings'
        property :mrp, :double, :required, 'MRP of MPSP'
        property :base_selling_price, :double, :required, 'Seling price of 1 MPSP'
        property :savings, :double, :required, 'Savings on 1 MPSP'
        property :service_tax, :double, :required, 'service tax to be applied'
        property :vat, :double, :required, 'vat tax to be applied'
        property :discount, :double, :optional, 'discount offered'
        property :taxes, :double, :required, 'additional taxes'
        property :ladder, :array, :required, 'array of quantity wise ladder pricing',
                 { 'items': { 'type': :ladder } }
      end

      swagger_model :ladder do
        description 'details of MPSP ladder pricing based on quantity'
        property :quantity, :integer, :required, 'quantity of MPSP'
        property :selling_price, :double, :required, 'selling pricde as per quantity'
        property :additional_savings, :double, :required, 'savings based on quantity'
      end

      #
      # function to update marketplace_selling_pack
      #
      # post /update_marketplace_selling_pack
      #
      # Request:: request object contain
      #   * marketplace_selling_pack_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with update marketplace_selling_pack
      #    else returns BadRequest response
      #
      def update_marketplace_selling_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        update_marketplace_selling_pack_api = UPDATE_MARKETPLACE_SELLING_PACK.new(deciding_params)
        response = update_marketplace_selling_pack_api.enact(
          marketplace_selling_pack_params.merge({ id: get_id_from_params }))
        send_response(response)
      end

      swagger_controller :marketplace_selling_pack, 'MarketplaceProductModule APIs'
      swagger_api :update_marketplace_selling_pack do
        summary 'It updates the marketplace_selling_pack with given input'
        notes 'update marketplace_selling_pack'
        param :path, :id, :integer, :required, 'marketplace_selling_pack to update'
        param :body, :update_marketplace_selling_pack_request, :update_marketplace_selling_pack_request, :required,
              'create_marketplace_selling_pack request'
        response :bad_request, 'if request params are incorrect or api fails to create marketplace_selling_pack'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_marketplace_selling_pack_request do
        description 'request schema for /create_category API'
        property :marketplace_selling_pack, :marketplace_selling_pack_update_hash, :required,
                 'marketplace_selling_pack model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :marketplace_selling_pack_update_hash do
        description 'details of marketplace_selling_pack'
        property :display_pack_size, :string, :optional, 'app display pack size'
        property :mpsp_sub_category_id, :string, :required, 'mpsp sub category_id'
        property :display_name, :string, :optional, 'app display name'
        property :images, :array, :required,
                 'array of image_id & respective priority',
                 { 'items': { 'type': :image_hash_array } }
        property :description, :string, :optional, 'description of product'
        property :primary_tags, :string, :optional, 'primary tags to identify the selling branc pack'
        property :secondary_tags, :string, :optional, 'scondary tags to identify the selling price'
        property :max_quantity, :integer, :optional, 'max quantity of MPSP that can be bought'
        property :is_on_sale, :boolean, :required, 'If a product is on sale'
        property :is_ladder_pricing_active, :boolean, :required, 'If product has ladder pricing active'
        property :max_quantity, :integer, :required, 'max quantity of MPSP that can be bought'
        property :dirty_bit, :boolean, :optional, 'if it used for testing or not'
        property :pricing, :marketplace_selling_pack_pricing, :required,
                 'it has details of marketplace selling pack'
      end

      swagger_model :image_hash_array do
        description 'details of image id and corresponding priority'
        property :image_id, :integer, :required, 'reference to image'
        property :priority, :integer, :required, 'priority of image'
      end

      #
      # function to get all marketplace_selling_packs
      #
      # GET /get_all_marketplace_selling_packs
      #
      # Response::
      #   * list of marketplace_selling_packs
      #
      def get_all_marketplace_selling_packs
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_marketplace_selling_packs_api = GET_ALL_MARKETPLACE_SELLING_PACK.new(deciding_params)
        response = get_marketplace_selling_packs_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :marketplace_selling_pack, 'MarketplaceProductModule APIs'
      swagger_api :get_all_marketplace_selling_packs do
        summary 'It returns details of all the marketplace_selling_packs'
        notes 'get_all_marketplace_selling_packs API returns the details of all the marketplace_selling_packs'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all marketplace_selling_pack'
        param :query, :per_page, :integer, :optional, 'how many marketplace_selling_pack to display in a page'
        param :query, :state, :integer, :optional, 'to fetch marketplace_selling_packs based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data',
              { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to change the status of marketplace_selling_pack
      #
      # PUT /marketplace_selling_pack/state/:id
      #
      # Request::
      #   * marketplace_selling_pack_id [integer] id of marketplace_selling_pack of which status change is requested
      #
      # Response::
      #   * sends ok response to the panel
      #
      def change_state
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_marketplace_selling_pack_state_api = CHANGE_MARKETPLACE_SELLING_PACK_STATE_API.new(deciding_params)
        response = change_marketplace_selling_pack_state_api.enact(change_state_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :marketplace_selling_pack, 'MarketplaceProductModule APIs'
      swagger_api :change_state do
        summary 'It changes the status of marketplace_selling_pack'
        notes 'change_state API reads which event to trigger, verifies if event could be triggered based on
               current status of the marketplace_selling_pack & changes the status if everyhting is correct'
        param :path, :id, :integer, :required, 'marketplace_selling_pack_id to be deactivated/activated'
        param :body, :change_marketplace_selling_pack_state_request, :change_marketplace_selling_pack_state_request,
              :required, 'change_state request'
        response :bad_request, 'wrong parameters or marketplace_selling_pack not found or it fails to change state'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
        response :precondition_required, 'event cant be triggered based on the current status'
      end

      #
      # swagger_model to generate model schema for the request
      # swagger_model: user_request
      # description: description of swagger model
      # property: name, type, required, description, hash{}
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      #     * In hash{} we can use $ref to ref to another model
      #       to create complex swagger_model
      swagger_model :change_marketplace_selling_pack_state_request do
        description 'request schema for /change_state API'
        property :marketplace_selling_pack, :change_marketplace_selling_pack_state_hash, :required,
                 'hash of event to trigger state change'
        property :session_token, :string, :required, 'session token to authenticate user'
      end

      swagger_model :change_marketplace_selling_pack_state_hash do
        description 'event to trigger status change'
        property :event, :integer, :required, 'event to trigger status change'
      end

      def delete_marketplace_selling_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        change_marketplace_selling_pack_state_api = CHANGE_MARKETPLACE_SELLING_PACK_STATE_API.new(deciding_params)
        response = change_marketplace_selling_pack_state_api.enact({ 
                      id: get_id_from_params, 
                      event: COMMON_EVENTS::SOFT_DELETE 
                    })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :marketplace_selling_pack, 'MarketplaceProductModule APIs'
      swagger_api :delete_marketplace_selling_pack do
        summary 'It delete the marketplace_selling_pack'
        notes 'delete_marketplace_selling_pack API change the status of marketplace_selling_pack to soft delete'
        param :query, :session_token, :string, :required, 'to authenticate user'
        param :path, :id, :integer, :required, 'marketplace_selling_pack_id to be deleted'
        response :bad_request, 'wrong parameters or marketplace_selling_pack not found or it fails to delete'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a marketplace_selling_pack
      #
      # GET /marketplace_selling_packs/:id
      #
      # Request::
      #   * marketplace_selling_pack_id [integer] id of marketplace_selling_pack of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_marketplace_selling_pack
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_marketplace_selling_pack_api = GET_MARKETPLACE_SELLING_PACK_API.new(deciding_params)
        response = get_marketplace_selling_pack_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :marketplace_selling_pack, 'MarketplaceProductModule APIs'
      swagger_api :get_marketplace_selling_pack do
        summary 'It return a marketplace_selling_pack'
        notes 'get_marketplace_selling_pack API return a single marketplace_selling_pack corresponding to the id'
        param :path, :id, :integer, :required, 'marketplace_selling_pack_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or marketplace_selling_pack not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      def get_mpsp_by_department_and_category
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_mpsp_by_department_and_category_api = GET_MPSP_BY_DEPARTMENT_AND_CATEGORY_API.new(deciding_params)
        response = get_mpsp_by_department_and_category_api.enact(mpsp_by_department_and_category_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :marketplace_selling_pack, 'MarketplaceProductModule APIs'
      swagger_api :get_mpsp_by_department_and_category do
        summary 'return the mpsp after applying filters'
        notes 'return mpsp after applying filters'
        param :query, :mpsp_department_id, :integer, :required, 'event to trigger status change'
        param :query, :mpsp_category_id, :integer, :optional, 'event to trigger status change'
        param :query, :mpsp_sub_category_id, :integer, :optional, 'event to trigger status change'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        response :bad_request, 'wrong parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      private 

      def marketplace_selling_pack_params
        if params[:marketplace_selling_pack].present? && 
            params[:marketplace_selling_pack][:pricing].present? && 
            params[:marketplace_selling_pack][:pricing][:ladder].present? &&
            params[:marketplace_selling_pack][:pricing][:ladder].is_a?(String)
          params[:marketplace_selling_pack][:pricing][:ladder] = JSON.parse(params[:marketplace_selling_pack][:pricing][:ladder])
        end
        if params[:marketplace_selling_pack].present? && params[:marketplace_selling_pack][:marketplace_brand_packs].is_a?(String)
          params[:marketplace_selling_pack][:marketplace_brand_packs] = JSON.parse(params[:marketplace_selling_pack][:marketplace_brand_packs])
        end
        if params[:marketplace_selling_pack].present? && params[:marketplace_selling_pack][:images].present? &&
          params[:marketplace_selling_pack][:images].is_a?(String)
          params[:marketplace_selling_pack][:images] = JSON.parse(params[:marketplace_selling_pack][:images])
        end
        params.require(:marketplace_selling_pack).permit(
          :id,
          :mpsp_sub_category_id,
          :is_on_sale,
          :display_name,
          :display_pack_size,
          :description,
          :primary_tags,
          :secondary_tags,
          :is_ladder_pricing_active,
          :max_quantity,
          :dirty_bit,
          images: [:image_id, :priority],
          marketplace_brand_packs: [:marketplace_brand_pack_id, :quantity],
          pricing: [:service_tax, :vat, :taxes, :discount, :mrp, :base_selling_price, :savings, ladder: [:quantity, :selling_price, :additional_savings]]
        )
      end

      def change_state_params
        params.require(:marketplace_selling_pack).permit(:event).merge(
          { id: get_id_from_params, action: params[:action] })
      end

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :state, :sort_by, :order)
      end

      def mpsp_by_department_and_category_params
        params.permit(:mpsp_department_id, :mpsp_category_id, :mpsp_sub_category_id)
      end
    end
  end
end