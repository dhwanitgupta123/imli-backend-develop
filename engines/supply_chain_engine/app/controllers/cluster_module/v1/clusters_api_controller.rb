#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # ClustersController contains API related to cluster
    #
    class ClustersApiController < BaseModule::V1::ApplicationController

      APPLICATION_HELPER = CommonModule::V1::ApplicationHelper
      GET_CLUSTER_API = ClusterModule::V1::GetClusterApi
      GET_CLUSTERS_API = ClusterModule::V1::GetClustersApi

      def initialize
      end

      #
      # function to get a cluster
      #
      # GET /clusters/:id
      #
      # Request::
      #   * cluster_id [integer] id of cluster of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_cluster
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_cluster_api = GET_CLUSTER_API.new(deciding_params)
        response = get_cluster_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :clusters, 'ClusterModule APIs'
      swagger_api :get_cluster do
        summary 'It return a cluster'
        notes 'get_company API return a single cluster corresponding to the id'
        param :path, :id, :integer, :required, 'cluster_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or cluster not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :clusters, 'ClusterModule APIs'
      swagger_api :delete_cluster do
        summary 'It delete the cluster'
        notes 'delete_cluster API change the status of cluster to soft delete'
        param :path, :id, :integer, :required, 'cluster_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or cluster not found or it fails to delete'
      end

      #
      # function to get all clusters
      #
      # GET /get_all_clusters
      #
      # Response::
      #   * list of clusters
      #
      def get_all_clusters
        deciding_params = APPLICATION_HELPER.get_deciding_params(params)
        get_clusters_api = GET_CLUSTERS_API.new(deciding_params)
        response = get_clusters_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :clusters, 'ClusterModule APIs'
      swagger_api :get_all_clusters do
        summary 'It returns details of all the Clusters'
        notes 'get_all_clusters API returns the details of all the Clusters'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :page_no, :integer, :optional, 'page no from which to get all Clusters'
        param :query, :per_page, :integer, :optional, 'how many Clusters to display in a page'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        param :query, :level_id, :integer, :optional, 'level id of level'
        param :query, :level, :integer, :optional, 'level of cluster'
        param :query, :state, :integer, :optional, 'status of cluster'
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      private

      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        params.permit(:page_no, :per_page, :sort_by, :order, :level_id, :level, :state)
      end
    end
  end
end
