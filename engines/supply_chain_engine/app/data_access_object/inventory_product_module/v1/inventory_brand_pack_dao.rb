#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Data Access Object to interact with InventoryBrandPackModel
    #
    class InventoryBrandPackDao < SupplyChainBaseModule::V1::BaseDao

      INVENTORY_BRAND_PACK_MODEL = InventoryProductModule::V1::InventoryBrandPack
      INVENTORY_PRICING_DAO = InventoryPricingModule::V1::InventoryPricingDao
      INVENTORY_DAO = SupplyChainModule::V1::InventoryDao
      CATEGORY_UTIL = CategorizationModule::V1::CategoryUtil
      PRODUCT_UTIL = MasterProductModule::V1::ProductUtil
      SUB_BRAND_UTIL = MasterProductModule::V1::SubBrandUtil

      def initialize(params={})
        super
      end

      def create(args)
        inventory_pricing_dao = INVENTORY_PRICING_DAO.new
        inventory_model = INVENTORY_BRAND_PACK_MODEL.new(model_params(args, INVENTORY_BRAND_PACK_MODEL))
        inventory_model.inventory_pricing = inventory_pricing_dao.new_inventory_pricing(args[:pricing])
        begin
          inventory_model.save!
          return inventory_model
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Inventory brand pack, ' + e.message)
        end
      end

      def update(args, inventory_brand_pack)
        begin
          inventory_pricing_dao = INVENTORY_PRICING_DAO.new
          inventory_brand_pack.update_attributes!(model_params(args, inventory_brand_pack))
          inventory_brand_pack.inventory_pricing = inventory_pricing_dao.update_inventory_pricing(
            args[:pricing], inventory_brand_pack.inventory_pricing)
          return inventory_brand_pack
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Inventory brand pack, '+ e.message)
        end
      end

      def get_by_id(id)
        get_model_by_id(INVENTORY_BRAND_PACK_MODEL, id)
      end

      def get_all(pagination_params = {})
        get_all_element(INVENTORY_BRAND_PACK_MODEL, pagination_params)
      end

      def get_pricing_for_ibp(inventory_brand_pack)
        return inventory_brand_pack.inventory_pricing
      end

      def get_active_element_by_id(id)
        get_active_model_by_id(INVENTORY_BRAND_PACK_MODEL, id)
      end

      #
      # Function to fetch IBPS based on filters & pagination params
      #
      def get_filtered_inventory_brand_pack_by_inventory_id(args)
        sub_categories = CATEGORY_UTIL.get_sub_categories({
          department_id: args[:department_id],
          category_id: args[:category_id],
          sub_category_id: args[:sub_category_id]
        })
        sub_brands = SUB_BRAND_UTIL.get_sub_brands({
          company_id: args[:company_id],
          brand_id: args[:brand_id],
          sub_brand_id: args[:sub_brand_id]
        })
        inventory = get_inventory_by_id(args[:id])
        if sub_categories.present? || sub_brands.present?
          if sub_categories.present?
            ibps = get_ibp_by_sub_categories(sub_categories, inventory)
          else
            ibps = get_ibp_joined_with_filter_table(inventory)
          end
          if sub_brands.present?
            products = PRODUCT_UTIL.filter_products({ sub_brands: sub_brands })
            ibps = get_ibp_by_products(ibps, products)
          end
        else
          ibps = get_ibp_joined_with_filter_table(inventory)
        end
        paginated_ibps = get_paginated_element(INVENTORY_BRAND_PACK_MODEL, ibps, args)
        return {
          inventory_brand_packs: paginated_ibps[:elements],
          page_count: paginated_ibps[:page_count]
        }
      end

      #
      # Fuction to fetch inventory by it's ID
      #
      def get_inventory_by_id(inventory_id)
        inventory_dao = INVENTORY_DAO.new
        inventory = inventory_dao.get_by_id(inventory_id)
        if inventory.blank?
          raise CUSTOM_ERROR_UTIL::ResourceNotFoundError.new(CONTENT::INVENTORY_NOT_FOUND)
        end
        return inventory
      end

      #
      # Function to get IBPS based on sub categories to which they belong
      # 
      def get_ibp_by_sub_categories(sub_categories, inventory)
        return INVENTORY_BRAND_PACK_MODEL.includes(:inventory).includes(brand_pack: :sub_category).includes(
          brand_pack: :product).where(inventory: inventory, brand_packs: {sub_category_id: sub_categories})
      end

      #
      # Function to get IBP wcich include Products table
      #
      def get_ibp_joined_with_filter_table(inventory)
        return INVENTORY_BRAND_PACK_MODEL.includes(:inventory).includes(brand_pack: :product).where(
          inventory: inventory)
      end

      #
      # Function to fetch IBP based on Filters of Products
      #
      def get_ibp_by_products(ibps, products)
        return ibps.where(brand_packs: { product_id: products })
      end

      #
      # Function to fetch IBP based on Inventory & Brand Pack
      #
      def ibp_of_brand_pack_for_a_vendor(inventory, brand_pack)
        return INVENTORY_BRAND_PACK_MODEL.where(inventory: inventory, brand_pack: brand_pack).first
      end
    end
  end
end
