#
# Module to handle all the functionalities related to seller products
#
module SellerProductModule
  #
  # Version1 for seller product module
  #
  module V1
    #
    # Model for seller buying brand pack table
    #
    class SellerBrandPackDao < SupplyChainBaseModule::V1::BaseDao
    	SELLER_BRAND_PACK_MODEL = SellerProductModule::V1::SellerBrandPack
      PAGINATION_UTIL = CommonModule::V1::Pagination
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates
      SELLER_PRICING_DAO = SellerPricingModule::V1::SellerPricingDao
    	def initialize
        super
      end

      #
      # Create the seller brand pack
      #
      # @param args [name] seller brand pack name
      #
      # @return [SellerBrandPack Model]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        seller_pricing_dao = SELLER_PRICING_DAO.new
        seller_brand_packs = get_sbp_by_brand_pack_seller(args[:sbp])
        if seller_brand_packs.present?
          seller_brand_pack = seller_brand_packs.first
        end
        if seller_brand_pack.blank?
          seller_brand_pack = SELLER_BRAND_PACK_MODEL.new(args[:sbp])
        end
        seller_brand_pack.warehouse_brand_packs << args[:warehouse_brand_pack]
        seller_brand_pack.seller_pricing = seller_pricing_dao.new_seller_pricing(args[:pricing])
        begin
          seller_brand_pack.save!
          return seller_brand_pack
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Seller Brand Pack, ' + e.message)
        end
      end

      #
      # Update the given seller brand pack with name
      #
      # @param name [String] new seller brand pack name
      # @param seller brand pack [Model] SellerBrandPack Model
      #
      # @return [SellerBrandPack Model] updated seller brand pack
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, seller_brand_pack)
        seller_pricing_dao = SELLER_PRICING_DAO.new        
        seller_brand_pack.seller_id = args[:sbp][:seller_id] if args[:sbp][:seller_id].present?
        if args[:warehouse_brand_pack].present? && args[:sbp][:brand_pack_id].present?
          seller_brand_pack.brand_pack_id = args[:sbp][:brand_pack_id]
          seller_brand_pack.warehouse_brand_packs << args[:warehouse_brand_pack]
        end
        seller_brand_pack.seller_pricing = seller_pricing_dao.update_seller_pricing(args[:pricing], seller_brand_pack.seller_pricing)
        begin
          seller_brand_pack.save!
          return seller_brand_pack
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Seller Brand Pack, ' + e.message)
        end
      end

      #
      # Find the seller brand pack by id
      #
      # @param name [Integer] seller_brand_pack_id
      #
      # @return [SellerBrandPack Model] return seller brand pack model else nil
      #
      def get_seller_brand_pack_by_id(seller_brand_pack_id)
        begin
          SELLER_BRAND_PACK_MODEL.find(seller_brand_pack_id)
          rescue ActiveRecord::RecordNotFound => e
            raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::SELLER_BRAND_PACK_NOT_FOUND)
        end
      end

      #
      # Find the sbp by brand pack & seller
      #
      # @param args [hash] contains brand pack and seller
      #
      # @return [SBP model] return sbp model else nil
      #
      def get_sbp_by_brand_pack_seller(args)
        SELLER_BRAND_PACK_MODEL.where({ seller_id: args[:seller_id], brand_pack_id: args[:brand_pack_id] })
      end

      #
      # function to return array of all the seller brand packs paginated
      #
      # @return [array of seller brand packs object]
      def paginated_seller_brand_pack(paginate_param)
        if paginate_param[:state].present?
          return get_seller_brand_pack_by_status(paginate_param)
        else
          return get_all_seller_brand_packs(paginate_param)
        end
      end

      #
      # function to return paginated seller brand packs based on the status
      #
      def get_seller_brand_pack_by_status(paginate_param)
        set_pagination_properties(paginate_param)
        seller_brand_pack = SELLER_BRAND_PACK_MODEL.where({ status: @state }).order(@sort_order)
        return get_paginated_seller_brand_pack(seller_brand_pack)
      end

      #
      # Returns all the seller brand packs
      #
      # @return [Array] array of seller brand pack model
      #
      def get_all_seller_brand_packs(paginate_param)
        valid_paginate_params(paginate_param)
        set_pagination_properties(paginate_param)
        seller_brand_pack = SELLER_BRAND_PACK_MODEL.where.not({ status: COMMON_MODEL_STATES::DELETED }).order(@sort_order)
        return get_paginated_seller_brand_pack(seller_brand_pack)
      end

      # 
      # function to validate the paginated params
      #
      def valid_paginate_params(paginate_params)
        if paginate_params[:order].present? && paginate_params[:order] != 'ASC' && paginate_params[:order] != 'DESC'
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_ORDER)
        end
        if paginate_params[:sort_by].present? && !SELLER_BRAND_PACK_MODEL.attribute_names.include?(paginate_params[:sort_by])
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::VALID_SORT_BY)
        end
      end

      #
      # function to set pagination properties
      #
      def set_pagination_properties(paginate_param)
        @per_page = (paginate_param[:per_page] || PAGINATION_UTIL::PER_PAGE).to_f
        @page_no = (paginate_param[:page_no] || PAGINATION_UTIL::PAGE_NO).to_f
        sort_by = paginate_param[:sort_by] || PAGINATION_UTIL::SORT_BY
        order = paginate_param[:order] || PAGINATION_UTIL::ORDER
        @sort_order = sort_by + ' ' + order
        @state = paginate_param[:state]
      end

      #
      # function to paginate the seller
      #
      def get_paginated_seller_brand_pack(seller_brand_pack)
        page_count = (seller_brand_pack.count / @per_page).ceil
        paginated_seller_brand_pack = seller_brand_pack.limit(@per_page).offset((@page_no - 1) * @per_page)
        return { seller_brand_pack: paginated_seller_brand_pack, page_count: page_count }
      end

      # 
      # function to change the state of the seller brand pack based on the event to trigger
      # @param args [args] [hash] contains the event to trigger the status change
      # 
      # @return [Seller object]
      def change_state(args)
        seller_brand_pack = get_seller_brand_pack_by_id(args[:id])
        case args[:event].to_i
        when COMMON_EVENTS::ACTIVATE
          seller_brand_pack.trigger_event('activate')
        when COMMON_EVENTS::DEACTIVATE
          seller_brand_pack.trigger_event('deactivate')
        when COMMON_EVENTS::SOFT_DELETE
          seller_brand_pack.trigger_event('soft_delete')
        else
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::INVALID_EVENT)
        end
        return seller_brand_pack
      end

    end
  end
end