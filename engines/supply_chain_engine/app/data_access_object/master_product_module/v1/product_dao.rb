#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Data Access Object to interact with Product Model
    #
    class ProductDao < SupplyChainBaseModule::V1::BaseDao
      PRODUCT_MODEL = MasterProductModule::V1::Product
      SUB_BRAND_UTIL = MasterProductModule::V1::SubBrandUtil
      PRODUCT_UTIL = MasterProductModule::V1::ProductUtil

      def initialize
        super
      end

      #
      # Create the product
      #
      # @param args [name] product name
      #
      # @return [productModel]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_product = PRODUCT_MODEL.new(args)
        begin
          new_product.save!
          return new_product
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Product, ' + e.message)
        end
      end

      #
      # Update the given product with name
      #
      # @param name [String] new product name
      # @param product [Model] productModel
      #
      # @return [productModel] updated product
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, product)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PRODUCT_MISSING) if product.nil? || args.nil?
        begin
          product.update_attributes!(args)
          return product
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Product, ' + e.message)
        end
      end

      #
      # Find the product by name
      #
      # @param name [String] product name
      #
      # @return [productModel] return product model else nil
      #
      def get_product_by_id(product_id)
        PRODUCT_MODEL.find_by(id: product_id)
      end

      #
      # Returns all the products
      #
      # @return [Array] array of product model
      #
      def get_all(pagination_params = {})
        get_all_element(PRODUCT_MODEL, pagination_params)
      end

      #
      # validate if product exists or not if exists then call dao to change the state of product
      # else return error
      #
      # @param product_id [String] product id of which status is to be changed
      # @param event [String] event to trigger to change the status
      #
      # @error [InvalidArgumentsError] if product with give product_id doesn't exists or if it fails to update state
      #
      def change_product_state(product_id, event)
        product = PRODUCT_MODEL.find_by(id: product_id)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::PRODUCT_NOT_FOUND) if product.nil?
        change_state(product, event)
        return product
      end

      #
      # Fetch elements after applying filters and with pagination
      # Currently filters are:
      #   - company_id, brand_id, sub_brand_id
      # Finally,
      # -sub_brands are fetched and corresponding brandPacks are filtered over it
      #
      # @param filter_params [JSON] [Hash of filter params]
      # 
      # @return [JSON] [Hash of paginated products {:elements, :page_count}]
      #
      def get_paginated_filtered_products(filter_params)
        sub_brands = SUB_BRAND_UTIL.get_sub_brands({
          company_id: filter_params[:company_id],
          brand_id: filter_params[:brand_id],
          sub_brand_id: filter_params[:sub_brand_id]
          })
        
        if sub_brands.present?
          filtered_products = PRODUCT_UTIL.filter_products({
            sub_brands: sub_brands
          })
          # Call BaseDao function to apply pagination over filtered products
          products = get_paginated_element(PRODUCT_MODEL, filtered_products, filter_params)    
        else
          products = get_all(filter_params)
        end

        return products
      end

      #
      # Returns all the products
      #
      # @return [Array] array of products model
      #
      def get_all(pagination_params = {})
        get_all_element(PRODUCT_MODEL, pagination_params)
      end

      #
      # Return product by product name and sub_brand_id
      # 
      # @return [ProductModel] single product assocaited with the sub_brand_id
      # 
      def get_product_by_name_and_sub_brand(name, sub_brand_id)
        PRODUCT_MODEL.find_by(name: name, sub_brand_id: sub_brand_id)
      end

      #
      # Return product by given sub_brands
      # 
      # @return [PoductModel] products assocaited with the given sub_brands
      # 
      def get_products_by_sub_brands(sub_brands)
        PRODUCT_MODEL.where(sub_brand: sub_brands)
      end

      #
      # delete all the records
      #
      def delete_all
        PRODUCT_MODEL.delete_all
      end
    end
  end
end
