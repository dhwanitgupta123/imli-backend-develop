#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Data Access Object to interact with Variant Model
    #
    class VariantDao < SupplyChainBaseModule::V1::BaseDao
      VARIANT_MODEL = MasterProductModule::V1::Variant
      def initialize
        super
      end

      #
      # Create the Variant
      #
      # @param args [name] Variant name
      #
      # @return [ Variant Model]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_variant = VARIANT_MODEL.new(args)

        begin
          new_variant.save!
          return new_variant
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to create Variant, ' + e.message)
        end
      end

      #
      # Update the given variant
      #
      # @param args [hash] values to be updated
      # 
      # @return [variant Model] updated variant
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, variant)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::VARIANT_NOT_FOUND) if variant.nil? || args.nil?
        begin
          variant.update_attributes!(args)
          return variant
        rescue ActiveRecord::RecordInvalid => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new('Not able to update Variant, ' + e.message)
        end
      end

      #
      # Find the variant by id
      #
      # @param variant_id [integer] variant id 
      #
      # @return [variant model] return variant model else nil
      #
      def get_variant_by_id(variant_id)
        VARIANT_MODEL.find_by(id: variant_id)
      end

      #
      # Returns all the variants
      #
      # @return [Array] array of variant model
      #
      def get_all_variants
        VARIANT_MODEL.all
      end
    end
  end
end
