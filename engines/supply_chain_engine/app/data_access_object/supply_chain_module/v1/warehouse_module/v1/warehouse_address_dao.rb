module SupplyChainModule::V1
  module WarehouseModule::V1
    class WarehouseAddressDao < SupplyChainBaseModule::V1::BaseDao
      WAREHOUSE_ADDRESS = SupplyChainModule::V1::WarehouseModule::V1::WarehouseAddress
      def initialize
        super
      end

      #
      # Function to add address of a warehouse
      #
      def add_address(args)
        args = args[:addresses].first
        create_model(args, WAREHOUSE_ADDRESS, 'Warehouse Address')
      end

      # 
      # Function to update address of a warehouse
      #
      def update_address(args, present_address)
        args = args[:addresses].first
        update_model(args, present_address, 'Warehouse Address')
      end

      #
      # function to fetch warehouse address using it's ID
      #
      def get_address_from_id(address_id)
        begin
          WAREHOUSE_ADDRESS.find(address_id)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::ADDRESS_NOT_FOUND)
        end
      end
    end
  end
end
