module SupplyChainModule::V1
  module InventoryModule::V1
    class InventoryAddressDao < SupplyChainBaseModule::V1::BaseDao
      INVENTORY_ADDRESS = SupplyChainModule::V1::InventoryModule::V1::InventoryAddress
      def initialize
        super
      end

      #
      # Function to add address of a inventory
      #
      def add_address(args)
        args = args[:address]
        create_model(args, INVENTORY_ADDRESS, 'Inventory Address')
      end

      # 
      # Function to update address of a inventory
      #
      def update_address(args, present_address)
        args = args[:address]
        update_model(args, present_address, 'Inventory Address')
      end

      #
      # function to fetch inventory address using it's ID
      #
      def get_address_from_id(address_id)
        begin
          INVENTORY_ADDRESS.find(address_id)
        rescue ActiveRecord::RecordNotFound => e
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::ADDRESS_NOT_FOUND)
        end
      end
    end
  end
end
