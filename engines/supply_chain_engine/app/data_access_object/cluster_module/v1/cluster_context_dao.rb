#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Data Access Object to interact with ClusterContext Model
    #
    class ClusterContextDao < SupplyChainBaseModule::V1::BaseDao

      CLUSTER_CONTEXT_MODEL = ClusterModule::V1::ClusterContext

      def initialize
        super
      end

      # 
      # create cluster context
      #
      # @param args [Hash] model args
      # 
      # @return [Model] ClusterContext
      #
      def create(args)
        create_model(args, CLUSTER_CONTEXT_MODEL, 'CLUSTER CONTEXT')
      end

      # 
      # update cluster context
      #
      # @param args [Hash] model args
      # @param cluster_context [Model] cluster context
      # 
      # @return [Model] ClusterContext
      #
      def update(args, cluster_context)  
        update_model(args, cluster_context, 'CLUSTER CONTEXT')
      end
    end
  end
end
