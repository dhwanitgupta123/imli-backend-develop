#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainCommonModule
  #
  # Version1 for supply chain module
  #
  module V1
    # 
    # Module to keep information of model state
    # 
    class CommonStates
      ACTIVE      = 1
      INACTIVE    = 2
      DELETED     = 3
      ALLOWED_STATES = [1,2,3]
    end
  end
end