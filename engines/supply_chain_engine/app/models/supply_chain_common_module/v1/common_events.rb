#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainCommonModule
  #
  # Version1 for supply chain module
  #
  module V1
    # 
    # Module to keep information of model state
    # 
    class CommonEvents
      ACTIVATE    = 1
      DEACTIVATE  = 2
      SOFT_DELETE = 3
    end
  end
end