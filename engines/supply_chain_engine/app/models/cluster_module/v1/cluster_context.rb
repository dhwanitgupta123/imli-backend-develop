#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Model for cluster_context table
    #
    class ClusterContext < BaseModule::BaseModel

      belongs_to :cluster, dependent: :destroy

      validates :cluster, presence: true
    end
  end
end
