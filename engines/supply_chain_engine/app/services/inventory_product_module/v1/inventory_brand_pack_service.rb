#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class InventoryBrandPackService < SupplyChainBaseModule::V1::BaseService

      INVENTORY_BRAND_PACK_DAO = InventoryProductModule::V1::InventoryBrandPackDao
      BRAND_PACK_DAO = MasterProductModule::V1::BrandPackDao
      INVENTORY_DAO = SupplyChainModule::V1::InventoryDao
      WAREHOUSE_SERVICE = SupplyChainModule::V1::WarehouseService
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      IBP_UTIL = InventoryProductModule::V1::InventoryBrandPackUtil
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      def initialize(params={})
        super
        @inventory_dao = INVENTORY_DAO.new
        @brand_pack_dao = BRAND_PACK_DAO.new
        @inventory_brand_pack_dao = INVENTORY_BRAND_PACK_DAO.new
      end

      # 
      # Transactional creation of IBP
      #
      def transactional_create_inventory_brand_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          #
          # TO-DO
          # 
          # Call 'create_inventory_brand_pack' function when we have multiple warehouse
          #
          return create_ibp_to_mpbp(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Transactional updation of IBP
      #
      def transactional_update_inventory_brand_pack(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return update_inventory_brand_pack(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      def create_inventory_brand_pack(args)
        validate_if_exists(args[:inventory_id], @inventory_dao, 'Inventory')
        validate_if_exists(args[:brand_pack_id], @brand_pack_dao, 'BrandPack')
        create_model(args, @inventory_brand_pack_dao)
      end 

      #
      # Function create IBP and it's corresponding WBP, SBP & MPBP
      #
      def create_ibp_to_mpbp(args)
        unless args[:verified]
          IBP_UTIL.validate_add_ibp_request(args)
        end
        validate_if_exists(args[:inventory_id], @inventory_dao, 'Inventory')
        validate_if_exists(args[:brand_pack_id], @brand_pack_dao, 'BrandPack')
        ibp = create_model(args, @inventory_brand_pack_dao)
        warehouse_service = WAREHOUSE_SERVICE.new(@params)
        warehouses = warehouse_service.get_all_warehouses
        warehouse = warehouses[:warehouse].first
        wbp = get_wbp_for_ibp_and_warehouse(ibp.brand_pack, warehouse)
        if wbp.blank?
          wbp = create_wbp(ibp, warehouse)
        end
        return ibp
      end 

      def update_inventory_brand_pack(args)
        unless args[:verified]
          IBP_UTIL.validate_update_ibp_request(args)
        end
        validate_if_exists(args[:inventory_id], @inventory_dao, 'Inventory') if args[:inventory_id].present?
        validate_if_exists(args[:brand_pack_id], @brand_pack_dao, 'BrandPack') if args[:brand_pack_id].present?
        update_model(args, @inventory_brand_pack_dao, 'Inventory Brand Pack')
      end

      def get_inventory_brand_pack_by_id(id)
        validate_if_exists(id, @inventory_brand_pack_dao, 'Inventory Brand Pack')
      end

      def get_all_inventory_brand_packs(pagination_params = {})
        get_all_elements(@inventory_brand_pack_dao, pagination_params)
      end

      def change_inventory_brand_pack_state(args)
        change_state(args, @inventory_brand_pack_dao, 'Inventory Brand Pack')
      end

      def get_active_inventory_brand_pack_by_id(id)
        get_active_element(id, @inventory_brand_pack_dao, 'Inventory Brand Pack')
      end

      #
      # Function to fetch IBPs of an inventory with pagination & categorization filter
      #
      def get_inventory_brand_pack_by_inventory_id(request)
        ibp_details = @inventory_brand_pack_dao.get_filtered_inventory_brand_pack_by_inventory_id(request)
        return { inventory_brand_packs: ibp_details[:inventory_brand_packs], page_count: ibp_details[:page_count] }
      end

      #
      # Function to fetch WBP corresponding to IBP & warehouse
      #
      def get_wbp_for_ibp_and_warehouse(brand_pack, warehouse)
        wbp_service = WBP_SERVICE.new(@params)
        return wbp_service.get_wbp_by_parameters(brand_pack, warehouse)
      end

      #
      # Function to create WBP if corresponding to IBP & Warehouse
      #
      def create_wbp(inventory_brand_pack, warehouse)
        wbp_service = WBP_SERVICE.new(@params)
        return wbp_service.create({
          warehouse_id: warehouse.id,
          inventory_brand_pack_id: inventory_brand_pack.id,
          brand_pack_id: inventory_brand_pack.brand_pack.id,
          status: COMMON_MODEL_STATES::ACTIVE
          })
      end
    end
  end
end