#
# Module to handle all the functionalities related to dao
#
module SupplyChainBaseModule
  #
  # Version1 for base module
  #
  module V1
  	class BaseService
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CONTENT = CommonModule::V1::Content
      COMMON_EVENTS = SupplyChainCommonModule::V1::CommonEvents
      CACHE_UTIL = CommonModule::V1::Cache
      def initialize(params='')
      end

      #
      # This funtion call the create function of Dao
      #
      # @param args [Hash] contains corresponding argument to create record
      # @param dao [DaoObject] DAO of which create function to call
      #
      # @return [Object] object if it succeed to create else pass the error raised by DAO
      #
      def create_model(args, dao)
        model = dao.create(args)
      end

      #
      # This funtion call the update function of Dao
      #
      # @param args [Hash] contains corresponding argument to update model
      # @param dao [DaoObject] DAO of which update function to call
      # @param message [String] custom message
      #
      # @return [Object] object if it succeed to update else pass the error raised by DAO
      #
      def update_model(args, dao, message='')
        if message.blank?
          model = validate_if_exists(args[:id], dao)
        else
          model = validate_if_exists(args[:id], dao, message)
        end

        dao.update(args, model)
      end

      #
      # This funtion call the get_by_id function of Dao
      #
      # @param id [Integer] id of the model
      # @param dao [DaoObject] DAO of which get_by_id function to call
      # @param message [String] custom message
      #
      # @return [Object] object if it find the record
      #
      # @error [InvalidArgumentsError] if record not found
      #
      def validate_if_exists(id, dao, message='')
        message = "Record " if message.blank?
        model = dao.get_by_id(id)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(message + ' not found') if model.nil?

        return model
      end

      #
      # This funtion call the get_all function of Dao
      #
      # @param dao [DaoObject] DAO of which get_by_id function to call
      #
      # @return [Array] array of model
      #
      def get_all_elements(dao, pagination_params)
        return dao.get_all(pagination_params)
      end

      #
      # This funtion call the change_state function of Dao
      #
      # @param args [Hash] contains corresponding argument of change state
      # @param dao [DaoObject] DAO of which change_state function to call
      # @param message [String] custom message
      #
      # @return [Object] object if it succeed to update else pass the error raised by DAO
      #
      def change_state(args, dao, message='')
        message = "Record " if message.blank?
        model = validate_if_exists(args[:id], dao, message)
        dao.change_state(model, args[:event])
      end

      #
      # Function to get active element of a particular model
      #
      def get_active_element(id, dao, message='')
        message = 'Record ' if message.blank?
        model = dao.get_active_element_by_id(id)
        raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(message + ' not found') if model.nil?
        return model
      end
  	end
  end
end