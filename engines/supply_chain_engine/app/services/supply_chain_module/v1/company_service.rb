#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class CompanyService < SupplyChainBaseModule::V1::BaseService
      COMPANY_DAO = SupplyChainModule::V1::CompanyDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      def initialize(params={})
        @params = params
        super
      end

      #
      # create company
      #
      # @param args [Hash] contain params for creating company
      #
      # @return [companyObject]
      #
      # @error [InvalidArgumentsError] if it fails to create company
      #
      def create_company(args)
        company_dao_class = COMPANY_DAO.new(@params)

        company = company_dao_class.get_company_by_name(args[:name])

        if company.nil?
          begin
            company = company_dao_class.create args
          rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
            raise e
          end
        end

        company
      end

      #
      # update company
      #
      # @param args [Hash] contain params for updating company
      #
      # @return [companyObject]
      #
      # @error [InvalidArgumentsError] if it fails to update company
      #
      def update_company(args, company_id)
        company_dao_class = COMPANY_DAO.new(@params)
        company = get_company_by_id(company_id)
        company_dao_class.update(args, company)
      end

      #
      # Return all the companies
      #
      # @return [List] list of companies
      #
      def get_all_companies(pagination_params = {})
        company_dao_class = COMPANY_DAO.new(@params)
        get_all_elements(company_dao_class, pagination_params)
      end

      #
      # change the state of company
      #
      # @param company [Object] company of which status is to be changed
      # @param event [String] event to trigger to change the status 
      #
      def change_company_state(id, event)
        company_dao_class = COMPANY_DAO.new(@params)
        return company_dao_class.change_company_state(id, event)
      end

      #
      # return company corresponding to the id
      #
      # @param id [Integer] company id
      #
      # @return company [Model] compnay object
      #
      # @error [InvalidArgumentsError] if company doesn't exists
      #
      def get_company_by_id(id)
        company_dao_class = COMPANY_DAO.new(@params)

        company = company_dao_class.get_company_by_id(id)

        if company.nil?
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::COMPANY_NOT_FOUND)
        end

        return company
      end

      #
      # This function calls the companies dao to get label of all the companies
      # which have non-deleted brands and sub_brands under it
      # 
      # @return [Json Response]
      #
      def get_companies_tree
        company_dao_class = COMPANY_DAO.new(@params)
        aggregated_data = company_dao_class.get_companies_tree

        return aggregated_data
      end
    end
  end
end
