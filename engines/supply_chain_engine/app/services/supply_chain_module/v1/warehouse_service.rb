#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class WarehouseService < SupplyChainBaseModule::V1::BaseService
      WAREHOUSE_DAO = SupplyChainModule::V1::WarehouseDao
      CUSTOM_ERROR_UTIL = CommonModule::V1::CustomErrors
      CASH_AND_CARRY_DAO = SupplyChainModule::V1::CashAndCarryDao
      WAREHOUSE_ADDRESS_SERVICE = SupplyChainModule::V1::WarehouseModule::V1::WarehouseAddressService
      WBP_SERVICE = WarehouseProductModule::V1::WarehouseBrandPackService
      CATEGORY_UTIL = CategorizationModule::V1::CategoryUtil
      PAGINATION_UTIL = CommonModule::V1::Pagination
      def initialize(params={})
        super
      end

      #
      # create warehouse
      #
      # @param args [Hash] contain params for creating warehouse
      #
      # @return [warehouse object]
      #
      # @error [InvalidArgumentsError] if it fails to create warehouse
      #
      def create(args)
        warehouse_dao = WAREHOUSE_DAO.new
        validate_reference_params(args)
        begin
          warehouse = warehouse_dao.create(args)
        rescue CUSTOM_ERROR_UTIL::InvalidArgumentsError => e
          raise e
        end
        return warehouse
      end

      #
      # Return all the warehouse
      #
      # @return [List] list of warehouse
      #
      def get_all_warehouses(paginate_params={})
        warehouse_dao = WAREHOUSE_DAO.new
        warehouses = warehouse_dao.paginated_warehouse(paginate_params)
        return warehouses
      end

      #
      # Return the requested warehouse details
      #
      # @return [List] object of warehouse
      #
      def get_warehouse(warehouse_id)
        warehouse_dao = WAREHOUSE_DAO.new
        warehouse = warehouse_dao.get_warehouse_by_id(warehouse_id)
        return warehouse
      end

      # 
      # function to call dao changes state function to change the status of warehouse
      # 
      # @param request [request] [hash] contains event type to change state
      # 
      # @return [object] [warehouse]
      def change_state(request)
        raise CUSTOM_ERROR_UTIL::InsufficientDataError.new(CONTENT::WAREHOUSE_MISSING) if request[:id].blank?
        warehouse_dao = WAREHOUSE_DAO.new
        warehouse = warehouse_dao.change_state(request)
        return warehouse
      end

      #
      # update warehouse
      #
      # @param args [Hash] contain params for updating warehouse
      #
      # @return [warehosue object]
      #
      # @error [InvalidArgumentsError] if it fails to update warehouse
      #
      def update(args)
        warehouse_dao = WAREHOUSE_DAO.new
        warehouse = warehouse_dao.get_warehouse_by_id(args[:id])
        validate_reference_params(args)
        warehouse_dao.update(args, warehouse)
      end

      def add_warehouse_address(request)
        warehouse_dao = WAREHOUSE_DAO.new
        warehouse = warehouse_dao.get_warehouse_by_id(request[:id])
        warehouse_address_service = WAREHOUSE_ADDRESS_SERVICE.new
        warehouse.warehouse_addresses << warehouse_address_service.add_address(request)
        return warehouse
      end

      def update_warehouse_address(request)
        warehouse_dao = WAREHOUSE_DAO.new
        warehouse = warehouse_dao.get_warehouse_by_id(request[:id])
        warehouse_address_service = WAREHOUSE_ADDRESS_SERVICE.new
        warehouse_address_service.update_address(request)
        return warehouse
      end

      def get_all_warehouse_address(warehouse_id)
        warehouse_dao = WAREHOUSE_DAO.new
        warehouse = warehouse_dao.get_warehouse_by_id(warehouse_id)
        return warehouse
      end

      # 
      # Function to check if referencing attributes exists
      #
      def validate_reference_params(args)
        cash_and_carry_dao = CASH_AND_CARRY_DAO.new
        if args[:cash_and_carry_id].present?
          cash_and_carry = cash_and_carry_dao.get_cash_and_carry_by_id(args[:cash_and_carry_id])
        end
      end

      #
      # Transactional block to mark the end of the day for a warehouse
      #
      def transactional_warehouse_end_of_day(args)
        transactional_function = Proc.new do |args|
          return warehouse_end_of_day(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Function to audit the availability of WBP in the warehouse
      #
      def warehouse_end_of_day(args)
        warehouse_dao = WAREHOUSE_DAO.new
        wbp_service = WBP_SERVICE.new(@params)
        warehouse = warehouse_dao.get_warehouse_by_id(args[:id])
        wbp_service.audit_warehouse_brand_packs(warehouse.warehouse_brand_packs)
      end

      #
      # Function to get the stock details of the WBP present in the warehouse
      #
      def get_warehouse_stock_details(args)
        warehouse_dao = WAREHOUSE_DAO.new
        wbp_service = WBP_SERVICE.new(@params)
        warehouse_brand_packs_hash = warehouse_dao.get_filtered_wbps_by_pagination_params(args)
        stock_details = wbp_service.get_wbps_stock_detail(warehouse_brand_packs_hash[:warehouse_brand_packs])
        return { stock_details: stock_details, page_count: warehouse_brand_packs_hash[:page_count] }
      end

      #
      # Function to get data of all WBP stock details of a warehouse
      def download_warehouse_stock_details(args)
        args = args.merge(per_page: PAGINATION_UTIL::INFINITE_PER_PAGE)
        return get_warehouse_stock_details(args)
      end
    end
  end
end
