#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class DistributorService < SupplyChainBaseModule::V1::BaseService

      DISTRIBUTOR_DAO = SupplyChainModule::V1::DistributorDao
      ADD_INVENTORY_API = SupplyChainModule::V1::AddInventoryApi
      INVENTORY_SERVICE = SupplyChainModule::V1::InventoryService
      COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

      def initialize
        super
        @distributor_dao = DISTRIBUTOR_DAO.new
      end

      # 
      # create_distributor function to create a distributor
      # 
      def create_distributor(args)
        create_model(args, @distributor_dao)
      end

      # 
      # create_distributor_with_inventory function to create a distributor and a corresponding inventory
      # 
      def create_distributor_with_inventory(args)
        # to auto-trigger creation of inventory upon creation of a distributor
        add_inventory_api = ADD_INVENTORY_API.new({})
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          distributor = create_distributor(args.merge(status: COMMON_MODEL_STATES::ACTIVE))
          # added to auto-trigger inventory creation upon distributor creation due to one-to-one relation
          # can be removed later when another approach is taken for the same
          inventory = add_inventory_api.enact({
              distributor_id: distributor.id.to_s,
              name: distributor.name.to_s,
              status: COMMON_MODEL_STATES::ACTIVE
            })
          return distributor
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      def transactional_create_distributor_with_inventory_and_address(args)
        transactional_function = Proc.new do |args|
          return create_distributor_with_inventory_and_address(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      def create_distributor_with_inventory_and_address(args)
        # to auto-trigger creation of inventory upon creation of a distributor
        add_inventory_api = ADD_INVENTORY_API.new({})
        # to auto-trigger creation of inventory address upon creation of a inventory
        inventory_service = INVENTORY_SERVICE.new({})
        distributor = create_distributor(args.merge(status: COMMON_MODEL_STATES::ACTIVE))
        # added to auto-trigger inventory creation upon distributor creation due to one-to-one relation
        # can be removed later when another approach is taken for the same
        inventory = add_inventory_api.enact({
            distributor_id: distributor.id.to_s,
            name: distributor.name.to_s,
            status: COMMON_MODEL_STATES::ACTIVE
          })
        # add address of inventory
        if args[:inventory].present? && args[:inventory][:address].present?
          inventory_address = inventory_service.add_inventory_address(args[:inventory].merge(id: inventory[:payload][:inventory][:id]))
        end
        return distributor
      end

      def update_distributor(args)
        update_model(args, @distributor_dao, 'Distributor')
      end

      def get_distributor_by_id(id)
        validate_if_exists(id, @distributor_dao, 'Distributor')
      end

      def get_all_distributors(pagination_params = {})
        get_all_elements(@distributor_dao, pagination_params)
      end

      def change_distributor_state(args)
        change_state(args, @distributor_dao, 'Distributor')
      end
    end
  end
end
