module SupplyChainModule::V1
  module InventoryModule::V1
    class InventoryAddressService < SupplyChainBaseModule::V1::BaseService
      INVENTORY_ADDRESS_DAO = SupplyChainModule::V1::InventoryModule::V1::InventoryAddressDao
      AREA_SERVICE = AddressModule::V1::AreaService
      def initialize(params={})
        super
      end

      #
      # Function to add address for a inventory
      #
      def add_address(args)
        inventory_address_dao = INVENTORY_ADDRESS_DAO.new
        area_service = AREA_SERVICE.new
        area = area_service.get_area_by_id(args[:address][:area_id])
        address = inventory_address_dao.add_address(args)
        return address
      end

      # 
      # Function to update address of a inventory
      #
      def update_address(args)
        inventory_address_dao = INVENTORY_ADDRESS_DAO.new
        area_service = AREA_SERVICE.new
        area = area_service.get_area_by_id(args[:address][:area_id]) if args[:address][:area_id].present?
        present_address = inventory_address_dao.get_address_from_id(args[:address_id])
        validate_address_owner(present_address, args[:id].to_i)
        address = inventory_address_dao.update_address(args, present_address)
        return address    
      end

      #
      # Function to validate if address belongs to the proper inventory
      #
      def validate_address_owner(address, inventory_id)
        unless address.inventory_id == inventory_id.to_i
          raise CUSTOM_ERROR_UTIL::InvalidArgumentsError.new(CONTENT::WRONG_INVENTORY_ADDRESS_REFERENCE)
        end
      end

      #
      # Function to get address of a inventory using ID
      #
      def get_inventory_address_by_id(id)
        inventory_address_dao = INVENTORY_ADDRESS_DAO.new
        return inventory_address_dao.get_address_from_id(id)
      end
    end
  end
end
