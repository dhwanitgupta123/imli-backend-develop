#
# This module is responsible for logging meta data
#
module LoggingModule
  #
  # Version1 for logging module
  #
  module V1
    #
    # This class ressponsible to save data in SearchQueryLog document
    #
    class QueryLoggerService < BaseModule::V1::BaseService

      SEARCH_QUERY_LOG = LoggingModule::V1::SearchQueryLog

      def initialize
      end

      #
      # This function save the query log in SEARCH_QUERY_LOG mongo document
      # it save query and results for the corresponding query
      #
      # @param query [String] query string
      # @param results [Array] array of result
      #
      # @return [SeachQueryLog] SearchQueryLog document
      #
      def log_query_and_result(query, results)
        @search_query_log = SEARCH_QUERY_LOG.new
        @search_query_log.query = query
        @search_query_log.results = { results: results }
        @search_query_log.save
      end
    end
  end
end
