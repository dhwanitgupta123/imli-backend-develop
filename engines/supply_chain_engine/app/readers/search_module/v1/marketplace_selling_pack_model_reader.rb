#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # Reader class for marketplace_selling_pack model
    #
    class MarketplaceSellingPackModelReader
      def initialize(results)
        @results = results
      end

      #
      # Map marketplace_selling_pack result to array of attributes hash
      #
      # @return [Array] array of marketplace_selling_pack attributes
      #
      def get_attribute_array

        return [] if @results.blank?

        product_ids = []

        if ImliStaticSettings.get_feature_value('OUT_OF_STOCK_FLAG') == '1'

          @results.each {|result| product_ids.push(result.attributes['product']['id'])}

          aggregated_mpsp_details = MarketplaceProductModule::V1::MarketplaceSellingPackDao.
            get_mpsp_and_brand_pack_details(product_ids)

          return filter_out_of_stock_results(aggregated_mpsp_details)
        end

        attributes = []

        @results.each {|result| attributes.push(result.attributes['product'])}

        return attributes
      end

      private

      def filter_out_of_stock_results(aggregated_mpsp_details)
        attributes = []

        @results.each do |result|
          is_out_of_stock = MarketplaceProductModule::V1::MarketplaceSellingPackDao.
            is_out_of_stock?(aggregated_mpsp_details.find(result.attributes['product']['id']))
          attributes.push(result.attributes['product']) if is_out_of_stock == false
        end

        return attributes
      end
    end
  end
end
