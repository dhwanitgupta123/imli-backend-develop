#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V2
    #
    # Reader class for marketplace_selling_pack model
    #
    class MarketplaceSellingPackModelReader
      def initialize(results)
        @results = results
      end

      #
      # Map marketplace_selling_pack result to array of attributes hash
      #
      # @return [Array] array of marketplace_selling_pack attributes
      #
      def get_attribute_array

        return [] if @results.blank?

        attributes = []
        product_ids = []

        @results.each {|result| product_ids.push(result.attributes['product']['id'])}

        aggregated_mpsp_details = MarketplaceProductModule::V1::MarketplaceSellingPackDao.
          get_mpsp_and_brand_pack_details(product_ids)

        @results.each do |result|
          is_out_of_stock = MarketplaceProductModule::V1::MarketplaceSellingPackDao.
            is_out_of_stock?(aggregated_mpsp_details.find(result.attributes['product']['id']))
          if ImliStaticSettings.get_feature_value('OUT_OF_STOCK_FLAG') == '1'
            if (USER_SESSION[:platform_type] == 'ANDROID' && USER_SESSION[:version_number] < 22)
              attributes.push(result.attributes['product'].merge!(is_out_of_stock: is_out_of_stock)) unless is_out_of_stock
            else
              if (USER_SESSION[:platform_type] == 'WEB') || (USER_SESSION[:platform_type] == 'ANDROID' && USER_SESSION[:version_number] >= 22)
                if ImliStaticSettings.get_feature_value('SHOW_OUT_OF_STOCK_PRODUCT_FLAG') == '0'
                  attributes.push(result.attributes['product'].merge!(is_out_of_stock: is_out_of_stock)) unless is_out_of_stock
                else
                  attributes.push(result.attributes['product'].merge!(is_out_of_stock: is_out_of_stock))
                end
              else
                attributes.push(result.attributes['product'].merge!(is_out_of_stock: is_out_of_stock)) unless is_out_of_stock
              end
            end
          else
            attributes.push(result.attributes['product'].merge!(is_out_of_stock: is_out_of_stock))
          end
        end
        return attributes
      end
    end
  end
end
