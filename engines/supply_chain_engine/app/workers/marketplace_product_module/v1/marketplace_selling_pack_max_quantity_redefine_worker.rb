#
# This module is responsible for Marketplace Products
#
module MarketplaceProductModule
  #
  # Version1 for MarketplaceProductModule
  #
  module V1
    #
    # This class is responsible to redefine the max quantity of mpsp
    #
    class MarketplaceSellingPackMaxQuantityRedefineWorker < MarketplaceProductWorker

      #
      # function to perform async redefining of the Max quantity of MPSPS
      #
      # @param mpsps [MPSPS array]
      #
      def perform(mpsps)
        marketplace_selling_pack_service = MarketplaceProductModule::V1::MarketplaceSellingPackService.new
        marketplace_selling_pack_service.redefine_max_quantity_of_products(mpsps)
      end
    end
  end
end
