module MarketplaceProductModule
  module V1
    class MarketplaceProductWorker

      include Sidekiq::Worker
      sidekiq_options :queue => :default, :retry => false

    end
  end
end
