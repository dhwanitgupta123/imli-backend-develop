#
# This module is responsible for logging meta data
#
module LoggingModule
  #
  # Version1 for logging module
  #
  module V1
    #
    # This class is responsible to log the query and result in mongoid
    #
    class QueryLogWorker < SupplyChainLogWorker

      QUERY_LOGGER_SERVICE = LoggingModule::V1::QueryLoggerService
      #
      # function to save the log in ActivityLog table
      #
      # @param query [String] query entered by user
      # @param [Array] results return by search service
      #
      def perform(query, results)
        query_logger__service_class = QUERY_LOGGER_SERVICE.new
        query_logger__service_class.log_query_and_result(query, results)
      end
    end
  end
end
