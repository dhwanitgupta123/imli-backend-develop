#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Helper class to create JsonResponse
    #
    class SupplyChainResponseDecorator < BaseModule::V1::BaseResponseDecorator
      RESPONSE_CODES_UTIL = CommonModule::V1::ResponseCodes
      CONTENT_UTIL = CommonModule::V1::Content
      COMPANY_SERVICE_HELPER = SupplyChainModule::V1::CompanyServiceHelper

      def self.create_company_response(companies, page_count)
        {
          payload: {
            companies: companies
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: page_count
          }
        }
      end

      def self.create_single_company_response(company)
        {
          payload: {
            company: company
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
      
      def self.create_single_seller_response(seller)
        {
          payload: {
            seller: seller
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_distributor_response(distributors, page_count)
        {
          payload: {
            distributors: distributors
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: page_count
          }
        }
      end

      def self.create_single_distributor_response(distributor)
        {
          payload: {
            distributor: distributor
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_seller_response(seller)
        {
          payload: {
            sellers: seller[:seller]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: seller[:page_count]
          }
        }
      end

      def self.create_inventory_response(inventories, page_count)
        {
          payload: {
            inventories: inventories
          },
          response: RESPONSE_CODES_UTIL::SUCCESS,
          meta: {
            page_count: page_count
          }
        }
      end

      def self.create_single_inventory_response(inventory)
        {
          payload: {
            inventory: inventory
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
      
      # 
      # Decorator function for Cash & Carrys 
      #
      def self.create_cash_and_carry_response(args)
        {
          payload: {
            cash_and_carrys: args[:cash_and_carry]
          },
          meta: {
            page_count: args[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      # 
      # Decorator function for Cash & Carry 
      #
      def self.create_single_cash_and_carry_response(c_and_c)
        {
          payload: {
            cash_and_carry: c_and_c
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      # 
      # Decorator function for Warehouse
      #
      def self.create_warehouse_response(args)
        {
          payload: {
            warehouses: args[:warehouse]
          },
          meta: {
            page_count: args[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      #
      # Decorator function for Warehouse
      #
      def self.create_single_warehouse_response(warehouse)
        {
          payload: {
            warehouse: warehouse
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      # 
      # Returns aggregated company, brands and sub_brands
      #
      # @param aggregated_data [Model] Joined company, brands and sub_brands
      # 
      # @return [JsonResponse]
      #
      def self.create_aggregated_data_response(aggregated_data)
        aggregated_data = COMPANY_SERVICE_HELPER.get_aggregated_data_array(aggregated_data)
        response = {
          payload: {
            companies: aggregated_data
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
        return response
      end
    end
  end
end
