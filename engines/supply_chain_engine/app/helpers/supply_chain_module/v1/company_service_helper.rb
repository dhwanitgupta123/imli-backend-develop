#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Module to generate CompanyService responses & other functionalities
    #
    # @author [kartik]
    #
    module CompanyServiceHelper
      #
      # defining global variable with versioning
      #
      BRAND_HELPER = MasterProductModule::V1::BrandHelper

      # 
      # This will create companies to brands to sub_brands tree
      # it also include one dummy company, brand and sub_brand which
      # is ALL which is required in panel
      #
      # @param aggregated_data [Model] Joined companies, brands and sub_brands
      # 
      # @return [Array] Aggregated array where company is root node
      #
      def self.get_aggregated_data_array(aggregated_data)
        companies = []

        return companies if aggregated_data.blank?

        aggregated_data.each do |data|
          companies.push({
              id: data.id,
              name: data.name,
              brands: BRAND_HELPER.get_brands_array(data.brands)
            })
        end

        return companies
      end

    end
  end
end
