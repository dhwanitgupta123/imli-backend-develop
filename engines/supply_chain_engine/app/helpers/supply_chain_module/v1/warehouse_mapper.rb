#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Map c&c object to Json
    #
    module WarehouseMapper

      CASH_AND_CARRY_MAPPER = SupplyChainModule::V1::CashAndCarryMapper
      WBP_MAPPER = WarehouseProductModule::V1::WarehouseBrandPackMapper

      def self.map_warehouse_to_hash(warehouse)
        return {} if warehouse.blank?
        cash_and_carry_hash = CASH_AND_CARRY_MAPPER.get_cash_and_carry_hash(warehouse.cash_and_carry)
        warehouse_hash = {
          id: warehouse.id,
          name: warehouse.name,
          cash_and_carry: cash_and_carry_hash,
          addresses: map_warehouse_address_to_array(warehouse)[:warehouse][:addresses],
          status: warehouse.status
        }
        return warehouse_hash
      end

      def self.map_warehouses_to_array(args)
        warehouses = args[:warehouse]
        warehouse_array = []
        warehouses.each do |warehouse|
          warehouse_array.push(map_warehouse_to_hash(warehouse))
        end
        return { warehouse: warehouse_array, page_count: args[:page_count] }
      end

      def self.map_warehouse_to_array(warehouse)
        warehouse = [ map_warehouse_to_hash(warehouse) ]
        return { warehouse: warehouse }
      end

      def self.map_short_warehouse_to_hash(warehouse)
        return {} if warehouse.blank?
        cash_and_carry_hash = CASH_AND_CARRY_MAPPER.get_cash_and_carry_hash(warehouse.cash_and_carry)
        warehouse_hash = {
          id: warehouse.id,
          name: warehouse.name,
          cash_and_carry: cash_and_carry_hash,
          status: warehouse.status
        }
        return warehouse_hash
      end

      def self.map_warehouse_address_to_hash(address)
        return {} if address.blank?
        response = {
          id: address.id,
          warehouse_name: address.warehouse_name || "",
          address_line1: address.address_line1 || "",
          address_line2: address.address_line2 || "",
          landmark: address.landmark || "",
          area: address.area.name || "",
          city: address.area.city.name || "",
          state: address.area.city.state.name || "",
          country: address.area.city.state.country.name || "",
          pincode: address.area.pincode || ""
        }
        return response
      end

      def self.map_warehouse_address_to_array(warehouse)
        warehouse_addresses = warehouse.warehouse_addresses
        warehouse_address_array = []
        warehouse_addresses.each do |warehouse_address|
          warehouse_address_array.push(map_warehouse_address_to_hash(warehouse_address))
        end
        return {
          warehouse: {
            id: warehouse.id,
            addresses: warehouse_address_array
          }
        }
      end

      def self.map_warehouse_stock_to_hash(warehouse_stock_details)
        warehouse_stock_array = []
        return warehouse_stock_array if warehouse_stock_details.blank?
        warehouse_stock_details.each do |wbp_stock|
          wbp_stock_hash = WBP_MAPPER.get_wbp_stock_hash(wbp_stock)
          warehouse_stock_array.push(wbp_stock_hash)
        end
        return warehouse_stock_array
      end

      def self.map_warehouse_procurement_to_hash(warehouse_procurement_details)
        warehouse_procurement_array = []
        return warehouse_procurement_array if warehouse_procurement_details.blank?
        warehouse_procurement_details.each do |wbp_procurement|
          wbp_procurement_hash = WBP_MAPPER.get_wbp_procurement_hash(wbp_procurement)
          warehouse_procurement_array.push(wbp_procurement_hash)
        end
        return warehouse_procurement_array
      end
    end
  end
end
