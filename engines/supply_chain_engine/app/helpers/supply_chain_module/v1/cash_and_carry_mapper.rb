#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Map c&c object to Json
    #
    module CashAndCarryMapper

      def self.map_cash_and_carry_to_hash(c_and_c)
        return {} if c_and_c.blank?
        c_and_c_hash = {
          id: c_and_c.id,
          name: c_and_c.name,
          status: c_and_c.status
        }
        return c_and_c_hash
      end

      def self.map_cash_and_carrys_to_array(args)
        cash_and_carrys = args[:cash_and_carry]
        c_and_c_array = []
        cash_and_carrys.each do |c_and_c|
          c_and_c_array.push(map_cash_and_carry_to_hash(c_and_c))
        end
        return { cash_and_carry: c_and_c_array, page_count: args[:page_count] }
      end

      def self.map_cash_and_carry_to_array(c_and_c)
        c_and_c = [ map_cash_and_carry_to_hash(c_and_c) ]
        return { cash_and_carry: c_and_c }
      end

      def self.get_cash_and_carry_hash(c_and_c)
        return {} if c_and_c.blank?
        c_and_c_hash = {
          id: c_and_c.id,
          name: c_and_c.name
        }
        return c_and_c_hash
      end
    end
  end
end
