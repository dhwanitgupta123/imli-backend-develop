#
# Module to handle all the functionalities related to supply chain
#
module SupplyChainModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Map company object to Json
    #
    module CompanyMapper

      def self.map_company_to_hash(company)
        return {} if company.blank?
        response = {
          id: company.id,
          name: company.name,
          logo_url: company.logo_url || '',
          vat_number: company.vat_number || '',
          tin_number: company.tin_number || '',
          cst: company.cst || '',
          trade_promotion_margin: company.trade_promotion_margin || '',
          data_sharing_margin: company.data_sharing_margin || '',
          incentives: company.incentives || '',
          reconciliation_period: company.reconciliation_period || '',
          legal_document_url: company.legal_document_url || '',
          initials: company.initials,
          status: company.status
        }
        return response
      end

      def self.map_short_company_to_hash(company)
        return {} if company.blank?
        response = {
          id: company.id,
          name: company.name
        }
        return response
      end

      def self.map_companies_to_array(companies)
        companies_array = []
        return companies_array if companies.blank?
        companies.each do |company|
          companies_array.push(map_company_to_hash(company))
        end
        companies_array
      end

      def self.map_company_to_array(company)
        [ map_company_to_hash(company) ]
      end
    end
  end
end
