#
# Module to handle all the functionalities related to cluster
#
module ClusterModule
  #
  # Version1 for cluster module
  #
  module V1
    #
    # Responsible to map service return object to required api response
    #
    module ClusterMapper

      MPSP_CATEGORIZATION_API_RESPONSE_DECORATOR = CategorizationModule::V1::MpspCategorizationApiResponseDecorator
      IMAGE_SERVICE_HELPER = ImageServiceModule::V1::ImageServiceHelper
      CLUSTER_TYPES_UTIL = ClusterModule::V1::ClusterTypes
      CLUSTER_LEVELS_UTIL = ClusterModule::V1::ClusterLevels
      MARKETPLACE_SELLING_PACK_HELPER = MarketplaceProductModule::V1::MarketplaceSellingPackHelper

      #
      # This function maps array of clusters to array of cluster hash
      #
      # @param clusters [Array] array of active record
      #
      # @return [Array] return array of cluster hash
      #
      def self.map_cluster_array_to_array_of_hash(clusters)

        return [] if clusters.blank?

        clusters_array = []

        clusters.each do |cluster|
          clusters_array.push(map_cluster_to_hash(cluster, false))
        end

        return clusters_array
      end

      #
      # This function maps cluster model to required cluster hash
      #
      # @param cluster [Model] cluster model
      # @param mpsps_info = true [Boolean] if response contain whole mpsp information
      #
      # @return [Hash] cluster hash
      #
      def self.map_cluster_to_hash(cluster, mpsps_info = true)

        return {} if cluster.blank?
        marketplace_selling_packs = cluster.marketplace_selling_packs
        active_mpsps = MARKETPLACE_SELLING_PACK_HELPER.filter_active_mpsps(marketplace_selling_packs)
        in_stock_mpsps = MARKETPLACE_SELLING_PACK_HELPER.filter_out_of_stock_mpsps(active_mpsps)
        return {} if in_stock_mpsps.blank?
        context = get_context_hash(cluster.cluster_context)
        display_cluster_type = CLUSTER_TYPES_UTIL.get_display_type(cluster.cluster_type)

        cluster_hash = {
                id: cluster.id,
                label: cluster.label,
                cluster_type: cluster.cluster_type,
                status: cluster.status,
                display_cluster_type: display_cluster_type,
                description: cluster.description,
                images: IMAGE_SERVICE_HELPER.get_images_hash_by_ids(cluster.images),
                context: context
              }

        if mpsps_info == true
          marketplace_sellling_packs = cluster.marketplace_selling_packs
          aggregated_mpsp_categorization = MarketplaceProductModule::V1::MarketplaceSellingPackDao.
            get_aggregated_mpsp_categorization(marketplace_sellling_packs.ids)

          sorted_mpsps = MARKETPLACE_SELLING_PACK_HELPER.
                                      get_sorted_mpsps_by_categorization(aggregated_mpsp_categorization)

          active_mpsps = MARKETPLACE_SELLING_PACK_HELPER.filter_active_mpsps(sorted_mpsps)
          in_stock_mpsps = MARKETPLACE_SELLING_PACK_HELPER.filter_out_of_stock_mpsps(active_mpsps)

          cluster_hash[:products] = MPSP_CATEGORIZATION_API_RESPONSE_DECORATOR.get_display_product_array(in_stock_mpsps)
        else
          cluster_hash[:product_ids] = cluster.marketplace_selling_pack_ids
        end

        return cluster_hash
      end

      #
      # Return cluster_context used to display in panel
      #
      # @param cluster_context [Model] cluster_context model
      #
      # @return [Hash] containing information related to cluster_context
      #
      def self.get_context_hash(cluster_context)

        return {} if cluster_context.blank?

        display_level = CLUSTER_LEVELS_UTIL.get_display_level(cluster_context.level)
        level_label = CLUSTER_LEVELS_UTIL.get_level_label(cluster_context.level, cluster_context.level_id)

        return {
          id: cluster_context.id,
          level: cluster_context.level,
          level_id: cluster_context.level_id,
          display_level: display_level,
          level_label: level_label
        }
      end
    end
  end
end
