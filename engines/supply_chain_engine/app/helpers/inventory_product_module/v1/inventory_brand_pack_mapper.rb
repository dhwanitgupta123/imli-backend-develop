#
# Module to handle all the functionalities related to supply chain
#
module InventoryProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Map inventory_brand_pack object to Json
    #
    module InventoryBrandPackMapper

      PRICING_MAPPER = InventoryPricingModule::V1::InventoryPricingMapper
      BRAND_PACK_MAPPER = MasterProductModule::V1::BrandPackMapper 
      INVENTORY_MAPPER = SupplyChainModule::V1::InventoryMapper

      def self.map_inventory_brand_pack_to_hash(inventory_brand_pack)
        if inventory_brand_pack.inventory_pricing.present?
          pricing = PRICING_MAPPER.map_inventory_pricing_to_hash(inventory_brand_pack.inventory_pricing)
        end

        if inventory_brand_pack.brand_pack.present?
          brand_pack_hash = BRAND_PACK_MAPPER.map_brand_pack_to_hash(inventory_brand_pack.brand_pack)
        end
        response = {
          id: inventory_brand_pack.id,
          status: inventory_brand_pack.status,
          inventory_id: inventory_brand_pack.inventory_id,
          distributor_pack_code: inventory_brand_pack.distributor_pack_code || '',
          brand_pack: brand_pack_hash,
          pricing: pricing
        }
        return response
      end

      def self.map_short_inventory_brand_pack_to_hash(inventory_brand_pack)
        if inventory_brand_pack.inventory_pricing.present?
          pricing = PRICING_MAPPER.map_inventory_pricing_to_hash(inventory_brand_pack.inventory_pricing)
        end

        if inventory_brand_pack.brand_pack.present?
          brand_pack_hash = BRAND_PACK_MAPPER.map_short_brand_pack_to_hash(inventory_brand_pack.brand_pack)
        end
        response = {
          id: inventory_brand_pack.id,
          status: inventory_brand_pack.status,
          inventory_id: inventory_brand_pack.inventory_id,
          distributor_pack_code: inventory_brand_pack.distributor_pack_code,
          brand_pack: brand_pack_hash,
          pricing: pricing
        }
        return response
      end

      def self.map_short_inventory_brand_pack_to_hash_with_inventory(inventory_brand_pack)
        if inventory_brand_pack.inventory_pricing.present?
          pricing = PRICING_MAPPER.map_inventory_pricing_to_hash(inventory_brand_pack.inventory_pricing)
        end

        if inventory_brand_pack.inventory.present?
          inventory_hash = INVENTORY_MAPPER.map_short_inventory_to_hash(inventory_brand_pack.inventory)
        end
        response = {
          id: inventory_brand_pack.id,
          status: inventory_brand_pack.status,
          distributor_pack_code: inventory_brand_pack.distributor_pack_code,
          inventory: inventory_hash || {},
          pricing: pricing
        }
        return response
      end

      def self.map_inventory_brand_packs_to_array(inventory_brand_packs)
        inventory_brand_packs_array = []
        inventory_brand_packs.each do |inventory_brand_pack|
          inventory_brand_packs_array.push(map_inventory_brand_pack_to_hash(inventory_brand_pack))
        end
        inventory_brand_packs_array
      end

      def self.map_inventory_brand_pack_to_array(inventory_brand_pack)
        [ map_inventory_brand_pack_to_hash(inventory_brand_pack) ]
      end

      def self.map_short_inventory_brand_packs_to_array(inventory_brand_packs)
        inventory_brand_packs_array = []
        inventory_brand_packs.each do |inventory_brand_pack|
          inventory_brand_packs_array.push(map_short_inventory_brand_pack_to_hash(inventory_brand_pack))
        end
        inventory_brand_packs_array
      end

      def self.map_short_inventory_brand_packs_to_array_with_inventory(inventory_brand_packs)
        inventory_brand_packs_array = []
        inventory_brand_packs.each do |inventory_brand_pack|
          inventory_brand_packs_array.push(map_short_inventory_brand_pack_to_hash_with_inventory(inventory_brand_pack))
        end
        inventory_brand_packs_array
      end
    end
  end
end
