#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Map marketplace_brand_pack object to Json
    #
    module MarketplaceSellingPackHelper

      MARKETPLACE_SELLING_PACK_DAO = MarketplaceProductModule::V1::MarketplaceSellingPackDao

      #
      # This function take aggregated mpsp categorization as input and sort mpsps by
      # mpsp_department, mpsp_category and mpsp_sub_category
      #
      # @param brand_packs [ActiveRecordRelationArray] aggregated_mpsp_categorization
      #
      # @return [ActiveRecordRelationArray] sorted array of mpsp_ids by categorization
      #
      def self.get_sorted_mpsps_by_categorization(aggregated_mpsp_categorization)

        mpsp_categorzation_array = []

        return aggregated_mpsp_categorization if aggregated_mpsp_categorization.blank?

        aggregated_mpsp_categorization.each do |mpsp_categorization|
          mpsp_categorzation_array.push(get_mpsp_categorization_hash(mpsp_categorization))
        end

        sorted_mpsp_categorization_array = mpsp_categorzation_array.sort{|a, b| [
                                                                                  a[:mpsp_department_priority],
                                                                                  a[:mpsp_category_priority],
                                                                                  a[:mpsp_sub_category_priority]
                                                                                ] <=> [
                                                                                  b[:mpsp_department_priority],
                                                                                  b[:mpsp_category_priority],
                                                                                  b[:mpsp_sub_category_priority]
                                                                                ]
                                                                              }

        sorted_mpsps = []

        sorted_mpsp_categorization_array.each do |mpsp_categorization|
          sorted_mpsps.push(mpsp_categorization[:marketplace_selling_pack])
        end

        return sorted_mpsps
      end

      private

      #
      #  create hash of mpsp and categorization priority
      #
      def self.get_mpsp_categorization_hash(mpsp_categorization)
        {
          id: mpsp_categorization.id,
          mpsp_sub_category_priority: mpsp_categorization.mpsp_sub_category.priority,
          mpsp_category_priority: mpsp_categorization.mpsp_sub_category.mpsp_parent_category.priority,
          mpsp_department_priority: mpsp_categorization.mpsp_sub_category.mpsp_parent_category.mpsp_department.priority,
          marketplace_selling_pack: mpsp_categorization
        }
      end

      #
      # return only active mpsps
      #
      def self.filter_active_mpsps(marketplace_selling_packs)

        active_marketplace_selling_packs = []

        return active_marketplace_selling_packs if marketplace_selling_packs.blank?

        marketplace_selling_packs.each do |marketplace_selling_pack|
          if MARKETPLACE_SELLING_PACK_DAO.is_node_active?(marketplace_selling_pack)
            active_marketplace_selling_packs.push(marketplace_selling_pack)
          end
        end
        return active_marketplace_selling_packs
      end

      #
      # Return only in stock MPSPS
      #
      def self.filter_out_of_stock_mpsps(marketplace_selling_packs)
        in_stock_marketplace_selling_packs = []
        return in_stock_marketplace_selling_packs if marketplace_selling_packs.blank?
        marketplace_selling_packs.each do |marketplace_selling_pack|
          in_stock_marketplace_selling_packs.push(marketplace_selling_pack) unless is_mpsp_out_of_stock(marketplace_selling_pack)
        end
        return in_stock_marketplace_selling_packs
      end

      def self.is_mpsp_out_of_stock(mpsp)
        # OUT_OF_STOCK_FLAG to turn on & off out of stock feature
        # SHOW_OUT_OF_STOCK_PRODUCT_FLAG to show and not to show out of stock products
        if ImliStaticSettings.get_feature_value('OUT_OF_STOCK_FLAG') == '1'
          if (USER_SESSION[:platform_type] == 'ANDROID' && USER_SESSION[:version_number] < 22)
            return MARKETPLACE_SELLING_PACK_DAO.is_out_of_stock?(mpsp)
          else            
            if (USER_SESSION[:platform_type] == 'WEB') || (USER_SESSION[:platform_type] == 'ANDROID' && USER_SESSION[:version_number] >= 22)
              if ImliStaticSettings.get_feature_value('SHOW_OUT_OF_STOCK_PRODUCT_FLAG') == '0'
                return MARKETPLACE_SELLING_PACK_DAO.is_out_of_stock?(mpsp)
              else
                false
              end
            else
              return MARKETPLACE_SELLING_PACK_DAO.is_out_of_stock?(mpsp)
            end
          end
        else
          return false
        end
      end

      def self.is_ladder_active(mpsp)
        return false if mpsp.blank?
        return mpsp.is_ladder_pricing_active
      end

    end
  end
end
