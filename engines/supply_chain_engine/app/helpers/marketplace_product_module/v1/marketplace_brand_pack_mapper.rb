#
# Module to handle all the functionalities related to supply chain
#
module MarketplaceProductModule
  #
  # Version1 for supply chain module
  #
  module V1
    #
    # Map marketplace_brand_pack object to Json
    #
    module MarketplaceBrandPackMapper

      BRAND_PACK_MAPPER = MasterProductModule::V1::BrandPackMapper 
      PRICING_MAPPER = MarketplacePricingModule::V1::MarketplaceBrandPackPricingMapper
      def self.map_marketplace_brand_pack_to_hash(marketplace_brand_pack)
        return {} if marketplace_brand_pack.blank?

        if marketplace_brand_pack.brand_pack.present?
          brand_pack_hash = BRAND_PACK_MAPPER.map_brand_pack_to_hash(marketplace_brand_pack.brand_pack)
        end

        if marketplace_brand_pack.marketplace_brand_pack_pricing.present?
          pricing = PRICING_MAPPER.map_marketplace_brand_pack_pricing_to_hash(
            marketplace_brand_pack.marketplace_brand_pack_pricing)
        end
        response = {
          id: marketplace_brand_pack.id,
          status: marketplace_brand_pack.status,
          is_on_sale: marketplace_brand_pack.is_on_sale,
          is_ladder_pricing_active: marketplace_brand_pack.is_ladder_pricing_active,
          seller_brand_packs: map_seller_brand_packs(marketplace_brand_pack.seller_brand_packs),
          brand_pack: brand_pack_hash,
          pricing: pricing
        }
        return response
      end

      def self.map_marketplace_brand_packs_to_array(marketplace_brand_packs)
        marketplace_brand_pack_array = []
        return marketplace_brand_pack_array if marketplace_brand_packs.blank?

        marketplace_brand_packs.each do |marketplace_brand_pack|
          marketplace_brand_pack_array.push(map_marketplace_brand_pack_to_hash(marketplace_brand_pack))
        end
        marketplace_brand_pack_array
      end

      def self.map_marketplace_brand_pack_to_array(marketplace_brand_pack)
        [ map_marketplace_brand_pack_to_hash(marketplace_brand_pack) ]
      end

      def self.map_seller_brand_packs(sbps)
        response_array = []
        return response_array if sbps.blank?

        sbps.each do |sbp|
          response_array.push(sbp.id)
        end
        return response_array
      end
    end
  end
end