#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Brand pack helper module to do some calculations
    #
    module BrandPackHelper

      #
      # Function to create brand pack code
      #
      def self.create_brand_pack_code(args)
        brand_pack_code = args[:product].sub_brand.brand.company.initials + '-' +
                          args[:product].sub_brand.brand.initials + '-' +
                          args[:product].sub_brand.initials + '-' +
                          args[:product].initials
        if args[:product].variant.present?
          brand_pack_code = brand_pack_code + '-' + args[:product].variant.initials
        end
        if args[:sku_size].present?
          brand_pack_code = brand_pack_code + '-' + args[:sku_size]
        end
        if args[:unit].present?
          brand_pack_code = brand_pack_code + '-' + args[:unit]
        end
        return brand_pack_code
      end

      #
      # function to update the brand pack code
      #
      def self.update_brand_pack_code(args)
        brand_pack_code = args[:brand_pack].product.sub_brand.brand.company.initials + '-' +
                          args[:brand_pack].product.sub_brand.brand.initials + '-' +
                          args[:brand_pack].product.sub_brand.initials + '-' +
                          args[:brand_pack].product.initials
        if args[:brand_pack].product.variant.present?
          brand_pack_code = brand_pack_code + '-' + args[:brand_pack].product.variant.initials
        end
        if args[:sku_size].present?
          brand_pack_code = brand_pack_code + '-' + args[:sku_size]
        else
          brand_pack_code = brand_pack_code + '-' + args[:brand_pack].sku_size
        end
        if args[:unit].present?
          brand_pack_code = brand_pack_code + '-' + args[:unit]
        else
          brand_pack_code = brand_pack_code + '-' + args[:brand_pack].unit
        end
        return brand_pack_code
      end
    end
  end
end
