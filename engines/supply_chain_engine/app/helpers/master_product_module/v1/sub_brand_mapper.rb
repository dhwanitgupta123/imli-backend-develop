#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Map sub_brand object to Json
    #
    module SubBrandMapper
      def self.map_sub_brand_to_hash(sub_brand)
        {
          id: sub_brand.id,
          name: sub_brand.name,
          logo_url: sub_brand.logo_url,
          brand_id: sub_brand.brand_id,
          initials: sub_brand.initials,
          status: sub_brand.status

        }
      end

      def self.map_sub_brands_to_array(sub_brands)
        sub_brands_array = []
        sub_brands.each do |sub_brand|
          sub_brands_array.push(map_sub_brand_to_hash(sub_brand))
        end
        sub_brands_array
      end

      def self.map_sub_brand_to_array(sub_brand)
        [ map_sub_brand_to_hash(sub_brand) ]
      end
    end
  end
end