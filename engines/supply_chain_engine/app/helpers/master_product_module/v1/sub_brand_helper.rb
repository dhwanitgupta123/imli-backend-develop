#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
  	#
    # Module to generate SubBrandService responses & other functionalities
    #
    # @author [kartik]
    #
    module SubBrandHelper
    	#
      # defining global variable with versioning
      #
    	COMMON_MODEL_STATES = SupplyChainCommonModule::V1::CommonStates

    	#
      # Creates sub_brands array with dummy sub_brand
      #
      def self.get_sub_brands_array(sub_brands)
        sub_brands_array = []

        return sub_brands_array if sub_brands.blank?
        
        sub_brands.each do |sub_brand|
          sub_brand_hash = get_sub_brand_detail(sub_brand)
          sub_brands_array.push(sub_brand_hash)
        end
        return sub_brands_array
      end

      #
      # Creates sub_brand hash with it's details.
      # Extension: Can be extended with more details later
      #
      def self.get_sub_brand_detail(sub_brand)
        sub_brand_hash = {}
        return sub_brand_hash if sub_brand.blank?

        sub_brand_hash = {
          id: sub_brand.id,
          name: sub_brand.name,
        }   
      end
      
    end
  end
end