#
# Module to handle all the functionalities related to master product
#
module MasterProductModule
  #
  # Version1 for master product module
  #
  module V1
    #
    # Product helper module to do some calculations
    #
    module ProductHelper

      #
      # Function to create product code
      #
      def self.create_product_code(args)
        product_code = args[:sub_brand].brand.company.initials + '-' +
                       args[:sub_brand].brand.initials + '-' +
                       args[:sub_brand].initials + '-' +
                       args[:initials]
        if args[:variant].present?
          product_code = product_code + '-' + args[:variant].initials
        end
        return product_code
      end

      #
      # function to update the product code with new initials
      #
      def self.update_product_code(args)
        product_code = args[:product].sub_brand.brand.company.initials + '-' +
                       args[:product].sub_brand.brand.initials + '-' +
                       args[:product].sub_brand.initials + '-' +
                       args[:product].initials
        if args[:variant].present?
          product_code = product_code + '-' + args[:variant].initials
        # else
        #   product_code = product_code + '-' + args[:product].variant.initials
        end
        return product_code
      end
    end
  end
end
