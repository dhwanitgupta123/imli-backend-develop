#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    require "rails_helper"
    RSpec.describe PincodeRepositoryInitializerWorker, type: :worker do

      let(:pincode_repository_initializer_worker) { SearchModule::V1::PincodeRepositoryInitializerWorker}

      it "enqueues a PincodeRepositoryInitializerWorker" do
        pincode_repository_initializer_worker.perform_async
        expect(pincode_repository_initializer_worker).to have_enqueued_job
      end
          
      #to check the queue name
      it { is_expected.to be_processed_in :default }
    end
  end
end