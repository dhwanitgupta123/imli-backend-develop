
#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    require 'rails_helper'

    RSpec.describe GeographicalClustersController, type: :routing do

      # specifying engine routes
      routes { LocationEngine::Engine.routes }

      describe 'routing' do

        it 'routes to #add_geographical_cluster' do
          expect(:post => 'geographical_clusters/new').to route_to(
                                       {
                                         'format' => 'json',
                                         'controller' => 'geographical_clusters_module/v1/geographical_clusters',
                                         'action' => 'add_geographical_cluster'
                                       })
        end

        it 'routes to #update_geographical_cluster' do
          expect(:put => 'geographical_clusters/1').to route_to(
                                                         {
                                                           'format' => 'json',
                                                           'controller' => 'geographical_clusters_module/v1/geographical_clusters',
                                                           'action' => 'update_geographical_cluster',
                                                           'id' => '1'
                                                         })
        end

        it 'routes to #get_geographical_cluster' do
          expect(:get => 'geographical_clusters/1').to route_to(
                                                         {
                                                           'format' => 'json',
                                                           'controller' => 'geographical_clusters_module/v1/geographical_clusters',
                                                           'action' => 'get_geographical_cluster',
                                                           'id' => '1'
                                                         })
        end
      end
    end
  end
end
