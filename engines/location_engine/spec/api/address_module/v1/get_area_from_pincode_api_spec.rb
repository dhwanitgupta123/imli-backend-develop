module AddressModule
  module V1
    require 'rails_helper'
    RSpec.describe AddressModule::V1::GetAreaFromPincodeApi do
      let(:version) { 1 }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:get_area_from_pincode_api) { AddressModule::V1::GetAreaFromPincodeApi.new(version) }

      let(:area_service) { AddressModule::V1::AreaService }

      let(:pincode) { Faker::Number.number(6) }

      let(:city) { FactoryGirl.build_stubbed(:city) }
      let(:area) { FactoryGirl.build_stubbed(:area) }
      let(:area_reader) { AddressModule::V1::AreaReader.new(area) }

      context 'enact ' do
        it 'with invalid request' do
          data = get_area_from_pincode_api.enact({})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with nil pincode' do
          data = get_area_from_pincode_api.enact pincode: nil
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with pincode less than 6 digits' do
          data = get_area_from_pincode_api.enact pincode: Faker::Number.number(5).to_s
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with pincode containing alphabets' do
          data = get_area_from_pincode_api.enact pincode: Faker::Lorem.characters(6)
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with not existing area' do
          expect_any_instance_of(area_service).to receive(:get_area_from_pincode)
            .with(pincode).and_raise(custom_errors_util::RecordNotFoundError.new)
          data = get_area_from_pincode_api.enact pincode: pincode
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid pincode' do
          expect_any_instance_of(area_service).to receive(:get_area_from_pincode).with(pincode).and_return(area_reader)
          data = get_area_from_pincode_api.enact pincode: pincode
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
