#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    require 'rails_helper'
    RSpec.describe GeographicalClustersModule::V1::GetGeographicalClusterApi do
      let(:version) { 1 }
      let(:response_codes) { CommonModule::V1::ResponseCodes }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:geographical_cluster_service) { GeographicalClustersModule::V1::GeographicalClusterService }
      let(:geographical_cluster) { FactoryGirl.build(:geographical_cluster) }
      let(:get_geographical_cluster_api) { GeographicalClustersModule::V1::GetGeographicalClusterApi.instance(version) }

      context 'enact ' do
        it 'with invalid args' do
          data = get_geographical_cluster_api.enact({id: nil})
          expect(data[:response]).to eq(response_codes::BAD_REQUEST)
        end

        it 'with valid args' do
          expect_any_instance_of(geographical_cluster_service).to receive(:get_geographical_cluster_by_id).
            and_return(geographical_cluster)
          data = get_geographical_cluster_api.enact({id: Faker::Number.number(1) })
          expect(data[:response]).to eq(response_codes::SUCCESS)
        end
      end
    end
  end
end
