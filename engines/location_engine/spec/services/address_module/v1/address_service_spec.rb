module AddressModule
  module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::AddressService do
    	let(:custom_error_util) { CommonModule::V1::CustomErrors }
     
      let(:address_service) { AddressModule::V1::AddressService.new }
      let(:area_service) { AddressModule::V1::AreaService }
      let(:city_service) { AddressModule::V1::CityService }
      let(:address_dao) { AddressModule::V1::AddressDao }

      let(:message) { FactoryGirl.build_stubbed(:message) }
      let(:provider) { FactoryGirl.build_stubbed(:provider, message: message) }
      let(:user) { FactoryGirl.build(:user, provider: provider) }

      let(:area) { FactoryGirl.build_stubbed(:area) }
      let(:address) { FactoryGirl.build_stubbed(:address) }

      let(:add_address_request) {
        {
        	user: user,
        	address: {
	          area_id: area.id,
	        	nickname: address.nickname,
	          landmark: address.landmark,
	        	address_line1: address.address_line1,
	        	address_line2: address.address_line2
        	}
        }
      }

      let(:add_address_params) {
        {
        	user: user,
        	area: area,
        	address: {
	          area_id: area.id,
	          nickname: address.nickname,
	          landmark: address.landmark,
	        	address_line1: address.address_line1,
	        	address_line2: address.address_line2
        	}
        }
      }

      let(:update_address_request) {
        {
        	user: user,
        	address: {
            id: address.id,
            area_id: area.id,
	        	nickname: address.nickname,
	          landmark: address.landmark,
	        	address_line1: address.address_line1,
	        	address_line2: address.address_line2
        	}
        }
      }

      let(:update_address_params) {
        {
        	user: user,
        	area: area,
        	new_address: {
	          area_id: area.id,
	          id: address.id,
        		nickname: address.nickname,
	          landmark: address.landmark,
	        	address_line1: address.address_line1,
	        	address_line2: address.address_line2
        	},
        	present_address: address
        }
      }

      context "add address for user" do

      	it "with valid area" do
      		expect_any_instance_of(area_service).to receive(:get_area_by_id).
            with(add_address_request[:address][:area_id]).and_return(area)
          expect_any_instance_of(address_dao).to receive(:add_address_for_user).
          	with(add_address_params).and_return(address)

          response = address_service.add_address_for_user(add_address_request)
          expect(response).to eq(address)
      	end

      	it "with invalid area" do
      		expect_any_instance_of(area_service).to receive(:get_area_by_id).
            with(add_address_request[:address][:area_id]).
            and_raise(custom_error_util::RecordNotFoundError.new)
          expect { address_service.add_address_for_user(add_address_request) }.
            to raise_error(custom_error_util::RecordNotFoundError)
      	end

        it "with invalid address" do
          expect_any_instance_of(area_service).to receive(:get_area_by_id).
            with(update_address_request[:address][:area_id]).and_return(area)
          expect_any_instance_of(address_dao).to receive(:add_address_for_user).
            with(add_address_params).
            and_raise(custom_error_util::RecordNotFoundError.new)
          expect { address_service.add_address_for_user(add_address_request) }.
            to raise_error(custom_error_util::RecordNotFoundError)
        end

        it "with valid address" do
          expect_any_instance_of(area_service).to receive(:get_area_by_id).
            with(update_address_request[:address][:area_id]).and_return(area)
          expect_any_instance_of(address_dao).to receive(:add_address_for_user).
            with(add_address_params).
            and_return(address)
          response = address_service.add_address_for_user(add_address_request)
          expect(response).to eq(address)
        end
      end

      context "update address for user" do

        it "with invalid user trying to update address not owned by him" do
          update_address_request[:user] = ''
          expect_any_instance_of(area_service).to receive(:get_area_by_id).
            with(update_address_request[:address][:area_id]).and_return(area)
          expect_any_instance_of(address_dao).to receive(:get_address_from_id).
            with(update_address_request[:address][:id]).and_return(address)
          expect { address_service.update_address_for_user(update_address_request) }.
            to raise_error(custom_error_util::UnAuthorizedUserError)
        end

      	it "with non existent address" do
          expect_any_instance_of(area_service).to receive(:get_area_by_id).
            with(update_address_request[:address][:area_id]).and_return(area)
      		expect_any_instance_of(address_dao).to receive(:get_address_from_id).
            with(update_address_request[:address][:id]).
            and_raise(custom_error_util::RecordNotFoundError.new)
          expect { address_service.update_address_for_user(update_address_request) }.
            to raise_error(custom_error_util::RecordNotFoundError)
      	end

      	it "with invalid area" do
          expect_any_instance_of(area_service).to receive(:get_area_by_id).
            with(update_address_request[:address][:area_id]).and_raise(custom_error_util::RecordNotFoundError.new)
          expect { address_service.update_address_for_user(update_address_request) }.
            to raise_error(custom_error_util::RecordNotFoundError)
      	end

      	it "with valid area and valid existing address" do
          user.addresses << address
          expect_any_instance_of(area_service).to receive(:get_area_by_id).
            with(update_address_request[:address][:area_id]).and_return(area)
      		expect_any_instance_of(address_dao).to receive(:get_address_from_id).
            with(update_address_request[:address][:id]).and_return(address)
          expect_any_instance_of(address_dao).to receive(:update_address_for_user).
          	with(update_address_params).and_return(address)

          response = address_service.update_address_for_user(update_address_request)
          expect(response).to eq(address)
      	end
      end
    end
  end
end