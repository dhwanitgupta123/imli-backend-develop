module AddressModule
  module V1
    require 'rails_helper'
    RSpec.describe AddressModule::V1::CityService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:city_service) { AddressModule::V1::CityService.new }

      let(:city_dao) { AddressModule::V1::CityDao }
      let(:state_service) { AddressModule::V1::StateService }

      let(:city) { FactoryGirl.build_stubbed(:city) }
      let(:state) { FactoryGirl.build_stubbed(:state) }
      let(:country_name) { Faker::Address.country }
      let(:state_name) { Faker::Address.state }
      let(:city_name) { Faker::Address.city }

      context 'create city' do
        it 'with pre-existing city' do
          expect_any_instance_of(state_service).to receive(:create_state).
            with(state_name,country_name).and_return(state)
          expect_any_instance_of(city_dao).to receive(:get_city_by_name_and_state_id).and_return(city)

          response = city_service.create_city(city_name, state_name, country_name)

          expect(response).to equal(city)
        end

        it 'with non-existing city ' do
          expect_any_instance_of(state_service).to receive(:create_state).and_return(state)
          expect_any_instance_of(city_dao).to receive(:create).and_return(city)

          response = city_service.create_city(city_name, state_name, country_name)

          expect(response).to equal(city)
        end

        it 'with invalid country_name or state_name ' do
          expect_any_instance_of(state_service).to receive(:create_state).
            and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{city_service.create_city(city_name, state_name, country_name)}.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid city name ' do
          expect_any_instance_of(state_service).to receive(:create_state).and_return(state)
          expect_any_instance_of(city_dao).to receive(:get_city_by_name_and_state_id).and_return(nil)
          expect_any_instance_of(city_dao).to receive(:create).and_raise(custom_error_util::InvalidArgumentsError.new)

          expect{city_service.create_city(city_name, state_name, country_name)}.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end
