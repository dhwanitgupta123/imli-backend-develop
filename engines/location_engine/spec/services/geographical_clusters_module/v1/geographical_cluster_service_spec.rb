#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    require 'rails_helper'
    RSpec.describe GeographicalClustersModule::V1::GeographicalClusterService do
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:geographical_cluster_service) { GeographicalClustersModule::V1::GeographicalClusterService.instance({}) }
      let(:geographical_cluster_dao) { GeographicalClustersModule::V1::GeographicalClusterDao }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:cluster_types) { ClusterModule::V1::ClusterTypes }
      let(:geographical_cluster) { FactoryGirl.build(:geographical_cluster) }

      let(:geographical_cluster_request) {
        {
          label: Faker::Company.name,
          level: Faker::Number.number(1),
          level_id: Faker::Number.number(1)
        }
      }

      let(:update_geographical_cluster_request) {
        {
          id: geographical_cluster.id,
          label: Faker::Company.name
        }
      }

      context 'create geographical cluster' do
        it 'with valid arguments' do
          expect_any_instance_of(geographical_cluster_dao).to receive(:create).
            and_return(geographical_cluster)
          response = geographical_cluster_service.create_geographical_cluster(geographical_cluster_request)

          expect(response).to equal(geographical_cluster)
        end
      end

      context 'update geographical cluster' do
        it 'with non existing cluster' do
          expect_any_instance_of(geographical_cluster_dao).to receive(:get_by_id).
            and_return(nil)
          expect{ geographical_cluster_service.update_geographical_cluster(update_geographical_cluster_request) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with existing geographical cluster' do
          expect_any_instance_of(geographical_cluster_dao).to receive(:get_by_id).
            and_return(geographical_cluster)
          expect_any_instance_of(geographical_cluster_dao).to receive(:update).
            and_return(geographical_cluster)
          response = geographical_cluster_service.update_geographical_cluster(update_geographical_cluster_request)
          expect(response).to equal(geographical_cluster)
        end
      end


      context 'get geographical cluster by id' do
        it 'with existing cluster' do
          expect_any_instance_of(geographical_cluster_dao).to receive(:get_by_id).
            and_return(geographical_cluster)
          response = geographical_cluster_service.get_geographical_cluster_by_id(Faker::Number.number(1))
          expect(response).to equal(geographical_cluster)
        end

        it 'with non existing geographical cluster' do
          expect_any_instance_of(geographical_cluster_dao).to receive(:get_by_id).
            and_return(nil)
          expect{ geographical_cluster_service.get_geographical_cluster_by_id(Faker::Number.number(1)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end
