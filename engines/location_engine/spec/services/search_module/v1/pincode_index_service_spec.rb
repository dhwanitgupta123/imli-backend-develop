#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    require 'rails_helper'
    RSpec.describe SearchModule::V1::PincodeIndexService do
      let(:pincode_repository) { SearchModule::V1::PincodeRepository }
      let(:custom_errors_util) { CommonModule::V1::CustomErrors }
      let(:area_dao) { AddressModule::V1::AreaDao }
      let(:pincode_index_service_class) { SearchModule::V1::PincodeIndexService }
      let(:total_results) { 10 }

      context 'search in index' do
        it 'with valid repository' do
          pincode_model = SearchModule::V1::Pincode.new
          area_array = []
          area_array.push(FactoryGirl.build_stubbed(:area))
          expect_any_instance_of(area_dao).to receive(:get_all_area).and_return(area_array)
          pincode_model_reader = AddressModule::V1::PincodeModelReader.new({})
          expect_any_instance_of(pincode_index_service_class).to receive(:search).and_return([pincode_model])
          expect_any_instance_of(pincode_index_service_class).to receive(:delete_previous_index).and_return(true)
          expect_any_instance_of(pincode_index_service_class).to receive(:repository_exists?).twice.and_return(true)
          expect_any_instance_of(pincode_index_service_class).to receive(:index_exists?).and_return(true)
          expect_any_instance_of(pincode_index_service_class).to receive(:save).and_return(true)
          pincode_index_service = pincode_index_service_class.new
          pincode_index_service.initialize_index('true')
          data = pincode_index_service.get_results(Faker::Lorem.characters(10), total_results, Faker::Lorem.characters(1))
          expect(data).not_to equal(pincode_model_reader)
        end

        it 'with invalid repository' do
          pincode_index_service = pincode_index_service_class.new
          expect_any_instance_of(pincode_index_service_class).to receive(:repository_exists?).and_return(false)

          expect(pincode_index_service.initialize_index).to equal(false)
        end
      end
    end
  end
end
