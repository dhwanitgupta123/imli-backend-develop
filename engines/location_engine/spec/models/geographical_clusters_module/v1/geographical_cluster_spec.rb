#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    require 'rails_helper'

    RSpec.describe GeographicalClustersModule::V1::GeographicalCluster, type: :model do

      let(:geographical_cluster) {GeographicalClustersModule::V1::GeographicalCluster}

      it "is valid" do
        FactoryGirl.create(:geographical_cluster).should be_valid
      end

    end
  end
end
