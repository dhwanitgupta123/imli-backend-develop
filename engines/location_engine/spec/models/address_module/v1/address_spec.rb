module UsersModule
  module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::Address, type: :model do
      let(:area) { FactoryGirl.build_stubbed(:area) }

      it 'is valid' do
        FactoryGirl.build(:address, user_id: Faker::Number.number(2), area: area).should be_valid
      end

      it 'is invalid without user_id' do
        FactoryGirl.build(:address, user: nil, area: area).should_not be_valid
      end

      it 'is invalid without address1' do
        FactoryGirl.build(:address, address_line1: nil, user_id: Faker::Number.number(2), area: area).
          should_not be_valid
      end

      it 'is invalid without area' do
        FactoryGirl.build(:address, area: nil, user_id: Faker::Number.number(2)).should_not be_valid
      end
    end
  end
end
