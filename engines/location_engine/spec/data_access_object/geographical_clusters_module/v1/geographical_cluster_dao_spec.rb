#
# Module to handle all the functionalities related to cluster
#
module GeographicalClustersModule
  #
  # Version1 for cluster module
  #
  module V1
    require 'rails_helper'

    RSpec.describe GeographicalClustersModule::V1::GeographicalClusterDao, type: :dao do
      let(:geographical_cluster_dao) { GeographicalClustersModule::V1::GeographicalClusterDao.instance }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:common_events) { SupplyChainCommonModule::V1::CommonEvents }
      let(:common_states) { SupplyChainCommonModule::V1::CommonStates }

      let(:geographical_cluster_request) {
        {
          label: Faker::Company.name,
          level: 0
        }
      }

      context 'create geographical cluster' do
        it 'with valid params' do
          geographical_cluster = geographical_cluster_dao.create(geographical_cluster_request)
          expect(geographical_cluster.label).to eq(geographical_cluster_request[:label])
        end
      end

      context 'update geographical cluster' do
        it 'with valid arguments' do
          geographical_cluster_before = FactoryGirl.create(:geographical_cluster)
          new_label = Faker::Company.name
          geographical_cluster = geographical_cluster_dao.update({ label: new_label }, geographical_cluster_before)
          expect(geographical_cluster.label).to eq(new_label)
        end
      end

    end
  end
end
