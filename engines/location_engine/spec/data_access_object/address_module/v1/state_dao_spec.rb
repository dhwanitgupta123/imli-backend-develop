module AddressModule
	module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::StateDao, type: :dao do
      let(:state_dao) { AddressModule::V1::StateDao.new }
      let(:country) { FactoryGirl.build_stubbed(:country) }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:state_args) {
        {
          name: Faker::Address.state,
          country: country
        }
      }

      context 'create state' do
        it 'with valid name and country' do
          country_new = FactoryGirl.create(:country)
          state = state_dao.create(state_args.merge(country: country_new))
          expect(state.name).to eq(state_args[:name])
          expect(state.country_id).to eq(country_new.id)
          country_new.destroy
          state.destroy
        end

        it 'with invalid name' do
          expect{state_dao.create(state_args.merge(name: nil))}.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid country' do
          expect{state_dao.create(state_args.merge(country: nil))}.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end

      context 'update state' do
        let(:before_name) { Faker::Address.state }
        let(:state_before) { FactoryGirl.build(:state, name: before_name, country: country) }
        it 'with valid country' do
          country_new = FactoryGirl.create(:country)
          state = state_dao.update(state_args.merge(country: country_new), state_before)
          expect(state.name).to eq(state_args[:name])
          country_new.destroy
          state.destroy
        end

        it 'with invalid state' do
          expect { state_dao.update(state_args, nil) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid state name' do
          expect { state_dao.update(nil, state_before) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end