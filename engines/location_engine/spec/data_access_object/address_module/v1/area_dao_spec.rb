module AddressModule
	module V1
    require 'rails_helper'

    RSpec.describe AddressModule::V1::AreaDao, type: :dao do
      let(:area_dao) { AddressModule::V1::AreaDao.new }
      let(:custom_error_util) { CommonModule::V1::CustomErrors }
      let(:country) { FactoryGirl.build_stubbed(:country) }
      let(:state) { FactoryGirl.build_stubbed(:state, country: country) }
      let(:city) { FactoryGirl.build_stubbed(:city, state: state) }
      let(:area_args) {
        {
          name: Faker::Address.street_name,
          pincode: Faker::Number.number(6).to_s,
          city: city
        }
      }

      context 'create area' do
        it 'with valid name and city' do
          country_new = FactoryGirl.create(:country)
          state_new = FactoryGirl.create(:state, country: country_new)
          city_new  = FactoryGirl.create(:city, state: state_new)
          area = area_dao.create(area_args.merge(city: city_new))
          expect(area.name).to eq(area_args[:name])
          expect(area.city_id).to eq(city_new.id)

          area.delete
          city_new.delete
          state_new.delete
          country_new.delete
        end

        it 'with invalid name' do
          expect { area_dao.create(area_args.merge(name: nil)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid city' do
          expect { area_dao.create(area_args.merge(city: nil)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid pincode' do
          expect { area_dao.create(area_args.merge(pincode: nil)) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with pre existing pincode' do
          country_new = FactoryGirl.create(:country)
          state_new = FactoryGirl.create(:state, country: country_new)
          city_new  = FactoryGirl.create(:city, state: state_new)
          area = FactoryGirl.create(:area, pincode: area_args[:pincode], city: city_new)
          expect { area_dao.create(area_args) }.
            to raise_error(custom_error_util::InvalidArgumentsError)
          area.delete
          city_new.delete
          state_new.delete
          country_new.delete
        end
      end

      context 'update area' do
        let(:before_name) { Faker::Address.street_name }
        let(:area_before) { FactoryGirl.build(:area, name: before_name, city: city) }
        it 'with valid city' do
          country_new = FactoryGirl.create(:country)
          state_new = FactoryGirl.create(:state, country: country_new)
          city_new  = FactoryGirl.create(:city, state: state_new)
          area = area_dao.update(area_args.merge(city: city_new), area_before)
          expect(area.name).to eq(area_args[:name])
          area.delete
          city_new.delete
          state_new.delete
          country_new.delete
        end

        it 'with invalid area' do
          expect{area_dao.update(area_args, nil)}.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end

        it 'with invalid area name' do
          expect{area_dao.update(nil, area_before)}.
            to raise_error(custom_error_util::InvalidArgumentsError)
        end
      end
    end
  end
end
