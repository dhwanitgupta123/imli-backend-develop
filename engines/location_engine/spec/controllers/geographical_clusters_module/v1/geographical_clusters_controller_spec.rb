#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    require 'rails_helper'
    RSpec.describe GeographicalClustersController, type: :controller do
      # specifying engine routes
      routes { LocationEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:get_geographical_cluster_api) { GeographicalClustersModule::V1::GetGeographicalClusterApi }
      let(:add_geographical_cluster_api) { GeographicalClustersModule::V1::AddGeographicalClusterApi }
      let(:update_geographical_cluster_api) { GeographicalClustersModule::V1::UpdateGeographicalClusterApi }

      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }

      let(:geographica_cluster_service) { GeographicalClustersModule::V1::GeographicalClusterService }

      let(:stub_ok_response) {
        {
          payload: {
            geographical_cluster: {
              level: Faker::Number.number(1),
              label: Faker::Lorem.word
            }
          },
          response: response_codes_util::SUCCESS
        }
      }


      before(:each) do
        before_actions
        after_actions
      end

      describe 'get geographical cluster' do
        it 'returns details of geographical cluster successfully' do
          expect_any_instance_of(get_geographical_cluster_api).to receive(:enact).and_return(stub_ok_response)
          get :get_geographical_cluster, id: Faker::Number.number(1)
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          expect_any_instance_of(get_geographical_cluster_api).to receive(:enact).and_return(stub_bad_request_response)
          get :get_geographical_cluster, id: Faker::Number.number(1)
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update geographical cluster' do
        it 'returns updated geographical cluster successfully' do
          expect_any_instance_of(update_geographical_cluster_api).to receive(:enact).and_return(stub_ok_response)
          put :update_geographical_cluster, id: Faker::Number.number(1), geographical_cluster: { level: Faker::Number.number(1) }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when geographical cluster does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          expect_any_instance_of(update_geographical_cluster_api).to receive(:enact).and_return(stub_bad_request_response)
          put :update_geographical_cluster, id: Faker::Number.number(1), geographical_cluster: { level: Faker::Number.number(1) }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
