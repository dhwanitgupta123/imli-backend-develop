module AddressModule
  module V1
    require 'rails_helper'
    RSpec.describe AreasController, type: :controller do
      # specifying engine routes
      routes { LocationEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:get_area_from_pincode) { AddressModule::V1::GetAreaFromPincodeApi }
      let(:update_area) { AddressModule::V1::UpdateAreaApi }
      let(:change_area_status) { AddressModule::V1::ChangeAreaStatus }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }

      let(:area_service) { AddressModule::V1::AreaService }
      let(:pincode) { Faker::Number.number(6) }
      let(:area) { FactoryGirl.build_stubbed(:area) }
      let(:city) { FactoryGirl.build_stubbed(:city) }
      let(:state) { FactoryGirl.build_stubbed(:state) }
      let(:country) { FactoryGirl.build_stubbed(:country) }
      let(:stub_ok_response) {
        {
            payload: {
              pincode: pincode,
              area: area,
              city: city,
              state: state,
              country: country,
              is_active: "1"
            },
            response: response_codes_util::SUCCESS
        }
      }
      

      before(:each) do
        before_actions
        after_actions
      end

      describe 'get_area_from_pincode' do
        it 'returns details of area successfully' do
          stub_ok_response = {
            payload: {
              pincode: pincode,
              area: area,
              city: city,
              state: state,
              country: country,
              is_active: "1"
            },
            response: response_codes_util::SUCCESS
          }

          get_area_from_pincode.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_area_from_pincode, pincode: pincode
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          get_area_from_pincode.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_area_from_pincode
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end

      describe 'update area' do
        it 'returns updated area successfully' do
          update_area.any_instance.stub(:enact).and_return(stub_ok_response)
          put :update_area, id: area.id, area: { name: area.name }
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when area does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_area.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_area, id: area.id, area: { name: area.name }
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end

        it 'when request does not exist' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          update_area.any_instance.stub(:enact).and_return(stub_bad_request_response)
          put :update_area, id: area.id
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end