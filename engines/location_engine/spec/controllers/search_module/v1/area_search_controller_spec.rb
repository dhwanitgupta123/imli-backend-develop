module SearchModule
  module V1
    require 'rails_helper'
    RSpec.describe AreaSearchController, type: :controller do
      # specifying engine routes
      routes { LocationEngine::Engine.routes }

      let(:before_actions) { access_control_controller.any_instance.stub(:before_actions).and_return(true) }
      let(:after_actions) { access_control_controller.any_instance.stub(:after_actions).and_return(true) }
      let(:access_control_controller) { UsersModule::V1::AccessControlController }
      let(:search_area_by_query) { SearchModule::V1::SearchAreaByQueryApi }
      let(:content_util) { CommonModule::V1::Content }
      let(:response_codes_util) { CommonModule::V1::ResponseCodes }
      let(:status_codes) { CommonModule::V1::StatusCodes }

      before(:each) do
        before_actions
        after_actions
      end

      describe 'get_area_by_query' do
        it 'returns list of area successfully' do
          stub_ok_response = {
            payload: {
              results: [
                {
                  title: Faker::Lorem.characters(6)
                }
              ]
            },
            response: response_codes_util::SUCCESS
          }

          search_area_by_query.any_instance.stub(:enact).and_return(stub_ok_response)
          get :get_area_by_query, query: Faker::Lorem.characters(6)
          expect(response).to have_http_status(status_codes::SUCCESS)
          expect(JSON.parse(response.body)['payload'].to_json).to eq(stub_ok_response[:payload].to_json)
        end

        it 'when required parameters are missing' do
          stub_bad_request_response = {
            'error': { message: content_util::INSUFFICIENT_DATA },
            'response': response_codes_util::BAD_REQUEST
          }
          search_area_by_query.any_instance.stub(:enact).and_return(stub_bad_request_response)
          get :get_area_by_query
          expect(response).to have_http_status(status_codes::BAD_REQUEST)
        end
      end
    end
  end
end
