module AddressModule
  module V1
    FactoryGirl.define do
      factory :country, :class => AddressModule::V1::Country do
        name Faker::Address.country
        initials Faker::Lorem.characters(3)
      end
    end
  end
end
