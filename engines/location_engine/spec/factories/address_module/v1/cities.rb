module AddressModule
  module V1
    FactoryGirl.define do
      factory :city, :class => AddressModule::V1::City do
      	association :state, factory: :state
        name Faker::Address.city
        initials Faker::Lorem.characters(3)
      end
    end
  end
end

