module AddressModule
  module V1
    FactoryGirl.define do
      factory :state, :class => AddressModule::V1::State do
      	association :country, factory: :country
        name Faker::Address.state
        initials Faker::Lorem.characters(3)
      end
    end
  end
end
