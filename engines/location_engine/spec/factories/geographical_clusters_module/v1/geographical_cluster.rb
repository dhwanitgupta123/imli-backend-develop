#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    FactoryGirl.define do
      factory :geographical_cluster, :class => GeographicalClustersModule::V1::GeographicalCluster do
        label Faker::Name.name
        level 0
      end
    end
  end
end

