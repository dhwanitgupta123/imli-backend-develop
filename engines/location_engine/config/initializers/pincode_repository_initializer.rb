#
# Initializing pincode index repository
#
elastic_search_url = LOCATION_ENGINE_CONFIG['ELASTIC_SEARCH_URL']
PINCODE_INDEX_REPOSITORY = SearchModule::V1::PincodeRepository.new url: elastic_search_url, log: true