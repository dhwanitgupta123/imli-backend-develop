# Location Engine CONFIG initialization should be done before engine initialization
# Load all environment specific application config variables into LOCATION_ENGINE_CONFIG variable

location_engine_config_file = LocationEngine::Engine.root.join("config", "location_engine_config.yml").to_s

# Global variable LOCATION_ENGINE_CONFIG to store all environment specific config
# This is similar to APP_CONFIG in application
LOCATION_ENGINE_CONFIG = {}

if File.exists?(location_engine_config_file)
  YAML.load_file(location_engine_config_file)[Rails.env].each do |key, value|
    LOCATION_ENGINE_CONFIG[key.to_s] = value.to_s
  end # end YAML.load_file
end # end if
