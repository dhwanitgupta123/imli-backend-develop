#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
    #
    # This worker initiates the pincode repository
    #
    class PincodeRepositoryInitializerWorker < ElasticSearchRepositoryInitializers
 
      PINCODE_INDEX_SERVICE = SearchModule::V1::PincodeIndexService
      #
      # function to initialize pincode repository
      #
      def perform 
        # Initializing pincode index
        pincode_index_service = SearchModule::V1::PincodeIndexService.new
        pincode_index_service.initialize_index
      end
    end
  end
end
