#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for search module
  #
  module V1
  	#
  	# Base class to specify queue intial settings
  	#
    class ElasticSearchRepositoryInitializers
      include Sidekiq::Worker
      sidekiq_options :queue => :default
    end
  end
end
