#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    #
    # GeographicalCluster controller service class to route panel API calls to the GeographicalClusterApi
    #
    class GeographicalClustersController < BaseModule::V1::ApplicationController

      #
      # before actions to be executed
      #
      before_action do
        @deciding_params = CommonModule::V1::ApplicationHelper.get_deciding_params(params)
      end
      #
      # initialize GeographicalClustersController Class
      #
      def initialize
      end

      #
      # function to add geographical_cluster
      #
      # post /add_geographical_cluster
      #
      # Request:: request object contain
      #   * geographical_cluster_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with create geographical_cluster
      #    else returns BadRequest response
      #
      def add_geographical_cluster
        add_geographical_cluster_api = GeographicalClustersModule::V1::AddGeographicalClusterApi.instance(@deciding_params)
        response = add_geographical_cluster_api.enact(geographical_cluster_params)
        send_response(response)
      end

      swagger_controller :geographical_cluster, 'GeographicalClustersModule APIs'
      swagger_api :add_geographical_cluster do
        summary 'It creates the geographical_cluster with given input'
        notes 'create geographical_cluster'
        param :body, :geographical_cluster_request, :geographical_cluster_request, :required, 'create_geographical_cluster request'
        response :bad_request, 'if request params are incorrect or api fails to create geographical_cluster'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :geographical_cluster_request do
        description 'request schema for /create_geographical_cluster API'
        property :geographical_cluster, :geographical_cluster_hash, :required, 'geographical_cluster model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      swagger_model :geographical_cluster_hash do
        description 'details of geographical_cluster'
        property :label, :string, :required, 'label of geographical_cluster'
        property :level, :integer, :required, 'level of geographical_cluster'
        property :child_cluster_ids, :array, :optional, 'array of description',
                 { 'items': { 'type': 'integer' } }
      end

      #
      # function to update geographical_cluster
      #
      # post /update_geographical_cluster
      #
      # Request:: request object contain
      #   * geographical_cluster_params [hash] it contains name
      #
      # Response::
      #   * If action succeed then returns Success Status code with update geographical_cluster
      #    else returns BadRequest response
      #
      def update_geographical_cluster
        update_geographical_cluster_api = GeographicalClustersModule::V1::UpdateGeographicalClusterApi.instance(@deciding_params)
        response = update_geographical_cluster_api.enact(geographical_cluster_params.merge({ id: get_id_from_params }))
        send_response(response)
      end

      swagger_controller :geographical_cluster, 'GeographicalClustersModule APIs'
      swagger_api :update_geographical_cluster do
        summary 'It updates the geographical_cluster with given input'
        notes 'update geographical_cluster'
        param :path, :id, :integer, :required, 'geographical_cluster to update'
        param :body, :update_geographical_cluster_request, :update_geographical_cluster_request, :required, 'create_geographical_cluster request'
        response :bad_request, 'if request params are incorrect or api fails to create geographical_cluster'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      swagger_model :update_geographical_cluster_request do
        description 'request schema for /create_geographical_cluster API'
        property :geographical_cluster, :geographical_cluster_hash, :required, 'geographical_cluster model hash'
        property :session_token, :string, :required, 'to authenticate user'
      end

      #
      # function to get all geographical_clusters
      #
      # GET /get_all_geographical_clusters
      #
      # Response::
      #   * list of geographical_clusters
      #
      def get_filtered_geographical_clusters
        get_geographical_clusters_api = GeographicalClustersModule::V1::GetFilteredGeographicalClustersApi.
                                                                                    instance(@deciding_params)
        response = get_geographical_clusters_api.enact(pagination_params)
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :geographical_cluster, 'GeographicalClustersModule APIs'
      swagger_api :get_filtered_geographical_clusters do
        summary 'It returns details of all the geographical_clusters'
        notes 'get_all_geographical_clusters API returns the details of all the geographical_clusters'
        param :query, :session_token, :string, :required, 'session token to authenticate user'
        param :query, :level, :integer, :optional, 'level of geo cluster'
        param :query, :label, :string, :optional, 'label regex of geo cluster'
        param :query, :page_no, :integer, :optional, 'page no from which to get all geographical_cluster'
        param :query, :per_page, :integer, :optional, 'how many geographical_cluster to display in a page'
        param :query, :state, :integer, :optional, 'to fetch geographical_clusters based on their current state'
        param :query, :sort_by, :string, :optional, 'attribute on which to order the data', { 'default_value': 'label' }
        param :query, :order, :string, :optional, 'ASC or DESC order', { 'default_value': 'ASC'}
        param :query, :childs_info, :boolean, :optional, 'true or false, to return full info of child cluster or not'
        param :query, :available_geographical_clusters, :boolean, :optional, 'to fetch available geo clusters'
        response :bad_request, 'wrong pagination parameters'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end

      #
      # function to get a geographical_cluster
      #
      # GET /geographical_clusters/:id
      #
      # Request::
      #   * geographical_cluster_id [integer] id of geographical_cluster of which need to retrieve
      #
      # Response::
      #   * sends ok response to the panel
      #
      def get_geographical_cluster
        get_geographical_cluster_api = GeographicalClustersModule::V1::GetGeographicalClusterApi.instance(@deciding_params)
        response = get_geographical_cluster_api.enact({ id: get_id_from_params })
        send_response(response)
      end

      #
      # swagger_api to generate JSON Docs to consume RESTful APIs
      # summary: API description
      # notes: Detail description of API
      # pram: param_type, name, type, required, description, hash{}
      #   * para_type:: form, path, query, body
      #   * name:: parameter name
      #   * type:: string, integer, object, array
      #   * required:: if it's a required param otherwise false
      #   * description:: description of parameter
      #   * hash{}:: additional info
      # response: code, description
      #   * code:: response code
      #   * description:: response message
      swagger_controller :geographical_cluster, 'GeographicalClustersModule APIs'
      swagger_api :get_geographical_cluster do
        summary 'It return a geographical_cluster'
        notes 'get_geographical_cluster API return a single geographical_cluster corresponding to the id'
        param :path, :id, :integer, :required, 'geographical_cluster_id to be deleted'
        param :query, :session_token, :string, :required, 'to authenticate user'
        response :bad_request, 'wrong parameters or geographical_cluster not found'
        response :forbidden, 'User does not have permissions to access this page'
        response :unauthorized, 'User not found'
      end


      ###############################
      #       Private Functions     #
      ###############################

      private

      def geographical_cluster_params
        if params[:geographical_cluster].present? && params[:geographical_cluster][:child_cluster_ids].present? &&
          params[:geographical_cluster][:child_cluster_ids].is_a?(String)
          params[:geographical_cluster][:child_cluster_ids] = JSON.parse(params[:geographical_cluster][:child_cluster_ids])
        end
        params.require(:geographical_cluster).permit(:label, :level, :child_cluster_ids => [])
      end


      def get_id_from_params
        return params.require(:id)
      end

      def pagination_params
        general_helper = CommonModule::V1::GeneralHelper
        if params[:childs_info].present?
          params[:childs_info] = general_helper.string_to_boolean(params[:childs_info])
        end
        if params[:available_geographical_clusters].present?
          params[:available_geographical_clusters] = general_helper.
                                                          string_to_boolean(params[:available_geographical_clusters])
        end
        params.permit(:page_no, :per_page, :state, :sort_by, :order, :level, :label, :childs_info,
                      :available_geographical_clusters)
      end
    end
  end
end
