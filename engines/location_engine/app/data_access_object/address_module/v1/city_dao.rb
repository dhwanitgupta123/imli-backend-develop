#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Data Access Object to interact with CityModel
    #
    class CityDao < LocationBaseModule::V1::BaseDao
      def initialize
        super
        @city_model = AddressModule::V1::City
      end

      #
      # Create the city
      #
      # @param args [hash] args contain city_name and state
      #
      # @return [CityObject]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_city = @city_model.new(args)
        begin
          new_city.save!
          return new_city
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # Update the given city with args
      #
      # @param args [hash] args contain city_name and state
      # @param city [Model] city model
      #
      # @return [City] updated city
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, city)
        raise @custom_error_util::InvalidArgumentsError.new('city cannot be null') if city.nil? || args.nil?

        begin
          city.update_attributes!(args)
          return city
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # Find the city by name and state
      #
      # @param name [String] state name
      # @param state_id [Integer] Foreign key
      #
      # @return [CityModel] return city model else nil
      #
      def get_city_by_name_and_state_id(name, state_id)
        @city_model.find_by(name: name, state_id: state_id)
      end
    end
  end
end
