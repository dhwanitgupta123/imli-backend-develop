#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Data Access Object to interact with StateModel
    #
    class StateDao < LocationBaseModule::V1::BaseDao
      def initialize
        super
        @state_model = AddressModule::V1::State
      end

      #
      # Create the state
      #
      # @param args [hash] args contain state_name and country
      #
      # @return [StateModel]
      #
      # @error [InvalidArgumentsError] if it fails to create
      #
      def create(args)
        new_state = @state_model.new(args)
        begin
          new_state.save!
          return new_state
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # Update the given state with args
      #
      # @param args [hash] contain fields and their values, filds may be
      #  state_name or country_id
      # @param state [Model] StateModel
      #
      # @return [StateModel] updated state
      #
      # @error [InvalidArgumentsError] if unable to update the model
      #
      def update(args, state)
        raise @custom_error_util::InvalidArgumentsError.new('state cannot be null') if state.nil? || args.nil?

        begin
          state.update_attributes!(args)
          return state
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new(e.message)
        end
      end

      #
      # Get the state by state_name and country
      #
      # @param name [String] state name
      # @param country_id [Integer] foreign key
      #
      # @return [StateModel] if state present else nil
      #
      def get_state_by_name_and_country_id(name, country_id)
        @state_model.find_by(name: name, country_id: country_id)
      end
    end
  end
end
