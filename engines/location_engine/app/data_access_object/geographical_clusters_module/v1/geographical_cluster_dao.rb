require 'imli_singleton'
#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    #
    # Data Access Object to interact with GeographicalClusterModel
    #
    class GeographicalClusterDao < LocationBaseModule::V1::BaseDao
      include ImliSingleton

      def initialize(params = '')
        @params = params
        @geographical_cluster_model = GeographicalClustersModule::V1::GeographicalCluster
        super
      end

      #
      # create geographical_cluster
      #
      # @param args [Hash] model args
      #
      # @return [Model] geographical_cluster
      #
      def create(args)
        geographical_cluster = @geographical_cluster_model.new(args)
        begin
          geographical_cluster.save!
          return geographical_cluster
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new('Unable to create geographical cluster ,' + e.message)
        end
      end

      #
      # update geographical_cluster
      #
      # @param args [Hash] model args
      # @param geographical_cluster [Model] geographical_cluster
      #
      # @return [Model] geographical_cluster
      #
      def update(args, geographical_cluster)

        begin
          geographical_cluster.update_attributes!(args)
        rescue ActiveRecord::RecordInvalid => e
          raise @custom_error_util::InvalidArgumentsError.new('Unable to update geographical cluster ,' + e.message)
        end
        return geographical_cluster
      end

      #
      # Return geographical_cluster by input id or nil if not present
      #
      # @param id [Integer] geographical_cluster id
      #
      # @return [Model] geographical_cluster
      #
      def get_by_id(id)
        get_model_by_id(@geographical_cluster_model, id)
      end

      #
      # Return geographical_cluster after applying all the pagination_params
      #
      # @param filters [Hash] containg condition of pagination
      #
      # @return [Array] Array of geographical_cluster satisfying filters
      #
      def get_all(pagination_params = {})
        set_pagination_properties(pagination_params, @geographical_cluster_model)
        if pagination_params[:available_geographical_clusters] == true
          geographical_clusters = @geographical_cluster_model.where(parent_cluster_id: nil)
        else
          geographical_clusters = @geographical_cluster_model.all
        end

        page_count = (geographical_clusters.count / @per_page).ceil

        geographical_clusters = geographical_clusters.order(@sort_order).limit(@per_page).
          offset((@page_no - 1) * @per_page)
        return {elements: geographical_clusters, page_count: page_count}
      end

      #
      # Return geographical_cluster after applying all the pagination_params, label and level_id filter
      #
      # @param filters [Hash] containg condition of pagination
      #
      # @return [Array] Array of geographical_cluster satisfying filters
      #
      def get_geographical_clusters_by_label_and_level_id(filters)
        set_pagination_properties(filters, @geographical_cluster_model)

        label = filters[:label]
        level = filters[:level]
        if filters[:available_geographical_clusters] == true
          geographical_clusters = @geographical_cluster_model.where(level: level, parent_cluster_id: nil).
                                                                                        where("label ~* '#{label}'")
        else
          geographical_clusters = @geographical_cluster_model.where(level: level).where("label ~* '#{label}'")
        end

        page_count = (geographical_clusters.count / @per_page).ceil

        geographical_clusters = geographical_clusters.order(@sort_order).limit(@per_page).
                                                                  offset((@page_no - 1) * @per_page)
        return {elements: geographical_clusters, page_count: page_count}
      end

      #
      # Return geographical_cluster after applying all the pagination_params and level filter
      #
      # @param filters [Hash] containg condition of pagination
      #
      # @return [Array] Array of geographical_cluster satisfying filters
      #
      def get_geographical_clusters_by_level(filters)
        set_pagination_properties(filters, @geographical_cluster_model)

        level = filters[:level]

        if filters[:available_geographical_clusters] == true
          geographical_clusters = @geographical_cluster_model.where(level: level, parent_cluster_id: nil)
        else
          geographical_clusters = @geographical_cluster_model.where(level: level)
        end

        page_count = (geographical_clusters.count / @per_page).ceil

        geographical_clusters = geographical_clusters.order(@sort_order).limit(@per_page).
                                                                            offset((@page_no - 1) * @per_page)

        return {elements: geographical_clusters, page_count: page_count}
      end

      #
      # Return geographical_cluster after applying all the pagination_params and label filter
      #
      # @param filters [Hash] containg condition of pagination
      #
      # @return [Array] Array of geographical_cluster satisfying filters
      #
      def get_geographical_clusters_by_label(filters)
        set_pagination_properties(filters, @geographical_cluster_model)

        label = filters[:label]

        if filters[:available_geographical_clusters] == true
          geographical_clusters = @geographical_cluster_model.where("label ~* '#{label}'", parent_cluster_id: nil)
        else
          geographical_clusters = @geographical_cluster_model.where("label ~* '#{label}'")
        end

        page_count = (geographical_clusters.count / @per_page).ceil

        geographical_clusters = geographical_clusters.order(@sort_order).limit(@per_page).
                                                                            offset((@page_no - 1) * @per_page)
        return {elements: geographical_clusters, page_count: page_count}
      end

      #
      # Return parent cluster of root pincode geographical cluster
      #
      # @param pincode [String] pincode to identify area
      #
      # @return [Model] parent geographical cluster
      #
      def self.get_root_geographical_cluster(pincode)

        area_dao = AddressModule::V1::AreaDao.new

        area = area_dao.get_area_by_pincode(pincode)

        geographical_cluster = area.geographical_cluster

        return geographical_cluster.parent_cluster if geographical_cluster.parent_cluster.present?
        return geographical_cluster
      end

      #
      # Return parent cluster of root pincode geographical cluster
      #
      # @param area [Model] area model object
      #
      # @return [Model] parent geographical cluster
      #
      def self.get_root_geographical_cluster_by_area(area)
        geographical_cluster = area.geographical_cluster

        return geographical_cluster.parent_cluster if geographical_cluster.parent_cluster.present?
        return geographical_cluster
      end


    end
  end
end
