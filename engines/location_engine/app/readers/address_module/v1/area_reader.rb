#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Reader class for area model
    #
    class AreaReader
      def initialize(area)
        @area_model = area if area.present?
        @city_model = @area_model.city if @area_model.present?
        @state_model = @city_model.state if @city_model.present?
        @country_model = @state_model.country if @state_model.present?
      end

      def get_area_id
        return @area_model.id
      end

      def get_pincode
        return @area_model.pincode if @area_model.present?
      end

      def get_area
        return @area_model.name if @area_model.present?
      end

      def get_area_model
        return @area_model if @area_model.present?
      end

      def get_city
        return @city_model.name if @city_model.present?
      end

      def get_city_model
        return @city_model if @city_model.present?
      end

      def get_state
        return @state_model.name if @state_model.present?
      end

      def get_state_model
        return @state_model if @state_model.present?
      end

      def get_country
        return @country_model.name if @country_model.present?
      end

      def get_country_model
        return @country_model if @country_model.present?
      end

      def get_status
        return @area_model.status if @area_model.present?
      end
    end
  end
end