#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Reader class for pincode model
    #
    class PincodeModelReader
      def initialize(results)
        @results = results
      end

      #
      # Map pincode result to array of attributes hash
      #
      # @return [Array] array of pincode attributes
      #
      def get_attribute_array
        attributes = []
        @results.each do |result|
          attributes.push(result.attributes)
        end
        attributes
      end
    end
  end
end
