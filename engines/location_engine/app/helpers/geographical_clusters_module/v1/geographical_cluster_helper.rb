
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    module GeographicalClusterHelper

      #
      # Validate level constrains of geographical_cluster
      #
      def self.validate_level_constrains(geographical_cluster)

        current_level = geographical_cluster.level

        if current_level == 0
          zero_level_validation(geographical_cluster)
        else
          level_validation(geographical_cluster)
        end
      end

      #
      # Validate level where geographical_cluster level > 0
      #
      def self.level_validation(geographical_cluster)

        current_level = geographical_cluster.level

        child_clusters = geographical_cluster.child_clusters

        child_clusters.each do |cluster|
          return false if cluster.level >= current_level
        end

        return true
      end

      #
      # Validate level where geographical_cluster level == 0
      #
      def self.zero_level_validation(geographical_cluster)
        return false if  geographical_cluster.child_clusters.present?
        return true
      end

      #
      # Fetch parent cluster of root pincode geographical cluster
      #
      # @param area [Model] area model object
      #
      # @return [Model] parent geographical cluster
      #
      def self.get_root_geographical_cluster_by_area(area)
        GeographicalClustersModule::V1::GeographicalClusterDao.get_root_geographical_cluster_by_area(area)
      end

    end #End of class
  end
end
