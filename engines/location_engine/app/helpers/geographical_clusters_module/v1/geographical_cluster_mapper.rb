#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    #
    # Map geographical_cluster object to Json
    #
    module GeographicalClusterMapper

      #
      # map geographical cluster to hash
      #
      def self.map_geographical_cluster_to_hash(geographical_cluster, child_cluster_info = false)
        time_slot_dao = MarketplaceOrdersModule::V1::TimeSlotDao
        time_slot_mapper = MarketplaceOrdersModule::V1::TimeSlotMapper

        non_deleted_time_slots = time_slot_dao.get_non_deleted_time_slots(geographical_cluster.time_slots)

        if child_cluster_info == true
          child_clusters = get_child_cluster_array(geographical_cluster)
          time_slots = time_slot_mapper.map_time_slots_to_array(non_deleted_time_slots)
        else
          child_clusters = geographical_cluster.child_cluster_ids
          time_slots = non_deleted_time_slots.present? ? non_deleted_time_slots.ids : []
        end

        active_time_slots = time_slot_dao.get_active_time_slots(geographical_cluster.time_slots)

        has_active_time_slots = active_time_slots.present? ? true : false

        {
          id: geographical_cluster.id,
          level: geographical_cluster.level,
          label: geographical_cluster.label,
          child_clusters: child_clusters,
          time_slots: time_slots,
          has_active_time_slots: has_active_time_slots
        }
      end

      #
      # return child cluster array of geographical cluster
      #
      def self.get_child_cluster_array(geographical_cluster)
        child_clusters = []
        return child_clusters if geographical_cluster.blank? || geographical_cluster.child_clusters.blank?

        geographical_cluster.child_clusters.each do |child_cluster|
          child_clusters.push(get_short_cluster_hash(child_cluster))
        end

        return child_clusters
      end

      #
      # return short cluster hash of geographical cluster
      #
      def self.get_short_cluster_hash(geographical_cluster)
        {
          id: geographical_cluster.id,
          level: geographical_cluster.level,
          label: geographical_cluster.label,
          child_cluster_ids: geographical_cluster.child_cluster_ids
        }
      end

      #
      # map geographical_clusters to array
      #
      def self.map_geographical_clusters_to_array(geographical_clusters, child_cluster_info = false)

        geographical_clusters = geographical_clusters.eager_load(:time_slots, :child_clusters)
        geographical_cluster_array = []
        geographical_clusters.each do |geographical_cluster|
          geographical_cluster_array.push(map_geographical_cluster_to_hash(geographical_cluster, child_cluster_info))
        end
        geographical_cluster_array
      end

      #
      # map geographical_cluster to json response
      #
      def self.add_geographical_cluster_api_response_hash(geographical_cluster)
        return {
          geographical_cluster: map_geographical_cluster_to_hash(geographical_cluster, true)
        }
      end

      #
      # map geographical_clusters to json response
      #
      def self.get_geographical_clusters_api_response_hash(response, child_cluster_info = false)
        return {
          geographical_clusters: map_geographical_clusters_to_array(response[:elements], child_cluster_info),
          page_count: response[:page_count]
        }
      end

      #
      # map geographical_cluster to json response
      #
      def self.get_geographical_cluster_api_response_hash(geographical_cluster)
        return {
          geographical_cluster: map_geographical_cluster_to_hash(geographical_cluster, true)
        }
      end

      #
      # map geographical_cluster to json response
      #
      def self.update_geographical_cluster_api_response_hash(geographical_cluster)
        return {
          geographical_cluster: map_geographical_cluster_to_hash(geographical_cluster, true)
        }
      end

    end
  end
end
