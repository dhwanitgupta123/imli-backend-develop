#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Helpers for address_module
    #
    module AddressHelper
      PINCODE_REGEX = CommonModule::V1::RegexUtil::PINCODE_REGEX
      ADDRESS_RESPONSE_DECORATOR = AddressModule::V1::AddressResponseDecorator
      ADDRESS_STATES = AddressModule::V1::ModelStates::V1::AddressStates

      #
      # Validates if pincode have digits only and length should be equal to 6
      #
      # @param pincode [String]
      #
      # @return [Boolean] true if valid pincode else false
      #
      def self.valid_pincode?(pincode)
        return true if (pincode.length == 6) && (pincode =~ PINCODE_REGEX).present?
        false
      end

      # 
      # Create response with all the addresses
      # 
      def self.get_addresses_hash(addresses)
        addresses_hash = []
        return addresses_hash if addresses.blank?
        addresses.each do |address|
          address = ADDRESS_RESPONSE_DECORATOR.create_address_hash(address)
          addresses_hash.push(address)
        end
        return addresses_hash
      end

      # 
      # Create response with only filtered ACTIVE addresses
      # 
      def self.get_active_addresses_hash(addresses)
        addresses = addresses.where(status: ADDRESS_STATES::ACTIVE)
        return get_addresses_hash(addresses)
      end

      #
      # Get zone for which address belongs to
      #
      # @param address [Model] [Address model object]
      #
      # @return [String] [label of the zone in which address belongs to]
      #
      def self.get_zone_of_address(address)
        return '' if address.blank?
        area = address.area
        geographical_cluster = GeographicalClustersModule::V1::GeographicalClusterHelper.get_root_geographical_cluster_by_area(area)
        return geographical_cluster.present? ? geographical_cluster.label : ''
      end

    end #End of class
  end
end
