#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class StateService < BaseModule::V1::BaseService
      def initialize
        super
        @state_dao = AddressModule::V1::StateDao
        @country_dao = AddressModule::V1::CountryDao
      end

      #
      # Function check pre-existence of city, if not then creates the city
      # then checks for the pre-existing state with same state_name and country_id
      # if exists then return the state else creates the state
      #
      # @param state_name [String] state name
      #
      # @param country_name [String] country name
      #
      # @return [StateObject]
      #
      # @error [InvalidArgumentsError] if it fails to create country or state
      #
      def create_state(state_name, country_name)
        state_dao_class = @state_dao.new

        country = get_country(country_name)

        state = state_dao_class.get_state_by_name_and_country_id(state_name, country.id)

        if state.nil?
          begin
            state = state_dao_class.create name: state_name, country: country
          rescue @custom_error_util::InvalidArgumentsError => e
            raise e
          end
        end

        return state
      end

      private

      #
      # This function checks if country with county_name exists of not
      # if it exists then return the country else first create the country
      # then return it
      # 
      # @param country_name [String] country name which is unique for country model
      #
      def get_country(country_name)
        country_dao_class = @country_dao.new

        country = country_dao_class.get_country_by_name(country_name)

        if country.nil?
          begin
            country = country_dao_class.create(country_name)
          rescue @custom_error_util::InvalidArgumentsError => e
            raise e
          end
        end
        return country
      end
    end
  end
end
