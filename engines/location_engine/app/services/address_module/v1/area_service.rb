  #
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Logic layer for area specific queries
    #
    class AreaService < BaseModule::V1::BaseService
      def initialize
        super
        @area_dao = AddressModule::V1::AreaDao
        @area_reader = AddressModule::V1::AreaReader
        @city_service = AddressModule::V1::CityService
        @pincode_index_service = SearchModule::V1::PincodeIndexService

      end

      #
      # Function call the area_dao to get the area from pincode
      #
      # @param pincode [String] pincode (consisting of 6 digits)
      #
      # @return [AreaReaderObject] this object retrieves the fields from complex object area
      #
      # @error [RecordNotFoundError] when area for corresponding pincode doesn't exists
      #
      def get_area_from_pincode(pincode)
        area_dao_class = @area_dao.new

        begin
          area = area_dao_class.get_area_by_pincode(pincode)
        rescue @custom_error_util::RecordNotFoundError => e
          raise e
        end
        @area_reader.new(area)
      end

      #
      # Function call the area_dao to get the area from area_id
      #
      # @param area_id [String] existing area_id
      #
      # @return area [Object] object of area model
      #
      # @error [RecordNotFoundError] when area for corresponding area_id doesn't exists
      #
      def get_area_by_id(area_id)
        area_dao_class = @area_dao.new

        begin
          area = area_dao_class.get_area_by_id(area_id)
        rescue @custom_error_util::RecordNotFoundError => e
          raise e
        end
      end

      #
      # Transactional Function call the area_service to update area
      #
      # @param id [String] existing area_id
      #
      # @return area [Object] object of area model
      #
      # Rolls back if exception is thrown
      #
      def transactional_update_area(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return update_area(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Function call the area_dao to update area
      #
      # @param id [String] existing area_id
      #
      # @return area [Object] object of area model
      # 
      def update_area(args)
        area_dao_class = @area_dao.new
        pincode_index_service_class = @pincode_index_service.new

        area_id = args[:id]

        area = get_area_by_id(area_id)
        area = area_dao_class.update({name: args[:name]}, area)

        pincode_index_service_class.update_document(area)
        return area
      end

      # 
      # This function call create area within transaction block so if anything fails
      # it rollback all the previous commits
      #
      # @param args [Hash] consists of pincode, area, city, state, country, is_active
      #
      # @return [AreaReaderObject] this object retrieves the fields from complex object area
      #
      def transactional_create_area(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return create_area(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Function check if area corresponding to given pincod exists or not if not then create
      # 
      # city using city service and then create area using city
      #
      # @param request [Hash] consists of pincode, area, city, state, country, is_active
      #
      # @return [AreaReaderObject] this object retrieves the fields from complex object area
      #
      # @error [RecordAlreadyExistsError] when area for corresponding pincode doesn't exists
      #
      # @error [InvalidArgumentsError] when action at dao fails due to invalid arguments
      #
      def create_area(request)
        @area_dao_class = @area_dao.new
        pincode = request[:pincode]

        # as pincode is unique property therefore checking if area already exists or not
        validate_area_already_exists(pincode)
        pincode_index_service_class = @pincode_index_service.new

        city = get_city(request[:city], request[:state], request[:country])

        begin
          area = @area_dao_class.create(
            { 
              pincode: pincode,
              name: request[:area],
              city: city,
              status: request[:status]
            }
          )
        rescue @custom_error_util::InvalidArgumentsError => e
          raise e
        end

        pincode_index_service_class.add_document(area)
        @area_reader.new(area)
      end

      # 
      # This function call change area status within transaction block so if anything fails
      # it rollback all the previous commits
      #
      # @param args [Hash] consists of pincode, event
      #
      # @return [AreaReaderObject] this object retrieves the fields from complex object area
      #
      def transactional_change_area_status(args)
        #Transaction block to make sure that the block rolls back if anything fails in the block
        transactional_function = Proc.new do |args|
          return change_area_status(args)
        end
        transaction_block = TransactionHelper.new({
          function: transactional_function,
          args: args
          })
        transaction_block.run();
      end

      #
      # Function change state if area corresponding to given pincode exists
      # 
      # @param request [Hash] consists of pincode, event
      #
      # @return [AreaObject] updated Area Object
      #
      # @error [RecordNotFoundError] when area for corresponding pincode doesn't exists
      #
      # @error [InvalidArgumentsError] when event at dao fails due to invalid arguments
      # 
      # @error [PreConditionRequiredError] when the event is invalid at dao
      #
      def change_area_status(request)
        @area_dao_class = @area_dao.new
        pincode = request[:pincode]
        event = request[:event]

        area_reader = get_area_from_pincode(pincode)
        area = area_reader.get_area_model

        pincode_index_service_class = @pincode_index_service.new

        updated_area = @area_dao_class.change_state(area, event)
        pincode_index_service_class.update_area_status(updated_area.id, updated_area.status)
        return updated_area   
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # This function check if area with given pincode exists or not
      #
      # return true if area with given pincode doesn't exists else raise error
      # RecordAlreadyExistsError
      #
      def validate_area_already_exists(pincode)
        begin
          @area_dao_class.get_area_by_pincode(pincode)
        rescue @custom_error_util::RecordNotFoundError => e
          return true
        end

        raise @custom_error_util::RecordAlreadyExistsError.new('Area with pincode ' + pincode + ' already exists')
      end

      #
      # This function return the city, it calls the city_service.create_city
      # function which first verify if city with state and country exists or not
      # if exists then return the city else first create the city and then returns city
      #
      # @param city [String] city name
      # @param state [String] state name
      # @param country [String] country name
      #
      # @return city [CityObject] returns the city with given city name, state name and country name
      #
      # @error [InvalidArgumentsError] if any of the arg in blank ( arg.blank? == true )
      #
      def get_city(city, state, country)
        begin
          city_service_class = @city_service.new
          city_service_class.create_city(city, state, country)
        rescue @custom_error_util::InvalidArgumentsError => e
          raise e
        end
      end
    end
  end
end
