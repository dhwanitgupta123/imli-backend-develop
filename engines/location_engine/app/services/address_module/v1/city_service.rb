#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Logic layer for city specific queries
    #
    class CityService < BaseModule::V1::BaseService
      def initialize
        super
        @city_dao = AddressModule::V1::CityDao
        @state_service = AddressModule::V1::StateService
      end

      #
      # Function first retrieve the state and then check for the
      # existing city, if it doesn't exist then creates the city
      #
      # @param city_name [String] city name
      # @param state_name [String] state name
      # @param country_name [String] country name
      #
      # @return [CityObject] city object
      #
      # @error [InvalidArgumentsError] if it fails to create city
      #
      def create_city(city_name, state_name, country_name)
        city_dao_class = @city_dao.new

        state = get_state(state_name, country_name)

        city = city_dao_class.get_city_by_name_and_state_id(city_name, state.id)
        if city.nil?
          begin
            city = city_dao_class.create name: city_name, state: state
          rescue @custom_error_util::InvalidArgumentsError => e
            raise e
          end
        end
        return city
      end

      ###############################
      #       Private Functions     #
      ###############################

      private

      #
      # This function return the state, it calls state_service.create_state
      # which checks if state with country name exists or not if exists then it returns
      # the state else create the state and then return it
      #
      # @param state_name [String] state name
      # @param country_name [String] country name
      #
      # @return [StateObject] return state with given state name and country name
      #
      # @error [InvalidArgumentsError] if given arguments are invalid like coutry_nam nil or state_name nil
      #
      def get_state(state_name, country_name)
        state_service_class = @state_service.new

        begin
          state_service_class.create_state(state_name, country_name)
        rescue @custom_error_util::InvalidArgumentsError => e
          raise e
        end
      end
    end
  end
end
