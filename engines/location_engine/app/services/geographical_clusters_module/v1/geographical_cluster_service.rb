require 'imli_singleton'
#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    #
    # Logic layer for state specific queries
    #
    class GeographicalClusterService < LocationBaseModule::V1::BaseService
      include ImliSingleton

      def initialize(params='')
        @params = params
        @geographical_cluster_dao = GeographicalClustersModule::V1::GeographicalClusterDao.instance(@params)
        super
      end

      #
      # create geographical_cluster
      #
      # @param args [Hash] model args
      #
      # @return [Model] geographical_cluster
      #
      _RunInTransactionBlock_
      def create_geographical_cluster(args)
        @geographical_cluster_dao.create(args)
      end

      #
      # validate if geographical_cluster exist and then update geographical_cluster
      #
      # @param args [Hash] model args
      #
      # @return [Model] geographical_cluster
      #
      _RunInTransactionBlock_
      def update_geographical_cluster(args)
        update_model(args, @geographical_cluster_dao, 'GeographicalCluster')
      end

      #
      # Return geographical_cluster by input id or nil if not present
      #
      # @param id [Integer] geographical_cluster id
      #
      # @return [Model] cluster
      #
      def get_geographical_cluster_by_id(id)
        validate_if_exists(id, @geographical_cluster_dao, 'GeographicalCluster')
      end

      #
      # Return array of geographical_cluster satisfying pagination conditions
      # according to params it call specific queries and paginate
      # it at same time
      #
      # @param pagination_params [Hash] conditions
      #
      # @return [Array] array of geographical_clusters
      #
      def get_geographical_clusters_by_filters(args)
        if args[:level].present? && args[:label].present?
          @geographical_cluster_dao.get_geographical_clusters_by_label_and_level_id(args)
        elsif args[:level].present?
          @geographical_cluster_dao.get_geographical_clusters_by_level(args)
        elsif args[:label].present?
          @geographical_cluster_dao.get_geographical_clusters_by_label(args)
        else
          get_all_elements(@geographical_cluster_dao, args)
        end
      end
    end
  end
end
