module LocationBaseModule
  module V1
    class BaseService
      extend AnnotationsModule::V1::TransactionalBlockAnnotations

      def initialize(params = '')
        self.extend AnnotationsModule::V1::TransactionalBlockAnnotations
        @custom_error_util = CommonModule::V1::CustomErrors
      end

      #
      # This funtion call the get_by_id function of Dao
      #
      # @param id [Integer] id of the model
      # @param dao [DaoObject] DAO of which get_by_id function to call
      # @param message [String] custom message
      #
      # @return [Object] object if it find the record
      #
      # @error [InvalidArgumentsError] if record not found
      #
      def validate_if_exists(id, dao, message='')
        message = "Record " if message.blank?
        model = dao.get_by_id(id)
        raise @custom_error_util::InvalidArgumentsError.new(message + ' not found') if model.nil?

        return model
      end

      #
      # This funtion call the update function of Dao
      #
      # @param args [Hash] contains corresponding argument to update model
      # @param dao [DaoObject] DAO of which update function to call
      # @param message [String] custom message
      #
      # @return [Object] object if it succeed to update else pass the error raised by DAO
      #
      def update_model(args, dao, message='')
        if message.blank?
          model = validate_if_exists(args[:id], dao)
        else
          model = validate_if_exists(args[:id], dao, message)
        end

        dao.update(args, model)
      end

      #
      # This funtion call the get_all function of Dao
      #
      # @param dao [DaoObject] DAO of which get_by_id function to call
      #
      # @return [Array] array of model
      #
      def get_all_elements(dao, pagination_params)
        return dao.get_all(pagination_params)
      end

    end
  end
end
