#
# Module to handle all the functionalities related to search
#
module SearchModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Pincode class with instance valriable attributes hash
    #
    class Pincode
      attr_reader :attributes

      def initialize(attributes = {})
        @attributes = attributes
      end

      def to_hash
        @attributes
      end
    end
  end
end
