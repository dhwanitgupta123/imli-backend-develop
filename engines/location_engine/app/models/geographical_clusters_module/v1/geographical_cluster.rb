module GeographicalClustersModule
  module V1
    class GeographicalCluster < BaseModule::BaseModel

      belongs_to :parent_cluster, class_name: 'GeographicalClustersModule::V1::GeographicalCluster'
      has_many :child_clusters, class_name: 'GeographicalClustersModule::V1::GeographicalCluster',
                                foreign_key: 'parent_cluster_id'

      has_one :area, class_name: 'AddressModule::V1::Area'

      has_many :time_slots, class_name: 'MarketplaceOrdersModule::V1::TimeSlot'

      validate :before_save_validations

      #
      # validates child geoclusters level
      #
      def before_save_validations

        if self.level > 0 && self.child_cluster_ids.blank?
          errors.add(:base, 'Sub zones cannot be blank')
          return false
        end

        geographical_cluster_helper = GeographicalClustersModule::V1::GeographicalClusterHelper
        mutex = Mutex.new
        mutex.synchronize {
          if geographical_cluster_helper.validate_level_constrains(self) == false
            errors.add(:level, ', child cluster level should be less than current cluster')
            return false
          else
            return true
          end
        }
      end
    end
  end
end
