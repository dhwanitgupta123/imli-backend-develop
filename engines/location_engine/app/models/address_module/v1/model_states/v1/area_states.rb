#
# Module to handle all the functionalities related to address
# 
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Module to keep information of area state
    #
    module ModelStates
      module V1
        #
        # AreaStates has enums corresponding to the possible states of area model
        #
        class AreaStates
          ACTIVE = 1
          INACTIVE = 2
          ALL = 3
          ALLOWED_STATES = [1,2,3]
        end
      end
    end
  end
end
