#
# Module to handle all the functionalities related to address
# 
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Module to keep information of area state
    #
    module ModelStates
      module V1
        #
        # AddressStates has enums corresponding to the possible states of area model
        #
        class AddressStates
          ACTIVE = 1
          INACTIVE = 2
          DELETED = 3

          def self.get_address_status_hash_by_id(state)
            return {} if state.blank?
            label = ''
            case state.to_i
            when ACTIVE
              label = 'Active'
            when INACTIVE
              label = 'Inactive'
            when DELETED
              label = 'Deleted'
            end
            return {id: state, label: label}
          end

        end
      end
    end
  end
end
