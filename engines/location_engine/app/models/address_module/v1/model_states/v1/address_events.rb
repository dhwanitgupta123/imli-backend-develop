#
# Module to handle all the functionalities related to address
# 
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # Module to keep information of area events
    #
    module ModelStates
      module V1
        #
        # AreaStates has enums corresponding to the possible states of area model
        #
        class AddressEvents
          ACTIVATE    = 1
          DEACTIVATE  = 2
          SOFT_DELETE = 3
        end
      end
    end
  end
end