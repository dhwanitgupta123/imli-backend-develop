#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    #
    # Helper class to create JsonResponse
    #
    class GeographicalClusterResponseDecorator < BaseModule::V1::BaseResponseDecorator

      def self.create_geographical_cluster_response(geographical_cluster)
        {
          payload: {
            geographical_cluster: geographical_cluster[:geographical_cluster]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end

      def self.create_geographical_clusters_array_response(response)
        {
          payload: {
            geographical_clusters: response[:geographical_clusters]
          },
          meta: {
            page_count: response[:page_count]
          },
          response: RESPONSE_CODES_UTIL::SUCCESS
        }
      end
    end
  end
end
