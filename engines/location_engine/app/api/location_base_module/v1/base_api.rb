#
# Module to handle all the functionalities related to API
#
module LocationBaseModule
  #
  # Version1 for address module
  #
  module V1
    #
    # BaseApi to inject basic funcionalities
    #
    class BaseApi
      extend AnnotationsModule::V1::ExceptionHandlerAnnotations
      def initialize(params='')
        @custom_error_util = CommonModule::V1::CustomErrors
        @address_response_decorator = AddressModule::V1::AddressResponseDecorator
        @content = CommonModule::V1::Content
        self.extend AnnotationsModule::V1::ExceptionHandlerAnnotations
      end
    end
  end
end
