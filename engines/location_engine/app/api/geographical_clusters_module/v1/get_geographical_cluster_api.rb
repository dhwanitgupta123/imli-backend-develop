require 'imli_singleton'
#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    #
    # AddGeographicalCluster api, it validates the request, call the
    # geographical_cluster service and return the JsonResponse
    #
    class GetGeographicalClusterApi < LocationBaseModule::V1::BaseApi
      include ImliSingleton


      def initialize(params = '')
        @params = params
        super
      end

      #
      # Return geographical_cluster corresponding to id
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)

        geographical_cluster_response_decorator = GeographicalClustersModule::V1::GeographicalClusterResponseDecorator
        geographical_cluster_mapper = GeographicalClustersModule::V1::GeographicalClusterMapper
        geographical_cluster_service = GeographicalClustersModule::V1::GeographicalClusterService.instance(@params)

        validate_request(request)
        geographical_cluster = geographical_cluster_service.get_geographical_cluster_by_id(request[:id])
        response = geographical_cluster_mapper.get_geographical_cluster_api_response_hash(geographical_cluster)
        return geographical_cluster_response_decorator.create_geographical_cluster_response(response)

      end

      private

      #
      # Validates the required params geographical_cluster name
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request[:id].blank?
          raise  @custom_error_util::InsufficientDataError.new(@content::FIELD_MUST_PRESENT % {field: 'Geo Cluster ID'})
        end
      end
    end
  end
end
