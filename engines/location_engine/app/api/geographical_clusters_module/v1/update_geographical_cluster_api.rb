require 'imli_singleton'
#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    #
    # AddGeographicalCluster api, it validates the request, call the
    # geographical_cluster service and return the JsonResponse
    #
    class UpdateGeographicalClusterApi < LocationBaseModule::V1::BaseApi
      include ImliSingleton

      def initialize(params = '')
        @params = params
        super
      end

      #
      # Function takes input corresponding to update geographical_cluster and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have geographical_cluster arguments to update
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)

        geographical_cluster_response_decorator = GeographicalClustersModule::V1::GeographicalClusterResponseDecorator
        geographical_cluster_mapper = GeographicalClustersModule::V1::GeographicalClusterMapper
        geographical_cluster_service = GeographicalClustersModule::V1::GeographicalClusterService.instance(@params)

        validate_request(request)
        geographical_cluster = geographical_cluster_service.update_geographical_cluster(request)
        response = geographical_cluster_mapper.update_geographical_cluster_api_response_hash(geographical_cluster)
        return geographical_cluster_response_decorator.create_geographical_cluster_response(response)

      end

      private

      #
      # Validates the required params geographical_cluster name
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request[:id].blank?
          raise  @custom_error_util::InsufficientDataError.new(@content::FIELD_MUST_PRESENT % {field: 'Geo Cluster ID'})
        end
      end
    end
  end
end
