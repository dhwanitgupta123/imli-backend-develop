require 'imli_singleton'
#
# Module to handle all the functionalities related to GeographicalClusters
#
module GeographicalClustersModule
  #
  # Version1 for GeographicalClusters module
  #
  module V1
    #
    # AddGeographicalCluster api, it validates the request, call the
    # geographical_cluster service and return the JsonResponse
    #
    class GetFilteredGeographicalClustersApi < LocationBaseModule::V1::BaseApi
      include ImliSingleton


      def initialize(params = '')
        @params = params
        super
      end

      #
      # Return geographical_cluster corresponding to filters
      #
      # @return [JsonResponse]
      #
      _ExceptionHandler_
      def enact(request)

        geographical_cluster_response_decorator = GeographicalClustersModule::V1::GeographicalClusterResponseDecorator
        geographical_cluster_mapper = GeographicalClustersModule::V1::GeographicalClusterMapper
        geographical_cluster_service = GeographicalClustersModule::V1::GeographicalClusterService.instance(@params)

        geographical_clusters = geographical_cluster_service.get_geographical_clusters_by_filters(request)

        child_cluster_info = false
        child_cluster_info = true if request[:childs_info].present? && request[:childs_info] == true

        response = geographical_cluster_mapper.get_geographical_clusters_api_response_hash(geographical_clusters,
                                                                                                    child_cluster_info)
        return geographical_cluster_response_decorator.create_geographical_clusters_array_response(response)

      end
    end
  end
end
