#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # AddArea api, it validates the request, call the
    # area service and return the JsonResponse
    #
    class AddArea < LocationBaseModule::V1::BaseApi
      def initialize
        super
        @address_helper = AddressModule::V1::AddressHelper
        @area_service = AddressModule::V1::AreaService
      end

      #
      # Function takes input corresponding to create the area and returns the
      # success or error response
      #
      # @param request [Hash] Request object, it should have pincode, area,
      #   city, state, country and status (optional)
      #
      # @return [JsonResponse]
      #
      def enact(request)
        area_service_class = @area_service.new

        begin
          validate_request(request)
          area_reader = area_service_class.transactional_create_area(request)
          return @address_response_decorator.create_ok_response(area_reader)
        rescue @custom_error_util::RecordAlreadyExistsError, @custom_error_util::InvalidArgumentsError => e
          return @address_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue @custom_error_util::RunTimeError => e
          return @address_response_decorator.create_response_runtime_error(e.message)
        end
      end

      private

      #
      # Validates the required params and pincode format
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank? || request[:pincode].blank? || request[:area].blank? || request[:city].blank? || request[:state].blank? || request[:country].blank?
          raise @custom_error_util::InvalidArgumentsError.new('Request should contain pincode, area, city, state and country')
        end

        raise @custom_error_util::InvalidArgumentsError.new('Invalid pincode, pincode should be of 6 digits') unless @address_helper.valid_pincode?(request[:pincode])
      end
    end
  end
end
