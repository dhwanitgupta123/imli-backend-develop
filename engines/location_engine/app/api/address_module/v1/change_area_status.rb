#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # ChangeAreaStatus api, it validates the request, call the
    # area service and return the JsonResponse
    #
    class ChangeAreaStatus < LocationBaseModule::V1::BaseApi
      def initialize(params)
        super
        @params = params
        @address_helper = AddressModule::V1::AddressHelper
        @area_service = AddressModule::V1::AreaService
      end

      #
      # Function takes input area id and changes area status
      # success or error response
      #
      # @param request [Hash] Request object, it should have id
      #
      # @return [JsonResponse]
      #
      def enact(request)
        area_service_class = @area_service.new

        begin
          validate_request(request)
          area = area_service_class.transactional_change_area_status(request)
          return @address_response_decorator.create_area_details_response(area)
        rescue @custom_error_util::InvalidArgumentsError => e
          return @address_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue @custom_error_util::RecordNotFoundError, @custom_error_util::InvalidArgumentsError => e
          return @address_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue @custom_error_util::PreConditionRequiredError => e
          return @address_response_decorator.create_response_pre_condition_required(e.message)
        rescue @custom_error_util::RunTimeError => e
          return @address_response_decorator.create_response_runtime_error(e.message)
        end
      end

      private

      #
      # Validates the required params
      #
      # @param request [Hash]
      #
      # @error [InvalidArgumentsError] if given request not valid
      #
      def validate_request(request)
        if request.blank?
          raise @custom_error_util::InvalidArgumentsError.new(@content::INSUFFICIENT_DATA)
        end
        if request[:event].blank?
          raise @custom_error_util::InvalidArgumentsError.new(@content::MISSING_EVENT_PARAM)
        end
        if request[:pincode].blank?
          raise @custom_error_util::InvalidArgumentsError.new(@content::MISSING_PINCODE_PARAM)
        end
        if !@address_helper.valid_pincode?(request[:pincode])
          raise @custom_error_util::InvalidArgumentsError.new(@content::INVALID_PINCODE)
        end
      end
    end
  end
end