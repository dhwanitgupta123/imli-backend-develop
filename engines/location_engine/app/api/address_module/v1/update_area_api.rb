#
# Module to handle all the functionalities related to address
#
module AddressModule
  #
  # Version1 for address module
  #
  module V1
    #
    # UpdateArea api, it validates the request, call the
    # area service and return the JsonResponse
    #
    class UpdateAreaApi < LocationBaseModule::V1::BaseApi
      def initialize(params)
        super
        @params = params
        @address_helper = AddressModule::V1::AddressHelper
        @area_service = AddressModule::V1::AreaService
      end

      def enact(request)
        area_service_class = @area_service.new

        begin
          validate_request(request)
          area = area_service_class.transactional_update_area(request)
          return @address_response_decorator.create_area_details_response(area)
        rescue @custom_error_util::RecordNotFoundError, @custom_error_util::InvalidArgumentsError => e
          return @address_response_decorator.create_response_invalid_arguments_error(e.message)
        rescue @custom_error_util::RunTimeError => e
          return @address_response_decorator.create_response_runtime_error(e.message)
        end
      end

      private

      #
      # validate the request params
      #
      # @param request [hash] request
      #
      # @error [InvalidArgumentsError] raise error if request is not valid
      #
      def validate_request(request)
        if request.blank? || request[:id].blank?
          raise @custom_error_util::InvalidArgumentsError.new(@content::INSUFFICIENT_DATA)
        end
      end
    end
  end
end


