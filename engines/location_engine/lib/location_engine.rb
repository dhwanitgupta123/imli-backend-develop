require 'location_engine/engine'
#
# Deals with all location related functionalities
#
module LocationEngine
  # Don't have prefix method return anything.
  # This will keep Rails Engine from generating all table prefixes with the engines name
  def self.table_name_prefix
    # Suppose if this function return XYZ then Model file ABC search for the table XYZ_ABC
    # But we dont want to add prefix in table name hence return noothing
  end
end
