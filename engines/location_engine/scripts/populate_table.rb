require 'csv'
#
# This class reads the CSV file and populate the corresponding table
#
class PopulateTable
  RESPONSE_CODES_UTIL = ::CommonModule::V1::ResponseCodes

  def populate_area_from_csv(csv_file = File.join(File.dirname(__FILE__), '/data/area.csv'))
    add_area_api = AddressModule::V1::AddArea.new

    CSV.foreach(csv_file) do |row|
      next if row[2].downcase != 'mumbai'
      request = create_request(row)
      add_area_api.enact(request)
    end
  end

  def create_request(row)
    request = {}
    request[:pincode] = row[0]
    request[:area] = row[1]
    request[:city] = row[2]
    request[:state] = row[3]
    request[:country] = 'India'
    request[:status] = row[4].to_i
    return request
  end
end
