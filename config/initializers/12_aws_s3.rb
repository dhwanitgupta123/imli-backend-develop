# S3 configurations for Image Uploading functionality
puts '--> Initializing S3 bucket configurations'

S3_PRIVATE_FOLDER = APP_CONFIG['config']['S3_PRIVATE_FOLDER']

S3  = AWS::S3.new(
  :access_key_id => APP_CONFIG['config']['AWS_ACCESS_KEY_ID'],
  :secret_access_key => APP_CONFIG['config']['AWS_SECRET_ACCESS_KEY'],
  :s3_endpoint => APP_CONFIG['config']['S3_HOST_NAME']
)

S3_BUCKET = S3.buckets[APP_CONFIG['config']['S3_BUCKET']]

begin
S3_BUCKET.exists?
rescue => e
puts '[ERROR]: S3 Bucket failed to initialize due to the following error ' + e.message
end

if Rails.env == 'production'

  S3_IMAGE_STYLES = {
    default: {
      xxxxhdpi_large: { geometry: '1720x1720>', convert_options: '-quality 80' },
      # xxxxhdpi_medium: -> use hdpi_large
      # xxxxhdpi_small:  -> use xhdpi_medium
      xxxhdpi_large: { geometry: '1376x1376>', convert_options: '-quality 80' },
      xxxhdpi_medium: { geometry: '460x460>', convert_options: '-quality 80' },
      xxxhdpi_small: { geometry: '192x192>', convert_options: '-quality 90' },
      xxhdpi_large: { geometry: '1032x1032>', convert_options: '-quality 80' },
      xxhdpi_medium: { geometry: '345x345>', convert_options: '-quality 90' },
      xxhdpi_small: { geometry: '144x144>', convert_options: '-quality 90' },
      xhdpi_large: { geometry: '688x688>', convert_options: '-quality 80' },
      xhdpi_medium: { geometry: '230x230>', convert_options: '-quality 80' },
      xhdpi_small: { geometry: '96x96>', convert_options: '-quality 90' },
      hdpi_large: { geometry: '516x516>', convert_options: '-quality 80' },
      hdpi_medium: { geometry: '173x173>', convert_options: '-quality 90' },
      hdpi_small: { geometry: '72x72>', convert_options: '-quality 90' },
      mdpi_large: { geometry: '344x344>', convert_options: '-quality 90' },
      mdpi_medium: { geometry: '115x115>', convert_options: '-quality 90' },
      mdpi_small: { geometry: '48x48>', convert_options: '-quality 90' }
    },
    carousel: {
      banner_small:{ geometry: '768x192>', convert_options: '-quality 90' },
      banner_medium:{ geometry: '1500x375>', convert_options: '-quality 80' },
      banner_large:{ geometry: '3000x750>', convert_options: '-quality 80' }
    }
  }
else

  S3_IMAGE_STYLES = {
    default: {
      xxxxhdpi_large: { geometry: "1720x1720>", convert_options: '-quality 70' },
      xxxhdpi_large: { geometry: "1376x1376>", convert_options: '-quality 70' },
      xxhdpi_large: { geometry: "1032x1032>", convert_options: '-quality 70' },
      xhdpi_large: { geometry: "688x688>", convert_options: '-quality 70' },
      mdpi_large: { geometry: "516x516>", convert_options: '-quality 70' },
      xhdpi_medium: { geometry: "230x230>", convert_options: '-quality 70' }
    },
    carousel: {
      banner_small:{ geometry: '768x192>', convert_options: '-quality 70' },
      banner_medium:{ geometry: '1500x375>', convert_options: '-quality 70' },
      banner_large:{ geometry: '3000x750>', convert_options: '-quality 70' }
    }
  }
end
