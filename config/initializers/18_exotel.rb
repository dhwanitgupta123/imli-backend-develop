# Exotel configurations for communicating with Exotel APIs
puts '--> Initializing Exotel configurations'

Exotel.configure do |c|
  c.exotel_sid   = APP_CONFIG['config']['EXOTEL_SID']
  c.exotel_token = APP_CONFIG['config']['EXOTEL_TOKEN']
end
