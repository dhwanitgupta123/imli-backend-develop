#
# Class numeric to add percentage calculating function
#
class Numeric

  #
  # Function to add calculate percentage value of a number in numeric class
  #
  def percentage(n)
    self.to_f * n.to_f / 100.0
  end
end