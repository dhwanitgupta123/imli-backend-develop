require 'sidekiq'
require 'sidekiq/web'

puts '--> Initializing Sidekiq'

#
# To authenticate the user in production environment
#
if Rails.env == 'production' || Rails.env == 'feature' || Rails.env == 'pre_prod'
  Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
    [user, password] == ['admin', 'imli-tech']
  end
end

#
# Specifying redis port, used by sidekiq
#
Sidekiq.configure_server do |config|
  redis_host = APP_CONFIG['config']['redis_host']
  redis_port = APP_CONFIG['config']['redis_port']
  redis_url = redis_host + ':' + redis_port
  config.redis = { url: 'redis://' + redis_url }
end
#
# CLient and Server configurtions MUST be same
#
Sidekiq.configure_client do |config|
  redis_host = APP_CONFIG['config']['redis_host']
  redis_port = APP_CONFIG['config']['redis_port']
  redis_url = redis_host + ':' + redis_port
  config.redis = { url: 'redis://' + redis_url }
end
