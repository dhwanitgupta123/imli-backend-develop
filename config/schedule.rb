# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
require File.expand_path(File.dirname(__FILE__) + '/../config/environment')

set :environment, Rails.env
set :output, { error: 'log/cron_error_log.log', standard: 'log/cron_log.log' }


##################################################################################
#                                 Misc. Crons                                    #
##################################################################################

# Runs Pending Users, Email Invites, Product Suggestions Cron everyday at 10:00 hrs
every 1.day, at: Time.zone.now.change(hour: 10).in_time_zone('UTC') do
  # rake 'pending:users' Unscheduling as no more required
  rake 'request:email_invites'
  rake 'product:suggestions'
end

# Runs account_monthly_summary emails for all active users each month on 5th of each month
# The mails get triggered via perform_at operation at 10:00 hrs
every '0 1 5 * *' do
  runner "MarketplaceOrdersModule::V1::OrderPanelService.send_monthly_account_summary_to_users"
end

#### Misc. Crons Ends ####


##################################################################################
#                           Order Aggregation Crons                              #
##################################################################################

# # Runs Order Aggregation for interval "00:00" to "08:00" hrs everyday at 08:00 hrs
# every 1.day, at: Time.zone.now.change(hour: 8, min: 00).in_time_zone('UTC') do
#   rake 'aggregate:orders["00:00:01","08:00:00"]'
# end
#
# # Runs Order Aggregation for interval "08:00:01" to "13:00:00" hrs everyday at 13:00 hrs
# every 1.day, at: Time.zone.now.change(hour: 13, min: 00).in_time_zone('UTC') do
#   rake 'aggregate:orders["08:00:01","13:00:00"]'
# end
#
# # Runs Order Aggregation for interval "13:00:01" to "18:00:00" hrs everyday at 18:00 hrs
# every 1.day, at: Time.zone.now.change(hour: 18, min: 00).in_time_zone('UTC') do
#   rake 'aggregate:orders["13:00:01","18:00:00"]'
# end
#
# # Runs Order Aggregation for interval "18:00:01" to "23:59:59" hrs everyday at 23:59 hrs
# every 1.day, at: Time.zone.now.change(hour: 00, min: 00).in_time_zone('UTC') do
#   rake 'aggregate:orders["18:00:01","00:00:00"]'
# end
#

# # Runs Order Aggregation for interval "07:00:01" to "15:00:00" hrs everyday at 15:00 hrs
# every 1.day, at: Time.zone.now.change(hour: 15, min: 00).in_time_zone('UTC') do
#   rake 'aggregate:orders["07:00:01","15:00:00"]'
# end

# Runs Order Aggregation for interval "03:00:00" to "12:00:00" hrs everyday at 12:15 hrs
every 1.day, at: Time.zone.now.change(hour: 12, min: 15).in_time_zone('UTC') do
  rake 'aggregate:orders["03:00:00","12:00:00"]'
end

# Runs Order Aggregation for interval "12:00:00" to "03:00:00" hrs everyday at 07:00 hrs
every 1.day, at: Time.zone.now.change(hour: 07, min: 00).in_time_zone('UTC') do
  rake 'aggregate:orders["12:00:00","03:00:00"]'
end

#### Order Aggregation Crons Ends ####


##################################################################################
#                          Kibana Data Aggregation Cron                          #
##################################################################################
#
# This will run every day at 01:00 hours and update data that is used
# by kibana
#
every 1.day, at: Time.zone.now.change(hour: 04, min: 00).in_time_zone('UTC') do
  runner 'DataAggregationModule::V1::KibanaDataAggregationService.aggregate'
end

#### Kibana Data Aggregation Cron Ends ####

#
# Commenting this cron as we are not using it currently
#
# every 1.day, at: Time.zone.now.change(hour: 01, min: 00).in_time_zone('UTC') do
#   runner 'SweeperModule::V1::ImageSweeperWorker.perform_async'
# end

##################################################################################
#                                 Warehouse Data Cron                            #
##################################################################################
#
# This will run every day at 06:00 hours and update threshold stock data
#
every 1.day, at: Time.zone.now.change(hour: 06, min: 00).in_time_zone('UTC') do
  runner 'SupplyChainModule::V1::WarehouseUtil.update_stock_threshold_value'
end

#
# This will run every day at 06:00 hours and update buffer stock data
#
every 1.day, at: Time.zone.now.change(hour: 05, min: 00).in_time_zone('UTC') do
  runner 'SupplyChainModule::V1::WarehouseUtil.update_buffer_stock_value'
end

#
# This will run every monday to send the fill rate of each vendor
#
every :friday, at: Time.zone.now.change(hour: 9, min: 00).in_time_zone('UTC') do
  runner 'InventoryOrdersModule::V1::InventoryOrdersUtil.get_po_fill_reports'
end

#
# This will run every day at 8 and send PO dump
#
every 1.day, at: Time.zone.now.change(hour: 8, min: 00).in_time_zone('UTC') do
  runner 'InventoryOrdersModule::V1::InventoryOrdersUtil.get_purchase_order_dump'
end

#
# This will run every day at 8 and send Out of stock Products
#
every 1.day, at: Time.zone.now.change(hour: 8, min: 00).in_time_zone('UTC') do
  runner 'SupplyChainModule::V1::WarehouseUtil.send_out_of_stock_products_report'
end

##################################################################################
#                              Membership Expiry Cron                            #
##################################################################################
#
# Cron to execute membership expiry
#
every 1.day, at: Time.zone.now.change(hour: 05).in_time_zone('UTC') do
  runner 'UsersModule::V1::MembershipService.expire_membership'
end
