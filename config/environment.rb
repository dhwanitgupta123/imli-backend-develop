# Load the Rails application.
require File.expand_path('../application', __FILE__)
# Initialize global variable for storing user session
USER_SESSION = {}
# Global variable APP_CONFIG to store all environment specific config
APP_CONFIG = {}

##################################################################
#               FETCH GLOBAL CONFIG FROM CONSUL                  #
##################################################################

# Consul configuration (environment specific)
# Use default host and ACL token for development, test and feature environment
puts '--> Initializing Consul - distributed store'
consul_host_url = 'http://consul.buyample.com/'
consul_acl_token = 'f575f23d-36be-4ab9-356e-c558371b7654'
case Rails.env
  when 'development'
    consul_acl_token = 'f575f23d-36be-4ab9-356e-c558371b7654'
  when 'feature'
    consul_acl_token = '9ba5bd58-bcba-1fd4-d1e0-123c8132b004'
  when 'pre_prod'
    consul_acl_token = '2f24308d-b230-5a02-c51a-a2690c087f96'
  when 'production'
    consul_acl_token = '444d803f-74b1-fc2a-cb2a-e90308111b42'
end

# Configuration for accessing CONSUL
# Needed to do before initialization so that global APP_CONFIG
# can be fetched
Diplomat.configure do |config|
  # Set up a custom Consul URL
  config.url = consul_host_url
  # Set up a custom Faraday Middleware
  #config.middleware = MyCustomMiddleware
  # Connect into consul with custom access token (ACL)
  config.acl_token =  consul_acl_token
  # Set extra Faraday configuration options
  #config.options = {ssl: { version: :TLSv1_2 }}
end

#
# Function to fetch Application Configuration from Consul
#
def fetch_app_config_from_consul
  begin
    default_app_config = JSON.parse(Diplomat::Kv.get('default' + '/backend/imli-backend/default/default/APP_CONFIG'))
  rescue => e
    puts '[FAILED]: CONSUL - default APP CONFIG fetching failed, ' + e.message
    # Do not raise anything
  end
  begin
    environment_app_config = JSON.parse(Diplomat::Kv.get(Rails.env + '/backend/imli-backend/default/default/APP_CONFIG'))
  rescue => e
    puts '[FAILED]: CONSUL - ' + Rails.env.to_s + ' specific APP_CONFIG fetching failed, ' + e.message
    # Do not raise anything
  end
  if default_app_config.present? || environment_app_config.present?
    puts '[SUCCESS]: CONSUL - APP Config fetched from consul successfully'
  end
  if default_app_config.present? && environment_app_config.present?
    return default_app_config.merge(environment_app_config)
  else
    return default_app_config || environment_app_config
  end
end

#
# Function to fetch Application Configuration from File
#
def fetch_app_config_from_file
  app_config_file = Rails.root.join("config", "app_config.yml").to_s
  app_config_hash = {}
  if File.file?(app_config_file)
    YAML.load_file(app_config_file)[Rails.env].each do |key, value|
      app_config_hash[key.to_s] = value.to_s
    end # end YAML.load_file
    puts '[SUCCESS]: YML - AppConfig fetched from local file path: ' + app_config_file.to_s
  end # end if File.exists?
  return app_config_hash
end

# APP CONFIG initialization should be done before application initialization
# Load all environment specific application config variables into APP_CONFIG variable
puts '********************************************'
puts '*********** FETCHING APP_CONFIG ************'
puts '********************************************'

app_config = fetch_app_config_from_consul
if app_config.nil?
  app_config = fetch_app_config_from_file
end
APP_CONFIG['config'] = app_config

puts ''
puts 'App config fetched successfuly and is stored'
puts 'in global variable APP_CONFIG[:config]      '
puts '********************************************'
puts ''

##################################################################
#               RAILS APPLICATION INITIALIZATION                 #
##################################################################

# Initialize the Rails application.
Rails.application.initialize!
